#!/bin/bash

THIS=$(readlink -f "${BASH_SOURCE[0]}")
BASE_DIR=$(dirname $(dirname $THIS))

CLEAR=n
BATCH=
APT_ONLY=
INSTALL="$BASE_DIR/install"

while [ "$1" != "" ]; do
    if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
	cat <<EOF

build LN on osl15.4:

  ${BASH_SOURCE[0]} [-c] [<INSTALL_PREFIX>]

options:

  -c    clear any pre-existing build-artifacts

<INSTALL_PREFIX> defaults to $INSTALL.

this script will try to install any required zypper packages.

EOF
	exit 0
    fi
    if [ "$1" == "--batch" ]; then
	BATCH="-y"
	shift
	continue
    fi
    if [ "$1" == "--zypper-only" ]; then
	APT_ONLY=y
	shift
	continue
    fi
    if [ "$1" == "-c" ]; then
	CLEAR=y
	shift
	continue
    fi
    INSTALL="$1"
    shift
done

mkdir -p "$INSTALL"
INSTALL=$(cd "$INSTALL"; pwd -P)

BUILD_DIR=$BASE_DIR/build
REQ_BASE_DIR=$BASE_DIR/requirements

SUDO=
if [ $(id -u) != 0 ]; then
    SUDO="sudo "
fi

# todo:
#  check / `apt install` required system packages
REQUIRED="make git python3 scons which gcc-c++ automake autoconf libtool procps-devel libboost_python-py3-1_75_0-devel openssh-clients python3-devel python3-numpy-devel python3-Sphinx python3-PyYAML python3-numpy python3-gobject typelib-1_0-Gtk-3_0 typelib-1_0-Vte-2.91 typelib-1_0-GtkSource-4 iproute2"
TO_INSTALL=""
for pkg in $REQUIRED; do
    if ! rpm -q "$pkg" >/dev/null 2>&1; then
	TO_INSTALL="$TO_INSTALL $pkg"
    fi
done
if [ ! -z "$TO_INSTALL" ]; then
    echo $SUDO zypper install $BATCH $TO_INSTALL
    $SUDO zypper install $BATCH $TO_INSTALL
fi

if [ "$APT_ONLY" == "y" ]; then
    echo "--zypper-only done."
    exit 0
fi

if [ "$CLEAR" == "y" ]; then
    test -d "$BUILD_DIR" && rm -rf $BUILD_DIR
    (
	cd "$BASE_DIR"
	scons -c
    )
fi

mkdir -p "$REQ_BASE_DIR"
(
    cd "$REQ_BASE_DIR"
    test -d libstring_util/.git || git clone --recursive https://gitlab.com/links_and_nodes/libstring_util
    cd libstring_util
    test -f configure || autoreconf -if
    mkdir -p build
    cd build
    set -e
    test -f Makefile || ../configure "--prefix=$INSTALL"
    make -j4 install
)


(
    export LD_LIBRARY_PATH=$INSTALL/lib:$LD_LIBRARY_PATH
    cd $BASE_DIR
    scons "--prefix=$INSTALL" --use-private-libtomcrypt --write-env install
)

if [ "$BATCH" == "" ]; then
    echo
    echo "execute following command to load environment for this ln-install:"
    echo "    . $INSTALL/ln_env.sh"
fi
