#!/usr/bin/env python3

from whats_new import *

if __name__ == "__main__":
    # whats new ?
    whats_new(
        "release/2.1.0", "origin/release/2.0.9",
        skip_prefixes=[
            "tests: ",
        ],

        skip_authors=[
            "Johannes", # aside from documentation updates
        ],

        skip_patterns=[
            ".*param.*", # aside from C++-parameters
            ".*doc.*", # aside from documentation updates
            "Merge .*branch '.*release/2.",
        ],
    )
