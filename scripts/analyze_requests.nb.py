## nb properties ##
{'current_cell': 3, 'window_position': (202, 26), 'window_size': (1080, 1165)}
## nb properties end ##
## cell 0 input ##
dn = "/tmp/debug_requests/20191125132210"
## cell 0 end ##
## cell 1 input ##
files = dict()
for e in os.listdir(dn):
    if e[:1] == ".": continue    
    if "_" not in e: continue
    p = os.path.join(dn, e)
    if not os.path.isfile(p): continue
    cnt, bn = e.split("_", 1)    
    if bn not in files:
        files[bn] = dict()
        % bn
    files[bn][int(cnt)] = p

## cell 1 output ##
# bn: recv
# bn: send
# 
## cell 1 end ##
## cell 2 input ##
import sys
ln_base = "/home/that/fs/ln_base"
sys.path.insert(0, ln_base + "/manager")
sys.path.insert(0, ln_base + "/contrib")
## cell 2 end ##
## cell 3 input ##
fn = files["recv"][420]
% os.path.getsize(fn)
import cPickle
with file(fn, "rb") as fp:
    data = cPickle.load(fp)
% type(data), len(data)
if isinstance(data, tuple):
    %% tuple(map(type, data))

rtype, rdata = data
% rtype
for key, value in rdata.iteritems():
    % key, type(value)
%% rdata
## cell 3 output ##
#  os.path.getsize(fn): 3442702
# type(data), len(data): (<type 'tuple'>, 2)
# tuple(map(type, data)): (<type 'str'>, <type 'dict'>)
#  rtype: notify
# key, type(value): ('msg', <type 'str'>)
# key, type(value): ('client', <class 'ClientStore.ClientStore'>)
# key, type(value): ('ts', <type 'float'>)
# rdata: {'client': <client at program_name: 'justin robotkernel-5 1', pid: 3583, host: face>,
#  'msg': "unregister_service 'justin robotkernel-5 1.ecat.slave_10.mailbox.canopen_protocol.pop_emergency_message'",
#  'ts': 1574684497.416771}
# 
## cell 3 end ##
