import re
import git

def whats_new(new_ref_name, old_ref_name, skip_prefixes=[], skip_patterns=[], skip_authors=[]):
    repo = git.Repo()

    new_ref = old_ref = None
    for ref in repo.references:
        if str(ref) == new_ref_name:
            new_ref = ref
        if str(ref) == old_ref_name:
            old_ref = ref
    if new_ref is None:
        raise Exception("new_ref %r not found!" % new_ref_name)
    if old_ref is None:
        raise Exception("old_ref %r not found!" % old_ref_name)

    branch_start = new_ref.log()[0]

    new_commits = []
    for c in git.objects.Commit.iter_items(repo, "%s..%s" % (branch_start.newhexsha, new_ref)):
        new_commits.append(c)
    old_commits = []
    old_hash = {} # hash -> commit
    old_msg = {} # msg -> commit
    for c in git.objects.Commit.iter_items(repo, "%s..%s" % (branch_start.newhexsha, old_ref)):
        old_commits.append(c)
        old_hash[c.hexsha] = c
        old_msg[c.message] = c

    for c in new_commits:
        if c.message in old_msg:
            continue
        if c.hexsha in old_hash:
            continue
        skip = False
        for sp in skip_prefixes:
            if c.summary.startswith(sp):
                skip = True
                break
        for sp in skip_patterns:
            if re.match(sp, c.summary) is not None:
                skip = True
                break
        for a in skip_authors:
            if a in str(c.author):
                skip = True
                break
        if skip:
            continue

        print("%-80.80s %s %s %s" % (
            c.summary, c.hexsha, c.author, c.committed_datetime, ))
