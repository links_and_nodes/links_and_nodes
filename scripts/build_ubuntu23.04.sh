#!/bin/bash

THIS=$(readlink -f "${BASH_SOURCE[0]}")
BASE_DIR=$(dirname $(dirname $THIS))

CLEAR=n
BATCH=
APT_ONLY=
INSTALL="$BASE_DIR/install"

while [ "$1" != "" ]; do
    if [ "$1" == "-h" ]; then
	cat <<EOF

build LN on ubuntu23.04:

  ${BASH_SOURCE[0]} [-c] [<INSTALL_PREFIX>]

options:

  -c    clear any pre-existing build-artifacts

<INSTALL_PREFIX> defaults to $INSTALL.

this script will try to install any required debian packages.

EOF
	exit 0
    fi
    if [ "$1" == "--batch" ]; then
	BATCH="--no-install-recommends -y"
	shift
	continue
    fi
    if [ "$1" == "--apt-only" ]; then
	APT_ONLY=y
	shift
	continue
    fi
    if [ "$1" == "-c" ]; then
	CLEAR=y
	shift
	continue
    fi
    INSTALL="$1"
    shift
done

mkdir -p "$INSTALL"
INSTALL=$(cd "$INSTALL"; pwd -P)

BUILD_DIR=$BASE_DIR/build
REQ_BASE_DIR=$BASE_DIR/requirements

SUDO=
if [ $(id -u) != 0 ]; then
    SUDO="sudo "
fi

# todo:
#  check / `apt install` required system packages
REQUIRED="git g++ scons automake make libtool libproc2-dev libdpkg-perl libpython3-dev python3-sphinx python3-dev libboost-python-dev python3-yaml python3-numpy python3-gi python3-gi-cairo gir1.2-gtk-3.0 gir1.2-vte-2.91 gir1.2-gtksource-4 openssh-client iproute2"
TO_INSTALL=""
for pkg in $REQUIRED; do
    if ! dpkg -s $pkg >/dev/null 2>&1; then
	TO_INSTALL="$TO_INSTALL $pkg"
    fi
done
if [ ! -z "$TO_INSTALL" ]; then
    echo $SUDO apt-get install $BATCH $TO_INSTALL
    $SUDO apt-get install $BATCH $TO_INSTALL
fi

if [ "$APT_ONLY" == "y" ]; then
    echo "--apt-only done."
    exit 0
fi

if [ "$CLEAR" == "y" ]; then
    test -d "$BUILD_DIR" && rm -rf $BUILD_DIR
    (
	cd "$BASE_DIR"
	scons -c
    )
fi

mkdir -p "$REQ_BASE_DIR"
set -e
(
    cd "$REQ_BASE_DIR"
    test -d libstring_util/.git || git clone --recursive https://gitlab.com/links_and_nodes/libstring_util
    cd libstring_util
    test -f configure || autoreconf -if
    mkdir -p build
    cd build
    test -f Makefile || ../configure "--prefix=$INSTALL"
    make -j4 install
)

(
    export LD_LIBRARY_PATH=$INSTALL/lib:$LD_LIBRARY_PATH
    cd $BASE_DIR
    scons "--prefix=$INSTALL" --use-private-libtomcrypt --write-env install
)

if [ "$BATCH" == "" ]; then
    echo
    echo "execute following command to load environment for this ln-install:"
    echo "    . $INSTALL/ln_env.sh"
fi
