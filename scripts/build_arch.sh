#!/bin/bash

# tested with archlinux:base-20230723.0.166908

THIS=$(readlink -f "${BASH_SOURCE[0]}")
BASE_DIR=$(dirname $(dirname $THIS))

CLEAR=n
BATCH=
APT_ONLY=
INSTALL="$BASE_DIR/install"

while [ "$1" != "" ]; do
    if [ "$1" == "-h" ]; then
	cat <<EOF

build LN on archlinux:base-20230723.0.166908:

  ${BASH_SOURCE[0]} [-c] [<INSTALL_PREFIX>]

options:

  -c    clear any pre-existing build-artifacts

<INSTALL_PREFIX> defaults to $INSTALL.

this script will try to install any required debian packages.

EOF
	exit 0
    fi
    if [ "$1" == "--batch" ]; then
	BATCH="--noconfirm"
	shift
	continue
    fi
    if [ "$1" == "--apt-only" ]; then
	APT_ONLY=y
	shift
	continue
    fi
    if [ "$1" == "-c" ]; then
	CLEAR=y
	shift
	continue
    fi
    INSTALL="$1"
    shift
done

mkdir -p "$INSTALL"
INSTALL=$(cd "$INSTALL"; pwd -P)

BUILD_DIR=$BASE_DIR/build
REQ_BASE_DIR=$BASE_DIR/requirements

SUDO=
if [ $(id -u) != 0 ]; then
    SUDO="sudo "
fi

# todo:
#  check / `apt install` required system packages
REQUIRED="git gcc scons base-devel            libprocps                  python         python-sphinx              boost               python-yaml  python-numpy  python-gobject python-cairo gtk3       vte3            gtksourceview4     openssh         iproute2"
TO_INSTALL=""
for pkg in $REQUIRED; do
    if ! pacman -Qs $pkg >/dev/null 2>&1; then
	TO_INSTALL="$TO_INSTALL $pkg"
    fi
done
if [ ! -z "$TO_INSTALL" ]; then
    echo $SUDO pacman -S $BATCH $TO_INSTALL
    $SUDO pacman -S $BATCH $TO_INSTALL
fi

if [ "$APT_ONLY" == "y" ]; then
    echo "--apt-only done."
    exit 0
fi

if [ "$CLEAR" == "y" ]; then
    test -d "$BUILD_DIR" && rm -rf $BUILD_DIR
    (
	cd "$BASE_DIR"
	scons -c
    )
fi

mkdir -p "$REQ_BASE_DIR"
set -e
(
    cd "$REQ_BASE_DIR"
    test -d libstring_util/.git || git clone --recursive https://gitlab.com/links_and_nodes/libstring_util
    cd libstring_util
    test -f configure || autoreconf -if
    mkdir -p build
    cd build
    test -f Makefile || ../configure "--prefix=$INSTALL"
    make -j4 install
)

(
    export LD_LIBRARY_PATH=$INSTALL/lib:$LD_LIBRARY_PATH
    cd $BASE_DIR
    scons "--prefix=$INSTALL" --use-private-libtomcrypt --write-env install
)

if [ "$BATCH" == "" ]; then
    echo
    echo "execute following command to load environment for this ln-install:"
    echo "    . $INSTALL/ln_env.sh"
fi
