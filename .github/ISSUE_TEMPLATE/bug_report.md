---
name: Bug report
about: Create a report to help us improve
title: ''
labels: bug
assignees: ''

---

Describe the bug: A clear and concise description of what the bug is.

if this problem is about the ln-manager, please use the `save all` feature directly after you discovered the problem, see here: https://rmc-github.robotic.dlr.de/pages/common/links_and_nodes/howto_report_lnm_bug.html
and attach the generated file here.
