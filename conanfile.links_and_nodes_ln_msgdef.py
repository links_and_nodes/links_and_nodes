import os

from conan import ConanFile, conan_version
from conan.tools.files import copy
from conan.tools.scm import Version


class links_and_nodes_ln_msgdef_conan(ConanFile):
    package_type = "shared-library"
    name = "links_and_nodes_ln_msgdef"
    url = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"
    author = "Florian Schmidt <florian.schmidt@dlr.de>"
    license = "GPLv3"
    description = "links_and_nodes standard set of message definitions"
    settings = None
    exports_sources = [
        # all of python for scons to be able to build python
        "share/message_definitions/*",
        "ln_runtime/file_services/message_definitions/*",
        "ln_runtime/lnrecorder/message_definitions/*",
    ]

    def package(self):
        copy(self, "message_definitions/*", os.path.join(self.source_folder, "share"), self.package_folder)
        copy(
            self, "message_definitions/*", os.path.join(self.source_folder, "ln_runtime", "file_services"), self.package_folder
        )
        copy(self, "message_definitions/*", os.path.join(self.source_folder, "ln_runtime", "lnrecorder"), self.package_folder)

    def package_info(self):
        msg_def_dirs = os.path.join(self.package_folder, "message_definitions")
        if Version(conan_version) < "2.0.0":
            self.env_info.LN_MESSAGE_DEFINITION_DIRS.append(msg_def_dirs)
        self.buildenv_info.append_path("LN_MESSAGE_DEFINITION_DIRS", msg_def_dirs)
        self.runenv_info.append_path("LN_MESSAGE_DEFINITION_DIRS", msg_def_dirs)
