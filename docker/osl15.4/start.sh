#!/bin/sh

# allow access to ssh-forwarded x-connection / DISPLAY
if [ -z "$XAUTHORITY" ]; then
    XAUTHORITY=$HOME/.Xauthority
fi
echo DISPLAY: $DISPLAY XAUTHORITY: $XAUTHORITY
chmod go+r $XAUTHORITY
# for local X11 you might need:
#  xhost + >/dev/null 2>&1
#    -v /dev:/dev \
#    -v /tmp/.X11-unix:/tmp/.X11-unix \

docker run --name ln_osl15_4 \
    --privileged \
    -it \
    -e DISPLAY=$DISPLAY \
    -v $XAUTHORITY:/root/.Xauthority \
    -e https_proxy=$https_proxy \
    -e http_proxy=$http_proxy \
    --net host \
    --rm \
    --ipc host \
    ln/osl:15.4
