#!/bin/bash

# allow access to ssh-forwarded x-connection / DISPLAY
if [ -z "$XAUTHORITY" ]; then
    XAUTHORITY=$HOME/.Xauthority
fi
echo DISPLAY: $DISPLAY XAUTHORITY: $XAUTHORITY
chmod go+r $XAUTHORITY
# for local X11 you might need:
#  xhost + >/dev/null 2>&1
#    -v /dev:/dev \
#    -v /tmp/.X11-unix:/tmp/.X11-unix \

THIS=$(readlink -f ${BASH_SOURCE[0]})
THIS_DIR=$(dirname $THIS)
BASE_DIR=$(dirname $(dirname $THIS_DIR))

docker run --name ln_arch_base \
    --privileged \
    -it \
    -e DISPLAY=$DISPLAY \
    -v $XAUTHORITY:/root/.Xauthority \
    -e https_proxy=$https_proxy \
    -e http_proxy=$http_proxy \
    --net host \
    --rm \
    --ipc host \
    ln/arch:base

#    --mount type=bind,source=$BASE_DIR,target=/opt/links_and_nodes/src \
#    --mount type=bind,source=$BASE_DIR/python/links_and_nodes_manager,target=/opt/links_and_nodes/lib/python3.11/site-packages/links_and_nodes_manager \
