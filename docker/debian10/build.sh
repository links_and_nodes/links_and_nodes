#!/bin/sh

exec docker build \
     --build-arg "https_proxy=$https_proxy" \
     --build-arg "http_proxy=$http_proxy" \
     --network host \
     -t ln/debian:10 \
     -f Dockerfile \
     ../..
