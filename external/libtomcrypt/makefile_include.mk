#
# Include makefile used by makefile + makefile.shared
#  (GNU make only)

# The version - BEWARE: VERSION, VERSION_PC and VERSION_LT are updated via ./updatemakes.sh
VERSION=1.18.2-develop
VERSION_PC=1.18.2
# http://www.gnu.org/software/libtool/manual/html_node/Updating-version-info.html
VERSION_LT=1:1

# Compiler and Linker Names
ifndef CROSS_COMPILE
  CROSS_COMPILE:=
endif

# We only need to go through this dance of determining the right compiler if we're using
# cross compilation, otherwise $(CC) is fine as-is.
ifneq (,$(CROSS_COMPILE))
ifeq ($(origin CC),default)
CSTR := "\#ifdef __clang__\nCLANG\n\#endif\n"
ifeq ($(PLATFORM),FreeBSD)
  # XXX: FreeBSD needs extra escaping for some reason
  CSTR := $$$(CSTR)
endif
ifneq (,$(shell echo $(CSTR) | $(CC) -E - | grep CLANG))
  CC := $(CROSS_COMPILE)clang
else
  CC := $(CROSS_COMPILE)gcc
endif # Clang
endif # cc is Make's default
endif # CROSS_COMPILE non-empty

LD:=$(CROSS_COMPILE)ld
AR:=$(CROSS_COMPILE)ar

# Archiver [makes .a files]
#AR=ar
ARFLAGS:=r

ifndef MAKE
# BSDs refer to GNU Make as gmake
ifneq (,$(findstring $(PLATFORM),FreeBSD OpenBSD DragonFly NetBSD))
  MAKE=gmake
else
  MAKE=make
endif
endif

ifndef INSTALL_CMD
$(error your makefile must define INSTALL_CMD)
endif
ifndef UNINSTALL_CMD
$(error your makefile must define UNINSTALL_CMD)
endif

ifndef EXTRALIBS
ifneq ($(shell echo $(CFLAGS) | grep USE_LTM),)
EXTRALIBS=$(shell PKG_CONFIG_PATH=$(LIBPATH)/pkgconfig pkg-config libtommath --libs)
else
ifneq ($(shell echo $(CFLAGS) | grep USE_TFM),)
EXTRALIBS=$(shell PKG_CONFIG_PATH=$(LIBPATH)/pkgconfig pkg-config tomsfastmath --libs)
endif
endif
endif

need-help := $(filter help,$(MAKECMDGOALS))
define print-help
$(if $(need-help),$(info $1 -- $2))
endef

#
# Compilation flags. Note the += does not write over the user's CFLAGS!
#
# Also note that we're extending the environments' CFLAGS.
# If you think that our CFLAGS are not nice you can easily override them
# by giving them as a parameter to make:
#  make CFLAGS="-I./src/headers/ -DLTC_SOURCE ..." ...
#
LTC_CFLAGS += -I./src/headers/ -DLTC_SOURCE -Wall -Wsign-compare -Wshadow

ifdef OLD_GCC
LTC_CFLAGS += -W
# older GCCs can't handle the "rotate with immediate" ROLc/RORc/etc macros
# define this to help
LTC_CFLAGS += -DLTC_NO_ROLC
else
LTC_CFLAGS += -Wextra
# additional warnings
LTC_CFLAGS += -Wsystem-headers -Wbad-function-cast -Wcast-align
LTC_CFLAGS += -Wstrict-prototypes -Wpointer-arith
LTC_CFLAGS += -Wdeclaration-after-statement
LTC_CFLAGS += -Wwrite-strings
endif

ifdef LTC_DEBUG
$(info Debug build)
# compile for DEBUGGING (required for ccmalloc checking!!!)
LTC_CFLAGS += -g3 -DLTC_NO_ASM

ifneq (,$(strip $(LTC_DEBUG)))
LTC_CFLAGS += -DLTC_TEST_DBG=$(LTC_DEBUG)
else
LTC_CFLAGS += -DLTC_TEST_DBG
endif
endif # LTC_DEBUG

ifndef IGNORE_SPEED
# optimize for SPEED
LTC_CFLAGS += -O3 -funroll-loops

# add -fomit-frame-pointer.  hinders debugging!
LTC_CFLAGS += -fomit-frame-pointer
endif # IGNORE_SPEED

ifdef LTC_SMALL
# optimize for SIZE
LTC_CFLAGS += -Os -DLTC_SMALL_CODE
endif # LTC_SMALL


ifneq ($(findstring clang,$(CC)),)
LTC_CFLAGS += -Wno-typedef-redefinition -Wno-tautological-compare -Wno-builtin-requires-header
LTC_CFLAGS += -Wno-missing-field-initializers -Wno-missing-braces
endif
ifneq ($(findstring mingw,$(CC)),)
LTC_CFLAGS += -Wno-shadow -Wno-attributes
endif
ifeq ($(PLATFORM), Darwin)
LTC_CFLAGS += -Wno-nullability-completeness
endif


GIT_VERSION := $(shell { [ -e .git ] && which git 2>/dev/null 1>&2 ; } && { printf git- ; git describe --tags --always --dirty ; } || echo $(VERSION))
ifneq ($(GIT_VERSION),)
LTC_CFLAGS += -DGIT_VERSION=\"$(GIT_VERSION)\"
endif

LTC_CFLAGS := $(LTC_CFLAGS) $(CFLAGS)

ifneq ($(findstring -DLTC_PTHREAD,$(LTC_CFLAGS)),)
LTC_LDFLAGS += -pthread
endif

LTC_LDFLAGS := $(LTC_LDFLAGS) $(LDFLAGS)

ifeq ($(V)$(filter clean,$(MAKECMDGOALS)),0)
$(warning CFLAGS=$(LTC_CFLAGS))
$(warning LDFLAGS=$(LTC_LDFLAGS))
endif

#List of demo objects
DSOURCES = $(wildcard demos/*.c)
DOBJECTS = $(DSOURCES:.c=.o)

#List of tests headers
THEADERS = $(wildcard tests/*.h)

TEST=test

# Demos that are even somehow useful and could be installed as a system-tool
USEFUL_DEMOS   = hashsum

# Demos that are usable but only rarely make sense to be installed
USEABLE_DEMOS  = ltcrypt sizes constants

# Demos that are used for testing or measuring
TEST_DEMOS     = small tv_gen

# Demos that are in one config broken
#  aesgcm      - can't be built with LTC_EASY
#  openssl-enc - can't be built with LTC_EASY
#  timing      - not really broken, but older gcc builds spit warnings
BROKEN_DEMOS   = aesgcm openssl-enc timing

# Combine demos in groups
UNBROKEN_DEMOS = $(TEST_DEMOS) $(USEABLE_DEMOS) $(USEFUL_DEMOS)
DEMOS          = $(UNBROKEN_DEMOS) $(BROKEN_DEMOS)

#LIBPATH  The directory for libtomcrypt to be installed to.
#INCPATH  The directory to install the header files for libtomcrypt.
#DATAPATH The directory to install the pdf docs.
#BINPATH  The directory to install the binaries provided.
DESTDIR  ?=
PREFIX   ?= /usr/local
LIBPATH  ?= $(PREFIX)/lib
INCPATH  ?= $(PREFIX)/include
DATAPATH ?= $(PREFIX)/share/doc/libtomcrypt/pdf
BINPATH  ?= $(PREFIX)/bin

#Who do we install as?
ifdef INSTALL_USER
USER=$(INSTALL_USER)
else
USER=root
endif

ifdef INSTALL_GROUP
GROUP=$(INSTALL_GROUP)
else
GROUP=wheel
endif


#The first rule is also the default rule and builds the libtomcrypt library.
library: $(call print-help,library,Builds the library) $(LIBNAME)


# List of objects to compile (all goes to libtomcrypt.a)
OBJECTS= src/ciphers/tea.o src/ciphers/xtea.o \
src/hashes/helper/hash_memory.o \
src/hashes/helper/hash_memory_multi.o src/hashes/sha1.o \
src/math/fp/ltc_ecc_fp_mulmod.o src/math/gmp_desc.o src/math/ltm_desc.o src/math/multi.o \
src/math/radix_to_bin.o src/math/rand_bn.o src/math/rand_prime.o src/math/tfm_desc.o src/misc/burn_stack.o src/misc/compare_testvector.o \
src/misc/crc32.o src/misc/crypt/crypt.o src/misc/crypt/crypt_argchk.o \
src/misc/crypt/crypt_cipher_descriptor.o src/misc/crypt/crypt_cipher_is_valid.o \
src/misc/crypt/crypt_constants.o src/misc/crypt/crypt_find_cipher.o \
src/misc/crypt/crypt_find_cipher_any.o src/misc/crypt/crypt_find_cipher_id.o \
src/misc/crypt/crypt_find_hash.o src/misc/crypt/crypt_find_hash_any.o \
src/misc/crypt/crypt_find_hash_id.o src/misc/crypt/crypt_find_hash_oid.o \
src/misc/crypt/crypt_find_prng.o src/misc/crypt/crypt_fsa.o src/misc/crypt/crypt_hash_descriptor.o \
src/misc/crypt/crypt_hash_is_valid.o src/misc/crypt/crypt_inits.o \
src/misc/crypt/crypt_ltc_mp_descriptor.o src/misc/crypt/crypt_prng_descriptor.o \
src/misc/crypt/crypt_prng_is_valid.o src/misc/crypt/crypt_prng_rng_descriptor.o \
src/misc/crypt/crypt_register_all_ciphers.o src/misc/crypt/crypt_register_all_hashes.o \
src/misc/crypt/crypt_register_all_prngs.o src/misc/crypt/crypt_register_cipher.o \
src/misc/crypt/crypt_register_hash.o src/misc/crypt/crypt_register_prng.o src/misc/crypt/crypt_sizes.o \
src/misc/crypt/crypt_unregister_cipher.o src/misc/crypt/crypt_unregister_hash.o \
src/misc/crypt/crypt_unregister_prng.o src/misc/error_to_string.o src/misc/mem_neq.o src/misc/padding/padding_depad.o \
src/misc/padding/padding_pad.o src/misc/zeromem.o src/modes/ctr/ctr_decrypt.o \
src/modes/ctr/ctr_done.o src/modes/ctr/ctr_encrypt.o src/modes/ctr/ctr_getiv.o \
src/modes/ctr/ctr_setiv.o src/modes/ctr/ctr_start.o src/modes/ctr/ctr_test.o \
src/pk/asn1/der/bit/der_decode_bit_string.o \
src/pk/asn1/der/bit/der_decode_raw_bit_string.o src/pk/asn1/der/bit/der_encode_bit_string.o \
src/pk/asn1/der/bit/der_encode_raw_bit_string.o src/pk/asn1/der/bit/der_length_bit_string.o \
src/pk/asn1/der/boolean/der_decode_boolean.o src/pk/asn1/der/boolean/der_encode_boolean.o \
src/pk/asn1/der/boolean/der_length_boolean.o src/pk/asn1/der/choice/der_decode_choice.o \
src/pk/asn1/der/custom_type/der_decode_custom_type.o \
src/pk/asn1/der/custom_type/der_encode_custom_type.o \
src/pk/asn1/der/custom_type/der_length_custom_type.o src/pk/asn1/der/general/der_asn1_maps.o \
src/pk/asn1/der/general/der_decode_asn1_identifier.o src/pk/asn1/der/general/der_decode_asn1_length.o \
src/pk/asn1/der/general/der_encode_asn1_identifier.o src/pk/asn1/der/general/der_encode_asn1_length.o \
src/pk/asn1/der/general/der_length_asn1_identifier.o src/pk/asn1/der/general/der_length_asn1_length.o \
src/pk/asn1/der/generalizedtime/der_decode_generalizedtime.o \
src/pk/asn1/der/generalizedtime/der_encode_generalizedtime.o \
src/pk/asn1/der/generalizedtime/der_length_generalizedtime.o \
src/pk/asn1/der/ia5/der_decode_ia5_string.o src/pk/asn1/der/ia5/der_encode_ia5_string.o \
src/pk/asn1/der/ia5/der_length_ia5_string.o src/pk/asn1/der/integer/der_decode_integer.o \
src/pk/asn1/der/integer/der_encode_integer.o src/pk/asn1/der/integer/der_length_integer.o \
src/pk/asn1/der/object_identifier/der_decode_object_identifier.o \
src/pk/asn1/der/object_identifier/der_encode_object_identifier.o \
src/pk/asn1/der/object_identifier/der_length_object_identifier.o \
src/pk/asn1/der/octet/der_decode_octet_string.o src/pk/asn1/der/octet/der_encode_octet_string.o \
src/pk/asn1/der/octet/der_length_octet_string.o \
src/pk/asn1/der/printable_string/der_decode_printable_string.o \
src/pk/asn1/der/printable_string/der_encode_printable_string.o \
src/pk/asn1/der/printable_string/der_length_printable_string.o \
src/pk/asn1/der/sequence/der_decode_sequence_ex.o \
src/pk/asn1/der/sequence/der_decode_sequence_flexi.o \
src/pk/asn1/der/sequence/der_decode_sequence_multi.o \
src/pk/asn1/der/sequence/der_encode_sequence_ex.o \
src/pk/asn1/der/sequence/der_encode_sequence_multi.o src/pk/asn1/der/sequence/der_length_sequence.o \
src/pk/asn1/der/sequence/der_sequence_free.o src/pk/asn1/der/sequence/der_sequence_shrink.o \
src/pk/asn1/der/set/der_encode_set.o src/pk/asn1/der/set/der_encode_setof.o \
src/pk/asn1/der/short_integer/der_decode_short_integer.o \
src/pk/asn1/der/short_integer/der_encode_short_integer.o \
src/pk/asn1/der/short_integer/der_length_short_integer.o \
src/pk/asn1/der/teletex_string/der_decode_teletex_string.o \
src/pk/asn1/der/teletex_string/der_length_teletex_string.o \
src/pk/asn1/der/utctime/der_decode_utctime.o src/pk/asn1/der/utctime/der_encode_utctime.o \
src/pk/asn1/der/utctime/der_length_utctime.o src/pk/asn1/der/utf8/der_decode_utf8_string.o \
src/pk/asn1/der/utf8/der_encode_utf8_string.o src/pk/asn1/der/utf8/der_length_utf8_string.o \
src/pk/asn1/oid/pk_get_oid.o src/pk/asn1/oid/pk_oid_cmp.o src/pk/asn1/oid/pk_oid_str.o \
src/pk/dsa/dsa_decrypt_key.o src/pk/dsa/dsa_encrypt_key.o src/pk/dsa/dsa_export.o src/pk/dsa/dsa_free.o \
src/pk/dsa/dsa_generate_key.o src/pk/dsa/dsa_generate_pqg.o src/pk/dsa/dsa_import.o \
src/pk/dsa/dsa_make_key.o src/pk/dsa/dsa_set.o src/pk/dsa/dsa_set_pqg_dsaparam.o \
src/pk/dsa/dsa_shared_secret.o src/pk/dsa/dsa_sign_hash.o src/pk/dsa/dsa_verify_hash.o \
src/pk/dsa/dsa_verify_key.o \
src/prngs/rng_get_bytes.o src/prngs/rng_make_prng.o src/prngs/yarrow.o

# List of test objects to compile (all goes to libtomcrypt_prof.a)
TOBJECTS=tests/base16_test.o tests/base32_test.o tests/base64_test.o tests/bcrypt_test.o \
tests/cipher_hash_test.o tests/common.o tests/der_test.o tests/dh_test.o tests/dsa_test.o \
tests/ecc_test.o tests/ed25519_test.o tests/file_test.o tests/mac_test.o tests/misc_test.o \
tests/modes_test.o tests/mpi_test.o tests/multi_test.o tests/no_prng.o tests/padding_test.o \
tests/pkcs_1_eme_test.o tests/pkcs_1_emsa_test.o tests/pkcs_1_oaep_test.o tests/pkcs_1_pss_test.o \
tests/pkcs_1_test.o tests/prng_test.o tests/rotate_test.o tests/rsa_test.o tests/ssh_test.o \
tests/store_test.o tests/test.o tests/x25519_test.o

# The following headers will be installed by "make install"
HEADERS_PUB=src/headers/tomcrypt.h src/headers/tomcrypt_argchk.h src/headers/tomcrypt_cfg.h \
src/headers/tomcrypt_cipher.h src/headers/tomcrypt_custom.h src/headers/tomcrypt_hash.h \
src/headers/tomcrypt_mac.h src/headers/tomcrypt_macros.h src/headers/tomcrypt_math.h \
src/headers/tomcrypt_misc.h src/headers/tomcrypt_pk.h src/headers/tomcrypt_pkcs.h \
src/headers/tomcrypt_prng.h

HEADERS=$(HEADERS_PUB) src/headers/tomcrypt_private.h

#These are the rules to make certain object files.
src/ciphers/aes/aes.o: src/ciphers/aes/aes.c src/ciphers/aes/aes_tab.c
src/ciphers/twofish/twofish.o: src/ciphers/twofish/twofish.c src/ciphers/twofish/twofish_tab.c
src/hashes/sha2/sha224.o: src/hashes/sha2/sha224.c src/hashes/sha2/sha256.c
src/hashes/sha2/sha384.o: src/hashes/sha2/sha384.c src/hashes/sha2/sha512.c
src/hashes/sha2/sha512_224.o: src/hashes/sha2/sha512.c src/hashes/sha2/sha512_224.c
src/hashes/sha2/sha512_256.o: src/hashes/sha2/sha512.c src/hashes/sha2/sha512_256.c
src/hashes/whirl/whirl.o: src/hashes/whirl/whirl.c src/hashes/whirl/whirltab.c

$(DOBJECTS): LTC_CFLAGS := -Itests $(LTC_CFLAGS)
$(TOBJECTS): LTC_CFLAGS := -Itests $(LTC_CFLAGS)

#Dependencies on *.h
$(OBJECTS): $(HEADERS)
$(DOBJECTS): $(HEADERS) $(THEADERS)
$(TOBJECTS): $(HEADERS) $(THEADERS)

all: $(call print-help,all,Builds the library and all demos and test utils (test $(UNBROKEN_DEMOS) $(BROKEN_DEMOS))) all_test $(BROKEN_DEMOS)

all_test: $(call print-help,all_test,Builds the library and all unbroken demos and test utils (test $(UNBROKEN_DEMOS))) test $(UNBROKEN_DEMOS)

bins: $(call print-help,bins,Builds the library and all useful demos) $(USEFUL_DEMOS)

check: test
	./test

#build the doxy files (requires Doxygen, tetex and patience)
doxygen: $(call print-help,doxygen,Builds the doxygen html documentation)
	$(MAKE) -C doc/ $@ V=$(V)
doxy: $(call print-help,doxy,Builds the complete doxygen documentation including refman.pdf (takes long to generate))
	$(MAKE) -C doc/ $@ V=$(V)
docs: $(call print-help,docs,Builds the Developer Manual)
	$(MAKE) -C doc/ $@ V=$(V)

doc/crypt.pdf: $(call print-help,doc/crypt.pdf,Builds the Developer Manual)
	$(MAKE) -C doc/ crypt.pdf V=$(V)


install_all: $(call print-help,install_all,Install everything - library bins docs tests) install install_bins install_docs

INSTALL_OPTS ?= -m 644

.common_install: $(LIBNAME)
	install -p -d $(DESTDIR)$(INCPATH)
	install -p -d $(DESTDIR)$(LIBPATH)
	$(INSTALL_CMD) -p $(INSTALL_OPTS) $(LIBNAME) $(DESTDIR)$(LIBPATH)/$(LIBNAME)
	install -p -m 644 $(HEADERS_PUB) $(DESTDIR)$(INCPATH)

$(DESTDIR)$(BINPATH):
	install -p -d $(DESTDIR)$(BINPATH)

.common_install_bins: $(USEFUL_DEMOS) $(DESTDIR)$(BINPATH)
	$(INSTALL_CMD) -p -m 775 $(USEFUL_DEMOS) $(DESTDIR)$(BINPATH)

install_docs: $(call print-help,install_docs,Installs the Developer Manual) doc/crypt.pdf
	install -p -d $(DESTDIR)$(DATAPATH)
	install -p -m 644 doc/crypt.pdf $(DESTDIR)$(DATAPATH)

install_test: $(call print-help,install_test,Installs the self-test binary) test $(DESTDIR)$(BINPATH)
	$(INSTALL_CMD) -p -m 775 $< $(DESTDIR)$(BINPATH)

install_hooks: $(call print-help,install_hooks,Installs the git hooks)
	for s in `ls hooks/`; do ln -s ../../hooks/$$s .git/hooks/$$s; done

HEADER_FILES=$(notdir $(HEADERS_PUB))
.common_uninstall:
	$(UNINSTALL_CMD) $(DESTDIR)$(LIBPATH)/$(LIBNAME)
	rm $(HEADER_FILES:%=$(DESTDIR)$(INCPATH)/%)

#This rule cleans the source tree of all compiled code, not including the pdf
#documentation.
clean: $(call print-help,clean,Clean everything besides the pdf documentation)
	find . -type f    -name "*.o"   \
               -o -name "*.lo"  \
               -o -name "*.a"   \
               -o -name "*.la"  \
               -o -name "*.obj" \
               -o -name "*.lib" \
               -o -name "*.exe" \
               -o -name "*.dll" \
               -o -name "*.so"  \
               -o -name "*.gcov"\
               -o -name "*.gcda"\
               -o -name "*.gcno"\
               -o -name "*.il"  \
               -o -name "*.dyn" \
               -o -name "*.dpi"  | xargs rm -f
	rm -f $(TIMING) $(TEST) $(DEMOS)
	rm -f *_tv.txt
	rm -f *.pc
	rm -rf `find . -type d -name "*.libs" | xargs`
	$(MAKE) -C doc/ clean

zipup: $(call print-help,zipup,Prepare the archives for a release) doc/crypt.pdf
	@# Update the index, so diff-index won't fail in case the pdf has been created.
	@#   As the pdf creation modifies crypt.tex, git sometimes detects the
	@#   modified file, but misses that it's put back to its original version.
	@git update-index --refresh
	@git diff-index --quiet HEAD -- || ( echo "FAILURE: uncommited changes or not a git" && exit 1 )
	@perl helper.pl --check-all || ( echo "FAILURE: helper.pl --check-all errors" && exit 1 )
	rm -rf libtomcrypt-$(VERSION) crypt-$(VERSION).*
	@# files/dirs excluded from "git archive" are defined in .gitattributes
	git archive --format=tar --prefix=libtomcrypt-$(VERSION)/ HEAD | tar x
	@echo 'fixme check'
	-@(find libtomcrypt-$(VERSION)/ -type f | xargs grep 'FIXM[E]') && echo '############## BEWARE: the "fixme" marker was found !!! ##############' || true
	mkdir -p libtomcrypt-$(VERSION)/doc
	cp doc/crypt.pdf libtomcrypt-$(VERSION)/doc/crypt.pdf
	tar -c libtomcrypt-$(VERSION)/ | xz -6e -c - > crypt-$(VERSION).tar.xz
	zip -9rq crypt-$(VERSION).zip libtomcrypt-$(VERSION)
	rm -rf libtomcrypt-$(VERSION)
	gpg -b -a crypt-$(VERSION).tar.xz
	gpg -b -a crypt-$(VERSION).zip

codecheck: $(call print-help,codecheck,Check the code of the library)
	perl helper.pl -a
	perlcritic *.pl

help: $(call print-help,help,That's what you're currently looking at)

# ref:         $Format:%D$
# git commit:  $Format:%H$
# commit time: $Format:%ai$
