to build via scons, call:

    (source rmc/conan_source all; scons -j 12 --from-env)

this will use "conan install" to setup an environment for scons such that it can find the needed dependencies.

to install to a sandbox (a.k.a DESTDIR):

    (source rmc/conan_source all; scons -j 12 --from-env --install-sandbox=./install)

this will install to build/SUBDIR/install.

to create conan packages you can instead just call:

    ver=2.2.1
    pyver=3
    rmc/conan_create_all.sh $ver $pyver

to build with `build_type=Debug`:

    rmc/conan_create_all.sh $ver $pyver common/snapshot -s build_type=Debug

or do it in individual steps:

    conan create conanfile.libln.py                       liblinks_and_nodes/$ver@common/snapshot
    conan create conanfile.ln_runtime.py                  links_and_nodes_runtime/$ver@common/snapshot
    conan create conanfile.msgdef.py                      links_and_nodes_ln_msgdef/$ver@common/snapshot
    conan create conanfile.links_and_nodes_base_python.py links_and_nodes_base_python/$ver@common/snapshot -o python_version=$pyver
    conan create conanfile.links_and_nodes_python.py      links_and_nodes_python/$ver@common/snapshot -o python_version=$pyver
    conan create conanfile.links_and_nodes_manager.py     links_and_nodes_manager/$ver@common/snapshot -o python_version=$pyver
    conan create conanfile.lnrdb.py                       lnrdb_python/$ver@common/snapshot -o python_version=$pyver

to execute the automatic tests for this new ln_manager-package you can call:

    conan test tests links_and_nodes_manager/$ver@common/snapshot -o links_and_nodes_manager:python_version=$pyver

you can then use the ln_manager like this:

    conan install links_and_nodes_manager/$ver@common/snapshot -if conan_lnm -o python_version=$pyver -g virtualenv -g virtualenv_python
    bash
    source conan_lnm/activate.sh; source conan_lnm/activate_run_python.sh
    ln_manager -c ...

ln_generate
===========
if you want to use ln_generate to generate source files for your project from message definitions, you can

    conan install links_and_nodes_base_python/$ver@common/snapshot -g virtualenv
    source activate.sh
    ln_generate -o my_ln_messages.h ln/request_topic --md-dir my-local-project-msg-defs/ myproject/msgdef1

if you already have some python installed you can instead also use links_and_nodes_base_python_buildtool
which does not depend on a python-dist:

    conan install links_and_nodes_base_python_buildtool/$ver@common/snapshot -g virtualenv
    source activate.sh
    ln_generate -o my_ln_messages.h ln/request_topic --md-dir my-local-project-msg-defs/ myproject/msgdef1

when developing
===============

while developing it is unnecessary to install/copy files to some target prefix for every code change/test. for this
use-case you can just build and then start the ln_manager from within the source-tree:

    (source rmc/conan_source all; scons --from-env --python=python3 --use-private-libtomcrypt --prefix=$(pwd)/install --rmc-doc)
    source rmc/conan_source ln_manager; export PYTHONPATH=$(pwd)/python:$PYTHONPATH
    ./python/links_and_nodes_manager/ln_manager -c ...

for python2:

    export PYVER=2
    (source rmc/conan_source all2; scons --from-env --python=python2 --build=build_py2 --use-private-libtomcrypt --prefix=$(pwd)/install2 --rmc-doc)
    source rmc/conan_source ln_manager2; export PYTHONPATH=$(pwd)/python:$PYTHONPATH
    python2 ./python/links_and_nodes_manager/ln_manager -c ...

this ln_manager will realize that it is called from source tree and will try to adjust search pathes accordingly.
(manager will try to start ln_daemon also from source tree)