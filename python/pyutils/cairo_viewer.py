import os
import traceback
import numpy as np

# By default, Gdk loads the OpenGL 3.2 Core profile. However, PyCAM's rendering
# code uses the fixed function pipeline, which was removed in the Core profile.
# So we have to resort to this semi-public API to ask Gdk to use a Compatibility
# profile instead.
os.environ['GDK_GL'] = 'legacy'
# stolen from https://github.com/SebKuzminsky/pycam/pull/140/files
import gi # noqa: E402
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf # noqa: E402

import cairo # noqa: E402

__all__ = [ "CairoViewer", "get_viewer" ]

_viewers = {} # name -> CairoViewer

# https://lazka.github.io/pgi-docs/GdkPixbuf-2.0/classes/Pixbuf.html#GdkPixbuf.Pixbuf.new_from_bytes

def get_viewer(name):
    global _viewers
    viewer = _viewers.get(name)
    if viewer is None:
        viewer = _viewers[name] = CairoViewer(name=name)
    return viewer

class CairoViewer(object):
    def __init__(self, name=None):
        self.name = name
        win = self.mw = Gtk.Window()
        if self.name:
            win.set_title(self.name)

        self.bg_image = None
        da = self.da = Gtk.DrawingArea()
        win.add(da)
        da.connect('draw', self.on_draw)
        da.connect("configure-event", self.on_reconfigure)
        
        da.connect("button-press-event", self.on_button)
        da.connect("button-release-event", self.on_button_release)
        da.connect("scroll-event", self.on_scroll)
        da.connect("motion-notify-event", self.on_motion)
        
        da.add_events(Gdk.EventMask.BUTTON_PRESS_MASK | Gdk.EventMask.BUTTON_RELEASE_MASK | Gdk.EventMask.POINTER_MOTION_MASK | Gdk.EventMask.SCROLL_MASK)
        win.show_all()
        
        self.drawer = None
        self._events = "button,button_release,scroll,motion".split(",")
        for ev in self._events:
            setattr(self, "_on_" + ev, None)

    def _forward_ev(self, event, ev):
        method = getattr(self, "_on_" + event, None)
        if method is None:
            return False
        return method(ev)
    def on_button(self, da, ev):
        return self._forward_ev("button", ev)
    def on_button_release(self, da, ev):
        return self._forward_ev("button_release", ev)
    def on_scroll(self, da, ev):
        return self._forward_ev("scroll", ev)
    def on_motion(self, da, ev):
        return self._forward_ev("motion", ev)
    def _check_set_method(self, drawer, what):
        method = getattr(drawer, what, None)
        if method:
            setattr(self, "_" + what, method)

    def close(self):
        if self.da:
            self.da.destroy()
            self.da = None
        if self.mw:
            self.mw.destroy()
            self.mw = None
        
    def __del__(self):
        self.close()
        
    def on_reconfigure(self, da, ev):
        self.da.queue_draw()

    def on_draw(self, da, cr):
        cr.set_antialias(cairo.ANTIALIAS_SUBPIXEL)
        cr.set_line_width(1)       

        r = da.get_allocation()
        self.win_size = r.width, r.height

        if self.bg_image is None:
            cr.set_source_rgb(1, 1, 1)
        else:
            Gdk.cairo_set_source_pixbuf(cr, self.bg_image, 0, 0)
        cr.rectangle(0, 0, r.width, r.height)
        cr.fill()

        if self.drawer:
            try:
                self.drawer.draw(self, cr)
            except Exception:
                traceback.print_exc()
        
    def redraw(self, drawer=None):
        if drawer:
            self.drawer = drawer
            for ev in self._events:
                self._check_set_method(self.drawer, "on_" + ev)            
        self.da.queue_draw()

    def get_pil_from_ndarray(self, arr):
        from PIL import Image
        return Image.fromarray(arr)

    def _on_destroy_bg_image(self, *args):
        print("destroy", args)
    def set_bg_image(self, img):
        if img is None:
            self.bg_image = None
            return
        assert len(img.shape) == 3
        assert img.dtype == np.uint8
        self.bg_image = GdkPixbuf.Pixbuf.new_from_data(
            data=img.tobytes(),
            colorspace=GdkPixbuf.Colorspace.RGB,
            has_alpha=False,
            bits_per_sample=8,
            width=img.shape[1],
            height=img.shape[0],
            rowstride=img.strides[0],
            destroy_fn=lambda *args: self._on_destroy_bg_image(img, *args) # this will keep a ref to the img
        )
        self.redraw()
