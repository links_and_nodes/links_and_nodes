#!/usr/bin/python
"""
    Copyright 2014 DLR e.V., Florian Schmidt
    
    This file is part of floatlog_gui.
    
    floatlog_gui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    floatlog_gui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import print_function

import os
import sys
import time
import pprint
import socket
import datetime
import traceback
import subprocess

import scipy.io

import gobject # todo: migrate to Gtk3
import gtk
import gtk.glade

import pyutils
import pyutils.floatlog_downloader

class config_class(object):
    pass

config = config_class()
config.glade_fn = os.path.join(os.path.dirname(__file__), "floatlog_gui.glade")
config.connections_fn = os.path.expanduser("~/.floatlog_gui_connections")
config.download_dir = "/tmp"
config.verbose = False

class floatlog_gui(object):
    def __init__(self):
        self.last_all_downloaded_fn = None
        self._downloading = False
        self.logger = None
        self.connected = False
        self.last_downloads = {}

        self._load_connections()
        self._init_gui()

    def run(self, args):
        skip = 0
        connect_to = None
        for i, arg in enumerate(args):
            if skip:
                skip -= 1
                continue
            connect_to = arg
        if connect_to and connect_to in self.saved_connections:
            host, port = self.saved_connections[connect_to]
            self._connect(host, port)
        gtk.main()

    def __getattr__(self, name):
        if not hasattr(self, "xml") or self.xml is None:
            raise AttributeError(name)
        widget = self.xml.get_widget(name)
        if not widget:
            raise AttributeError(name)
        return widget

    def _init_gui(self):
        self.xml = gtk.glade.XML(config.glade_fn)
        self.xml.signal_autoconnect(self)

        self.get_all_into_clipboard_btn.set_property("visible", False)

        # saved connections combobox
        cb = self.saved_combobox
        m = gtk.ListStore(gobject.TYPE_STRING)
        cb.set_model(m)
        cr = gtk.CellRendererText()
        cb.pack_start(cr)
        cb.set_attributes(cr, text=0)

        # blocks in model treeview
        tv = self.blocks_tv
        m = gtk.ListStore(
            gobject.TYPE_STRING,
            gobject.TYPE_BOOLEAN,
            gobject.TYPE_STRING,
            gobject.TYPE_STRING,
            )
        self.blocks_model = m
        tv.set_model(m)
        cr = gtk.CellRendererText()
        cr_check = gtk.CellRendererToggle()
        cr_check.connect("toggled", self.on_enable_toggled)
        tv.insert_column_with_attributes(-1, "block name", cr, text=0)
        tv.insert_column_with_attributes(-1, "enabled", cr_check, active=1)
        tv.insert_column_with_attributes(-1, "sample size", cr, text=2)
        tv.insert_column_with_attributes(-1, "signal names", cr, text=3)
        

    def _process(self, *args):
        try:
            self.logger.process(*args)
        except Exception:
            print("exception processing:\n%s" % traceback.format_exc())
            self._disconnect()
            return False
        return True

    def _reconnect(self):
        self._connect(*self._last_connect)
    
    def _connect(self, host, port):
        self._last_connect = host, port
        self.connected = True
        self.connect_btn.set_label("disconnect")
        print("connect to", host, port)
        try:
            self.logger = pyutils.floatlog_downloader.floatlog_downloader(host, port, verbose=config.verbose)
        except Exception:
            print(traceback.format_exc())
            self._disconnect()
        self.logger.source = gobject.io_add_watch(self.logger.s, gobject.IO_IN, self._process)
        self.logger.last_lines = []
        self.logger.s.send("blocks\n")
        line = None
        while not line:
            gtk.main_iteration(True)
            for l in self.logger.last_lines:
                if l.startswith("OK blocks"):
                    line = l
                    break
                print(l)
            self.logger.last_lines = []
        blocks = line.split(": ", 1)[1]
        self.logger.blocks = blocks.split(", ")
        print(repr(self.logger.blocks))
        m = self.blocks_model
        m.clear()
        for b in self.logger.blocks:
            self.logger.status = None
            self.logger.choose(b)
            while self.logger.status is None:
                gtk.main_iteration(True)
            #pprint.pprint(self.logger.status)
            s = self.logger.status
            m.append((
                    b, 
                    bool(int(s["enabled"])), 
                    s["sample_size"],
                    ", ".join(["%s (%s)" % p for p in zip(s["signal_names"], s["vector"].split(", "))])
                    ))
        self.connection_frame.set_sensitive(True)

    def on_blocks_tv_cursor_changed(self, tv):
        path, col = tv.get_cursor()
        m = self.blocks_model
        row = m[path]
        block = row[0]
        self._update_block(block, set_size=True)

    def _get_status(self, block):
        self._current_block = block
        self.logger.status = None
        if self.logger._selected_block != block:
            self.logger.choose(block)
        else:
            self.logger.get_status()
        while self.logger.status is None:
            gtk.main_iteration(True)
        return self.logger.status

    def _update_block(self, block, set_size=False):
        if self.logger is None:
            del self.update_source
            return False
        if block is None and self._downloading:
            del self.update_source
            return False
        if block is None:
            block = self._current_block
        self._current_block = block
        self.selected_block_frame.set_sensitive(True)
        self._get_status(block)
        s = config_class()
        s.__dict__.update(self.logger.status)
        self.name_label.set_text(s.name)
        self.use_doubles_label.set_text(str(bool(int(s.doubles))))
        self.divisor_entry.set_text(s.divisor)
        enabled = bool(int(s.enabled))
        self.ringbuffer_size_entry.set_sensitive(not enabled)
        if set_size and s.ringbuffer_size == "0" and hasattr(s, "initial_buffer_size"):
            self.ringbuffer_size_entry.set_text(str(s.initial_buffer_size))
        else:
            self.ringbuffer_size_entry.set_text(s.ringbuffer_size)

        if int(s.ringbuffer_size):
            p = float(s.fill) / int(s.ringbuffer_size) * 100
        else:
            p = 0
        t = "%d, %.1f%%" % (int(s.fill), p)
        self.fill_progressbar.set_fraction(p / 100.)
        self.fill_progressbar.set_text(t)

        self.react_on_enable = False
        self.enable_togglebutton.set_active(enabled)
        self.react_on_enable = True

        m = self.blocks_model
        i = m.get_iter_first()
        while i:
            b = m[i][0]
            if b == block:
                # update row!
                m[i][1] = enabled
                break
            i = m.iter_next(i)

        if block in self.last_downloads:
            self.last_downloaded_frame.set_property("visible", True)
            fn = self.last_downloads[block]
            bn = os.path.basename(fn)
            self.last_downloaded_btn.set_label(bn)
            self.last_downloaded_btn.set_uri(fn)
        else:
            self.last_downloaded_frame.set_property("visible", False)
            self.get_into_clipboard_btn.set_property("visible", False)

        if not hasattr(self, "update_source") and enabled:
            self.update_source = gobject.timeout_add(500, self._update_block, None)
        if not enabled and hasattr(self, "update_source"):
            del self.update_source
        return enabled

    def on_enable_togglebutton_toggled(self, btn):
        if not self.react_on_enable:
            return
        enabled = int(self.logger.status["enabled"])
        if enabled:
            print("disable!")
            self.logger.disable()
            self._update_block(self._current_block)
        else:
            s = int(self.ringbuffer_size_entry.get_text())
            self._enable_block(self._current_block, s)

    def on_enable_toggled(self, cr, row):
        path = int(row), 
        enabled = cr.get_active()
        block = self.blocks_model[path][0]
        self._update_block(block)
        if enabled:
            # disable
            self.logger.disable()
            self._update_block(block)
        else:
            s = int(self.ringbuffer_size_entry.get_text())
            self._enable_block(self._current_block, s)
    def on_divisor_entry_activate(self, entry):
        s = int(entry.get_text())
        self.logger.set_divisor(s)
        self._update_block(self._current_block)

    def on_ringbuffer_size_entry_activate(self, entry):
        s = int(entry.get_text())
        self._enable_block(self._current_block, s)

    def _enable_block(self, block, si=None):
        if block != self._current_block:
            self.logger.choose(block)
        if si == 0:
            raise Exception("won't enable block with 0 ringbuffer size!")

        s = int(self.divisor_entry.get_text())
        self.logger.set_divisor(s)

        self.logger.enable(si)
        self._update_block(self._current_block)

    def on_reset_btn_clicked(self, btn):
        print("resetting...")
        self.logger.reset()
        self._update_block(self._current_block, set_size=True)

    def on_download_btn_clicked(self, btn):
        self._download()

    def _download(self, save_to_file=True):
        self._downloading = True
        self.logger.last_download = None
        self.logger.last_lines = []
        self.logger.data_left = None
        self.download_label.set_text("download in progress...")
        self.logger.start_download()
        self.keep_downloading = True
        reconnect = True
        self.download_dialog.show()
        def set_pb(f, force_update=False):
            self.download_progressbar.set_fraction(f)
            self.download_progressbar.set_text("%.0f %%" % (f * 100))
            if force_update:
                gtk.main_iteration(False)
                gtk.main_iteration(False)
        set_pb(0)
        def start_unpack(f):
            set_pb(f)
            self.download_label.set_text("unpacking data...")
            gtk.main_iteration(False)
            gtk.main_iteration(False)
        self.logger.unpack_hook = start_unpack
        last = time.time()
        while self.logger.last_download is None and self.keep_downloading:
            gtk.main_iteration(True)
            for l in self.logger.last_lines:
                if "ERROR" in l:
                    self.download_label.set_text("download error:\n%s" % l)
                    self.keep_downloading = False
                    self.download_dialog.run()
                    reconnect = False
            self.logger.last_lines = []
            if self.logger.data_left is not None:
                if time.time() - last >= 0.25:
                    last = time.time()
                    f = 1 - (self.logger.data_left / float(self.logger.data_len))
                    set_pb(f)
        if not self.keep_downloading:
            print("download aborted!")
            if reconnect:
                print("reconnecting...")
                self._disconnect()
                self._reconnect()
            fn = None
        else:
            set_pb(1, True)
            if save_to_file:
                fn = self._get_download_fn(self._current_block)
                bn = os.path.basename(fn)

                self.download_label.set_text("saving data to disc...")
                gtk.main_iteration(False)
                gtk.main_iteration(False)
                scipy.io.savemat(fn, self.logger.last_download_dict)

                self.last_downloaded_btn.set_property("visible", True)
                self.last_downloaded_btn.set_label(bn)
                self.last_downloaded_btn.set_uri(fn)
                self.last_downloads[self._current_block] = fn
        self.download_dialog.hide()
        self._downloading = False
        self._update_block(None)
        if save_to_file:
            return fn
        return self.logger.last_download_dict

    def _get_download_fn(self, cookie):
        bn = "floatlog_%s_%s.mat" % (
            cookie,
            datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
        fn = os.path.join(config.download_dir, bn)
        return fn

    def need_matlab(self):
        if not hasattr(self, "matlab"):
            self.matlab = None
        if self.matlab:
            if self.matlab.poll() is not None:
                print("matlab terminated")
                self.matlab.wait()
                self.matlab = None
        if self.matlab is None:
            receiver_dir = os.path.dirname(__file__)
            cmd = "matlab_acad -nosplash -r 'addpath %s; floatlog_receive_commands()'" % (receiver_dir)
            print(cmd)
            self.matlab = subprocess.Popen(cmd, shell=True, stderr=subprocess.PIPE)
            self.matlab_port = None
            while True:
                line = self.matlab.stderr.readline()
                if not line:
                    print("eof from matlab!")
                    self.matlab.wait()
                    print("matlab finished!")
                    self.matlab = None
                    break
                if line.startswith("port: "):
                    self.matlab_port = int(line.split(": ", 1)[1])
                    break
                print("got matlab line: %r" % line)
            if self.matlab_port is None:
                raise Exception("could not get matlab commands connection!")
        
    def matlab_execute(self, cmd):
        self.need_matlab()
        if not hasattr(self, "matlab_socket"):
            self.matlab_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.matlab_socket.sendto("string:%s\n" % cmd, 0, ('', self.matlab_port))

    def on_last_downloaded_btn_clicked(self, btn):
        fn = btn.get_uri()
        #os.system('matlab_acad -nosplash -r "load %s; whos" ' % fn)
        self.matlab_execute("load %s; whos" % fn)

    def on_get_into_clipboard_btn_clicked(self, btn):
        fn = self.last_downloads[self._current_block]
        self._put_to_clipboard(fn)

    def _put_to_clipboard(self, data):
        cb = gtk.Clipboard(selection="CLIPBOARD")
        cb.set_text(data)
        cb = gtk.Clipboard(selection="PRIMARY")
        cb.set_text(data)
        print("stored to clipboard: %s" % data)

    def on_download_cancel_btn_clicked(self, btn):
        self.keep_downloading = False

    def _disconnect(self):
        self.connected = False
        if self.logger:
            gobject.source_remove(self.logger.source)
            self.logger.disconnect()
        self.logger = None
        self.connect_btn.set_label("connect")
        self.connection_frame.set_sensitive(False)
        self.selected_block_frame.set_sensitive(False)

    def _load_connections(self):
        with open(config.connections_fn, "rb") as fp:
            conns = fp.read()
        try:
            self.saved_connections = eval(conns)
        except Exception:
            print(traceback.format_exc())
            self.saved_connections = {}
            return

    def _add_connection(self, name, host, port):
        self.saved_connections[name] = host, port
        data = pprint.pformat(self.saved_connections)
        with open(config.connections_fn, "wb") as fp:
            fp.write(data)
        
    def on_connect_btn_clicked(self, btn):
        if not self.connected:
            # connect!
            cb = self.saved_combobox
            m = cb.get_model()
            m.clear()
            connections = list(self.saved_connections.keys())
            connections.sort()
            for c in connections:
                m.append((c, ))
            self.host_entry.set_text("")
            self.port_entry.set_text("54385")
            self.save_as_entry.set_text("")
            self.connect_dialog.show()
            response = self.connect_dialog.run()
            self.connect_dialog.hide()
            if not response:
                return
            host = self.host_entry.get_text()
            port = int(self.port_entry.get_text())
            save_as = self.save_as_entry.get_text().strip()
            if save_as:
                self._add_connection(save_as, host, port)
            self._connect(host, port)
            return
        # disconnect!
        self._disconnect()

    def on_saved_combobox_changed(self, cb):
        at = cb.get_active_text()
        if at is None:
            return
        host, port = self.saved_connections[at]
        self.host_entry.set_text(host)
        self.port_entry.set_text(str(port))
        self.save_as_entry.set_text(at)

    def on_main_window_delete_event(self, windows, ev):
        gtk.main_quit()

    def on_enable_all_btn_clicked(self, btn):
        a = time.time()
        for block in self.logger.blocks:
            s = self._get_status(block)
            enabled = bool(int(s["enabled"]))
            if enabled:
                continue
            ringbuffer_size = int(s["ringbuffer_size"])
            if ringbuffer_size <= 0:
                print("block %s has 0 ringbuffer_size" % block)
                continue
            self._enable_block(block, ringbuffer_size)
        b = time.time() - a
        print("enabled all blocks after %.3fs" % b)
    def on_disable_all_btn_clicked(self, btn):
        a = time.time()
        for block in self.logger.blocks:
            s = self._get_status(block)
            enabled = bool(int(s["enabled"]))
            if not enabled:
                continue
            self.logger.disable()
        b = time.time() - a
        print("disabled all blocks after %.3fs" % b)
        for block in self.logger.blocks:
            self._update_block(block)
    def on_reset_all_btn_clicked(self, btn):
        a = time.time()
        for block in self.logger.blocks:
            s = self._get_status(block)
            enabled = bool(int(s["enabled"]))
            if not enabled:
                continue
            self.logger.reset()
        b = time.time() - a
        print("resetted all blocks after %.3fs" % b)

    def on_download_all_btn_clicked(self, btn):
        a = time.time()
        data = {}
        keys_seen = set()
        duplicated_keys = set()
        for block in self.logger.blocks:
            s = self._get_status(block)
            if int(s["fill"]) <= 0:
                continue
            block_data = self._download(save_to_file=False)
            
            duplicated = keys_seen.intersection(list(block_data.keys()))
            if duplicated:
                duplicated_keys.update(duplicated)
            keys_seen.update(list(block_data.keys()))
            data[block] = block_data
        b = time.time() - a
        print("downloaded all blocks after %.3fs" % b)

        all_data = {}
        for block, block_data in data.items():
            for signal_name, signal_data in block_data.items():
                if signal_name in duplicated_keys:
                    signal_name = "%s_%s" % (block, signal_name)
                all_data[signal_name] = signal_data
        
        fn = self._get_download_fn("all_blocks")
        scipy.io.savemat(fn, all_data)
        print("downloaded to file %s (%.2fMB)" % (fn, os.path.getsize(fn)/1024./1024.))
        self.last_all_downloaded_fn = fn
        self.get_all_into_clipboard_btn.set_property("visible", True)

    def on_get_all_into_clipboard_btn_clicked(self, btn):
        self._put_to_clipboard(self.last_all_downloaded_fn)

    def on_print_sample_btn_clicked(self, btn):
        self.logger.get_last_sample()
        d = self.logger.last_download_dict
        l = 0
        for key in list(d.keys()):
            if len(key) > l:
                l = len(key)
        print()
        for key in self.logger.status["signal_names"]:
            data = d[key]
            print("%-*.*s = [%s]" % (l, l, key, ", ".join(["%11.7f" % f for f in data[0]])))


if __name__ == "__main__":
    gui = floatlog_gui()
    gui.run(sys.argv[1:])

