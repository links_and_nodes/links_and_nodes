%    Copyright 2014 DLR e.V., Florian Schmidt
%    
%    This file is part of floatlog_gui.
%    
%    floatlog_gui is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%    
%    floatlog_gui is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%    
%    You should have received a copy of the GNU General Public License
%    along with this program.  If not, see <http://www.gnu.org/licenses/>.

function floatlog_receive_commands()
    fd = mxudp_receiver('create');
  
    n = 'floatlog_receive_commands_timer';
    t = timerfind('Name', n);
    if ~isempty(t)
        for tt = t
            stop(tt);
            fd = get(tt, 'UserData');
            mxudp_receiver('close', fd);
            delete(tt);
        end
    end
    
    t = timer(...
        'Name', n, ...
        'TimerFcn', @floatlog_receive_commands_callback, ...
        'Period', 0.1, ...
        'ExecutionMode', 'fixedRate', ...
        'BusyMode', 'drop',...
        'UserData', fd);
    start(t);
end

function floatlog_receive_commands_callback(t, event)
    fd = get(t, 'UserData');
    has_data = mxudp_receiver('has_data', fd);
    if ~has_data
        return
    end
    [ip, port, packet] = mxudp_receiver('recv', fd);
    evalin('base', packet);
end
