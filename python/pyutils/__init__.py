"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt
"""

from __future__ import print_function

import sys
import traceback
import time

debug = False

from . import statistic # noqa: E402,F401

try:
    from . import topxml # noqa: E402,F401
except ImportError:
    topxml = None

from pyutils.line_assembler import line_assembler # noqa: E402,F401
from pyutils.load_module_from_string import get_code_object_from_string, exec_string, load_module_from_string # noqa: E402,F401
from pyutils.hooked_object import hooked_object # noqa: E402,F401

if not hasattr(traceback, "format_exc"):
    def format_exc(limit=None):
        return "".join(traceback.format_exception(sys.exc_info()[0], sys.exc_info()[1], sys.exc_info()[2]))
    traceback.format_exc = format_exc
    print("missing format_exc()")



class interpret_sequence(object):
    def __init__(self, *interpretation):
        pos = 0
        self.interpretation = {}
        for name, length in interpretation:
            self.interpretation[name] = (pos, pos + length)
            pos += length
        self.length = pos
    def set(self, seq):
        self.seq = seq
    def items(self):
        d = []
        for name, (start, end) in self.interpretation.items():
            if name == "skip":
                continue
            d.append((name, self.seq[start:end]))
        return d
    def __repr__(self):
        if not len(self.seq):
            return "<interpret_sequence no sequence set!>"
        d = list(self.items())
        import pprint
        return "<interpret_sequence\n%s>" % pprint.pformat(d)
    def __getattr__(self, name):
        if name not in self.interpretation:
            raise AttributeError()
        start, end = self.interpretation[name]
        return self.seq[start:end]

    def set_item(self, name, value):
        if name not in self.interpretation:
            raise AttributeError()
        start, end = self.interpretation[name]
        self.seq[start:end] = value

class every(object):
    """
    this object "returns True" in the specivied interval of seconds.
    e = every(2) # means become true every 2 seconds
    while True:
        if e: # this calls the __nonzero__ operator and becomes true once every 2 seconds
            print("do something")
    """

    def __init__(self, seconds):
        self.interval = seconds
        self.last = time.time()

    def __bool__(self):
        now = time.time()
        if self.last is None:
            self.last = now
            return True
        if now >= self.last + self.interval:
            self.last = now
            return True
        return False

def system(cmd):
    # own version of os.system without fd inheritance!
    import subprocess
    p = subprocess.Popen(cmd, shell=True, close_fds=True)
    p.communicate()
    return p.returncode

