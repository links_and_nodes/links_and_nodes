from distutils.core import setup

import os
import glob
from . import package_spec
from .package_spec import module, package, pkg_package, get_modules

from Cython.Distutils import build_ext

laser_sc_version = "2.5"
ard_version = "0.9.4"
trafo3d_version = "2.0.1"

#package_spec.compiler = "linux-intel-gcc4"
#package_spec.cflags = ["-g", "-Wall", "-I/home/schm_fl/local/include"]
package_spec.cflags = ["-g", "-Wall"]
package_spec.ldflags = ["-L/home/schm_fl/local/lib"]

arch = os.uname()[4]
if arch == "i686":
    arch = "x86"
arch = "sled11-%s-gcc4.x" % arch
package_spec.compiler = arch

platform_model = "platform_only"
package_spec.available_packages = { # avaliable packages
    #	"ardcom": package("/home/ard/packages/ard/ard-%s" % ard_version,
    #					  libs=("ardcom", "rt",),
    #					  includes="/home/ard/packages/ard/ard-%s/src" % ard_version),
    # own debugging variant
    "ardcom": package("/home/schm_fl/packages/ardcom_debug/ard-%s" % ard_version,
                      libs=("ardcom", "rt",),
                      includes="/home/schm_fl/packages/ardcom_debug/ard-%s/src" % ard_version),
    "rt": package(libs=("rt",)),
    "ScannerRPC": package("/home/laser-sc/packages/%s" % laser_sc_version,
                          libs=("rt", "Tools++", "ScannerRPC", "ScannerCan", "Tiff3D"),
                          libdirs=["/home/laser-sc/packages/%s/linux-intel-gcc4" % laser_sc_version]),
    "sensor_server_rpc": package("/home/laser-sc/packages/%s" % laser_sc_version,
                                 libs=("rt", "ScannerCan"),
                                 libdirs=["/home/laser-sc/packages/%s/linux-intel-gcc4" % laser_sc_version]),
    "ART": package("/home/laser-sc/packages/%s" % laser_sc_version,
                   libs="art"),
    "OpenCV": package("/home/schm_fl/local",
                      libs=("cxcore", "cv", "cvaux"),
                      append_arch=False),
    #"PQP": package("/home/schm_fl/packages/PQP_v1.3", append_arch=False),
    "solid": package("/home/schm_fl/packages/solid-3.5.6", append_arch=False, libs="solid"),
    "SWIFT": package("/home/schm_fl/packages/SWIFT_1.3", append_arch=False),
    #	"Xv": package(libs=("X11", "Xv", "Xext")),
    "tiff": package(libs=("tiff", )),
    "trafo3d": package(includes="/home/odsw/packages/trafo3d/%s/include" % trafo3d_version),
    "odsw": package(includes=("/home/odsw/packages/include", "/home/odsw/packages/include/common", "/home/odsw/foreign_packages/std/%s" % package_spec.compiler)),
    "matlab_rtw": package(includes=[
        "/optnfs/matlab/v18/rtw/c/ert",
        "/optnfs/matlab/v18/extern/include",
        "/optnfs/matlab/v18/simulink/include",
        "/optnfs/matlab/v18/rtw/c/src",
        "/optnfs/matlab/v18/rtw/c/src/ext_mode/common",
        "/optnfs/matlab/v18/rtw/c/libsrc",
    ],
                          cflags=["-ffloat-store", #  -fPIC -m32",
                                  "-DRT", "-DUSE_RTMODEL", "-DERT", "-DTID01EQ=1", "-DMODEL=platform_only",
                                  "-DNUMST=2", "-DNCSTATES=52", "-DUNIX",
                                  "-DMAT_FILE=0", "-DINTEGER_CODE=0", "-DONESTEPFCN=1", "-DTERMFCN=1",
                                  "-DHAVESTDIO", "-DMULTI_INSTANCE_CODE=0", "-DMT=0"
                              ]),
    
    
    "platform_only": package("./platform_only",
                             includes=["./platform_only/%s_ert_shrlib_rtw" % platform_model],
                             libs=[
                                 "/home/schm_fl/packages/pyutils/platform_only/%s.so" % platform_model,
                             ],
                              append_lib=False),
    "bernhard_ipols": package("/home/that/sd/models/SimuLibs-2.0/sfunTOP",
                              includes=["/home/that/sd/models/SimuLibs-2.0/sfunTOP"],
                              libs=["/home/that/sd/models/SimuLibs-2.0/sfunTOP/linux-intel-gcc4/libipols.a",
                                    "/home/bernhard/packages/gsl/obj/linux-intel-gcc4/lib/libgsl.a",
                                    "/home/bernhard/packages/gsl/obj/linux-intel-gcc4/lib/libgslcblas.a",
                                ],
                              append_lib=False),
    #package("/home/bernhard/packages/Aop2/obj/torso/linux-intel-gcc4",
    #                          includes=("/home/that/sd/models/SimuLibs-2.0/sfunTOP/tools/itp/jointViaCubic",
    #                                    "/home/that/sd/models/SimuLibs-2.0/sfunTOP"),
    #                          libs=["jointViaCubic"],
    #                          append_arch=False),
    "t4_torque": package("/home/schm_fl/workspace/t4_torque",
                         libs=[],
                         includes=["/home/schm_fl/workspace/t4_torque"]),
    "Coin": pkg_package("Coin"),	
    "alsa": pkg_package("alsa"),	
    "qhull": package("/home/f_soft/foreign_packages/qhull/2011.1",
                     libs=["qhullstatic"],
                     includes=["/volume/software/common/foreign_packages/qhull/2011.1/include/qhull"],
                     libdirs=["/volume/software/common/foreign_packages/qhull/2011.1/lib/" + arch],
                 ),
        "gsl": package(libs=["gsl", "gslcblas"]),
    #"slatec": package(
    #    libdirs=["./slatec/static"],
    #    libs=["slatec"]),
}

osver = os.getenv("OSVER")
pyver = os.getenv("PYVER")

if osver == "suse11" or osver is None:
    if pyver is None:
        pyver = "2.6"
    import numpy
    package_spec.available_packages.update({
        "numpy": package(includes=numpy.get_include()),
    })
else:
    package_spec.available_packages.update({
        "numpy": package(includes="/home/schm_fl/local/lib/python%s/site-packages/numpy/core/include" % pyver),
    })


modules = { # python modules to build
    "t4_torque": module(["t4_torque.pyx"],
                        packages=["t4_torque", "numpy"]),
    "lbr_grav_model": module(["lbr_grav_model.pyx"],
                             packages=["numpy"]),
    #    "mobile_platform": module(["mobile_platform.pyx"],
    #                                packages=["platform_only", "numpy", "matlab_rtw"]),
    "jointviacubicipol": module(["jointviacubicipol.pyx"],
                                packages=["bernhard_ipols", "numpy"],
                                only_archs=["sled11-x86-gcc4.x"]),
    "cartbesselipol": module(["cartbesselipol.pyx"],
                             packages=["bernhard_ipols", "numpy"],
                                only_archs=["sled11-x86-gcc4.x"]),
    "linblendipol": module(["linblendipol.pyx"],
                           packages=["bernhard_ipols", "numpy"],
                                only_archs=["sled11-x86-gcc4.x"]),
    #"sensor_server_client": module(["sensor_server_client.pyx"],
    #                               packages=["sensor_server_rpc", "numpy"]),
    #	'card': module(['card.cpp', "swig_util.cpp"],
    #				   packages=("ardcom", )),
    'posix_ipc': module(['posix_ipc.cpp'],
                        packages=("rt", )),
    #	'art': module(['art.cpp', "swig_util.cpp"],
    #				  packages=("odsw", "trafo3d", "ART", )),
    #'xv': module(['xv.cpp', "swig_util.cpp"]),
    'trafo3d': module(['trafo3d.cpp', "swig_util.cpp"],
                      packages=("odsw", "trafo3d", "numpy")),
    #'_pqp': module(["_pqp.cpp", "swig_util.cpp"],
    #			   depends=glob.glob("*pqp*cpp"),
    #			   packages=("numpy", "PQP")),
    '_coin_util': module(["_coin_util.cpp", "swig_util.cpp"],
                         packages=("Coin", "numpy")),
    '_solid': module(["_solid.cpp", "swig_util.cpp"],
                     depends=glob.glob("_solid*cpp"),
                     packages=("numpy", "solid")),
    #	'_swift': module(["_swift.cpp", "swig_util.cpp"],
    #					 depends=glob.glob("_swift*cpp") + ["../SWIFT_1.3/lib/libSWIFT.a"],
    #					 packages=("numpy", "SWIFT")),
    #	'ScannerRPC': module(["ScannerRPC.cpp", "swig_util.cpp"],
    #						 packages=("ScannerRPC", "odsw", "tiff")),
    #	'_waveform': module(["_waveform.cpp", "swig_util.cpp"],
    #						packages=("alsa", )),
    "coin_get_points": module(["_coin_get_points.pyx", "coin_get_points.cpp", "swig_util.cpp"],
                              packages=["Coin", "numpy"]),
    "_vox_util": module(["_vox_util.pyx"],
                        packages=["numpy"]),
    "qhull_interface": module(["_qhull_interface.pyx", "qhull_interface.cpp"],
                              packages=["qhull", "numpy"]),
    #"ransac_search_plane": module(["ransac_search_plane.pyx", "plane_ransac.cpp"],
    #                            packages=["numpy", "gsl"]),
    #"slatec": module(["slatec.pyx"],
    #                            packages=["numpy", "gsl", "slatec"]),

}

if __name__ == "__main__":
    setup(
        cmdclass = {'build_ext': build_ext},
        name='pyutils',
        version='1.0',
        description='dlr utils',
        ext_modules=get_modules(modules))
