"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt
"""

from __future__ import print_function

import os
import sys
import fnmatch
import traceback

import pivy.coin as coin
import numpy as np

# By default, Gdk loads the OpenGL 3.2 Core profile. However, PyCAM's rendering
# code uses the fixed function pipeline, which was removed in the Core profile.
# So we have to resort to this semi-public API to ask Gdk to use a Compatibility
# profile instead.
if "gi.repository.Gtk" in sys.modules and os.getenv("GDK_GL") != "legacy":
    print("\n\n"
          "*****************************\n"
          "WARNING: `Gtk` was already imported from `gi.repository` but `GDK_GL` env-var is not set to `legacy`!\n"
          "         you will very likely see `Invalid argument` OpenGL errors!\n"
          "         please set `GDK_GL=legacy` before importing Gtk to make classic OpenGL work!\n"
          "*****************************\n")
os.environ['GDK_GL'] = 'legacy'
# stolen from https://github.com/SebKuzminsky/pycam/pull/140/files
import gi # noqa: E402
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk # noqa: E402
# https://lazka.github.io/pgi-docs/Gtk-3.0/index.html
# https://lazka.github.io/pgi-docs/Gdk-3.0/index.html

import pyutils # noqa: E402
import pyutils.inventor # noqa: E402
import pyutils.SoPyGtkViewer # noqa: E402

__all__ = ["coin_node", "coin_viewer", "get_coin_viewer"]

coord_file = os.path.join(os.path.dirname(__file__), "coord_015.iv")


def get_iv_source(node):
    tfn = "/dev/shm/out"
    out = coin.SoOutput()
    out.openFile(tfn)
    out.setStage(coin.SoOutput.COUNT_REFS)
    node.writeInstance(out)
    out.setStage(coin.SoOutput.WRITE)
    node.writeInstance(out)
    out.closeFile()
    with open(tfn, "rb") as fp:
        return fp.read()

def coin_search_type(base, node_type):
    """
    search a node of type node_type below base
    """
    searchaction = coin.SoSearchAction()
    searchaction.reset()
    searchaction.setType(node_type)
    searchaction.apply(base)
    p = searchaction.getPath()
    if p is None:
        raise Exception("path not found!")
    p.ref()
    node = p.getTail()
    p.unref()
    return node

def is_eye(frame):
    return abs(frame - np.eye(4)).max() < 1e-6

class coin_node(pyutils.hooked_object):
    def __init__(self, name, parent, frame=None):
        if frame is None:
            frame = np.eye(4)
        pyutils.hooked_object.__init__(self)
        self.name = name
        self.parent = parent
        self.root = self.parent.root
        self._already_added = set()
        self._eye_transforms = {} # tname -> (parent, idx)
        self._transforms = {}
        self._manipulators = {}
        self._has_coord = False

        self._ignore_setattrs = True
        self.separator = coin.SoSeparator()
        self.separator.ref()
        self.separator.setName(name)
        self.draw_style = None
        self.branches = { None: self.separator } # name -> sep
        self.current_branch = None

        self.switch = coin.SoSwitch()
        self.switch.ref()
        self.switch.addChild(self.separator)
        self.root.addChild(self.switch)
        self.switch.whichChild = coin.SO_SWITCH_ALL

        self.switch.unref()
        self.separator.unref()

        self._ignore_setattrs = False
        self.add_transform("origin", frame, new_branch="root")
        
    def _get_draw_style(self):
        if self.draw_style is None:
            self.draw_style = coin.SoDrawStyle()
            self.separator.insertChild(self.draw_style, 0)
        return self.draw_style

    def hide(self):
        self.switch.whichChild = coin.SO_SWITCH_NONE
        self.switch.touch()

    def show(self, mode="filled"):
        self.switch.whichChild = coin.SO_SWITCH_ALL
        ds = self._get_draw_style()
        if mode == "lines":
            ds.style = coin.SoDrawStyle.LINES
        elif mode == "points":
            ds.style = coin.SoDrawStyle.POINTS
        else: # filled
            ds.style = coin.SoDrawStyle.FILLED
        self.switch.touch()

    def serialize(self):
        data = dict(
            name=self.name,
            childs=[]
        )
        for idx in range(self.separator.getNumChildren()):
            child = self.separator.getChild(idx)
            # is it one of our transforms?
            for tname, t in self._transforms.items():
                if child == t:
                    # yes!
                    frame = self.get_transform(tname)
                    data["childs"].append(("transform", tname, frame))
                    break
            else:
                # no
                for name, value in self.__dict__.items():
                    if not isinstance(value, coin.SoBase):
                        continue
                    if value == child:
                        attr_name = name
                        break
                else:
                    attr_name = None
                src = get_iv_source(child)
                data["childs"].append(("node", attr_name, src))
        return data

    def unserialize(self, data):
        for child in data["childs"]:
            if child[0] == "node":
                attr_name = child[1]
                src = child[2]
                node = pyutils.inventor.load_string(src)
                node = node.getChild(0)
                if attr_name is not None:
                    self.__setattr__(attr_name, node)
                else:
                    self.add(node)
            elif child[0] == "transform":
                tname = child[1]
                frame = child[2]
                if tname in self._transforms:
                    self.set_transform(tname, frame)
                else:
                    self.add_transform(tname, frame)
            else:
                print("  unknown node child %r" % (child, ))

    def __contains__(self, name):
        return name in self.__dict__

    def load(self, fn):
        return pyutils.inventor.load(fn)

    def enable_coord(self):
        if not self._has_coord:
            coord = self.parent._get_coord()
            self.branches["root"].insertChild(coord, 1) # behind transform!
            self._has_coord = True
            self.branches["root"].touch()
    def disable_coord(self):
        if not self._has_coord:
            return
        self.branches["root"].removeChild(self.parent._get_coord())
        self.branches["root"].touch()
        self._has_coord = False
    def toggle_coord(self):
        if self._has_coord:
            self.disable_coord()
        else:
            self.enable_coord()

    def on_dragger_motion(self, tname, dragger):
        frame = self.get_transform(tname)
        self.call_hook(("transform_changed", tname), frame)

    def enable_manip(self, tname, mtype="Transformer"):
        manip_name = "coin.So%sManip()" % mtype
        manip = eval(manip_name)
        transform_path = pyutils.inventor.get_path(self._transforms[tname], self.separator)
        manip.replaceNode(transform_path)
        self._manipulators[tname] = manip
        dragger = manip.getDragger()
        dragger.addMotionCallback(self.on_dragger_motion, tname)
        return manip
        
    def disable_manip(self, tname):
        manip = self._manipulators[tname]
        manip_index = self.separator.findChild(manip)
        path = pyutils.inventor.get_path(manip, self.separator)
        manip.replaceManip(path, self._transforms[tname])
        del self._manipulators[tname]
        
    def select_branch(self, branch):
        if branch not in self.branches:
            raise Exception("unknown branch name %r" % branch)
        self.current_branch = branch
        
    def add_transform(self, tname, frame=None, in_branch=None, new_branch=None):
        if frame is None:
            frame = np.eye(4)
        is_eye_transform = is_eye(frame)
        if is_eye_transform:
            t = coin.SoInfo() # place-holder if we ever want to change this transform...
            t.string.setValue("dummy-identity-transform")
        else:
            t = coin.SoTransform()
            t.setMatrix(pyutils.inventor.get_ivmatrix(frame))

        if in_branch is None:
            in_branch = self.current_branch
        #if in_branch is None:
        #    setattr(self, "%s_transform" % tname, t)
        #else:
        if new_branch is None:
            parent = self.branches[in_branch]
            parent.addChild(t)
        else:
            self.branches[new_branch] = parent = coin.SoSeparator()
            self.branches[in_branch].addChild(parent)
            parent.addChild(t)
            self.current_branch = new_branch

        self._transforms[tname] = t
        if is_eye_transform:
            self._eye_transforms[tname] = (parent, parent.getNumChildren() - 1)
        return t
    def set_transform(self, tname, frame):
        frame = np.array(frame, dtype=float)
        if tname in self._eye_transforms:
            if is_eye(frame):
                return # no change
            # replace info-nodewith real transform
            parent, idx = self._eye_transforms.pop(tname)
            t = coin.SoTransform()
            parent.replaceChild(idx, t)
            self._transforms[tname] = t
        if tname in self._manipulators:
            # manipulator is active!
            t = self._manipulators[tname]
        else:
            t = self._transforms[tname]
        t.setMatrix(pyutils.inventor.get_ivmatrix(frame))
        self.call_hook(("transform_changed", tname), frame)
        
    def get_transform(self, tname):
        m = coin.SbMatrix()
        if tname in self._eye_transforms:
            return np.eye(4)
        if tname in self._manipulators:
            # manipulator is active!
            t = self._manipulators[tname]
        else:
            t = self._transforms[tname]
        m.setTransform(t.translation.getValue(), t.rotation.getValue(), coin.SbVec3f(1, 1, 1))
        return np.array(m.getValue()).transpose()
        #return array(self._transforms[tname].matrix.getValue()).transpose()

    def add(self, node, branch=None):
        if node in self._already_added:
            return
        if branch is None:
            branch = self.current_branch
        self.branches[branch].addChild(node)

    def __setattr__(self, name, value):
        had_sep = hasattr(self, "separator")
        self.__dict__[name] = value
        if had_sep and not self._ignore_setattrs and isinstance(value, coin.SoBase):
            #print("adding new attr %r to sep!"% name, type(value), value)
            self.branches[self.current_branch].addChild(value)
            self._already_added.add(value)
        #else:
        #    print("NOT! adding new attr %r to sep!" % name, type(value), value)
        

class coin_viewer(object):
    def __init__(self, name):
        global coord_file
        self.name = name

        #if hasattr(self.app.config, "coord_file"):
        #    coord_file = self.app.config.coord_file

        self.window = Gtk.Window()
        self.window.connect("delete_event", self.on_delete)
        self.window.set_default_size(780, 580)
        self.viewer = pyutils.SoPyGtkViewer.SoPyGtkViewer()
        self.mvbox = Gtk.VBox()
        self.vbox = Gtk.VBox()
        self.mvbox.pack_start(self.vbox, False, False, 0)
        self.mvbox.pack_start(self.viewer, True, True, 0)
        self.window.add(self.mvbox)
        self.window.show_all()
        self.window.set_title("coin viewer 0x%x" % id(self))

        # popup menu
        m = self.popup_menu = Gtk.Menu()
        self.pick_active = None
        self.pick_item = Gtk.MenuItem()
        self.pick_item.set_label("pick object")
        self.pick_item.connect("activate", self.on_pick)
        m.append(self.pick_item)
        self.unpick_item = Gtk.MenuItem()
        self.unpick_item.set_label("UNpick object")
        self.unpick_item.connect("activate", self.on_unpick)
        m.append(self.unpick_item)
        #self.viewer.notify_popup = self.on_popup_menu
        self.popup_menu.show_all()

        # pick objects
        self.viewer.on_interact_button = self.on_interact_button

        self.root = coin.SoSelection()
        self.root.renderCaching = coin.SoSeparator.OFF
        self.root.ref()
        def with_manager():
            self.viewer.setSceneGraph(self.root)
            self.viewer._render_action = coin.SoBoxHighlightRenderAction()
            #self.viewer._render_action.setSmoothing(True)
            #self.viewer._render_action.setNumPasses(num_passes) # our context has no accumulation buffer...

        self.viewer.after_got_manager = with_manager
        self.childs = {}

        self.had_viewAll = False
        self.translation_enabled = True
        self.rotation_enabled = True

        self.translation_scale = 1
        self.rotation_scale = 1
        self.on_user_button = None
        self.on_user_pick = None

    def select(self, node):
        self.root.select(node)
        self.root.touch()
    def deselect(self, node):
        self.root.deselect(node)
        self.root.touch()

    def on_spacemouse(self, app, axes, buttons):
        pose = np.array(axes)
        if self.translation_enabled:
            t = 1./1000. * 0.2 * self.translation_scale / 10. / 2.
        else:
            t = 0
        if self.rotation_enabled:
            r = 1./1000. * self.rotation_scale / 5. / 10.
        else:
            r = 0
        #t = t / self.handler.fps * 25.
        #r = r / self.handler.fps * 25.
        pose[3], pose[4], pose[5] = pose[3], -pose[5], pose[4]
        T = pyutils.matrix.pose2tr(
            pose,
            translation_scale=t,
            rotation_scale=r)
        #R = T[0:3, 0:3]
        #T[0:3, 0:3] = R
        T[1, 3], T[2, 3] = -T[2, 3], T[1, 3]
        

        rot = self.viewer.camera.orientation.getValue()
        translation = np.eye(4)
        translation[0:3, 3] = rot.multVec(coin.SbVec3f(T[0:3, 3])).getValue()
        rotation = np.eye(4)
        rotation[0:3, 0:3] = T[0:3, 0:3]
        
        m = coin.SbMatrix(*list(rotation.transpose().flatten()))
        smrot = coin.SbRotation(m)
        smrot = rot.inverse() * smrot * rot
        m.setRotate(smrot)
        m = np.array(m.getValue()).transpose()
        rotation[0:3, 0:3] = m[0:3, 0:3]

        trans = self.sm_frame[0:3, 3]
        rotated = np.dot(rotation, self.sm_frame)
        rotated[0:3, 3] = trans
        self.sm_frame = np.dot(translation, rotated)        
        self.sm_node.set_transform("origin", self.sm_frame)

        return True

    def enable_space_mouse_on(self, node_name):
        if node_name not in self.childs:
            raise Exception("nodes does not exist")
        self.sm_node = self.childs[node_name]
        self.sm_frame = self.sm_node.get_transform("origin")
        self.app.add_hook("sm_input", self.on_spacemouse, from_who="coin_viewer")

    def on_interact_button(self, ev, enforce_pick=False):
        if self.viewer.currentmode == "INTERACT":
            return False # pass

        if ev.type != Gdk.EventType.BUTTON_PRESS and not enforce_pick:
            return False # pass
        if enforce_pick:
            is_pick_button = True
            is_coord_button = is_user_button = False
        else:
            is_pick_button = ev.button == 1
            is_coord_button = ev.button == 2
            is_user_button = ev.button == 3

        rpaction = coin.SoRayPickAction(self.viewer.getViewportRegion())
        ry = self.viewer.height - ev.y
        rpaction.setPoint(coin.SbVec2s(int(ev.x), int(ry)))
        rpaction.setRadius(2)
        rpaction.apply(self.viewer.sceneroot)
        picked = rpaction.getPickedPoint()
        if not picked:
            if is_pick_button and self.pick_active:
                self.pick_active.disable_manip("origin")
                self.pick_active = None
            return False # pass event
        hitpoint = picked.getPoint()
        if is_pick_button and self.pick_active:
            return False
        path = picked.getPath()
        second = path.getNode(2)
        # search matching node
        for name, node in self.childs.items():
            if node.switch == second:
                if is_pick_button:
                    if self.on_user_pick:
                        self.on_user_pick(node, path)
                        return True
                    else:
                        node.enable_manip("origin", "TransformBox")
                        self.pick_active = node
                if is_user_button:                    
                    point = np.array(picked.getPoint())
                    normal = np.array(picked.getNormal())
                    obji = np.linalg.inv(node.get_transform("origin"))
                    opoint = np.dot(obji[:3, :3], point) + obji[:3, 3]
                    onormal = np.dot(obji[:3, :3], normal)
                    if self.on_user_button is not None:
                        try:
                            self.on_user_button(
                                node, 
                                opoint,
                                onormal)
                        except Exception:
                            print("got exception calling on_user_button %r:\n%s" % (self.on_user_button, traceback.format_exc()))
                    return True
                if is_coord_button:
                    node.toggle_coord()
                return True # handled
        if is_pick_button:
            if self.on_user_pick:
                self.on_user_pick(None)
        return False

    def on_pick(self, item):
        print("pick")
    def on_unpick(self, item):
        print("UNpick")        
    def on_popup_menu(self, ev):
        self.unpick_item.set_sensitive(self.pick_active is not None)
        self.popup_menu.popup_at_pointer(None)

    def present(self):
        self.window.present()        

    def on_delete(self, window, ev):
        self.window.hide()
        del self.window
        del self.viewer
        del _coin_viewers[self.name]
        return True

    def get_node(self, node_name, frame=None):
        if frame is None:
            frame = np.eye(4)
        if node_name in self.childs:
            return self.childs[node_name]
        n = coin_node(node_name, self, frame)
        self.childs[node_name] = n
        return n
        
    def viewAll_once(self):
        if self.had_viewAll:
            return
        self.had_viewAll = True
        self.viewer.viewAll()

    def _get_coord(self):
        if not hasattr(self, "coord"):
            self.coord = pyutils.inventor.load(coord_file)
            self.coord.ref()
        return self.coord

    def place_coord(self, name, frame, scale=1):
        coord_name = "coord_%s" % name
        node = self.get_node(coord_name, frame)
        if not hasattr(node, "coord"):
            node.scale = coin.SoScale()
            node.coord = self._get_coord()
        else:
            node.set_transform("origin", frame)
        node.scale.scaleFactor = scale, scale, scale

    def place_plane(self, name, point, normal, color=(1, 0, 0, 0.1), extends=(1, 1)):
        coord_name = "plane_%s" % name

        point = np.array(point)
        normal = np.array(normal) / np.linalg.norm(normal)

        xdir = np.array([1, 0, 0])
        ydir = np.array([0, 1, 0])

        if np.dot(normal, xdir) < np.dot(normal, ydir):
            dir1 = xdir
        else:
            dir1 = ydir

        dir2 = np.cross(normal, dir1)
        dir2 /= np.linalg.norm(dir2)

        dir1 = np.cross(dir2, normal)
        dir1 /= np.linalg.norm(dir1)

        ext = np.array(extends, dtype=float) / 2.0

        ext1 = ext[0] * dir1
        ext2 = ext[1] * dir2
        
        points = [
           point + ext1 + ext2,
           point - ext1 + ext2,
           point - ext1 - ext2,
           point + ext1 - ext2,
           ]
        points = np.array(points).tolist()
        node = self.get_node(coord_name)
        first = False
        if "coords" not in node:
            first = True
            node.coords = coin.SoCoordinate3()
            node.mat = coin.SoMaterial()
            node.hints = coin.SoShapeHints()
            node.hints.vertexOrdering = coin.SoShapeHints.CLOCKWISE #  or COUNTERCLOCKWISE,
            node.hints.shapeType = coin.SoShapeHints.UNKNOWN_SHAPE_TYPE
            node.ifs = coin.SoIndexedFaceSet()
            
        node.mat.diffuseColor = color[:3]
        if len(color) > 3:
            node.mat.transparency = 1 - color[3] # alpha to transparency!
        else:
            node.mat.transparency = 0
        
        node.coords.point.setValues(points)

        if first:
            # set ifs
            node.ifs.coordIndex.setValues([
                0, 1, 2, 3, -1])

    def place_arrow(self, name, a, b, color=None, arrow_len=None):
        arrow_name = "arrow_%s" % name
        node = self.get_node(arrow_name)
        direction = b[:3] - a[:3]
        l = np.linalg.norm(direction)

        if "transform" not in node:
            if color:
                node.m = coin.SoMaterial()
                node.add(node.m)

            node.transform = coin.SoMatrixTransform()
            node.add(node.transform)

            node.sc = coin.SoCoordinate3()
            node.add(node.sc)
            node.ifs = coin.SoIndexedLineSet()
            node.add(node.ifs)
            node.t = coin.SoTranslation()
            node.add(node.t)
            node.r = coin.SoRotation()
            node.add(node.r)
            node.cone = coin.SoCone()
            node.add(node.cone)

        if color is not None:
            node.m.diffuseColor.setValue(color)
        node.sc.point.setValues(0, 2, [(0, 0, 0), direction])
        node.ifs.coordIndex.setValues(0, 2, [0, 1])
        if arrow_len is None:
            arrow_len = 0.3 * l
            f = 0.15
        else:
            f = (arrow_len / l) / 2.
        node.cone.height = arrow_len
        node.cone.bottomRadius = arrow_len / 6.
        nl = l - (node.cone.height.getValue() / 2)
        f = nl / l
        node.t.translation = direction * f
        def get_rotation(v):
            rot = pyutils.matrix.get_any_rotation_from_vector(v)
            m = coin.SbMatrix(*list(rot.transpose().flatten()))
            return coin.SbRotation(m)
        node.r.rotation.setValue(get_rotation(direction))
        
        start_frame = np.eye(4)
        start_frame[:3, 3] = a[:3]
        node.transform.matrix = pyutils.inventor.get_ivmatrix(start_frame)

    def show_points(self, name, points, point_size=1, color=(1, 1, 1), trafo=None):
        root = self.get_node("points_%s" % name)
        if "c3" not in root:
            root.transform = coin.SoMatrixTransform()
            root.c3 = coin.SoCoordinate3()
            root.ds = coin.SoDrawStyle()
            root.mat = coin.SoMaterial()
            root.mb = coin.SoMaterialBinding()
            root.ps = coin.SoPointSet()
        root.c3.point.deleteValues(0)
        root.c3.point.setValues(points)
        root.ds.pointSize = point_size
        if len(color) == 3 or len(color) != len(points):
            root.mat.diffuseColor = color
            root.mb.value = coin.SoMaterialBinding.OVERALL
        else:
            root.mat.diffuseColor.setValues(0, len(color), color)
            root.mb.value = coin.SoMaterialBinding.PER_VERTEX
        if trafo is None:
            trafo = np.eye(4)
        root.transform.matrix = pyutils.inventor.get_ivmatrix(trafo)
        return root

    def show_lines(self, name, vertices, line_width=1, color=(1, 1, 1), trafo=None):
        root = self.get_node("lines_%s" % name)
        if "c3" not in root:
            root.transform = coin.SoMatrixTransform()
            root.c3 = coin.SoCoordinate3()
            root.ds = coin.SoDrawStyle()
            root.mat = coin.SoMaterial()
            root.mb = coin.SoMaterialBinding()
            root.ps = coin.SoLineSet()
        root.c3.point.deleteValues(0)
        root.c3.point.setValues(vertices)
        root.ds.lineWidth = line_width
        if len(color) == 3 or len(color) != len(vertices):
            root.mat.diffuseColor = color
            root.mb.value = coin.SoMaterialBinding.OVERALL
        else:
            root.mat.diffuseColor.setValues(0, len(color), color)
            root.mb.value = coin.SoMaterialBinding.PER_VERTEX
        if trafo is None:
            trafo = np.eye(4)
        root.transform.matrix = pyutils.inventor.get_ivmatrix(trafo)
        return root
    
    def clear(self, pattern=None):
        to_del = []
        for node_name, node in self.childs.items():
            if pattern is None or fnmatch.fnmatch(node_name, pattern):
                if self.pick_active == node:
                    self.pick_active = None
                to_del.append(node_name)
        for node_name in to_del:
            self.root.removeChild(self.childs[node_name].switch)
            del self.childs[node_name]

    def serialize(self):
        data = dict(
            nodes=[],
            camera=get_iv_source(self.viewer.camera)
        )        
        for idx in range(self.root.getNumChildren()):
            child = self.root.getChild(idx)
            for node_name, node in self.childs.items():
                if node.separator == child:
                    break
            else:
                continue
            data["nodes"].append((node_name, node.serialize()))
        return data

    def set_camera(self, frame, focal_distance=None):
        sbm = pyutils.inventor.get_ivmatrix(frame)
        self.viewer.camera.position.setValue(frame[:3, 3])
        self.viewer.camera.orientation.setValue(coin.SbRotation(sbm))
        if focal_distance is not None:
            self.viewer.camera.focalDistance.setValue(focal_distance)
        self.viewer.call_hook("camera_changed")

    def get_camera(self):
        x, y, z,w = self.viewer.camera.orientation.getValue().getValue()
        angle, axis = pyutils.matrix.get_angle_axis_from_quaternion(np.array([w, x, y, z]))
        T = pyutils.matrix.angle_axis2tr(angle, axis)
        T[:3, 3] = self.viewer.camera.position.getValue()
        return T, self.viewer.camera.focalDistance.getValue()

    def unserialize(self, data):
        node = pyutils.inventor.load_string(data["camera"])
        camera = node.getChild(0)
        self.viewer.camera.position.setValue(camera.position)
        self.viewer.camera.orientation.setValue(camera.orientation)
        self.viewer.camera.focalDistance = camera.focalDistance

        for node_name, node_data in data["nodes"]:
            n = coin_node(node_name, self)
            n.unserialize(node_data)
            self.childs[node_name] = n
        
    def save(self, fn):
        self.app.save(fn, self.serialize())
    def load(self, fn):
        self.unserialize(self.app.load(fn))

    def save_img(self, fn):
        from PIL import Image
        from PIL import ImageOps
        from OpenGL.GL import glReadPixels, GL_RGB, GL_UNSIGNED_BYTE
        self.do_save = True
        def my_render_callback(viewer):
            if not self.do_save:
                self.viewer.additional_render_callback = None
                return
            w = viewer.width
            h = viewer.height
            b = glReadPixels(0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE)
            row_stride = int(np.ceil(float(w)*3/4) * 4)
            print("save img to %s" % fn)
            img = ImageOps.flip(Image.frombuffer("RGB", (w, h), b, "raw", "RGB", row_stride, 1))
            img.save(fn)
            self.viewer.additional_render_callback = None
            self.do_save = False
        self.viewer.additional_render_callback = my_render_callback
        self.viewer.on_expose_event(None, None)
_coin_viewers = {}
def get_coin_viewer(name):
    if name in _coin_viewers:
        return _coin_viewers[name]
    v = coin_viewer(name)
    _coin_viewers[name] = v
    return v 

if __name__ == "__main__":
    v = get_coin_viewer("tester")
    v.place_coord("origin", np.eye(4))
    Gtk.main()
