"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt
"""

import time

class fps_counter:
	def __init__(self, output=1, size=20):
		self.size = size
		self.hits = []
		self.count = 0
		self.output_intervall = output
		self.last_output = 0
		self.start = time.time()

	def runtime(self):
		return time.time() - self.start

	def output(self):
		now = time.time()
		if now - self.last_output < self.output_intervall:
			return False
		self.last_output = now
		return True

	def hit(self):
		self.hits.append(time.time())
		self.count += 1
		while len(self.hits) > self.size:
			del self.hits[0]
			
	def fps(self):
		if not len(self.hits):
			return 0
		now = time.time()
		duration = now - self.hits[0]
		return len(self.hits) / duration

	__call__ = fps
