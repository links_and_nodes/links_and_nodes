"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt
"""
from __future__ import print_function
import traceback

class hooked_object(object):
    def __init__(self):
        self.hooks = {}
        self.iterating = 0
        self.iterating_stacks = []
        self.disabled_hooks = set()

    def add_hook(self, where, to_what, from_who=None, arg=None):        
        if where not in self.hooks:
            self.hooks[where] = {}
        h = self.hooks[where]
        if from_who is None:
            i = 0
            while True:
                from_who = "unnamed %d" % (len(h) + i)
                if from_who not in h:
                    break # ok, found unused name
                # error, this unnamed name is already in use!
                i += 1
        if self.iterating:
            pass
        h[from_who] = (to_what, arg)
        return self
        
    def call_hook(self, where, *args):
        if where in self.disabled_hooks:
            return False
        matching_hooks = self.hooks.get(where)
        if matching_hooks is None:
            return False # no hooks
        #to_del = []
        to_del = None
        self.iterating += 1
        #self.iterating_stacks.append(traceback.extract_stack()[:-1])
        had_exception = False
        for from_who, hook in list(matching_hooks.items()):
            try:
                if hook[1] is None:
                    ret = hook[0](self, *args)
                else:
                    ret = hook[0](hook[1], self, *args)
            except Exception:
                print("hook %r threw exception - args: %r, %r:" % (from_who, self, repr(args)[1:-1]))
                had_exception = True
                exc_text = traceback.format_exc()
                print(traceback.format_exc())
                ret = False
            if ret is None:
                print("warning: callback %s (%s) on hook %r returned None - removing callback!" % (
                    hook[0], from_who, where))
            if not ret:
                if to_del is None:
                    to_del = [from_who]
                else:
                    to_del.append(from_who)
        self.iterating -= 1
        #del self.iterating_stacks[-1]
        if not to_del and not had_exception:
            return True # had hook!
        if to_del:
            if self.iterating:
                print("warning: want to delete: %s but there are iteratings: %d" % (
                    to_del, self.iterating))
            for from_who in to_del:
                if from_who in matching_hooks:
                    del matching_hooks[from_who]
        if had_exception:
            raise Exception(exc_text)
        return True # had hook!

    def disable_hook(self, hook):
        self.disabled_hooks.add(hook)

    def enable_hook(self, hook):
        if hook in self.disabled_hooks:
            self.disabled_hooks.remove(hook)
    
    def remove_hooks_to(self, from_who):
        for signal, hooks in list(self.hooks.items()):
            if from_who in hooks:
                del hooks[from_who]
        
