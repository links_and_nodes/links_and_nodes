"""
Copyright 2014 Florian Schmidt

This file is part of pygtksvnb.

pygtksvnb is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pygtksvnb is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

to start gtk inspector:

GTK_DEBUG=interactive

"""

# do not enforce this on the user: from __future__ import print_function

import re
import os
import sys
import time
import traceback
import linecache
try:
    import queue
except ImportError:
    import Queue as queue
if sys.version_info[0] >= 3:
    unicode = str

# By default, Gdk loads the OpenGL 3.2 Core profile. However, PyCAM's rendering
# code uses the fixed function pipeline, which was removed in the Core profile.
# So we have to resort to this semi-public API to ask Gdk to use a Compatibility
# profile instead.
os.environ['GDK_GL'] = 'legacy'
# stolen from https://github.com/SebKuzminsky/pycam/pull/140/files
import gi # noqa: E402
gi.require_version('PangoCairo', '1.0')
gi.require_version('GLib', '2.0')

gtksource_versions = "4", "3.0"
for ver in gtksource_versions:
    try:
        gi.require_version('GtkSource', ver)
        gtksource_version = tuple(map(int, ver.split(".")))
        break
    except Exception:
        pass
else:
    raise Exception("need one of these GtkSource versions: %s (try apt-get install gir1.2-gtksource-4)" % (", ".join(gtksource_versions)))

from gi.repository import Gtk, Gdk, GLib, Pango, PangoCairo, GtkSource # noqa: E402

class Item:
    pass

class user_gui(Gtk.VBox):
    def __init__(self, cell):
        self.cell = cell
        Gtk.VBox.__init__(self)

    def __contains__(self, name):
        return name in self.__dict__

    def add(self, widget):
        self.pack_start(widget, False, False, 0)
        widget.show()

    def _reexecute(self, *args):
        self.cell.execute()
        return True

    def clear(self, name=None):
        if name is None: # all
            for name, obj in list(self.__dict__.items()):
                if not isinstance(obj, Item):
                    continue
                delattr(self, name)
            self.foreach(lambda w: self.remove(w))
            return
        # single Item-like:
        getattr(self, name).remove()
        delattr(self, name)

    def add_scale(self, name, value, min_value, max_value, step_size, page_size):
        if name not in self:
            scale = Item()
            scale.hbox = Gtk.HBox()
            scale.label = Gtk.Label(name)
            scale.hbox.pack_start(scale.label, False, False, 2)
            scale.scale = Gtk.Scale()
            scale.hbox.pack_start(scale.scale, True, True, 2)
            scale.scale.set_has_origin(False)
            scale.scale.set_orientation(Gtk.Orientation.HORIZONTAL)
            scale.config = min_value, max_value, step_size, page_size
            scale.adjustment = Gtk.Adjustment(value, min_value, max_value, step_size, page_size, page_size)
            scale.scale.set_adjustment(scale.adjustment)
            scale.scale.set_draw_value(True)
            scale.get_value = lambda: scale.adjustment.get_value()
            scale.remove = lambda: self.remove(scale.scale)
            setattr(self, name, scale)
            self.add(scale.hbox)
            scale.scale.connect("value_changed", self._reexecute)
            self.show_all()
        else:
            scale = getattr(self, name)
            this_config = min_value, max_value, step_size, page_size
            if this_config != scale.config:
                scale.adjustment.configure(value, min_value, max_value, step_size, page_size, page_size)
                scale.config = this_config
        return scale

def get_monospace_font_name():
    fmap = PangoCairo.font_map_get_default()
    fonts = dict((font.get_name().lower(), font) for font in fmap.list_families())
    have_misc_fixed = bool([fn for fn in list(fonts.keys()) if "misc fixed" in fn])
    if have_misc_fixed:
        return "Misc Fixed medium 10"
    have_fixed = bool([fn for fn in list(fonts.keys()) if "fixed" in fn])
    if have_fixed:
        return "Fixed semicondensed 10"
    return "monospace 9"

def get_monospace_font_description():
    font = get_monospace_font_name()
    return Pango.FontDescription(font)

        
class notebook_cell(object):    
    line_numbers = False
    all_cells = []
    
    def __init__(self, nb, text="", font=None, expanded=True):
        notebook_cell.all_cells.append(self)
        self.nb = nb

        self.stdout_queue = queue.Queue()
        self.stdout_idle = None

        self._skip_this_anim = False

        if font is None:
            fd = get_monospace_font_description()
        else:
            fd = Pango.FontDescription(font)
        self.fd = fd

        self.expander = Gtk.Expander()
        self.expander.set_expanded(expanded)
        self.expander.connect("notify::expanded", self._on_expander)

        self.input_label = l = Gtk.Label("In [ ]:")
        l.get_style_context().add_class("input-label")
        l.set_alignment(0, 0)
        l.set_size_request(60, -1)

        self.collapsed_label = l = Gtk.Label("collapsed")
        l.get_style_context().add_class("collapsed-label")
        l.set_alignment(0, 0)

        self.input_widget = f = Gtk.Frame()
        #f.set_size_request(-1, 50)
        self.input_widget_sw = sw = Gtk.ScrolledWindow()
        sw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.NEVER)
        f.add(sw)

        self._scroll_check = None
        self._scroll_check_under_pointer = False
        self.sv = tv = GtkSource.View()
        sw.add(tv)
        tv.get_style_context().add_class("input-textview")
        tv.connect("key-press-event", self.on_tv_key_press_event)
        tv.connect("focus", self.on_focus)
        tv.connect("focus-in-event", self.on_focus)
        tv.connect("focus-out-event", self.on_lost_focus)
        b = tv.get_buffer()
        if text is not None:
            b.set_text(text)
        b.connect("changed", self.on_input_change)
        if True:
            tv.set_show_line_numbers(notebook_cell.line_numbers)
            tv.set_show_line_marks(self.nb._do_line_marks)
            tv.set_auto_indent(True)
            tv.set_tab_width(4)
            tv.set_insert_spaces_instead_of_tabs(True)
            tv.set_show_right_margin(False)
            b.set_max_undo_levels(100)
            lm = GtkSource.LanguageManager()
            lang = lm.get_language("python")
            b.set_language(lang)
            b.set_highlight_syntax(True)
            tv.modify_font(fd)

            attrs = GtkSource.MarkAttributes()
            attrs.set_icon_name("go-next") # media-playback-start")
            tv.set_mark_attributes("play_marker", attrs, 0)

            attrs = GtkSource.MarkAttributes()
            attrs.set_icon_name("go-down") # jump")
            tv.set_mark_attributes("stack_marker", attrs, 0)

            attrs = GtkSource.MarkAttributes()
            attrs.set_icon_name(Gtk.STOCK_STOP)
            attrs.connect("query-tooltip-text", self._on_error_marker_query_tooltip)
            tv.set_mark_attributes("error_marker", attrs, 0)
            self._error_mark = None

            self._stack_markers = dict()

            tv.connect("line-mark-activated", self.on_line_mark_activate)

        self.output_label = Gtk.Label()
        self.output_label.set_no_show_all(True)
        self.output_label.get_style_context().add_class("output-label")
        self.output_label.set_alignment(0, 0)
        
        self.output_widget = Gtk.VBox()

        self.gui = user_gui(self)
        self.output_widget.pack_start(self.gui, False, False, 0)
        
        self.out_tv = tv = Gtk.TextView()
        tv.get_style_context().add_class("output-textview")
        self.out_tv.connect("key-press-event", self.on_out_tv_key_press_event)

        self.output_widget_sw = sw = Gtk.ScrolledWindow()
        sw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.NEVER)
        self.output_widget.pack_start(sw, False, False, 0)
        sw.add(tv)

        self.out_buffer = self.out_tv.get_buffer()
        tv.set_no_show_all(True)
        tv.set_editable(False)
        tv.modify_font(fd)
        b = tv.get_buffer()
        b.create_tag("red", foreground="red") # , weight=pango.WEIGHT_BOLD)
        #b.create_tag("underlined", underline=pango.UNDERLINE_SINGLE)

        if not expanded:
            name = self._get_cell_name()
            if name:
                self.collapsed_label.set_label(name)

        self.output_visible = False

    def _on_expander(self, expander, value):
        value = expander.get_expanded()
        for widget in (self.input_widget, self.output_label, self.output_widget):
            if value:
                widget.show_all()
            else:
                widget.hide()
        name = self._get_cell_name()
        if value or not name:
            self.collapsed_label.hide()
        else:
            self.collapsed_label.set_label(name)
            self.collapsed_label.show()

    def get_expanded(self):
        return self.expander.get_expanded()

    def get_insert(self):
        buffer = self.sv.get_buffer()
        return buffer.get_iter_at_mark(buffer.get_insert()).get_offset()
        
    def search_select_next(self, search, last_pos=None, reverse=False):
        buf = self.sv.get_buffer()
        text = self.get_input_text()
        len_search = len(search)
        if not reverse:
            if last_pos is None:
                offset = 0
            else:
                offset = last_pos + len_search
            pos = text.find(search, offset)
        else:
            if last_pos is None:
                last_pos = -1
            pos = text.rfind(search, 0, last_pos)
        if pos == -1:
            if hasattr(self, "cursor_before_search") and self.cursor_before_search is not None:
                buf.place_cursor(self.cursor_before_search)
                self.cursor_before_search = None
            return None
        start = buf.get_iter_at_offset(pos)
        end = buf.get_iter_at_offset(pos + len_search)
        if not hasattr(self, "cursor_before_search") or self.cursor_before_search is None:
            self.cursor_before_search = buf.get_iter_at_mark(buf.get_insert())
        buf.select_range(start, end)
        self._focus(skip_grab=False)
        return pos
        
    def search_all(self, text, search):
        offset = 0
        len_search = len(search)
        while True:
            pos = text.find(search, offset)
            if pos == -1:
                return
            yield pos, len_search
            offset = pos + len_search

    def highlight_search(self, search):
        buf = self.sv.get_buffer()
        text = self.get_input_text()
        method = self.search_all
        if not hasattr(self, "highlight_search_tag"):
            self.highlight_search_tag = buf.create_tag("highlight_search_tag", background="yellow")
        else:
            buf.remove_tag(self.highlight_search_tag, buf.get_start_iter(), buf.get_end_iter())
        if not search:
            return True
        anything = False
        for pos, len_search in method(text, search):
            start = buf.get_iter_at_offset(pos)
            end = buf.get_iter_at_offset(pos + len_search)
            buf.apply_tag(self.highlight_search_tag, start, end)
            anything = True
        return anything
            
    def attach(self, index):
        # each cell has 2 rows to attach to!!
        tr = index * 2
        self.nb.table.attach(
            self.expander, 0, 1, tr, tr + 1,
            Gtk.AttachOptions.FILL, Gtk.AttachOptions.FILL,
            0, 0)
        self.nb.table.attach(
            self.input_label, 1, 2, tr, tr + 1,
            Gtk.AttachOptions.FILL, Gtk.AttachOptions.FILL,
            0, 2)
        self.nb.table.attach(
            self.input_widget, 2, 3, tr, tr + 1,
            Gtk.AttachOptions.EXPAND|Gtk.AttachOptions.FILL, Gtk.AttachOptions.FILL,
            0, 0)
        self.nb.table.attach(
            self.collapsed_label, 2, 3, tr, tr + 1,
            Gtk.AttachOptions.EXPAND|Gtk.AttachOptions.FILL, Gtk.AttachOptions.FILL,
            0, 0)

        tr += 1
        self.nb.table.attach(
            self.output_label, 1, 2, tr, tr + 1,
            Gtk.AttachOptions.FILL, Gtk.AttachOptions.FILL,
            0, 0)
        self.nb.table.attach(
            self.output_widget, 2, 3, tr, tr + 1,
            Gtk.AttachOptions.FILL, Gtk.AttachOptions.FILL,
            0, 0)
    
        self.expander.show()
        self.input_label.show()
        if self.get_expanded():
            self.input_widget.show_all()
            self.output_label.show()
            self.output_widget.show_all()
            self.collapsed_label.hide()
        else:
            self.input_widget.hide()
            self.output_label.hide()
            self.output_widget.hide()
            self.collapsed_label.show()

    def deattach(self):
        t = self.nb.table
        t.remove(self.expander)
        t.remove(self.input_label)
        t.remove(self.input_widget)
        t.remove(self.collapsed_label)
        t.remove(self.output_label)
        t.remove(self.output_widget)
        
    def reattach(self, index):
        # each cell has 2 rows to attach to!!
        tr = index * 2
        self.nb.table.child_set(
            self.expander, **{
                "left-attach": 0,
                "right-attach": 1,
                "top-attach": tr,
                "bottom-attach": tr + 1})
        self.nb.table.child_set(
            self.input_label, **{
                "left-attach": 1,
                "right-attach": 2,
                "top-attach": tr,
                "bottom-attach": tr + 1})
        self.nb.table.child_set(
            self.input_widget, **{
                "left-attach": 2,
                "right-attach": 3,
                "top-attach": tr,
                "bottom-attach": tr + 1})
        self.nb.table.child_set(
            self.collapsed_label, **{
                "left-attach": 2,
                "right-attach": 3,
                "top-attach": tr,
                "bottom-attach": tr + 1})
        tr += 1
        self.nb.table.child_set(
            self.output_label, **{
                "left-attach": 1,
                "right-attach": 2,
                "top-attach": tr,
                "bottom-attach": tr + 1})
        self.nb.table.child_set(
            self.output_widget, **{
                "left-attach": 2,
                "right-attach": 3,
                "top-attach": tr,
                "bottom-attach": tr + 1})

    def find_cell_index(self):
        return self.nb.cells.index(self)

    def on_input_change(self, b):
        self.cursor_before_search = None
        self.nb.changed()
        return True
    
    def on_out_tv_key_press_event(self, tv, ev):
        pass_to_sv_handler = False
        if ev.state & Gdk.ModifierType.SHIFT_MASK and ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Down:
            # insert new cell below us
            pass_to_sv_handler = True
        if ev.state & Gdk.ModifierType.SHIFT_MASK and ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Up:
            # insert new cell above us
            pass_to_sv_handler = True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Down:
            pass_to_sv_handler = True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Up:
            pass_to_sv_handler = True
        if ev.keyval == Gdk.KEY_Up:
            pass_to_sv_handler = True
        if ev.keyval == Gdk.KEY_Down:
            pass_to_sv_handler = True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == ord('l'):
            pass_to_sv_handler = True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == ord('s'):
            pass_to_sv_handler = True
        if pass_to_sv_handler:
            self.on_tv_key_press_event(tv, ev)        
        return False

    def _do_scroll_check(self):
        tv = self.sv
        b = tv.get_buffer()

        # GtkSource.View vscroll has to stay at pos 0!
        adj = self.sv.get_vadjustment()
        adj.set_value(0)

        # current cursor position:
        insert_mark = b.get_insert()
        insert_iter = b.get_iter_at_mark(insert_mark)
        loc = tv.get_iter_location(insert_iter)
        left = loc.x
        right = loc.x + loc.width
        top = loc.y
        bottom = loc.y + loc.height

        # in viewport:
        r = self.input_widget.get_allocation()
        top += r.y
        bottom += r.y
        left += r.x
        right += r.x

        # check vertical scroll:
        vadj = self.nb.sw.get_vadjustment()
        vvalue = vadj.get_value()
        top -= vvalue
        bottom -= vvalue

        r = self.nb.sw.get_allocation()
        scroll_down = bottom - r.height
        scroll_up = -top

        if self._scroll_check_under_pointer:
            _, py = self.nb.sw.get_pointer()
            scroll_up += py - int(loc.height / 2)
            scroll_down += (r.height - py) - int(loc.height / 2)
            self._scroll_check_under_pointer = False

        if self._skip_this_anim:
            skip_anim = True
            self._skip_this_anim = False
        else:
            skip_anim = False

        if scroll_down > 0:
            self.nb._vscroll_to(vvalue + scroll_down, skip_anim=skip_anim)
        elif scroll_up > 0:
            vvalue -= scroll_up
            if vvalue < 0:
                vvalue = 0
            self.nb._vscroll_to(vvalue, skip_anim=skip_anim)

        # check horizontal scroll:
        hadj = self.nb.sw.get_hadjustment()
        hvalue = hadj.get_value()
        left -= hvalue
        right -= hvalue

        r = self.nb.sw.get_allocation()
        scroll_left = left - r.width
        scroll_right = -right
        if scroll_left > 0:
            hadj.set_value(hvalue + scroll_left)
        elif scroll_right > 0:
            hvalue -= scroll_right
            if hvalue < 0:
                hvalue = 0
            hadj.set_value(hvalue)

        self._scroll_check = None
        return False
    
    def schedule_scroll_check(self, under_pointer=False):
        self._scroll_check_under_pointer = self._scroll_check_under_pointer or under_pointer
        if self._scroll_check is None:
            self._scroll_check = GLib.idle_add(self._do_scroll_check)
        
    def on_tv_key_press_event(self, tv, ev):
        is_sv = tv == self.sv
        if is_sv and ev.keyval in (Gdk.KEY_Return, Gdk.KEY_KP_Enter):
            self.schedule_scroll_check()
        if (ev.state & Gdk.ModifierType.CONTROL_MASK and (ev.keyval == ord("f") or ev.keyval == ord("F") or ev.keyval == ord("r"))) or ev.keyval == Gdk.KEY_F3:
            preset = None
            buffer = tv.get_buffer()
            if buffer.get_has_selection():
                start, end = buffer.get_selection_bounds()
                preset = buffer.get_text(start, end, False)
            reverse = ev.keyval == ord("r") or ev.state & Gdk.ModifierType.SHIFT_MASK
            self.nb.search(reverse=reverse, preset=preset)
            return True
        if ev.keyval == Gdk.KEY_BackSpace:
            b = self.sv.get_buffer()
            insert_mark = b.get_insert()
            insert_iter = b.get_iter_at_mark(insert_mark)
            if insert_iter.get_offset() > 0:
                if is_sv: self.schedule_scroll_check()
                return False
            if self.get_input_text() != "":
                if is_sv: self.schedule_scroll_check()
                return False
            if len(self.nb.cells) == 1:
                if is_sv: self.schedule_scroll_check()
                return False
            old_idx = self.find_cell_index()
            idx = self.nb.delete_cell(self, by_backspace=True)
            cell = self.nb.cells[idx]
            # set cursor to end of buffer of above cell
            b = cell.sv.get_buffer()
            if old_idx == 0:
                cursor_iter = b.get_bounds()[0] # beginning
            else:
                cursor_iter = b.get_bounds()[1] # end
            b.place_cursor(cursor_iter)
            cell.focus()
            return True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == ord("-"):
            # split current cell at cursor
            b = self.sv.get_buffer()
            insert_mark = b.get_insert()
            insert_iter = b.get_iter_at_mark(insert_mark)
            rest = b.get_text(insert_iter, b.get_end_iter(), False)
            b.delete(insert_iter, b.get_end_iter())
            
            idx = self.find_cell_index()
            cell = self.nb.new_cell_at(index=idx + 1)
            b2 = cell.sv.get_buffer()
            b2.set_text(rest)
            cell.focus()
            return True 
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Page_Up:
            # join with cell above
            idx = self.find_cell_index()
            if idx > 0:
                cell_above = self.nb.cells[idx - 1]

                b = self.sv.get_buffer()
                this = b.get_text(b.get_start_iter(), b.get_end_iter(), False)
            
                b2 = cell_above.sv.get_buffer()
                above = b2.get_text(b2.get_start_iter(), b2.get_end_iter(), False)
                b2.set_text(above.rstrip("\n") + "\n" + this.lstrip("\n"))
                cell_above.focus()
                self.nb.delete_cell(self)
            return True 
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Page_Down:
            # join with cell above
            idx = self.find_cell_index()
            if idx > 0:
                cell_above = self.nb.cells[idx + 1]

                b = self.sv.get_buffer()
                this = b.get_text(b.get_start_iter(), b.get_end_iter(), False)
            
                b2 = cell_above.sv.get_buffer()
                above = b2.get_text(b2.get_start_iter(), b2.get_end_iter(), False)
                b2.set_text(this.rstrip("\n") + "\n" + above.lstrip("\n"))
                cell_above.focus()
                self.nb.delete_cell(self)
            return True 
        if ev.state & Gdk.ModifierType.SHIFT_MASK and ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Down:
            # insert new cell below us
            idx = self.find_cell_index()
            cell = self.nb.new_cell_at(index=idx + 1)
            cell.focus()
            return True
        if ev.state & Gdk.ModifierType.SHIFT_MASK and ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Up:
            # insert new cell above us
            idx = self.find_cell_index()
            cell = self.nb.new_cell_at(index=idx)
            cell.focus()
            return True 
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Down:
            idx = self.find_cell_index()
            while idx < len(self.nb.cells):
                idx += 1
                if idx < len(self.nb.cells) and self.nb.cells[idx].get_expanded():
                    # move to cell below us
                    cell = self.nb.cells[idx]
                    cell.focus()
                    return True
            return True 
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Up:
            idx = self.find_cell_index()
            idx -= 1
            while idx >= 0:
                # move to cell above us
                cell = self.nb.cells[idx]
                if cell.get_expanded():
                    cell.focus()
                    return True
                idx -= 1
            return True 
        if ev.keyval == Gdk.KEY_Up or ev.keyval == Gdk.KEY_Page_Up:
            b = tv.get_buffer()
            insert_mark = b.get_insert()
            insert_iter = b.get_iter_at_mark(insert_mark)
            if b.get_line_count() == 1:
                line2_iter = b.get_end_iter()
            else:
                line2_iter = b.get_iter_at_line(1)
            if insert_iter.compare(line2_iter) < 0 or (insert_iter.compare(line2_iter) == 0 and b.get_line_count() == 1):
                # we are already at the first line!
                idx = self.find_cell_index() - 1
                while idx >= 0:
                    cell = self.nb.cells[idx]
                    if cell.get_expanded():
                        cell.focus()
                        b = cell.sv.get_buffer()
                        b.place_cursor(b.get_end_iter())
                        return True
                    idx -= 1
                return True 
            if is_sv: self.schedule_scroll_check()
            return False
        if ev.keyval == Gdk.KEY_Down or ev.keyval == Gdk.KEY_Page_Down:
            b = tv.get_buffer()
            insert_mark = b.get_insert()
            insert_iter = b.get_iter_at_mark(insert_mark)
            line2_iter = b.get_iter_at_line(b.get_line_count() - 1)
            if insert_iter.compare(line2_iter) >= 0:
                # we are already at the last line!
                idx = self.find_cell_index() + 1
                while idx < len(self.nb.cells):
                    cell = self.nb.cells[idx]
                    if cell.get_expanded():
                        cell.focus()
                        b = cell.sv.get_buffer()
                        b.place_cursor(b.get_start_iter())
                        return True
                    idx += 1
                return True 
            if is_sv: self.schedule_scroll_check()
            return False
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == ord('l'):
            notebook_cell.line_numbers = not notebook_cell.line_numbers
            for cell in notebook_cell.all_cells:
                cell.sv.set_show_line_numbers(notebook_cell.line_numbers)
            return True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == ord('s'):
            self.nb.save()
            return True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Return:
            if not self.execute():
                return True
            if not ev.state & Gdk.ModifierType.SHIFT_MASK:
                return True
        if ev.state & Gdk.ModifierType.SHIFT_MASK and ev.keyval == Gdk.KEY_Return:
            # select next cell or add new cell if last cell
            idx = self.find_cell_index()
            if idx + 1 == len(self.nb.cells):
                # we are last! -> add new cell!
                cell = self.nb.new_cell() # index=idx + 1)
            else:
                # just select next cell
                cell = self.nb.cells[idx + 1]
            cell.focus()
            return True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == ord('a'):
            # goto start of line
            b = tv.get_buffer()
            insertmark = b.get_insert()
            insertiter = b.get_iter_at_mark(insertmark)
            line_top, line_height = tv.get_line_yrange(insertiter)
            lineiter, line_top = tv.get_line_at_y(line_top)
            b.place_cursor(lineiter)
            return True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == ord('e'):
            # goto end of line
            b = tv.get_buffer()
            insertmark = b.get_insert()
            insert_iter = b.get_iter_at_mark(insertmark)
            tv.forward_display_line_end(insert_iter)
            b.place_cursor(insert_iter)
            return True
        if is_sv:
            is_modifier_key = ev.keyval in (Gdk.KEY_Alt_L, Gdk.KEY_Alt_R, Gdk.KEY_Control_L, Gdk.KEY_Control_R,  Gdk.KEY_Shift_L, Gdk.KEY_Shift_R, Gdk.KEY_ISO_Level3_Shift)
            if not is_modifier_key:
                self.schedule_scroll_check()

    def get_input_text(self):
        b = self.sv.get_buffer()
        return b.get_text(b.get_start_iter(), b.get_end_iter(), False)
    def get_output_text(self):
        b = self.out_buffer
        return b.get_text(b.get_start_iter(), b.get_end_iter(), False)
    def set_output_text(self, text):
        self.out_buffer.set_text(text)
        self.output_visible = True
        self.out_tv.show()       
        self.output_label.show()

    def replace_rprint_magic(self, m):
        indent = m.group(1)
        expression = m.group(2)
        return '%sdo_pprint(%s, %s)' % (
            indent, repr(expression.strip()), expression)
        #return '%sprint %s,; pprint.pprint((%s))' % (indent, repr(expression.strip() + ": "), expression)
    
    def replace_print_magic(self, m):
        indent = m.group(1)
        expression = m.group(2)
        if sys.version_info[0] >= 3:
            return '%sprint(%s, (%s))' % (indent, repr(expression.strip() + ":"), expression)
        return '%sprint %s, (%s)' % (indent, repr(expression.strip() + ":"), expression)
    
    def clear(self):
        self.out_buffer.set_text("")

    def _get_cell_name(self):
        src = self.get_input_text()
        names = list([line for line in src.split("\n") if line.startswith("## ") and "name:" in line])
        if names:
            return names[0].split("name:", 1)[1].strip()
        return
    
    def execute(self, redirect_stdout=True, output_there=False):
        src = self.get_input_text()

        if self.nb._do_line_marks:
            if len(self.nb.module.cells) == 0:
                self.nb._remove_error_markers()
            self.nb._cell_stack_move_play_markers()

        if not src.strip():
            # nothing to execute
            self.had_output = False
            self.out_buffer.set_text("")
            self.output_label.hide()
            self.out_tv.hide()       
            self.output_visible = False
            return False

        idx = self.find_cell_index()
        name = self._get_cell_name()
        if name:
            self.name = name
            self.nb.insert_cell_name(idx, name)
            name_or_idx = "cell[%s]" % name
        else:
            name_or_idx = "cell[%d]" % idx

        # preprocess source
        if not hasattr(self, "pprint_re"):
            self.pprint_re = re.compile("^([ \t]*)%%[ \t]*(.*)$", flags=re.M)
            self.print_re = re.compile("^([ \t]*)%[ \t]*(.*)$", flags=re.M)
        src = re.sub(self.pprint_re, self.replace_rprint_magic, src)
        src = re.sub(self.print_re, self.replace_print_magic, src)

        self.nb.executions += 1
        self.execution_id = self.nb.executions
        self.input_label.set_text("In [%d]:" % self.execution_id)

        self.had_output = False
        old_output = self.get_output_text()
        self.clear()

        self.sv.get_style_context().add_class("active-input-textview")
        # allow gui to redraw cell as executing
        wait_time = 0.004
        deadline = time.time() + wait_time
        timer_source_id = GLib.timeout_add(int(wait_time * 1100), lambda: True)
        while time.time() < deadline:
            Gtk.main_iteration()
        GLib.source_remove(timer_source_id)

        self.nb.module.cell = self
        self.nb.module.clear = self.clear
        self.nb.module.cell_id = this_cell_id = self.nb.cells.index(self)
        self.grab_focus = False
        #self.grab_focus = True
        #if self.nb.module.cells:
        #    self.nb.module.cells[-1].grab_focus = False
        self.nb.module.cells.append(self)
        self.stop_request_at_start = self.nb.stop_request
        if (len(self.nb.module.cells) == 1 or output_there) and redirect_stdout:
            stdout_before = sys.stdout
            sys.stdout = self
        self.nb.module.sys = sys
        
        try:
            fn = "<%s>" % name_or_idx
            linecache.cache[fn] = len(src), None, src.split("\n"), fn
        except:
            pass

        self._did_display_error = False
        try:
            code = compile(src, fn, "exec")
            exec(code, self.nb.module.__dict__)

            self.nb.already_executed.add(this_cell_id)
            if hasattr(self, "name"):
                self.nb.already_executed.add(self.name)

        except Exception:
            tb = traceback.format_exc().strip().split("\n")
            del tb[1:3]
            if "stop requested" in tb[-1]:
                is_cell_return = "(cell_return)" in tb[-1]
                if is_cell_return:
                    self.nb.stop_request -= 1 # undo stop request for calling cell's!
                del tb[-3:-1]
            else:
                is_cell_return = False
            if not is_cell_return:
                tb = "\n".join(tb)
                if len(self.nb.module.cells) > 1:
                    raise
                self._did_display_error = True
                if self.nb._do_line_marks:
                    self.nb._display_error_markers()
                if redirect_stdout:
                    self.write("\n" + tb, tags=["red"], from_gui_context=True)
                else:
                    sys.stdout.write("\n" + tb)
        finally:
            if self.nb._do_line_marks:
                this = self, self.sv.get_buffer()
                self.nb._remove_markers([this], "play_marker") # UUGGLY!
                if not self._did_display_error:
                    self.nb._remove_markers([this], "stack_marker") # UUGGLY!

            # todo: not working with Gtk3
            self.sv.get_style_context().remove_class("active-input-textview")

            del self.nb.module.cells[-1]
            if self.nb.module.cells:
                self.nb.module.cell = self.nb.module.cells[-1]
            else:
                self.nb.module.cell = None

            if redirect_stdout:
                if len(self.nb.module.cells) == 0:
                    sys.stdout = self.nb.original_stdout
                elif output_there:
                    sys.stdout = stdout_before
                self._add_stdout()

            if not self.had_output:
                self.output_label.hide()
                self.out_tv.hide()
                self.output_visible = False
            else:
                this_output = self.get_output_text()
                self.nb.outputs[self.execution_id] = this_output
                while len(self.nb.outputs) > self.nb._max_outputs:
                    eid = self.execution_id - self.nb._max_outputs
                    if eid in self.nb.outputs:
                        del self.nb.outputs[eid]
                    else:
                        too_many = len(self.nb.outputs) - self.nb._max_outputs
                        for eid in sorted(self.nb.outputs.keys())[:too_many]:
                            del self.nb.outputs[eid]
                if old_output != this_output:
                    self.nb.changed(output_changed=True)

            if redirect_stdout and self.grab_focus:
                self.sv.grab_focus() # steal back focus for e.g. Gtk.Window creation or plotting
            if len(self.nb.module.cells) != 0:
                idx = self.nb.module.cells[-1].find_cell_index()
            self.nb.highlight_cell_name(idx)
        return True
    
    def flush(self):
        # for stdout emulation, flush output
        for i in range(10):
            Gtk.main_iteration_do(False)

    def _add_stdout(self):
        b = self.out_buffer
        while True:
            try:
                text, tags = self.stdout_queue.get(block=False)
            except queue.Empty:
                break
            #self.nb.original_stdout.write("write: %r\n" % text)

            if tags is None:
                b.insert(b.get_end_iter(), text)
            else:
                b.insert_with_tags_by_name(b.get_end_iter(), text, *tags)

        Gtk.main_iteration_do(False)

        if not self.had_output:
            self.output_label.set_text("Out[%d]:" % self.execution_id)
            self.had_output = True

        if not self.output_visible:
            self.output_visible = True
            self.out_tv.show()
            self.output_label.show()

        self.stdout_idle = None
        return False

    def write(self, text, tags=None, from_gui_context=False):
        # this call might originate from different threads!
        if type(text) != unicode:
            text = text.decode("utf-8")
        self.stdout_queue.put((text, tags))

        if from_gui_context:
            self._add_stdout()
        elif self.stdout_idle is None:
            self.stdout_idle = GLib.idle_add(self._add_stdout)

    def _focus(self, skip_grab=False, under_pointer=False):
        if not skip_grab:
            self.sv.grab_focus()
        self.schedule_scroll_check(under_pointer=under_pointer)
        return False
    
    def focus(self, skip_anim=False, jump_line=None):
        if skip_anim:
            self._skip_this_anim = True
        self.nb.current_cell = self
        if jump_line is not None:
            b = self.sv.get_buffer()
            iter = b.get_iter_at_line(jump_line - 1)
            b.place_cursor(iter)
        self.sv.grab_focus()
        self.expander.set_expanded(True)
        Gtk.main_iteration_do(False)
        GLib.idle_add(self._focus, False, jump_line is not None)

    def on_focus(self, widget, direction):
        self.nb.current_cell = self
        self.input_label.get_style_context().add_class("highlighted-label")
        #self.sv.get_style_context().add_class("highlighted-input-textview")
        if self.nb.module.cells:
            self.nb.module.cells[-1].grab_focus = False
        if hasattr(self, "name") and self.name:
            self.nb.highlight_cell_name(self.find_cell_index())
        else:
            self.nb.highlight_cell_name(-1)

    def on_lost_focus(self, widget, direction):
        self.input_label.get_style_context().remove_class("highlighted-label")
        #self.sv.get_style_context().remove_class("highlighted-input-textview")

    def on_line_mark_activate(self, view, iter, ev):
        b = self.sv.get_buffer()
        stack_markers = b.get_source_marks_at_line(iter.get_line(), "stack_marker")
        if not stack_markers:
            stack_markers = b.get_source_marks_at_line(iter.get_line(), "error_marker")
        if not stack_markers:
            return True
        for mark in stack_markers:
            down_up = self._stack_markers.get(mark)
            if down_up is None:
                continue
            down, up = down_up
            if ev.get_button().button == 1:
                have_control = bool(ev.state & Gdk.ModifierType.CONTROL_MASK)
                if not have_control and down:
                    cell, line = down
                    cell.focus(jump_line=line)
                elif have_control and up:
                    cell, line = up
                    cell.focus(jump_line=line)
            break
        return True

    def _remember_stack_marker(self, mark, stack_down, stack_up):
        self._stack_markers[mark] = stack_down, stack_up

    def _remember_error(self, mark, text):
        self._error_mark = mark, text

    def _on_error_marker_query_tooltip(self, attrs, mark):
        if self._error_mark and mark == self._error_mark[0]:
            return self._error_mark[1]
        return
