"""
Copyright 2014 Florian Schmidt

This file is part of pygtksvnb.

pygtksvnb is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pygtksvnb is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# do not enforce this on the user: from __future__ import print_function

import re
import os
import sys
import imp
import time
import pprint
import pickle
import hashlib
import numpy
import matplotlib
import traceback
try:
    from cStringIO import StringIO
    # still use cStringIO on py2 to not have to use u"" everywhere
except ImportError:
    from io import StringIO

# By default, Gdk loads the OpenGL 3.2 Core profile. However, PyCAM's rendering
# code uses the fixed function pipeline, which was removed in the Core profile.
# So we have to resort to this semi-public API to ask Gdk to use a Compatibility
# profile instead.
os.environ['GDK_GL'] = 'legacy'
# stolen from https://github.com/SebKuzminsky/pycam/pull/140/files
import gi # noqa: E402
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GLib # noqa: E402
# https://lazka.github.io/pgi-docs/#Gtk-3.0
# https://lazka.github.io/pgi-docs/GObject-2.0/index.html

skip_pylab_import = os.getenv("SKIP_PYLAB_IMPORT", "false")[0] in "1yt"

if (not hasattr(matplotlib, "already_choosen") or not matplotlib.already_choosen) and os.getenv("MPLBACKEND") is None and not skip_pylab_import:
    # try to select an acceptable interactive backend
    try:
        matplotlib.use('GTK3Agg')
        matplotlib.interactive(1)
        skip_pylab_import = False
    except Exception:
        del sys.modules['matplotlib.backends']
        try:
            matplotlib.use('Qt5Agg')
            matplotlib.interactive(1)
            import matplotlib.pylab
            skip_pylab_import = False
        except Exception:
            del sys.modules['matplotlib.backends']
            sys.stdout.write("GTKAgg and Qt5Agg backend not avaliable. please check https://matplotlib.org/faq/usage_faq.html#id3\n"
                  "to learn how to configure which backend you want to use for gtk and do the matplotlib.pylab\n"
                  "import yourself in your notebook.")
            skip_pylab_import = True
elif not skip_pylab_import:
    matplotlib.interactive(1)

from pyutils.pygtksvnb.notebook_cell import notebook_cell # noqa: E402

class notebook(object):
    opened = {}
    have_main = False
    next_cell_prefix = "## next cell"

    def __init__(self, filename=None, storage="filesystem", add_to_context=None, run_in_init_context=None, on_hide=None, new_notebook_cell_content=None, create_file=False, redirect_stdout=True, no_scroll_anim=False):
        if add_to_context is None:
            add_to_context = {}
        self.storage = storage
        self.create_file = create_file
        self.no_scroll_anim = no_scroll_anim
        if new_notebook_cell_content is None:
            self.new_notebook_cell_content = 'print("may the source be with you!")\n'
        else:
            self.new_notebook_cell_content = new_notebook_cell_content
        self._init_once()
        self.original_stdout = sys.stdout
        self.executions = 0
        self.stop_request = 0
        self.initial_context = add_to_context
        if run_in_init_context is None:
            run_in_init_context = """
from numpy import *
from numpy.linalg import *
import pprint
import sys
import os
"""
            if not skip_pylab_import:
                run_in_init_context += "\nfrom matplotlib.pylab import *\n"
        #exec run_in_init_context in self.initial_context
        exec(run_in_init_context, self.initial_context)

        self.on_hide = on_hide
        self._vscroll_timeout = None
        self.window = w = Gtk.Window()
        
        gtkrc_file = os.path.join(os.path.dirname(__file__), "gtkrc.css")
        css_provider = Gtk.CssProvider()
        css_provider.load_from_path(gtkrc_file)
        screen = Gdk.Screen.get_default()
        self.window.get_style_context().add_provider_for_screen(
            screen,
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        self.window.set_default_size(470, 480)
        w.connect("delete-event", self.on_delete)
        w.connect("configure-event", self.on_configure)
        w.connect_after("key-press-event", self.on_window_key_press_event)
        
        vb = Gtk.VBox()
        w.add(vb)

        tb = Gtk.Toolbar()
        tb.set_property("toolbar-style", Gtk.ToolbarStyle.ICONS)
        vb.pack_start(tb, False, False, 0)

        def append_button_item(text, tooltip_text, tooltip_private_text, icon, cb, *cb_data):
            btn = Gtk.ToolButton(label=text, icon_widget=icon)
            btn.set_tooltip_text(tooltip_text)
            btn.connect("clicked", lambda *args: cb(*cb_data))
            tb.insert(btn, -1)
        def append_toggle_button_item(text, tooltip_text, tooltip_private_text, icon, cb, *cb_data):
            btn = Gtk.ToggleToolButton(label=text, icon_widget=icon)
            btn.set_tooltip_text(tooltip_text)
            btn.connect("toggled", lambda *args: cb(args[0], *cb_data))
            tb.insert(btn, -1)
            return btn
        def append_space():
            space = Gtk.SeparatorToolItem()
            space.set_draw(False)
            tb.insert(space, -1)

        if self.storage == "filesystem":
            append_button_item(
                None, "new notebook", None,
                Gtk.Image.new_from_stock(Gtk.STOCK_NEW, Gtk.IconSize.SMALL_TOOLBAR),
                self.new_notebook)

        append_button_item(
            None, "save notebook",
            None,
            Gtk.Image.new_from_stock(Gtk.STOCK_SAVE, Gtk.IconSize.SMALL_TOOLBAR),
            lambda *args: self.save())

        if self.storage == "filesystem":
            append_button_item(
                None, "save notebook with a different name",
                None,
                Gtk.Image.new_from_stock(Gtk.STOCK_SAVE_AS, Gtk.IconSize.SMALL_TOOLBAR),
                lambda *args: self.save(True))
            append_button_item(
                None, "open a saved notebook",
                None,
                Gtk.Image.new_from_stock(Gtk.STOCK_OPEN, Gtk.IconSize.SMALL_TOOLBAR),
                lambda *args: self.open_notebook())

        append_space()

        append_button_item(
            None, "move to cell above", None,
            Gtk.Image.new_from_stock(Gtk.STOCK_GO_UP, Gtk.IconSize.SMALL_TOOLBAR),
            lambda *args: self.move_cell_up())
        append_button_item(
            None, "move to cell below", None,
            Gtk.Image.new_from_stock(Gtk.STOCK_GO_DOWN, Gtk.IconSize.SMALL_TOOLBAR),
            lambda *args: self.move_cell_down())

        append_space()
        
        append_button_item(
            None, "insert cell above", None,
            Gtk.Image.new_from_stock(Gtk.STOCK_GOTO_TOP, Gtk.IconSize.SMALL_TOOLBAR),
            lambda *args: self.insert_cell_up())
        append_button_item(
            None, "insert cell below", None,
            Gtk.Image.new_from_stock(Gtk.STOCK_GOTO_BOTTOM, Gtk.IconSize.SMALL_TOOLBAR),
            lambda *args: self.insert_cell_down())

        append_space()

        append_button_item(
            None, "run cell", None,
            Gtk.Image.new_from_stock(Gtk.STOCK_MEDIA_PLAY, Gtk.IconSize.SMALL_TOOLBAR),
            lambda *args: self.run_cell())
        append_button_item(
            None, "run cell and jump to next", None,
            Gtk.Image.new_from_stock(Gtk.STOCK_MEDIA_NEXT, Gtk.IconSize.SMALL_TOOLBAR),
            lambda *args: self.run_cell(True))
        append_button_item(
            None, "stop cell (needs to have preemption points!)", None,
            Gtk.Image.new_from_stock(Gtk.STOCK_MEDIA_STOP, Gtk.IconSize.SMALL_TOOLBAR),
            lambda *args: self.stop_cell())

        if self.storage == "filesystem":
            append_space()
            self.out_in_separate_file_tb = append_toggle_button_item(
                None, "save cell-output in separate file", None,
                Gtk.Image.new_from_stock(Gtk.STOCK_HARDDISK, Gtk.IconSize.SMALL_TOOLBAR),
                self.on_toggle_separate_file)
            self.out_in_separate_file_tb.set_active(os.getenv("NB_SEPARATE_OUT_FILE", "y")[:1].lower() in ("1", "y", "t"))
        else:
            self.out_in_separate_file_tb = None
        append_space()

        append_button_item(
            None, "insert cell showing help", None,
            Gtk.Image.new_from_stock(Gtk.STOCK_HELP, Gtk.IconSize.SMALL_TOOLBAR),
            lambda *args: self.insert_help_cell())

        
        self.cell_names_visible = False
        self.hpaned = hb = Gtk.HPaned()
        vb.pack_start(hb, True, True, 0)
        
        self.sw = sw = Gtk.ScrolledWindow()
        hb.pack1(sw, True, True)
        sw.set_size_request(200, 200)
        sw.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        
        sw_vbox = Gtk.VBox()
        self.sw.add_with_viewport(sw_vbox)
        self.sw_vbox_vp = sw_vbox.get_parent()
        self.sw_vbox_vp.set_focus_hadjustment(Gtk.Adjustment(value=0, lower=0, upper=0))
        self.sw_vbox_vp.set_focus_vadjustment(Gtk.Adjustment(value=0, lower=0, upper=0))

        self.table = t = Gtk.Table(n_rows=1, n_columns=2, homogeneous=False)
        sw_vbox.pack_start(t, False, False, 0)
        t.set_row_spacings(5)
        t.set_border_width(10)

        self.spacer = Gtk.Label()
        self.spacer.set_size_request(-1, 400)
        sw_vbox.pack_start(self.spacer, False, False, 0)

        self.cell_name_list = lv = Gtk.TreeView()
        lv.connect("cursor-changed", self.cell_name_cursor_change)
        lv.set_headers_visible(False)
        
        self.cell_name_sw = nsw = Gtk.ScrolledWindow()
        nsw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        nsw.add(lv)

        self.skip_cell_name_change = False
        self.cell_name_model = m = Gtk.ListStore(int, str)
        lv.set_model(m)
        lv.insert_column_with_attributes(-1, "cell name", Gtk.CellRendererText(), text=1)

        self._try_load_user_settings()

        self.have_cell_names = False
        self.cells = []
        self._do_line_marks = os.getenv("NB_NO_LINE_MARKS", "n")[:1].lower() not in "1yt"
        self._buffers_with_error_markers = []
        vb.show_all()

        if filename is not None:
            self.filename = filename
            self.derive_name()
            self.reset()
            self.load(filename, redirect_stdout)
        else:
            self.filename = None
            self.derive_name()
            self.reset()

        if self.filename is None or len(self.cells) == 0:
            nc = self.new_notebook_cell_content
            nc = nc.split("\n#~~~--~~~\n")
            self.current_cell = None
            autoexec_cells = []
            for content in nc:
                cell = self.new_cell(text=content)
                if self.current_cell is None:
                    self.current_cell = cell
                if list([line for line in content.split("\n") if line.startswith("## ") and "autoexec" in line]):
                    autoexec_cells.append(cell)
            self.changed(False)
            for cell in autoexec_cells:
                cell.execute()
            if self.current_cell:
                self.current_cell.focus(skip_anim=True)
        else:
            self.changed(False)

        #self.reset()
        if not self.have_cell_names:
            self.cell_name_list.hide()
            hb.set_position(-1)

    def on_toggle_separate_file(self, tb):
        if hasattr(self, "name"):
            self.changed(True)

    def search_entry_normal(self):
        self.search_dialog_entry.get_style_context().remove_class("search-not-found")
    def search_entry_not_found(self):
        self.search_dialog_entry.get_style_context().add_class("search-not-found")

    def do_search(self, text, reverse=None):
        if reverse is None:
            reverse = self.search_dialog_reverse
    
        if self.search_state is not None:
            idx = self.search_state[0]
        else:
            if self.current_cell:
                idx = self.current_cell.find_cell_index()
                self.search_state = idx, self.current_cell.get_insert()
            else:
                idx = 0
        if not reverse:
            candidates = enumerate(self.cells[idx:])
            idx_base = idx
        else:
            candidates = list(enumerate(self.cells[0:idx+1]))[::-1]
            idx_base = 0
        for idx_offset, cell in candidates:
            cidx = idx_base + idx_offset
            if self.search_state and self.search_state[0] == cidx:
                pos = cell.search_select_next(text, self.search_state[1], reverse=reverse)
            else:
                pos = cell.search_select_next(text, reverse=reverse)
            if pos is not None:
                self.search_state = cidx, pos
                self.search_entry_normal()
                return
            self.search_state = None
        # no more search results
        self.search_entry_not_found()
        
    def do_highlight_search(self, text):
        if not hasattr(self, "search_dialog"):
            return
        anything = False
        for cell in self.cells:
            anything = cell.highlight_search(text) or anything
        self.search_state = None
        if anything:
            self.search_entry_normal()
        else:
            self.search_entry_not_found()
            
        
    def on_search_dialog(self, dialog, response):
        if response == 3:
            self.search_dialog_reverse = False
            response = 1
        if response == 2:
            self.search_dialog_reverse = True
            response = 1
        if response == 1:
            self.do_search(self.search_dialog_entry.get_text())
            return True
        if response == Gtk.ResponseType.DELETE_EVENT:
            dialog.hide()
            self.do_highlight_search("")
            self.search_dialog_visible = False
            return True
        return True
        
    def search(self, reverse=False, preset=None):
        was_already_visible = hasattr(self, "search_dialog_visible") and self.search_dialog_visible
        if not hasattr(self, "search_dialog"):
            self.search_state = None
            self.search_dialog = dialog = Gtk.Dialog(
                "search",
                self.window,
                Gtk.DialogFlags.DESTROY_WITH_PARENT,
                (Gtk.STOCK_GO_BACK, 2,
                 Gtk.STOCK_GO_FORWARD, 3,
                 Gtk.STOCK_CLOSE, Gtk.ResponseType.DELETE_EVENT))
            dialog.connect("response", self.on_search_dialog)
            dialog.connect("delete-event", lambda diag, ev: self.on_search_dialog(dialog, Gtk.ResponseType.DELETE_EVENT))
            dialog.connect("key-press-event", self.on_window_key_press_event)
            
            dialog.vbox.pack_start(Gtk.Label("search for:"), False, False, 0)
            self.search_dialog_entry = entry = Gtk.Entry()
            dialog.vbox.pack_start(entry, True, True, 0)
            entry.connect("activate", lambda entry: self.do_search(entry.get_text()))
            entry.connect("changed", lambda entry: self.do_highlight_search(entry.get_text()))

            self.search_dialog_reverse = False
            
            dialog.vbox.show_all()
        
        self.search_dialog_reverse = bool(reverse)
        if preset is None:
            self.do_highlight_search(self.search_dialog_entry.get_text())
        
        self.search_entry_normal()
        self.search_dialog_visible = True
        self.search_dialog.present()        
        if preset is not None:
            if not was_already_visible:
                self.search_dialog_entry.set_text(preset)
            self.do_highlight_search(preset)
        
        if was_already_visible:
            self.on_search_dialog(self.search_dialog, 1)
        else:
            self.search_dialog_entry.select_region(0, -1)
            self.search_dialog_entry.grab_focus()
                    
    def on_window_key_press_event(self, w, ev):
        if (ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == ord("f")) or ev.keyval == Gdk.KEY_F3:
            self.search(reverse=ev.state & Gdk.ModifierType.SHIFT_MASK)
            return True
        return False
    
    _once_initialized = False
    @staticmethod
    def _init_once():
        if notebook._once_initialized:
            return
        notebook._once_initialized = True

    def _get_stop_request_at_start(self):
        executing_cell = self.module.cell
        if executing_cell is None:
            return self.stop_request
        return executing_cell.stop_request_at_start

    def _had_stop_request(self, stop_request_at_start):
        return stop_request_at_start != self.stop_request

    def _raise_on_stop_request(self, stop_request_at_start, msg="user"):
        if self._had_stop_request(stop_request_at_start):
            raise Exception("stop requested by %s" % msg)

    def _while_sleeping(self):
        return True

    def sleep(self, t, show_play_marker="auto"):
        start = time.time()
        if self._do_line_marks and show_play_marker is True or (t > 0.05 and show_play_marker == "auto"):
            self._cell_stack_move_play_markers()

        sr = self._get_stop_request_at_start()
        if t > 0:
            src = GLib.timeout_add(int(numpy.ceil(t * 1000)), self._while_sleeping)
            while time.time() - start < t and not self._had_stop_request(sr):
                Gtk.main_iteration_do(True)
            GLib.source_remove(src)
        Gtk.main_iteration_do(False)
        self._raise_on_stop_request(sr)

    def preempt(self, blocking=False):
        self._raise_on_stop_request(self._get_stop_request_at_start())
        Gtk.main_iteration_do(blocking)

    def require_once(self, cell_id, redirect_stdout=True, output_there=False):
        if cell_id in self.already_executed:
            return False
        cell = self._find_cell(cell_id)
        cell.execute(redirect_stdout=redirect_stdout, output_there=output_there)
        return True

    def ln_unpack_log_data(self, data, only_one_topic=False, no_topic_prefix=False, topic_prefix_map=None, stack_fields=None):
        """
         assume flat mapping from pickle file or raw logger_data
         only_one_topic == False:
           data is expected to be dict(topic1=dict(field1=data1, field2=data2, ...), ...)
         only_one_topic == True:
           data is expected to be dict(field1=data1, field2=data2, ...)
        """
        if topic_prefix_map is None:
            topic_prefix_map = {}
        if stack_fields is None:
            stack_fields = []
        if type(data).__name__ == "logger_data_wrapper":
            return data.unpack_into(self.module.__dict__, stack_fields=stack_fields)
        if only_one_topic:
            data = { "": data }
        shapes = []
        max_name_len = 0
        def proper_identifier(name):
            return name.strip().replace(".", "_").replace("/", "_").replace(" ", "_")
        for topic_name, topic_data in data.items():
            topic_name = topic_prefix_map.get(topic_name, topic_name)
            topic_name = proper_identifier(topic_name)
            for field_name, value in topic_data.items():
                field_name = proper_identifier(field_name)
                if topic_name and not no_topic_prefix:
                    name = "%s_%s" % (topic_name, field_name)
                else:
                    name = field_name
                if len(name) > max_name_len:
                    max_name_len = len(name)
                shapes.append((name, value.shape))
                self.module.__dict__[name] = value
        shapes.sort()
        for name, shape in shapes:
            sys.stdout.write("%-*.*s %s\n" % (max_name_len, max_name_len, name, shape))

    def _find_cell(self, cell_id):
        # search cell_id
        # can be int: cell-number from top
        cell = None
        if type(cell_id) == int:
            cell = self.cells[cell_id]
        else:
            # guess its a cell name
            # search matching cell
            for cell in self.cells:
                if hasattr(cell, "name") and cell.name == cell_id:
                    break
            else:
                raise Exception("no cell with name %r found in this notebook!" % cell_id)
        return cell

    def _cell_execute(self, cell_id, redirect_stdout=True, output_there=False):
        cell = self._find_cell(cell_id)
        cell.execute(redirect_stdout=redirect_stdout, output_there=output_there)
        return True

    def _cell_focus(self, cell_id, skip_anim=True):
        cell = self._find_cell(cell_id)
        cell.focus(skip_anim=skip_anim)
        return True

    def do_pprint(self, label, *args):
        if len(args) == 1 and type(args[0]) in (numpy.ndarray, ):
            sys.stdout.write("%s: \n" % label)
            pprint.pprint(args[0])
        else:
            sys.stdout.write("%s: " % label)
            if len(args) > 1:
                pprint.pprint(args)
            else:
                pprint.pprint(args[0])
                
    def pickle_save(self, fn, data):
        with open(fn, "wb") as fp:
            pickle.dump(data, fp, pickle.HIGHEST_PROTOCOL)
    def pickle_load(self, fn):
        with open(fn, "rb") as fp:
            data = pickle.load(fp)
        return data

    def get_coin_viewer(self, name):
        import pyutils.coin_viewer
        return pyutils.coin_viewer.get_coin_viewer(name)
    
    def reset(self):
        # reset notebook context
        self.outputs = dict()
        self._max_outputs = 50
        self.already_executed = set()
        self.module = imp.new_module(self.name)
        self.module.__dict__.update(dict(
            nb=self,
            out=self.outputs,
            sleep=self.sleep,
            cell=None, # currently executing cell
            cells=[], # stack of currently executing cells
            preempt=self.preempt,
            require_once=self.require_once,
            ln_unpack_log_data=self.ln_unpack_log_data,
            execute=self._cell_execute,
            focus=self._cell_focus,
            do_pprint=self.do_pprint,
            save=self.pickle_save,
            load=self.pickle_load,
            get_coin_viewer=self.get_coin_viewer
            ))
        self.module.__dict__.update(self.initial_context)
        self.module.__dict__.update(dict(
            pause=self.pause, # overwrite pyplot.pause
            stop=self._cell_stop, # overwrite pyplot.pause
            ))
        
    def derive_name(self):
        if hasattr(self, "name") and self.name in notebook.opened:
            old_name = self.name
        else:
            old_name = None
        if self.filename is not None:
            name = os.path.basename(self.filename)
        else:
            name = "unnamed notebook"
        already_in = notebook.opened.get(name)
        if already_in is not None and already_in != self:
            bn = name
            i = 1
            while True:
                name = "%s %d" % (bn, i)
                already_in = notebook.opened.get(name)
                if already_in is None or already_in == self:
                    break
                i += 1
        self.name = name
        if self.name != old_name or self.name not in notebook.opened:
            notebook.opened[self.name] = self
        if old_name is not None and old_name != self.name:
            del notebook.opened[old_name]
        
    def changed(self, is_changed=True, output_changed=False):
        if is_changed and output_changed and self._output_in_separate_file():
            return # ignore

        self.is_changed = is_changed
        if self.is_changed:
            self.do_highlight_search("")
            self.window.set_title("%s*" % self.name)
        else:
            self.window.set_title(self.name)

    def show(self):
        self.window.show()
    def present(self):
        self.window.present()
    def hide(self):
        self.window.hide()

    def highlight_cell_name(self, idx):
        m = self.cell_name_model
        iter = m.get_iter_first()
        while iter and m.iter_is_valid(iter):
            row = m[iter]
            if row[0] == idx:
                self.skip_cell_name_change = True
                self.cell_name_list.set_cursor(m.get_path(iter))
                self.skip_cell_name_change = False
                return
            iter = m.iter_next(iter)
        self.skip_cell_name_change = True
        self.cell_name_list.get_selection().unselect_all()
        self.skip_cell_name_change = False
        
    def insert_cell_name(self, idx, name):
        m = self.cell_name_model
        iter = m.get_iter_first()
        while iter and m.iter_is_valid(iter):
            row = m[iter]
            if row[0] == idx:
                m[iter][1] = name
                break
            if row[0] > idx:
                m.insert_before(iter, (idx, name))
                break
            iter = m.iter_next(iter)
        else:
            m.append((idx, name))
        self.have_cell_names = True
        self.update_cell_names_visibility()
        self.highlight_cell_name(idx)
        
    def remove_cell_name(self, idx):
        m = self.cell_name_model
        iter = m.get_iter_first()
        subs = 0
        while iter and m.iter_is_valid(iter):
            row = m[iter]
            if row[0] == idx:
                if not m.remove(iter):
                    return
                subs += 1
            m[iter][0] -= subs
            iter = m.iter_next(iter)
    
    def update_cell_names(self):
        self.skip_cell_name_change = True

        # fetch cell names
        cell_names = []
        for idx, cell in enumerate(self.cells):
            name = cell._get_cell_name()
            if name:
                if not hasattr(cell, "name") or cell.name != name:
                    cell.name = name
                    self.insert_cell_name(idx, name)
                cell_names.append((idx, cell.name, ))
                
        self.have_cell_names = len(cell_names) > 0

        # check/update treeview
        m = self.cell_name_model
        iter = m.get_iter_first()
        i = 0
        while iter:
            if i >= len(cell_names):
                if not m.remove(iter):
                    break
                continue
            required = cell_names[i]
            if m[iter][0] < required[0]:
                if not m.remove(iter):
                    break
                continue
            if m[iter] == required:
                iter = m.iter_next(iter)
                i += 1
                continue
            if m[iter][0] > required[0]:
                new_iter = m.insert_before(sibling=iter)
                m[new_iter] = required
                i += 1
                continue
            # id is equal
            m[iter][1] = required[1]
            i += 1
            iter = m.iter_next(iter)

        while i < len(cell_names):
            m.append(cell_names[i])
            i += 1
        
        self.update_cell_names_visibility()
        self.skip_cell_name_change = False
        
    def update_cell_names_visibility(self):
        if self.have_cell_names and not self.cell_names_visible:
            self.hpaned.pack2(self.cell_name_sw, False, False)
            def update_hpaned():
                position = self.saved_separator_position or self.hpaned.get_allocated_width() - 120
                self.hpaned.set_position(position)
                return False
            GLib.idle_add(update_hpaned)
            self.cell_name_sw.show_all()
            self.cell_names_visible = True
        elif not self.have_cell_names and self.cell_names_visible:
            self.hpaned.remove(self.cell_name_sw)
            self.cell_names_visible = False

    def cell_name_cursor_change(self, tv):
        if self.skip_cell_name_change:
            return True
        path, col = tv.get_cursor()
        if not path:
            return False
        m = self.cell_name_model        
        row = m[path]
        idx = row[0]
        self.cells[idx].focus()
        return True
        
    def new_cell_at(self, text="", index=-1, update_names=True, expanded=True):
        cell = notebook_cell(self, text=text, expanded=expanded, font=self.settings.get("font"))
        
        self.table.resize(2 * (len(self.cells) + 1), 2)
        
        if index == -1:
            self.cells.append(cell)
            new_index = len(self.cells) - 1
        else:
            self.cells.insert(index, cell)
            new_index = index
            # do shift other cells
            for idx in range(new_index+1, len(self.cells)):
                self.cells[idx].reattach(idx)

        # attach new cell
        cell.attach(new_index)

        if update_names:
            self.update_cell_names()
        return cell
    new_cell = new_cell_at

    def delete_cell(self, cell, update_names=True, by_backspace=False):
        idx = self.cells.index(cell)
        if hasattr(cell, "name"):
            self.remove_cell_name(idx)
        cell.deattach()
        del self.cells[idx]
        if by_backspace:
            if idx > 0:
                next_idx = idx - 1
            else:
                next_idx = idx
        else:
            if idx == len(self.cells):
                next_idx = idx - 1
            else:
                next_idx = idx
        # do shift other cells
        for idx in range(idx, len(self.cells)):
            self.cells[idx].reattach(idx)
        if update_names:
            self.update_cell_names()
        return next_idx
    
    def load(self, fn, redirect_stdout=True):
        autoexec_cells = []

        if self.create_file and not os.path.isfile(fn):
            return
        
        if self.storage == "filesystem":
            with open(fn, "rb") as fp:
                data = fp.read().decode("utf-8")
        else:
            data = self.storage.read(fn)
        
        state = None
        nb_props = []
        for line in data.rstrip("\n").split("\n"):
            line += "\n"

            if state is None:
                m = re.match("## cell ([0-9]+) input ##\n", line)
                if m is not None:
                    state = "input"
                    cell_id = m.group(1)
                    cell_input = []
                    cell_output = []
                    is_collapsed = False
                    continue
                m = re.match("## nb properties ##\n", line)
                if m is not None:
                    state = "nb_props"
                    continue
                continue
            if state == "nb_props":
                m = re.match("## nb properties end ##\n", line)
                if m is not None:
                    state = None
                    continue
                nb_props.append(line)
                continue
            if state == "input":
                if line == "## collapsed\n":
                    is_collapsed = True
                    continue
                m = re.match("## cell ([0-9]+) output ##\n", line)
                if m is not None:
                    state = "output"
                    continue
                m = re.match("## cell ([0-9]+) end ##\n", line)
                if m is None:
                    cell_input.append(line)
                    continue
                # cell finished
            if state == "output":
                m = re.match("## cell ([0-9]+) end ##\n", line)
                if m is None:
                    if line.startswith("# "):
                        line = line[2:]
                    cell_output.append(line)
                    continue
                # cell finished
            # finish cell
            state = None
            cell_source = ("".join(cell_input))[:-1]
            cell = self.new_cell(text=cell_source, update_names=False, expanded=not is_collapsed)
            if cell_output:
                cell.set_output_text("".join(cell_output[:-1]))

            lines = cell_source.split("\n")
            if list([line for line in lines if line.startswith("## ") and "autoexec" in line]):
                autoexec_cells.append(cell)
            names = list([line for line in lines if line.startswith("## ") and "name:" in line])
            if names:
                cell.name = names[0].split("name:", 1)[1].strip()
        nb_props = "".join(nb_props)
        if nb_props:
            nb_props = eval(nb_props)
        else:
            nb_props = dict()
        nb_props.update(self._try_load_user_props(fn))
        if "window_size" in nb_props:
            self.window.resize(*nb_props["window_size"])
        if "window_position" in nb_props:
            self.window.move(*nb_props["window_position"])
        self.saved_separator_position = nb_props.get("separator_position", None)
        ofn = fn + ".out"
        output_in_separate_file = nb_props.get("output_in_separate_file", False)
        if self.out_in_separate_file_tb is not None:
            self.out_in_separate_file_tb.set_active(output_in_separate_file)
        if output_in_separate_file and os.path.isfile(ofn):
            is_first = True
            i = 0
            def set_cell(i, cell_out):
                cell_out = "".join(cell_out)[:-1] # strip last trailing nl
                self.cells[i].set_output_text(cell_out)
            with open(ofn, "r") as fp:
                for line in fp:
                    if line.startswith("#" + notebook.next_cell_prefix):
                        line = line[1:]
                    elif line.startswith(notebook.next_cell_prefix):
                        if is_first:
                            is_first = False
                        else:
                            set_cell(i, cell_out)
                            i += 1
                            if i == len(self.cells):
                                break
                        cell_out = []
                        continue
                    cell_out.append(line)
                else:
                    set_cell(i, cell_out)
        self.filename = fn
        self.changed(False)
        
        for cell in autoexec_cells:
            cell.execute(redirect_stdout=redirect_stdout)

        if "current_cell" in nb_props and 0 <= nb_props["current_cell"] < len(self.cells):
            self.current_cell = self.cells[nb_props["current_cell"]]
            self.current_cell.focus(skip_anim=True)
        self.update_cell_names()
            
    def _output_in_separate_file(self):
        if self.out_in_separate_file_tb is None:
            return False
        return self.out_in_separate_file_tb.get_active()

    def save(self, ask_for_filename=False):
        self.update_cell_names()
        self.highlight_cell_name(self.cells.index(self.current_cell))
        if self.storage == "filesystem" and (self.filename is None or ask_for_filename):
            dlg = Gtk.FileChooserDialog(
                "save notebook", parent=self.window,
                action=Gtk.FileChooserAction.SAVE,
                buttons=(
                    Gtk.STOCK_CANCEL, Gtk.ResponseType.REJECT,
                    Gtk.STOCK_OK, Gtk.ResponseType.ACCEPT)
                    )
            if self.filename is not None:
                dn = os.path.dirname(self.filename)
            else:
                dn = os.getcwd()
            dlg.set_current_folder(os.path.abspath(dn))
            dlg.show()
            r = dlg.run()
            dlg.hide()
            if r == Gtk.ResponseType.REJECT:
                return False
            fn = dlg.get_filename()
        else:
            fn = self.filename
        if not os.path.isfile(fn) and self.create_file:
            dn = os.path.dirname(os.path.realpath(fn))
            if not os.path.isdir(dn):
                os.makedirs(dn)

        nb_properties = dict(
            window_size=self.window_size,
            window_position=self.window_position,
            separator_position=self.hpaned.get_position() if self.cell_names_visible else None,
            current_cell=self.cells.index(self.current_cell),
            output_in_separate_file=self._output_in_separate_file(),
            )

        fp = StringIO()

        # split nb_properties into user- & non-user- properties
        # user-properties are not stored in the notebook-file!
        non_user_props = dict(nb_properties)
        user_props = dict()
        for prop in "current_cell,window_position,window_size,separator_position".split(","):
            value = non_user_props.pop(prop, None)
            if value is not None:
                user_props[prop] = value
        fp.write("## nb properties ##\n%s\n## nb properties end ##\n" % (
            pprint.pformat(non_user_props)))

        out_texts = []
        for i, cell in enumerate(self.cells):
            in_text = cell.get_input_text()
            if not cell.get_expanded():
                in_text = "## collapsed\n" + in_text
            fp.write("## cell 0 input ##\n%s" % in_text)
            out_text = cell.get_output_text()
            if self._output_in_separate_file():
                name = cell._get_cell_name()
                out_text = out_text.split("\n")
                out_text = "\n".join(["#" + line if line.startswith(notebook.next_cell_prefix) else line for line in out_text])
                if name:
                    out_texts.append("%s: %s\n%s\n" % (notebook.next_cell_prefix, name, out_text))
                else:
                    out_texts.append("%s\n%s\n" % (notebook.next_cell_prefix, out_text))
            elif out_text:
                fp.write("\n## cell 0 output ##\n# %s" % (out_text.replace("\n", "\n# ")))
            fp.write("\n## cell 0 end ##\n")
        data = fp.getvalue()

        if self.storage == "filesystem":
            if sys.version_info[0] >= 3:
                data_bytes = data.encode("utf-8")
            else:
                data_bytes = data
            with open(fn, "wb") as fp:
                fp.write(data_bytes)
                fp.close()
            if self._output_in_separate_file():
                out_texts = "".join(out_texts)
                with open(fn + ".out", "w") as fp:
                    fp.write(out_texts)
            self._try_store_user_props(fn, user_props)
        else:
            self.storage.save(fn, data)
        
        self.filename = fn
        self.derive_name()
        self.changed(False)
        return True
            
    def _get_user_props_fn(self, fn):
        fn_hd = hashlib.sha3_256(fn.encode("utf-8")).hexdigest()
        up_fn = os.path.expanduser(os.path.join("~", ".pyutils_notebooks", "%s_%s" % (fn_hd, os.path.basename(fn))))
        return up_fn

    def _try_store_user_props(self, fn, user_props):
        upfn = self._get_user_props_fn(fn)
        updn = os.path.dirname(upfn)
        if not os.path.isdir(updn):
            try:
                os.makedirs(updn)
            except Exception:
                return
        try:
            with open(upfn, "w") as fp:
                fp.write(pprint.pformat(user_props))
        except Exception:
            pass

    def _try_load_user_settings(self):
        usfn = os.path.expanduser(os.path.join("~", ".pyutils_notebooks", "global_settings"))
        self.settings = dict()
        if os.path.isfile(usfn):
            with open(usfn, "r") as fp:
                self.settings.update(eval(fp.read()))

    def _try_load_user_props(self, fn):
        upfn = self._get_user_props_fn(fn)
        if not os.path.isfile(upfn):
            return dict()
        try:
            with open(upfn, "r") as fp:
                return eval(fp.read())
        except Exception:
            return dict()

    def on_delete(self, w, ev):
        if self.is_changed:
            dlg = Gtk.MessageDialog(
                self.window,
                Gtk.DialogFlags.MODAL,
                Gtk.MessageType.QUESTION,
                Gtk.ButtonsType.NONE,
                "notebook is changed. do you want to save before exiting?")
            dlg.add_buttons(
                Gtk.STOCK_CANCEL, Gtk.ResponseType.REJECT,
                Gtk.STOCK_NO, Gtk.ResponseType.NO,
                Gtk.STOCK_YES, Gtk.ResponseType.YES)
            dlg.show_all()
            r = dlg.run()
            dlg.hide()
            if r == Gtk.ResponseType.REJECT:
                return True
            if r == Gtk.ResponseType.YES:
                self.save()
        self.window.hide()
        
        if self.on_hide is None:
            # already remove nb from list of registered
            if self.name in notebook.opened:
                del notebook.opened[self.name]
        
        if notebook.have_main and len(notebook.opened) == 0:
            Gtk.main_quit()
        
        if self.on_hide is not None:
            self.on_hide() # leave nb removal to parent
        
        return True

    def run(self):
        notebook.have_main = True
        return Gtk.main()

    def on_configure(self, widget, ev):
        self.window_size = tuple(widget.get_size())
        self.window_position = tuple(widget.get_position())
        return False

    def open_notebook(self):
        # ask for filename
        dlg = Gtk.FileChooserDialog(
            "save notebook", parent=self.window,
            action=Gtk.FileChooserAction.OPEN,
            buttons=(
                Gtk.STOCK_CANCEL, Gtk.ResponseType.REJECT,
                Gtk.STOCK_OK, Gtk.ResponseType.ACCEPT)
                )
        if self.filename is not None:
            dn = os.path.dirname(self.filename)
        else:
            dn = os.getcwd()
        dlg.set_current_folder(os.path.abspath(dn))
        dlg.show()
        r = dlg.run()
        dlg.hide()
        if r == Gtk.ResponseType.REJECT:
            return True
        fn = dlg.get_filename()
        # create new notebook
        nb = notebook(filename=fn)
        nb.show()
        return nb

    def new_notebook(self):
        nb = notebook()
        nb.show()
        return nb

    def move_cell_up(self):
        idx = self.cells.index(self.current_cell)
        if idx > 0:
            self.cells[idx - 1].focus()
    def move_cell_down(self):
        idx = self.cells.index(self.current_cell)
        if idx + 1 < len(self.cells):
            self.cells[idx + 1].focus()
    def insert_cell_up(self):
        idx = self.cells.index(self.current_cell)
        cell = self.new_cell_at(index=idx)
        cell.focus()
    def insert_cell_down(self):
        idx = self.cells.index(self.current_cell)
        cell = self.new_cell_at(index=idx+1)
        cell.focus()
        
    def run_cell(self, skip_to_next=False):
        self.current_cell.execute()
        if skip_to_next:
            self.move_cell_down()
    def stop_cell(self):
        self.stop_request += 1

    def insert_help_cell(self):
        examples = [
"""# key bindings:
#   Ctrl-Enter             execute current cell
#   Ctrl-Shift-Enter       execute current cell and jump to next cell 
#                          (possibly creating a new one)
#   Ctrl-{Up, Down}        jump cell up or down
#   Ctrl-Shift-{Up, Down}  insert cell above or below
#   Backspace              when cell is empty: delete cell
#   Ctrl-l                 show line numbers
#   Ctrl-s                 save
#   Ctrl--                 "-" split cell at current position
#
# magic commands:
#   lines starting with % are transformed into a print-line.
#     example:
#     % 3*3
#     will output:
#     3*3: 9
#
#   cells which contain a magic line like this:
#     ## autoexec
#   will be automatically executed once on notebook load.
#
# usefull notebook specifiy stuff in cell environment:
#   preempt()              check whether user pressed stop
#   sleep(t)               sleep for t seconds (user can press stop)
#   pause(msg)             pause execution, waiting for user to press button. `msg` is optional.
#   execute(cell-id)       execute another cell, specified by cell-id (cell name or index)
#   stop()                 stop execution of this and calling cells.
#   cell_id                id of currently executing cell
#   cell                   currently executing cell
#   nb                     current notebook
#   cells                  list/stack of executing cells
#   out[exec-id]               output number idx (see left column for exec-id, default max 50 back)
""",

            """% 3*3
% cell
% nb
% cells
% out[1]""",

"""print "Hello World - verbose integration!"
% app
% page
% viewer
% item
""",

"""# numpy and pyplot ready to use:
y = random.random((100, ))
clf()
grid(True)
plot(y)
""",
]
        for i, text in enumerate(examples):
            cell = self.new_cell_at(index=i, text=text, update_names=False)
            if i == 0:
                cell.focus()
        self.update_cell_names()

    vscroll_rate = 50
    def _vscroll_to(self, new_target, skip_anim=False):
        self._vscroll_target = new_target
        #print("target", new_target)
        if self.no_scroll_anim or skip_anim:
            vadj = self.sw.get_vadjustment()
            vadj.set_value(self._vscroll_target)
            if self._vscroll_timeout is not None:
                self._vscroll_last_current = None
                GLib.source_remove(self._vscroll_timeout)
                self._vscroll_timeout = None
            return

        if self._vscroll_timeout is None:
            if not hasattr(self, "_vscroll_current_speed"):
                self._vscroll_max_speed = 30000 # px/s
                self._vscroll_max_acc   = 10000 # px/s^2
                self._vscroll_current_speed = 0
                self._vscroll_last_current = None
            self._vscroll_timeout = GLib.timeout_add(int(1000 / self.vscroll_rate), self._vscroll)

    def _vscroll(self):
        vadj = self.sw.get_vadjustment()
        current = vadj.get_value()
        if self._vscroll_last_current is not None and current == self._vscroll_last_current:
            #print("can not scroll further. stop it!")
            self._vscroll_target = current
        delta = self._vscroll_target - current
        if delta > 0:
            self._vscroll_current_speed += self._vscroll_max_acc / self.vscroll_rate
            if self._vscroll_current_speed > self._vscroll_max_speed:
                self._vscroll_current_speed = self._vscroll_max_speed
        else:
            self._vscroll_current_speed -= self._vscroll_max_acc / self.vscroll_rate
            if self._vscroll_current_speed < -self._vscroll_max_speed:
                self._vscroll_current_speed = -self._vscroll_max_speed
        speed_step = self._vscroll_current_speed / self.vscroll_rate
        if abs(speed_step) > abs(delta):
            speed_step = delta
        self._vscroll_last_current = current
        current += speed_step
        vadj.set_value(current)
        target_reached = abs(self._vscroll_target - current) < 1e-3
        if target_reached:
            self._vscroll_timeout = None
            self._vscroll_current_speed = 0
            self._vscroll_last_current = None
            return False
        return True

    def _get_cell_stack(self, use_traceback=False):
        """
        return most recent cell location first!
        """
        if use_traceback:
            stack = traceback.extract_tb(sys.exc_info()[2])
        else:
            stack = traceback.extract_stack()
        for fs in stack[::-1]:
            if not fs.filename.startswith("<cell"):
                continue
            cell_id = fs.filename.split("[", 1)[1].split("]", 1)[0]
            that_cell = None
            if cell_id.isdigit():
                try:
                    that_cell = self._find_cell(int(cell_id))
                except:
                    that_cell = None
            if that_cell is None:
                try:
                    that_cell = self._find_cell(cell_id)
                except:
                    continue
            yield that_cell, fs.lineno

    def _cell_stack_move_play_markers(self):
        first = True
        for that_cell, lineno in self._get_cell_stack(use_traceback=False):
            sv = that_cell.sv
            b = sv.get_buffer()
            line_iter = b.get_iter_at_line(lineno - 1)
            b.remove_source_marks(b.get_start_iter(), b.get_end_iter(), "play_marker") # UUGGLY!
            b.remove_source_marks(b.get_start_iter(), b.get_end_iter(), "stack_marker") # UUGGLY!
            if first:
                b.create_source_mark("play", "play_marker", line_iter)
                first = False
            else:
                b.create_source_mark("play", "stack_marker", line_iter)

    def _cell_stack_add_markers(self, category, name=None, category_after_first=None, use_traceback=False):
        if category_after_first is None:
            category_after_first = category
        record_stack = category in ("error_marker", "stack_marker") or category_after_first == "stack_marker"
        stack_down = None
        marks = []
        added_markers = []
        for that_cell, lineno in self._get_cell_stack(use_traceback=use_traceback):
            sv = that_cell.sv
            b = sv.get_buffer()
            mark = b.create_source_mark(name, category, b.get_iter_at_line(lineno - 1))
            if record_stack:
                if len(marks) > 0:
                    marks[-1][-1] = that_cell, lineno
                marks.append([(that_cell, mark), stack_down, None])
                stack_down = that_cell, lineno
            added_markers.append((that_cell, b))
            if category == "error_marker" and use_traceback:
                that_cell._remember_error(mark, str(sys.exc_info()[1]))
            category = category_after_first
        if record_stack:
            for (this_cell, this_mark), stack_down, stack_up in marks:
                this_cell._remember_stack_marker(this_mark, stack_down, stack_up)
        return added_markers

    def _remove_markers(self, cell_buffers, category):
        for cell, b in cell_buffers:
            b.remove_source_marks(b.get_start_iter(), b.get_end_iter(), category)
            if category in ("stack_marker", "error_marker") and cell._stack_markers:
                cell._stack_markers = dict()
                cell._error_mark = None

    def _remove_error_markers(self):
        if not self._buffers_with_error_markers:
            return
        self._remove_markers(self._buffers_with_error_markers, "error_marker")
        self._remove_markers(self._buffers_with_error_markers, "stack_marker")
        self._buffers_with_error_markers = []

    def _display_error_markers(self):
        self._buffers_with_error_markers = self._cell_stack_add_markers("error_marker", category_after_first="stack_marker", use_traceback=True)

    def pause(self, text="click to continue..."):
        cell = self.module.cell
        def on_clicked(button):
            cell._paused = False
        def do_scroll():
            sw_rect = self.sw.get_allocation()
            rect = cell.gui.pause_btn.get_allocation()
            target = int(rect.y + 1.1*rect.height - sw_rect.height)
            vadj = self.sw.get_vadjustment()
            if target > vadj.get_value():
                self._vscroll_to(target, skip_anim=True)
            return False
        if "pause_btn" not in cell.gui:
            cell.gui.pause_btn = Gtk.Button(text)
            cell.output_widget.pack_start(cell.gui.pause_btn, False, False, 0)
        else:
            cell.gui.pause_btn.set_label(text)
        cell._paused = True
        cell.gui.pause_btn.connect("clicked", on_clicked)
        cell.gui.pause_btn.show()
        self._cell_stack_move_play_markers()
        focussed_before = self.current_cell
        cell.gui.pause_btn.grab_focus()
        GLib.timeout_add(50, do_scroll)
        def undo():
            cell.gui.pause_btn.hide()
        try:
            while cell._paused:
                self.sleep(0.05, show_play_marker=False)
        finally:
            undo()
        focussed_before.sv.grab_focus()

    def _cell_stop(self, cell_return=False):
        sr = self._get_stop_request_at_start()
        self.stop_cell()
        self._raise_on_stop_request(sr, "code" + "(cell_return)" if cell_return else "user")


if __name__ == "__main__":
    nb = None
    for nb in sys.argv[1:]:
        nb = notebook(filename=nb)
        nb.show()
    if nb is None:
        nb = notebook()
        nb.show()
    nb.run()
