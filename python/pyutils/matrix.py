"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt
"""

from __future__ import print_function

import sys
import math
#from math import asin, acos, sin, cos, pi

#import cvxopt
#from cvxopt.base import matrix
#from cvxopt.lapack import getri, getrf
#cvxopt.base.print_options["dformat"] = "-8.6f"

import numpy as np
np.set_printoptions(linewidth=140, precision=4, suppress=True)

eps = 1e-8

#def inv(M):
#   A = matrix(M)
#   ipiv = matrix(0, (min(*A.size), 1))
#   getrf(A, ipiv)
#   getri(A, ipiv)
#   return A

#def zeros(m, n=None):
#   if n is None: n = m
#   return matrix(0., (m, n))

#def eye(n, m=None):
#   A = zeros(n, m)
#   for d in xrange(min(*A.size)):
#       A[d, d] = 1;
#   return A

#def norm(a, ord=2):
#   return numpy.linalg.norm(a, ord)

#def vector_norm(a):
#   if a.size[0] == 1:
#       r = a * a.trans()
#       r = r[0]
#       return math.sqrt(r)
#   elif a.size[1] == 1:
#       r = a.trans() * a
#       r = r[0]
#       return math.sqrt(r)
#   raise "only know of vector norm"

def setZyxFixed(data, x, y, z):
    sx = math.sin(x)
    cx = math.cos(x)
    sy = math.sin(y)
    cy = math.cos(y)
    sz = math.sin(z)
    cz = math.cos(z)
    czsy = cz*sy
    szsy = sz*sy
    # register real* d = &matrix[0][0]
    data[0, :3] = [ cz*cy,          -sz*cy,          sy    ]
    data[1, :3] = [ sz*cx+czsy*sx,  cz*cx-szsy*sx,  -cy*sx ]
    data[2, :3] = [ sz*sx-czsy*cx,  cz*sx+szsy*cx,   cy*cx ]

def setXyzMoving(data, x, y, z):
    return setZyxFixed(data, x, y, z)
    
def getXyzMoving(data):
    # register const real* data = &matrix[0][0];
    #  0 1 2
    #  3 4 5
    #  6 7 8
    if abs( data[0, 2] + 1.0 ) < eps:
        x = 0
        y = -np.pi/2
        z = math.atan2( data[1, 0], data[1, 1] )
    elif abs( data[0, 2] - 1.0 ) < eps:
        x = 0
        y = np.pi/2
        z = math.atan2(  data[1, 0], data[1, 1] )
    else:
        x = math.atan2( -data[1, 2], data[2, 2] )
        y = math.atan2(  data[0, 2], math.sqrt( data[1, 2] * data[1, 2] + data[2, 2] * data[2, 2] ) )
        z = math.atan2( -data[0, 1], data[0, 0] )
    return x, y, z

"""
0.5, z_rot=5)
tr(x=0.5, z_rot_deg=5, x_rot=pi)
tr(x=0.5, z_rot=5)
tr_xyzm(x=0.5, z_rot=5, x_rot=10)
tr(rot=5, axis=(1, 0, 0))
"""

def r2d(r):
    return r / np.pi * 180.

def hom(a, scale=1., hom_el=1):
    size = a.size[0] + 1, a.size[1]
    m = np.matrix(0., size)
    m[size[0]-1] = hom_el
    m[:size[0]-1] = a * scale
    return m

def rotx(theta):
    # rad
    cs = math.cos(theta)
    si = math.sin(theta)
    return np.array([
         [1.,  0.,  0., 0.],
         [0.,  cs, -si, 0.],
         [0.,  si,  cs, 0.],
         [0.,  0.,  0., 1.]
        ], dtype=np.float64)

def roty(theta):
    # rad
    cs = math.cos(theta)
    si = math.sin(theta)
    return np.array([
        [ cs,  0.,  si, 0.],
        [ 0.,  1.,  0., 0.],
        [-si,  0.,  cs, 0.],
        [ 0.,  0.,  0., 1.]
        ], dtype=np.float64)

def rotz(theta):
    # rad
    cs = math.cos(theta)
    si = math.sin(theta)
    return np.array([
         [cs, -si,  0., 0.],
         [si,  cs,  0., 0.],
         [0.,  0.,  1., 0.],
         [0.,  0.,  0., 1.]
        ], dtype=np.float64)

def txyz(x, y, z):
    # rad
    return np.array([
         [1.,  0.,  0., x],
         [0.,  1.,  0., y],
         [0.,  0.,  1., z],
         [0.,  0.,  0., 1.]
        ], dtype=np.float64)

def tr2angle_axis(T):
    # [axis, angle] = tr2angle_axis(T)  
    # new 21.03.00 cb. code from
    # Meyberg, Vachenauer - Hoehere Mathematik 1, p. 319
    # 
    pi = math.pi
    
    # first step is to calculate positive angle
    cs = 0.5 * (T[0:3, 0:3].trace() - 1.)
    if cs <= -1:
        angle = pi
    elif cs < 1:
        angle = math.acos(cs)
    else:
        angle = 0

    if abs(abs(angle) - np.pi) < 10 * eps:
        if abs(T[0, 0] - 1) < abs(T[1, 1] - 1) and abs(T[0, 0] - 1) < abs(T[2, 2] - 1):
            axis = np.array([ 1.0, 0.0, 0.0 ])
        elif abs(T[1, 1] - 1) < abs(T[0, 0] - 1) and abs(T[1, 1] - 1) < abs(T[2, 2] - 1):
            axis = np.array([ 0.0, 1.0, 0.0 ])
        elif abs(T[2, 2] - 1) < abs(T[0, 0] - 1) and abs(T[2, 2] - 1) < abs(T[1, 1] - 1):
            axis = np.array([ 0.0, 0.0, 1.0 ])
    elif abs(angle) > 10 * eps:
        # next is to determine axis and direction of axis
        axis = np.array([
            T[2, 1] - T[1, 2],
            T[0, 2] - T[2, 0],
            T[1, 0] - T[0, 1]
        ])
        # next is to norm axis
        #factor = 1 / (2.0 * sin(angle));
        #axis = factor * axis;
        axis = axis / np.linalg.norm(axis)
    else:
        # Angle is zero, matrix is the identity, no unique axis, so
        # return (1,0,0) for as good a guess as any.
        axis = np.array([ 1.0, 0.0, 0.0 ])
        
    return angle, axis

def angle_axis2tr(angle, axis, assertion=True):
    if assertion:
        assert abs(np.linalg.norm(axis) - 1) < eps
    x, y, z = axis[0:3]
    s = math.sin(angle)
    c = math.cos(angle)
    
    v = 1 - c
    xyv = x * y * v
    yzv = y * z * v
    xzv = x * z * v
    
    return np.array([
        x*x*v + c,  xyv - z*s,  xzv + y*s, 0,
        xyv + z*s,  y*y*v + c,  yzv - x*s, 0,
        xzv - y*s,  yzv + x*s,  z*z*v + c, 0,
        0, 0, 0, 1]).reshape(4, 4)


def tr2diff(t1, t2=None):
    """
    taken from matlab robotic toolbox
    TR2DIFF Convert a transform difference to differential representation

    D = TR2DIFF(T)
    D = TR2DIFF(T1, T2)

    First form converts a homogeneous transform representing an
    infinitessimal motion to a 6-element differential representation.
    Such a homogeneous transform has a rotational submatrix that is,
    approximately, skew symmetric.
    
    Second form returns the 6-element differential motion required to move
    from T1 to T2 in base coordinates.
    
    See also: DIFF2TR.
    Copyright (C) 1993-2002, by Peter I. Corke
    MOD.HISTORY
    1/95    take mean of both values in the skew-symmetric matrix
    2/95    add two argument version as part of ikine() revamp
    $Log: tr2diff.m,v $
    Revision 1.3  2002/04/14 11:01:23  pic
    Updated see also line.
    
    Revision 1.2  2002/04/01 11:47:18  pic
    General cleanup of code: help comments, see also, copyright, remnant dh/dyn
    references, clarification of functions.
    
    $Revision: 21510 $
    """
    d = np.empty((6, ))
    if t2 is None:
        d[0:3] = t1[0:3, 3]
        d[3:6] = [
            t1[2, 1] - t1[1, 2],
            t1[0, 2] - t1[2, 0],
            t1[1, 0] - t1[0, 1]]
    else:
        d[0:3] = t2[0:3, 3] - t1[0:3, 3]
        d[3:6] = (
            np.cross(t1[0:3, 0], t2[0:3, 0]) +
            np.cross(t1[0:3, 1], t2[0:3, 1]) +
            np.cross(t1[0:3, 2], t2[0:3, 2]))
    d[3:6] = 0.5 * d[3:6]
    return d


def diff2tr(d):
    return np.array([
        [ 0,    -d[5],  d[4], d[0] ],
        [ d[5],  0,    -d[3], d[1] ],
        [-d[4],  d[3],  0,    d[2] ],
        [ 0,     0,     0,    0    ]])

if __name__ == "__main__":
    a = np.array([[1, 2],[3.123456, 4]])
    print(a)
    print(type(a))
    print(a.shape)
    print(a.size)
    print(a.dtype)
    print(a.itemsize)
    print(repr(a.data))
    sys.exit(0)
    
    angles = np.matrix([0, 0, 0, 0, 180, 60]) / 180. * np.pi
    print(angles)

    sys.exit(0)
    
    a = np.matrix([
          [0.9501,    0.8913,    0.8214,    0.9218],
          [0.2311,    0.7621,    0.4447,    0.7382],
          [0.6068,    0.4565,    0.6154,    0.1763],
          [0.4860,    0.0185,    0.7919,    0.4057]]).trans()
    print(a)
    print(np.linalg.norm(a, ord=2))
    print(a[0:3, 0])
    print(np.linalg.norm(a[0:3, 0], ord=2))
    
    sys.exit(0)
    top_in = [1, 2, 3, 0.5, 0.7, 0.9]
    print(top_in)


def print_matrices_aside(*args):
    np.set_printoptions(linewidth=140, precision=4, suppress=True)
    delim = " | "
    delim = " "
    mat_names = None
    mats = []
    mlc = 0
    for i, mat in enumerate(args):
        if type(mat) == tuple:
            mat_name, mat = mat
            if mat_names is None:
                mat_names = [""] * len(args)
            mat_names[i] = mat_name
        lines = str(mat).split("\n")
        lc = len(lines)
        if lc > mlc:
            mlc = lc
        ml = np.amax([len(l) for l in lines])
        mats.append((lines, ml, lc))

    if mat_names is not None:
        for mi, (mat, ml, lc) in enumerate(mats):
            if mi != 0:
                sys.stdout.write(delim)
            sys.stdout.write("%-*.*s" % (ml, ml, mat_names[mi]))
        sys.stdout.write("\n")
    for line_no in range(mlc):
        for mi, (mat, ml, lc) in enumerate(mats):
            if line_no >= lc:
                continue
            if mi != 0:
                sys.stdout.write(delim)
            sys.stdout.write("%-*.*s" % (ml, ml, mat[line_no]))
        sys.stdout.write("\n")
        
def check_mat(title, mat):
    print(title)
    print(mat)
    print("col1", np.linalg.norm(mat[0:3, 0]), "col2", np.linalg.norm(mat[0:3, 1]), "col3", np.linalg.norm(mat[0:3, 2]))
    print("1-2", np.dot(mat[0:3, 0], mat[0:3, 1]), "2-3", np.dot(mat[0:3, 1], mat[0:3, 2]), "1-3", np.dot(mat[0:3, 0], mat[0:3, 2]))

def norm_frame_old(f):
    o = np.array(f)
    x = f[0:3, 0]
    y = f[0:3, 1]
    z = f[0:3, 2]

    z = np.cross(x, y)
    y = np.cross(z, x)

    f[0:3, 0] = x / np.linalg.norm(x)
    f[0:3, 1] = y / np.linalg.norm(y)
    f[0:3, 2] = z / np.linalg.norm(z)
    return f


def normalize_matrix(T):
    T[0:3, 2] = np.cross(T[0:3, 0], T[0:3, 1])
    T[0:3, 2] = T[0:3, 2] / np.linalg.norm(T[0:3, 2])

    T[0:3, 1] = np.cross(T[0:3, 2], T[0:3, 0])
    T[0:3, 1] = T[0:3, 1] / np.linalg.norm(T[0:3, 1])

    T[0:3, 0] = np.cross(T[0:3, 2], T[0:3, 1])
    T[0:3, 0] = -T[0:3, 0] / np.linalg.norm(T[0:3, 0])
    return T

norm_frame = normalize_matrix


def tr2rodrigues(tr):
    # taken from camera calibration toolbox for matlab
    # http://www.vision.caltech.edu/bouguetj/calib_doc/
    # % RODRIGUES   Transform rotation matrix into rotation vector and viceversa.
    # %     
    # %     Sintax:  [OUT]=RODRIGUES(IN)
    # %         If IN is a 3x3 rotation matrix then OUT is the
    # %     corresponding 3x1 rotation vector
    # %         if IN is a rotation 3-vector then OUT is the 
    # %     corresponding 3x3 rotation matrix
    # %
    # %%
    # %%        Copyright (c) March 1993 -- Pietro Perona
    # %%        California Institute of Technology
    # %%
    # %% ALL CHECKED BY JEAN-YVES BOUGUET, October 1995.
    # %% FOR ALL JACOBIAN MATRICES !!! LOOK AT THE TEST AT THE END !!
    # %% BUG when norm(om)=pi fixed -- April 6th, 1997;
    # %% Jean-Yves Bouguet
    # %% Add projection of the 3x3 matrix onto the set of special ortogonal matrices SO(3) by SVD -- February 7th, 2003;
    # %% Jean-Yves Bouguet
    # % BUG FOR THE CASE norm(om)=pi fixed by Mike Burl on Feb 27, 2007
    m, n = tr.shape
    bigeps = 10e20 * eps

    R = tr[0:3, 0:3]
    # project the rotation matrix to SO(3);
    U, S, V = np.linalg.svd(R)
    R = np.dot(U, V.transpose()) # todo: really transpose?

    tr = (np.trace(R) - 1) / 2
    theta = np.acos(tr)

    if np.sin(theta) >= 1e-4:
        return 1 / (2 * np.sin(theta)) * np.array([
            R[2, 1] - R[1, 2],
            R[0, 2] - R[2, 0],
            R[1, 0] - R[0, 1]]) * theta
    
    if tr > 0: # case norm(om)=0;
        return np.zeros((1, 3))
    
    #case norm(om)=pi;
    raise Exception("todo")
    #return theta * (sqrt((diag(R) + 1) / 2) .* [1 ; 2 * (R(1,2:3) >= 0)' - 1]);

def rodrigues2tr(omc):
    raise Exception("todo: test")
    if len(omc.shape) != 1:
        raise ValueError("not a vector: %r" % omc)
    if omc.shape[0] != 3:
        raise ValueError("not a rotation vector: %r" % omc)
    #m, n = omc.shape
    theta = np.linalg.norm(omc)
    if theta < eps: return np.eye(4)
    omega = omc / theta
    alpha = np.cos(theta)
    beta = np.sin(theta)
    gamma = 1 - np.cos(theta)
    omegav = np.array([
        [        0, -omega[2],  omega[1]],
        [ omega[2],         0, -omega[0]],
        [-omega[1],  omega[0],         0]])
    # assume scalar as result
    A = np.mat(omega) * np.mat(omega).transpose()
    R = np.eye(4)
    R[0:3, 0:3] = np.eye(3) * alpha + omegav * beta + A * gamma
    return R

def get_any_rotation_from_vector(v):
    rot = np.eye(4)
    if np.linalg.norm(v) < 1e-9:
        return rot
    rot[0:3, 1] = v / np.linalg.norm(v)
    ov = np.array(v)
    if ov[0] == 0:
        ov[0] = - 1
    elif ov[1] == 0:
        ov[1] = - 1
    elif ov[2] == 0:
        ov[2] = - 1
    else:
        ov[0] -= 1
    zaxis = np.cross(rot[0:3, 1], ov)
    rot[0:3, 2] = zaxis / np.linalg.norm(zaxis)
    xaxis = np.cross(rot[0:3, 1], rot[0:3, 2])
    rot[0:3, 0] = xaxis / np.linalg.norm(xaxis)
    return rot

def unit(v):
    return v / np.linalg.norm(v)

def new_base(start, xdir, yplane, in_place=False):
    if in_place:
        result = start
    else:
        # create copy
        result = np.matrix(start)
    
    # x
    result[0:3, 0] = xdir[0:3, 3] - start[0:3, 3]
    result[0:3, 0] = result[0:3, 0] / np.linalg.norm(result[0:3, 0])
    
    # yplane
    yw = yplane[0:3, 3] - start[0:3, 3]
    yw = yw / np.linalg.norm(yw)
    # z
    result[0:3, 2] = np.cross(result[0:3, 0], yw)
    result[0:3, 2] = result[0:3, 2] / np.linalg.norm(result[0:3, 2])
    
    # y
    result[0:3, 1] = -np.cross(result[0:3, 0], result[0:3, 2])
    return result

def new_base_mat(start, xdir, yplane):
    # create copy
    result = np.matrix(start)
    # x
    result[0:3, 0] = xdir[0:3, 3] - start[0:3, 3]
    result[0:3, 0] = result[0:3, 0] / np.linalg.norm(result[0:3, 0])
    # yplane
    yw = yplane.A[0:3, 3] - start.A[0:3, 3]
    yw = yw / np.linalg.norm(yw)
    # z
    result.A[0:3, 2] = np.cross(result.A[0:3, 0], yw)
    result[0:3, 2] = result[0:3, 2] / np.linalg.norm(result[0:3, 2])
    # y
    result.A[0:3, 1] = -np.cross(result.A[0:3, 0], result.A[0:3, 2])
    return result

def transform_points(points, transformation, hom=1):
    """
    for each point p assume a hom points hp and create new point np by
     hp = [p, 1]
     np = dot(transformation, hp)
    """
    npoints = np.empty(points.shape, dtype=points.dtype)
    for i in range(3):
        npoints[:, i] = transformation[i, 0] * points[:, 0] + transformation[i, 1] * points[:, 1] + transformation[i, 2] * points[:, 2]
    if hom == 1:
        npoints += transformation[:3, 3]
    return npoints



def get_rpy_from_rot(r):
    # craig, page 47 and following
    beta = math.atan2(-r[2, 0], math.sqrt(r[0, 0]**2 + r[1, 0]**2))
    if abs(beta - (math.pi/2)) < 1e-3:
        alpha = 0
        gamma = math.atan2(r[0, 1], r[1, 1])
    elif abs(beta + (math.pi/2)) < 1e-3:
        alpha = 0
        gamma = -math.atan2(r[0, 1], r[1, 1])
    else:
        cb = math.cos(beta)
        alpha = math.atan2(r[1, 0] / cb, r[0, 0] / cb)
        gamma = math.atan2(r[2, 1] / cb, r[2, 2] / cb)
    return gamma, beta, alpha

def fstr(f):
    return ("%.7f" % f).rstrip("0").rstrip(".")

def get_pose_from_frame(frame, as_string=True):
    pose = frame[:3, 3].tolist()
    # euler angles in radians: roll, pitch, yaw
    pose.extend(get_rpy_from_rot(frame[:3, :3]))
    if as_string:
        return " ".join(map(fstr, pose))
    else:
        return np.array(pose)

def normalize_quaternion(q):
    # q: (w, x, y, z)
    s = np.sqrt(np.dot(q, q))
    eps = 1e-7
    if np.fabs(s) < eps:
        return np.array([1, 0, 0, 0], dtype=float)
    return q / s

def get_quaternion_from_rpy(rpy):
    # q: (w, x, y, z)
    phi, the, psi = np.array(rpy) / 2.0
    q = np.array([
        np.cos(phi) * np.cos(the) * np.cos(psi) + np.sin(phi) * np.sin(the) * np.sin(psi),
        np.sin(phi) * np.cos(the) * np.cos(psi) - np.cos(phi) * np.sin(the) * np.sin(psi),
        np.cos(phi) * np.sin(the) * np.cos(psi) + np.sin(phi) * np.cos(the) * np.sin(psi),
        np.cos(phi) * np.cos(the) * np.sin(psi) - np.sin(phi) * np.sin(the) * np.cos(psi),
    ])
    return normalize_quaternion(q)

def get_angle_axis_from_quaternion(q):
    # q: (w, x, y, z)
    qlen = np.dot(q[1:], q[1:])
    eps = 1e-7
    if np.fabs(qlen) < eps:
        return 0.0, np.array([1, 0, 0], dtype=float)
    return 2.0 * math.acos(q[0]), q[1:] / math.sqrt(qlen)

def get_angle_axis_from_rpy(rpy):
    return get_angle_axis_from_quaternion(
        get_quaternion_from_rpy(rpy))

def angle_axis2rot(angle, axis):
    x, y, z = axis[0:3]
    s = math.sin(angle)
    c = math.cos(angle)   
    v = 1 - c
    xyv = x * y * v
    yzv = y * z * v
    xzv = x * z * v
    return np.array([
        x*x*v + c,  xyv - z*s,  xzv + y*s,
        xyv + z*s,  y*y*v + c,  yzv - x*s,
        xzv - y*s,  yzv + x*s,  z*z*v + c]).reshape(3, 3)

def get_frame_from_pose(pose):
    frame = np.eye(4)
    frame[:3, 3] = pose[:3]
    angle, axis = get_angle_axis_from_rpy(pose[3:])
    frame[:3, :3] = angle_axis2rot(angle, axis)
    return frame

#http://lazax.com/www.cs.columbia.edu/~laza/html/Stewart/matlab/

# rot2quat - converts a rotation matrix (3x3) to a unit quaternion(3x1)
#
#    q = rot2quat(R)
# 
#    R - 3x3 rotation matrix, or 4x4 homogeneous matrix 
#    q - 3x1 unit quaternion
#        q = sin(theta/2) * v
#        teta - rotation angle
#        v    - unit rotation axis, |v| = 1
#
#    
# See also: quat2rot, rotx, roty, rotz, transl, rotvec
def rot2quat(R):
    # returns q
    w4 = 2 * np.sqrt(1 + np.trace(R[:3, :3])) # can this be imaginary?
    q = np.array([
        ( R[2, 1] - R[1, 2] ) / w4,
        ( R[0, 2] - R[2, 0] ) / w4,
        ( R[1, 0] - R[0, 1] ) / w4
    ])
    return q
    
# quat2rot - a unit quaternion(3x1) to converts a rotation matrix (3x3) 
#
#    R = quat2rot(q)
# 
#    q - 3x1 unit quaternion
#    R - 4x4 homogeneous rotation matrix (translation component is zero) 
#        q = sin(theta/2) * v
#        teta - rotation angle
#        v    - unit rotation axis, |v| = 1
#
# See also: rot2quat, rotx, roty, rotz, transl, rotvec
def quat2rot(q):
    # returns R
    p = np.dot(q, q)
    if p > 1:
        raise RuntimeWarning("Warning: quat2rot: quaternion greater than 1")
    w = np.sqrt(1 - p)                   # w = cos(theta/2)
    R = np.eye(4)
    R[:3, :3] = 2 * np.outer(q, q) + 2 * w * skew(q) + np.eye(3) - 2 * np.diag([p, p, p])
    return R

# skew - returns skew matrix of a 3x1 vector. 
#        cross(V,U) = skew(V)*U
#
#    S = skew(V)
#
#          0  -Vz  Vy
#    S =   Vz  0  -Vx  
#         -Vy  Vx  0
#
# See also: cross
def skew(V):
    # returns S
    S = np.array([
        [    0., -V[2],  V[1] ],
        [  V[2],    0., -V[0] ],
        [ -V[1],  V[0],    0. ]])
    return S
    
#TRANSL	Translational transform
#
#	T= TRANSL(X, Y, Z)
#	T= TRANSL( [X Y Z] )
#
#	[X Y Z]' = TRANSL(T)
#
#	[X Y Z] = TRANSL(TG)
#
#	Returns a homogeneous transformation representing a 
#	translation of X, Y and Z.
#
#	The third form returns the translational part of a
#	homogenous transform as a 3-element column vector.
#
#	The fourth form returns a  matrix of the X, Y and Z elements
#	extracted from a Cartesian trajectory matrix TG.
#
#	See also ROTX, ROTY, ROTZ, ROTVEC.
# 	Copyright (C) Peter Corke 1990
def transl(x, y=None, z=None):
    # returns r
    if y is None and z is None:
        if len(x.shape) == 2 and x.shape[0] == 4 and x.shape[1] == 4:
            r = np.eye(4)
            r[:3, 3] = x[:3, 3]
        else:
            r = np.eye(4)
            r[:3, 3] = x[:3]
    else:
        r = np.eye(4)
        r[:3, 3] = x, y, z
    return r

# handEye - performs hand/eye calibration
# 
#     gHc = handEye(bHg, wHc)
# 
#     bHg - pose of gripper relative to the robot base..
#           (Gripper center is at: g0 = Hbg * [0;0;0;1] )
#           Matrix dimensions are 4x4xM, where M is ..
#           .. number of camera positions. 
#           Algorithm gives a non-singular solution when ..
#           .. at least 3 positions are given
#           Hbg(:,:,i) is i-th homogeneous transformation matrix
#     wHc - pose of camera relative to the world ..      
#           (relative to the calibration block)
#           Dimension: size(Hwc) = size(Hbg)
#     gHc - 4x4 homogeneous transformation from gripper to camera      
#           , that is the camera position relative to the gripper.
#           Focal point of the camera is positioned, ..
#           .. relative to the gripper, at
#                 f = gHc*[0;0;0;1];
#           
# References: R.Tsai, R.K.Lenz "A new Technique for Fully Autonomous 
#           and Efficient 3D Robotics Hand/Eye calibration", IEEE 
#           trans. on robotics and Automaion, Vol.5, No.3, June 1989
#
# Notation: wHc - pose of camera frame (c) in the world (w) coordinate system
#                 .. If a point coordinates in camera frame (cP) are known
#                 ..     wP = wHc * cP
#                 .. we get the point coordinates (wP) in world coord.sys.
#                 .. Also refered to as transformation from camera to world
def handEye(bHg, wHc):
    # M number of measurements
    # bHg -- robot-base to gripper transform (4x4xM)
    # wHc -- world to camera transform (4x4xM)
    #
    # returns:
    # gHc -- gripper to camera transform

    M = bHg.shape[2] # M = size(bHg, 3);

    K = (M * M - M) / 2.               # Number of unique camera position pairs
    A = np.zeros((3*K, 3), dtype=float)   # will store: skew(Pgij + Pcij)
    B = np.zeros((3*K, ), dtype=float)   # will store: Pcij - Pgij
    k = 0
    
    # Now convert from wHc notation to Hc notation used in Tsai paper.
    Hg = bHg
    # Hc = cHw = inv(wHc); We do it in a loop because wHc is given, not cHw
    Hc = np.zeros((4, 4, M), dtype=float)
    for i in range(M):
        Hc[:, :, i] = np.linalg.inv(wHc[:, :, i])
    
    for i in range(M): # 1:M,
        for j in range(i + 1, M): # = i+1:M;
            Hgij = np.dot(np.linalg.inv(Hg[: , :, j]), Hg[:, :, i])    # Transformation from i-th to j-th gripper pose
            Pgij = 2 * rot2quat(Hgij)                     # ... and the corresponding quaternion
            Hcij = np.dot(Hc[:, :, j], np.linalg.inv(Hc[:, :, i]))     # Transformation from i-th to j-th camera pose
            Pcij = 2 * rot2quat(Hcij)                     # ... and the corresponding quaternion

            A[k * 3:(k + 1) * 3, :] = skew(Pgij + Pcij)   # left-hand side
            B[k * 3:(k + 1) * 3]    = Pcij - Pgij         # right-hand side
            k += 1                                        # Form linear system of equations

    # Rotation from camera to gripper is obtained from the set of equations:
    #    skew(Pgij+Pcij) * Pcg_ = Pcij - Pgij
    # Gripper with camera is first moved to M different poses, then the gripper
    # .. and camera poses are obtained for all poses. The above equation uses
    # .. invariances present between each pair of i-th and j-th pose.
    #Pcg_ = A \ B                # Solve the equation A*Pcg_ = B
    Pcg_, res, rank, s = np.linalg.lstsq(A, B)
    print("res: %f" % res)
    
    # Obtained non-unit quaternin is scaled back to unit value that
    # .. designates camera-gripper rotation
    print("Pcg_")
    print(Pcg_)
    Pcg = 2 * Pcg_ / np.sqrt(1 + np.dot(Pcg_, Pcg_))
    print("Pcg normed")
    print(Pcg)
    Rcg = quat2rot(Pcg / 2)      # Rotation matrix
    print("Rcg")
    print(Rcg)
    # Calculate translational component
    k = 0
    for i in range(M): # = 1:M,
        for j in range(i + 1, M): # = i+1:M;
            Hgij = np.dot(np.linalg.inv(Hg[:, :, j]), Hg[:, :, i])    # Transformation from i-th to j-th gripper pose
            Hcij = np.dot(Hc[:, :, j], np.linalg.inv(Hc[:, :, i]))    # Transformation from i-th to j-th camera pose

            A[k * 3:(k + 1) * 3, :] = Hgij[:3, :3] - np.eye(3) # left-hand side
            B[k * 3:(k + 1) * 3]    = np.dot(Rcg[:3, :3], Hcij[:3, 3]) - Hgij[:3, 3]     # right-hand side
            k += 1                                       # Form linear system of equations
    #Tcg = A \ B
    Tcg = np.linalg.lstsq(A, B)
    Tcg, res, rank, s = np.linalg.lstsq(A, B)
    print("res: %f" % res)
    
    gHc = np.dot(transl(Tcg), Rcg)	# incorporate translation with rotation

    return gHc

# empty calss to pickle arbitrary calibration objects
class calibration(object):
    pass

def transformation(x=None, y=None, z=None,
                   x_rot=None, x_rot_deg=None,
                   y_rot=None, y_rot_deg=None,
                   z_rot=None, z_rot_deg=None):
    T = np.eye(4)

    if x_rot_deg is not None: x_rot = x_rot_deg / 180. * np.pi
    if y_rot_deg is not None: y_rot = y_rot_deg / 180. * np.pi
    if z_rot_deg is not None: z_rot = z_rot_deg / 180. * np.pi

    if x_rot is not None: T = np.dot(T, rotx(x_rot))
    if y_rot is not None: T = np.dot(T, roty(y_rot))
    if z_rot is not None: T = np.dot(T, rotz(z_rot))

    if x is not None: T[0, 3] = x
    if y is not None: T[1, 3] = y
    if z is not None: T[2, 3] = z

    return T

def translation(x=None, y=None, z=None):
    if type(x) not in (None, float, int) and len(x) == 3:
        x, y, z = x
    return transformation(x, y, z)
