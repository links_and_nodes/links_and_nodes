"""
    Copyright 2014 DLR e.V., Florian Schmidt
    
    This file is part of floatlog_gui.
    
    floatlog_gui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    floatlog_gui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import print_function

import sys
import time
import socket
import select
import struct

import numpy as np

import pyutils

if sys.version_info >= (3, 0):
    raw_input = input


class floatlog_downloader(object):
    def __init__(self, host="", port=54385, verbose=False, quiet=False):
        self.quiet = quiet
        self._selected_block = None
        self.unpack_hook = None
        self.last_lines = []
        self.verbose = verbose
        self.oks_seen = 0
        self.s = socket.socket()
        try:
            self.s.connect((host, port))
        except Exception:
            print("could not connect to %r %r" % (host, port))
            self.s = None
            raise
        self.s.setblocking(False)
        self.la = pyutils.line_assembler(self.line)
        self.data_left = None
        self.last_download = None

        #self.s.send("status\n")
        #self.s.send("enable 10\n")

    def is_connected(self):
        return self.s is not None

    def disconnect(self):
        if self.s is not None:
            try:
                self.s.shutdown(socket.SHUT_WR)
            except Exception:
                pass
        self.s = None

    def _send(self, msg):
        try:
            self.s.send(msg)
        except Exception:
            self.disconnect()
            raise

    def choose(self, block_name):
        self._send("choose %s\n" % block_name)
        while True:
            self.last_lines = []
            self.iterate(True)
            for l in self.last_lines:
                if l.startswith("ERROR"):
                    raise Exception("error, block name %r does not exist!" % block_name)
                if l.startswith("OK hello!"):
                    continue
                if l.startswith("OK"):
                    print("ok, selected block %r: %r - now requesting status..." % (block_name, l))
                    self._selected_block = block_name
                    self._send("status\n")
                    return
                print(repr(l))

    def get_status(self):
        self.status = None
        self._send("status\n")

    def get_last_sample(self):
        self.last_download = None
        self._send("c\n")
        while self.last_download is None:
            self.iterate(True)
        return self.last_download
    
    def start_download(self):
        self.last_download = None
        self._send("download\n")

    def wait_for_download(self):
        while self.last_download is None:
            self.iterate(True)
        return self.last_download

    def download(self):
        self.start_download()
        d = self.wait_for_download()
        return self.interpret_signals(d)

    def interpret_signals(self, d):
        i = {}
        for n, s in zip(self.status["signal_names"], d):
            i[n] = s
        return i

    def enable(self, samples):
        self._send("enable %d\n" % samples)

    def disable(self):
        self._send("disable\n")

    def reset(self):
        self._send("reset\n")
        
    def set_divisor(self, div):
        self._send("divisor %d\n" % div)
    
    def line(self, line):
        if self.verbose: print("floatlog: %r" % line)
        self.last_lines.append(line)
        if line.startswith("OK status"):
            #print "line:", repr(line)
            l = line.split(": ", 1)[1]
            #print "l", repr(l)
            try:
                v, r = l.split("], ", 1)
            except Exception:
                raise Exception("invalid line: %r" % l)
            r, signal_names = r.split(", signal_names: '", 1)
            l = r.split(", ")
            #print l
            l = dict([tuple(i.split(" ", 1)) for i in l])
            signal_names = [s.strip() for s in signal_names[:-1].split(",")]
            l["signal_names"] = signal_names
            l["vector"] = v.split("[", 1)[1]
            l["name"] = l["name:"]
            del l["name:"]
            l["ringbuffer_size"] = l["ringbuffer:"].split(" ", 1)[1]
            del l["ringbuffer:"]
            self.status = l
        if line.startswith("OK"):
            self.oks_seen += 1
        if line.startswith("OK") and line.endswith("will follow"):
            self.data = []
            self.data_left = int(line.split(" ", 2)[1])
            self.data_len = self.data_left
            rd = self.la.data[self.la.rest:]
            if self.la.data[self.la.rest:]:
                #print "have rest in line reader: %r" % self.la.data[self.la.rest:]
                dd = self.la.data[self.la.rest:self.la.rest + self.data_left]
                self.data.append(dd)
                self.data_left -= len(dd)
                self.la.reset()
                if self.data_left == 0:
                    self.process_download("".join(self.data))
            return False

    def process_download(self, data):
        if self.unpack_hook:
            self.unpack_hook(0)
        #print "received data: %d" % (len(data))
        if self.status["doubles"] == "1":
            f = "d"
            s = 8
        else:
            f = "f"
            s = 4
        vector = list(map(int, self.status["vector"].strip("[]").split(", ")))

        sample_size = int(self.status["sample_size"])
        sample_cnt = len(data) / sample_size
        #print "samples", sample_cnt

        fmt = []
        result = []
        sumcount = 0
        for cnt in vector:
            sumcount += cnt
        start = time.time()

        fmt = "%d%s" % (sumcount * sample_cnt, f)
        a = np.array(struct.unpack(fmt, data)).reshape(sample_cnt, sumcount)
        result = []
        o = 0
        dict_result = {}
        for i, (cnt, sn) in enumerate(zip(vector, self.status["signal_names"])):
            va = a[:, o:o+cnt]
            result.append(va)
            o += cnt
            dict_result[sn] = va
            if self.unpack_hook:
                self.unpack_hook(float(i) / len(vector))
        
        b = time.time() - start
        #print "unpack time: %.3f" % b
        if self.unpack_hook:
            self.unpack_hook(1)
        self.last_download = result
        self.last_download_dict = dict_result

    def process(self, *args):
        if self.s is None:
            return False
        d = self.s.recv(102400)
        if not d:
            self.disconnect()
            raise Exception("eof from server")
        if self.data_left:
            #print "got data for download"
            dd = d[:self.data_left]
            self.data.append(dd)
            self.data_left -= len(dd)
            #print "self.data_left", self.data_left
            if self.data_left == 0:
                self.process_download("".join(self.data))
            d = d[len(dd):]
            #print "to la: %r" % d
        self.la.write(d)
        return True

    def iterate(self, block=False, timeout=None):
        if block:
            to = timeout
        else:
            to = 0
        import select
        rfd, wfd, efd = select.select([self.s], [], [], to)
        if self.s in rfd:
            return self.process()
        return False

    def loop(self):
        while True:
            rfd, wfd, efd = select.select([self.s, sys.stdin], [], [])
            if self.s in rfd:
                d = self.s.recv(1024)
                if not d:
                    raise Exception("eof from server")
                if self.data_left:
                    #print "got data for download"
                    dd = d[:self.data_left]
                    self.data.append(dd)
                    self.data_left -= len(dd)
                    #print "self.data_left", self.data_left
                    if self.data_left == 0:
                        self.process_download("".join(self.data))
                    d = d[len(dd):]
                    #print "to la: %r" % d
                self.la.write(d)
            if sys.stdin in rfd:
                i = raw_input()
                i = i.strip()
                if i and i[0] == "d":
                    self._send("download\n")
                elif i and i[0] == "e":
                    samples = int(i.split(" ", 1)[1])
                    self._send("enable %d\n" % samples)                    
                else:
                    self._send("status\n")

if __name__ == "__main__":
    c = floatlog_downloader()
    c.loop()
