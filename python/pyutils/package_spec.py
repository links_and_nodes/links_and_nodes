from __future__ import print_function

from distutils.core import Extension
import os

compiler = ""

# global flags added to every module
ldflags = []
cflags = []

def module(sources, packages=None, **kwargs):
    global available_packages
    d = { "sources": sources }
    has_pyx = False
    for s in sources:
        if s.endswith(".pyx"):
            has_pyx = True
            break
    if has_pyx:
        d["language"] = "c++"
    d.update(kwargs)
    dp = package()
    dp.update(None, d)
    if packages is not None:
        for pn in packages:
            p = available_packages[pn]
            p.update(pn, d)
    return d

class package(object):
    def __init__(self, package_base=None, libs=None, includes=None, append_arch=True, append_lib=True, cflags=None, libdirs=None):
        self.cflags = cflags
        self.package_base = package_base
        if type(libs) == str: libs = [libs]
        self.libs = libs
        if type(includes) == str: includes = [includes]
        self.includes = includes
        self.append_arch = append_arch
        self.append_lib = append_lib
        self.libdirs = libdirs
    def _add(self, d, n, v):
        if n not in d:
            d[n] = []
        if v not in d[n]:
            d[n].append(v)
            return True
        return False
    def _extend(self, d, n, v):
        if n not in d:
            d[n] = []
        d[n].extend(v)
    def _add_cflag(self, d, flag):
        return self._add(d, "extra_compile_args", flag)
    def _add_ldflag(self, d, flag):
        return self._add(d, "extra_link_args", flag)
    def _add_include(self, d, inc_dir):
        return self._add_cflag(d, "-I%s" % inc_dir)
    def _add_libpath(self, d, lib_dir):
        if self._add_ldflag(d, "-L%s" % lib_dir):
            self._extend(d, "extra_link_args", ["-Xlinker", "-rpath" ,"-Xlinker", lib_dir])
            return True
        return False
    def _add_lib(self, d, lib):
        if lib.startswith("/"): # absolute path
            return self._add_ldflag(d, "%s" % lib)
        if lib.startswith("./"): # absolute path
            return self._add_ldflag(d, "%s" % lib)
        return self._add_ldflag(d, "-l%s" % lib)
    def update(self, package_name, d):
        global ldflags
        if "depends" not in d:
            d["depends"] = []
        if "setup.py" not in d["depends"]:
            d["depends"].append("setup.py")
        if self.includes:
            for inc_dir in self.includes:
                self._add_include(d, inc_dir)
        if self.package_base:
            pb = self.package_base
            self._add_include(d, os.path.join(pb, "include"))
            if self.append_lib:
                lib = "lib"
            else:
                lib = ""
            if self.append_arch:
                self._add_libpath(d, os.path.join(pb, lib, compiler))
            else:
                self._add_libpath(d, os.path.join(pb, lib))
        if self.libdirs:
            for ld in self.libdirs:
                self._add_libpath(d, ld)
        if self.libs:
            list(map(lambda l: self._add_lib(d, l), self.libs))
        if self.libs is None and self.package_base is not None:
            self._add_lib(d, package_name)
        if ldflags:
            list(map(lambda l: self._add_ldflag(d, l), ldflags))
        if cflags:
            list(map(lambda l: self._add_cflag(d, l), cflags))
        if self.cflags:
            list(map(lambda l: self._add_cflag(d, l), self.cflags))
class pkg_package(package):
    def pkg_config(args):
        cin, cout = os.popen4("pkg-config %s" % args)
        cin.close()
        answer = cout.read()
        if "not found" in answer:
            print("pkg-config can't find package: %r" % args)
            print(answer)
            raise ValueError("pkg-config")
        flags = answer.strip().split(" ")
        cout.close()
        return flags
    pkg_config = staticmethod(pkg_config)
    def __init__(self, pkg_name):
        package.__init__(self, append_arch=False)
        self.cflags = self.pkg_config("--cflags %s" % pkg_name)
        self.libs = self.pkg_config("--libs %s" % pkg_name)
    def update(self, package_name, d):
        for f in self.cflags:
            self._add_cflag(d, f)
        for f in self.libs:
            self._add_ldflag(d, f)
        
def get_modules(modules):
    import os
    arch = os.uname()[4]
    if arch == "i686":
        arch = "x86"
    this_arch = "sled11-%s-gcc4.x" % arch
    import sys
    only_one = "-only" in sys.argv
    if only_one:
        i = sys.argv.index("-only")
        del sys.argv[i]
        only_one = sys.argv[i]
        del sys.argv[i]
    else:
        only_one = None
    mods = []
    for n, m in modules.items():
        if only_one is not None and n != only_one:
            print("skipping", n)
            continue
        if "only_archs" in m:
            print(m["only_archs"])
            if this_arch not in m["only_archs"]:
                print("module %r not supported for this arch!" % n)
                continue
        module = Extension(n, **m)
        mods.append(module)
    return mods
