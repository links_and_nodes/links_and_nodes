import os
import numpy as np

def read_obj(fn, file_mapper=None):
    """
    read a wavefront obj file
    return a dict of contained vertices, normals, texcoords, objects and materails
    """

    materials = dict()
    objects = dict()
    vertices = [] # x, y, z
    vertex_normals = [] # nx, ny, nz
    texcoords = []  # u, v, w
    ret = dict(
        objects=objects,
        materials=materials,
        vertices=vertices,
        vertex_normals=vertex_normals,
        texcoords=texcoords
    )

    current_object = None

    def get_new_object(name):
        if name in objects:
            cnt = 2
            while True:
                test = "%s %d" % (name, cnt)
                if test not in objects:
                    name = test
                    break
                cnt += 1
        o = dict(
            name=name,
            faces=[], # list of 3-tuples: (vidx, tidx, nidx) 1-based, might be 0
            face_materials=[], # list of 2-tuples: (first-face-index, material-name) will get (first-face-index, n-faces, material-name)
        )
        objects[name] = o
        return o, o["faces"], o["face_materials"]

    def make_abs_fn(mtlfn):
        return os.path.join(os.path.dirname(os.path.realpath(fn)), mtlfn)

    def read_mtllib(mtlfn):
        if file_mapper:
            mtlfn = file_mapper(mtlfn)
        else:
            mtlfn = make_abs_fn(mtlfn)
        current_material = None
        with open(mtlfn) as fp:
            for line in fp:
                line = line.rstrip()
                if line[:1] == "#" or not line:
                    continue
                verb, args = line.split(" ", 1)
                if verb == "newmtl":
                    current_material = dict(name=args)
                    materials[args] = current_material
                    continue
                if verb == "illum":
                    current_material[verb] = int(args)
                elif verb in ("Ns", "Ni", "d", "Tr"):
                    current_material[verb] = float(args)
                elif verb.startswith("map_"):
                    current_material[verb] = make_abs_fn(args)
                else:
                    current_material[verb] = np.fromstring(args, dtype=np.double, sep=" ")

    with open(fn) as fp:
        for line in fp:
            line = line.rstrip()
            if line[:1] == "#" or not line:
                continue
            verb, args = line.split(" ", 1)

            if verb == "mtllib":
                read_mtllib(args)
                continue
            elif verb == "o":
                current_object, current_faces, face_materials = get_new_object(args)
                continue
            elif verb == "s":
                # smoothing group
                # todo
                continue
            elif verb == "v":
                vertices.append(np.fromstring(args, dtype=np.double, sep=" "))
                continue
            elif verb == "vn":
                vertex_normals.append(np.fromstring(args, dtype=np.double, sep=" "))
                continue
            elif verb == "vt":
                texcoords.append(np.fromstring(args, dtype=np.double, sep=" "))
                continue

            # from here on we need an object
            if current_object is None:
                current_object, current_faces, face_materials = get_new_object("unnamed object")

            if verb == "usemtl":
                face_materials.append((len(current_faces), args))
            elif verb == "f":
                face = [] # list of 3-tuples: (vertex-index, texture-coordinate-index, normal-index) -> all 1-based, 0 indicates that this info is not set
                for face_str in args.split(" "):
                    face_args = face_str.split("/")
                    face_vertex = np.zeros((3, ), dtype=np.uint32)
                    for i, face_arg in enumerate(face_args):
                        if face_arg:
                            face_vertex[i] = int(face_arg)
                    face.append(face_vertex)
                current_faces.append(np.array(face, dtype=np.uint32))
            else:
                print("WARNING: unknown obj verb %r with args %r" % (verb, args))

    for w in "vertices", "vertex_normals", "texcoords":
        ret[w] = np.array(ret[w], dtype=np.double)

    # optimize mem layout
    for obj in objects.values():
        last_fi = 0
        last_mat = None
        face_mats = []
        for fi, mat in obj["face_materials"]:
            since_last = fi - last_fi
            if since_last > 0:
                face_mats.append((last_fi, since_last, last_mat))
            last_fi, last_mat = fi, mat
        fi = len(obj["faces"])
        since_last = fi - last_fi
        if since_last > 0:
            face_mats.append((last_fi, since_last, last_mat))
        obj["face_materials"] = face_mats
    return ret

def print_obj_stat(obj):
    print("n vertices: %d" % len(obj["vertices"]))
    print("n normals: %d" % len(obj["vertex_normals"]))
    print("n texcoords: %d" % len(obj["texcoords"]))
    objects, materials = obj["objects"], obj.get("materials", {})
    print("have %d objects:" % len(objects))
    for obj_name, o in objects.items():
        print("  object: %r" % obj_name)
        print("    n faces: %d" % len(o["faces"]))
        print("    n faces materials: %d" % len(o["face_materials"]))
        for start_idx, n_faces, matname in o["face_materials"]:
            print("      %d faces with material %r" % (n_faces, matname))
    print("have %d materials: %s" % (len(materials), list(materials.keys())))
    #for mat in materials.values():
    #    %% mat
