#include <Python.h>

#include "swig_util.h"

typedef void *(*swig_converter_func)(void *);
typedef struct swig_type_info *(*swig_dycast_func)(void **);

/* Structure to store inforomation on one type */
typedef struct swig_type_info {
        const char             *name;                   /* mangled name of this type */
        const char             *str;                    /* human readable name of this type */
        swig_dycast_func        dcast;          /* dynamic cast function down a hierarchy */
        struct swig_cast_info  *cast;                   /* linked list of types that can cast into this type */
        void                   *clientdata;             /* language specific type data */
        int                    owndata;         /* flag if the structure owns the clientdata */
} swig_type_info;

/* Structure to store a type and conversion function used for casting */
typedef struct swig_cast_info {
        swig_type_info         *type;                   /* pointer to type that is equivalent to this type */
        swig_converter_func     converter;              /* function to cast the void pointers */
        struct swig_cast_info  *next;                   /* pointer to next cast in linked list */
        struct swig_cast_info  *prev;                   /* pointer to the previous cast */
} swig_cast_info;

typedef struct {
        PyObject_HEAD
        void *ptr;
        swig_type_info *ty;
        int own;
        PyObject *next;
} PySwigObject;

static inline PyObject *_SWIG_This(void) {
        return PyString_FromString("this");
}

static PyObject *SWIG_This(void) {
        static PyObject *swig_this = _SWIG_This();
        return swig_this;
}

void* get_ptr(PyObject* data) {
        PyObject **dictptr = _PyObject_GetDictPtr(data);
        if(dictptr == __null) {
                printf("could not get dict pointer!\n");
                return NULL;
        }
        PyObject* dict = *dictptr;
        PySwigObject* obj = (PySwigObject*)PyDict_GetItem(dict, SWIG_This());
        return obj->ptr;
}
