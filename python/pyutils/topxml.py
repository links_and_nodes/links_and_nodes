
from __future__ import print_function

import io
import re
import sys
import copy
import pprint

import xml.parsers.expat

if sys.version_info >= (3, 0):
    unicode = str
    long = int
    raw_input = input


def format_xml(pyeops, pyops, pytasks, ipol="jntDefault"):
    eops = []
    ops = []
    tasks = []
    for this_eop in pyeops:
        if len(this_eop) == 3:
            eop_name, actuator, j = this_eop
            this_ipol = ipol
        else:
            eop_name, actuator, j, this_ipol = this_eop
        used = " ".join(["1"] * len(j))
        if actuator == "file motion":
            eops.append('<Elemop name="%s">\n\t<TrajectoryFile action="replay" file="%s" ipol="%s"></TrajectoryFile>\n</Elemop>' % (
                eop_name, j, this_ipol))
        else:
            eops.append('<Elemop name="%s">\n\t<Joint actuator="%s" ipol="%s" motion="absolute" used="%s">%s</Joint>\n</Elemop>' % (
                eop_name, actuator, this_ipol, used, " ".join(["%.4f" % f for f in j])))

    for op_name, _eops in pyops:
        ops.append('<Operation name="%s">' % op_name)
        for eop in _eops:
            if type(eop) == list or type(eop) == tuple:
                eop = " ".join(eop)
            ops.append('\t<Elemop ref="%s"></Elemop>' % eop)
        ops.append('</Operation>')

    for task_name, _ops in pytasks:
        tasks.append('<Task name="%s">' % task_name)
        for op in _ops:
            tasks.append('\t<Operation ref="%s "></Operation>' % op)
        tasks.append('</Task>')
        
    eops = "\n".join(eops)
    ops = "\n".join(ops)
    tasks = "\n".join(tasks)        
    return eops, ops, tasks

def generate_xml(template, output_fn, pyeops, pyops, pytasks, ipol="jntDefault"):
    eops, ops, tasks = format_xml(pyeops, pyops, pytasks, ipol)

    with open(template, "rb") as fp:
        template = fp.read()
    output = []
    last = 0
    for m in re.finditer("^(.*?)<!-- insert: (.*?) -->", template, re.M):
        if m.group(2) == "elmop":
            output.append(template[last:m.start()])
            output.append(m.group(1) + eops.replace("\n", "\n" + m.group(1)))
            last = m.end()
        elif m.group(2) == "op":
            output.append(template[last:m.start()])
            output.append(m.group(1) + ops.replace("\n", "\n" + m.group(1)))
            last = m.end()
        elif m.group(2) == "task":
            output.append(template[last:m.start()])
            output.append(m.group(1) + tasks.replace("\n", "\n" + m.group(1)))
            last = m.end()
    output.append(template[last:])
    with open(output_fn, "wb") as fp:
        fp.write("".join(output))

def update_xml(fn, pyeops, pyops, pytasks, ipol="jntDefault"):
    eops, ops, tasks = format_xml(pyeops, pyops, pytasks, ipol)
    with open(fn, "rb") as fp:
        data = fp.read()

    # first remove all old instances
    def remove_re(regex, data):
        match = re.search(regex, data, re.M | re.S)
        if not match:
            return data
        start = match.start(0)
        end = match.end(0)
        return data[0:start].rstrip() + "\n" + data[end:]
        
    for this_eop in pyeops:
        if len(this_eop) == 3:
            eop_name, actuator, j = this_eop
        else:
            eop_name, actuator, j, this_ipol = this_eop
        regex = '<Elemop name="%s">.*?</Elemop>([\t\r ]*\n)?' % eop_name
        data = remove_re(regex, data)
    for op_name, _eops in pyops:
        regex = '<Operation name="%s">.*?</Operation>([\t\r ]*\n)?' % op_name
        data = remove_re(regex, data)
    for task_name, _ops in pytasks:
        regex = '<Task name="%s">.*?</Task>([\t\r ]*\n)?' % task_name
        data = remove_re(regex, data)
    # write new instances
    m = re.search("^(.*?)<Operation", data, re.M)
    data = data[0:m.start()] + m.group(1) + eops.replace("\n", "\n" + m.group(1)) + "\n" + data[m.start():]
    
    m = re.search("^(.*?)<Task", data, re.M)
    data = data[0:m.start()] + m.group(1) + ops.replace("\n", "\n" + m.group(1)) + "\n" + data[m.start():]
    
    m = re.search("</Top", data, re.M)
    data = data[0:m.start()].rstrip() + "\n" + tasks.rstrip() + "\n" + data[m.start():]
    
    #file("out.xml", "wb").write(data)
    with open(fn, "wb") as fp:
        fp.write(data)
    

### newer, better xml classes
class xml_file_reader(object):
    def read(fn=None, data=None, encode=None):
        if fn is None:
            r = xml_file_reader(data=data, encode=encode)
        else:
            r = xml_file_reader(fn=fn, encode=encode)
        return r.root
    read = staticmethod(read)

    def __init__(self, fn=None, data=None, encode=None):
        self.encode = encode
        self.p = xml.parsers.expat.ParserCreate()
        self.p.StartElementHandler = self._start_element
        self.p.EndElementHandler = self._end_element
        self.p.CharacterDataHandler = self._char_data
        self.p.ordered_attributes = 1
        self.xml_data = []
        self.root = [None, {}, None, []]
        self.element = self.root
        if fn is None:
            self.p.Parse(data)
        else:
            with open(fn, "rb") as fp:
                self.p.ParseFile(fp)
        def cleanup(element):
            del element[2]
            for child in element[2]:
                if type(child) == list:
                    cleanup(child)
        cleanup(self.root)

    def _start_element(self, name, attrs):
        current = self.element

        new_attrs = []
        for i, arg in enumerate(attrs):
            if i % 2:
                continue
            value = attrs[i+1]
            if self.encode:
                arg = arg.encode(self.encode)
                value = value.encode(self.encode)
            new_attrs.append(
                (arg, value))
            
        if self.encode:
            name = name.encode(self.encode)
        self.element = [name, new_attrs, current, []]
        current[3].append(
            self.element)

    def _end_element(self, name):
        if self.element and self.element[0] != name:
            print("warning: have closing %r waiting for closing %r" % (
                name, self.element[0]))
        self.element = self.element[2]

    def _char_data(self, data):
        if not self.element:
            print("skipping data %r" % data)
            return
        if self.encode:
            data = data.encode(self.encode)
        self.element[3].append(data)

class xml_file_writer(object):
    def write(fn, data):
        w = xml_file_writer(fn, data)
        return
    write = staticmethod(write)

    def __init__(self, fn=None, data=None):
        if fn:
            self.fp = open(fn, "w")
            self.string = False
        else:
            self.fp = io.StringIO()
            self.string = True
        self.fp.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        self._write(data)
        if fn:
            self.fp.write("\n")
            self.fp.close()

    def __str__(self):
        if self.string:
            return self.fp.getvalue()
        else:
            return "<xml_file_writer %s>" % id()

    def _write(self, node):
        try:
            have_childs = len(node[2])
        except Exception:
            pprint.pprint(node)
            raise
        if node[0] is not None:
            try:
                self.fp.write(self._get_start_tag(node, have_childs))
            except Exception:
                import traceback
                try:
                    raise ValueError(
                        "invalid node : [%r, %r, [...]]\n%s" % (
                        node[0], node[1], traceback.format_exc()))
                except Exception:
                    raise ValueError("invalid node:\n%s\n%s" % (
                        pprint.pformat(node), traceback.format_exc()))
        for data in node[2]:
            if type(data) == list:
                self._write(data)
            else:
                self.fp.write(data)
        if node[0] is not None and have_childs:
            self.fp.write("</%s>" % node[0])
        
    def _get_start_tag(self, node, have_childs=True):
        if not node[1]:
            if have_childs:
                return "<%s>" % node[0]
            else:
                return "<%s />" % node[0]                
        entries = [node[0]]
        if type(node[1]) == tuple or type(node[1]) == list:
            attrs = node[1]
        else:
            attrs = list(node[1].items())
        for arg, value in attrs:
            entries.append(
                '%s="%s"' % (arg, self._escape_value(value)))
        if have_childs:
            return "<%s>" % " ".join(entries)
        else:
            return "<%s />" % " ".join(entries)
            

    def _escape_value(self, value):
        if type(value) == int:
            value = str(value)
        elif type(value) == float:
            value = str(value)
        try:
            return value.replace('"', "&quot;")
        except Exception:
            raise Exception("exception while trying to escape %r!" % value)

class xml_path(object):
    def __init__(self):
        self._path = []

    def push(self, node):
        if not self._path:
            node_count = 0
        else:
            node_count = self._path[-1][0].get(node[0], 0)
        if node_count != 0:
            node_id = "%s:%d" % (node[0], node_count + 1)
        else:
            if node[0] is None:
                node_id = ""
            else:
                node_id = "%s" % (node[0])
        if self._path:
            self._path[-1][0][node[0]] = node_count + 1
        
        element_counter = {}
        self._path.append(
            (element_counter, node_id, node))

    def pop(self, node):
        if self._path[-1][2] != node:
            raise ValueError("invalid pop!")
        del self._path[-1]

    def __str__(self):
        return "/".join([n[1] for n in self._path])

    def __len__(self):
        return len(self._path)

class xml_data(object):
    @staticmethod
    def _search_element(name, root, recurse=False, attrs=None):
        def matches(node, name):
            if node[0] != name: return False
            if attrs is None: return True
            this_attrs = dict(node[1])
            for name, value in attrs.items():
                if this_attrs.get(name) != value: return False
            return True
        for child in root[2]:
            if type(child) != list: continue
            if name is None or matches(child, name): return child
            if not recurse:
                continue
            ret = xml_data._search_element(name, child, recurse, attrs)
            if ret is not None:
                return ret
        return None    

    @staticmethod
    def _search_all_elements(name, root, recurse=False, attrs=None, found=None):
        if found is None:
            found = []
        def matches(node, name):
            if node[0] != name: return False
            if attrs is None: return True
            this_attrs = dict(node[1])
            for name, value in attrs.items():
                if this_attrs.get(name) != value: return False
            return True
        for child in root[2]:
            if type(child) != list: continue
            if name is None or matches(child, name):
                found.append(child)
            if not recurse:
                continue
            xml_data._search_all_elements(name, child, recurse=recurse, found=found)
        return found

    @staticmethod
    def _search_all_elements_re(name, root, recurse=False, attrs=None, found=None):
        if found is None:
            found = []
        def matches(node, name):
            if node[0] != name: return False
            if attrs is None: return True
            this_attrs = dict(node[1])
            for aname, value in attrs.items():
                tavalue = this_attrs.get(aname)
                if tavalue is None:
                    return False
                if re.match(value, tavalue) is None:
                    return False
            return True
        for child in root[2]:
            if type(child) != list: continue
            if name is None or matches(child, name):
                found.append(child)
            if not recurse:
                continue
            xml_data._search_all_elements_re(name, child, found=found)
        return found
    
    @staticmethod
    def get_element_by_path(root, *path):
        if not len(path):
            return root
        if type(path[0]) == tuple:
            tag_name, cnt = path[0]
        else:
            tag_name = path[0]
            cnt = 0
        for child in root[2]:
            if child[0] != tag_name:
                continue
            if cnt == 0:
                return xml_data.get_element_by_path(child, *path[1:])
            cnt -= 1
        raise Exception("no %r found below %r" % (path[0], root[0]))
        

class top_data(xml_data):
    def __init__(self, xml=None, filename=None):
        if xml is not None:
            self.root = xml
        elif filename is not None:
            self.root = xml_file_reader.read(filename)
        else:
            raise ValueError("neither xml nor filename keyword parameter specified!")
        self.top = self._search_element("Top", self.root, recurse=True)
        
    def transfer_task(self, task_name, source, postfix):
        self.postfix = postfix
        new_task_name = self._find_object_name(
            task_name, postfix)
        source_task = source.get_with_name("Task", task_name)
        ops = source.get_operations_from_task(source_task)
        self.changes = {
            "ipol": {
            "jntDefault": "jntDefaultNoVia",
            "headDefault": "jntDefaultNoVia",
            "handDefaultFast": "jntDefaultHandFastNoVia",
            "handDefault": "jntDefaultHandFastNoVia",
            "headFast": "jntDefaultNoVia"},
            "eop": {},
            "op": {}
            }
        checked = {}
        for t, entries in self.changes.items():
            if t not in checked:
                checked[t] = set()
            for o, n in entries.items():
                checked[t].add(o)
        new_ops = []
        for object, op_name in ops:
            op_name = op_name.strip()
            if op_name in checked["op"]:
                # already know about changes
                new_ops.append((object, self.changes["op"].get(op_name, op_name)))
                continue
            checked["op"].add(op_name)
            source_op = source.get_with_name("Operation", op_name)
            op_eops = source.get_elementary_operations_from_operation(source_op)
            create_new_op = False
            for eops in op_eops:
                for eop in eops:
                    if eop in checked["eop"]:
                        # already know about changes
                        create_new_op = eop in self.changes["eop"]
                        continue
                    checked["eop"].add(eop)
                    source_eop = source.get_with_name("Elemop", eop)
                    create_new_eop = True
                    # check ipol
                    while True:
                        ipol = source.get_ipol_from_eop(source_eop)
                        if ipol is None:
                            # no ipol used by this eop
                            create_new_eop = False
                            break
                        if ipol in checked["ipol"]:
                            # already know about changes
                            create_new_eop = ipol in self.changes["ipol"]
                            break
                        checked["ipol"].add(ipol)
                        source_ipol = source.get_with_name("Ipol", ipol)
                        if source_ipol is None:
                            raise ValueError("could not find source ipol %r" % ipol)
                        current_ipol = self.get_with_name("Ipol", ipol)
                        if current_ipol is None:
                            print("this is a new ipol", repr(ipol), checked, self.changes)
                            self._create_new(source_ipol)
                            break
                        if self._compare_nodes(source_ipol, current_ipol):
                            # they are identical
                            create_new_eop = False
                            break
                        print("ipols differ:", ipol)
                        new_name = self._create_new(source_ipol)
                        self.changes["ipol"][ipol] = new_name
                        break
                    if not create_new_eop:
                        # check eop
                        current_eop = self.get_with_name("Elemop", eop)
                        while True:
                            if current_eop is None:
                                # this is a new eop
                                create_new_eop = True
                                break
                            if self._compare_nodes(source_eop, current_eop):
                                # they are identical
                                break
                            # they differ! create new eop!
                            create_new_eop = True
                            break
                    if create_new_eop:
                        self.changes["eop"][eop] = self._create_new(source_eop)
                        create_new_op = True
            if not create_new_op:
                current_op = self.get_with_name("Operation", op_name)                       
                if not self._compare_nodes(source_op, current_op):
                    create_new_op = True
            if create_new_op:
                new_name = self._create_new(source_op)
                new_ops.append((object, new_name))
                self.changes["op"][op_name] = new_name
                continue
            new_ops.append((object, op_name))
        self.new_ops = new_ops
        new_name = self._create_new(source_task)

    def _create_new(self, template):
        # use self.postfix for name finding
        # return new name
        # use changes dict to possible use changed items with new names
        try:
            old_name = dict(template[1])["name"]
        except Exception:
            print("could not find old_name from")
            print(template)
            raise
        new_name = self._find_object_name(old_name, self.postfix)
        print("create new %r with name %r" % (template[0], new_name))
        new_node = copy.copy(template)
        new_attrs = dict(new_node[1])
        new_attrs["name"] = new_name
        if new_node[0] == "Elemop":
            # check for changes after Elemop's (interpolators)
            eop_data = self.get_data_from_eop(template)
            data_attrs = dict(eop_data[1])
            old_ipol = data_attrs.get("ipol")
            if old_ipol:
                data_attrs["ipol"] = self.changes["ipol"].get(old_ipol, old_ipol)
            eop_data[1] = list(data_attrs.items())
        elif new_node[0] == "Operation":
            op_eops = self.get_elementary_operations_from_operation(new_node)
            new_op_eops = []
            for eops in op_eops:
                new_eops = []
                ipol = None
                serialized = []
                for eop in eops:
                    eop_name = self.changes["eop"].get(eop, eop)
                    eop_node = self.get_with_name("Elemop", eop_name)
                    this_ipol = self.get_ipol_from_eop(eop_node)
                    if ipol is None:
                        ipol = this_ipol
                    elif ipol != this_ipol:
                        print("warning: different ipols in one operation %r line! serializing!" % new_name, ipol, this_ipol)
                        serialized.append(eop_name)
                        continue
                    new_eops.append(eop_name)
                new_eops = " ".join(new_eops)
                new_op_eops.append(
                    ["Elemop", [("ref", new_eops)], []])
                for name in serialized:
                    new_op_eops.append(
                        ["Elemop", [("ref", name)], []])
                        
            new_node[2] = new_op_eops
        elif new_node[0] == "Task":
            print(new_node)
            new_ops = []
            for object, op in self.new_ops:
                attrs = {
                    "ref": op
                    }
                if object:
                    attrs["object"] = object
                new_ops.append(
                    ["Operation", list(attrs.items()), []])
            new_node[2] = new_ops
        new_node[1] = list(new_attrs.items())
        self.top[2].append(new_node)
        return new_name
        
    def _compare_nodes(self, opa, opb):
        a = list(opa[1])
        a.sort()
        b = list(opb[1])
        b.sort()
        if a != b:
            return False
        ca = opa[2]
        cb = opb[2]
        ia = 0
        ib = 0
        while True:
            while ia < len(ca) and type(ca[ia]) != list:
                ia += 1
            while ib < len(cb) and type(cb[ib]) != list:
                ib += 1
            if ia >= len(ca) and ib >= len(cb):
                return True
            if ia >= len(ca):
                return False
            if ib >= len(cb):
                return False
            if not self._compare_nodes(ca[ia], cb[ib]):
                return False
            ia += 1
            ib += 1
        
    def get_with_name(self, what, name):
        top = self._search_element("Top", self.root, recurse=True)
        return self._search_element(what, top, attrs={"name": name})

    def get_elementary_operations(self):
        ops = []
        nodes = self._search_all_elements("Elemop", self.top, recurse=False)
        for n in nodes:
            ops.append(dict(n[1])["name"])
        return ops

    def get_operations(self):
        ops = []
        nodes = self._search_all_elements("Operation", self.top, recurse=False)
        for n in nodes:
            ops.append(dict(n[1])["name"].strip())
        return ops

    def get_tasks(self):
        ops = []
        nodes = self._search_all_elements("Task", self.top, recurse=False)
        for n in nodes:
            ops.append(dict(n[1])["name"])
        return ops

    def get_operations_from_task(self, task):
        ops = []
        for child in task[2]:
            if type(child) != list:
                continue
            a = dict(child[1])
            ops.append(
                (a.get("object"), a.get("ref").strip()))
        return ops

    def get_elementary_operations_from_operation(self, op):
        eops = []
        for child in op[2]:
            if type(child) != list:
                continue
            a = dict(child[1])
            eops.append(
                a["ref"].strip().split(" "))
        return eops

    def get_data_from_eop(self, eop):
        return self._search_element(None, eop, recurse=False)

    def get_ipol_from_eop(self, eop):
        ipol = self.get_data_from_eop(eop)
        return dict(ipol[1]).get("ipol")

    def _find_object_name(self, name, postfix):
        counter = None
        while True:
            if counter is None:
                test_name = name
                counter = 0
            elif counter == 0:
                test_name = name + postfix
                counter += 1
            else:
                test_name = "%s%s_%s" % (
                    name, postfix, counter)
                counter += 1
            if not self._has_object(test_name):
                return test_name
        
    def _has_object(self, name):
        def has_name(node, name):
            attrs = dict(node[1])
            if node[0] not in ("Joints", "Actuator"):
                if attrs.get("name") == name: return True
            for child in node[2]:
                if type(child) == list and has_name(child, name):
                    return True
            return False
        return has_name(self.root, name)

    def pretty_format(self):
        import traceback
        try:
            unpretty_format(self.root[2][0])
            pretty_format(self.root[2][0])
        except Exception:
            print(traceback.format_exc())
            

def unpretty_format(root):
    indent_string = " " * 4
    
    def unprettify(node):
        to_del = []
        for i, c in enumerate(node[2]):
            if isinstance(c, (str, unicode)):
                if not c.strip("\n\r \t"):
                    to_del.insert(0, i)
                continue
            unprettify(c)
        for i in to_del:
            del node[2][i]
    unprettify(root)
    
def pretty_format(root):
    indent_string = " " * 4
    
    def prettify(node, indent=1):
        try:
            ln2 = len(node[2])
        except Exception:
            pprint.pprint(node)
            raise
        if ln2:
            if node[0] is None:
                indent -= 1
            skip = False
            has_non_string_childs = False
            for i, c in enumerate(node[2]):
                if isinstance(c, (str, unicode)) or skip:
                    skip = False
                    continue
                has_non_string_childs = True
                try:
                    prettify(c, indent=indent + 1)
                except Exception:
                    pprint.pprint(node)
                    raise
                node[2].insert(i, "\n" + (indent_string * indent))
                skip = True
            if has_non_string_childs:
                node[2].append("\n" + indent_string * (indent - 1))
    prettify(root)
