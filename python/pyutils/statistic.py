"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt
"""
from __future__ import print_function

from math import sqrt

class average:
	def __init__(self, count):
		self.count = count
		self.data = []
		self.sum = 0

	def add(self, value):
		self.data.append(value)
		self.sum += value
		while len(self.data) > self.count:
			self.sum -= self.data[0]
			del self.data[0]

	def get_avg(self):
		return float(self.sum) / float(len(self.data))

	def __call__(self):
		return self.get_avg()

class first_two_moments:
	def __init__(self):
		self.data = []
		self.sum = 0
		self.count = 0
		self.mean = None
		self.std_deviation = None
		self.min_value = self.max_value = None

	def add(self, value):
		self.data.append(value)
		self.sum += value
		self.count += 1

		if self.min_value is None or value < self.min_value:
			self.min_value = value
		if self.max_value is None or value > self.max_value:
			self.max_value = value
		
		self.mean = None
		self.std_deviation = None

	def get_mean(self):
		self.mean = float(self.sum) / float(len(self.data))
		return self.mean
	
	def get_std_deviation(self):
		if self.mean is None:
			self.get_mean()
		variance = 0
		for v in self.data:
			variance += (v - self.mean) ** 2
		variance = variance / len(self.data)
		self.std_deviation = sqrt(variance)
		return self.std_deviation

	def print_stat(self, name, scale=1):
		print(" %s min, mean, max: %9.4f >= %9.4f >= %9.4f. std deviation: %9.4f. max-min: %9.4f" % (
			name,
			self.min_value * scale,
			self.get_mean() * scale,
			self.max_value * scale,
			self.get_std_deviation() * scale,
			(self.max_value - self.min_value) * scale))

	def __call__(self):
		return self.get_mean(), self.get_std_deviation()
	
