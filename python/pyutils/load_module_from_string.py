"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt
"""

import linecache
import os
import sys
import types

def get_code_object_from_string(data, fn=None, skip_frames=0):
    if fn is None:
        try:
            raise ZeroDivisionError
        except ZeroDivisionError:
            f = sys.exc_info()[2].tb_frame.f_back
            for i in range(skip_frames):
                f = f.f_back
        if f is not None:
            fn = "imported from %s:%d" % (f.f_code.co_filename, f.f_lineno)
        else:
            fn = "special_string_code_%s" % hash(data)

    lines = data.split("\n")
    linecache.cache[os.path.basename(fn)] = linecache.cache[fn] = len(data), None, lines, fn
    co = compile(data, fn, 'exec')
    return co, fn

def exec_string(data, env=None, fn=None):
    if env is None:
        env = {}
    co, fn = get_code_object_from_string(data, fn=fn, skip_frames=1)
    exec(co, env)

def load_module_from_string(modname, data, env=None, fn=None):
    if env is None:
        env = {}
    co, fn = get_code_object_from_string(data, fn=fn, skip_frames=1)

    m = types.ModuleType(modname)
    m.__dict__['__file__'] = fn
    sys.modules[modname] = m
    m.__dict__.update(env)
    
    exec(co, m.__dict__)
    return m
