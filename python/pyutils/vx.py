# -*- mode: python -*-
"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt
"""

import os
import sys
import signal
import telnetlib
import io

if sys.version_info >= (3, 0):
    unicode = str
    long = int
    raw_input = input

class vx_telnet(object):
    def __init__(self, host, port=0):
        self.host = host
        self.conn = telnetlib.Telnet(host, port)
        self.prompts = ['[vxWorks *]# ']
        self.output = ""
        self.had_prompt = False
        self.prepend_dir = False

    def _output(self, data):
        sys.stdout.write(data)
        sys.stdout.flush()

    def on_sigint(self, signo, frame):
        self.had_sigint = True
        
    def wait_string(self, waitfor):
        output = io.StringIO()
        if isinstance(waitfor, (str, unicode)):
            waitfor = [waitfor]
        while True:
            data = self.conn.read_some()
            output.write(data)
            for search in waitfor:
                if search in output.getvalue():
                    return search, output.getvalue()
        
    def wait_prompt(self, do_print=False, skip_first_line=False, pass_sigint=True, debug=False):
        if pass_sigint:
            self.had_sigint = False
            self.org_signal_handler = signal.signal(signal.SIGINT, self.on_sigint)
        self.had_prompt = False
        printed = 0
        had_cr = False
        had_first_line = False
        first_data = True
        while True:
            try:
                data = self.conn.read_some()
            except Exception:
                if "Interrupted system call" in str(sys.exc_info()[1]) and self.had_sigint:
                    self.conn.write("\003") # send SIGINT to vxworks
                    continue
                raise
            if data == "":
                raise Exception("eof")
            p = 0
            while p < len(data):
                if not had_cr:
                    np = data.find("\r", p)
                else:
                    if data[p] != "\n":
                        data = data[:p] + "\n" + data[p:]
                    had_cr = False
                    p = p + 1
                    continue
                if np == -1:
                    break
                had_cr = True
                p = np + 1
            if skip_first_line and not had_first_line and "\n" in data:
                # found first line!
                p = data.find("\n")
                had_first_line = True
                if skip_first_line:
                    data = data[p+1:]
                    skip_first_line = False
            if self.prepend_dir and first_data:
                first_data = False
                data = "<< " + data
            if self.prepend_dir and "\n" in data:
                data = data.replace("\n", "\n<< ")
            if not skip_first_line:
                self.output += data
            for prompt in self.prompts:
                if debug:
                    print("self.output: %r" % self.output)
                if self.output.endswith(prompt):
                    self.output = self.output[:-len(prompt)]
                    if do_print:
                        non_printed = len(self.output) - printed
                        if non_printed > 0:
                            self._output(self.output[-non_printed:])
                        else:
                            self._output("\r")
                    self.had_prompt = True
                    break
            if self.had_prompt:
                break
            if do_print and not skip_first_line:
                self._output(data)
                printed = len(self.output)
        if pass_sigint:
            signal.signal(signal.SIGINT, self.org_signal_handler)
        return self.output

    def run_command(self, cmd, do_print=True, wait=True):
        if not self.had_prompt:
            self.wait_prompt()
        self.output = ""
        self.conn.write("%s\n" % cmd)
        if not wait:
            return
        return self.wait_prompt(do_print=do_print, skip_first_line=True)

if __name__ == "__main__":
    host = sys.argv[1]
    batchfile = None
    batch = False
    vx = vx_telnet(host)
    for i, arg in enumerate(sys.argv):
        if arg == "--batchfile":
            batchfile = sys.argv[i + 1]
            del sys.argv[i]
            del sys.argv[i]
            continue
        if arg == "--batch":
            batch = True
            del sys.argv[i]
            continue

    if batch is True:
        # read commands from stdin line-by-line and execute them
        #vx.prepend_dir = True
        while True:
            try:
                cmd = raw_input()
            except Exception:
                if "EOF" in str(sys.exc_info()[0]):
                    break
                raise
            cmd = cmd.strip()
            if not cmd or cmd.startswith("#"):
                continue
            print("\r# %s" % cmd)
            vx.run_command(cmd)
        sys.exit(0)

    if batchfile is not None:
        #vx.prepend_dir = True
        for cmd in open(batchfile):
            cmd = cmd.strip()
            if not cmd or cmd.startswith("#"):
                continue
            print("\r# %s" % cmd)
            vx.run_command(cmd)
        sys.exit(0)

    do_cd = True
    if do_cd:
        cwd = os.getcwd()
        ret = vx.run_command("cd %s" % cwd, do_print=False)
        if ret:
            raise Exception("while trying to cd into %r we got return value:\n%s" % (cwd, ret))

    cmd = " ".join(sys.argv[2:])
    vx.run_command(cmd)
