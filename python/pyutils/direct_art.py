from __future__ import print_function

import time
import select
import socket
import pprint

import numpy as np

class art_packet:
	def __init__(self):
		self.bodies = {}
		self.marker = []
	def __str__(self):
		return pprint.pformat(self.__dict__)

class art:
	def __init__(self, server, send_port=50105, recv_port=5001):
		self.server = server
		self.send_port = send_port
		self.recv_port = recv_port
		self.fd_recv = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.fd_recv.bind(("", recv_port))
		self.mapping = {
			"fr": ("frame_id", int),
			"ts": "timestamp",
			"6d": ("bodies", self._read_bodies),
			"3d": ("marker", self._read_markers),
			}

	def __exit__(self):
		self.fd_recv.close()
		
	def start_measurement(self):
		response = self._command("dtrack2 tracking start\x00")
		
		result = response.split()[1]
		if result == "ok\x00":
			print("tracking started successfully")
		else:
			print("error while trying to start tracking")
			
			result = response.split()[2]
			if result == "-5":
				print("tracking already started")
			elif result == "-11":
				print("other user connected to server")

	def stop_measurement(self):
		response = self._command("dtrack2 tracking stop\x00")
		
		result = response.split()[1]
		if result == "ok\x00":
			print("tracking stopped successfully")
		else:
			print("error while trying to stop tracking")
			
			result = response.split()[2]
			if result == "-5":
				print("tracking already stopped")
			elif result == "-11":
				print("other user connected to server")

	def _command(self, command):
		fd_send = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		fd_send.connect((self.server, self.send_port))
		
		fd_send.send(command)
		data = fd_send.recv(1024)
		
		fd_send.close()
		
		return data

	def receive(self, timeout=0.03):
		rfd, sfd, efd = select.select([self.fd_recv], [], [], timeout)
		if not rfd:
			return None
		data = self.fd_recv.recv(1024)
		p = art_packet()
		data = data.strip().split("\r\n")
		for line in data:
			try:
				kw, value = line.split(" ", 1)
			except Exception:
				raise ValueError("received invalid line from dTrack: %r" % line)
			kw_map = self.mapping.get(kw, None)
			if kw_map is None:
				continue
			if type(kw_map) == tuple and len(kw_map) == 2:
				attr, method = kw_map
			else:
				attr = kw_map
				method = float
			setattr(p, attr, method(value))
		return p

	def _read_bodies(self, value):
		try:
			count, value = value.split(" ", 1)
		except Exception:
			return {}
		bodies = {}
		for values in value.strip("[] ").split("] ["):
			values = values.replace("][", " ").split(" ")
			bodies[int(values[0])] = (
				list(map(float, values[2:5])),             # loc
				list(map(float, values[5:8])),             # rot
				np.array(list(map(float, tuple(values[8:8+3*3])))).reshape(3, 3).transpose()) # rot mat
		return bodies

	def _read_markers(self, value):
		count, value = value.split(" ", 1)
		count = int(count)
		if not count:
			return []
		markers = [None] * count
		for i, values in enumerate(value.strip("[] ").split("] [")):
			values = values.replace("][", " ").split(" ")
			markers[i] = (int(values[0]), tuple(map(float, values[2:])))
		return markers
	
if __name__ == "__main__":
	# create sensor object
	# config sensor, default tcpport_send=50105, udpport_rec=5001
	s = art("129.247.181.217")
	s.start_measurement()
	from . import fps_counter
	fps = fps_counter.fps_counter()
	count = 0
	time.sleep(10)
	last = 0
	while True:
		data = s.receive(0)
		if data is None:
			break
		count += 1
		diff = data.timestamp - last
		print("%5d %.2f %5.1f/s" % (count, data.timestamp, 1 / diff))
		last = data.timestamp
