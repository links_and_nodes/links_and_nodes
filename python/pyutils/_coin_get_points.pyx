# -*- mode: python -*-

import numpy as np
cimport numpy as np

cdef extern from "swig_util.h":
    void* get_ptr(object data)

cdef extern from "coin_get_points.h":
    ctypedef struct c_SoNode "SoNode":
        pass
    double* coin_get_points(void* base, int* n_points)

def get_points(object base):
    return _get_points(base)

cdef _get_points(object base):
    cdef int n_points
    cdef double* points_ptr
    points_ptr = coin_get_points(<c_SoNode*>get_ptr(base), &n_points);
    cdef np.ndarray[double, ndim=2] points = np.empty((n_points, 3), dtype=float)
    cdef int p
    cdef int i
    for p in xrange(n_points):
        for i in xrange(3):
            points[p, i] = points_ptr[p * 3 + i];
    return points


