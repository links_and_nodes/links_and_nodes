"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt
"""

from __future__ import print_function

import os
import math
import time
import traceback
import threading

import pyutils

# By default, Gdk loads the OpenGL 3.2 Core profile. However, PyCAM's rendering
# code uses the fixed function pipeline, which was removed in the Core profile.
# So we have to resort to this semi-public API to ask Gdk to use a Compatibility
# profile instead.
os.environ['GDK_GL'] = 'legacy'
# stolen from https://github.com/SebKuzminsky/pycam/pull/140/files
import gi # noqa: E402
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, Gdk # noqa: E402

import OpenGL.GL as gl # noqa: E402
import pivy # noqa: E402
import pivy.coin as coin # noqa: E402

is_newer_pivy = hasattr(pivy, "__version__") and tuple(map(int, pivy.__version__.split("."))) >= (0, 6, 5)


for i in range(5):
    globals()["BUTTON%d" % (i+1)] = 2**i

class SoPyGtkViewer(Gtk.GLArea, pyutils.hooked_object):
    def __init__(self, debug=False):
        self._nav_styles = "Inventor", "FreeCAD", "Ogre"
        navigation_style = os.getenv("PYUTILS_COIN_NAVIGATION_STYLE", self._nav_styles[0])
        self.before_seek_mode = "IDLE"
        self.get_depth_buffer = False
        self.additional_render_callback = None
        self.depth_buffer_type = gl.GL_UNSIGNED_BYTE
        self.special_motion_handler = None
        self.notify_keypress = None
        self.notify_popup = None
        self.on_before_default_popup = None
        self.on_interact_button = None
        self.clear_color = None
        self.debug = debug

        register_sensor_manager()

        # init gtk base widget
        Gtk.GLArea.__init__(self)
        pyutils.hooked_object.__init__(self)

        self.cross_cursor = Gdk.Cursor(Gdk.CursorType.CROSSHAIR)
        self.hand_cursor = Gdk.Cursor(Gdk.CursorType.HAND2)
        self.pointer_cursor = Gdk.Cursor(Gdk.CursorType.LEFT_PTR)
        self.interact_cursor = Gdk.Cursor(Gdk.CursorType.SIZING)

        # init event handlers
        self.connect("resize", self.on_resize)
        self.connect("realize", self.on_realize)
        self.connect("render", self.on_render)

        self.connect("button-press-event", self.on_button)
        self.connect("button-release-event", self.on_button)
        self.connect("scroll-event", self.on_scroll)
        self.connect("motion-notify-event", self.on_motion)
        self.connect("key-press-event", self.on_keypress, "press")
        self.connect("key-release-event", self.on_keypress, "release")
        self.add_events(Gdk.EventMask.BUTTON_PRESS_MASK | Gdk.EventMask.BUTTON_RELEASE_MASK |
                        Gdk.EventMask.BUTTON_MOTION_MASK | Gdk.EventMask.KEY_PRESS_MASK |
                        Gdk.EventMask.KEY_RELEASE_MASK |  Gdk.EventMask.SCROLL_MASK)

        # init default popup menu
        m = self.default_popup_menu = Gtk.Menu() # user are invited to extend this
        item = Gtk.MenuItem("view All")
        item.connect("activate", lambda *args: self.viewAll())
        m.append(item)
        item = Gtk.MenuItem("navigation styles")
        sub = Gtk.Menu()
        item.set_submenu(sub)
        m.append(item)
        self._nav_style_btns = []
        first_item = None
        for style in self._nav_styles:
            item = Gtk.RadioMenuItem(style)
            def handler(item, style=style):
                self.set_navigation_style(style)
                return True
            item.connect("activate", handler)
            self._nav_style_btns.append(item)
            sub.append(item)
            if first_item is None:
                first_item = item
            else:
                item.join_group(first_item)
        m.show_all()

        self.navigation_style = None
        self.set_navigation_style(navigation_style)

        self.manager = None
        self.after_got_manager = None

    def set_navigation_style(self, style):
        if style not in self._nav_styles:
            raise Exception("unknown navigation style %r!" % style)
        if self.navigation_style == style:
            return
        self.navigation_style = style
        idx = self._nav_styles.index(style)
        self._nav_style_btns[idx].set_active(True)
        return True

    def _init_coin(self, ctx):
        if not ctx.is_legacy():
            print("\n\nWARNING: this Gtk.GLArea's GLContext is not marked as 'legacy'!\n"
                  "but this is needed for SoPyGtkViewer to work!\n"
                  "you need to set the environment variable GDK_GL=legacy BEFORE first import of Gtk!\n")

        # initialize scenemanager instance
        self._render_action = None
        self.manager = coin.SoSceneManager()
        self.manager.activate()
        self.manager.setRenderCallback(self.render_callback, None)
        self.camera = None
        # actions
        self.searchaction = coin.SoSearchAction()
        self.matrixaction = coin.SoGetMatrixAction(coin.SbViewportRegion(100,100))
        # clipping
        self.autoclipvalue = 0.6
        # init dragging
        self.buttons = 0
        self.log_size = 16
        self.log = []
        self.spinprojector = coin.SbSphereSheetProjector(coin.SbSphere(coin.SbVec3f(0, 0, 0), 0.8))
        volume = coin.SbViewVolume()
        volume.ortho(-1, 1, -1, 1, -1, 1)
        self.spinprojector.setViewVolume(volume)
        self.currentmode = "IDLE"
        self.set_property("can-focus", True)
        # hack
        self.drop_events = False
        self.skip_event = 3

        if self.after_got_manager:
            self.after_got_manager()
            self.call_hook("after_got_manager")

    def setCamera(self, cam):
        self.camera = cam

    def getCamera(self):
        return self.camera

    def setSceneGraph(self, root):
        # search camera
        action = coin.SoSearchAction()
        action.setType(coin.SoCamera.getClassTypeId())
        action.apply(root)
        p = action.getPath()
        if not p:
            # add camera
            new_root = coin.SoSeparator()
            new_root.ref()
            new_root.addChild(coin.SoDirectionalLight())
            self.camera = coin.SoPerspectiveCamera()
            new_root.addChild(self.camera)
            new_root.addChild(root)
            root = new_root
        else:
            self.camera = p.getTail()
            #print("found camera:", self.camera.getTypeId().getName())
        self.manager.setSceneGraph(root)
        self.sceneroot = root
        self.camera.viewAll(root, self.manager.getViewportRegion())
        self.call_hook("camera_changed")

    def viewAll(self):
        self.camera.viewAll(self.sceneroot, self.manager.getViewportRegion())
        self.call_hook("camera_changed")

    def getSceneGraph(self):
        return self.sceneroot

    def getSceneManager(self):
        return self.manager

    def on_realize(self, gl):
        # We need to make the context current if we want to
        # call GL API
        self.make_current()
        # If there were errors during the initialization or
        # when trying to make the context current, this
        # function will return a #GError for you to catch
        err = self.get_error()
        if err is not None:
            print("gl error after make_current: %r" % err)
        
        self.set_has_depth_buffer(True)
        self.set_double_buffered(True)
        err = self.get_error()
        if err is not None:
            print("gl error after enable dbl-buf & depth-buf: %r" % err)

        # for slow scene-rendering: self.set_auto_render(False)
        #self.set_auto_render(True)

        self.is_first = True
        return True
        
    def on_resize(self, area, width, height):
        self.width, self.height = width, height
        if self.manager is not None:
            self.manager.setWindowSize(coin.SbVec2s(self.width, self.height))

    def render_callback(self, userData, scene_manager):
        self.redraw_canvas()

    def on_render(self, area, ctx):
        if self.is_first:
            self.is_first = False
            if self.debug:
                print(("on_render:\n"
                       "  gl context version: %r\n"
                       "  is_legacy: %r\n"
                       "  has depth buffer: %r\n"
                       "  has alpha: %r\n"
                       "  double buffered: %r\n"
                       "  use_es: %r") % (
                           ctx.get_version(), # 4.6 with GDK_GL=legacy
                           ctx.is_legacy(),
                           self.get_has_depth_buffer(),
                           self.get_has_alpha(),
                           self.get_double_buffered(),
                           self.get_use_es(),
                ))
            self._init_coin(ctx)
            self.manager.setWindowSize(coin.SbVec2s(self.width, self.height))

        #self.attach_buffers()
        
        gl.glEnable(gl.GL_DEPTH_TEST)
        if self.clear_color is not None:
            gl.glClearColor(*self.clear_color)
            gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
        else:
            gl.glClearColor(0, 0, 0, 1)
            gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

        # auto clipping
        self.setClippingPlanes()
        # real rendering
        if self._render_action:
            self._render_action.setViewportRegion(self.manager.getGLRenderAction().getViewportRegion())
            self.manager.render(self._render_action)
        else:
            self.manager.render()

        if self.get_depth_buffer:
            self.last_depth_buffer = gl.glReadPixels(0, 0, self.width, self.height, gl.GL_DEPTH_COMPONENT, self.depth_buffer_type)

        if self.additional_render_callback:
            try:
                self.additional_render_callback(self)
            except Exception:
                print("additional_render_callback threw exception:\n%s" % traceback.format_exc())
                self.additional_render_callback = None

        return True

    def redraw_canvas(self):
        self.queue_render()
        #self.window.process_updates(True)

    def set_mode(self, mode):
        #if mode != self.currentmode: print("change mode from %r to %r" % (self.currentmode, mode))
        self.currentmode = mode

        if mode in ("DRAGGING", "PANNING"):
            self.log = [] # release log
            cursor = self.hand_cursor
        elif mode == "SEEK_WAIT_MODE":
            cursor = self.cross_cursor
        elif mode == "INTERACT":
            cursor = self.interact_cursor
        else: # IDLE, ZOOMING
            cursor = self.pointer_cursor

        self.get_window().set_cursor(cursor)

    def _send_event_down_the_scene_graph(self, event):
        ry = self.height - event.y
        # send event down to scene graph
        ev = coin.SoMouseButtonEvent()
        ev.setButton(coin.SoMouseButtonEvent.BUTTON1 + event.button - 1)
        if event.type == Gdk.EventType.BUTTON_PRESS:
            ev.setState(coin.SoButtonEvent.DOWN)
        if event.type == Gdk.EventType.BUTTON_RELEASE:
            ev.setState(coin.SoButtonEvent.UP)
        ev.setPosition(coin.SbVec2s(int(event.x), int(ry)))
        t = coin.SbTime()
        t.setMsecValue(event.get_time())
        ev.setTime(t)
        s = event.get_state()
        ev.setShiftDown(s & Gdk.ModifierType.SHIFT_MASK)
        ev.setCtrlDown(s & Gdk.ModifierType.CONTROL_MASK)
        ev.setAltDown(s & Gdk.ModifierType.MOD1_MASK)
        # print("interact button")
        self.manager.setViewportRegion(coin.SbViewportRegion(self.width, self.height))
        self.manager.processEvent(ev)
        return True

    def _send_scroll_event_down_the_scene_graph(self, event):
        ry = self.height - event.y
        # send event down to scene graph
        # does not work,
        # we manager.processEvent does not pass throu buttons 4 and 5
        # and we have no real release event
        ev = coin.SoMouseButtonEvent()
        if event.direction == Gdk.ScrollDirection.UP:
            ev.setButton(coin.SoMouseButtonEvent.BUTTON4)
        else:
            ev.setButton(coin.SoMouseButtonEvent.BUTTON5)
        if event.type == Gdk.EventType.BUTTON_PRESS:
            ev.setState(coin.SoButtonEvent.DOWN)
        ev.setPosition(coin.SbVec2s(int(event.x), int(ry)))
        t = coin.SbTime()
        t.setMsecValue(event.get_time())
        ev.setTime(t)
        s = event.get_state()
        ev.setShiftDown(s & Gdk.ModifierType.SHIFT_MASK)
        ev.setCtrlDown(s & Gdk.ModifierType.CONTROL_MASK)
        ev.setAltDown(s & Gdk.ModifierType.MOD1_MASK)
        self.manager.processEvent(ev)
        return True

    def on_button(self, da, event):
        self.grab_focus()
        ry = self.height - event.y
        self.last_normalized_position = coin.SbVec2f(
            event.x / float(max(self.width - 1, 1)),
            ry / float(max(self.height - 1, 1)))

        is_press = event.type == Gdk.EventType.BUTTON_PRESS
        mask = 1 << (event.button - 1)
        if is_press:
            self.buttons |= mask
            self._last_press_pos = event.x, event.y
        else:
            self.buttons &= ~mask

        if is_press and self.buttons == 0b100 and self.notify_popup:
            return self.notify_popup(event)

        elif not is_press and self.buttons == 0b000 and event.button == 3 and not self.notify_popup and self._last_press_pos == (event.x, event.y): # show default popup menu
            if self.on_before_default_popup:
                 self.on_before_default_popup(event)
            self.default_popup_menu.popup_at_pointer(event)
            return True

        if self.currentmode == "SEEK_WAIT_MODE":
            if event.type == Gdk.EventType.BUTTON_PRESS:
                if self.navigation_style == "FreeCAD":
                    factor = 0.5 # 1
                else:
                    factor = 0.5
                self.seekToPoint(event.x, ry, factor=factor)
                return

        if self.currentmode == "INTERACT":
            if self.on_interact_button is not None:
                if self.on_interact_button(event):
                    return
            self._send_event_down_the_scene_graph(event)
            return
        if self.navigation_style == "Inventor":
            if self.buttons & coin.SoMouseButtonEvent.BUTTON1 and self.buttons & coin.SoMouseButtonEvent.BUTTON2:
                self.set_mode("ZOOMING")
            elif self.buttons & coin.SoMouseButtonEvent.BUTTON1:
                self.set_mode("DRAGGING")
            elif self.buttons & coin.SoMouseButtonEvent.BUTTON2:
                self.set_mode("PANNING")
            else:
                self.set_mode("IDLE")

        elif self.navigation_style == "FreeCAD":
            if is_press:
                if (event.button == 1 or event.button == 3) and self.currentmode == "PANNING":
                    # orient around current point!
                    self.set_mode("DRAGGING")
                elif event.button == 1:
                    # interact click
                    if self.on_interact_button is not None:
                        if self.on_interact_button(event):
                            return
                    self._send_event_down_the_scene_graph(event)
                elif event.button == 2:
                    # pan
                    #self.seekToPoint(event.x, ry, factor=1)
                    self.set_mode("PANNING")
            else:
                if event.button == 1 and self.currentmode == "DRAGGING":
                    # back from dragging to panning
                    self.set_mode("PANNING")
                else:
                    self.set_mode("IDLE")

        elif self.navigation_style == "Ogre":
            if is_press:
                if event.button == 1:
                    self.set_mode("DRAGGING")
                elif event.button == 2:
                    self.set_mode("PANNING")
                elif event.button == 3:
                    self.set_mode("ZOOMING")
            else:
                self.set_mode("IDLE")
        return True
        
    def on_scroll(self, da, event):
        ry = self.height - event.y
        if self.currentmode == "INTERACT":
            return self._send_scroll_event_down_the_scene_graph(event)

        if self.navigation_style == "Inventor":
            if event.direction == Gdk.ScrollDirection.UP:
                self.zoom(0.1) # SoGuiFullViewerP::zoom(this->getCamera(), 0.1f);
            if event.direction == Gdk.ScrollDirection.DOWN:
                self.zoom(-0.1) # SoGuiFullViewerP::zoom(this->getCamera(), -0.1f);
        elif self.navigation_style == "FreeCAD":
            if event.direction == Gdk.ScrollDirection.UP:
                self.zoom(-0.1)
            if event.direction == Gdk.ScrollDirection.DOWN:
                self.zoom(0.1)
        elif self.navigation_style == "Ogre":
            if event.direction == Gdk.ScrollDirection.UP:
                self.zoom(-0.05)
            if event.direction == Gdk.ScrollDirection.DOWN:
                self.zoom(0.05)
        
    def on_motion(self, da, event):
        if self.drop_events:
            return
        ry = self.height - event.y
        posn = coin.SbVec2f(event.x / float(max(self.width - 1, 1)),
                       ry / float(max(self.height - 1, 1)))
        prevnormalized = self.last_normalized_position
        self.last_normalized_position = posn
        
        if self.currentmode == "INTERACT":
            # send event down to scene graph
            self.skip_event = 10
            ev = coin.SoLocation2Event()
            #ev.setPosition(SbVec2s(int(event.x), int(ry)))
            ev.setPosition(coin.SbVec2s(int(event.x), int(ry)))
            t = coin.SbTime(time.time())
            #t.setMsecValue(event.get_time())
            #now = time.time()
            #t.setMsecValue(int(1000 * (now - int(now))))
            ev.setTime(t)
            s = event.get_state()
            ev.setShiftDown(s & Gdk.ModifierType.SHIFT_MASK)
            ev.setCtrlDown(s & Gdk.ModifierType.CONTROL_MASK)
            ev.setAltDown(s & Gdk.ModifierType.MOD1_MASK)
            #print("interact motion")
            self.manager.setViewportRegion(coin.SbViewportRegion(self.width, self.height))
            if self.special_motion_handler:
                self.special_motion_handler(ev)
            else:
                self.manager.processEvent(ev)

        elif self.currentmode == "DRAGGING":
            self.addToLog((event.x, ry), event.get_time())
            self.spin(posn)
        elif self.currentmode == "PANNING":
            aspect = self.width / float(self.height)
            cam = self.camera
            vv = cam.getViewVolume(aspect)
            panningplane = vv.getPlane(cam.focalDistance.getValue())
            self.pan(aspect, panningplane, posn, prevnormalized)
        elif self.currentmode == "ZOOMING":
            self.zoomByCursor(posn, prevnormalized)
        
    def on_keypress(self, viewer, event, event_type):
        press = event_type == "press"
        if press and event.keyval == Gdk.KEY_Escape and self.currentmode == "INTERACT":
            self.set_mode("IDLE")
        elif press and event.keyval == Gdk.KEY_Escape and self.currentmode != "INTERACT":
            self.set_mode("INTERACT")
        elif press and event.string == "s" and self.currentmode != "SEEK_WAIT_MODE":
            self.before_seek_mode = self.currentmode
            self.set_mode("SEEK_WAIT_MODE")
        elif press and event.string == "s" and self.currentmode == "SEEK_WAIT_MODE":
            self.set_mode(self.before_seek_mode)
        elif press and self.currentmode == "INTERACT":
            # send event down to scene graph
            ev = coin.SoKeyboardEvent()
            ev.setState(press)
            t = coin.SbTime()
            t.setMsecValue(event.get_time())
            ev.setTime(t)
            s = event.get_state()
            ev.setShiftDown(s & Gdk.ModifierType.SHIFT_MASK)
            ev.setCtrlDown(s & Gdk.ModifierType.CONTROL_MASK)
            ev.setAltDown(s & Gdk.ModifierType.MOD1_MASK)
            ev.setKey(self._map_key(event))
            ev.setPrintableCharacter(event.string)
            self.manager.processEvent(ev)
        elif press and self.notify_keypress is not None:
            self.notify_keypress(event.string)

    def _map_key(self, ev):
        gk = ev.keyval
        if gk >= Gdk.KEY_A and gk <=Gdk.KEY_Z:
            ck = gk + 0x020
        else:
            ck = gk
        return ck

    def seekToPoint(self, x, y, factor=0.5):
        """
        Call this method to initiate a seek action towards the 3D
        intersection of the scene and the ray from the screen coordinate's
        point and in the same direction as the camera is pointing.
        
        Returns \c TRUE if the ray from the \a screenpos position intersect
        with any parts of the onscreen geometry, otherwise \c FALSE.
        """
        if self.camera is None: return False

        rpaction = coin.SoRayPickAction(self.getViewportRegion())
        screenpos = coin.SbVec2s(int(x), int(y))
        rpaction.setPoint(screenpos)
        rpaction.setRadius(2)
        rpaction.apply(self.sceneroot)
        picked = rpaction.getPickedPoint()
        if not picked: return False
        
        hitpoint = picked.getPoint()

        # move point to the camera coordinate system, consider
        # transformations before camera in the scene graph
        cameramatrix, camerainverse = self.getCameraCoordinateSystem(self.camera, self.sceneroot)
        hitpoint = camerainverse.multVecMatrix(hitpoint)

        fd = factor * (hitpoint - self.camera.position.getValue()).length()
        self.camera.focalDistance = fd
        newdir = hitpoint - self.camera.position.getValue()
        newdir.normalize()
        # find a rotation that rotates current camera direction into new
        # camera direction.
        olddir = self.camera.orientation.getValue().multVec(coin.SbVec3f(0, 0, -1))
        
        diffrot = coin.SbRotation(olddir, newdir)
        self.camera.position = hitpoint - fd * newdir
        self.camera.orientation = self.camera.orientation.getValue() * diffrot
        self.call_hook("camera_changed")

        self.set_mode(self.before_seek_mode)
        return True

    def getViewportRegion(self):
        return self.manager.getGLRenderAction().getViewportRegion()

    def getCameraCoordinateSystem(self, camera, root):
        """
        Returns the coordinate system the current camera is located in. If
        there are transformations before the camera in the scene graph,
        this must be considered before doing certain operations. \a matrix
        and \a inverse will not contain the transformations caused by the
        camera fields, only the transformations traversed before the camera
        in the scene graph.
        """
        self.searchaction.reset()
        self.searchaction.setSearchingAll(coin.TRUE)
        self.searchaction.setInterest(coin.SoSearchAction.FIRST)
        self.searchaction.setNode(camera)
        self.searchaction.apply(root)
        p = self.searchaction.getPath()
        if p:
            self.matrixaction.apply(p)
            matrix = self.matrixaction.getMatrix()
            inverse = self.matrixaction.getInverse()
        else:
            matrix = coin.SbMatrix.identity()
            inverse = coin.SbMatrix.identity()
        self.searchaction.reset()
        return matrix, inverse

    def setClippingPlanes(self):
        """
        Position the near and far clipping planes just in front of and
        behind the scene's bounding box. This will give us the optimal
        utilization of the z buffer resolution by shrinking it to its
        minimum depth.
        Near and far clipping planes are specified in the camera fields
        nearDistance and farDistance.
        """
        # This is necessary to avoid a crash in case there is no scene
        # graph specified by the user.
        if self.camera is None: return
        if not hasattr(self, "autoclipbboxaction"):
            self.autoclipbboxaction = coin.SoGetBoundingBoxAction(self.getViewportRegion())
        else:
            self.autoclipbboxaction.setViewportRegion(self.getViewportRegion())
        self.autoclipbboxaction.apply(self.sceneroot)
        # rotate bbox in camera coordinate system
        xbox = self.autoclipbboxaction.getXfBoundingBox()
        mat, inverse = self.getCameraCoordinateSystem(self.camera, self.sceneroot)
        xbox.transform(inverse)
        mat.setTranslate(-self.camera.position.getValue())
        xbox.transform(mat)
        mat.setRotate(self.camera.orientation.getValue().inverse())
        xbox.transform(mat)
        box = xbox.project()
        # Bounding box was calculated in camera space, so we need to "flip"
        # the box (because camera is pointing in the (0,0,-1) direction
        # from origo.
        nearval = -box.getMax()[2]
        farval = -box.getMin()[2]
        # Check if scene is completely behind us.
        if farval <= 0.0:
            return
        if self.camera.isOfType(coin.SoPerspectiveCamera.getClassTypeId()):
            # Disallow negative and small near clipping plane distance.
            # From glFrustum() documentation: Depth-buffer precision is
            # affected by the values specified for znear and zfar. The
            # greater the ratio of zfar to znear is, the less effective the
            # depth buffer will be at distinguishing between surfaces that
            # are near each other. If r = far/near, roughly log (2) r bits
            # of depth buffer precision are lost. Because r approaches
            # infinity as znear approaches zero, you should never set znear
            # to zero.
            depthbits = gl.glGetIntegerv(gl.GL_DEPTH_BITS)
            use_bits = int(float(depthbits) * (1.0 - self.autoclipvalue))
            r = 2.0 ** use_bits
            nearlimit = farval / r
            if nearlimit >= farval:
                # (The "5000" magic constant was found by fiddling around a bit
                # on an OpenGL implementation with a 16-bit depth-buffer
                # resolution, adjusting to find something that would work well
                # with both a very "stretched" / deep scene and a more compact
                # single-model one.)
                nearlimit = farval / 5000.0
            # adjust the near plane if the the value is too small.
            if nearval < nearlimit:
                nearval = nearlimit
            # Some slack around the bounding box, in case the scene fits
            # exactly inside it. This is done to minimize the chance of
            # artifacts caused by the limitation of the z-buffer
            # resolution. One common artifact if this is not done is that the
            # near clipping plane cuts into the corners of the model as it's
            # rotated.
            SLACK = 0.001
            # FrustumCamera can be found in the SmallChange CVS module. We
            # should not change the nearDistance for this camera, as this will
            # modify the frustum.
            if self.camera.getTypeId().getName() == "FrustumCamera":
                nearval = self.camera.nearDistance.getValue()
                farval *= (1.0 + SLACK)
                if farval <= nearval:
                    # nothing is visible, so just set farval to som value > nearval.
                    farval = nearval + 10.0
            else:
                nearval *= 1.0 - SLACK
                farval *= 1.0 + SLACK
        if nearval != self.camera.nearDistance.getValue():
            self.camera.nearDistance = nearval
        if farval != self.camera.farDistance.getValue():
            self.camera.farDistance = farval

    def zoomByCursor(self, thispos, prevpos):
        """
        Calculate a zoom/dolly factor from the difference of the current
        cursor position and the last.
        """
        # There is no "geometrically correct" value, 20 just seems to give
        # about the right "feel".
        if self.navigation_style == "Ogre":
            sign = -1
        else:
            sign = 1
        self.zoom(sign * ((thispos[1] - prevpos[1]) * 20.0))
        
    def addToLog(self, pos, time):
        """
        This method adds another point to the mouse location log, used for spin
        animation calculations.
        """
        if len(self.log) > 0 and pos == self.log[0]:
            return
        self.log.insert(0, (pos, time))
        while len(self.log) > self.log_size:
            del self.log[-1]
        
    def spin(self, pointerpos):
        """
        Uses the sphere sheet projector to map the mouseposition unto
        a 3D point and find a rotation from this and the last calculated point.
        """
        if len(self.log) < 2:
            return
        lastpos = self.log[1][0]
        lastpos = coin.SbVec2f((
            lastpos[0] / float(max(self.width - 1, 1)),
            lastpos[1] / float(max(self.height - 1, 1))))

        self.spinprojector.project(lastpos)
        r = coin.SbRotation()
        self.spinprojector.projectAndGetRotation(pointerpos, r)
        r.invert()
        self.reorientCamera(r)
        # todo
        # Calculate an average angle magnitude value to make the transition
        # to a possible spin animation mode appear smooth.
        #SbVec3f dummy_axis, newaxis;
        #float acc_angle, newangle;
        #self.spinincrement.getValue(dummy_axis, acc_angle)
        #acc_angle *= this->spinsamplecounter; // weight
        #r.getValue(newaxis, newangle);
        #acc_angle += newangle;
        
        #this->spinsamplecounter++;
        #acc_angle /= this->spinsamplecounter;
        # FIXME: accumulate and average axis vectors aswell? 19990501 mortene.
        #this->spinincrement.setValue(newaxis, acc_angle);
        # Don't carry too much baggage, as that'll give unwanted results
        # when the user quickly trigger (as in "click-drag-release") a spin
        # animation.
        #if (this->spinsamplecounter > 3) this->spinsamplecounter = 3;

    def reorientCamera(self, rot):
        """
        Rotate the camera by the given amount, then reposition it so we're
        still pointing at the same focal point.
        """
        cam = self.camera
        if cam is None: return
        # Find global coordinates of focal point.
        # SbVec3f direction; # todo remove
        direction = cam.orientation.getValue().multVec(coin.SbVec3f(0, 0, -1)) # todo test return
        focalpoint = cam.position.getValue() + cam.focalDistance.getValue() * direction
        # Set new orientation value by accumulating the new rotation.
        cam.orientation = rot * cam.orientation.getValue()
        # Reposition camera so we are still pointing at the same old focal point.
        direction = cam.orientation.getValue().multVec(coin.SbVec3f(0, 0, -1))
        cam.position = focalpoint - cam.focalDistance.getValue() * direction
        self.call_hook("camera_changed")

    def pan(self, aspectratio, panningplane, currpos, prevpos):
        # Move camera parallel with the plane orthogonal to the camera
        # direction vector.
        cam = self.camera
        if cam is None: return
        if currpos == prevpos: return # useless invocation
        
        # Find projection points for the last and current mouse coordinates.
        vv = cam.getViewVolume(aspectratio)
        
        line = vv.projectPointToLine(currpos)
        line = coin.SbLine(*line)
        if is_newer_pivy:
            current_planept = panningplane.intersect(line)
        else:
            current_planept = coin.SbVec3f() # output argument
            panningplane.intersect(line, current_planept)

        line = vv.projectPointToLine(prevpos)
        line = coin.SbLine(*line)
        old_planept = coin.SbVec3f() # output argument
        
        if is_newer_pivy:
            old_planept = panningplane.intersect(line)
        else:
            old_planept = panningplane.intersect(line)
        
        # Reposition camera according to the vector difference between the
        # projected points.
        cam.position = cam.position.getValue() - (current_planept - old_planept)
        self.call_hook("camera_changed")
        
    def zoom(self, diffvalue):
        cam = self.camera
        if cam is None:
            return # can happen for empty scenegraph
        t = cam.getTypeId()
        tname = t.getName()
        # This will be in the range of <0, ->>.
        multiplicator = float(math.exp(diffvalue))
        if t.isDerivedFrom(coin.SoOrthographicCamera.getClassTypeId()):
            # Since there's no perspective, "zooming" in the original sense
            # of the word won't have any visible effect. So we just increase
            # or decrease the field-of-view values of the camera instead, to
            # "shrink" the projection size of the model / scene.
            cam.height = cam.height.getValue() * multiplicator
            return
        # FrustumCamera can be found in the SmallChange CVS module (it's
        # a camera that lets you specify (for instance) an off-center
        # frustum (similar to glFrustum())
        if not t.isDerivedFrom(coin.SoPerspectiveCamera.getClassTypeId()) and tname != "FrustumCamera":
            print(("Warning: %r.zoom: Unknown camera type, will zoom by" +
                    "moving position, but this might not be correct.") % self)
        oldfocaldist = cam.focalDistance.getValue()
        newfocaldist = oldfocaldist * multiplicator
        direction = cam.orientation.getValue().multVec(coin.SbVec3f(0, 0, -1))
        oldpos = cam.position.getValue()
        newpos = oldpos + (newfocaldist - oldfocaldist) * -direction
        cam.position = newpos
        cam.focalDistance = newfocaldist
        self.call_hook("camera_changed")

timer_timeout = None
idle_active = None
delay_timeout = None
sources_lock = threading.RLock()
#first_thread_id = None

def locked(lock):
    def lock_decorator(function):
        def lock_decorated(*args, **kwargs):
            with lock:
                return function(*args, **kwargs)
        return lock_decorated
    return lock_decorator

@locked(sources_lock)
def _sensor_manager_changed_callback(arg=None):
    global timer_timeout, idle_active, delay_timeout, sources_lock #, first_thread_id
    sensormanager = coin.SoDB.getSensorManager()
    timevalue = coin.SbTime()
    if sensormanager.isTimerSensorPending(timevalue) and False:
        interval = timevalue - coin.SbTime.getTimeOfDay()
        # On a system with some load, the interval value can easily get
        # negative. For Xt, this means it will never trigger -- which
        # causes all kinds of problems. Setting it to 0 will make it
        # trigger more-or-less immediately.
        print("have timer sensor running")
        if interval.getValue() < 0.0:
            interval.setValue(0.)
        if timer_timeout is not None:
            #print("removing sensor manager")
            GLib.source_remove(timer_timeout)
        if interval.getMsecValue() < 1.0:
            #print("adding idle sensor manager")
            timer_timeout = GLib.idle_add(_sensor_manager_timeout)
        else:
            #print("adding sensor manager with", interval.getMsecValue())
            timer_timeout = GLib.timeout_add(interval.getMsecValue(), _sensor_manager_timeout)
    elif timer_timeout is not None:
        GLib.source_remove(timer_timeout)
        timer_timeout = None

    if sensormanager.isDelaySensorPending():
        if idle_active is None:
            idle_active = GLib.idle_add(_sensor_manager_idle)
        if delay_timeout is None:
            timeout = coin.SoDB.getDelaySensorTimeout().getMsecValue()
            delay_timeout = GLib.timeout_add(timeout, _sensor_manager_delay_timeout)
    else:
        if idle_active is not None:
            GLib.source_remove(idle_active)
            idle_active = None
        if delay_timeout is not None:
            GLib.source_remove(delay_timeout)
            delay_timeout = None

    #if first_thread_id is None:
    #    first_thread_id = id(threading.current_thread())
    #elif first_thread_id != id(threading.current_thread()):
    #    print("".join(traceback.format_stack()))
    return False

SensorManagerRegistered = False
def register_sensor_manager():
    global SensorManagerRegistered
    if SensorManagerRegistered:
        return
    SensorManagerRegistered = True
    sm = coin.SoDB.getSensorManager()
    sm.setChangedCallback(_sensor_manager_changed_callback, sm)
    GLib.timeout_add(10, _sensor_manager_idle2)

@locked(sources_lock)
def _sensor_manager_timeout():
    global timer_timeout
    #import pdb
    #pdb.set_trace()
    timer_timeout = None
    coin.SoDB.getSensorManager().processTimerQueue()
    _sensor_manager_changed_callback()
    return False

@locked(sources_lock)
def _sensor_manager_idle():
    global idle_active
    idle_active = None
    coin.SoDB.getSensorManager().processDelayQueue(True)
    _sensor_manager_changed_callback()
    return False

def _sensor_manager_idle2():
    sm = coin.SoDB.getSensorManager()
    sm.processImmediateQueue()
    sm.processDelayQueue(False)
    sm.processTimerQueue()
    return True

@locked(sources_lock)
def _sensor_manager_delay_timeout():
    global delay_timeout
    delay_timeout = None
    coin.SoDB.getSensorManager().processDelayQueue(False)
    _sensor_manager_changed_callback()
    return False
