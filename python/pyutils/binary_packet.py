"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt
"""

from __future__ import print_function

import sys
import re
import math
import struct
import numpy

class binary_packet(object):
    def __init__(self, format, endianess="little"):     
        if endianess == "little":
            self.endianess = "<"
        elif endianess == "big" or endianess == "network":
            self.endianess = ">"
        elif endianess == "host":
            self.endianess = "="
        else:
            raise Exception("invalid endiness: %r" % endianess)
            
        # py2.5 format_string = "".join(f for name, f in format)
        self.format = format
        self.format_string = self.generate_format(format)
        self.send = False
        
        self.error_format = (
            ('state', 'i'),
            ('counter', 'I'),
            ('error', 's', 128)
            )
        self.error_format_string = self.generate_format(self.error_format, set_attributes=False)

    def generate_format(self, format, set_attributes=True):
        format_string = [self.endianess]
        self.bitfields = []

        def finish_bitfield():
            if not self.bitfields:
                return
            bit_count = 0
            for n, bits in self.bitfields:
                bit_count += bits
            bytes = int(math.ceil(bit_count / 8.))
            format_string.append("B" * bytes)
            self.bitfields = []
        
        for d in format:
            if len(d) == 2:
                if isinstance(d[1], str):
                    finish_bitfield()
                    set_attributes and setattr(self, d[0], None)
                    format_string.append(d[1])
                else:
                    # bitfields
                    self.bitfields.append(
                        (d[0], d[1]))
            elif len(d) == 3:
                finish_bitfield()
                set_attributes and setattr(self, d[0], [None] * d[2])
                format_string.append("%d%s" % (d[2], d[1]))
        finish_bitfield()
        return "".join(format_string)
    
    def size(self):
        return struct.calcsize(self.format_string)

    def dict(self, filter=None):
        format, format_string = self.format, self.format_string
        def get_default(d, v):
            if v is None:
                if d[1] == "s":
                    v = ""
                else:
                    v = 0
            return v
        data = {}
        for d in format:
            if len(d) == 2:
                data[d[0]] = get_default(d, getattr(self, d[0]))
            elif len(d) == 3:
                if d[1] == "s":
                    data[d[0]] = get_default(d, getattr(self, d[0]))
                else:
                    data[d[0]] = [get_default(d, f) for f in getattr(self, d[0])]
        if filter is not None:
            for s in filter:
                del data[s]
        return data

    def iteritems(self):
        return iter(self.dict().items())

    def _interpret(self, data):
        pos = 0
        bit_position = 0
        self.__data = []
        for d in self.format:
            if len(d) == 2:
                if isinstance(d[1], str):
                    bit_position = 0
                    an = d[0]
                    av = data[pos]
                    pos += 1
                else:
                    an = d[0]
                    bits = d[1] # 1
                    #bit_position # 0
                    mask = 2**(bits)-1
                    bits_left = 8 - bit_position - bits

                    if bits_left < 0:
                        mask1 = 2**(bits-bits_left)-1
                        av1 = (data[pos] & mask1) << (-bits_left)
                        pos += 1
                        bit_position = 0

                        # todo: works until 16bits width, tood: loop!
                        bits = -bits_left
                        mask2 = 2**(bits)-1
                        bits_left2 = 8 - bit_position - bits
                        av2 = ((data[pos] >> bits_left2) & mask2)
                        av = av1 | av2
                        bits_left = bits_left2
                        
                    else:
                        av = (data[pos] >> bits_left) & mask

                    setattr(self, an, av)
                    #self.__data[an] = av
                    if bits_left == 0:
                        bit_position = 0
                        pos += 1
                    else:
                        bit_position += bits
                    continue
                    
            elif len(d) == 3:
                bit_position = 0
                if d[1] == "s":
                    an = d[0]
                    av = data[pos].strip("\000")
                    pos += 1
                else:
                    an = d[0]
                    av = data[pos:pos+d[2]]
                    pos += d[2]
            setattr(self, an, av)
        return self
    def unpack(self, data):
        data = struct.unpack(self.format_string, data)
        self._interpret(data)
        return self
    def unpack_from(self, buffer, offset=0):
        data = struct.unpack_from(self.format_string, buffer, offset)
        self._interpret(data)
        return self
                    
    def _pack(self):
        data = []
        if hasattr(self, "error"):
            self.error = "01234567890123456789012345678901234567890123456789012345678901234567890123456789"
            format, format_string = self.error_format, self.error_format_string
        else:
            format, format_string = self.format, self.format_string
        def get_default(d, v):
            if v is None:
                if d[1] == "s":
                    v = ""
                else:
                    v = 0
            return v
        for d in format:
            if len(d) == 2:
                data.append(get_default(d, getattr(self, d[0])))
            elif len(d) == 3:
                if d[1] == "s":
                    data.append(get_default(d, getattr(self, d[0])))
                else:
                    data_value = getattr(self, d[0])
                    if type(data_value) == numpy.ndarray:
                        data_value = data_value.transpose().flatten()
                    data.extend([get_default(d, f) for f in data_value])
        if hasattr(self, "error"):
            del self.error
        return format_string, data

    def pack(self):
        format, data = self._pack()
        try:
            return struct.pack(format, *data)                   
        except Exception:
            print("you provided data of wrong length!")
            data = []
            def get_default(d, v):
                if v is None:
                    if d[1] == "s":
                        v = ""
                    else:
                        v = 0
                return v
            for d in self.format:
                if len(d) == 2:
                    name, dtype = d
                    cnt = 1
                else:
                    name, dtype, cnt = d

                v = getattr(self, d[0])
                if v is None:
                    v = "<default value>"
                else:
                    nv = "<%s" % str(type(v))[1:-1]
                    if hasattr(v, "shape"):
                        nv += " shape %s>" % (v.shape, )
                        l = len(v.flatten())
                    elif hasattr(v, "__len__"):
                        nv += " len %d>" % len(v)
                        l = len(v)
                    else:
                        nv += "> " + repr(v)
                        l = 1
                    v = nv
                print(" member %s %d times type %r: %s" % (name, cnt, dtype, v))
                if l != cnt:
                    print(" ^-- error: you provided %d entries but %d are needed!" % (l, cnt))
            raise
    def pack_into(self, buffer, offset=0):
        format, data = self._pack()
        return struct.pack_into(format, buffer, offset, *data)
    
                    
    def __repr__(self):
        return repr(self.__dict__)

        format_string, size = self.get_format(format)


if __name__ == "__main__":
    import pprint
    p = binary_packet((
        ("testint", "i"),
        ("bit1", 1), # 1
        ("nibble1", 4), # 0
        ("nibble2", 4), # 14
        ("bit2", 1),
        ("padding", 6),
        ("padding2", 16),
        ("testint2", "i")))

    # 76543210
    # 10000111 0
    # bnnnnNNN N
    s = "\x01\x00\x00\x00\x87\xC0\xbb\xee\xff\x00\x00\x00"

    p.unpack(s)
    
    pprint.pprint(p.__dict__)
    print()
    pprint.pprint(p.dict())
    
try:
    from pyparsing import Word, Literal, Forward, Group, Optional, Or, alphas, alphanums, nums, cppStyleComment
except Exception:
    print("no pyparsing!")
type_map = {
    "unsigned char": "B",
    "short": "h",
    "signed short": "h",
    "unsigned short": "H",
    "int": "i",
    "signed int": "i",
    "unsigned int": "I",
    "unsigned": "I",
    "float": "f",
    "VEC3": "f",
    "VEC4": "f",
    "VEC6": "f",
    "char": "s"}

def cstruct_to_binary_packet(struct_data, indent=1):
    identifier = Word(alphas + "_", alphanums + "_")
    number = Word(nums + ".+-")
    semicolon = Literal(";").suppress()

    typename = Or(list(map(Literal, list(type_map.keys()))))

    array_spec = Literal("[").suppress() + Or([number, identifier]) +  Literal("]").suppress()
    
    comment = cppStyleComment
    comments = Forward()
    comments << comment + Optional(comment)

    bit_spec = Literal(":") + number
    
    declaration = (
        typename + identifier +
        Or([bit_spec, Optional(array_spec, default=None) + Optional(array_spec, default=None)]) + semicolon +
        Optional(Group(comments), default=[])
        )

    #declarations = delimitedList(declaration, delim=semicolon).ignore(cppStyleComment).ignore("\n")
    declarations = Forward()
    declarations << Group(declaration) + Optional(declarations)

    attributes = Literal("__attribute__") + Literal("((") + identifier  + Literal("))")
    struct = Literal("struct") + Group(Optional(identifier)) + Literal("{").suppress() + Group(declarations) + Literal("}").suppress() + Optional(attributes)
    typedef = Literal("typedef") + Group(struct) + identifier
    
    #expression = (Or([typedef, struct]) + semicolon).ignore(cppStyleComment)
    expression = (Or([typedef, struct]) + semicolon)
    
    try:
        data = expression.parseString(struct_data)
    except Exception:
        if type(sys.exc_info()[1]) != AttributeError:
            print("at line: %r" % sys.exc_value.line)
            print("         " + (" " * sys.exc_value.col) + "^")
        raise

    def read_struct(data, pos):
        x = data[pos]
        members = []
        max_len = 0
        strcomments = {}
        bit_specifier_hold = []
        had_bit_specifiers = False
        for i, (t, n, array, array2, comments) in enumerate(x):
            mt = type_map[t]
            # dirty hack:
            if t.startswith("VEC"):
                if array is None:
                    array = t[3:]
                elif array2 is None:
                    array2 = t[3:]
                else:
                    raise ValueError("vector type %r with more than one other dimension not supported yet!" % (
                        (t, n, array, array2, comments), ))
                if mt == "VEC6": array2 = "6"
                if mt == "VEC4": array2 = "4"
                if mt == "VEC3": array2 = "3"
            cstr = []
            for c in comments:
                if c.startswith("//"):
                    c = c[2:].strip()
                elif c.startswith("/*"):
                    c = c.strip()[2:-2].strip()
                cstr.append(c)
            if array is None:
                m = "(%r, %r)" % (n, mt)
            else:
                if array2 is None:
                    m = "(%r, %r, %s)" % (n, mt, array)
                else:
                    if array == ":":
                        # whaha: bit specifier!
                        bit_specifier_hold.append((n, mt, array2))
                        had_bit_specifiers = True
                        continue
                    else:
                        m = "(%r, %r, %s * %s)" % (n, mt, array, array2)
            if bit_specifier_hold:
                name = "_".join([bs[0] for bs in bit_specifier_hold])
                ftype = bit_specifier_hold[0][1]
                bit_length = sum([int(bs[2]) for bs in bit_specifier_hold])
                print("warning: combined bit specified members to %r" % name)
                mm = "(%r, %r)" % (name, ftype)
                bit_specifier_hold = []
                if i == len(x) - 2:
                    members.append(m)
                else:
                    members.append(mm + ",")
                
            if cstr:
                strcomments[i] = " ".join(cstr).replace("\n", "\n#")
                    
            if len(m) > max_len:
                max_len = len(m) + 2
            #if i == len(x) - 1:
            #    members.append(m)
            #else:
            members.append(m + ",")
        if not had_bit_specifiers:
            for i, c in strcomments.items():
                members[i] = "%-*.*s # %s" % (
                    max_len, max_len,
                    members[i],
                    c)
        return "\n".join(members)
    def read(data, pos):
        x = data[pos]
        if x == "struct":
            struct_name = data[pos + 1]
            struct_members = read_struct(data, pos + 2)
            output = "%s%s = pyutils.ard_packet((\n%s%s\n%s))" % (
                ("\t" * indent),
                struct_name,
                ("\t" * (indent + 1)),
                struct_members.replace("\n", "\n%s" % ("\t" * (indent + 1))),
                ("\t" * indent))
            return output
        if x == "typedef":
            typedef_name = data[pos + 2]
            output = read(data[pos + 1], 0)
            return "%s # %s" % (output, typedef_name)
        raise ValueError("unknown parse element %r" % x)
    return read(data, 0)

def extract_struct(data, name):
    # search matching typedef
    match = re.search("typedef[ \t\n]+struct[^}]*?}[ \t\r\n]*(__attribute__[^;]*?)?[ \t\r\n]*%s[ \t\r\n]*;" % name,
                      data, re.M | re.S)
    if match is None:
        # search matching struct definition
        match = re.search("struct[ \t\r\n]+%s[^}]*?}[ \t\r\n]*(__attribute__[^;]*?)?[ \t\r\n]*;" % name, data, re.M | re.S)
        if match is None:
            raise ValueError("typedef %r not found!" % (name, ))
    return match.group(0)

    
def packet_from_struct(include_file, typedef_name=None, locals=None, debug=False):
    """
    returns an binary_packet
    reading a typedef from an c-header file
    """
    if typedef_name is not None:
        # read file!
        with open(include_file, "rb") as fp:
            data = fp.read()
        sd = extract_struct(data, typedef_name)
    else:
        # got struct source as first arguments
        sd = include_file
    code = cstruct_to_binary_packet(sd)
    name_type, spec = code.split("\n", 1)
    if debug:
        print("code:")
        print(code)
    ev = "binary_packet((%s" % spec
    try:
        ret = eval(ev, globals(), locals)
    except Exception:
        print("error evaluating:\n%s\nspec:\n%s\ncode:\n%s\nstruct data:\n%s\n---" % (
            ev, spec, code, sd))
        raise

    return ret

