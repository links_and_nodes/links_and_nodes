#!/usr/bin/env python3
"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

from generate_info import parse_enum, parse_defines, to_export

e = to_export()

e.syscalls = parse_enum(
    "/opt/qnx630/target/qnx6/usr/include/sys/kercalls.h",
    strip="__KER_"
    )

e.states = parse_enum(
    "/opt/qnx630/target/qnx6/usr/include/sys/states.h",
    strip="STATE_"
    )

e.policies = {
    1: "SCHED_FIFO", 
    2: "SCHED_RR",
    3: "SCHED_OTHER",
    4: "SCHED_SPORADIC",
}

e.thread_flags = parse_defines(
    "/opt/qnx630/target/qnx6/usr/include/sys/neutrino.h",
    prefix="_NTO_TF_",
)

e.debug_flags = parse_defines(
    "/opt/qnx630/target/qnx6/usr/include/sys/debug.h",
    prefix="_DEBUG_FLAG_",
)

e.signals = dict(
    SIGSTOP=23,
    SIGCONT=25
)

e.write_file("qnx_info.py")
