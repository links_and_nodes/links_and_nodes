"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

from __future__ import print_function

import sys
import pprint
import re


c_comment = re.compile("/\*.*?\*/", re.M | re.S)
def parse_enum(fn, strip=None):
    with open(fn, "r") as fp:
        data = fp.read()
    
    data = re.sub(c_comment, "", data)
    data = re.sub("[\n\r\t ]+", " ", data)

    m = re.search("enum .*?\{(.*?)\}", data, re.M | re.S)
    if not m:
        print(data)
        raise Exception("no enum found!")
    data = list(map(str.strip, m.group(1).split(",")))
    d = {}
    i = 0
    for f in data:
        if strip is not None:
            if f.startswith(strip):
                f = f[len(strip):]
        if "=" in f:
            k, v = f.split("=", 1)
            f = k.strip()
            i = int(v)
        d[i] = f
        i += 1

    return d

def parse_defines(fn, prefix):
    with open(fn, "r") as fp:
        data = fp.read()
    data = re.sub(c_comment, "", data)
    data = re.sub("[\r\t ]+", " ", data)
    d = {}
    for m in re.finditer("^[ \t]*#define[ \t]+%s(.*?)[ \t]+(.*?)$" % prefix, data, re.M):
        k = m.group(1)
        v = m.group(2)
        if v[:1] == "(":
            continue # skip combined flags
        v = eval(v)
        d[v] = k
    return d

class to_export(object):
    def write_file(self, fn):
        with open(fn, "w") as fp:
            fp.write("""# warning: this is a auto-generated file by %s
    # do not edit!

    """ % sys.argv[0])
            for k, v in self.__dict__.items():
                fp.write("%s = {\n %s\n}\n\n" % (k, pprint.pformat(v)[1:-1]))
