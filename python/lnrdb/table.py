from __future__ import print_function

import os
import traceback
import numpy as np

from lnrdb.base_dir import BaseDir

class Table(BaseDir):
    def __init__(self, db, base, use_yaml=False):
        BaseDir.__init__(self, base)
        self.db = db
        try:
            self.meta = self._load_meta(use_yaml=use_yaml)
        except Exception:
            raise Exception("could not read meta of table at %r:\n%s" % (base, traceback.format_exc()))
        subs = self.meta.get("field-subtables")
        self.st_fields = {}
        if subs:
            self.subtables = dict(subs)
            for k, v in self.subtables.items():
                if "/" in k:
                    bn = k.rsplit("/", 1)[1]
                else:
                    bn = k
                self.st_fields[k + "_len"] = k, bn
                self.st_fields[k + "_offset"] = False

        self.dtype = self.get_numpy_dtype()
        self.n_rows = int(os.path.getsize(self.dir_entry("data")) / self.dtype.itemsize)
        self.name = self.meta["name"]
        self._mmap = None
        self._recarray = None
        self._scan_tables()        

    def mmap(self, as_recarray=True):
        if self._mmap is None:
            if self.n_rows > 0:
                self._mmap = np.memmap(self.dir_entry("data"), self.dtype, "r")
            else:
                self._mmap = np.empty((0), dtype=self.dtype)
                self._mmap.setflags(write=False)
        
        if as_recarray and self._recarray is None:
            self._recarray = self._mmap.view(np.recarray)
            
        if as_recarray:
            return self._recarray
        return self._mmap
        
    def __iter__(self):
        return iter(self.mmap())

    def get_sub(self, field, field_bn, row):
        table_name = self.subtables.get(field)
        if table_name is None:
            print(self.subtables.keys())
            raise Exception("%s has no subtable!" % field)
        table = self.tables[table_name]
        offset = getattr(row, "%s_offset" % field_bn)
        length = getattr(row, "%s_len" % field_bn)
        if length == 0:
            return table, []
        data = table.mmap()
        return table, data[offset:offset + length]

    def get_numpy_dtype(self):
        field_desc = self.meta.get("field-description")
        if field_desc is None:
            return np.dtype("u1")
        def tupelize(field_desc):
            ret = []
            for item in field_desc:
                if isinstance(item[1], list):
                    stype = tupelize(item[1])
                else:
                    stype = item[1]
                if len(item) == 2:
                    ret.append((item[0], stype))
                else:
                    ret.append((item[0], stype, item[2]))
            return ret
        t = tupelize(field_desc)
        return np.dtype(t)

    def get_row_count(self):
        return self.n_rows
    
    def show_row(self, row, ilevel=0, dtype=None, prefix=""):
        if dtype is None:
            dtype = self.dtype
        indent = "  " * (ilevel + 1)
        for k, v in zip(dtype.names, row):
            if prefix:
                pk = prefix + "/" + k
            else:
                pk = k
            st = self.st_fields.get(pk)
            if st is None:
                if isinstance(v, np.record):
                    print("%s%s:" % (indent, k))
                    self.show_row(v, ilevel=ilevel+1, dtype=v.dtype, prefix=pk)
                    continue
                print("%s%s: %r" % (indent, k, v))
                continue
            if st is False:
                continue
            stable, srows = self.get_sub(st[0], st[1], row)
            print("%s%s: %d items" % (indent, st[1], len(srows)))
            for srow in srows:
                stable.show_row(srow, ilevel=ilevel+1)

    def show_rows(self, limit=None):
        rows = self.mmap()
        print("\n%s has %d rows:" % (self.meta["name"], rows.shape[0]))
        for i, row in enumerate(rows):
            print("row %d:" % i)
            self.show_row(row)
            if limit is not None:
                limit -= 1
                if limit == 0:
                    break

