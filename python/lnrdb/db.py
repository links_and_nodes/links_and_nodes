from lnrdb.base_dir import BaseDir

all = [ "Db" ]

class Db(BaseDir):
    def __init__(self, base, use_yaml=False):
        BaseDir.__init__(self, base)
        self.meta = self._load_meta(use_yaml=use_yaml)
        self._scan_tables(use_yaml=use_yaml)
