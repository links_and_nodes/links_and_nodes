from __future__ import print_function

import sys
import pprint

import lnrdb

db = lnrdb.Db(sys.argv[1])
print("db meta:\n%s" % pprint.pformat(db.meta))
print("tables:")
for name, table in db.tables.items():
    print("  %r, %d rows. last row:" % (name, table.n_rows))
    if table.n_rows > 0:
        table.show_row(table.mmap()[-1], ilevel=2)
    else:
        print("         empty!")
