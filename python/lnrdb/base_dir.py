import os

class BaseDir(object):
    def __init__(self, base_dir):
        self.base_dir = base_dir
    
    def dir_entry(self, entry):
        return os.path.join(self.base_dir, entry)
        
    def read_all(self, entry):
        with open(self.dir_entry(entry), "rb") as fp:
            return fp.read()
    
    def _load_meta(self, use_yaml=False):
        if use_yaml:
            import yaml
            return yaml.load(self.read_all("meta"), Loader=yaml.SafeLoader)
        meta = {}
        for line in self.read_all("meta").decode("utf-8").split("\n"):
            line = line.strip()
            if not line or line[:1] == "#":
                continue
            key, value_expr = line.split(": ", 1)
            meta[key] = eval(value_expr)
        return meta
    
    def _scan_tables(self, use_yaml=False):
        import lnrdb.table
        self.tables = {}
        for e in os.listdir(self.base_dir):
            p = os.path.join(self.base_dir, e)
            if not os.path.isdir(p):
                continue
            metap = os.path.join(p, "meta")
            if not os.path.isfile(metap):
                continue
            new_table = lnrdb.table.Table(self, p, use_yaml=use_yaml)
            self.tables[new_table.name] = new_table
    
