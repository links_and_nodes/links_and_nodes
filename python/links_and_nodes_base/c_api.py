from ctypes import c_void_p, c_char_p, c_int, cdll

libc = cdll.LoadLibrary("libc.so.6")

free = libc.free
free.argtypes = [ c_void_p ]

libln = cdll.LoadLibrary("libln.so")

ln_init = libln.ln_init
ln_init.restype = c_int
ln_init.argtypes = [c_void_p, c_char_p, c_int, c_void_p]

ln_service_init = libln.ln_service_init
ln_service_init.restype = c_int
ln_service_init.argtypes = [c_void_p, c_void_p, c_char_p, c_char_p, c_char_p]

ln_get_service_signature_for_service = libln.ln_get_service_signature_for_service
ln_get_service_signature_for_service.restype = c_int
ln_get_service_signature_for_service.argtypes = [ c_void_p, c_char_p, c_void_p, c_void_p, c_void_p ]

ln_service_call = libln.ln_service_call
ln_service_call.restype = c_int
ln_service_call.argtypes = [ c_void_p, c_void_p ]

ln_get_error_message = libln.ln_get_error_message
ln_get_error_message.restype = c_char_p
ln_get_error_message.argtypes = [ c_void_p ]

ln_service_deinit = libln.ln_service_deinit
ln_service_deinit.restype = c_int
ln_service_deinit.argtypes = [ c_void_p ]

ln_deinit = libln.ln_deinit
ln_deinit.restype = c_int
ln_deinit.argtypes = [ c_void_p ]

# todo: incomplete

all = [ "libln", "libc", "free" ]
for key in list(locals().keys()):
    if key.startswith("ln_"):
        all.append(key)
