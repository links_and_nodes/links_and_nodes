# -*- mode: python, encoding: utf-8 -*-

"""
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import print_function

import os
import re
import sys
import pprint
import struct
import hashlib
import traceback

from itertools import dropwhile

import numpy

import links_and_nodes_base as ln_base

simulink_type_numbers = {
    "SS_SINGLE": 1,
    "SS_DOUBLE": 0,
    "SS_INT8": 2,
    "SS_UINT8": 3,
    "SS_INT16": 4,
    "SS_UINT16": 5,
    "SS_INT32": 6,
    "SS_UINT32": 7,
    "SS_INT64": -1,
    "SS_UINT64": -2,
}

if sys.version_info >= (3, 0):
    unicode = str

class open_text(object):
    def __init__(self, fn, mode, encoding="utf-8"):
        self.fp = open(fn, mode)
        self.encoding = encoding
    def write(self, what):
        if isinstance(what, str):
            what = what.encode(self.encoding)
        self.fp.write(what)
    def read(self, n=-1):
        data = self.fp.read(n)
        return data.decode(self.encoding)
    def close(self):
        self.fp.close()
    def __enter__(self):
        return self
    def __exit__(self, type, value, traceback):
        return self.fp.__exit__(type, value, traceback)

def search_message_definition(name, allow_rescan=True, with_abs_name=False):
    for d in ln_base.message_definition_dirs:
        tfn = os.path.join(d, name)
        if os.path.isfile(tfn):
            if not with_abs_name:
                return tfn
            else:
                return tfn, name
    if os.path.isfile(name):
        if not with_abs_name:
            return os.path.realpath(name)
        else:
            return os.path.realpath(name), os.path.realpath(name)
            
    if allow_rescan:
        ln_base.rescan_message_definition_dirs()
        return search_message_definition(name, allow_rescan=False, with_abs_name=with_abs_name)
    pathes = "\n".join(ln_base.message_definition_dirs)
    raise Exception("message definition of name %r not found in these dirs:\n%s" % (name, pathes))


def get_message_definition_from_filename(fn):
    best = None
    for d in ln_base.message_definition_dirs:
        if fn.startswith(d) and (best is None or len(d) > len(best)):
            best = d
    if best is None:
        raise Exception("can not get message_definition name for path %r" % fn)
    if best[-1] != "/":
        best += "/"
    return fn[len(best):]


class data_type(object):
    def __init__(self, name, pyformat, simulink_name, java_name, aliases=None):
        if aliases is None:
            aliases = []
        self.name = name
        self.pyformat = pyformat
        self.size = struct.calcsize(self.pyformat)
        self.simulink_name = simulink_name
        self.java_name = java_name
        self.aliases = aliases
        if name.endswith("_t"):
            nptype_name = name[:-2]
        elif self.aliases:
            nptype_name = self.aliases[0][:-2]
        else:
            nptype_name = name
        if nptype_name == "char":
            self.npformat = numpy.char.chararray
        else:
            self.npformat = getattr(numpy, nptype_name)

    def get_c_name(self):
        return self.name

    def get_java_name(self):
        return self.java_name

    def get_simulink_id(self):
        if not self.simulink_name:
            raise Exception("datatype '%s' is not supported by simulink!" % (self.name))
        return simulink_type_numbers[self.simulink_name]

    def get_sizeof(self):
        return self.size


# keep in sync with ln_runtime/lnrecorder/lnrdb/src/field_description.cpp lntype_map
data_type_list = [
    data_type("float", "f", "SS_SINGLE", "float", aliases=["float32_t"]),
    data_type("double", "d", "SS_DOUBLE", "double", aliases=["float64_t"]),
    data_type("char", "b", "SS_INT8", "byte", aliases=[]),
    data_type("int8_t", "b", "SS_INT8", "byte", aliases=[]),
    data_type("uint8_t", "B", "SS_UINT8", "byte", aliases=[]),  # warning: java has no unsigned byte!
    data_type("int16_t", "h", "SS_INT16", "short", aliases=["short"]),
    data_type("uint16_t", "H", "SS_UINT16", "short", aliases=[]),  # warning: java has no unsigned short!
    data_type("int32_t", "i", "SS_INT32", "int", aliases=["int"]),
    data_type("uint32_t", "I", "SS_UINT32", "int", aliases=[]),  # warning: java has no unsigned int!
    # those are available in simulink starting from 2017a with special treatment:
    data_type("int64_t", "q", "SS_INT64", "long", aliases=[]),
    data_type("uint64_t", "Q", "SS_UINT64", "long", aliases=[]),  # warning: java has no unsigned int64 and no bigger type!
]
data_type_map = {}
for t in data_type_list:
    data_type_map[t.name] = t
    for ta in t.aliases:
        data_type_map[ta] = t


class message_definition(object):
    """
    this class represents a single message definition

    
    mdf = message_definition_file("some_file")
    will load the message definition from a file
    """

    def _reset(self):
        self.fields = []
        self.resp_fields = []  # only used if self.is_service == True

        self.defines = []
        self.define_abs_names = {}

        self.is_service = False
        self.is_event = False
        self.is_dynamic = False

    def __init__(self, name, fn=None, text=None, encode="utf-8"):
        self.name = name
        self.fn = fn
        self.encode = encode # do first encode text if it is a unicode!

        if fn and fn.startswith("from_dict:"):
            d = eval(fn.split(":", 1)[1])
            self.load_from_dict(d)
        elif fn:
            self.load_from_file(fn)
        elif text is not None:
            self.load(text)
        else:
            self._reset()

    def dict(self):
        return dict(fields=self.fields, resp_fields=self.resp_fields, defines=self.get_user_defines())

    def load_from_dict(self, d):
        self._reset()
        self.filename = "<from_dict>"
        self.fields = d["fields"]
        self.resp_fields = d.get("resp_fields", {})
        for defname, fields in d.get("defines", {}).items():
            self.defines.append((defname, "from_dict:%s" % dict(fields=fields)))
            self.define_abs_names[defname] = defname

    def load_from_file(self, fn):
        with open_text(fn, "rb") as fp:
            text = fp.read()
        self.load(text, fn)

    def load(self, text, fn="<text>"):
        self._reset()
        if isinstance(text, unicode) and unicode is not str and self.encode:
            text = text.encode(self.encode) # basically only needed for on _ln.so extensions that can not handle unicode objects
        self.filename = fn
        defines_set = set()
        service_mode = "req"
        line_no = 0
        first_non_empty = True
        pointers = dict(req=[], resp=[])
        for line in text.split("\n"):
            line_no += 1
            line = line.strip()
            if not line or line.startswith("#"):
                continue
            if "#" in line:
                line = line.split("#", 1)[0].strip()
            if first_non_empty:
                first_non_empty = False
                if line == "service":
                    self.is_service = True
                    continue
                if line == "event":
                    self.is_event = True
                    service_mode = "connect"
                    continue
            if self.is_service:
                if line == "request":
                    service_mode = "req"
                    continue
                if line == "response":
                    service_mode = "resp"
                    continue
            if self.is_event:
                if line == "connect":
                    # service_mode = "connect"
                    service_mode = "req"
                    continue
                if line == "call":
                    # service_mode = "call"
                    service_mode = "resp"
                    continue
            try:
                first_word, rest = line.split(" ", 1)
            except Exception:
                raise Exception("syntax error in message definition file\n%s:%d: unknown line syntax %r" % (
                    fn, line_no, line))

            if first_word == "define":
                new_define, from_file = rest.strip().split(" as ", 1)
                new_define = new_define.strip()
                if '"' in new_define:
                    raise Exception(
                            "syntax error in message definition file\n%s:%d: invalid define/type-name %r in line\n%s" % (
                                fn, line_no, new_define, line))
                from_file = from_file.strip(" \r\t\n")[1:-1]
                abs_name = os.path.join(os.path.dirname(self.name), from_file)
                tfn = os.path.join(os.path.dirname(fn), from_file)
                if not os.path.isfile(tfn):
                    try:
                        tfn, abs_name = search_message_definition(from_file, with_abs_name=True)
                    except Exception:
                        raise Exception("%s:%d: error: there is a type-define %r to a missing file %r!\n%s" % (
                            fn, line_no, new_define, from_file, traceback.format_exc()))
                defines_set.add(new_define)
                self.defines.append(
                        [new_define, tfn])
                self.define_abs_names[new_define] = abs_name
                continue
            if first_word.endswith("*"):
                self.is_dynamic = True
                is_pointer = True
                first_word = first_word[:-1]
            else:
                is_pointer = False
            # assume first_word to be data type name
            if first_word in data_type_map or first_word in defines_set:
                field = rest.strip()
                if "[" in field:
                    if is_pointer:
                        raise Exception(
                                "syntax error in message definition file\n%s:%d: currently array of pointer-types like %s* are not implemented!" % (
                                    fn, line_no, first_word))
                    field, count = field.split("[", 1)
                    count = int(eval(count.strip(" []")))
                else:
                    count = 1
                for invalid in u";.,+-*/{}()#$äöü?'`\"\\":
                    if invalid in field:
                        raise Exception(
                                "syntax error in message definition file\n%s:%d: char %r is not allowed in a field name: %r!" % (
                                    fn, line_no, invalid, field))
                # if field == "_timestamp":
                #    raise Exception("syntax error in message definition file\n%s:%d: special field-name _timestamp not allowed!" % (
                #            fn, line_no))
                if is_pointer:
                    first_word += "*"
                    pointers[service_mode].append(field)
                field = [first_word, field, count]
                if (self.is_service or self.is_event) and service_mode == "resp":
                    self.resp_fields.append(field)
                else:
                    self.fields.append(field)
                continue
            raise Exception("syntax error in message definition file\n%s:%d: unknown type %r" % (
                fn, line_no, first_word))
        if self.is_service or self.is_event or self.is_dynamic:
            # fill missing _len members for pointer types!
            # for mode, fields in ("req", self.fields), ("resp", self.resp_fields):
            #    for ptr in pointers[mode]:
            #        ptr_len = "%s_len" % ptr
            #        # does this field exist?
            #        for ft, fn, fc in fields:
            #            if fn == ptr_len:
            #                break # yes!
            #        else:
            #            # no!
            #            fields.append(("uint32_t", ptr_len, 1))
            #        # ok!
            # sort *_len fields exactly (wrong:__after__) BEFORE their pointers!
            if self.is_service or self.is_event:
                todo = ("req", self.fields), ("resp", self.resp_fields)
            else:  # dynamic packet only has req
                todo = ("req", self.fields),
            for mode, fields in todo:
                for ptr in pointers[mode]:
                    for i, (ft, fn, fc) in enumerate(fields):
                        if fn == ptr:
                            break
                    else:
                        raise Exception("alg error")
                    # ptr is at i
                    # does this field exist?
                    ptr_len = "%s_len" % ptr
                    for k, (ft, fn, fc) in enumerate(fields):
                        if fn == ptr_len:
                            break  # yes!
                    else:
                        # no! -> insert at i
                        fields.insert(i, ["uint32_t", ptr_len, 1])
                        continue
                    # field exists at k
                    if k == i - 1:
                        # correct
                        continue
                    # move field from k to i
                    field = fields[k]
                    del fields[k]
                    fields.insert(i, field)

    def calculate_size(self):
        # dependencies
        define_sizes = {}
        for defname, deffile in self.defines:
            defmd = message_definition(defname, deffile)
            define_sizes[defname] = defmd.calculate_size()
        fields = []
        size = 0
        for field_type, field_name, field_count in self.fields:
            if field_type.endswith("*"):
                continue
            if field_type in define_sizes:
                size += define_sizes[field_type] * field_count
            else:
                size += data_type_map[field_type].get_sizeof() * field_count
        return size

    def get_user_defines(self, update_defines=None):
        if update_defines is not None:
            defines = update_defines
        else:
            defines = {}
        for defname, deffile in self.defines:
            if deffile == self.filename:
                # self recursion
                defmd = self
                defines[defname] = defmd.fields
            else:
                defmd = message_definition(defname, deffile)
                defines[defname] = defmd.fields
                defmd.get_user_defines(defines)
        return defines

    def get_hash(self):
        defines = self.get_user_defines()
        dkeys = list(defines.keys())
        dkeys.sort()
        sorted_defines = [(key, defines[key]) for key in dkeys]
        data = ( self.fields, self.resp_fields, sorted_defines )
        hash_text = repr(data).encode("utf-8")
        return hashlib.sha256(hash_text).hexdigest()
    
    def __str__(self):
        return "fields:\n%s\ndefines:\n%s" % (
            pprint.pformat(self.fields),
            pprint.pformat(self.defines))

    def __eq__(self, other):
        """
        this will test whether two md's are equivalent for LN's IPC
        """
        return self.dict() == other.dict()

    def __ne__(self, other):
        return not self.__eq__(other)

    def locate_member(self, member):
        mp = member.split(".")

        def _locate_member(mp, md, offset=0):
            defines = {}
            define_sizes = {}
            for defname, deffile in md.defines:
                defmd = message_definition(defname, deffile)
                defines[defname] = defmd
                define_sizes[defname] = defmd.calculate_size()
            m = mp[0].strip()
            if m.endswith("]"):
                m, item = m.rsplit("[", 1)
                item = int(item[:-1])
            else:
                item = 0
            for field_type, field_name, field_count in md.fields:
                if field_type in data_type_map:
                    item_size = data_type_map[field_type].get_sizeof()
                else:
                    item_size = define_sizes[field_type]
                if field_name == m:
                    break
                offset += item_size * field_count
            else:
                raise Exception("md %s has no field %r from %r!" % (md, m, member))
            if item < 0:
                item = field_count + item
            if item < 0:
                raise Exception("from %r member %r item %d is invalid!" % (member, m, item))

            if item >= field_count:
                raise Exception("md %s field %r has only %d items! from %r!" % (md, m, field_count, member))
            offset += item * item_size
            if len(mp) == 1:
                return offset, item_size
            elif field_type in data_type_map:
                raise Exception(
                        "md %s field %r is integral type - it has no further members (for %r)!" % (md, m, member))
            return _locate_member(mp[1:], defines[field_type], offset)

        return _locate_member(mp, self)


class message_definition_header(object):
    """
    this class represents a to-be-generated C/C++ header file.
    it can read in message definition files and generate C-source code
    """

    def __init__(self, service_base_map=None):
        if service_base_map is None:
            service_base_map = {}
        self.message_definitions = []
        self.clnt = None
        self.defined_signature_names = set()
        self.service_base_map = service_base_map
        self.wrapping_namespaces = []
        self.md_prefix_ns_define = "LN_MD_PREFIX_NS"

    def add_message_definition(self, message_definition_name):
        if isinstance(message_definition_name, message_definition):
            md = message_definition_name
        else:
            fn = search_message_definition(message_definition_name)
            md = message_definition(message_definition_name, fn)
        md.map_base_to = self.service_base_map.get(md.name)
        self.message_definitions.append(md)

    def generate_sfunction_parameter(self, with_hash=False):
        md = self.message_definitions[0]
        flat = self.generate_flat_md(md, True).replace("\n", "|") + "|"
        if with_hash:
            return "%s|%s#%s" % (md.filename, flat, md.get_hash())
        return "%s|%s" % (md.filename, flat)

    def generate_sfunction_lnrk_parameter(self):
        output = []
        for md in self.message_definitions:
            flat = self.generate_flat_lnrk_md(md, True).replace("\n", "|")
            output.append("%s: %s" % (md.name, flat))
        print("\n".join(output))

    def _generate_idf(self, md, member_name, depth=0):
        fields = []
        # pprint.pprint(md.defines)
        defines = dict(md.defines)
        for field_type, field_name, field_count in md.fields:
            if field_type in defines:
                defname = field_type
                deffile = defines[field_type]
                defmd = message_definition(defname, deffile)
                if field_count > 1:
                    for i in range(field_count):
                        idf = self._generate_idf(defmd, "%s[%d]" % (field_name, i + 1), depth + 1)
                        fields.append(idf)
                else:
                    idf = self._generate_idf(defmd, field_name, depth + 1)
                    fields.append(idf)

                continue
            type_id = data_type_map[field_type].get_c_name()
            fields.append("  %s%s %s %d" % (("  " * depth), field_name, type_id, field_count))
        return "%sbus %s {\n%s\n%s}" % (("  " * depth), member_name, "\n".join(fields), ("  " * depth))

    def generate_idf(self):
        md = self.message_definitions[0]
        return self._generate_idf(md, "SIG")

    def _generate_struct_name(self, n):
        return n.replace(" ", "_").replace("/", "_") + "_t"
    def _generate_struct_name_for(self, md):
        return self._generate_struct_name(md.name)

    def generate(self, fp=None, output_fn=None, comment=""):
        """
        py3: fp should be io.StringIO-like, e.g. open(file, "w", encoding="utf-8")
        py2: fp should be cStringIO.StringIO-like
        in py2 and py3 this returns a str!
        """
        if fp is None:
            fp = sys.stdout
        if output_fn is None:
            output_fn = "ln_messages.h"
        define_name = os.path.basename(output_fn).upper().replace(".", "_")
        fp.write("%s#ifndef %s\n#define %s\n\n#include <stdint.h>\n#include <ln/ln.h>\n\n" % (comment, define_name, define_name))
        self.defined_signature_names = set()
        for md in self.message_definitions:
            sn = self._generate_struct_name_for(md)
            self.generate_md(fp, sn, md, True)
        fp.write("#endif // %s\n" % define_name)

    def generate_flat_md(self, md, top_level=False):
        # dependencies
        defines = {}
        for defname, deffile in md.defines:
            defmd = message_definition(defname, deffile)
            defsn = self._generate_struct_name("_%s_%s" % (md.name, defname))
            defines[defname] = self.generate_flat_md(defmd)
        fields = []
        for field_type, field_name, field_count in md.fields:
            if field_type in defines:
                if not defines[field_type]:
                    continue # empty define!
                for count in range(field_count):
                    for field in defines[field_type].split("\n"):
                        type_spec, inner_field_name = field.rsplit(" ", 1)
                        if field_count > 1:
                            inner_field_name += "[%s]" % count
                        fields.append("%s %s_%s" % (type_spec, field_name, inner_field_name))
                continue
            if field_type[-1] == "*":
                raise Exception("dynamic/pointer types not allowed in topics: %r" % field_type)
            type_id = data_type_map[field_type].get_simulink_id()
            field_size = data_type_map[field_type].get_sizeof()
            fields.append("%d %d %d %s" % (type_id, field_size, field_count, field_name))
        return "\n".join(fields)

    def generate_flat_lnrk_md(self, md, top_level=False):
        # dependencies
        defines = {}
        for defname, deffile in md.defines:
            defmd = message_definition(defname, deffile)
            defmd.size = defmd.calculate_size()
            defines[defname] = defmd
        # collect subtypes and their occurences
        subtypes = []
        offset = 0
        for field_type, field_name, field_count in md.fields:
            if field_type in defines:
                for i, st in enumerate(subtypes):
                    if st[0] == field_type:
                        st[1].append((field_name, offset, field_count))
                        break
                else:
                    subtypes.append((field_type, [(field_name, offset, field_count)]))
                size = defines[field_type].size
            else:
                subtypes.append((field_type, (field_name, offset, field_count)))
                size = data_type_map[field_type].get_sizeof()
            offset += size * field_count
        ports = []
        n_subtypes = 0
        for src_type, field in subtypes:
            if src_type in defines:
                n_subtypes += 1
                last_subtype = src_type
                defmd = defines[src_type]
                offset = 0
                for field_type, field_name, field_count in defmd.fields:
                    element_size = data_type_map[field_type].get_sizeof()
                    port_elements = []
                    port = ["%s_%s" % (src_type, field_name), field_type, 0, element_size, port_elements]
                    for src_name, src_offset, src_count in field:
                        for c in range(src_count):
                            port_elements.append((src_offset + offset + c * defmd.size, field_count))
                            port[2] += field_count
                    offset += element_size * field_count
                    ports.append(port)
            else:
                field_name, offset, field_count = field
                element_size = data_type_map[src_type].get_sizeof()
                port_elements = [(offset, field_count)]
                port = [field_name, src_type, field_count, element_size, port_elements]
                ports.append(port)
        if n_subtypes == 1:
            # strip src_type name
            for port in ports:
                if port[0].startswith(last_subtype):
                    port[0] = port[0][len(last_subtype) + 1:]
        return "".join(
                ["%s %s %s %s %s\n" % (p[0], data_type_map[p[1]].get_simulink_id(), p[2], p[3], " ".join(
                        ["%d %d" % (offset, count)
                         for offset, count in p[4]]))
                 for p in ports])
        
    def get_namespace(self, md_name):
        namespace = list(self.wrapping_namespaces)
        namespace.extend(md_name.split("/")[:-1])
        ons = "\n".join(["namespace %s {" % n for n in namespace])
        cns = "\n".join(["} // namespace %s" % n for n in namespace])
        return ons, cns
    
    def generate_md(self, fp, sn, md, top_level=False, return_signature=False):
        # print("generate_md sn %r md.name %r, is_service %r" % (sn, md.name, md.is_service))
        # dependencies
        define_map = {}
        signature_map = {}
        define_sizes = {}
        for defname, deffile in md.defines:
            abs_name = md.define_abs_names[defname]
            if deffile == md.filename:
                # self recursion!
                defmd = md
                defsn = sn
                define_map[defname] = defsn
            else:
                defmd = message_definition(abs_name, deffile)
                defmd.map_base_to = self.service_base_map.get(defmd.name)
                defsn = self._generate_struct_name(defmd.name)
                define_map[defname] = defsn
                define_sizes[defname], signature_map[defname] = self.generate_md(fp, defsn, defmd, return_signature=True)

        self.service_header_template = "service_header.cpp"
        self.event_header_template = "event_header.cpp"
        self.packet_header_template = "packet_header.cpp"
        sn_no_t = sn[:-2]
                
        def append_field_definitions(field_defs, add_indent=""):
            """
            generates C code for each field from field_defs and appends this code to fields list.
            it also generates a signature for those field_defs into signature list

            no side effects! (aside from define_map, signature_map and define_sizes read accesses)
            
            input arguments: 
            - field_defs
            will return:
            - fields
            - signature
            - fields
            - signature
            - size of host-side structure in bytes
            - need_self_forward_declaration
            """
            fields = []
            signature = []
            size = 0
            need_self_forward_declaration = False
            for field_type, field_name, field_count in field_defs:
                if field_type.endswith("*"):
                    ptr = "*"
                    field_type = field_type[:-1]
                else:
                    ptr = ""
                if field_type in define_map:
                    c_type_name = define_map[field_type]
                    if c_type_name == sn:
                        if not ptr:
                            raise Exception(
                                    "self-recursion detected in md file\n%s\nfor field %s which is not a pointer!" % (
                                        md.filename, field_name))
                        sig = "[]"  # not yet known!
                        s = 0  # not really needed
                        need_self_forward_declaration = True
                    else:
                        sig = "[%s]" % (signature_map[field_type])
                        s = define_sizes[field_type]
                else:
                    c_type_name = data_type_map[field_type].get_c_name()
                    sig = c_type_name
                    s = data_type_map[field_type].get_sizeof()
                if not ptr:
                    size += s * field_count

                sig = "%s%s %s %s" % (sig, ptr, s, field_count)
                if field_count > 1:
                    fields.append(add_indent + "\t%s %s[%d];" % (c_type_name, field_name, field_count))
                else:
                    fields.append(add_indent + "\t%s%s %s;" % (c_type_name, ptr, field_name))
                signature.append(sig)
            return fields, signature, size, need_self_forward_declaration
        
        def generate_c_header_packet(md):
            fields, signature, size, need_self_forward_declaration = append_field_definitions(md.fields)
            if fp and sn not in self.defined_signature_names:
                # message defined in output file
                self.defined_signature_names.add(sn)
                bn = os.path.basename(md.name)
                bname = md.map_base_to or bn
                ns_opening, ns_closing = self.get_namespace(md.name)
                template_data = dict(
                    mdname=md.name,
                    sn=sn,
                    sn_no_t=sn_no_t,
                    bname=bname,
                    md_prefix_ns_define=self.md_prefix_ns_define,
                    ns_opening=ns_opening,
                    ns_closing=ns_closing,
                    fields="\n".join(fields)
                )
                fp.write(fill_template(self.packet_header_template, template_data))
                    
            return size, ",".join(signature)
        def generate_c_header_event(md):
            # call
            call_fields, signature, size, nsf_decl = append_field_definitions(md.resp_fields)
            call_signature = ",".join(signature)
            fields, signature, size, nsf_decl = append_field_definitions(md.fields)
            connect_signature = ",".join(signature)
            if fp and sn not in self.defined_signature_names:
                # connect
                bn = os.path.basename(md.name)
                ns_opening, ns_closing = self.get_namespace(md.name)
                bn = os.path.basename(md.name)
                bname = md.map_base_to or bn
                template_data = dict(
                    name=sn,
                    bname=bname,
                    sn=sn,
                    sn_no_t=sn_no_t,
                    md_prefix_ns_define=self.md_prefix_ns_define,
                    ns_opening=ns_opening,
                    ns_closing=ns_closing,
                    fields="\n".join(fields),
                    call_fields="\n".join(call_fields),
                    event_connect_type="ln_event_connect_" + sn,
                    cpp_event_connect_type="ln_event_connect_" + sn,
                    event_call_type="ln_event_" + sn,
                    mdname=md.name,                    
                    connect_signature=connect_signature,
                    call_signature=call_signature
                )
                fp.write(fill_template(self.event_header_template, template_data))
                self.defined_signature_names.add(sn)
            # size is also meaningless for service
            return 0, "%s|%s" % (connect_signature, call_signature)
        def generate_c_header_service(md):
            fields = []
            typedefs = []
            if md.fields:
                add_fields, signature, s, nsf_decl = append_field_definitions(md.fields, add_indent="\t")
                fields.append("\tstruct LN_PACKED %s_request_t {" % sn_no_t)
                fields.extend(add_fields)
                fields.append("\t} req;")
                typedefs.append((sn_no_t + "_request_t", "request_t"))
                
                req_signature = ",".join(signature)
                service_signature = "%s|" % (req_signature)
            else:
                service_signature = "|"
            if md.resp_fields:
                add_fields, signature, s, nsf_decl = append_field_definitions(md.resp_fields, add_indent="\t")

                fields.append("\tstruct LN_PACKED %s_response_t {" % sn_no_t)
                fields.extend(add_fields)
                fields.append("\t} resp;")
                typedefs.append((sn_no_t + "_response_t", "response_t"))
                
                resp_signature = ",".join(signature)
                service_signature += resp_signature

            if fp and sn not in self.defined_signature_names:
                self.defined_signature_names.add(sn)
                bn = os.path.basename(md.name)
                name = md.map_base_to or bn
                svc_data_name = "data"
                class_name = name + "_base"
                if md.resp_fields:
                    clear_resp = "\n\t\tmemset(&" + svc_data_name + ".resp, 0, sizeof(" + svc_data_name + ".resp));"
                else:
                    clear_resp = ""
                ns_opening, ns_closing = self.get_namespace(md.name)
                if typedefs:
                    typedefs = (
                        "\n" +
                        "#ifdef __cplusplus\n"+
                        "\n".join(["\ttypedef %s %s;" % (s, td) for s, td in typedefs]) +
                        "\n#endif\n")
                else:
                    typedefs = ""
                template_data = dict(
                    md_prefix_ns_define=self.md_prefix_ns_define,
                    ns_opening=ns_opening,
                    ns_closing=ns_closing,
                    class_name=class_name,
                    full_name=md.name.replace("/", "_"),
                    name=name,
                    sn=sn,
                    sn_no_t=sn_no_t,
                    mdname=md.name,
                    clear_resp=clear_resp,
                    fields="\n".join(fields),
                    service_signature=service_signature,
                    typedefs=typedefs
                )
                fp.write(fill_template(self.service_header_template, template_data))
            # return 0 here, size has no real meaning for a service
            return 0, service_signature

        if md.is_event:
            size, signature = generate_c_header_event(md)
        elif md.is_service:
            size, signature = generate_c_header_service(md)
        else:
            size, signature = generate_c_header_packet(md)
        
        if return_signature:
            return size, signature
        return size

    def generate_java_classfile(self, folder, pkg, classname):
        filename = os.path.join(folder, classname + ".java")
        fp = open_text(filename, "wb")
        fp.write("package %s;\n" % pkg)
        return fp

    def find_java_type(self, pkg, field_type, md):
        if field_type in list(md.jaliases.keys()):
            field_type = md.jaliases[field_type].name
        if field_type in self.jtypes:
            return self.jtypes[field_type]
        elif pkg + "/" + field_type in self.jtypes:
            return self.jtypes[pkg + "/" + field_type]
        else:
            raise Exception("Unknown field type %s at package %s" % (field_type, pkg))

    def convert_to_java_fields(self, md, mdfields):
        java_fields = []
        imports = set()
        pkg = "/".join(md.name.split("/")[:-1])
        for ftype, fname, fsize in mdfields:
            if '*' == ftype[-1]:
                java_fields = java_fields[:-1]
                ftype = ftype[:-1]
                jtype = self.find_java_type(pkg, ftype, md)
                ctor_params = jtype['array_ctor_params'] if 'array_ctor_params' in jtype else ''
                java_fields.append({'type': jtype['pointer'], 'name': fname, 'ctor_params': ctor_params,
                                    'size': 'ListField.SIZE'})
            else:
                jtype = self.find_java_type(pkg, ftype, md)
                if fsize > 1:
                    ctor_params = jtype['array_ctor_params'] + ', ' if 'array_ctor_params' in jtype else ''
                    ctor_params += str(fsize)
                    java_fields.append({'type': jtype['array'], 'name': fname, 'ctor_params': ctor_params,
                                        'size': ('%s.SIZE*%i' % (jtype['field'], fsize))})
                else:
                    java_fields.append({'type': jtype['field'], 'name': fname, 'ctor_params': '',
                                        'size': ('%s.SIZE' % jtype['field'])})
            if "import" in jtype:
                imports.add(jtype["import"])
        return java_fields, imports

    def generate_java_topic(self, folder, pkg, classname, md):
        PACKET_TEMPLATE = """
import de.dlr.rm.ln.msgdef.*;
import de.dlr.rm.ln.msgdef.array.*;
import de.dlr.rm.ln.msgdef.list.*;
import de.dlr.rm.ln.msgdef.field.*;
%(imports)s

public class %(classname)s extends Packet{
    public final static java.lang.String MSG_DEF = "%(msgdef)s";
    public final static int SIZE = %(size)s;

    %(fields)s

    public %(classname)s(){
        super(new Field[]{%(field_ctors)s},
              new java.lang.String[]{%(field_names)s});
    }

    public final static PacketFactory<%(classname)s> CREATOR = new PacketFactory<%(classname)s>() {
        public %(classname)s create() { return new %(classname)s(); }
        public %(classname)s[] array(int size) { return new %(classname)s[size]; }
        public int sizeOfStruct() { return SIZE; }
    };
}"""
        fp = self.generate_java_classfile(folder, pkg, classname)
        (fields, field_ctors, field_names, sizes, imports) = self.create_java_inserts(md, md.fields)
        allsizes = " + ".join(sizes)
        if not len(allsizes):
            allsizes = "0"
        fp.write(PACKET_TEMPLATE % {'classname': classname, 'imports': '\n'.join(imports), 'size': allsizes,
                                    'fields': "\n\t".join(fields), 'field_ctors': ", ".join(field_ctors),
                                    'field_names': ", ".join('"{0}"'.format(name) for name in field_names),
                                    'msgdef': md.name,
                                    })
        fp.close()
        return 0

    def create_java_inserts(self, md, mdfields):
        java_fields, imports = self.convert_to_java_fields(md, mdfields)
        fields = []
        field_ctors = []
        field_names = []
        sizes = []
        for i, jfield in enumerate(java_fields):
            fields.append("public final %s %s = getField(%i);" % (jfield['type'], jfield['name'], i))
            field_ctors.append("new %(type)s(%(ctor_params)s)" % jfield)
            field_names.append("%(name)s" % jfield)
            sizes.append(jfield['size'])
        return fields, field_ctors, field_names,  sizes, imports

    def generate_java_service(self, folder, pkg, classname, md):
        SERVICE_TEMPLATE = """
import de.dlr.rm.ln.*;
import de.dlr.rm.ln.msgdef.*;
import de.dlr.rm.ln.msgdef.array.*;
import de.dlr.rm.ln.msgdef.list.*;
import de.dlr.rm.ln.msgdef.field.*;
import de.dlr.rm.ln.service.*;
%(imports)s

public class %(classname)s {
    public final static java.lang.String SIGNATURE = "%(signature)s";
    public final static java.lang.String INTERFACE = "%(interface)s";

    public static class Request extends Packet{
        public final static int SIZE = %(req_size)s;

        %(req_fields)s

        public Request() {
            super(new Field[]{%(req_field_ctors)s},
                  new java.lang.String[]{%(req_field_names)s});
        }

        public final static PacketFactory<Request> CREATOR = new PacketFactory<Request>() {
            public Request create() { return new Request(); }
            public Request[] array(int size) { return new Request[size]; }
            public int sizeOfStruct() { return SIZE; }
        };

    }

    public static class Response extends Packet{
        public final static int SIZE = %(resp_size)s;

        %(resp_fields)s
        public Response() {
            super(new Field[]{%(resp_field_ctors)s},
                  new java.lang.String[]{%(resp_field_names)s});
        }

        public final static PacketFactory<Response> CREATOR = new PacketFactory<Response>() {
            public Response create() { return new Response(); }
            public Response[] array(int size) { return new Response[size]; }
            public int sizeOfStruct() { return SIZE; }
        };

    }

    public static class Service extends ServiceProvider<Request, Response, Service> {
        public Service(LnClient client, java.lang.String svcName) throws LnError {
            super(client, svcName, INTERFACE, SIGNATURE, Request.CREATOR, Response.CREATOR);
        }
    }

    public static ServiceConnection<Request, Response> getServiceConnection(LnClient client, java.lang.String svcName) throws LnError{
        return client.getServiceConnection(svcName, INTERFACE, SIGNATURE, Request.CREATOR, Response.CREATOR);
    }
}"""

        sn = self._generate_struct_name_for(md)
        ret, signature = self.generate_md(None, sn, md, True, True)

        fp = self.generate_java_classfile(folder, pkg, classname)

        (req_fields, req_field_ctors, req_field_names, req_sizes, req_imports) = self.create_java_inserts(md, md.fields)
        (resp_fields, resp_field_ctors, resp_field_names, resp_sizes, resp_imports) = self.create_java_inserts(md, md.resp_fields)
        req_imports = req_imports.union(resp_imports)
        
        all_req_sizes = " + ".join(req_sizes)
        if not len(all_req_sizes):
            all_req_sizes = "0"
        all_resp_sizes = " + ".join(resp_sizes)
        if not len(all_resp_sizes):
            all_resp_sizes = "0"

        fp.write(SERVICE_TEMPLATE % {'classname': classname, 'signature': signature, 'interface': md.name,
                                     'imports': '\n'.join(req_imports),
                                     'req_size': all_req_sizes, 'resp_size': all_resp_sizes,
                                     'req_fields': "\n\t\t".join(req_fields),
                                     'req_field_ctors': ", ".join(req_field_ctors),
                                     'req_field_names': ", ".join('"{0}"'.format(name) for name in req_field_names),
                                     'resp_fields': "\n\t\t".join(resp_fields),
                                     'resp_field_ctors': ", ".join(resp_field_ctors),
                                     'resp_field_names': ", ".join('"{0}"'.format(name) for name in resp_field_names),
                                     })
        fp.close()
        return 0

    def jdefine(self, basepkg, md):
        package = []
        if basepkg and len(basepkg) > 0:
            package = basepkg.split(".")

        package_tmp = package[:]
        package.extend(dropwhile(
            lambda it: len(package_tmp) and it == package_tmp.pop(0),
            get_message_definition_from_filename(md.filename).split("/")))
        if basepkg != "de.dlr.rm.ln.builtin":
            pkgname = ".".join(package[:-1])
        else:
            pkgname = basepkg
        classname = "".join(str.capitalize(x) if x else '_' for x in package[-1].split("_"))
        self.jtypes[md.name] = {'field':classname, 'array':'PacketArray<%s>' % classname, 
            'pointer':'PacketList<%s>' % classname, 
            'array_ctor_params':'%s.CREATOR' % classname, 
            'import':'import %s.%s;' % (pkgname, classname)}
        md.jpkg = pkgname
        md.jclassname = classname
        return 0
       
    def find_md_for_filename(self, filename):
        for md in self.message_definitions:
            if md.filename == filename:
                return md
        return None

    def prepare_java_classes(self, basepkg):
        
        for md in self.message_definitions:
            self.jdefine(basepkg, md)
        for md in self.message_definitions:
            md.jaliases = {}
            for defname, deffile in md.defines:
                defmd = self.find_md_for_filename(deffile)
                if not defmd:
                    defmd = message_definition(get_message_definition_from_filename(deffile), deffile)
                    self.message_definitions.append(defmd)
                    isBuiltin = os.path.abspath(deffile).startswith(os.path.abspath(ln_base.builtin_msg_defs))
                    self.jdefine("de.dlr.rm.ln.builtin" if isBuiltin else basepkg, defmd)
                md.jaliases[defname] = defmd


    def generate_jna(self, base_output_dir, java_base_package="", jna_skip_builtin=False):
        if not java_base_package:
            java_base_package = ""

        jtype_char = {'field': 'Int8Field', 'array': 'CharArray', 'pointer': 'CharList'}
        jtype_int8 = {'field': 'Int8Field', 'array': 'Int8Array', 'pointer': 'Int8List'}
        jtype_int16 = {'field': 'Int16Field', 'array': 'Int16Array', 'pointer': 'Int16List'}
        jtype_int32 = {'field': 'Int32Field', 'array': 'Int32Array', 'pointer': 'Int32List'}
        jtype_int64 = {'field': 'Int64Field', 'array': 'Int64Array', 'pointer': 'Int64List'}
        jtype_float = {'field': 'FloatField', 'array': 'FloatArray', 'pointer': 'FloatList'}
        jtype_double = {'field': 'DoubleField', 'array': 'DoubleArray', 'pointer': 'DoubleList'}
        self.jaliases = {}
        self.jtypes = {
            'char': jtype_char,
            'int8_t': jtype_int8, 'uint8_t': jtype_int8, 'byte': jtype_int8,
            'int16_t': jtype_int16, 'uint16_t': jtype_int16, 'short': jtype_int16,
            'int32_t': jtype_int32, 'uint32_t': jtype_int32, 'int': jtype_int32,
            'int64_t': jtype_int64, 'uint64_t': jtype_int64, 'long': jtype_int64,
            'float': jtype_float, 'float32_t': jtype_float,
            'double': jtype_double, 'float64_t': jtype_double,
        }

        self.prepare_java_classes(java_base_package)

        if not base_output_dir:
            base_output_dir = os.getcwd()
        else:
            if base_output_dir[0] != '/':
                base_output_dir = os.path.join(os.getcwd(), base_output_dir)

        for md in self.message_definitions:
            if jna_skip_builtin:
                isBuiltin = os.path.abspath(md.filename).startswith(os.path.abspath(ln_base.builtin_msg_defs))
                if isBuiltin:
                    continue

            output_dir = os.path.join(base_output_dir, "/".join(md.jpkg.split(".")))
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)

            if md.is_service:
                self.generate_java_service(output_dir, md.jpkg, md.jclassname, md)
            elif md.is_event:
                raise Exception("event is not supported yet")
            else:  # topic
                self.generate_java_topic(output_dir, md.jpkg, md.jclassname, md)


    def generate_java_md(self, fn, sn, md, top_level, java_package="", return_signature=False):
        if java_package:
            java_package_string = "package " + java_package + ";\n"
        else:
            java_package_string = ""

        fp = open_text(fn, "wb")
        class_name = sn

        # JAVA CODE SHALL NOT BE PLATFORM DEPENDANT!!
        system_pointer_size = 8

        # dependencies
        define_map = {}
        signature_map = {}
        define_sizes = {}
        for defname, deffile in md.defines:
            md_file = get_message_definition_from_filename(deffile)
            defmd = message_definition(defname, deffile)
            defsn = self._generate_struct_name("ln_packet_%s" % md_file)
            define_map[defname] = defsn
            inner_fn = os.path.join(os.path.dirname(fn), defsn + ".java")
            define_sizes[defname], signature_map[defname] = self.generate_java_md(
                inner_fn, defsn, defmd, False, java_package, return_signature=True)
        fields = []
        field_inits = []
        field_interprets = []
        field_packs = []
        signature = []
        self.size = 0
        def append_field_definitions(field_defs, add_indent=""):
            ptr_lens = set()
            for field_type, field_name, field_count in field_defs:
                if field_type.endswith("*"):
                    ptr_lens.add(field_name + "_len")
            for field_type, field_name, field_count in field_defs:
                if field_type.endswith("*"):
                    ptr = "*"
                    jptr = "[]"
                    field_type = field_type[:-1]
                else:
                    ptr = ""
                    jptr = ""
                if field_type in define_map:
                    c_type_name = define_map[field_type]
                    sig = "[%s]" % (signature_map[field_type])
                    s = define_sizes[field_type]
                    is_unsigned = False
                    is_integral = False
                else:
                    is_integral = True
                    c_type_name = data_type_map[field_type].get_java_name()
                    real_c_type = data_type_map[field_type].get_c_name()
                    sig = real_c_type
                    is_unsigned = real_c_type[0] == "u"
                    #if ptr == "*" and field_type == "char":
                    #    c_type_name = "const %s" % c_type_name
                    s = data_type_map[field_type].get_sizeof()
                if ptr:
                    self.size += system_pointer_size
                else:
                    self.size += s * field_count
                sig = "%s%s %s %s" % (sig, ptr, s, field_count)
                if c_type_name == "byte":
                    getter = "get"
                    setter = "put"
                    upper_type = "Byte"
                else:
                    upper_type = "%s%s" % (c_type_name[0].upper(), c_type_name[1:])
                    getter = "get%s" % upper_type
                    setter = "put%s" % upper_type

                if is_unsigned:
                    unsigned_warning = " // warning: java has no unsigned version for c-type %s" % real_c_type
                else:
                    unsigned_warning = ""

                if field_count > 1 or jptr: # "array" case
                    # todo: non-integral/non-primitive case missing!
                    need_for_loop = True
                    if jptr: # dynamically sized
                        field_count = 0

                    fields.append(add_indent + "    %s[] %s;" % (c_type_name, field_name))
                    field_inits.append(add_indent + "        %s = new %s[%s];" % (field_name, c_type_name, field_count))

                    if not jptr and not is_integral: # statically sized, non primitives
                        # statically sized - construct elements
                        field_inits.append(
                            add_indent + "        for(int _i = 0; _i < %s; _i++) {" % field_count)
                        field_inits.append(
                            add_indent + "            %s[_i] = new %s();" % (field_name, c_type_name))
                        field_inits.append(
                            add_indent + "        }")


                    if jptr: # dynamically sized
                        fields.append(add_indent + "    ByteBuffer _%s_byte_buffer;" % (field_name))
                        field_count = "0"
                        field_count = "%s_len" % field_name
                        if not is_integral:
                            array_initializer = ("\n" +
                                add_indent + "            for(int _i = 0; _i < %(field_name)s_len; _i++) {\n" +
                                add_indent + "                %(field_name)s[_i] = new %(type_name)s();\n" +
                                add_indent + "            }\n") % dict(
                                field_name=field_name,
                                type_name=c_type_name)
                        else:
                            array_initializer = ""

                        field_interprets.append((
                            add_indent + "        if(%(field_name)s.length < %(field_name)s_len) {\n" +
                            add_indent + "            %(field_name)s = new %(type_name)s[%(field_name)s_len];%(array_initializer)s\n" +
                            add_indent + "        }") % dict(
                                array_initializer=array_initializer,
                                field_name=field_name,
                                type_name=c_type_name))
                        field_interprets.append((
                                add_indent + "        _%s_byte_buffer = serviceJNI.get_byte_buffer_at_from_buffer(bb, %s * %s);"
                                ) % (
                                field_name, field_count, s,
                                ))
                        field_packs.append((
                                add_indent + "        _%s_byte_buffer = ByteBuffer.allocateDirect(%s * %s);") % (
                                    field_name, field_count, s,
                                    ))
                        if is_integral:
                            if c_type_name != "byte":
                                field_interprets.append((
                                        add_indent + "        _%s_byte_buffer.order(ByteOrder.LITTLE_ENDIAN);\n" +
                                        add_indent + "        _%s_byte_buffer.as%sBuffer().get(%s, 0, %s);\n") % (
                                        field_name,
                                            field_name, upper_type, field_name,
                                            field_count))
                            else:
                                field_interprets.append((
                                        add_indent + "        _%s_byte_buffer.get(%s, 0, %s);\n") % (
                                            field_name, field_name, field_count))

                            if c_type_name != "byte":
                                field_packs.append((
                                        add_indent + "        _%s_byte_buffer.order(ByteOrder.LITTLE_ENDIAN);\n" +
                                        add_indent + "        _%s_byte_buffer.as%sBuffer().put(%s);\n" +
                                        add_indent + "        serviceJNI.put_ptr_into_byte_buffer(bb, serviceJNI.get_byte_buffer_address(_%s_byte_buffer));\n") % (
                                        field_name,
                                        field_name, upper_type, field_name,
                                        field_name
                                        ))
                            else:
                                field_packs.append((
                                        add_indent + "        _%s_byte_buffer.put(%s);\n" +
                                        add_indent + "        serviceJNI.put_ptr_into_byte_buffer(bb, serviceJNI.get_byte_buffer_address(_%s_byte_buffer));\n") % (
                                        field_name, field_name,
                                        field_name
                                        ))
                            need_for_loop = False # not needed because of more performat buffer operations
                        # non integral type needs forloop
                        byte_buffer_name = "_%s_byte_buffer" % field_name
                    else: # fixed size
                        byte_buffer_name = "bb"

                    if need_for_loop:
                        field_interprets.append(
                                add_indent + "        for(int _i = 0; _i < %s; _i++) {" % field_count)
                        field_packs.append(
                                add_indent + "        for(int _i = 0; _i < %s; _i++) {" % field_count)
                        if is_integral:
                            field_interprets.append(
                                    add_indent + "            %s[_i] = %s.%s();" % (field_name, byte_buffer_name, getter))
                            field_packs.append(
                                    add_indent + "            %s.%s(%s[_i]);%s" % (byte_buffer_name, setter, field_name, unsigned_warning))
                        else:
                            field_interprets.append(
                                    add_indent + "            %s[_i].interpret(%s);" % (field_name, byte_buffer_name))
                            field_packs.append((
                                    add_indent + "            %s.put(%s[_i].pack());") % (byte_buffer_name, field_name))
                        field_interprets.append(
                            add_indent + "        }")
                        field_packs.append(
                            add_indent + "        }")
                        if byte_buffer_name != "bb":
                            # need to transfer to bb buffer
                            field_packs.append((
                                    add_indent + "        serviceJNI.put_ptr_into_byte_buffer(bb, serviceJNI.get_byte_buffer_address(%s));") % (
                                        byte_buffer_name))

                else: # non-"array" case
                    fields.append(add_indent + "    %s%s %s;%s" % (c_type_name, jptr, field_name, unsigned_warning))
                    if not is_integral:
                        field_inits.append(add_indent + "        %s = new %s();" % (field_name, c_type_name))
                        field_interprets.append(add_indent + "        %s.interpret(bb);" % (field_name))
                        field_packs.append(add_indent + "        bb.put(%s.pack());" % (field_name))
                    else:
                        field_interprets.append(add_indent + "        %s = bb.%s();%s" % (field_name, getter, unsigned_warning))
                        if field_name in ptr_lens:
                            ptr_field_name = field_name[:-len("_len")]
                            #field_packs.append(add_indent + "        %s = %s.length;" % (field_name, ptr_field_name))
                        field_packs.append(add_indent + "        bb.%s(%s);%s" % (setter, field_name, unsigned_warning))
                signature.append(sig)
        def get_packet_definition(class_name, packet_size, members, member_inits, member_interpret, member_pack, add_indent=""):
            pmembers = []
            for line in members.split("\n"):
                m = re.match("^([ \t]*)(.*)$", line)
                pmembers.append("%spublic %s" % (m.group(1), m.group(2)))
            members = "\n".join(pmembers)
            return add_indent + ("""public class %s implements packet {
    byte[] _host_data;
    ByteBuffer bb;

%s

    public %s() {
        _host_data = new byte[%s];
        bb = ByteBuffer.wrap(_host_data);
        bb.order(ByteOrder.LITTLE_ENDIAN); // todo: this should be done inside LN
%s
    }

    public byte[] get_data() {
        return _host_data;
    }

    public void interpret(ByteBuffer obb) {
        obb.get(_host_data);
        interpret();
    }

    public void interpret() {
        bb.rewind();
%s
    }

    public byte[] pack() {
        bb.clear();
%s
        return _host_data;
    }

}
""" % (
                class_name,
                members,
                class_name,
                packet_size,
                member_inits,
                member_interpret,
                member_pack
                )).replace("\n", "\n%s" % add_indent)

        if not md.is_service:
            append_field_definitions(md.fields)
            fp.write("""%simport java.nio.ByteBuffer;
import java.nio.ByteOrder;

//import android.util.Log;

import de.dlr.robotic.ln.serviceJNI;
import de.dlr.robotic.ln.packet;

// this file is generated by ln_generate from
// %s
// %s (%d bytes)
// do not edit!

%s
""" % (
                    java_package_string,
                    md.filename, md.name, self.size,
                    get_packet_definition(
                        class_name,
                        self.size,
                        "\n".join(fields),
                        "\n".join(field_inits),
                        "\n".join(field_interprets),
                        "\n".join(field_packs))
                    ))
            if return_signature:
                return self.size, ",".join(signature)
            return self.size
        # its a service!
        if md.fields:
            #fields.append("\tstruct __attribute__((packed)) {")
            append_field_definitions(md.fields)
            req_signature = ",".join(signature)
            req_packet = "%spublic req_packet req;\n" % (
                get_packet_definition(
                    "req_packet",
                    self.size,
                    "\n".join(fields),
                    "\n".join(field_inits),
                    "\n".join(field_interprets),
                    "\n".join(field_packs),
                    add_indent="    ")
                )
            req_packet_init = "        req = new req_packet();\n"
            req_host_size = self.size
            del signature[0:]
            del fields[0:]
            del field_inits[0:]
            del field_interprets[0:]
            del field_packs[0:]
            self.size = 0
            #fields.append("\t} req;")
        else:
            req_signature = ""
            req_packet = ""
            req_packet_init = ""
            req_host_size = 0
        if md.resp_fields:
            #fields.append("\tstruct __attribute__((packed)) {")
            append_field_definitions(md.resp_fields)
            resp_signature = ",".join(signature)
            signature = "%s|%s" % (req_signature, resp_signature)
            resp_packet = "%spublic resp_packet resp;\n" % (
                get_packet_definition(
                    "resp_packet",
                    self.size,
                    "\n".join(fields),
                    "\n".join(field_inits),
                    "\n".join(field_interprets),
                    "\n".join(field_packs),
                    add_indent="    ")
                )
            resp_packet_init = "        resp = new resp_packet();\n"
            resp_host_size = self.size
            #fields.append("\t} resp;")
        else:
            signature = "%s|" % (req_signature)
            resp_signature = ""
            resp_packet = ""
            resp_packet_init = ""
            resp_host_size = 0

        class_name = sn
        fp.write("""%s
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

//import android.util.Log;

import de.dlr.robotic.ln.serviceJNI;
import de.dlr.robotic.ln.service_packet;
import de.dlr.robotic.ln.packet;

// this file is generated by ln_generate from
// %s
// %s
// do not edit!

public class %s extends service_packet {
    byte[] host_data;
    ByteBuffer bb;
%s
%s
    public static final String signature = "%s";
    public String get_signature() {
        return signature;
    }

    public %s() {
        host_data = new byte[%s]; // host bytes! req: %s, resp: %s
        bb = ByteBuffer.wrap(host_data);
        bb.order(ByteOrder.LITTLE_ENDIAN); // todo: this should be done inside LN
%s%s
    }
    public byte[] get_host_data() {
        return host_data;
    }
""" % (
                java_package_string,
                md.filename, md.name,
                class_name,
                req_packet, resp_packet,
                signature,
                class_name,
                req_host_size + resp_host_size, req_host_size, resp_host_size,
                req_packet_init, resp_packet_init,
                ))
        if req_packet:
            fp.write("""
    public packet get_req() {
	return req;
    }
    public byte[] prepare_request() {
        bb.clear();
        bb.put(req.pack());
        return host_data;
    }
    public void unpack_request() {
        bb.rewind();
        req.interpret(bb);
    }
""")
        else:
            fp.write("""
    public packet get_req() {
	return null;
    }
    public byte[] prepare_request() {
        bb.clear();
        return host_data;
    }
    public void unpack_request() {
        bb.rewind();
    }
""")
        if resp_packet:
            fp.write("""
    public packet get_resp() {
	return resp;
    }
    public byte[] prepare_response() {
        bb.put(resp.pack());
        return host_data;
    }
    public void unpack_response() {
        // bb should point directly after req_data!
        resp.interpret(bb);
    }
""")
        else:
            fp.write("""
    public packet get_resp() {
	return null;
    }
    public byte[] prepare_response() {
        return host_data;
    }
    public void unpack_response() {
        // bb should point directly after req_data!
    }
""")
        fp.write("""
}
""")
        return 0 # size has no meaning!

    def generate_java(self, output_dir, java_package):
        if java_package:
            output_dir = os.path.join(output_dir, "/".join(java_package.split(".")))
        for md in self.message_definitions:
            if not md.is_service:
                sn = self._generate_struct_name("ln_packet_%s" % md.name)
            else:
                sn = self._generate_struct_name("ln_service_%s" % md.name)

            class_name = sn
            fn = class_name + ".java"
            fn = os.path.join(output_dir, fn)

            members = ""
            member_inits = ""
            member_interpret = ""

            self.generate_java_md(fn, sn, md, True, java_package)

def compute_message_definition_size(name):
    fn = search_message_definition(name)
    md = message_definition(name, fn)
    return md.calculate_size()


def compute_message_definition_string(name):
    def calculate_string(md):
        # dependencies
        txt = ''

        for field_type, field_name, field_count in md.fields:
            typ = data_type_map[field_type].pyformat
            txt = txt + typ + '|' + str(field_name) + '|' + str(field_count) + '||'

        return txt

    fn = search_message_definition(name)
    md = message_definition(name, fn)
    return calculate_string(md)

def fill_template(template_fn, data):
    fn = os.path.join(
        os.path.dirname(__file__),
        "templates",
        template_fn
    )
    with open_text(fn, "rb") as fp:
        template = fp.read()
    return template % data
