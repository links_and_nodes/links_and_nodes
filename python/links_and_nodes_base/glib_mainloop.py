from links_and_nodes_base.mainloop import MainloopInterface

import traceback

import gi
gi.require_version('GLib', '2.0') 
from gi.repository import GLib # noqa: E402

all = [ "GLibMainloop" ]


class GLibCbWrapper(object):
    def __init__(self, cb, args):
        self.cb = cb
        self.args = args
    def __call__(self, none):
        try:
            return self.cb(*self.args)
        except Exception:
            print("in callback %s():\n%s" % (
                str(self.cb), traceback.format_exc()))
            return False


class GLibMainloop(MainloopInterface):
    """
    using a Glib.MainLoop from
    from gi.repository import GLib    

    https://docs.gtk.org/glib/
    """
    default = None

    @classmethod
    def get_for_default_context(cls):
        if cls.default is None:
            cls.default = GLibMainloop()
        return cls.default
    
    def __init__(self, context=None, loop=None):
        """
        if loop is None, a new GLib.MainLoop will be instantiated,
        if context is None, default context will be used for that new MainLoop
        https://docs.gtk.org/glib/ctor.MainLoop.new.html
        """
        MainloopInterface.__init__(self)
        if loop:
            self.loop = loop
        else:
            self.loop = GLib.MainLoop(context)
            if context is None and GLibMainloop.default is None:
                GLibMainloop.default = self
        self.ctx = self.loop.get_context()

    def run(self):
        self.loop.run()

    def quit(self):
        self.loop.quit()
    
    def iterate(self, blocking=True):
        self.ctx.iteration(blocking)

    def timeout_add(self, timeout_in_seconds, callback, *args):
        source = GLib.timeout_source_new(int(timeout_in_seconds * 1e3))
        source.set_callback(GLibCbWrapper(callback, args))
        return source.attach(self.ctx)

    def idle_add(self, callback, *args):
        source = GLib.idle_source_new()
        source.set_callback(GLibCbWrapper(callback, args))
        return source.attach(self.ctx)

    def fd_add(self, fd, condition, callback, *args):
        iocondition = None
        def set_or(value, mask):
            if value is None:
                return mask
            return value | mask
        if condition == "in"  or "in"  in condition: iocondition = set_or(iocondition, GLib.IOCondition.IN)
        if condition == "out" or "out" in condition: iocondition = set_or(iocondition, GLib.IOCondition.OUT)
        if condition == "hup" or "hup" in condition: iocondition = set_or(iocondition, GLib.IOCondition.HUP)
        if condition == "err" or "err" in condition: iocondition = set_or(iocondition, GLib.IOCondition.ERR)
        if hasattr(fd, "fileno"):
            fd = fd.fileno()
        channel =  GLib.IOChannel.unix_new(fd)
        source = GLib.io_create_watch(channel, iocondition)
        source.set_callback(GLibCbWrapper(callback, args))
        return source.attach(self.ctx)
    
    def source_remove(self, source_id):
        GLib.source_remove(source_id)
