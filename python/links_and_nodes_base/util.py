"""
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
"""
import inspect

import sys

is_py2 = sys.version_info < (3, 0)

def args_to_self(f):
    def args_to_self_decorator(*args, **kwargs):
        self = args[0]
        if is_py2:
            arg_names, _varargs, _keywords, defaults = inspect.getargspec(f)
        else:
            arg_names, _varargs, _keywords, defaults, _kwonlyargs, _kwonlydefaults, annotations = inspect.getfullargspec(f)

        if defaults:
            defaults = list(defaults)
        else:
            defaults = []

        # set positional arguments
        for arg in args:
            name = arg_names.pop(0)
            setattr(self, name, arg)
        
        # set kwargs
        for arg_name, arg in kwargs.items():
            setattr(self, arg_name, arg)
            defaults.pop(arg_names.index(arg_name))
            arg_names.remove(arg_name)
        
        # set defaults
        for arg_name, arg in zip(arg_names, defaults):
            setattr(self, arg_name, arg)
        return f(*args, **kwargs)
    return args_to_self_decorator
