
# this is a copy of conanfile.py but without python-dist requires
# ln_generate and all other python stuff in this package should
# run on any python >= 2.7

import os

from conans import ConanFile, tools


class links_and_nodes_base_python_buildtool_conan(ConanFile):
    python_requires = "ln_conan/[~5]@common/unstable"
    python_requires_extend = "ln_conan.Base"
    
    name = "links_and_nodes_base_python_buildtool"
    homepage = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"
    license = "LGPLv3"
    description = "links_and_nodes is a framework for easy creation and maintanance of distributed computing networks."
    settings = "os", "arch", "compiler", "build_type" # those are needed for conan 2 builds but deleted from the package id
    exports = [ "../../site_scons*" ]
    exports_sources = [
        # our self
        "../../python*",
        "!python/links_and_nodes_base/*.pyc",
        "!python/links_and_nodes/*",
        "!python/pyutils/*.pyc",
        "!python/pyutils/*.pyx",
        "!python/pyutils/.*",
        
        # will need base-build-system
        "../../SConstruct",        
    ] + ["!%s" % x for x in tools.Git().excluded_files()]

    def package_id(self):
        # the package id should not depend on any settings, delete all of them
        del self.info.settings.os
        del self.info.settings.arch
        del self.info.settings.build_type
        del self.info.settings.compiler

    def requirements(self):
        self.requires("links_and_nodes_ln_msgdef/%s" % self.same_branch_or("[>=1.2 <3]"))

    def source(self):
        self.write_version_file(os.path.join("libln", "version"))
            
    def build(self):
        self.scons_build("python", "--no-manager --no-python-api --no-python-lnrdb", with_python_lib=True)

    def package(self):
        install = os.path.join("build", "python", self.install_sandbox, self.prefix[1:])
        self.copy("version", src="libln", dst=os.path.join(self.python_path, "links_and_nodes_base")) # todo: this is strange
        self.copy("*", src=install, symlinks=True)
        
    def package_info(self):
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
        self.env_info.PYTHONPATH.append(os.path.join(self.package_folder, self.python_path))
