"""
    Copyright 2013-2019 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import print_function
import sys
import os
import glob
import traceback

ln_debug = os.getenv("LN_DEBUG")

if ln_debug:
    def output_env_path_list(name):
        value = os.getenv(name)
        print("links_and_nodes_base:", name, end=" ")
        if value is None:
            print("not set!")
            return
        print("=")
        for i, entry in enumerate(value.split(os.pathsep)):
            print(" ", entry, end="")
            if os.path.isdir(entry):
                if os.access(entry, os.R_OK | os.X_OK):
                    print()
                else:
                    print(" --> missing access rights!")
            else:
                print(" --> does not exist!")
    for var in [ "LD_LIBRARY_PATH", "PATH", "PYTHONPATH" ]:
        output_env_path_list(var)

    def search_in_env_path_list(name, pattern):
        value = os.getenv(name)
        found_some = False
        if value is not None:
            for entry in value.split(os.pathsep):
                results = glob.glob(os.path.join(entry, pattern))
                for res in results:
                    print("found %s" % res, end="")
                    while os.path.islink(res):
                        link = os.readlink(res)
                        print(" -> %s" % link, end=" ")
                        res = os.path.join(os.path.dirname(res), link)
                    if os.path.isdir(res):
                        print(os.sep)
                    else:
                        print(" (%d bytes)" % os.path.getsize(res))
                    found_some = True
        if not found_some:
            print("links_and_nodes_base: warning: can not find %r in %s!" % (pattern, name))
    search_in_env_path_list("PYTHONPATH", "links_and_nodes")
    search_in_env_path_list("LD_LIBRARY_PATH", "libln*")

if os.getenv("LN_COVERAGE_RECORD", "0")[:1] in "1yt":
    already_enabled = hasattr(sys, "_ln_coverage_trace_active") and sys._ln_coverage_trace_active
    if not already_enabled:
        if ln_debug: print("links_and_nodes_base: coverage recording was requested via environment!")
        lntest_path = os.getenv("LN_COVERAGE_RECORD_LNTEST_BASE", None)
        if lntest_path:
            sys.path.insert(0, lntest_path)
        import lntest.coverage_monitor
        sys._ln_coverage_monitor = lntest.coverage_monitor.coverage_monitor()
        coverage_fn = os.path.join(os.getenv("LN_COVERAGE_RECORD_DIR", os.getcwd()), "%s_%s_coverage.py" % (
            os.getenv("LN_COVERAGE_RECORD_NAME", "ln_base"), os.getpid()))
        sys._ln_coverage_monitor.start_recording(coverage_fn)
        if ln_debug: print("links_and_nodes_base: coverage recording started to %r" % coverage_fn)

additional_message_definition_dirs = []
message_definition_dirs = []

package_base = os.path.dirname(os.path.dirname(__file__))
if ln_debug: print("links_and_nodes_base: python-path: %r" % package_base)
site_packages_dir = os.path.dirname(os.path.dirname(__file__))

source_dir = os.path.dirname(os.path.realpath(package_base))
source_build_dir = os.path.join(source_dir, os.getenv("LN_BUILD_SUBDIR", "build"))
source_libln_dir = os.path.join(source_dir, "libln")

if os.path.isdir(source_dir) and os.path.isdir(source_libln_dir):
    # source tree
    if ln_debug: print("links_and_nodes_base: detected source tree!")
    ln_tree = "source"
    ln_release_version = "src"
    ln_libdir = os.path.join(source_build_dir, "libln")
    ln_manager_dir = os.path.join(source_dir, "python", "links_and_nodes_manager")
    ln_share_dir = os.path.join(source_dir, "share")
    builtin_msg_defs = os.path.join(ln_share_dir, "message_definitions")
    ln_runtime_dir = os.path.join(source_dir, "ln_runtime")
    runtime_msg_defs = [
        os.path.join(ln_runtime_dir, "lnrecorder", "message_definitions"),
        os.path.join(ln_runtime_dir, "file_services", "message_definitions")
    ]

    # import own pyutils
    import pyutils
    import pyutils.line_assembler # noqa: F401

else: # assume installed into site_packages
    prefix = os.path.dirname(os.path.dirname(os.path.dirname(package_base)))
    if ln_debug: print("links_and_nodes_base: detected install in prefix %r" % prefix)
    ln_tree = "install"
    ln_share_dir = os.path.join(prefix, "share", "links_and_nodes")
    ln_release_version = "install-unknown"
    ptfn = os.path.join(os.path.dirname(__file__), "version")
    try:
        with open(ptfn, "rb") as fp:
            ln_release_version = fp.read().strip()
    except Exception:
        ln_release_version = "<unknown release>"
    ln_libdir = os.path.join(prefix, "lib")
    ln_manager_dir = os.path.join(site_packages_dir, "links_and_nodes_manager")
    builtin_msg_defs = os.path.join(ln_share_dir, "message_definitions")
    runtime_msg_defs = [
        os.path.join(prefix, "share", "ln_runtime", "message_definitions")
    ]

default_gen_msg_defs = os.path.join(os.path.expanduser("~"), "ln_message_definitions", "gen")

ln_config_file = os.path.expanduser("~/.ln_config")
if os.path.isfile(ln_config_file):
    ln_config_reader = "client"
    exec(open(ln_config_file, "rb").read())

def rescan_message_definition_dirs():
    global message_definition_dirs, additional_message_definition_dirs
    
    message_definition_dirs = list(additional_message_definition_dirs)
    message_definition_dirs.append(builtin_msg_defs)
    message_definition_dirs += runtime_msg_defs
    message_definition_dirs.append(os.path.join(os.path.expanduser("~"), "ln_message_definitions"))

    md = os.getenv("LN_MESSAGE_DEFINITION_DIRS")
    if md is not None:
        message_definition_dirs.extend([os.path.realpath(s.strip()) for s in md.split(os.path.pathsep)])
    
    # search for md.conf files in these dirs!
    while True:
        new_md_dirs = []
        for dir in message_definition_dirs:
            if os.path.isdir(dir):
                fn = os.path.join(dir, "md.conf")
            elif os.path.isfile(dir):
                fn = dir
            else:
                continue
            if not os.path.isfile(fn):
                continue
            with open(fn, "r") as fp:
                for line in fp:
                    line = line.strip()
                    if not line or line[0] == "#":
                        continue
                    line = os.path.expanduser(line)
                    if not os.path.isabs(line):
                        line = os.path.join(os.path.dirname(fn), line)
                    new_dir = os.path.realpath(line)
                    if os.path.exists(new_dir) and new_dir not in message_definition_dirs:
                        new_md_dirs.append(new_dir)
        if not new_md_dirs:
            break # done
        for dir in new_md_dirs:
            if dir not in message_definition_dirs:
                message_definition_dirs.append(dir)

    # remove files from these dirs!
    message_definition_dirs = [dir for dir in message_definition_dirs if os.path.isdir(dir)]

postprocess_md_dirs = rescan_message_definition_dirs
rescan_message_definition_dirs()

from links_and_nodes_base.message_definitions import open_text, search_message_definition, data_type_map, message_definition_header, message_definition, compute_message_definition_size, get_message_definition_from_filename, compute_message_definition_string # noqa: E402,F401
from links_and_nodes_base.lnm_remote import pickle_socket_wrapper, lnm_remote # noqa: E402,F401
from links_and_nodes_base import util # noqa: E402,F401

def GLibMainloop(*args, **kwargs):
    from .glib_mainloop import GLibMainloop
    return GLibMainloop(*args, **kwargs)
def SelectMainloop(*args, **kwargs):
    from .select_mainloop import SelectMainloop
    return SelectMainloop(*args, **kwargs)

from links_and_nodes_base.mainloop import MainloopInterface # noqa: E402,F401

_ln_tb_strip_fn = os.getenv("LN_TB_STRIP_FN", None)
if _ln_tb_strip_fn:
    _ln_tb_strip_fn = _ln_tb_strip_fn.split(os.pathsep)
    if not hasattr(traceback, "org_StackSummary_format"):
        traceback.org_StackSummary_format = traceback.StackSummary.format
    def my_StackSummary_format(self, _ln_tb_strip_fn=_ln_tb_strip_fn):
        if _ln_tb_strip_fn:
            for frame in self:
                for prefix in _ln_tb_strip_fn:
                    if frame.filename.startswith(prefix):
                        frame.filename = "..." + frame.filename[len(prefix):]
                        break
        return traceback.org_StackSummary_format(self)
    traceback.StackSummary.format = my_StackSummary_format
    def my_excepthook(type, value, tb):
        traceback.print_exception(type, value=value, tb=tb)
    sys.excepthook = my_excepthook
