#!/usr/bin/env python3

"""
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import print_function

import sys
import pprint
import select
import socket
import struct
import traceback
import os

try:
    from cStringIO import BytesIO
except ImportError:
    from io import BytesIO

try:
    import cPickle as pickle
except ImportError:
    import pickle

debug_requests_folder = os.getenv("LN_DEBUG_REQUESTS_FOLDER", "")

def recv_all(fd, n_bytes):
    all = []
    while n_bytes:
        try:
            data = fd.recv(n_bytes)
        except Exception:
            if "10035" in str(sys.exc_info()[1]):
                # windows: socket.error: [Errno 10035] Ein nicht blockierender Socketvorgang konnte nicht sofort ausgefhrt werden
                select.select([fd], [], []) # block!
                continue
            raise
        if data == b"":
            return data
        all.append(data)
        n_bytes -= len(data)
    return b"".join(all)

class pickle_target(object):
    def __init__(self):
        self.set()
    def set(self, what=None):
        if what is not None:
            self._buf = BytesIO(what)
        else:
            self._buf = BytesIO()
    
    def write(self, data):
        self._buf.write(data)
    def get(self):
        ret = self._buf.getvalue()
        self.set()
        return ret
    
    def read(self, n):
        return self._buf.read(n)
    def readline(self):
        return self._buf.readline()
    

class pickle_socket_wrapper(object):
    def __init__(self, sfd, debug=False, blocking=False):
        self.sfd = sfd
        self.debug = debug
        self.blocking = blocking
        self.print_stats = False
        self.proto = pickle.HIGHEST_PROTOCOL
        
        self.bytes_sent = 0
        self.bytes_received = 0
        self.last = {}
    
        self.pprinter = pprint.PrettyPrinter(indent=1, width=120)
        self.pprint = self.pprinter.pprint

        self.pickler_fp = pickle_target()
        self.pickler = pickle.Pickler(self.pickler_fp, self.proto) # will remember which objects were already sent

        self.unpickler_fp = pickle_target()
        self.unpickler = pickle.Unpickler(self.unpickler_fp)
        
        if debug_requests_folder:
            print("debug_requests_folder is set to %r" % debug_requests_folder)
            if not os.path.isdir(debug_requests_folder):
                os.makedirs(debug_requests_folder)
        
        self._send_packets = []
        self._packets_dropped = 0
        self._blocked = False
        self._drop_ok_default = False

    def _get_debug_requests_fn(self, bn):
        if not hasattr(self, "_start_ts"):
            import datetime
            self._start_ts = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
            self._debug_req_cnt = dict()
            dn = os.path.join(debug_requests_folder, self._start_ts)
            os.makedirs(dn)
        cnt = self._debug_req_cnt.get(bn, 0) + 1
        self._debug_req_cnt[bn] = cnt
        return os.path.join(debug_requests_folder, self._start_ts, "%s_%s" % (cnt, bn))
    
    def _send_enqueue(self, data, drop_ok):
        if self._blocked and drop_ok:
            self._packets_dropped += 1
            print("drop packet instead of queue! (dropped %d packets, %d packets still in queue)" % (self._packets_dropped, len(self._send_packets)))
            return 0

        try:
            self.pickler.dump(data)
            data = self.pickler_fp.get()
        except Exception:
            self.exc_data = data
            raise Exception("can not pickle this data:\n%s\ncall-stack:\n%s\ndata:\n%s" % (
                traceback.format_exc(), "".join(traceback.format_stack()), pprint.pformat(data)))
        if debug_requests_folder:
            fn = self._get_debug_requests_fn("send")
            with open(fn, "wb") as fp:
                fp.write(data)
        data = b"%s%s" % (struct.pack("I", len(data)), data)
        self._send_packets.append((data, drop_ok))
        return len(data)

    def _process_send_queue(self):
        """
        this is nonblocking send!
        returns False if send queue is empty.
        on True return it should be retried (idle or timeout...)
        """
        while self._send_packets:
            buffer, drop_ok = self._send_packets.pop(0)
            if self.blocking:
                n_bytes_sent = self.sfd.send(buffer)
            else:
                try:
                    n_bytes_sent = self.sfd.send(buffer, socket.MSG_DONTWAIT)
                except socket.error as ex:
                    if ex.errno == 11: # temporarily unavailable
                        n_bytes_sent = 0
                    else:
                        raise
            if n_bytes_sent < len(buffer):
                self._blocked = True
                # requeue!
                if n_bytes_sent > 0:
                    if drop_ok:
                        print("got only %d bytes thru of drop_ok packet of len %d" % (n_bytes_sent, len(buffer)))
                    else:
                        print("got only %d bytes thru of NON-drop_ok packet of len %d" % (n_bytes_sent, len(buffer)))
                if n_bytes_sent == 0 and drop_ok:
                    print("drop packet because we're blocked (dropped %d packets)!" % self._packets_dropped)
                    self._packets_dropped += 1
                    continue
                self._send_packets.insert(0, (buffer[n_bytes_sent:], drop_ok and n_bytes_sent == 0)) # drop is not ok if we already started to transfer!
                return True # we want to send more!!!
            self._blocked = False
        return False # we finished send queue!
        
    def send(self, data, drop_ok=None):
        """
        drop_ok: packet can be dropped if connection is blocked
        """
        if drop_ok is None:
            drop_ok = self._drop_ok_default
        #print "send"
        #pprint.pprint(data)
        if self.debug:
            print("sending")
            self.pprint(data)
        self.bytes_sent += self._send_enqueue(data, drop_ok=drop_ok)
        try_again = self._process_send_queue()
        
        if self.debug or self.print_stats:
            self.sometimes("sent", self.bytes_sent)
        return try_again

    def recv_all(self, n_bytes):
        return recv_all(self.sfd, n_bytes)
        
    def recv(self):
        self._process_send_queue()
        n_bytes = self.recv_all(4)
        if n_bytes == b"":
            return b""
        self.bytes_received += 4
        n_bytes = struct.unpack("I", n_bytes)[0]
        data = self.recv_all(n_bytes)
        if data == "":
            return ""
        if len(data) != n_bytes:
            raise Exception("not all data arrived %d from %d!" % (len(data), n_bytes))
        self.bytes_received += n_bytes
        try:
            self.unpickler_fp.set(data)
            pydata = self.unpickler.load()
        except ModuleNotFoundError:
            einfo = str(sys.exc_info()[1])
            if "links_and_nodes_manager" in einfo:
                hint = "\nmake sure you have a recent version of `links_and_nodes_manager` available in your PYTHONPATH!"
            else:
                hint = ""
            raise Exception("can not unpickle received objects: incomplete PYTHONPATH: %s%s" % (einfo, hint))
        except Exception:
            with open("/tmp/last_data_pickle_error", "wb") as fp:
                fp.write(data)
            if n_bytes != len(data):
                raise Exception("protocol error: data loss?\n%s\nwanted to read n_bytes %d, got %d" % (
                    sys.exc_info()[1], n_bytes, len(data)))
            raise Exception("protocol error: data loss?\n%s\n" % (traceback.format_exc()))
        if self.debug:
            print("received")
            self.pprint(pydata)            
        if debug_requests_folder:
            fn = self._get_debug_requests_fn("recv")
            with open(fn, "wb") as fp:
                fp.write(data)
        if self.debug or self.print_stats:
            self.sometimes("recv", self.bytes_received)
        return pydata

    def sometimes(self, what, bytes):
        if what in self.last:
            last = self.last[what]

            if bytes - last > 1024:
                print("bytes %s: %.1fkB" % (what, bytes / 1024.))
        self.last[what] = bytes


class lnm_remote(object):    
    def __init__(self, address="localhost:54123", use_gtk_mainloop=False, use_mainloop_iterate=None, use_gobject=False, debug=False, mainloop=None):
        self.print_debug = debug
        self.address = address
        self.connected = False

        self.debug = self.simple_print
        self.info = self.simple_print
        self.warning = self.simple_print
        self.error = self.simple_print

        self.pprinter = pprint.PrettyPrinter(indent=1, width=120)
        self.pprint = self.pprinter.pprint
        self._pass_exceptions = True

        self.mainloop = mainloop

        if self.mainloop:
            self.use_mainloop_iterate = self.mainloop.iterate
        elif use_gtk_mainloop:
            import gtk
            self.use_mainloop_iterate = lambda: gtk.main_iteration(True)
        else:
            self.use_mainloop_iterate = use_mainloop_iterate

        if self.use_mainloop_iterate is None:
            import time
            def blocking_mainloop(timeout=None, deadline=None, raise_on_timeout=True):
                self.pickle_fd._process_send_queue()
                if timeout is None and deadline is None:
                    return self.on_data(None, None)
                if deadline is None:
                    if timeout is None:
                        timeout = 5
                    start = time.time()
                    deadline = start + timeout
                remaining_time = deadline - time.time()
                if remaining_time < 0:
                    remaining_time = 0
                readfds, writefds, excfds = select.select([self.sfd], [], [self.sfd], remaining_time)
                if not readfds and not writefds and not excfds:
                    if not raise_on_timeout:
                        return
                    raise Exception("timeout")
                self.on_data(None, None)
            self.use_mainloop_iterate = blocking_mainloop
        
        self.use_gobject = use_gobject

        self._connect()

    def simple_print(self, msg):
        sys.stdout.write(msg)

    def _connect(self):
        # establish tcp connection to manager!
        self.sfd = socket.socket()
        host, port = self.address.split(":")
        port = int(port)
        self.sfd.connect((host, port))
        # send magic
        self.sfd.send(b"GUI\0")
        self.pickle_fd = pickle_socket_wrapper(self.sfd, debug=False, blocking=True)
        if self.mainloop:
            self.io_watch = self.mainloop.fd_add(self.sfd, "in", self.on_data, self.sfd, "in")
        elif self.use_gobject:
            import gobject
            self.io_watch = gobject.io_add_watch(self.sfd, gobject.IO_IN, self.on_data)
        self.sfd.setblocking(1)
        self.sfd.settimeout(None)
        self.connected = True
        self.request_id = 0
        self.request_callbacks = {}
        self.responses = {}
        self.response_cbs = {}

    def on_data(self, fd, why):
        r = self.pickle_fd.recv()
        if r == "":
            self.error("EOF from manager!")
            if self._pass_exceptions:
                self.connected = False
                raise Exception("EOF from manager!")
            return False
        if r[0] == "notify":
            kwargs = r[1]
            if self.print_debug:
                print("hook: new_notification")
                self.pprint(kwargs)
            self.on_notification(kwargs)
        elif r[0] == "request_response":
            request_id, args, kwargs = r[1:]
            if self.print_debug:
                print("request_response")
                self.pprint(args)
                self.pprint(kwargs)
            cb = self.request_callbacks.get(request_id)
            if cb:
                del self.request_callbacks[request_id]
                cb(*args, **kwargs)
        elif r[0] == "answer":
            request_id = r[1]
            if self.print_debug:
                print("answer for request_id %r" % request_id)
                self.pprint(r)
            cb = self.response_cbs.get(request_id)
            if cb is not None:
                del self.response_cbs[request_id]                
                if r[0] == "exception":
                    cb(exception=r[2])
                    return True
                cb(r[2])
                return True
            self.responses[request_id] = r
        elif r[0] == "exception":
            request_id = r[1]
            if self.print_debug:
                print("exception for request_id %r" % request_id)
                self.pprint(r)
            self.responses[request_id] = r
        else:
            self.error("received unknown data from manager:\n%s" % (
                self.pformat(r)))
        return True
        
    def on_notification(self, kwargs):
        request = kwargs.get("request")
        if request == "log_msg":
            self.on_log_messages(kwargs["msgs"])
        elif request == "update_obj_state":
            self.on_obj_state(kwargs["obj_type"], kwargs["obj_name"], kwargs["obj_state"])
        elif request == "update_state_state":
            self.on_obj_state("State", kwargs["state_name"], kwargs["up_down"])
        elif request == "process_output":
            self.on_output(kwargs["name"], kwargs["output"])
        return True

    def request(self, request, *args, **kwargs):
        try:
            # formulate request and send to manager
            self.request_id += 1
            request_id = self.request_id
            if request == "send_request":
                obj, req, cb = args
                obj_type = obj.__class__.__name__
                obj_id = obj.get_instance_id()
                self.request_callbacks[self.request_id] = cb
                self.pickle_fd.send(("send_request", obj_type, obj_id, request_id, req, kwargs))
                return
            if request == "call": # sync call, not good...
                # sync call method of arbitrary object in manager
                method = args[0]
                obj = method.__self__
                method = method.__name__
                obj_type = obj.__class__.__name__
                obj_id = obj.get_instance_id() # obj needs to be derived from tracked_object
                self.pickle_fd.send((request, obj_type, obj_id, request_id, method, args[1:], kwargs))
                while request_id not in self.responses:
                    self.use_mainloop_iterate()
                answer = self.responses[request_id]
                del self.responses[request_id]
                # blocking read answer!
                if answer[0] == "exception":
                    raise Exception("exception on manager side:\n%s" % answer[2])
                # expect answer[0] == "answer"
                return answer[2]
            if request == "call_cb": # expect callback after method!
                # sync call method of arbitrary object in manager
                method = args[0]
                obj = method.__self__
                if method.__name__ == "with_connection_wrapper":
                    # find function "behind" decorator!
                    #import pdb; pdb.set_trace()
                    if len(method.__closure__) != 4 or not callable(method.__closure__[2].cell_contents) or not callable(method.__closure__[3].cell_contents) or not isinstance(method.__closure__[0].cell_contents, int):
                        raise Exception("with_connection_wrapper closure changed!! please check this code here! (closure cells contents: %s)" % (
                            ", ".join(map(repr, [cell.cell_contents for cell in method.__closure__]))))
                    method = method.__closure__[3].cell_contents
                method = method.__name__                
                obj_type = obj.__class__.__name__
                obj_id = obj.get_instance_id()
                self.response_cbs[request_id] = args[1]
                self.pickle_fd.send((request, obj_type, obj_id, request_id, method, args[2:], kwargs))
                return
            if request == "mgr_cb": # call method of manager itself, expect callback after method name!
                # sync call method of arbitrary object in manager
                method = args[0]
                self.response_cbs[request_id] = args[1]
                self.pickle_fd.send((request, request_id, method, args[2:], kwargs))
                return
            # sync call method of manager
            self.pickle_fd.send(("proxy", request_id, request, args, kwargs))
            while request_id not in self.responses:
                self.use_mainloop_iterate()
            answer = self.responses[request_id]
            del self.responses[request_id]
            # blocking read answer!
            if answer[0] == "exception":
                raise Exception("exception on manager side:\n%s" % answer[2])
            # expect answer[0] == "answer"
            return answer[2]
        except Exception:
            self.error("error in manager_proxy while calling\n%r(%r, **%r)\n%s" % (
                request, args, kwargs,
                traceback.format_exc()))
            if "Broken pipe" in str(sys.exc_info()[1]):
                self.connected = False
            if self._pass_exceptions:
                raise

    # please overload those:
    def on_log_messages(self, msgs):
        print("%d new log messages:" % len(msgs))
        for mid, ts, ll, src, msg, tb in msgs:
            print(" ", ts.strftime("%H:%M:%S"), msg.replace("\n", "\n         "))
    
    def on_obj_state(self, obj_type, name, state):
        print("new state %r for object %s: %r" % (state, obj_type, name))

    def on_output(self, name, output):
        print("new process output for object %r:\n   %s" % (
            name, output.replace("\n", "\n   ").rstrip()))
