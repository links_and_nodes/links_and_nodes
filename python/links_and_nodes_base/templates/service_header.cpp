/* %(mdname)s */
#define %(sn_no_t)s_signature "%(service_signature)s"
#include "ln/packed.h"
typedef struct LN_PACKED {
%(fields)s%(typedefs)s
} %(sn)s;
#include "ln/endpacked.h"

#ifdef __cplusplus
#ifdef %(md_prefix_ns_define)s
namespace %(md_prefix_ns_define)s {
#endif
%(ns_opening)s

typedef %(sn)s %(name)s_t;

class %(class_name)s {
public:
	virtual ~%(class_name)s() {
		unregister_%(name)s();
	}
private:
	static int cb(::ln::client&, ::ln::service_request& req, void* user_data) {
		%(class_name)s* self = (%(class_name)s*)user_data;
		%(name)s_t data;
		req.set_data(&data, %(sn_no_t)s_signature);%(clear_resp)s
		return self->on_%(name)s(req, data);
	}
protected:
	::ln::service* svc;
	
	%(class_name)s() : svc(NULL) {};
	
	void unregister_%(name)s() {
		if(svc) {
			svc->clnt->release_service(svc);
			svc = NULL;
		}
	}
	void register_%(name)s(::ln::client* clnt, const std::string service_name, const char* group_name=NULL) {
		svc = clnt->get_service_provider(
			service_name,
			"%(mdname)s", 
			%(sn_no_t)s_signature);
		svc->set_handler(&cb, this);
		svc->do_register(group_name);
	}
	virtual int on_%(name)s(::ln::service_request& req, %(name)s_t& data) = 0;
};
%(ns_closing)s
#ifdef %(md_prefix_ns_define)s
}
#endif
#endif
