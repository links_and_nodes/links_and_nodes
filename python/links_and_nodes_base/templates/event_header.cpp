
/* %(mdname)s */
#define %(sn_no_t)s_connect_signature "%(mdname)s: %(connect_signature)s"
#define %(sn_no_t)s_call_signature "%(mdname)s: %(call_signature)s"

#include "ln/packed.h"
typedef struct LN_PACKED {
%(call_fields)s
} %(sn_no_t)s_call_t;
#include "ln/endpacked.h"

#include "ln/packed.h"
typedef struct LN_PACKED {
%(fields)s
} %(sn_no_t)s_connect_t;
#include "ln/endpacked.h"

#ifdef __cplusplus
#ifdef %(md_prefix_ns_define)s
namespace %(md_prefix_ns_define)s {
#endif
%(ns_opening)s
typedef %(sn_no_t)s_connect_t %(bname)s_connect_t;
typedef %(sn_no_t)s_call_t    %(bname)s_call_t;

class %(bname)s_base {
public:
	virtual ~%(bname)s_base() {}
private:
	static void cb(::ln::event_call& call, void* user_data) {
		%(bname)s_base* self = (%(bname)s_base*)user_data;
		%(bname)s_call_t ev;
		call.set_data(&ev, %(sn_no_t)s_call_signature);
		self->on_%(bname)s(ev);
	}
protected:
	::ln::event_connection* connection;
	void connect_to_%(bname)s(::ln::client* clnt, const std::string event_name, %(bname)s_connect_t* connect_data) {
		connection = clnt->connect_to_event(
			event_name, %(sn_no_t)s_connect_signature, connect_data,
			&cb, this);
	}
	virtual void on_%(bname)s(%(bname)s_call_t& ev) {
		fprintf(stderr, "ERROR: no virtual int on_%(name)s() handler overloaded for event %(name)s!\\n");
	}
};
%(ns_closing)s
#ifdef %(md_prefix_ns_define)s
}
#endif
#endif // __cplusplus
