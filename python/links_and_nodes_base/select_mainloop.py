import time
import select
import traceback

from links_and_nodes_base.mainloop import MainloopInterface

all = [ "SelectMainloop" ]



class SelectMainloop(MainloopInterface):
    """
    using a select.select()
    """
    
    def __init__(self):
        MainloopInterface.__init__(self)

        self._timeouts = dict() # id -> (next deadline, period, (cb, cb-args))
        self._idles = dict() # id -> ((cb, cb-args), )
        self._io_watches = dict() # id -> (fd, conditions-tuple, (cb, cb-args))
        
        self._next_source_id = 1

    def _find_next_deadline(self):
        # naiive
        next_deadline = None
        for sid, to in self._timeouts.items():
            if next_deadline is None or to[0] < next_deadline:
                next_deadline = to[0]
        return next_deadline
        
    def _do_call(self, to_call, all_dict, hint):
        if not to_call:
            return False
        for sid in to_call:
            to = all_dict.get(sid)
            if to is None:
                # already removed
                continue
            cb, args = to[-1]
            try:
                ret = cb(*args)
            except Exception:
                print("exception in %s %r callback %r:\n%s" % (hint, sid, cb, traceback.format_exc()))
                ret = False
            if not ret:
                del all_dict[sid]
            elif hint == "timeout":
                to[0] += to[1]
        return True
    
    def iterate(self, blocking=True):
        #print("\niterate, blocking: %r" % blocking)
        if blocking:
            next_deadline = self._find_next_deadline()
            if next_deadline is None:
                timeout = None # blocking
            else:
                timeout = next_deadline - time.time()
                if timeout < 0:
                    timeout = 0.
        else:
            timeout = 0.

        # naiive
        rfd = []
        wfd = []
        efd = []
        for sid, (fd, conditions, cb) in self._io_watches.items():
            if "in" in conditions: rfd.append(fd)
            if "out" in conditions: wfd.append(fd)
            if "err" in conditions: efd.append(fd)

        #print("select %r, %r, %r, %r" % (rfd, wfd, efd, timeout))
        rfd, wfd, efd = select.select(rfd, wfd, efd, timeout)
        #if not rfd and not wfd and not efd:
        #    print(" -> select had timeout")
        #else:
        #    print(" -> rfd: %r, wfd: %r, efd: %r" % (rfd, wfd, efd))
        
        now = time.time()
        
        # any timeouts?
        to_call = []
        for sid, to in self._timeouts.items():
            if to[0] <= now:
                #print("sid %r has timeout since %r" % (sid, now - to[0]))
                to_call.append(sid)
        had_some = self._do_call(to_call, self._timeouts, "timeout")

        # any io's?
        to_call = []
        for sid, (fd, conditions, cb) in self._io_watches.items():
            if fd in rfd and "in" in conditions:
                #print("sid %r is readable" % sid)
                to_call.append(sid)
            elif fd in wfd and "out" in conditions:
                #print("sid %r is writable" % sid)
                to_call.append(sid)
            elif fd in efd and "err" in conditions:
                #print("sid %r has exc" % sid)
                to_call.append(sid)
        had_some = had_some or self._do_call(to_call, self._io_watches, "io-watch")

        if not had_some:
            # do idle's
            self._do_call(self._idles.keys(), self._idles, "idle")

    def timeout_add(self, timeout_in_seconds, callback, *args):
        sid = self._next_source_id
        self._next_source_id += 1
        self._timeouts[sid] = [ time.time() + timeout_in_seconds, timeout_in_seconds, (callback, args) ]
        return sid

    def idle_add(self, callback, *args):
        sid = self._next_source_id
        self._next_source_id += 1
        self._idles[sid] = ((callback, args), )
        return sid

    def fd_add(self, fd, conditions, callback, *args):
        sid = self._next_source_id
        self._next_source_id += 1        
        if conditions in ("in", "out", "err"):
            conditions = (conditions, )
        self._io_watches[sid] = (fd, conditions, (callback, args))
        #print("sid %d is fd %r for %r:\n%s" % (sid, fd, conditions, "".join(traceback.format_stack())))
        return sid
    
    def source_remove(self, source_id):
        for all_dict in (self._timeouts, self._idles, self._io_watches):
            if source_id in all_dict:
                del all_dict[source_id]
                return
