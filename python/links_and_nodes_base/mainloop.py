all = [ "MainloopInterface" ]


class MainloopInterface(object):
    def run(self):
        """
        run the mainloop, until quit() is called
        """
        self._keep_running = True
        while self._keep_running:
            self.iterate()

    def quit(self):
        """
        stop all run() methods
        """
        self._keep_running = False
    
    def iterate(self, blocking=True):
        """
        do a single mainloop iteration.
        if blocking is True, wait until there was atleast one event to process
        """
        raise NotImplementedError()
    
    def sleep(self, t):
        keep_waiting = [True]
        def sleep_done():
            keep_waiting[0] = False
            return False
        self.timeout_add(t, sleep_done)
        while keep_waiting[0]:
            self.iterate()

    def timeout_add(self, timeout_in_seconds, callback, *args):
        """
        register a callback to be called every given number of seconds.
        callback has to have signature cb(*args)

        returns a unique source_id
        """
        raise NotImplementedError()
    
    def idle_add(self, callback, *args):
        """
        register an idle-callback to be called whenever mainloop has no other events to process
        callback has to have signature cb(*args)

        returns a unique source_id
        """
        raise NotImplementedError()
    
    def fd_add(self, fd, condition, callback, *args):
        """
        call callback if condition on give file-descriptor is met.
        condition can be "out", "in", "err" or sequence of them.
        callback has to have signature cb(*args)

        returns a unique source_id
        """
        raise NotImplementedError()
    
    def source_remove(self, source_id):
        """
        remove a unique source_id (can be from idle_add(), fd_add() or timeout_add()
        """
        raise NotImplementedError()
