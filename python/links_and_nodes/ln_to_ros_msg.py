"""
    Copyright 2013-2015 DLR e.V., Peter Lehner, Florian Schmidt

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import print_function

import os
import sys
import getopt
import re

import warnings
warnings.warn("ln_to_ros_msg.py is obsolete and will be removed in version 3.X")

specifier_map = {
    "uint8_t": "uint8",
    "int8_t": "int8",
    "uint16_t": "uint16",
    "int16_t": "int16",
    "uint32_t": "uint32",
    "int32_t": "int32",
    "uint64_t": "uint64",
    "int64_t": "int64",
    "float": "float32",
    "double": "float64",
    "char": "string"
}

define_regex = r"define \S* as \S*"

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def package_exists(ros_package_dir):
    return os.path.exists(ros_package_dir)        

def is_comment(line):
    stripped = line.strip()
    return stripped[0][0] == '#'

def is_variable_definition(line, defines):
    specifier = line.split()[0]
    specifier = specifier.strip('*')
    return (specifier in specifier_map) or (specifier in defines)

def convert_variable_definition(line, defines):
    splitted = line.split()
    specifier = splitted[0]
    variable_name = splitted[1]

    specifier = specifier.strip('*')

    ros_specifier = specifier_map.get(specifier)
    if(ros_specifier is None):
        ros_specifier = defines.get(specifier)
    if(ros_specifier is None):
        raise Exception("Unknown specifier in convert_variable_definition")

    array = ''
    array_open_index = variable_name.find('[')
    if array_open_index >= 0:
        array_close_index = variable_name.find(']', array_open_index)
        array = variable_name[array_open_index:array_close_index+1]
        variable_name = variable_name[0:array_open_index]

    if line.find('char') < 0  and line.find('*'):
        array = '[]'
    

    comment = ''
    comment_index = line.find('#')
    if(comment_index >= 0):
        comment = line[comment_index:]

    ros_line = ros_specifier + array + ' ' + variable_name + comment + os.linesep

    return ros_line

def remove_quotes(string):
    if string.startswith('"') and string.endswith('"'):
        return string[1:-1]

def parse_define(defines, line):
    splitted = line.split()
    type = splitted[1]
    name = remove_quotes(splitted[3])
#    print name
#    print type
    defines[type] = name

def is_define_statement(line):
    return re.match(define_regex, line) is not None

def is_service_definition(lines):
    for line in lines:
        if is_comment(line):
          continue
        
        splitted = line.split()
        if splitted[0] == 'service':
            return True

        return False
        
def is_message_definition(lines):
    for line in lines:
        if not line.strip() or is_comment(line) or is_define_statement(line):
          continue
        splitted = line.split()
        specifier = splitted[0].strip('*')
        if specifier in specifier_map:
            return True

        return False

def is_response_keyword(line):
    return line.find('response') >= 0

def convert_message(lines):
    ros_message = ''
    defines = dict()
    for line in lines:
        ros_line = ''
        if not line.strip() or is_comment(line):
            ros_line = line
        elif is_variable_definition(line, defines):
            ros_line = convert_variable_definition(line, defines)
        elif is_define_statement(line):
            parse_define(defines, line)
        ros_message = ros_message + ros_line
    return ros_message

def convert_service(lines):
    ros_message = ''
    defines = dict()
    for line in lines:
        ros_line = ''
        if not line.strip() or is_comment(line):
            ros_line = line
        elif is_response_keyword(line):
            ros_line = '---' + os.linesep
        elif is_variable_definition(line, defines):
#            print defines
            ros_line = convert_variable_definition(line, defines)
        elif is_define_statement(line):
            parse_define(defines, line)
#            print defines
        ros_message = ros_message + ros_line
    return ros_message

def convert(ln_message_dir, ros_package_dir):
    if not package_exists(ros_package_dir):
        msg = "ros package does not exist. Use roscreate-pkg first."
        raise Usage(msg)
    msg_path = os.path.join(ros_package_dir, 'msg')
    if not os.path.exists(msg_path):
        os.mkdir(msg_path)
    srv_path = os.path.join(ros_package_dir, 'srv')
    if not os.path.exists(srv_path):
        os.mkdir(srv_path)
    
    if not os.path.exists(ln_message_dir):
        msg = "links_and_nodes message directory does not exist."
        raise Usage(msg)

    #todo currently gets every entry from the directory. But cannot really distinguish message definitions as they have no extension.
    ln_message_files = [f for f in os.listdir(ln_message_dir)] 
    
    for message_file_name in ln_message_files:
        file_path =  os.path.join(ln_message_dir, message_file_name)

        file = open(file_path)
        lines = file.readlines()
        file.close()

        if is_message_definition(lines):
            ros_message = convert_message(lines)
            ros_file_name = message_file_name + ".msg"
            ros_file_path = os.path.join(msg_path, ros_file_name)
#            print ros_file_name 
            print(ros_file_path)
#            print ros_message
            ros_file = open(ros_file_path, 'w')
            ros_file.write(ros_message)
            ros_file.close()
        elif is_service_definition(lines):
            ros_service = convert_service(lines)
            ros_file_name = message_file_name + ".srv"
            ros_file_path = os.path.join(srv_path, ros_file_name)
            print(ros_file_path)
#            print ros_service
            ros_file = open(ros_file_path, 'w')
            ros_file.write(ros_service)
            ros_file.close()


def usage():
    print("Converts links_and_nodes messages inside a specified directory to robot operating system messages inside a specified package directory. Usage:")
    print("ln_to_ros_msg.py ln_message_directory ros_package_directory")

def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        try:
            opt, args = getopt.getopt(argv[1:], "h", ["help"])
        except getopt.error as msg:
            raise Usage(msg)
        for o, _ in opt:
            if o in ("-h", "--help"):
                usage()
        if(len(args) != 2):
            msg = "Insufficient arguments"
            raise Usage(msg)
        
        ln_message_dir = args[0]
        ros_package_dir = args[1]
    
        convert(ln_message_dir, ros_package_dir)
    
    except Usage as err:
        print(err.msg, file=sys.stderr)
        print("for help use --help", file=sys.stderr)
        return 2

    

if __name__ == "__main__":
    sys.exit(main())
