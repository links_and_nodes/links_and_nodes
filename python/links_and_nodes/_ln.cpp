/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <ln/ln.h>

#ifdef _POSIX_C_SOURCE
#undef _POSIX_C_SOURCE
#endif
#ifdef _XOPEN_SOURCE
#undef _XOPEN_SOURCE
#endif

#include <boost/python.hpp>
#include <boost/python/raw_function.hpp>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

#include <stdio.h>
#include <sys/uio.h> // struct iovec

#if PY_MAJOR_VERSION >= 3
#define IS_PY3K
#endif

#ifndef PyLong_AS_LONG // py3.6?
#  define PyLong_AS_LONG(obj) PyLong_AsLong(obj)
#endif


using namespace boost::python;

#define ln_module import("links_and_nodes.ln_wrappers")

static std::string asciistr(PyObject* bytes_or_str)
{ // py3 will give str - PyUnicode, py2 will give bytes
	if(PyUnicode_Check(bytes_or_str)) {
		PyObject* bytes = PyUnicode_AsASCIIString(bytes_or_str);
		if(!bytes)
			throw std::runtime_error("could not ASCII encode!");
		std::string ret(PyBytes_AS_STRING(bytes), PyBytes_GET_SIZE(bytes));
		Py_DECREF(bytes);
		return ret;
	}
	// either PyBytes or PyString
	std::string ret(PyBytes_AS_STRING(bytes_or_str), PyBytes_GET_SIZE(bytes_or_str));
	return ret;
}

static std::string strrepr(PyObject* obj)
{
	PyObject* repr = PyObject_Repr(obj);
	std::string ret = asciistr(repr);
	Py_DECREF(repr);
	return ret;
}

static std::string strrepr(object obj)
{
	return strrepr(obj.ptr());
}

static std::string utf8str(PyObject* bytes_or_str)
{ // py3 will give str - PyUnicode, py2 will give bytes
	if(PyUnicode_Check(bytes_or_str)) {
		PyObject* bytes = PyUnicode_AsEncodedString(bytes_or_str, "utf-8", "strict");
		if(!bytes)
			throw std::runtime_error("could not utf-8 encode!");
		std::string ret(PyBytes_AS_STRING(bytes), PyBytes_GET_SIZE(bytes));
		Py_DECREF(bytes);
		return ret;
	}
	// either PyBytes or PyString, assume it is already correctly encoded
	if(!PyBytes_Check(bytes_or_str)) {
		std::string msg = "expect str or bytes, not: ";
		msg += strrepr(bytes_or_str);
		throw std::runtime_error(msg);
	}
	std::string ret(PyBytes_AS_STRING(bytes_or_str), PyBytes_GET_SIZE(bytes_or_str));
	return ret;
}

static std::string utf8str(object bytes_or_str)
{
	return utf8str(bytes_or_str.ptr());
}

class GILRelease {
private:
	PyThreadState* m_thread_state;
public:
	inline GILRelease() {
		m_thread_state = PyEval_SaveThread();
	}
	inline ~GILRelease() {
		PyEval_RestoreThread(m_thread_state);
	}
};

// stolen from: https://stackoverflow.com/a/16109416
struct pystring_converter
{

	/// @note Registers converter from a python interable type to the provided type.
	template <typename Container>
	pystring_converter& from_python() {
		boost::python::converter::registry::push_back(
			&pystring_converter::convertible,
			&pystring_converter::construct<Container>,
			boost::python::type_id<Container>());
		return *this;
	}

	/// Check if PyObject is a string-like
	static void* convertible(PyObject* object) {
		if(PyUnicode_Check(object)
		   || PyBytes_Check(object))
			return object;
		return NULL;
	}

	/// @brief Convert PyString to Container.
	///
	/// Container Concept requirements:
	///
	///   * Container::value_type is CopyConstructable from char.
	///   * Container can be constructed and populated with two iterators.
	///     I.e. Container(begin, end)
	template <typename Container>
	static void construct(PyObject* object,
			      boost::python::converter::rvalue_from_python_stage1_data* data) {
		namespace python = boost::python;
		// Object is a borrowed reference, so create a handle indicting it is
		// borrowed for proper reference counting.
		python::handle<> handle(python::borrowed(object));

		// Obtain a handle to the memory block that the converter has allocated
		// for the C++ type.
		typedef python::converter::rvalue_from_python_storage<Container> storage_type;
		void* storage = reinterpret_cast<storage_type*>(data)->storage.bytes;

		// Allocate the C++ type into the converter's memory block, and assign
		// its handle to the converter's convertible variable.  The C++
		// container is populated by passing the begin and end iterators of
		// the python object to the container's constructor.
		data->convertible = new (storage) Container(utf8str(object));
	}
};


class GILHolder {
private:
	PyGILState_STATE gstate;
	bool acquired;
public:
	GILHolder(bool acquire=true) {
		acquired = false;
		if(acquire)
			lock();
	}
	void lock() {
		if(acquired)
			return;
		gstate = PyGILState_Ensure();
		acquired = true;
	}
	void unlock() {
		if(!acquired)
			return;
		PyGILState_Release(gstate);
		acquired = false;
	}
	~GILHolder() {
		unlock();
	}
};


class byte_array_swapper {
	PyObject* holder;
	const char* buffer_name;
	void* data;
	unsigned int data_len;
	bool keep_locked;
	bool need_to_acquire_gil;

	PyObject* obj;
	PyByteArrayObject* buf;
	PyByteArrayObject org_buf;
	GILHolder gil;
	PyObject* lock_obj;
	
	bool _lock_buffer() {
		lock_obj = PyObject_GetAttrString(holder, "lock");
		if(!lock_obj) {
			PyErr_Format(PyExc_ValueError, "handler does not have lock attribute!");
			throw_error_already_set();
			if(need_to_acquire_gil) gil.unlock();
			return false;
		}
		PyObject* acquire_args = Py_BuildValue("()");
		PyObject* acquire_method = PyObject_GetAttrString(lock_obj, "acquire");
		PyObject* ret = PyObject_Call(acquire_method, acquire_args, NULL);
		Py_DECREF(acquire_method);
		Py_DECREF(acquire_args);
		if(!ret) {
			throw_error_already_set();
			if(need_to_acquire_gil) gil.unlock();
			return false;
		}
		Py_DECREF(ret);
		return true;
	}
	void _unlock_buffer() {
		PyObject* release_args = Py_BuildValue("()");
		PyObject* release_method = PyObject_GetAttrString(lock_obj, "release");
		PyObject* ret = PyObject_Call(release_method, release_args, NULL);
		Py_DECREF(release_method);
		Py_DECREF(release_args);
		if(!ret)
			throw_error_already_set();
		else
			Py_DECREF(ret);
	}
public:
	byte_array_swapper(
		PyObject* holder,
		const char* buffer_name,
		void* data,
		unsigned int data_len,
		bool keep_locked=true,
		bool need_to_acquire_gil=true
	)
		:
		holder(holder), buffer_name(buffer_name),
		data(data), data_len(data_len),
		keep_locked(keep_locked),
		need_to_acquire_gil(need_to_acquire_gil),
		gil(false), lock_obj(NULL) {

	}
	~byte_array_swapper() {
		if(need_to_acquire_gil)
			gil.unlock();
	}


	bool lock() {
		if(!holder) {
			PyErr_SetString(PyExc_ValueError, "handler object does not exist!");
			throw_error_already_set();
			return false;
		}
		if(need_to_acquire_gil) gil.lock();
		
		// acquire holder.lock
		if(!_lock_buffer())
			return false;
		
		obj = PyObject_GetAttrString(holder, buffer_name);
		if(!obj) {
			PyErr_Format(PyExc_ValueError, "handler does not have buffer attribute named '%s'!", buffer_name);
			throw_error_already_set();
			_unlock_buffer();
			if(need_to_acquire_gil) gil.unlock();
			return false;
		}
		if(!PyByteArray_CheckExact(obj)) {
			Py_DECREF(obj);
			PyErr_Format(PyExc_ValueError, "provided buffer attribute %s is not a bytearray-object!", buffer_name);
			throw_error_already_set();
			_unlock_buffer();
			if(need_to_acquire_gil) gil.unlock();
			return false;
		}
		buf = (PyByteArrayObject*)obj;

		org_buf = *buf;
		buf->ob_bytes = (char*)data;
#ifdef IS_PY3K
		buf->ob_start = buf->ob_bytes;
#endif
		buf->ob_alloc = data_len;
		((PyVarObject*)buf)->ob_size = data_len;
		
		if(need_to_acquire_gil && !keep_locked)
			gil.unlock();

		return true; // success
	}

	void unlock() {
		*buf = org_buf;
		Py_DECREF(obj);
		
		// release holder.lock
		_unlock_buffer();
		
		if(need_to_acquire_gil && keep_locked)
			gil.unlock();
	}
};


/*
  non-reference counting pointer wrapper
  (reference counting is done internal to ln-api)
*/
template <class T>
struct my_ptr {
	typedef T element_type;
	T* p;

	my_ptr(T* p) : p(p) {};

	T& operator*() { return *p; };
};
template <class T>
T* get_pointer(const my_ptr<T>& ptr) {
	return ptr.p;
}


/*
  pointer to T which belongs to instance of type boost::shared_ptr<hold_T>
*/
template <class T, class hold_T>
struct my_hold_ptr {
	typedef T element_type;
	typedef boost::shared_ptr<hold_T> holder_t;
	T* p;
	holder_t holder;

	my_hold_ptr(T* p, holder_t holder) : p(p), holder(holder) {};

	T& operator*() { return *p; };
};
template <class T, class hold_T>
T* get_pointer(const my_hold_ptr<T, hold_T>& ptr) {
	return ptr.p;
}

void translator(ln::exception const& e) {
	PyErr_SetString(PyExc_Exception, e.what());
}

boost::shared_ptr<ln::client> init_client(std::string client_name, object ln_manager) {
	if(ln_manager.ptr() == object().ptr())
		return boost::shared_ptr<ln::client>(new ln::client(client_name, 0, NULL));
	if(ln_manager.attr("__class__").attr("__name__") == "list") {
		boost::python::list args = (boost::python::list)ln_manager;
		char** argv = new char*[len(args)];
		for(int i = 0; i < len(args); i++)
			argv[i] = extract<char*>(args[i]);
		boost::shared_ptr<ln::client> ret(new ln::client(client_name, len(args), argv));
		delete[] argv;
		return ret;
	}
	return boost::shared_ptr<ln::client>(new ln::client(client_name, utf8str(ln_manager)));
}

boost::shared_ptr<ln::client> init_client2(std::string client_name) {
	return boost::shared_ptr<ln::client>(new ln::client(client_name, 0, NULL));
}
boost::shared_ptr<ln::client> construct_client_from_pointer(uintptr_t ln_client_ptr) {
	return boost::shared_ptr<ln::client>((ln::client*)ln_client_ptr);
}

object subscribe(boost::shared_ptr<ln::client> clnt, const std::string& topic_name, object message_definition_name=object(), double rate=-1, unsigned int buffers=1, bool need_reliable_transport=false) {
	std::string md_name_in;
	std::string md_name;
	std::string md;
	unsigned int msg_size;
	std::string hash;

	if(message_definition_name.ptr() == object().ptr()) // None
		md_name_in = "";
	else {
		// make sure it is a string
		if(PyUnicode_Check(message_definition_name.ptr()))
			md_name_in = utf8str(message_definition_name);
#if !defined(IS_PY3K)
		else if(PyString_Check(message_definition_name.ptr())) // assume it is already utf8-encoded!
			md_name_in = PyString_AsString(message_definition_name.ptr());
#endif
		else {
#if defined(IS_PY3K)
			PyErr_Format(PyExc_TypeError, "subscribe: 2nd argument `message_definition_name` must be either None or a str! (not %s)",
				     strrepr(message_definition_name).c_str());
#else
			PyErr_Format(PyExc_TypeError, "subscribe: 2nd argument `message_definition_name` must be either None, unicode or a str (utf-8 encoded)! (not %s)",
				     strrepr(message_definition_name).c_str());
#endif
			throw_error_already_set();
			return object();
		}
	}

	if(md_name_in.size() == 0)
		clnt->get_message_definition_for_topic(topic_name, md_name, md, msg_size, hash);
	else {
		md_name = md_name_in;
		clnt->get_message_definition(md_name, md, msg_size, hash);
	}
	ln::inport* port = clnt->subscribe(topic_name, md_name, rate, buffers, need_reliable_transport);
	return ln_module.attr("port_wrapper")(md_name, md, msg_size, hash, my_ptr<ln::inport>(port), clnt, topic_name, need_reliable_transport);
}
BOOST_PYTHON_FUNCTION_OVERLOADS(subscribe_overloads, subscribe, 2, 6)

void unsubscribe(boost::shared_ptr<ln::client> clnt, object inport) {
	clnt->unsubscribe(extract<ln::inport*>(inport.attr("port")));
	inport.attr("port") = 0;
}

list ln_client_get_remaining_args(boost::shared_ptr<ln::client> clnt) {
	std::vector<std::string> cl = clnt->get_remaining_args();
	list ret;
	for(std::vector<std::string>::iterator i = cl.begin(); i != cl.end(); ++i) {
		ret.append(*i);
	}
	return ret;
}


tuple get_message_definition(boost::shared_ptr<ln::client> clnt, const std::string& message_definition_name) {
	std::string md;
	unsigned int msg_size;
	std::string hash;
	std::string flat_md;
	clnt->get_message_definition(message_definition_name.c_str(), md, msg_size, hash, flat_md);
	return boost::python::make_tuple(md, msg_size, hash, flat_md);
}

tuple get_message_definition_for_topic(boost::shared_ptr<ln::client> clnt, const std::string& topic_name) {
	std::string md_name;
	std::string md;
	unsigned int msg_size;
	std::string hash;
	clnt->get_message_definition_for_topic(topic_name, md_name, md, msg_size, hash);
	return boost::python::make_tuple(md_name, md, msg_size, hash);
}

tuple get_service_signature_for_service(boost::shared_ptr<ln::client> clnt, const std::string& service_name) {
	std::string service_interface;
	std::string service_definition;
	std::string service_signature;
	clnt->get_service_signature_for_service(service_name, service_interface, service_definition, service_signature);
	return boost::python::make_tuple(service_interface, service_definition, service_signature);
}

tuple get_service_signature(boost::shared_ptr<ln::client> clnt, const std::string& service_interface) {
	std::string service_definition;
	std::string service_signature;
	clnt->get_service_signature(service_interface, service_definition, service_signature);
	return boost::python::make_tuple(service_definition, service_signature);
}

object publish(boost::shared_ptr<ln::client> clnt, const std::string& topic_name, const std::string& message_definition_name, unsigned int buffers) {
	std::string md;
	unsigned int msg_size;
	std::string hash;
	clnt->get_message_definition(message_definition_name, md, msg_size, hash);

	ln::outport* port = clnt->publish(topic_name, message_definition_name, buffers);
	return ln_module.attr("port_wrapper")(message_definition_name, md, msg_size, hash, my_ptr<ln::outport>(port), clnt, topic_name);
}
BOOST_PYTHON_FUNCTION_OVERLOADS(publish_overloads, publish, 2, 3)
void unpublish(boost::shared_ptr<ln::client> clnt, object outport) {
	clnt->unpublish(extract<ln::outport*>(outport.attr("port")));
	outport.attr("port") = 0;
}

bool ln_inport_read(ln::inport& port, object data, object blocking_or_timeout) {
	PyObject* buffer_object = data.ptr();
	if(!PyByteArray_Check(buffer_object)) {
		PyErr_SetString(PyExc_ValueError, "provided data is not a bytearray-object!");
		throw_error_already_set();
		return false;
	}
	char* buffer = ((PyByteArrayObject *)buffer_object)->ob_bytes;

	PyObject* blocking_or_timeout_ob = blocking_or_timeout.ptr();
	if(PyBool_Check(blocking_or_timeout_ob)) {
		bool blocking = blocking_or_timeout_ob == Py_True;
		if(blocking) {
			GILRelease unlockGIL;
			return port.read(buffer, blocking);
		}
		return port.read(buffer, blocking);
	}
	double timeout = PyFloat_AsDouble(blocking_or_timeout_ob);
	if(timeout > 0) {
		GILRelease unlockGIL;
		return port.read(buffer, timeout);
	}
	return port.read(buffer, timeout);
}

void ln_outport_write(ln::outport& port, object data) {
	PyObject* buffer_object = data.ptr();
	if(!PyByteArray_Check(buffer_object)) {
		PyErr_SetString(PyExc_ValueError, "provided data is not a bytearray-object!");
		throw_error_already_set();
		return;
	}
	char* buffer = ((PyByteArrayObject *)buffer_object)->ob_bytes;   
	port.write(buffer);
}
void ln_outport_write_ts(ln::outport& port, object data, double timestamp) {
	PyObject* buffer_object = data.ptr();
	if(!PyByteArray_Check(buffer_object)) {
		PyErr_SetString(PyExc_ValueError, "provided data is not a bytearray-object!");
		throw_error_already_set();
		return;
	}
	char* buffer = ((PyByteArrayObject *)buffer_object)->ob_bytes;   
	port.write(buffer, timestamp);
}

void my_copy2array(object target_array, object source_buffer, unsigned int offset, unsigned int size) {
	// todo: test
	PyObject* array_object = target_array.ptr();
	if(!PyArray_Check(array_object)) {
		PyErr_SetString(PyExc_ValueError, "provided array is not a numpy-array-object!");
		throw_error_already_set();
		return;
	}
	void* array = PyArray_DATA((PyArrayObject*)array_object);

	PyObject* buffer_object = source_buffer.ptr();
	char* buffer = NULL;

	if(PyByteArray_Check(buffer_object)) {
		buffer = ((PyByteArrayObject *)buffer_object)->ob_bytes;
		memcpy(array, buffer + offset, size);
		return;
	}

	if(PyObject_CheckBuffer(buffer_object)) {
		Py_buffer view;
		if(PyObject_GetBuffer(buffer_object, &view, PyBUF_SIMPLE) == -1) {
			PyErr_SetString(PyExc_ValueError, "provided object's buffer interface can not provide PyBUF_SIMPLE!");
			throw_error_already_set();
			return;
		}
		memcpy(array, buffer + offset, size);
		PyBuffer_Release(&view);
		return;

	}

	PyErr_SetString(PyExc_ValueError, "provided data is not a bytearray-object and not a buffer-object!");
	throw_error_already_set();
}

void my_copy2buffer(object target_buffer, unsigned int offset, object source_array, unsigned int size) {
	PyObject* buffer_object = target_buffer.ptr();
	if(!PyByteArray_Check(buffer_object)) {
		PyErr_SetString(PyExc_ValueError, "provided data is not a bytearray-object!");
		throw_error_already_set();
		return;
	}
	char* buffer = ((PyByteArrayObject *)buffer_object)->ob_bytes;   

	PyObject* array_object = source_array.ptr();
	if(!PyArray_Check(array_object)) {
		PyErr_SetString(PyExc_ValueError, "provided array is not a numpy-array-object!");
		throw_error_already_set();
		return;
	}
	PyArrayObject* array_object_cont = PyArray_GETCONTIGUOUS((PyArrayObject*)array_object);
	void* array = PyArray_DATA((PyArrayObject*)array_object_cont);	
	memcpy(buffer + offset, array, size);
	Py_DECREF(array_object_cont);
}


my_hold_ptr<ln::multi_waiter, ln::client> get_multi_waiter(boost::shared_ptr<ln::client> clnt) {
	return my_hold_ptr<ln::multi_waiter, ln::client>(clnt->get_multi_waiter(), clnt);
}

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(multi_waiter_wait_overloads, wait, 0, 1)

void multi_waiter_add_port(ln::multi_waiter& waiter, object inport) {
	waiter.add_port(extract<ln::inport*>(inport.attr("port")));
}
void multi_waiter_remove_port(ln::multi_waiter& waiter, object inport) {
	waiter.remove_port(extract<ln::inport*>(inport.attr("port")));
}
bool multi_waiter_can_read(ln::multi_waiter& waiter, object inport) {
	return waiter.can_read(extract<ln::inport*>(inport.attr("port")));
}

int multi_waiter_start_pipe_notifier_thread(ln::multi_waiter& waiter) {
	int fd;
	waiter.start_pipe_notifier_thread(&fd);
	return fd;
}

object get_parameters(boost::shared_ptr<ln::client> clnt, std::string parameters_name="", bool allow_empty=false) {
	return ln_module.attr("parameters_proxy")(clnt, parameters_name, allow_empty);
}

my_hold_ptr<ln::logger, ln::client> get_logger(boost::shared_ptr<ln::client> clnt, const std::string& logger_name) {
	ln::logger* l = clnt->get_logger(logger_name);
	return my_hold_ptr<ln::logger, ln::client>(l, clnt);
}

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(ln_logger_manager_save_overloads, ln::logger::manager_save, 1, 2)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(ln_logger_add_topic_overloads,    ln::logger::add_topic, 2, 3)

object _init_logger_data(ln::logger_data* data) {
	object ld = ln_module.attr("logger_data_wrapper")(my_ptr<ln::logger_data>(data));

	boost::python::dict topics;
	for(ln::logger_data::topic_iterator i = data->topics.begin(); i != data->topics.end(); ++i) {
		ln::logger_data::topic_cursor* c = *i;

		boost::python::str topic_name(c->name);
		topics[topic_name] = ln_module.attr("topic_cursor_wrapper")(my_ptr<ln::logger_data::topic_cursor>(c));
	}
	ld.attr("topics") = topics;
	
	return ld;
}

object _ln_logger_download(boost::shared_ptr<ln::logger> l) {
	return _init_logger_data(l->download());
}

object _ln_logger_direct_download(boost::shared_ptr<ln::logger> l) {
	return _init_logger_data(l->direct_download());
}

object _ln_logger_data_load(std::string filename) {
	return _init_logger_data(ln::logger_data::load(filename));
}

my_ptr<logger_topic_> _ln_logger_data_topic_cursor_get_topic(boost::shared_ptr<ln::logger_data::topic_cursor> c) {
	return my_ptr<logger_topic_>((struct logger_topic_*)c->topic);
}

void _ln_logger_data_topic_cursor_get_sample(boost::shared_ptr<ln::logger_data::topic_cursor> c, unsigned int idx, object target_buffer) {
	logger_sample* s = c->get_sample(idx);
	
	PyObject* buffer_object = target_buffer.ptr();
	if(!PyByteArray_Check(buffer_object)) {
		PyErr_SetString(PyExc_ValueError, "provided data is not a bytearray-object!");
		throw_error_already_set();
		return;
	}
	char* buffer = ((PyByteArrayObject *)buffer_object)->ob_bytes;   
	memcpy(buffer, s, c->topic->sample_size);
}

object logger_topic_get_md_name(boost::shared_ptr<logger_topic_> t) {
	return boost::python::str(std::string(t->md_name));
}
object logger_topic_get_md(boost::shared_ptr<logger_topic_> t) {
	return boost::python::str(std::string(t->md));
}

object get_service(boost::shared_ptr<ln::client> clnt, const std::string& name, std::string interface="", object mainloop=object()) {
	std::string definition;
	std::string signature;
	
	if(interface.size()) {
		clnt->get_service_signature(interface, definition, signature);
	} else {
		clnt->get_service_signature_for_service(name, interface, definition, signature);
	}
	
	ln::service* svc = clnt->get_service(name, interface, signature);

	dict kwargs;
	kwargs["name"] = name;
	kwargs["interface"] = interface;
	kwargs["definition"] = definition;
	kwargs["signature"] = signature;
	kwargs["svc"] = my_hold_ptr<ln::service, ln::client>(svc, clnt);  // as long as svc object is alive, we keep client alive!
	kwargs["clnt"] = clnt;
	kwargs["mainloop"] = mainloop;
	return ln_module.attr("service_wrapper")(
		*tuple(),
		**kwargs);
}

object get_service_provider(boost::shared_ptr<ln::client> clnt, const std::string& name, const std::string& interface, object mainloop=object()) {
	std::string definition;
	std::string signature;
	
	clnt->get_service_signature(interface, definition, signature);

	ln::service* svc = clnt->get_service_provider(name, interface, signature);
	
	dict kwargs;
	kwargs["name"] = name;
	kwargs["interface"] = interface;
	kwargs["definition"] = definition;
	kwargs["signature"] = signature;
	kwargs["svc"] = my_hold_ptr<ln::service, ln::client>(svc, clnt);
	kwargs["clnt"] = clnt;
	kwargs["use_numpy_arrays"] = true;
	kwargs["auto_cast"] = true;
	kwargs["is_provider"] = true;
	kwargs["mainloop"] = mainloop;
	return ln_module.attr("service_wrapper")(
		*tuple(),
		**kwargs);
}
BOOST_PYTHON_FUNCTION_OVERLOADS(get_service_provider_overloads, get_service_provider, 3, 4);

void release_service(boost::shared_ptr<ln::client> clnt, object svc_wrapper) {
	bool is_provider = svc_wrapper.attr("_is_provider");
	object svc = svc_wrapper.attr("svc");
	if(svc.is_none())
		return;
	extract<my_hold_ptr<ln::service, ln::client> > svc_hp(svc);
	if(!svc_hp.check())
		return;
	my_hold_ptr<ln::service, ln::client> hp = svc_hp;
	ln::service* service = get_pointer(hp);

	if(is_provider) {
		GILRelease unlockGIL;
		clnt->release_service(service);
	}
	
	svc_wrapper.attr("_cleanup")();

	if(!is_provider) {
		GILRelease unlockGIL;
		clnt->release_service(service);
	}
	
	svc_wrapper.attr("svc") = object();
	return;
}

/**
   fill_data_t
   given two bytearray python objects
   reads packet and fills a struct iovec* into one bytearray
   optionally writing casted values to the second bytearray

   returns struct iovec* and count of iovec-entries via get_iov()
 */
class fill_data_t {
	PyObject* cast_buffer_ob;
	unsigned int n_cast_buffer;

	PyObject* iovec_buffer_ob;
	unsigned int n_iovec_buffer;

	PyObject* iovec_element_lens_buffer_ob;

	ln_client clnt;	
public:
	bool debug;
	fill_data_t(ln_client clnt, PyObject* cast_buffer_ob, PyObject* iovec_buffer_ob, PyObject* iovec_element_lens_buffer_ob) :
		cast_buffer_ob(cast_buffer_ob), iovec_buffer_ob(iovec_buffer_ob), iovec_element_lens_buffer_ob(iovec_element_lens_buffer_ob), clnt(clnt) {

		debug = false;
		n_cast_buffer = 0;
		n_iovec_buffer = 0;
	}

	void print() {
		struct iovec* iov = (struct iovec*)(((PyByteArrayObject *)iovec_buffer_ob)->ob_bytes);
		if (debug) ln_debug(clnt, "%d iov entries\n", (int)(n_iovec_buffer / sizeof(struct iovec)));
		for(unsigned int i = 0; i < (n_iovec_buffer / sizeof(struct iovec)); i++) {
			if (debug) ln_debug(clnt, "iovec[%d]: base: %p, len: %d ", i, iov[i].iov_base, (int)iov[i].iov_len);
			unsigned char* data = (unsigned char*)iov[i].iov_base;
			if (debug) {
				for(unsigned int k = 0; k < iov[i].iov_len && k < 32; k++) {
					ln_debug(clnt, "%02x ", data[k]);
				}
				ln_debug(clnt, "\n");
			}
		}
	}

	struct iovec* get_iov(unsigned int& len) {
		len = n_iovec_buffer / sizeof(struct iovec);
		process_cast_buffer_queue();
		return (struct iovec*)(((PyByteArrayObject *)iovec_buffer_ob)->ob_bytes);
	}
	uint8_t* get_iov_element_lens() {
		
		return (uint8_t*)(((PyByteArrayObject *)iovec_element_lens_buffer_ob)->ob_bytes);
	}

	uint8_t* iov_element_len;
	struct iovec* get_next_iov() {
		unsigned int idx = n_iovec_buffer / sizeof(struct iovec);
		n_iovec_buffer += sizeof(struct iovec);
		if(n_iovec_buffer > (unsigned)PyByteArray_GET_SIZE(iovec_buffer_ob))  {
			if(PyByteArray_Resize(iovec_buffer_ob, n_iovec_buffer))
				return NULL;
			if(PyByteArray_Resize(iovec_element_lens_buffer_ob, idx + 1))
				return NULL;
		}
		struct iovec* iov = (struct iovec*)(((PyByteArrayObject *)iovec_buffer_ob)->ob_bytes + n_iovec_buffer - sizeof(struct iovec));
		iov_element_len =(uint8_t*)(((PyByteArrayObject *)iovec_element_lens_buffer_ob)->ob_bytes + idx);
		*iov_element_len = 0; // sentinel
		return iov;
	}

	void add_to_iov(void* data, unsigned int len, unsigned int element_len) {
		struct iovec* iov = get_next_iov();
		iov->iov_base = data;
		iov->iov_len = len;
		*iov_element_len = element_len;
	}

	struct _cast_buffer_queue_item {
		unsigned int iov_offset;
		unsigned int offset;
	};
	typedef std::list<_cast_buffer_queue_item> _cast_buffer_queue_t;
	_cast_buffer_queue_t _cast_buffer_queue;
	void add_to_cast_buffer_queue(struct iovec* iov, unsigned int offset) {
		struct iovec* iov_base = (struct iovec*)(((PyByteArrayObject *)iovec_buffer_ob)->ob_bytes);
		_cast_buffer_queue_item item = {(unsigned int)(iov - iov_base), offset};
		_cast_buffer_queue.push_back(item);
	}
	void process_cast_buffer_queue() {
		struct iovec* iov_base = (struct iovec*)(((PyByteArrayObject *)iovec_buffer_ob)->ob_bytes);
		char* cb_base = ((PyByteArrayObject *)cast_buffer_ob)->ob_bytes;
		for(_cast_buffer_queue_t::iterator i = _cast_buffer_queue.begin(); i != _cast_buffer_queue.end(); i++) {
			_cast_buffer_queue_item& item = *i;
			iov_base[item.iov_offset].iov_base = cb_base + item.offset;
		}
	}
	void add_to_cast_buffer_and_iov(const void* data, unsigned int len, unsigned int element_len) {
		n_cast_buffer += len;
		if(n_cast_buffer > (unsigned)PyByteArray_GET_SIZE(cast_buffer_ob))
			if(PyByteArray_Resize(cast_buffer_ob, n_cast_buffer))
				return; // error!
		char* cb_pos = ((PyByteArrayObject *)cast_buffer_ob)->ob_bytes + n_cast_buffer - len;
		struct iovec* iov = get_next_iov();
		*iov_element_len = element_len;
		add_to_cast_buffer_queue(iov, n_cast_buffer - len);
		// iov->iov_base = cb_pos; // bug!! cb_pos get invalid if resize to other mem region is needed!
		iov->iov_len = len;
		memcpy(cb_pos, data, len);
	}
	char* get_end_of_cast_buffer() {
		return ((PyByteArrayObject *)cast_buffer_ob)->ob_bytes + n_cast_buffer;
	}

	int fill_value(const char* data_type_name, PyObject* inner_field_value) {
		if(!strcmp(data_type_name, "float")) {
			float v = (float)PyFloat_AsDouble(inner_field_value);
			add_to_cast_buffer_and_iov(&v, sizeof(v), sizeof(v));
		} else if(!strcmp(data_type_name, "double")) {
			double v = PyFloat_AsDouble(inner_field_value);
			add_to_cast_buffer_and_iov(&v, sizeof(v), sizeof(v));
		} else if(!strcmp(data_type_name, "int8_t") || !strcmp(data_type_name, "uint8_t")) {
			char v = (char)PyLong_AsLong(inner_field_value);
			add_to_cast_buffer_and_iov(&v, sizeof(v), sizeof(v));
		} else if(!strcmp(data_type_name, "int16_t") || !strcmp(data_type_name, "uint16_t")) {
			int16_t v = (int16_t)PyLong_AsLong(inner_field_value);
			add_to_cast_buffer_and_iov(&v, sizeof(v), sizeof(v));
		} else if(!strcmp(data_type_name, "int32_t") || !strcmp(data_type_name, "uint32_t")) {
			int32_t v = (int32_t)PyLong_AsLong(inner_field_value);
			add_to_cast_buffer_and_iov(&v, sizeof(v), sizeof(v));
		} else if(!strcmp(data_type_name, "int64_t") || !strcmp(data_type_name, "uint64_t")) {
			int64_t v = (int64_t)PyLong_AsLongLong(inner_field_value);
			add_to_cast_buffer_and_iov(&v, sizeof(v), sizeof(v));
		} else {
			fprintf(stderr, "unknown data type '%s'\n", data_type_name);
			PyErr_SetString(PyExc_ValueError, "provided unknown data type!");
			return -1;
		}
		return 0;
	}
	unsigned int get_data_type_size(const char* data_type_name) {
		if(!strcmp(data_type_name, "float")) {
			return sizeof(float);
		} else if(!strcmp(data_type_name, "double")) {
			return sizeof(double);
		} else if(!strcmp(data_type_name, "int8_t") || !strcmp(data_type_name, "uint8_t")) {
			return 1;
		} else if(!strcmp(data_type_name, "int16_t") || !strcmp(data_type_name, "uint16_t")) {
			return sizeof(int16_t);
		} else if(!strcmp(data_type_name, "int32_t") || !strcmp(data_type_name, "uint32_t")) {
			return sizeof(int32_t);
		} else if(!strcmp(data_type_name, "int64_t") || !strcmp(data_type_name, "uint64_t")) {
			return sizeof(sizeof(int64_t));
		} else {
			fprintf(stderr, "unknown data type '%s'\n", data_type_name);
			PyErr_SetString(PyExc_ValueError, "provided unknown data type!");
		}
		return 0;
	}

	int fill(PyObject* packet_ob, PyObject* fields_ob, int indent=0) {
		unsigned int fields_len = PyList_GET_SIZE(fields_ob);

		char in[128];
		memset(in, ' ', 128);
		in[indent * 4] = 0;

		for(unsigned int i = 0; i < fields_len; i++) {
			PyObject* field = PyList_GET_ITEM(fields_ob, i); // type, name, count
			if (debug)
				ln_debug(clnt, "%s field: %s\n", in, strrepr(field).c_str());
			if(!PyList_Check(field)) {
				PyErr_Format(PyExc_ValueError, "field index %d is not of type list!\n", i);
				return -1;
			}
			if(PyList_Size(field) < 4) {
				PyErr_Format(PyExc_ValueError, "field index %d has only len of %d!: %s\n", i, (int)PyList_Size(field), strrepr(field).c_str());
				return -1;
			}
			PyObject* field_type_ob = PyList_GET_ITEM(field, 3);
			std::string field_type = asciistr(field_type_ob);
			if(!field_type.size()) {
				PyErr_Format(PyExc_ValueError, "field index %d's item index 3/field_type is not of type string: %s",
					     i, strrepr(field_type_ob).c_str());
				return -1;
			}
			PyObject* field_name_ob = PyList_GET_ITEM(field, 1);
			std::string field_name = utf8str(field_name_ob);
			if(!field_name.size()) {
				PyErr_Format(PyExc_ValueError, "field index %d's item index 1/field_name is not of type string: %s",
					     i, strrepr(field_name_ob).c_str());
				return -1;
			}
			
			if (debug) ln_debug(clnt, "%s field '%s', type: '%s'\n", in, field_name.c_str(), field_type.c_str());
			PyObject* field_value = NULL;
			if(field_type == "pyobject") { // todo: test
				std::string pyfield_name = field_name + "_packet";
				field_value = PyObject_GetAttrString(packet_ob, pyfield_name.c_str());
				if(!field_value) {
					PyErr_Format(PyExc_ValueError, "field index %d - pyobject object has no attribute '%s'\n", i, pyfield_name.c_str());
					return -1;
				}
			} else {
				field_value = PyObject_GetAttrString(packet_ob, field_name.c_str());
				if(!field_value) {
					PyErr_Format(PyExc_ValueError, "field index %d - object has no attribute '%s'\n", i, field_name.c_str());
					return -1;
				}
			}

			unsigned int field_count = PyLong_AS_LONG(PyList_GET_ITEM(field, 2));
			bool is_pointer = PyLong_AS_LONG(PyList_GET_ITEM(field, 4));
			bool is_primitive = PyLong_AS_LONG(PyList_GET_ITEM(field, 5));

			if (debug) ln_debug(clnt, "%s   count %d, is_pointer %d, is_primitive %d, value: %p\n", in, field_count, is_pointer, is_primitive, field_value);


			std::string data_type_name;
			if(is_primitive)
				data_type_name = asciistr(PyList_GET_ITEM(field, 6));

			if(is_pointer && !(is_primitive && data_type_name == "char")) {
				std::string len_field_name = field_name + "_len";
				field_count = extract<unsigned int>(object(handle<>(borrowed(packet_ob))).attr(len_field_name.c_str()));
				if (debug) ln_debug(clnt, "%s   field has length %d\n", in, field_count);
			}

			if(is_primitive) {
				if(data_type_name == "char") {
					bool have_str = true;
					std::string utf8_string;
					if(PyUnicode_Check(field_value))
						utf8_string = utf8str(field_value);
#if !defined(IS_PY3K)
					else if(PyString_Check(field_value)) // assume it is already utf8-encoded!
						utf8_string = PyString_AsString(field_value);
#endif
					else if(PyBytes_Check(field_value)) // assume it is already utf8-encoded!
						utf8_string = std::string(PyBytes_AS_STRING(field_value), PyBytes_GET_SIZE(field_value));
					else {
						if (debug) ln_debug(clnt, "%s   field has char* type but non-String python object!\n", in);
						have_str = false;
					}
					if(have_str) {
						if(!is_pointer) {
							int padding = field_count - utf8_string.size();
							if(padding > 0)
								utf8_string += std::string(padding, '\0');
							else
								utf8_string = utf8_string.substr(0, field_count);
						} else {
							// fix _len-field value!
							uint32_t* ptr_to_len = ((uint32_t*)get_end_of_cast_buffer()) - 1;
							*ptr_to_len = utf8_string.size();
						}
						add_to_cast_buffer_and_iov(utf8_string.c_str(), utf8_string.size(), 1);
						Py_DECREF(field_value);
						continue;
					}
				}
				if(!is_pointer && field_count == 1) {
					if (debug) ln_debug(clnt, "%s   primitive non pointer of type %s\n", in, data_type_name.c_str());
					if(fill_value(data_type_name.c_str(), field_value))
						return -1;
					Py_DECREF(field_value);
					continue;
				}
				// exect numpy array to read from!
				// todo: accept lists!
				if(!PyArray_Check(field_value)) {
					PyErr_Format(PyExc_ValueError, "provided array for field '%s' is not a numpy-array-object!", field_name.c_str());
					Py_DECREF(field_value);
					return -1;
				}
				PyArrayObject* ccont = PyArray_GETCONTIGUOUS((PyArrayObject*)field_value);
				Py_DECREF(field_value);
				if(!ccont) {
					PyErr_SetString(PyExc_ValueError, "provided array is not a contiguous!");
					return -1;
				}
				add_to_iov(PyArray_DATA(ccont), PyArray_NBYTES(ccont), get_data_type_size(data_type_name.c_str()));
				Py_DECREF(ccont);
				continue;
			}
			// not a primitive type
			PyObject* inner_fields = PyList_GET_ITEM(field, 6);
			if(!is_pointer && field_count == 1) {
				// single value
				int ret = fill(field_value, inner_fields, indent + 1);
				Py_DECREF(field_value);
				if(ret)
					return ret;
				continue;
			}
			// either pointer or field_count > 1
			// assume field_value is a list!
			// assume all entries are of the same size!
			for(unsigned int k = 0; k < field_count; k++) {
				//printf("%s item[%d]\n", in, k);
				int ret = fill(PyList_GET_ITEM(field_value, k), inner_fields, indent + 1);
				if(ret) {
					Py_DECREF(field_value);
					return ret;
				}
			}
			Py_DECREF(field_value);
		}
		return 0;
	}

};


int _service_fill_only(object svc_wrapper, unsigned int* len, struct iovec** iov, uint8_t** iov_element_lens) {
	object cast_buffer_ = svc_wrapper.attr("_cast_buffer");
	if(!PyByteArray_Check(cast_buffer_.ptr())) {
		PyErr_SetString(PyExc_ValueError, "provided _cast_buffer is not a bytearray-object!");
		throw_error_already_set();
		return -1;
	}

	object iovec_buffer_ = svc_wrapper.attr("_iovec_buffer");
	if(!PyByteArray_Check(iovec_buffer_.ptr())) {
		PyErr_SetString(PyExc_ValueError, "provided _iovec_buffer is not a bytearray-object!");
		throw_error_already_set();
		return -1;
	}
	
	object iovec_element_lens_buffer_ = svc_wrapper.attr("_iovec_element_lens_buffer");
	if(!PyByteArray_Check(iovec_element_lens_buffer_.ptr())) {
		PyErr_SetString(PyExc_ValueError, "provided _iovec_element_lens_buffer is not a bytearray-object!");
		throw_error_already_set();
		return -1;
	}
	
	fill_data_t fill_data(
		NULL, // only needed for debug prints
		cast_buffer_.ptr(), 
		iovec_buffer_.ptr(),
		iovec_element_lens_buffer_.ptr());
	
	if(fill_data.fill(
		   ((object)svc_wrapper.attr("req")).ptr(),
		   ((object)svc_wrapper.attr("_req_fields")).ptr())
		) {
		throw_error_already_set();
		return -1;
	}
	*iov = fill_data.get_iov(*len);
	if(iov_element_lens)
		*iov_element_lens = fill_data.get_iov_element_lens();
	return 0;
}

object service_fill_only(object svc_wrapper) {
	unsigned int len;
	struct iovec* iov;
	if(_service_fill_only(svc_wrapper, &len, &iov, NULL))
		return object();

	std::stringstream ss;
	unsigned int n_bytes = 0;
	for(unsigned int i = 0; i < len; i++) {
		struct iovec& v = iov[i];
		ss.write((char*)v.iov_base, v.iov_len);
		n_bytes += v.iov_len;
	}
	PyObject* py_ret = PyBytes_FromStringAndSize(ss.str().c_str(), n_bytes);
	handle<> handle(py_ret);
	return object(handle);
}

void _service_unpack_only(object svc_wrapper, char* response_buffer, uint32_t response_len, uint8_t need_swap)
{
	byte_array_swapper swapper(
		svc_wrapper.ptr(), "_response_buffer",
		response_buffer, response_len,
		false, /*need_to_acquire_gil=*/false
	);
	if(!swapper.lock())
		return;

	// process response
	svc_wrapper.attr("_need_swap") = need_swap;
	svc_wrapper.attr("_unpack_response")();

	swapper.unlock();
}

void service_unpack_only(object svc_wrapper, object response, uint8_t need_swap) {
	_service_unpack_only(svc_wrapper, PyBytes_AsString(response.ptr()), len(response), need_swap);
}


void service_call(ln::service* svc, object svc_wrapper, double timeout) {
	unsigned int len;
	struct iovec* iov;
	uint8_t* iov_element_lens = NULL;
	_service_fill_only(svc_wrapper, &len, &iov, &iov_element_lens);

	char* response_buffer;
	uint32_t response_len;
	uint8_t need_swap = 0;
	try {
		GILRelease unlockGIL;
		svc->callv(iov, len, iov_element_lens, &response_buffer, &response_len, &need_swap, timeout);
	}
	catch(ln::exception& e) {
		throw;
	}

	_service_unpack_only(svc_wrapper, response_buffer, response_len, need_swap);

	return;
}

object service_call_async(ln::service* svc, object svc_wrapper) {
	object _response_buffer = svc_wrapper.attr("_response_buffer");
	PyObject* response_buffer_object = _response_buffer.ptr();
	if(!PyByteArray_CheckExact(response_buffer_object)) {
		PyErr_SetString(PyExc_ValueError, "provided _response_buffer is not a bytearray-object!");
		throw_error_already_set();
		return object();
	}

	object cast_buffer_ = svc_wrapper.attr("_cast_buffer");
	if(!PyByteArray_Check(cast_buffer_.ptr())) {
		PyErr_SetString(PyExc_ValueError, "provided _cast_buffer is not a bytearray-object!");
		throw_error_already_set();
		return object();
	}

	object iovec_buffer_ = svc_wrapper.attr("_iovec_buffer");
	if(!PyByteArray_Check(iovec_buffer_.ptr())) {
		PyErr_SetString(PyExc_ValueError, "provided _iovec_buffer is not a bytearray-object!");
		throw_error_already_set();
		return object();
	}

	object iovec_element_lens_buffer_ = svc_wrapper.attr("_iovec_element_lens_buffer");
	if(!PyByteArray_Check(iovec_element_lens_buffer_.ptr())) {
		PyErr_SetString(PyExc_ValueError, "provided _iovec_element_lens_buffer is not a bytearray-object!");
		throw_error_already_set();
		return object();
	}
	
	fill_data_t fill_data(
		svc->clnt->clnt,
		cast_buffer_.ptr(), 
		iovec_buffer_.ptr(),
		iovec_element_lens_buffer_.ptr());

	if(fill_data.fill(
		   ((object)svc_wrapper.attr("req")).ptr(),
		   ((object)svc_wrapper.attr("_req_fields")).ptr())
		) {
		throw_error_already_set();
		return object();
	}

	// fill_data.print();
	unsigned int len;
	struct iovec* iov = fill_data.get_iov(len);

	ln::service_request* req;
	try {
		req = svc->callv_async(iov, len, fill_data.get_iov_element_lens()); // todo: give element lens
	}
	catch(ln::exception& e) {
		throw;
	}
	return object(my_ptr<ln::service_request>(req));
}

void service_async_unpack_response(ln::service* svc, object svc_wrapper)
{
	char* response_buffer;
	uint32_t response_len;
	svc->callv_async_get_response(&response_buffer, &response_len);

	byte_array_swapper swapper(
		svc_wrapper.ptr(), "_response_buffer",
		response_buffer, response_len,
		false, false
	);
	if(!swapper.lock())
		return;
	// todo: no need_swap?
	svc_wrapper.attr("_unpack_response")();
	swapper.unlock();
}

static int _py_service_handler(ln::client& clnt, ln::service_request& req, void* user_data) {
	PyObject* svc_ob = (PyObject*)user_data;

	char* request_data;
	uint64_t request_data_len;
	req.get_request_data(&request_data, &request_data_len);

	byte_array_swapper swapper(
		svc_ob, "_response_buffer",
		request_data, request_data_len,
		true, true
	);
	if(!swapper.lock())
		return -1;

	// process response
	PyObject* args = Py_BuildValue("(O)", object(my_ptr<ln::service_request>(&req)).ptr());
	PyObject* handler_ob = PyObject_GetAttrString(svc_ob, "_handler");
	PyObject* ret = PyObject_Call(handler_ob, args, NULL);
	Py_DECREF(args);

	// restore bytearray!
	swapper.unlock();

	int iret = 0;
	if(ret) {
		if(ret != Py_None)
			iret = PyLong_AS_LONG(ret);
		Py_DECREF(ret);
	}

	return iret;
}
	
void service_do_register(boost::shared_ptr<ln::service> svc, object service_wrapper, object group_name) {
	svc->set_handler(_py_service_handler, service_wrapper.ptr());
	if(group_name.ptr() == object().ptr())
		svc->do_register();
	else {
		std::string gn = extract<std::string>(group_name);
		svc->do_register(gn.c_str());
	}
}

static void _py_service_fd_handler(ln::client& clnt, ln_svc_client_event_t event, int fd, void* user_data) {
	PyObject* svc_ob = (PyObject*)user_data;

	std::string ev;
	if(event == LN_NEW_SVC_CLIENT)
		ev = "new";
	else if(event == LN_REMOVE_SVC_CLIENT)
		ev = "remove";
	else if(event == LN_NEW_SVC_FD)
		ev = "new_svc_fd";
	else if(event == LN_REMOVE_SVC_FD)
		ev = "remove_svc_fd";

	PyGILState_STATE gstate;
	gstate = PyGILState_Ensure();
	
	PyObject* handler_ob = PyObject_GetAttrString(svc_ob, "_fd_handler");

	// process response
	PyObject* args = Py_BuildValue("(si)", ev.c_str(), fd);
	PyObject* ret = PyObject_Call(handler_ob, args, NULL);
	Py_DECREF(args);
	Py_DECREF(handler_ob);
	Py_DECREF(ret);

	PyGILState_Release(gstate);
}

void service_set_client_fd_handler(boost::shared_ptr<ln::service> svc, object service_wrapper) {
	svc->set_client_fd_handler(_py_service_fd_handler, service_wrapper.ptr());
}

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(ln_client_needs_provider_overloads, ln::client::needs_provider, 1, 2)

void wait_and_handle_service_group_requests(boost::shared_ptr<ln::client> clnt, object group_name, double timeout) {
	GILRelease unlockGIL;
	if(group_name.ptr() == object().ptr())
		clnt->wait_and_handle_service_group_requests(NULL, timeout);
	else {
		std::string gn = extract<std::string>(group_name);
		clnt->wait_and_handle_service_group_requests(gn.c_str(), timeout);
	}
}

void svc_request_respondv(ln::service_request& req, object svc_wrapper) {
	object cast_buffer_ = svc_wrapper.attr("_cast_buffer");
	if(!PyByteArray_Check(cast_buffer_.ptr())) {
		PyErr_SetString(PyExc_ValueError, "provided _cast_buffer is not a bytearray-object!");
		throw_error_already_set();
		return;
	}

	object iovec_buffer_ = svc_wrapper.attr("_iovec_buffer");
	if(!PyByteArray_Check(iovec_buffer_.ptr())) {
		PyErr_SetString(PyExc_ValueError, "provided _iovec_buffer is not a bytearray-object!");
		throw_error_already_set();
		return;
	}

	object iovec_element_lens_buffer_ = svc_wrapper.attr("_iovec_element_lens_buffer");
	if(!PyByteArray_Check(iovec_element_lens_buffer_.ptr())) {
		PyErr_SetString(PyExc_ValueError, "provided _iovec_element_lens_buffer is not a bytearray-object!");
		throw_error_already_set();
		return;
	}
	
	fill_data_t fill_data(
		req.svc->clnt->clnt,
		cast_buffer_.ptr(), 
		iovec_buffer_.ptr(),
		iovec_element_lens_buffer_.ptr());

	if(fill_data.fill(
		   ((object)svc_wrapper.attr("resp")).ptr(),
		   ((object)svc_wrapper.attr("_resp_fields")).ptr())
		) {
		throw_error_already_set();
		return;
	}

	// fill_data.print();
	unsigned int len;
	struct iovec* iov = fill_data.get_iov(len);

	{
		GILRelease unlockGIL;
		req.respondv(iov, len);
	}
	return;
}

PyObject* dsa_dss1_sign_message(std::string key_file, object message_ob)
{
#ifdef NO_DAEMON_AUTH
#  warning NO_DAEMON_AUTH: key signing for ln_daemon auth disabled!
	PyErr_SetString(PyExc_ValueError, "we are compiled with NO_DAEMON_AUTH!");
	throw_error_already_set();
	return NULL;
#else
	if(!PyBytes_Check(message_ob.ptr())) {
		PyErr_SetString(PyExc_ValueError, "message is not a bytes object!");
		throw_error_already_set();
		return NULL;
	}
	std::string message(PyBytes_AS_STRING(message_ob.ptr()), PyBytes_GET_SIZE(message_ob.ptr()));
	std::string signature = ln::dsa_dss1_sign_message(key_file, message);
	PyObject* ret = PyBytes_FromStringAndSize((char*)signature.c_str(), signature.size());
	return ret;
#endif
}

void on_event(ln::event_call& call, void* user_data) {
	PyGILState_STATE gstate;
	gstate = PyGILState_Ensure();
	
	PyObject* wrapper_ob = (PyObject*)user_data;
	object wrapper(handle<>(borrowed(wrapper_ob)));
	object helper_svc = wrapper.attr("helper_svc");

	uint32_t receive_len;
	void* receive_buffer = call.get_buffer(&receive_len);
	
	_service_unpack_only(helper_svc, (char*)receive_buffer, receive_len, 0); // todo: dont' know whether we need swap!

	PyObject_Call(wrapper_ob, make_tuple().ptr(), NULL);
	
	PyGILState_Release(gstate);
}


object connect_to_event(tuple args, dict kwargs) {
	ln::client* clnt = extract<ln::client*>(args[0]);
	std::string event_name = extract<std::string>(args[1]);
	std::string event_md = extract<std::string>(args[2]); // todo: support none here
	
	std::string definition;
	std::string signature;
	
	if(event_md.size()) {
		clnt->get_service_signature(event_md, definition, signature);
	} else {
		clnt->get_service_signature_for_service(event_name, event_md, definition, signature);
	}
	
	return ln_module.attr("event_connection_wrapper")(args, kwargs, definition, signature);
}

void _connect_to_event(ln::client* clnt, object wrapper) {
	std::string event_name = extract<std::string>(wrapper.attr("name"));
	std::string connect_signature = extract<std::string>(wrapper.attr("connect_signature"));

	object connect_data = service_fill_only(wrapper.attr("helper_svc"));
	wrapper.attr("connect_data") = connect_data; // keep reference such that pointer stays valid!
	
	/*ln::event_connection* ev_conn = */
	auto connect_data_ptr = PyBytes_AS_STRING(connect_data.ptr());
	clnt->connect_to_event_flat(
		event_name, connect_signature, connect_data_ptr, len(connect_data),
		&on_event, wrapper.ptr());
	// wrapper.attr("connection") = ev_conn;
}


// internal requests
extern "C" {
void* _ln_new_request();
int _ln_init_request(void* clnt, void* req, ...);
int _ln_add_to_request(void* req, char* header, char* value);
int _ln_reset_request(void* req);
int _ln_destroy_request(void* req);
int _ln_reset_request(void* req);
int _ln_send_request(void* r);
int _ln_wait_response(void* resp);
void _ln_lock_request(void* r);
void _ln_unlock_request(void* r);
int _ln_get_from_request_raw(void* req, int i, char** field_name, char** target);
int _ln_get_count_from_request(void* req);

}

#ifdef LN_WITH_GL_HELPERS
#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glext.h>
#endif


#if PY_VERSION_HEX >= 0x03000000 // import_array() decides whether to call "return" or "return NULL"
void* void_import_array(bool* success)
{
	import_array();
	*success = true;
	return NULL;
}
#else
void void_import_array(bool* success)
{
	import_array();
	*success = true;
}
#endif

bool do_import_array()
{
	bool success = false;
	void_import_array(&success);
	return success;
}

void do_import_ln()
{
	def("copy2array", &my_copy2array);
	def("copy2buffer", &my_copy2buffer);

#ifdef LN_WITH_GL_HELPERS
	def("glEnableVertexAttribArray", &glEnableVertexAttribArray);
	def("glDisableVertexAttribArray", &glEnableVertexAttribArray);
#endif

	scope().attr("RATE_AS_PUBLISHED") = RATE_AS_PUBLISHED;
	scope().attr("RATE_ON_REQUEST_LAST") = RATE_ON_REQUEST_LAST;

	scope().attr("LN_LIBRARY_VERSION") = LN_LIBRARY_VERSION;
	scope().attr("LN_MANAGER_CLIENT_ID") = 0xffffffff;

	scope().attr("LNE_SVC_RECV_REQ") = (unsigned int)LNE_SVC_RECV_REQ;
	scope().attr("LNE_SVC_HANDLER_EXC") = (unsigned int)LNE_SVC_HANDLER_EXC;

#ifdef NO_DAEMON_AUTH
	scope().attr("NO_DAEMON_AUTH") = true;
#else
	scope().attr("NO_DAEMON_AUTH") = false;
#endif

	def("service_fill_only", &service_fill_only);
	def("service_unpack_only", &service_unpack_only);
	def("dsa_dss1_sign_message", &dsa_dss1_sign_message);
	def("construct_client_from_pointer", &construct_client_from_pointer);

	class_<ln::client, boost::noncopyable, boost::shared_ptr<ln::client> >("client", no_init)
		.def("__init__", make_constructor(&init_client))
		.def("__init__", make_constructor(&init_client2))
		.def_readonly("name", &ln::client::name)
		.def("ping", &ln::client::ping, (boost::python::arg("timeout")=-1))
		.def("get_remaining_args", &ln_client_get_remaining_args)
		.def("put_message_definition", &ln::client::put_message_definition)
		.def("get_message_definition", &get_message_definition)
		.def("get_message_definition_for_topic", &get_message_definition_for_topic)
		.def("describe_message_definition", &ln::client::describe_message_definition)
		.def("get_service_signature_for_service", &get_service_signature_for_service)
		.def("get_service_signature", &get_service_signature)
		.def("subscribe", &subscribe,
		     //subscribe_overloads(args("client", "topic_name", "message_definition_name", "rate")))
		     ( boost::python::arg("topic_name"),
		       boost::python::arg("message_definition_name")=object(),
		       boost::python::arg("rate")=-1.0,
		       boost::python::arg("buffers")=1,
		       boost::python::arg("need_reliable_transport")=false
			     ))
		.def("unsubscribe", &unsubscribe)
		.def("publish", &publish,
		     ( boost::python::arg("topic_name"),
		       boost::python::arg("message_definition_name")="",
		       boost::python::arg("buffers")=1
			     ))
		.def("unpublish", &unpublish)
		.def("get_multi_waiter", &get_multi_waiter)
		.def("wait_and_handle_service_group_requests",
		     &wait_and_handle_service_group_requests,
		     (
			     boost::python::arg("group_name")=NULL,
			     boost::python::arg("timeout")=-1))
		.def("handle_service_group_in_thread_pool", &ln::client::handle_service_group_in_thread_pool)
		.def("remove_service_group_from_thread_pool", &ln::client::remove_service_group_from_thread_pool)
		.def("set_max_threads", &ln::client::set_max_threads)
		.def("thread_pool_setschedparam", &ln::client::thread_pool_setschedparam)
		.def("get_service", &get_service, (
			     boost::python::arg("client"),
			     boost::python::arg("service_name"),
			     boost::python::arg("interface_name")="",
			     boost::python::arg("mainloop")=object()
			     )
			)
		.def("get_service_provider", &get_service_provider, get_service_provider_overloads(args("client", "service_name", "interface_name", "mainloop")))
		.def("release_service", &release_service)

		.def("needs_provider", &ln::client::needs_provider, ln_client_needs_provider_overloads(args("service_or_topic_name", "timeout")))
		.def("get_logger", &get_logger)
		.def("find_services_with_interface", &ln::client::find_services_with_interface)
		.def("get_parameters", &get_parameters,
		     ( boost::python::arg("client"),
		       boost::python::arg("parameter_group_prefix")="",
		       boost::python::arg("allow_empty")=false
			     ))
		.def("connect_to_event", raw_function(&connect_to_event, 3))
		.def("_connect_to_event", &_connect_to_event)
		.def("set_thread_safe", &ln::client::set_thread_safe)
		.def("register_as_service_logger", &ln::client::register_as_service_logger)
		.def("set_float_parameter", &ln::client::set_float_parameter)
		;

	class_<ln::inport, boost::noncopyable, my_ptr<ln::inport> >("inport", no_init)
		.def("read", &ln_inport_read)
		.def_readonly("timestamp", &ln::inport::timestamp)
		.def("has_publisher", &ln::inport::has_publisher)
		.def("unblock", &ln::inport::unblock)
		;
	class_<ln::outport, boost::noncopyable, my_ptr<ln::outport> >("outport", no_init)
		.def("write", &ln_outport_write)
		.def("write_ts", &ln_outport_write_ts)
		.def("has_subscriber", &ln::outport::has_subscriber)
		;

	class_<ln::multi_waiter, boost::noncopyable, my_hold_ptr<ln::multi_waiter, ln::client> >("multi_waiter", no_init)
		// .def("__init__", make_constructor(&init_multi_waiter))
		.def("add_port", &multi_waiter_add_port) // &ln::multi_waiter::add_port)
		.def("remove_port", &multi_waiter_remove_port) // &ln::multi_waiter::add_port)
		.def("wait", &ln::multi_waiter::wait, multi_waiter_wait_overloads(args("timeout")))
		.def("can_read", &multi_waiter_can_read)
		.def("start_pipe_notifier_thread", &multi_waiter_start_pipe_notifier_thread)
		.def("ack_pipe_notification", &ln::multi_waiter::ack_pipe_notification)
		;

	class_<ln::logger, boost::noncopyable, my_hold_ptr<ln::logger, ln::client> >("logger", no_init)
		.def_readonly("name", &ln::logger::name)
		.def("add_topic", &ln::logger::add_topic, ln_logger_add_topic_overloads(args("topic_name", "log_size", "divisor")))
		.def("clear_topics", &ln::logger::clear_topics)
		.def("set_only_ts", &ln::logger::set_only_ts)
		.def("enable", &ln::logger::enable)
		.def("disable", &ln::logger::disable)
		.def("manager_save", &ln::logger::manager_save, ln_logger_manager_save_overloads(args("filename", "format")))
		.def("download", &_ln_logger_download)
		.def("direct_download", &_ln_logger_direct_download)
		;

	class_<ln::logger_data, boost::noncopyable, my_ptr<ln::logger_data> >("logger_data", no_init)
		.def("save", &ln::logger_data::save)
		.def("load", &_ln_logger_data_load).staticmethod("load")
		;

	class_<ln::logger_data::topic_cursor, boost::noncopyable, my_ptr<ln::logger_data::topic_cursor> >("topic_cursor", no_init)
		.def_readonly("name", &ln::logger_data::topic_cursor::name)
		.def("get_topic", &_ln_logger_data_topic_cursor_get_topic)
		.def("get_sample", &_ln_logger_data_topic_cursor_get_sample)
		;

	class_<logger_topic_, boost::noncopyable, my_ptr<logger_topic_> >("logger_topic_", no_init)
		.def_readonly("log_size", &logger_topic_::log_size)
		.def_readonly("divisor", &logger_topic_::divisor)
		.def_readonly("n_samples", &logger_topic_::n_samples)
		.def_readonly("sample_size", &logger_topic_::sample_size)
		.add_property("md_name", &logger_topic_get_md_name)
		.add_property("md", &logger_topic_get_md)
		;

	class_<ln::service, boost::noncopyable, my_hold_ptr<ln::service, ln::client> >("service", no_init)
		.def("do_register", &service_do_register)
		.def("get_fd", &ln::service::get_fd)
		.def("set_client_fd_handler", &service_set_client_fd_handler)
		.def("call", &service_call)
		.def("call_async", &service_call_async)
		.def("abort", &ln::service::abort)
		.def("async_unpack_response", &service_async_unpack_response)
		;

	class_<ln::service_request, boost::noncopyable, my_ptr<ln::service_request> >("service_request", no_init)
		.def("respondv", &svc_request_respondv)
		.def("finished", &ln::service_request::finished)
		.def("get_finished_notification_fd", &ln::service_request::get_finished_notification_fd)
		.def("abort", &ln::service_request::abort)
		.def("is_aborted", &ln::service_request::is_aborted)
		;

	pystring_converter().from_python<std::string>();
	register_exception_translator<ln::exception>(translator);
	// register_ptr_to_python< boost::shared_ptr<ln::inport> >();
}

BOOST_PYTHON_MODULE(_ln)
{
	if(!do_import_array())
		return;
	try {
		do_import_ln();
	}
	catch(const std::exception& e) {
		printf("exception while doing import of _ln: %s\n", e.what());
	}
}
