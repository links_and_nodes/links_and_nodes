from __future__ import print_function

import os
import sys
import struct
from collections import namedtuple
from collections import OrderedDict as odict

from links_and_nodes import ln_wrappers

from .lnrecorder_packets import new_topic_packet, new_tpacket_packet, header_packet, del_topic_packet, new_service_peer_packet, new_service_packet, service_event_packet, packet

def hexlify(data):
    return " ".join(("%02x" % ord(b) for b in data))
        
# FILE_VERSION = 1
FILE_VERSION = 2 # new topic packet frames have extra publisher timestamp after packet data and use recorder wall time for pcap-header-ts
                 # and msg_def_hash for new {topic, service} packets

class lnrecorder_log(object):
    pcap_file_header_format = "IHHIIII"
    pcap_file_header = namedtuple("pcap_file_header", "magic,major,minor,timezone_offset,time_stmap_acc,snap_len,ll_header_type")
    pcap_file_header_len = struct.calcsize("=" + pcap_file_header_format)
    pcap_packet_header_format = "IIIII"
    pcap_packet_header = namedtuple("pcap_packet_header", "sec,nsec,n_bytes,full_n_bytes,frame_type")
    pcap_packet_header_len = struct.calcsize("=" + pcap_packet_header_format)
    
    def __init__(self, fn, for_writing=False):
        self.filename = fn
        self.writing = for_writing
        if not for_writing:
            # for reading
            self.fp = open(self.filename, "rb")
            self.log_filesize = os.path.getsize(self.filename)
            self.endianess = "="
            self.log_start = None
            self.file_version = None # not yet known
            
            self.read_file_header()
        else:            
            self.fp = open(self.filename + ".tmp", "wb")
            self.endianess = "="
            self.file_version = FILE_VERSION
            self.write_file_header()
        
        self.n_packets = 0
        self.topics = {}
        self.peers = {}
        self.services = {}

    @staticmethod
    def open_for_writing(fn):
        return lnrecorder_log(fn, for_writing=True)
    
    def write_file_header(self):
        hdr = lnrecorder_log.pcap_file_header(0xa1b2c3d4, 2, 4, 0, 0, 0xffffffff, 147)
        self.fp.write(self.pack(lnrecorder_log.pcap_file_header_format, hdr))
        self.have_header = False

        
    def write_new_topic(self, ts, message_size, topic_name, topic_md, msg_def, publisher_name, msg_def_hash):
        topic_id = len(self.topics)
        pkt = new_topic_packet(self, (topic_id, message_size, topic_name, topic_md, msg_def, publisher_name, msg_def_hash), self.file_version)
        self.write_frame(ts, pkt)
        self.topics[topic_id] = pkt
        return topic_id
    
    def write_new_topic_packet(self, ts, topic_id, packet_data, publisher_ts):
        pkt = new_tpacket_packet(self, (topic_id, packet_data, publisher_ts), self.file_version)
        self.write_frame(ts, pkt)

    def write_frame(self, ts, pkt):
        if not self.have_header:
            hdr_pkt = header_packet(self, 0xffff0000 + FILE_VERSION)
            self.have_header = True
            self.write_frame(ts, hdr_pkt)        
        sec = int(ts)
        nsec = int((ts - sec) * 1e9)
        n_bytes = len(pkt.data) + 4
        hdr = lnrecorder_log.pcap_packet_header(sec, nsec, n_bytes, n_bytes, pkt.frame_type)
        self.fp.write(self.pack(lnrecorder_log.pcap_packet_header_format, hdr))
        self.fp.write(pkt.data)
    def close(self):
        self.fp.close()
        self.fp = None
        os.rename(self.filename + ".tmp", self.filename)
    
    def pack(self, format, data):
        return struct.pack(self.endianess + format, *data)
    def unpack(self, format, data, tuple_type):
        return tuple_type(*struct.unpack(self.endianess + format, data))

    def read_file_header(self):        
        hdr_data = self.fp.read(lnrecorder_log.pcap_file_header_len)
        if len(hdr_data) != lnrecorder_log.pcap_file_header_len:
            raise Exception("could not read %d bytes of file header!" % lnrecorder_log.pcap_file_header_len)
        
        hdr = self.unpack(lnrecorder_log.pcap_file_header_format, hdr_data, lnrecorder_log.pcap_file_header)
        if hdr.magic != 0xa1b2c3d4:
            org_magic = hdr.magic
            # switch endianess
            if sys.byteorder == "little":
                self.endianess = ">"
            else:
                self.endianess = "<"
            hdr = self.unpack(lnrecorder_log.pcap_file_header_format, hdr_data, lnrecorder_log.pcap_file_header)
            if hdr.magic != 0xa1b2c3d4:
                raise Exception("file has wrong pcap header magic of %#x" % org_magic)
        
        if hdr.major != 2:
            print(("warning: file has unknown major %d", hdr.major))
        if hdr.minor != 4:
            print(("warning: file has unknown major %d", hdr.mino))
        if hdr.ll_header_type != 147:
            raise Exception("file has wrong pcap ll_header_type value of %d - expect 147 (USR1)", hdr.ll_header_type)

    def read_frame(self):
        """
        better call read_next_frame!
        """
        frame_hdr_data = self.fp.read(lnrecorder_log.pcap_packet_header_len)
        if len(frame_hdr_data) == 0:
            return None
        frame_hdr = self.unpack(lnrecorder_log.pcap_packet_header_format, frame_hdr_data, lnrecorder_log.pcap_packet_header)
        to_read = frame_hdr.n_bytes - 4
        frame_data = self.fp.read(to_read)
        if len(frame_data) != to_read:
            raise Exception("failed to read full %d bytes: %d (hdr: %s)" % (to_read, len(frame_data), frame_hdr))
        if self.log_start is None:
            self.log_start = frame_hdr.sec + float(frame_hdr.nsec) / 1e9

        return frame_hdr, frame_data
    
    def read_next_frame(self):
        offset = self.fp.tell()
        frame = self.read_frame()
        if frame is None:
            return None
        
        ft = frame[0].frame_type
        if ft in (0, 1, 4, 5):
            pkt = self.get_packet(frame[0], frame[1])
            if ft == 1:
                self.topics[pkt.topic_id] = pkt
            elif ft == 4:
                self.peers[pkt.peer_id] = pkt
            elif ft == 5:
                self.services[pkt.service_id] = pkt
        self.n_packets += 1
        
        return offset, frame[0], frame[1]
    
    def read_frame_at_offset(self, offset):
        org_offset = self.fp.tell()
        self.fp.seek(offset)
        frame = self.read_frame()
        self.fp.seek(org_offset)
        return frame

    def read_fields(self, data, data_offset=0, n_fields=1, return_new_offset=False):
        ret = []
        o = data_offset
        for _ in range(n_fields):
            field_len = struct.unpack(self.endianess + "I", data[o:o + 4])[0]
            ret.append(
                data[o + 4:o + 4 + field_len])
            o += 4 + field_len
        if not return_new_offset:
            if n_fields == 1:
                return ret[0]
            return ret
        if n_fields == 1:
            return ret[0], o
        ret.append(o)
        return ret
    
    def write_fields(self, *fields):
        data = []
        for field in fields:
            data.append(struct.pack(self.endianess + "I", len(field)))
            data.append(field.encode("utf-8"))
        return b"".join(data)
        
    def get_packet(self, hdr, data):
        if hdr.frame_type == 0: # header
            p = header_packet(self, data)
            self.file_version = p.version & 0xffff
            if self.file_version > FILE_VERSION:
                print("warning: file has version %d, we only now versions up to %d" % (self.file_version, FILE_VERSION))
        elif hdr.frame_type == 1:
            p = new_topic_packet(self, data, self.file_version)
        elif hdr.frame_type == 2:
            p = new_tpacket_packet(self, data, self.file_version)
        elif hdr.frame_type == 3:
            p = del_topic_packet(self, data)
        elif hdr.frame_type == 4:
            p = new_service_peer_packet(self, data)
        elif hdr.frame_type == 5:
            p = new_service_packet(self, data)
        elif hdr.frame_type == 6:
            p = service_event_packet(self, data)
        else:
            p = packet(self, data)
        p.header = hdr
        p.frame_type = hdr.frame_type
        
        if hdr.frame_type == 2 and self.file_version == 1:
            # publisher timestamp is stored in pcap frame header
            p.publisher_timestamp = hdr.sec + float(hdr.nsec) / 1e9
        return p
    
    def read_packet_at_offset(self, offset):
        hdr, data = self.read_frame_at_offset(offset)
        p = self.get_packet(hdr, data)
        p.offset = offset
        return p

    def iter_frames(self):
        """
        iterate over raw pcap-frames of this log
        """
        while True:
            ret = self.read_next_frame()
            if ret is None:
                break
            yield ret # offset, hdr, data
        
    def iter_raw_packets(self):
        """
        iterate over raw packets of this log
        """
        for offset, hdr, data in self.iter_frames():
            p = self.get_packet(hdr, data)
            p.offset = offset
            yield p
            
    def iter_packets(self, non_meta_frame_types=None, only_services=False):
        """
        iterate over non-meta packets of this log
        (~500000 pkts in 15 seconds)
        """
        if non_meta_frame_types is None:
            if only_services:
                non_meta_frame_types = set([6])
            else:
                non_meta_frame_types = set([2, 6])
        for offset, hdr, data in self.iter_frames():
            if hdr.frame_type not in non_meta_frame_types:
                continue
            p = self.get_packet(hdr, data)
            p.offset = offset
            yield p
    
    def decode_packet(self, pkt, with_meta=True):
        p = odict()
        p["header"] = pkt.header
        
        if pkt.frame_type == 6: # service event
            svc = self.services[pkt.service_id]
            p["type"] = "service event"
            p["name"] = svc.service_name
            p["interface"] = svc.service_interface
            p["from client"] = svc.client_name
            p["client_id"] = pkt.client_id
            p["request_id"] = pkt.request_id
            if with_meta:
                p["meta"] = meta = odict()
                meta["peer address"] = self.peers[pkt.peer_id].peer
                for field in "flags,completion_time,transfer_time,request_time,thread_id".split(","):
                    meta[field] = getattr(pkt, field)
                meta["message_definition"] = svc.message_definition
                
            if pkt.item_type == 0:
                p["event_type"] = "request"
                is_provider = True # unpack request!
            else:
                p["event_type"] = "response"
                is_provider = False

            lnp = ln_wrappers.service_wrapper(
                svc.service_name.decode("utf-8"),
                svc.service_interface.decode("utf-8"),
                svc.message_definition.decode("utf-8"),
                None, None, None,
                is_provider=is_provider
                )

            payload_len = struct.unpack("=I", pkt.packet[:4])[0]
            if payload_len != len(pkt.packet):
                raise Exception("invalid packet: payload_len in payload %#x does not match payload len from frame %#x!" % (
                    payload_len, len(pkt.packet)))
            payload = pkt.packet[4:]
            
            if is_provider:
                #print "is provider, flags: %#x, payload len: %#x" % (pkt.flags, len(pkt.packet))
                #print hexlify(pkt.packet)
                fields_and_def = (lnp.req, lnp._req_fields)
                field = lnp.req
                damaged_test_log = "rollin_justin_blinken_test_record" in self.filename
                old_service_providers = "alfred_t4.led.stop_all", "alfred_t4.led.start_animation"
                if pkt.flags & 2 or (damaged_test_log and svc.service_name not in old_service_providers):
                    # packet contains header
                    o = 2+4+4
                    #ver, client_id, request_id = struct.unpack("=HII", payload[:o])
                    #print "service call header:", ver, client_id, request_id
                else:
                    o = 0
                lnp._response_buffer = bytearray(payload[o:])
            else:
                #print "is not provider, response!"
                # response
                fields_and_def = None
                field = lnp.resp
                p["service_handler_retval"] = struct.unpack("I", payload[:4])[0]
                lnp._response_buffer = bytearray(payload[4:])

            lnp._unpack_response(fields_and_def)
            p["data"] = data = odict()
            for key, value in field.items():
                data[key] = value
                
        elif pkt.frame_type == 2: # topic packet
            topic = self.topics[pkt.topic_id]
            p["type"] = "topic packet"
            p["name"] = topic.name
            p["message_definition_name"] = topic.md
            p["message_definition"] = topic.msg_def
            p["publisher"] = topic.publisher
                
            lnp = ln_wrappers.ln_packet(
                topic.md,
                topic.msg_def,
                topic.message_size
            )
            lnp._data = bytearray(pkt.packet)
            lnp._data_buffer = memoryview(lnp._data)
            lnp._unpack()
            p["data"] = data = odict()
            def process_value(value):
                if isinstance(value, ln_wrappers.ln_packet_part):
                    inner = odict()
                    unpack_into(inner, value)
                    return inner
                if isinstance(value, list):
                    inner = []
                    unpack_list_into(inner, value)
                    return inner
                return value
            def unpack_into(data, p):
                for key, value in p.items():
                    data[key] = process_value(value)
            def unpack_list_into(data, p):
                for value in p:
                    data.append(process_value(value))
            unpack_into(data, lnp)

        else:
            p["packet"] = pkt
            #p["raw_payload"] = pkt.packet
        return p
        
