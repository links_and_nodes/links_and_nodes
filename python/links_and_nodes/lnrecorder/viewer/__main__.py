import sys

from . import lnrecorder_viewer

if __name__ == "__main__":
    viewer = lnrecorder_viewer(sys.argv[1:])
    viewer.run()
