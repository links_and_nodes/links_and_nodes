from __future__ import print_function

import sys
import time
from collections import OrderedDict as odict

import numpy as np

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
from gi.repository import Gtk, GLib, GObject # noqa: E402

from links_and_nodes.lnrecorder.lnrecorder_log import lnrecorder_log

np.set_printoptions(linewidth=140, precision=4, suppress=True)

__all__ = [ "lnrecorder_viewer" ]

class lnrecorder_viewer(object):
    def __init__(self, args):
        self.filename = None
        self.parse_args(args)
        self.init_gui()

        self.frame_types = {
            0: "HDR",
            1: "NTOP",
            2: "NPKT",
            3: "DTOP",
            4: "NSPN",
            5: "NSVC",
            6: "SVCE"
        }
        self.meta_frame_types = set([
            "HDR",
            "NTOP",
            "DTOP",
            "NSPN",
            "NSVC"])

        self.log = lnrecorder_log(self.filename)
        # start scanning thru complete file in short chunks via gobject idle handler
        self.log.state = "initial_scan"
        self.initial_count = 0
        self.initial_count_divider = 5
        self.log.start_time = time.time()

        self.display_generation = 0
        self.short_cache = {}
        self.collapsed_rows = set()
        GLib.idle_add(self.initial_log_scan)
        #while self.initial_log_scan(): pass

    def parse_args(self, args):
        skip = 0
        for _, arg in enumerate(args):
            if skip:
                skip -= 1
                continue
            self.filename = arg
        if self.filename is None:
            self.usage()

    def usage(self):
        print("usage: %s lnrecorder_logfile" % sys.argv[0])
        sys.exit(0)

    def init_gui(self):
        # layout
        self.window = Gtk.Window()
        self.window.set_title("lnrecorder_viewer %s" % self.filename)
        self.window.connect("delete-event", self.on_window_delete)
        self.window.set_default_size(900, 700)

        self.main_vbox = Gtk.VBox()
        self.window.add(self.main_vbox)

        self.toolbar = Gtk.HBox()
        self.hide_meta_cb = Gtk.CheckButton("hide meta pkts")
        self.hide_meta_cb.set_active(True)
        self.hide_meta_cb.connect("toggled", self.on_hide_topics)
        self.toolbar.pack_start(self.hide_meta_cb, False, False, 0)
        self.hide_topics_cb = Gtk.CheckButton("hide topics")
        self.hide_topics_cb.set_active(True)
        self.hide_topics_cb.connect("toggled", self.on_hide_topics)
        self.toolbar.pack_start(self.hide_topics_cb, False, False, 0)
        self.hide_services_cb = Gtk.CheckButton("hide services")
        self.hide_services_cb.set_active(False)
        self.hide_services_cb.connect("toggled", self.on_hide_topics)
        self.toolbar.pack_start(self.hide_services_cb, False, False, 0)

        self.main_vbox.pack_start(self.toolbar, False, False, 0)

        self.main_paned = Gtk.VPaned()
        self.main_paned.set_position(500)
        self.main_vbox.pack_start(self.main_paned, True, True, 0)

        self.statusbar_hbox = Gtk.HBox()
        self.main_vbox.pack_start(self.statusbar_hbox, False, False, 0)

        self.status = Gtk.Label()
        self.status.set_alignment(0, 0.5)
        self.statusbar_hbox.pack_start(self.status, True, True, 0)

        self.main_model = Gtk.ListStore(
            GObject.TYPE_UINT64, # file offset
            GObject.TYPE_UINT, # number
            GObject.TYPE_DOUBLE, # time
            GObject.TYPE_STRING, # type
            GObject.TYPE_BOOLEAN, # 4: no meta
            GObject.TYPE_BOOLEAN, # 5: no meta or topic
            GObject.TYPE_BOOLEAN, # 6: no meta or service
        )
        self.filtered_models = dict()
        self.filters = dict(
            no_meta=4,
            no_meta_or_topic=5,
            no_meta_or_service=6
        )

        tv = self.main_tv = Gtk.TreeView()
        tv.connect("cursor-changed", self.on_cursor_change)
        #tv.set_fixed_height_mode(True) # gtk.TREE_VIEW_COLUMN_FIXED
        self.set_filter("no_meta_or_topic")
        #tv.insert_column_with_attributes(-1, "offset", gtk.CellRendererText(), text=0)
        tv.insert_column_with_attributes(-1, "no.", Gtk.CellRendererText(), text=1)
        tv.get_columns()[-1].set_resizable(True)
        #col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
        tv.insert_column_with_attributes(-1, "time", Gtk.CellRendererText(), text=2)
        tv.get_columns()[-1].set_resizable(True)
        #col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
        tv.insert_column_with_attributes(-1, "type", Gtk.CellRendererText(), text=3)
        tv.get_columns()[-1].set_resizable(True)
        tv.insert_column_with_data_func(-1, "info", Gtk.CellRendererText(), self.on_update_short)
        tv.get_columns()[-1].set_resizable(True)
        #col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)

        self.main_tv_sw = Gtk.ScrolledWindow()
        self.main_tv_sw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        self.main_paned.add1(self.main_tv_sw)
        self.main_tv_sw.add(self.main_tv)

        #self.main_dis_model = gtk.TreeStore()
        self.main_dis = Gtk.TreeView()
        #self.main_dis.set_model(self.main_dis_model)
        self.main_dis_sw = Gtk.ScrolledWindow()
        self.main_dis_sw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        self.main_dis_sw.add(self.main_dis)
        self.main_paned.add2(self.main_dis_sw)

        self.main_dis_svc_req_model = Gtk.TreeStore(
            GObject.TYPE_STRING
        )
        self.main_dis.insert_column_with_attributes(-1, "info", Gtk.CellRendererText(), text=0)
        self.main_dis.set_headers_visible(False)
        self.main_dis.set_level_indentation(0)
        self.main_dis.connect("row-expanded", self.on_dis_row_expanded)
        self.main_dis.connect("row-collapsed", self.on_dis_row_collapsed)
        self.window.show_all()

    def set_filter(self, fn):
        m = self.filtered_models.get(fn)
        if m is None:
            self.filtered_models[fn] = m = self.main_model.filter_new()
            self.filtered_models[fn].set_visible_column(self.filters[fn])
        self.main_tv.set_model(m)

    def initial_log_scan(self):
        N = 500
        m = self.main_model
        for _ in range(N):
            frame = self.log.read_next_frame()
            if frame is None:
                # eof
                self.log.done_time = time.time()
                d = self.log.done_time - self.log.start_time
                print("ready")
                self.set_status("scanned %d packets in %.0fs (~%.0fpkts/s)" % (self.log.n_packets, d, self.log.n_packets/d))
                self.log.state = "ready"
                return False

            #self.get_short_for_offset(frame[0])
            ts = frame[1].sec + float(frame[1].nsec)/1e9 - self.log.log_start
            ft = self.frame_types[frame[1].frame_type]
            is_meta = ft in self.meta_frame_types
            is_topic = ft == "NPKT"
            is_service = ft == "SVCE"
            m.append((frame[0], self.log.n_packets, ts, ft,
                      not is_meta, not is_meta and not is_topic, not is_meta and not is_service))
            # 78s mit filter
        self.initial_count += 1
        if self.initial_count == 3:
            for col in self.main_tv.get_columns():
                w = col.get_width()
                col.set_sizing(Gtk.TreeViewColumnSizing.FIXED)
                col.set_fixed_width(int(1.1*w))
            self.main_tv.set_fixed_height_mode(True) # gtk.TREE_VIEW_COLUMN_FIXED
        if (self.initial_count % self.initial_count_divider) == 0:
            p = float(self.log.fp.tell()) / self.log.log_filesize
            est = self.log.n_packets / p
            s = "scanning... %d packets, %.0f%%, est. %d packets in file" % (self.log.n_packets, p * 100, est)
            print(s)
            self.set_status(s)
        return True

    def get_short_for_offset(self, offset):
        self.display_generation += 1
        pkt = self.short_cache.get(offset)
        if pkt is None:
            pkt = self.log.read_packet_at_offset(offset)
            pkt.short = pkt.get_short()
            self.short_cache[offset] = pkt
            pkt.display_generation = self.display_generation
            #print("self.short_cache len: %d" % len(self.short_cache))
            if len(self.short_cache) > 1000:
                lst = [(p.display_generation, o) for o, p in self.short_cache.items()]
                lst.sort()
                for i in range(500):
                    del self.short_cache[lst[i][1]]
        pkt.display_generation = self.display_generation
        return pkt.short

    def on_cursor_change(self, tv):
        path, col = tv.get_cursor()
        if path is None:
            return True
        row = tv.get_model()[path]
        offset = row[0]
        self.show_packet_at_offset(offset)
        return True

    def on_update_short(self, col, cr, m, iter):
        cr.set_property("text", self.get_short_for_offset(m[iter][0]))

    def set_status(self, msg):
        self.status.set_text(msg)

    def on_window_delete(self, win, ev):
        Gtk.main_quit()

    def run(self):
        Gtk.main()

    def on_hide_topics(self, btn):
        path, col = self.main_tv.get_cursor()
        if path:
            selected_frame = self.main_tv.get_model()[path][1] # packet number
        else:
            selected_frame = None

        if self.hide_topics_cb.get_active():
            self.hide_meta_cb.set_active(True)
        if self.hide_services_cb.get_active():
            self.hide_meta_cb.set_active(True)

        if self.hide_meta_cb.get_active() and not self.hide_topics_cb.get_active():
            self.set_filter("no_meta")
        elif self.hide_meta_cb.get_active() and self.hide_topics_cb.get_active():
            self.set_filter("no_meta_or_topic")
        elif self.hide_meta_cb.get_active() and self.hide_service_cb.get_active():
            self.set_filter("no_meta_or_service")
        else:
            self.main_tv.set_model(self.main_model)

        if selected_frame:
            # find this or next packet
            m = self.main_tv.get_model()
            for i, row in enumerate(m):
                if row[1] >= selected_frame:
                    path = (i, )
                    self.main_tv.set_cursor(path)
                    break
            else:
                print("not found")


    def show_packet_at_offset(self, offset):
        m = self.main_dis_svc_req_model
        m.clear()
        pkt = self.log.read_packet_at_offset(offset)
        if pkt.frame_type == 6: # service event
            svc = self.log.services[pkt.service_id]
            iter = m.insert(None, -1, row=("service event", ))
            m.insert(iter, -1, row=("name: %s" % svc.service_name, ))
            m.insert(iter, -1, row=("interface: %s" % svc.service_interface, ))
            m.insert(iter, -1, row=("from client: %s" % svc.client_name, ))
            m.insert(iter, -1, row=("request_id: %s/%s" % (pkt.client_id, pkt.request_id),))
            i3 = m.insert(iter, -1, row=("meta", ))
            m.insert(i3, -1, row=("peer address: %s" % (self.log.peers[pkt.peer_id].peer),))
            for field in "flags,completion_time,transfer_time,request_time,thread_id".split(","):
                m.insert(i3, -1, row=("%s: %s" % (field, getattr(pkt, field)), ))
            m.insert(i3, -1, row=("message_definition: %s" % (svc.message_definition),))
            m.insert(i3, -1, row=("msg_def_hash: %s" % (svc.msg_def_hash),))

            if pkt.item_type == 0: # request
                i2 = m.insert(iter, -1, row=("request", ))
            else:
                i2 = m.insert(iter, -1, row=("response", ))
            #i4 = m.insert(i2, -1, row=("raw_data: %r" % pkt.packet, ))
            #i4 = m.insert(i2, -1, row=("decoded:", ))
            self.decode_packet(m, i2, pkt, only_data=True)

        elif pkt.frame_type == 2: # topic packet
            topic = self.log.topics[pkt.topic_id]
            iter = m.insert(None, -1, row=("topic packet", ))
            m.insert(iter, -1, row=("name: %s" % topic.name, ))
            m.insert(iter, -1, row=("publisher: %s" % topic.publisher, ))
            m.insert(iter, -1, row=("message_definition_name: %s" % topic.md, ))
            i3 = m.insert(iter, -1, row=("meta", ))
            m.insert(i3, -1, row=("message_definition: %s" % (topic.msg_def),))
            m.insert(i3, -1, row=("msg_def_hash: %s" % (topic.msg_def_hash),))
            m.insert(i3, -1, row=("publisher_timestamp: %r" % pkt.publisher_timestamp,))
            hdr_ts = pkt.header.sec + float(pkt.header.nsec) / 1e9
            publisher_to_pcap_ts = hdr_ts - pkt.publisher_timestamp
            m.insert(i3, -1, row=("time from publisher-ts to log-ts: %.3fms" % (publisher_to_pcap_ts * 1e3),))

            i2 = m.insert(iter, -1, row=("data", ))
            self.decode_packet(m, i2, pkt, only_data=True)
        self.main_dis.set_model(m)
        #self.main_dis.expand_all()
        def check_expand_children(parent, this_path):
            iter = m.iter_children(parent)
            while iter:
                if m.iter_has_child(iter):
                    row = m[iter]
                    p = list(this_path)
                    p.append(row[0])
                    if tuple(p) not in self.collapsed_rows:
                        self.main_dis.expand_row(m.get_path(iter), False)
                        check_expand_children(iter, p)
                iter = m.iter_next(iter)
        check_expand_children(None, [])
    def decode_packet(self, model, parent, pkt, only_data=False):
        m = model
        decoded = self.log.decode_packet(pkt)
        if only_data:
            decoded = decoded["data"]
        def add_kv(parent, key, value):
            if isinstance(value, list):
                new_parent = m.insert(parent, -1, row=("%s: (list of %d)" % (key, len(value)), ))
                fill_list(new_parent, value)
                return
            if not isinstance(value, odict):
                m.insert(parent, -1, row=("%s: %r" % (key, value), ))
                return
            new_parent = m.insert(parent, -1, row=("%s:" % key, ))
            fill_tree(new_parent, value)
        def fill_tree(parent, decoded):
            for key, value in decoded.items():
                add_kv(parent, key, value)
        def fill_list(parent, decoded):
            for i, value in enumerate(decoded):
                add_kv(parent, "[%d]" % i, value)
        fill_tree(parent, decoded)

    def get_text_path(self, m, iter):
        path = []
        while iter and m.iter_is_valid(iter):
            path.insert(0, m[iter][0])
            iter = m.iter_parent(iter)
        return tuple(path)

    def on_dis_row_collapsed(self, tv, iter, path):
        path = self.get_text_path(tv.get_model(), iter)
        self.collapsed_rows.add(tuple(path))

    def on_dis_row_expanded(self, tv, iter, path):
        path = self.get_text_path(tv.get_model(), iter)
        if path in self.collapsed_rows:
            self.collapsed_rows.remove(path)
