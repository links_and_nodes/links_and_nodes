import sys
import struct

is_py2 = sys.version_info[0] < 3

def is_binary_data(obj):
    if is_py2:
        return isinstance(obj, str)
    return isinstance(obj, bytes)

class packet(object):
    def __init__(self, parent, data):
        self.parent = parent
        self.data = data
        
    def get_short(self):
        return "<unknown frame_type, data: %r>" % (self.data)

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self.get_short())
    
class header_packet(packet):
    frame_type = 0
    def __init__(self, parent, data):
        self.frame_type = self.__class__.frame_type
        if not is_binary_data(data):
            self.version = data
            data = struct.pack(parent.endianess + "I", self.version)
        else:
            self.version = struct.unpack(parent.endianess + "I", data)[0]
        packet.__init__(self, parent, data)

    def get_short(self):
        return "header version %#x" % (
            self.version)

class new_topic_packet(packet):
    frame_type = 1
    def __init__(self, parent, data, file_version):
        self.frame_type = self.__class__.frame_type
        
        if type(data) == tuple:
            # init from tuple
            if file_version < 2:
                self.topic_id, self.message_size, self.name, self.md, self.msg_def, self.publisher = data
            else:
                self.topic_id, self.message_size, self.name, self.md, self.msg_def, self.publisher, self.msg_def_hash = data
            data = struct.pack(parent.endianess + "II", self.topic_id, self.message_size)
            if file_version < 2:
                data += parent.write_fields(self.name, self.md, self.msg_def, self.publisher)
            else:
                data += parent.write_fields(self.name, self.md, self.msg_def, self.publisher, self.msg_def_hash)
        else:
            # init from binary data
            self.topic_id, self.message_size = struct.unpack(parent.endianess + "II", data[:8])
            if file_version < 2:
                self.name, self.md, self.msg_def, self.publisher = parent.read_fields(data, 8, 4)
                self.msg_def_hash = "" # unknown
            else:
                self.name, self.md, self.msg_def, self.publisher, self.msg_def_hash = parent.read_fields(data, 8, 5)
        packet.__init__(self, parent, data)

    def get_short(self):
        return "topic_id %d, %s, message_definition: %s, %d byte" % (
            self.topic_id,
            self.name, self.md, self.message_size)
    
class new_tpacket_packet(packet):
    frame_type = 2
    def __init__(self, parent, data, file_version):
        self.frame_type = self.__class__.frame_type
        if type(data) == tuple:
            # init from tuple
            self.topic_id, self.packet, self.publisher_timestamp = data
            data = struct.pack(parent.endianess + "I", self.topic_id) + self.packet
            if file_version >= 2:
                data += struct.pack(parent.endianess + "d", self.publisher_timestamp)
        else:
            # init from binary data
            self.topic_id = struct.unpack(parent.endianess + "I", data[:4])[0]
            if file_version < 2:
                self.packet = data[4:]
                self.publisher_timestamp = None # will be filled in lnrecorder_log.get_packet() from pcap header ts
            else:
                self.packet = data[4:-4]
                self.publisher_timestamp = struct.unpack(parent.endianess + "d", data[-8:])[0]
        packet.__init__(self, parent, data)

    def get_short(self):
        return "topic %s, %d byte" % (
            self.parent.topics[self.topic_id].name,
            len(self.packet))
    
class del_topic_packet(packet):
    def __init__(self, parent, data):
        packet.__init__(self, parent, data)
        self.topic_id = struct.unpack(self.parent.endianess + "I", data[:4])[0]

    def get_short(self):
        return "topic_id %d" % (
            self.topic_id)
    
class new_service_peer_packet(packet):
    def __init__(self, parent, data):
        packet.__init__(self, parent, data)
        self.peer = self.parent.read_fields(data)
        self.peer_id = struct.unpack(self.parent.endianess + "I", data[4 + len(self.peer):])[0]

    def get_short(self):
        return "peer_id %d: %s" % (
            self.peer_id,
            self.peer)

class new_service_packet(packet):
    def __init__(self, parent, data):
        packet.__init__(self, parent, data)
        self.client_name, self.service_name, self.service_interface, self.message_definition, new_offset = self.parent.read_fields(data, n_fields=4, return_new_offset=True)
        self.service_id = struct.unpack(self.parent.endianess + "I", data[new_offset:new_offset+4])[0]
        if self.parent.file_version >= 2:
            self.msg_def_hash = self.parent.read_fields(data, data_offset=new_offset + 4)
        else:
            self.msg_def_hash = None

    def get_short(self):
        return "sid %d: %s, interface: %s, client: %s" % (
            self.service_id,
            self.service_name, self.service_interface,
            self.client_name)

class service_event_packet(packet):
    fmt = "IBBdddIIIQ"
    fixed_len = struct.calcsize("=" + fmt)
    def __init__(self, parent, data):
        packet.__init__(self, parent, data)
        (self.service_id, self.item_type, self.flags, self.completion_time, self.transfer_time, self.request_time,
         self.peer_id, self.client_id, self.request_id, self.thread_id) = struct.unpack(
             self.parent.endianess + service_event_packet.fmt, data[:service_event_packet.fixed_len])
        self.packet = self.parent.read_fields(data, service_event_packet.fixed_len)

    def get_short(self):
        service = self.parent.services[self.service_id]
        if self.item_type == 0:
            return "%s.call(%r) -> %d/%d" % (
                service.client_name,
                service.service_name,
                self.client_id, self.request_id,
                )
        return "%s.respond(%r, %d/%d)" % (
            service.client_name,
            service.service_name,
            self.client_id, self.request_id
            )
