"""
    Copyright 2022 DLR e.V., Florian Schmidt

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import print_function

import traceback

class MultiWaiterInterface(object):
    def __init__(self, clnt):
        self.clnt = clnt
        self._subscribed_topics = {} # topic_id -> (port, [ (subscription-id, cb, cb-args) ] )
        self._next_subscription_id = 1
        self._subscription_ids = {} # subscription-id -> topic_id

    def subscribe_with_callback(self, topic_name, message_definition_name=None, rate=-1, buffers=2, callback=None, callback_args=tuple()):
        """
        subscribe given LN topic, if not already subscribed.
        add a new callback to be called whenever a new message arrives.

        returns a `subscription_id` which can be used to unsubscribe.
        """

        topic_id = topic_name, rate, buffers

        port_callbacks = self._subscribed_topics.get(topic_id)
        if port_callbacks is None:
            callbacks = []
            port = self.clnt.subscribe(topic_name, message_definition_name, rate, buffers)
            self._subscribed_topics[topic_id] = port, callbacks
        else:
            port, callbacks = port_callbacks

        subscription_id = self._next_subscription_id
        self._next_subscription_id += 1
        self._subscription_ids[subscription_id] = topic_id
        if not isinstance(callback_args, tuple):
            callback_args = (callback_args, )
        callbacks.append((subscription_id, callback, callback_args))
        _deinit_functions = callback.__globals__.get("_deinit_functions")
        if _deinit_functions is not None:
            _deinit_functions.append(lambda: self.unsubscribe_callback(subscription_id))

        if port_callbacks is None:
            self.add_port(port, self._subscription_callback, topic_id)

        return subscription_id

    def unsubscribe_callback(self, subscription_id):
        """
        unsubscribe a callback from a fiven LN-topic
        needs subscription_id returned from `subscribe_with_callback()`
        """
        topic_id = self._subscription_ids.get(subscription_id)
        if topic_id is None:
            return
        del self._subscription_ids[subscription_id]
        topic_name, rate, buffers = topic_id
        port, callbacks = self._subscribed_topics[topic_id]
        for i, (sub_id, cb, cb_args) in enumerate(callbacks): # noqa: B007
            if sub_id == subscription_id:
                del callbacks[i]
                if not callbacks:
                    self.remove_port(port)
                    self.clnt.unsubscribe(port)
                    del self._subscribed_topics[topic_id]
                return

    def _subscription_callback(self, port, topic_id):
        pkt = port.packet
        to_del = []
        for sub_id, cb, cb_args in self._subscribed_topics[topic_id][1]:
            try:
                ret = cb(pkt, *cb_args)
            except Exception:
                print("exception in subscribe_with_callback(%r, callback=%r(%s):\n%s" % (
                    topic_id[0], str(cb), repr(cb_args)[1:-1], traceback.format_exc()))
                ret = False
            if not ret:
                to_del.append(sub_id)
        for sub_id in to_del:
            self.unsubscribe_callback(sub_id)
        return True
    
