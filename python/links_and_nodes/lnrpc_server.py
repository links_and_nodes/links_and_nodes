#!/usr/bin/env python3
"""
    Copyright 2013-2015 DLR e.V., Daniel Leidner, Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import print_function

import os
import traceback

import links_and_nodes
import links_and_nodes_base

from links_and_nodes_manager import py2compat

class lnrpc_server():
    def __init__(self, name, messages_path, *args, use_gtk=False, mainloop=None):
        self.mainloop = mainloop
        if self.mainloop is None and use_gtk:
            self.mainloop = links_and_nodes_base.GLibMainloop()

        self.services = {}
        self.clnt = links_and_nodes.client("%s Server" % name, *args)
        for m in os.listdir(messages_path):
            if not m.startswith("."):
                svc = self.clnt.get_service_provider("%s.%s" % (name, m), "%s/%s" % (messages_path, m))
                svc.set_handler(self._process_message)
                if self.mainloop:
                    svc.do_register_with_mainloop(self.mainloop)
                else:
                    svc.do_register()
                self.services[m] = svc          
        # add raw interface
        svc = self.clnt.get_service_provider("%s.pyservice" % name, "ln2/pyservice")
        svc.set_handler(self._process_message)
        if self.mainloop:
            svc.do_register_with_mainloop(self.mainloop)
        else:
            svc.do_register()
        self.services["pyservice"] = svc
        self.keep_running = True

    def _process_message(self, svc, req, resp):
        resp.exception = ""
        resp.result = None
        method_name = svc.name.rsplit(".")[-1]  
        if method_name == "pyservice":
            method = getattr(self, req.method_name, None)
            if method is None:
                resp.exception = "Method %r does not exist." % req.method_name
                svc.respond()
                return
            args = req.args
            kwargs = req.kwargs
        else:
            args = []
            kwargs = {}
            method = getattr(self, method_name, None)  
            if method is None:
                resp.exception = "No such method %s" % method_name
                svc.respond()
                return
            for key in py2compat.inspect_getfullargspec(method).args:
                val = getattr(req, key, "") # think about 15, [], 
                if val != "":
                    kwargs[key] = val

        # calls the server method
        try:
            result = method(*args, **kwargs)
            if result is not None:
                resp.result = result
        except Exception:
            ex = traceback.format_exc()   
            resp.exception = ex
            print(ex)
        svc.respond()

    def run(self):
        # Waiting for service requests   
        if self.mainloop:
            return self.mainloop.run()
        while self.keep_running:
            self.clnt.wait_and_handle_service_group_requests(None, 1)
