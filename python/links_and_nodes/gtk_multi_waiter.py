"""
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import print_function

import traceback

class gtk_multi_waiter(object):
    def __init__(self, client):
        import glib
        self.clnt = client
        self.waiter = self.clnt.get_multi_waiter()
        self.ports = []
        self.fd = self.waiter.start_pipe_notifier_thread()
        self.watch = glib.io_add_watch(self.fd, glib.IO_IN, self.on_packet)
        self.to_remove = []

    def add_port(self, port, cb, *cb_args):
        for i, port_entry in enumerate(self.ports):
            if port_entry[0] == port:
                break
        else:
            self.waiter.add_port(port)
            self.ports.append([port, (cb, cb_args), False])
            return
        # only change callback
        self.ports[i][1] = (cb, cb_args)

    def remove_port(self, port):
        for i, port_entry in enumerate(self.ports):
            if port_entry[0] == port:
                break
        else:
            raise Exception("port %r not found!" % port.port)
        port, cb, can_read = port_entry
        self.waiter.remove_port(port)
        del self.ports[i]

    def on_packet(self, fd, why):
        for i, port_entry in enumerate(self.ports):
            if port_entry[0].port == 0:
                port_entry[2] = False
                self.to_remove.append(i)
                continue
            if self.waiter.can_read(port_entry[0]):
                port_entry[2] = True
        self.waiter.ack_pipe_notification()
        for i, port_entry in enumerate(self.ports):
            if not port_entry[2]:
                continue
            port, cb = port_entry[:2]
            port_entry[2] = False
            try:
                ret = cb[0](port, *cb[1])
            except Exception:
                if cb[0] is not None:
                    cb_name = cb[0].__name__
                else:
                    cb_name = "None"
                print("in gtk_multi_waiter cb %r for port %r:\n%s" % (cb_name, port.port, traceback.format_exc()))
                ret = False
            if not ret:
                self.to_remove.append(i)
        if self.to_remove:
            self.to_remove.reverse()
            for i in self.to_remove:
                port = self.ports[i][0]
                del self.ports[i]
                if port.port != 0:
                    self.waiter.remove_port(port)
            del self.to_remove[0:]
        return True
