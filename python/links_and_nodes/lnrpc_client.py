#!/usr/bin/env python
"""
    Copyright 2013-2015 DLR e.V., Daniel Leidner, Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import print_function

import sys
import os
import copy
import traceback
import links_and_nodes

class lnrpc_client():
    def __init__(self, name, messages_path, *args, **kwargs):
        self.catch_exceptions = kwargs.get("catch_exceptions", False)
        self.silent_exceptions = kwargs.get("silent_exceptions", False)
        self.name = name
        if "clnt" in kwargs:
            self.clnt = kwargs["clnt"]
        else:
            self.clnt = links_and_nodes.client("%s Client" % name, *args)
        self.services = {}               
        for m in os.listdir(messages_path):
            if not m.startswith("."):
                self.services[m] = self.clnt.get_service("%s.%s" %(name, m), "%s/%s" %(messages_path, m)) 
        self.services["pyservice"] = self.clnt.get_service("%s.pyservice" %(name), "ln2/pyservice") 

    def __repr__(self):
        return "<lnrcp_client %r>" % self.name
    def __str__(self):
        return self.__repr__()
        
    def __getattr__(self, method_name):
        def wrapper(*args, **kwargs): 
            if method_name not in self.services:
                svc = self.services["pyservice"]
                svc.req.method_name = method_name
                svc.req.args = args 
                svc.req.kwargs = kwargs              
            else:
                args = list(args)
                svc = self.services[method_name]
                if len(args):
                    for key in svc.req.keys():
                        if key.endswith("_len") and getattr(svc.req, key.split("_len")[0], None) is not None:
                            continue
                        setattr(svc.req, key, args.pop(0))
                        if not len(args):
                            break
                for key, value in kwargs.items():
                    setattr(svc.req, key, value)
            try:
                svc.call()
            except Exception:
                if self.catch_exceptions:
                    if self.silent_exceptions:
                        short = str(sys.exc_value)
                        search = "error Exception: "
                        if search in short:
                            short = short.split(search, 1)[1].split("\n", 1)[0]
                        print("lnrpc_client %s.%s caught exception %s returning None." % (
                            self.name, method_name, short))
                    else:
                        print("lnrpc_client %s.%s caught exception:\n%s\nreturning None" % (
                            self.name, method_name, traceback.format_exc()))
                    return None
                raise # rethrow
            if svc.resp.exception != "":
                if not self.catch_exceptions:
                    raise Exception(svc.resp.exception)
                print("lnrpc_client %s.%s did not throw exception:\n%s\nreturning None" % (
                    self.name, method_name, svc.resp.exception))
                return None
            return copy.copy(getattr(svc.resp, 'result', None)) #preferably primitive, possibly pyobject, or nothing in msgdef
        return wrapper     
