"""
file services client python wrapper.
"""

import links_and_nodes as ln

class file_services_client(ln.services_wrapper):
    def __init__(self, clnt, node):
        ln.services_wrapper.__init__(self, clnt, node)

        self.wrap_service("read_from_file", "ln/file_services2/read_from_file")
        self.wrap_service("write_file", "ln/file_services2/write_file")
        self.wrap_service("get_tree_info", "ln/file_services2/get_tree_info2")
        self.wrap_service("chown", "ln/file_services/chown")
        self.wrap_service("clock_settime", "ln/file_services/clock_settime")
