"""
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import print_function

from links_and_nodes_base import ln_tree, ln_release_version, ln_libdir, ln_manager_dir, ln_share_dir, builtin_msg_defs, runtime_msg_defs, ln_debug, package_base, util, lnm_remote, data_type_map, search_message_definition, message_definition_header, message_definition, rescan_message_definition_dirs, compute_message_definition_size, MainloopInterface, GLibMainloop, get_message_definition_from_filename, compute_message_definition_string # noqa: F401
from links_and_nodes_base import message_definitions # for backward compatibility!
import os
import sys
import sysconfig
platform_version = "%s-%s.%s" % (sysconfig.get_platform(), sys.version_info[0], sys.version_info[1])
if ln_tree == "source":
    python_ext_dir = os.path.join(os.path.dirname(__file__), "src-" + platform_version)
else:
    python_ext_dir = os.path.join(os.path.dirname(__file__), platform_version)
if ln_debug:
    print("links_and_nodes: adding extension dir %r" % python_ext_dir)

try:
    if not os.path.isdir(python_ext_dir):
        raise Exception("does not exist!")
    if not os.access(python_ext_dir, os.R_OK | os.X_OK):
        raise Exception("is not readable!")
    _ln_so = os.path.join(python_ext_dir, "_ln.so")
    if ln_debug and not os.path.isfile(_ln_so):
        raise Exception("_ln.so is missing!")
except Exception as e:
    sys.stderr.write("WARNING: computed python ext dir %r %s did you install LN for this python%s.%s interpreter?\n" % (
        python_ext_dir, str(e), sys.version_info[0], sys.version_info[1]))

sys.path.insert(0, python_ext_dir)
from . import ln_wrappers # noqa: E402, F401

from _ln import (  # noqa: E402, F401 ## check your LD_LIBRARY_PATH if you get `ImportError: libln.so.1` here (or rerun with env-var LN_DEBUG=y set)
    LN_LIBRARY_VERSION,
    LN_MANAGER_CLIENT_ID,
    LNE_SVC_HANDLER_EXC,
    LNE_SVC_RECV_REQ,
    NO_DAEMON_AUTH,
    RATE_AS_PUBLISHED,
    RATE_ON_REQUEST_LAST,
    client,
    construct_client_from_pointer,
    copy2array,
    copy2buffer,
    dsa_dss1_sign_message,
    inport,
    logger,
    logger_data,
    logger_topic_,
    multi_waiter,
    outport,
    service,
    service_fill_only,
    service_request,
    service_unpack_only,
    topic_cursor,
)


def _read_path_var(name):
    value = os.getenv(name)
    if value:
        return value.split(os.path.pathsep)
    return []
def _search_lib(libname, add_to):
    for dir in _read_path_var("LD_LIBRARY_PATH") + [ ln_libdir ]:
        p = os.path.join(dir, libname)
        if os.path.isfile(p):
            if dir not in add_to:
                add_to.append(dir)
            return dir

package_base_api = os.path.realpath(os.path.dirname(os.path.dirname(__file__)))

ld_library_path = [ ln_libdir ] # assumes libln (& libboost_python, ...) is installed to same prefix as links_and_nodes_base!
_search_lib("libln.so", ld_library_path)
_search_lib("libboost_python.so", ld_library_path)
# conan users will have to use other means to provide a valid LD_LIBRARY_PATH!
# (e.g. by deriving their processes from cissy_workspace generated templates,
# or by specifying "pass_environment: LD_LIBRARY_PATH" ...)

pythonpath = list(set([package_base_api, package_base]))

from links_and_nodes.ln_wrappers import ln_packet, port_wrapper, request_wrapper, service_wrapper, topic_cursor_wrapper, logger_data_wrapper, parameters_proxy, event_connection_wrapper, wrapped_service, services_wrapper, wrapped_service_provider, service_provider, ServiceErrorResponse, get_packet_ctor_name # noqa: E402, F401

def gtk_multi_waiter(*args, **kwargs):
    import links_and_nodes.gtk_multi_waiter
    return links_and_nodes.gtk_multi_waiter.gtk_multi_waiter(*args, **kwargs)

from .mainloop_multi_waiter import MainloopMultiWaiter # noqa: E402, F401
