"""
    Copyright 2012 DLR e.V., Florian Schmidt

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import print_function

import traceback
import links_and_nodes as ln
from .multi_waiter_interface import MultiWaiterInterface


class MainloopMultiWaiter(MultiWaiterInterface):
    def __init__(self, client, mainloop):
        """
        client: instance of ln.client() class
        mainloop: mainloop implementing ln.MainloopInterface

        this will consume the published packet!
        do not do a blocking read in your callbacks!
        """
        assert isinstance(mainloop, ln.MainloopInterface)

        MultiWaiterInterface.__init__(self, client)
        self.mainloop = mainloop
        self.waiter = self.clnt.get_multi_waiter()
        self._ports = [] # (port, (cb, cb-args))
        self._fd = self.waiter.start_pipe_notifier_thread()
        self._watch = None

    def add_port(self, port, cb, *cb_args):
        """
        add a new ln-topic-port to wait for new packets on.
        whenevery there is a new packet cb(port, *cb_args) will be called.
        in your callback you don't need to do `port.read()` this is already done in here.

        if your callback returns non-True your callback and the corresponding port will be removed!
        """
        for i, port_entry in enumerate(self._ports):
            if port_entry[0] == port:
                # only change callback
                self._ports[i][1] = (cb, cb_args)
                return

        self.waiter.add_port(port)
        self._ports.append([port, (cb, cb_args)])
        if self._watch is None:
            self._watch = self.mainloop.fd_add(self._fd, "in", self._on_packet)

    def remove_port(self, this_port):
        for i, (port, _) in enumerate(self._ports):
            if port == this_port:
                self.waiter.remove_port(port)
                del self._ports[i]
                return
        raise Exception("port %r not found!" % this_port.port)

    def stop(self):
        """
        remove & unregister all ports
        """
        for port, _ in self._ports:
            if port.port != 0:
                self.waiter.remove_port(port)
        self._ports = []
        if self._watch is not None:
            self.mainloop.source_remove(self._watch)
            self._watch = None

    # private impl:
    def _on_packet(self):
        to_remove = []
        for i, (port, cb) in enumerate(self._ports):
            if port.port == 0:
                to_remove.append(i)
                continue
            if not self.waiter.can_read(port):
                continue
            port.read()
            try:
                ret = cb[0](port, *cb[1])
            except Exception:
                if cb[0] is not None:
                    cb_name = cb[0].__name__
                else:
                    cb_name = "None"
                print("in MainloopMultiWaiter cb %r for port %r:\n%s" % (cb_name, port.port, traceback.format_exc()))
                ret = False
            if not ret:
                to_remove.append(i)
        
        if to_remove:
            to_remove.reverse()
            for i in to_remove:
                port = self._ports[i][0]
                del self._ports[i]
                if port.port != 0:
                    self.waiter.remove_port(port)
        
        self.waiter.ack_pipe_notification()
        
        return True

