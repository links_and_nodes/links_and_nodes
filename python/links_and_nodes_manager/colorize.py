import colorsys

import numpy as np

__all__ = [ "rgba_colorize" ]


def rgba_colorize(rgba, target_rgb):
    """
    rgba is (height, width) x 4-tuple of (red, green, blue, alpha) 0..255
    target_rgb is 3-tuple of 0..255 red, green, blue
    """
    def to_hsv(v):
        return colorsys.rgb_to_hsv(*v[:3])
    def to_rgb(v):
        return colorsys.hsv_to_rgb(*v)
    hsv = np.apply_along_axis(to_hsv, axis=-1, arr=rgba / 255.)
    hsv[:, :, :2] = colorsys.rgb_to_hsv(*(np.array(target_rgb, dtype=float) / 255))[:2]
    rgb_out = np.empty((hsv.shape[0], hsv.shape[1], 4), dtype=np.uint8)
    rgb_out[:, :, 3] = rgba[:, :, 3]
    rgb_out[:, :, :3] = np.array(np.apply_along_axis(to_rgb, axis=-1, arr=hsv) * 255, dtype=np.uint8)
    return rgb_out
