"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

import os
import datetime

from . import config

from .Logging import enable_logging_on_instance

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Pango', '1.0')
gi.require_version('GLib', '2.0')
from gi.repository import Gtk, Gdk, Pango, GLib # noqa: E402

class process_output(object):
    def __init__(self, parent, p, position=None):
        self.parent = parent
        self.p = p
        enable_logging_on_instance(self, "PO %s" % p.name)

        self.xml = Gtk.Builder()
        resources_dir = os.path.join(config.manager_base_dir, "resources")
        self.xml_fn = os.path.join(resources_dir, "process_output_window.ui")
        self.xml.add_from_file(self.xml_fn)
        self.xml.connect_signals(self)
        self.xml.add_objects_from_file(self.parent.xml_fn, ["log_tv_popup"])
        self.xml.connect_signals(self)
        self.log_tv_popup = self.xml.get_object("log_tv_popup")

        self.frozen = False
        self.freeze_btn.set_active(False)

        if self.p.use_vte:
            self._get_vte()
        else:
            self.vte = None
            self.po_log_tv.modify_font(self.parent.font_description)
        b = self.po_log_tv.get_buffer()
        b.create_tag("red", foreground="red", weight=Pango.Weight.BOLD)

        self.update_process_command()
        self._sb_adj = None
        self.set_process_log()

        self.ignore_buttons = False
        self.update_process()
        self.hidden = False
        w = self.process_output_window
        self._last_pos = None
        w.set_title("%s@%s" % (p.name, p.host))
        if position:
            window_size, pos = position
            w.resize(*window_size)
        w.show()
        if position:
            w.move(*pos)
        w.connect("configure-event", self.on_resize)
        w.connect("visibility-notify-event", self.on_visibility)
        self.fully_visible = True

    def update_process_command(self):
        if not hasattr(self.p, "command"):
            # its a state
            self.po_cmd_entry.set_text("check: %r, up: %r, down: %r" % (
                self.p.check, self.p.up, self.p.down))
        else:
            if self.p.command is None:
                cmd = "<no command specified>"
            else:
                cmd = self.p.command
            self.po_cmd_entry.set_text(cmd)
        
    def on_visibility(self, window, ev):
        self.fully_visible = ev.state == Gdk.VisibilityState.UNOBSCURED
        return True
        
    def _get_vte(self):
        from gi.repository import Vte
        self.vte = Vte.Terminal()
        self.parent._init_vte(self.vte, self.p, self.on_po_log_tv_button_press_event)
        self.po_vbox.remove(self.po_log_sw)
        self.po_vbox.pack_start(self.vte, True, True, 0)
        self.vte.show()
        self.vte.set_size_request(100, 100)

    def __getattr__(self, name):
        if hasattr(self, "xml"):
            widget = self.xml.get_object(name)
            if widget is not None:
                return widget
        raise AttributeError(name)

    def on_resize(self, widget, ev):
        s = widget.get_size()
        window_size = s.width, s.height
        p = widget.get_position()
        position = p.root_x, p.root_y
        new_pos = window_size, position
        if new_pos != self._last_pos:
            self._last_pos = new_pos
            self.parent.store_process_output_position(self.p.name, new_pos)

    def present(self, only_if_not_fully_visible=False):
        if self.hidden:
            widget = self.process_output_window
            window_size, position = self._last_pos
            widget.resize(*window_size)

            if self.p.use_vte and self.vte is None:
                self._get_vte()

            self.process_output_window.show()
            widget.move(*position)
            # maybe restore output text

        self.hidden = False
        if only_if_not_fully_visible and self.fully_visible:
            return
        self.process_output_window.present()

    def hide(self):
        self.hidden = True
        self.process_output_window.hide()


    def on_process_output_window_delete_event(self, *args):
        if self.parent.current_p == self.p:
            self.parent.show_po_btn.set_active(False)
        else:
            self.hide()
        return True

    def process_started(self):
        if self.p.use_vte:
            if self.vte is None:
                self._get_vte()
            self.vte.reset(True, True)
        return True

    def set_process_log(self):
        p = self.p
        unicode_text = self.parent.try_get_unicode_output(p, p.output)
        if self.p.use_vte:
            self.vte.reset(True, True)
            self.vte.feed(unicode_text.encode("utf-8"))
        else:
            b = self.po_log_tv.get_buffer()
            b.set_text(unicode_text)
        self._update_process_log_list = []
        self._update_process_log_scrollbar()

    def _update_process_log_scrollbar(self):
        if self.p.use_vte:
            return
        if not self.po_follow_btn.get_active():
            return
        def do_adjustment():
            adj = self.po_log_sw.get_vadjustment()
            adj.set_value(adj.get_upper() - adj.get_page_size())
            self._sb_adj = None
            return False
        if self._sb_adj is None:
            self._sb_adj = GLib.idle_add(do_adjustment)

    def update_process_log(self, p, to_add, to_remove):
        """
        `to_add` is encoded bytes-string from process
        """
        if self.frozen:
            return
        if self.p.use_vte:
            unicode_text = self.parent.try_get_unicode_output(p, to_add)
            self.vte.feed(unicode_text.encode("utf-8"))
            return
        b = self.po_log_tv.get_buffer()
        end = b.get_end_iter()
        if not hasattr(self, "_in_exception"):
            self._in_exception = False
        if len(to_add) and to_add[-1] == "\n":
            line_list = to_add[:-1].split("\n")
        else:
            line_list = to_add.split("\n")
        # assume always one trailing newline in to_add!
        for line in line_list:
            #self.info(" line to add to buffer: %r" % line)
            if not self._in_exception and "Traceback (most recent call last):" in line:
                self._in_exception = True
                self._in_exception_start = line.find("Traceback")
            elif self._in_exception and line[self._in_exception_start:].strip() == "":
                self._in_exception = False                
            line_with_newline = line + "\n"
            line_with_newline = self.parent.try_get_unicode_output(p, line_with_newline)

            if self._in_exception:
                tags = ["red"]
                #if line.lstrip().startswith("File"):
                #    tags.append("underlined")
                b.insert_with_tags_by_name(end, line_with_newline, *tags)
            else:
                b.insert(end, line_with_newline)
        if to_remove:
            b.delete(b.get_start_iter(), b.get_iter_at_offset(to_remove))
        self._update_process_log_scrollbar()
    
    def on_po_show_in_mgr_btn_clicked(self, btn):
        self.parent._select_process(self.p)
        self.parent.gui.select_page("processes")
        self.parent.gui.dialog.present()
        self.parent.gui.switch_to_page("processes")
        
    def on_po_log_tv_button_press_event(self, tv, ev):
        if ev.button == 3:
            self.log_tv_popup.popup(None, None, None, None, ev.button, ev.time)
            return True
        return False

    def on_log_tv_activate(self, item):
        #action = item.get_name()
        action = Gtk.Buildable.get_name(item)
        if action == "log_tv_clear":
            self.p.output = b""
            self.p.output_lines = 0
            self.p._in_exception = False
            self._in_exception = False
            if self.parent.current_p == self.p:
                self.parent.set_process_log(self.p)
            self.set_process_log()
        if action == "log_tv_append_sep":
            self.parent._append_separator(self.p)

    def on_freeze_btn_toggled(self, btn):
        self.frozen = btn.get_active()
        if self.frozen:
            self.p.frozen_output = self.p.output
        else:
            self.set_process_log()

    def update_process(self):
        self.ignore_buttons = True
        if self.p.requested_state == "stop":
            self.stop_btn.set_active(True)
            self.start_btn.set_active(False)
        elif self.p.requested_state == "start":
            self.stop_btn.set_active(False)
            self.start_btn.set_active(True)
        else:
            self.stop_btn.set_active(False)
            self.start_btn.set_active(False)
        self.ignore_buttons = False

        self.stop_btn.set_sensitive(self.p.state != "stopped")
        
        if not self.p.state:
            self.state_label.set_text("")
        else:
            self.state_label.set_text(self.p.state)
        
        started_or_ready = self.p.state in ("started", "ready")
        if started_or_ready:
            self.start_img.set_from_icon_name("gtk-refresh", Gtk.IconSize.BUTTON)
        else:
            self.start_img.set_from_icon_name("gtk-yes", Gtk.IconSize.BUTTON)

        self.show_gui_btn.set_sensitive(self.parent.has_present_window(self.p))
        self.show_gui_btn.set_property("visible", self.parent.has_present_window(self.p))

    def on_start_btn_pressed(self, btn, ev):
        self.start_btn_shift_pressed = bool(ev.state & Gdk.ModifierType.SHIFT_MASK)
        return False
    
    def on_start_btn_toggled(self, btn):
        if self.freeze_btn.get_active():
            self.freeze_btn.set_active(False)        
        if self.ignore_buttons:
            return True
        active = btn.get_active()
        if active:
            self.ignore_buttons = True
            self.stop_btn.set_active(False)
            self.ignore_buttons = False
            self.parent._request_state(self.p, "start", disable_gdb_run=self.start_btn_shift_pressed)
            self.update_process()
        else:
            self.parent._request_state(self.p, None)
        self.vte.grab_focus()
        return True

    def on_stop_btn_toggled(self, btn):
        if self.ignore_buttons:
            return True
        active = btn.get_active()
        if active:
            self.ignore_buttons = True
            self.start_btn.set_active(False)
            self.ignore_buttons = False
            self.parent._request_state(self.p, "stop")
        else:
            self.parent._request_state(self.p, None)
        self.vte.grab_focus()
        return True

    def on_show_gui_btn_clicked(self, btn):
        self.parent.on_show_gui_btn_clicked(btn, process=self.p)
