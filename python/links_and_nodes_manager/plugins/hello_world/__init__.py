# -*- mode: python -*-

from __future__ import print_function

import datetime

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
from gi.repository import Gtk, GLib # noqa: E402

class gui_plugin(object):
    def __init__(self, manager_gui, config):
        self.gui = manager_gui
        self.config = config
        
        print("this is the hello world gui plugin!")
        print("my config file entries are:")
        
        self.my_tab_name = "default tab name"
        self.my_content = "empty"
        
        for key, value in self.config.key_values:
            print((key, value))
            setattr(self, key, value)

        self.my_label = Gtk.Label(self.my_content)
        self.gui.add_notebook_page(self.my_tab_name, self.my_label)

        self.timer_id = GLib.timeout_add(250, self.on_update)

    def on_update(self):
        now = datetime.datetime.now()
        self.my_label.set_text("%s\n%s" % (self.my_content, now))

        return True # keep timer running
