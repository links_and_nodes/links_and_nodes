"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

import hashlib

from .Logging import enable_logging_on_instance
from .CommunicationUtils import ignore_cb

import links_and_nodes_base

import gi
gi.require_version('GLib', '2.0')
from gi.repository import GLib # noqa: E402

network_byte_order = "big"

class Topic(object):
    # pickling:
    to_pickle = "name,md_name,message_size,settings,state,publisher,source_port,subscribers"
    def __getstate__(self):
        state = {}
        for attr in self.to_pickle.split(","):
            attr = attr.strip()
            state[attr] = getattr(self, attr)
        return state
    def __setstate__(self, state):
        self.__dict__.update(state)
        self._init()
    def _init(self):
        enable_logging_on_instance(self, "topic:%s" % self.name)

    def __eq__(self, other):
        if other is None:
            return False
        return self.name == other.name

    def __init__(self, manager, name, md_name, settings=None):
        self.manager = manager
        self.name = name
        self._init()
        
        self.md_name = md_name
        self.md_cache = {}
        self.md_fn = links_and_nodes_base.search_message_definition(self.md_name)
        self.md = links_and_nodes_base.message_definition(self.md_name, self.md_fn)
        self.message_size = self.md.calculate_size()
        self.settings = settings

        self.info("new topic %r, msg_def %r,  size %d bytes" % (
                name, md_name, self.message_size))
        self.state = "init"

        self.publisher = None
        self.source_port = None

        self.subscribers = [] # topic_subscriptions

    def get_preferred_network_between(self, host0, host1):
        # todo: per topic setting
        common = host0.get_common_networks(host1)
        #print "common networks: %r" % common
        if self.settings is not None:
            net0 = self.settings.scheduling_on.get(host0, {}).get("preferred_network")        
            net1 = self.settings.scheduling_on.get(host1, {}).get("preferred_network")

            if net0 and net1 and net0 != net1:
                self.warning("host %s and %s have different preferred networks for topic %s:\n%s\n%s" % (
                    host0, host1, self.name,
                    net0,
                    net1))

            if net0 in common:
                return net0

            if net1 in common:
                return net1
        
        # no per topic setting, check host preferrences
        if host0.preferred_network in common:
            return host0.preferred_network
        if host1.preferred_network in common:
            return host1.preferred_network
        
        return common.pop() # no preference

    def get_scheduling_settings_for(self, host):
        if self.settings is None:
            return {}
        sched = self.settings.scheduling_on.get(host, {})
        remove = "preferred_network", "offer_endianess_swap", "deny_endianess_swap"
        allowed = set(sched.keys()).difference(remove)
        return dict([(key, sched[key]) for key in allowed])

    def has_port(self, port):
        if self.publisher is not None and str(self.source_port) == str(port):
            return "publisher" # yes -> publisher port!
        for sub in self.subscribers:
            if str(sub.port) == str(port):
                return "subscriber", sub.client.program_name
        return False

    def _generate_port_name(self, client, buffers, rate, is_reliable=True):
        if rate == -1:
            rate = "full"
        else:
            rate = str(rate).replace(".", "p")
        if is_reliable:
            is_reliable = "_rel"
        else:
            is_reliable = ""
        return "%s_%d_%s_%s%s" % (client.pid, buffers, rate, hashlib.md5(self.name.encode("utf-8")).hexdigest(), is_reliable)
    
    def generate_source_port_name(self, client, daemon, buffers, rate, is_reliable):
        n = self._generate_port_name(client, buffers, rate, is_reliable) + "_src"
        nn = n
        # check if it is already in remove queue
        c = 0
        while True:
            for deadline, p in daemon.remove_port_queue:
                if p.port_type == "shm" and p.name == nn:
                    break
            else:
                return nn
            c += 1
            nn = "%s_%d" % (n, c)
        
    def __str__(self):
        k = []
        for key, value in self.__dict__.items():
            k.append("%s: %r" % (key, value))
        return "<%s at %x %s>" % (self.__class__.__name__, id(self), ", ".join(k))
    def __repr__(self):
        return str(self)

    def set_publisher(self, client):
        if client is not None and self.publisher and self.publisher != client:
            raise Exception("topic %r already has publisher %r!" % (self.name, self.publisher))
        if client is None and self.publisher:
            self.manager.resource_event("topic_del_publisher", self.name, self.md_name, self.publisher.program_name)
            # is there still a source port for this topic?
            if self.source_port:
                self.source_port.del_publisher()
                self.source_port = None
        if client is not None and self.source_port:
            self.source_port.publisher_topic = self

        is_new_publisher = client is not None and self.publisher != client
        self.publisher = client
        self.manager.update_topic(self)
        if is_new_publisher:
            self.manager.resource_event("topic_new_publisher", self.name, self.md_name, self.publisher.program_name)
        if self.subscribers:
            self.schedule_subscriber_count_update()
            if self.publisher:
                # are there subscribers without port -> assign port!
                for sub in self.subscribers:
                    if sub.port:
                        continue
                    if sub.port == "in progress":
                        continue
                    # no subscriber port!
                    sub.client._create_subscriber_port(sub, delayed=True)
                    return # delay further connections until this subscriber has a port...
                # we already have subscribers!
                # link them!
                #self.connect(self.subscribers)
                for sub in self.subscribers:
                    if sub.port == "in progress":
                        continue
                    sub.client.connect_topic(sub, ignore_cb)
            self.schedule_publisher_update()
        else:
            self.check_del_topic()

    def check_del_topic(self):
        if self.publisher:
            return
        if self.subscribers:
            return
        if self.source_port and self.source_port.have_subscriptions():
            return
        # no subscribers, no loggers -> remove topic!
        self.manager.del_topic(self.name)

    def schedule_subscriber_count_update(self):
        if hasattr(self, "subscriber_count_update_timer"):
            return
        self.subscriber_count_update_timer = GLib.timeout_add(250, self.do_subscriber_count_update)

    def do_subscriber_count_update(self):
        if self.publisher:
            if self.publisher.is_blocked():
                return True
            self.publisher.request(
                method="update_subscribers", 
                topic=self.name,
                n_subscribers=len(self.subscribers))
        GLib.source_remove(self.subscriber_count_update_timer)
        del self.subscriber_count_update_timer
        return False

    def schedule_publisher_update(self):
        if hasattr(self, "publisher_update_timer"):
            return
        self.publisher_update_timer = GLib.timeout_add(250, self.do_publisher_update)

    def do_publisher_update(self):
        GLib.source_remove(self.publisher_update_timer)
        del self.publisher_update_timer
        if self.publisher:
            n = 1
        else:
            n = 0
        for sub in self.subscribers:
            if sub.client.is_blocked():
                sub.client.queue_send_on_unblock(
                    method="update_publishers", 
                    topic=self.name,
                    n_publishers=n)
                continue
            sub.client.request(
                method="update_publishers", 
                topic=self.name,
                n_publishers=n)
        return False

    def get_subscribed_clients(self):
        clients = set()
        for sub in self.subscribers:
            clients.add(sub.client)
        return clients

    def add_subscriber(self, sub, connect=True):
        self.subscribers.append(sub)
        self.manager.update_topic(self)
        self.manager.resource_event("topic_new_subscriber", self.name, self.md_name, sub.client.program_name)
        if self.publisher:
            # schedule update of subscriber count to publisher
            self.schedule_subscriber_count_update()
            self.schedule_publisher_update()
            if connect and sub.port != self.source_port:
                # we already have publisher!
                # link them!
                #self.connect(sub)
                sub.client.connect_topic(sub, ignore_cb)

    def del_subscriber(self, sub):
        if sub in self.subscribers:
            self.subscribers.remove(sub)
        else:
            self.info("topic %s is not subscribed by client %s" % (self.name, sub.client))
        self.manager.resource_event("topic_del_subscriber", self.name, self.md_name, sub.client.program_name)
        if self.publisher:
            # schedule update of subscriber count to publisher
            self.schedule_subscriber_count_update()
        self.manager.update_topic(self)

    def find_subscriber(self, sub_name, sub_rate=None):
        for sub in self.subscribers:
            if sub.client.program_name == sub_name and (sub_rate is None or sub.rate == sub_rate):
                return sub

    def _get_md(self, name, fn):
        mdid = name, fn
        if mdid in self.md_cache:
            return self.md_cache[mdid]
        self.md_cache[mdid] = links_and_nodes_base.message_definition(name, fn)
        return self.md_cache[mdid]

    def get_swap_endianess_description(self):
        def add_md(md):
            defines = dict(md.defines)
            eswap = []
            for ftype, fname, count in md.fields:
                if ftype in defines:
                    for c in range(count):
                        child_md = self._get_md(ftype, defines[ftype])
                        eswap.append(add_md(child_md))
                else:
                    dt = links_and_nodes_base.data_type_map[ftype]
                    eswap.append(str(dt.get_sizeof()) * count)
            return "".join(eswap)
        return add_md(self.md)

    def decide_endianess_swap(self, daemonA, daemonB):
        """
        decides whether daemonA has to do the endianess swap!

        both daemons need to be connected!
        """
        # todo: let user override this setting!
        daemonA.debug("decide endianess between %s and %s" % (daemonA.host, daemonB.host))
        if daemonA.endianess == daemonB.endianess and daemonB.endianess != "unknown":
            daemonA.debug("both daemons have same endianess!")
            return None
        if not daemonA.can_swap_endianess() and not daemonB.can_swap_endianess():
            raise Exception("neither daemon on %s(%s) nor daemon on %s(%s) can swap endianesses!" % (
                daemonA.host, daemonA.architecture, 
                daemonB.host, daemonB.architecture))
        if not daemonA.can_swap_endianess():
            daemonA.debug("only this daemon can swap endianess!")
            return None # other will have to do it

        if self.settings is not None:
            # has one of them an offer?
            offerA = "offer_endianess_swap" in self.settings.scheduling_on.get(daemonA.host, {})
            offerB = "offer_endianess_swap" in self.settings.scheduling_on.get(daemonB.host, {})

            if offerA: # we do it!
                daemonA.debug("this daemon offers to swap!")
                return self.get_swap_endianess_description()
            if offerB: # other will do it!
                daemonA.debug("other daemon offers to swap!")
                return None

            denyA = "deny_endianess_swap" in self.settings.scheduling_on.get(daemonA.host, {})
            denyB = "deny_endianess_swap" in self.settings.scheduling_on.get(daemonB.host, {})
            if denyA and not denyB:
                daemonA.debug("this daemon denies to swap!")
                return None # B should do it
            if denyB and not denyA: # we do it
                daemonA.debug("other daemon denies to swap!")
                return self.get_swap_endianess_description()
            # in case both deny we use network byte order
            if denyA and denyB:
                self.warning("both hosts (%s and %s) deny endianess-swapping for topic %s" % (
                    daemonA.host, daemonB.host, self.name))
        
        # shall daemonA do an edianess-swap?
        if daemonA.endianess == network_byte_order:
            # daemonA uses network byte order -> daemonB will have to swap!
            daemonA.debug("this daemon uses network byte order, other will have to swap!")
            return None
        # daemonA is not network byte order -> has to swap!
        daemonA.debug("this daemon does not use network byte order, we will have to swap!")
        return self.get_swap_endianess_description()
