"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

import os
import traceback

from . import config

from .Logging import enable_logging_on_instance
from .CommunicationUtils import escape_markup
from .PortInspector import PortInspector
from .topics_gui_treeview import TopicsGuiTreeView
from . import topic_priorities
import pyutils

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
gi.require_version('Pango', '1.0')
gi.require_version('GObject', '2.0')
from gi.repository import GObject, Gtk, GLib, Pango # noqa: E402

manager_base_dir = os.path.dirname(os.path.realpath(__file__))

class TopicsGui(pyutils.hooked_object):
    def __init__(self, gui):
        self.have_xml = False
        self.nb = None
        pyutils.hooked_object.__init__(self)
        enable_logging_on_instance(self, "TopicsGui")
        self.gui = gui
        self.manager_proxy = self.gui.manager_proxy

        # load processes gui
        resources_dir = os.path.join(config.manager_base_dir, "resources")
        self.xml = Gtk.Builder()
        self.xml_fn = os.path.join(resources_dir, "ln_manager_topics.ui")
        self.xml.add_from_file(self.xml_fn)
        self.have_xml = True

        vb = Gtk.VBox()
        self.main_paned.reparent(vb)
        self.gui.add_notebook_page("topics", vb, show=False)

        self.topics_tv = TopicsGuiTreeView(self)

        self.xml.connect_signals(self)

        tv = self.subscribers_tv
        tv.set_enable_tree_lines(True)
        tv.set_headers_visible(True)
        self.subscribers_model = Gtk.ListStore(
            GObject.TYPE_PYOBJECT, # client
            GObject.TYPE_STRING, # client-name
            GObject.TYPE_STRING, # subscriber fps
            GObject.TYPE_PYOBJECT, # topic_subscription
            )
        tv.insert_column_with_attributes(-1, "client", Gtk.CellRendererText(), text=1)
        col = tv.get_columns()[-1]
        col.set_resizable(True)
        tv.insert_column_with_attributes(-1, "rate [Hz]", Gtk.CellRendererText(), text=2)
        tv.set_model(self.subscribers_model) # cursor-changed-fine

        # variables
        self.last_message_def_label = None
        self.last_publisher_label = None

        self.current_topic = None
        self.ports_needing_info = {}
        self.get_info_idle = None
        self.port_info_in_progress = None
        ### disable topic update for now! 
        GLib.timeout_add(1000, self.on_update_topics_timeout)
        self.inspectors = {}
        self.topic_priorities = {}
        self.topics_prios_btn.set_sensitive(False)

    def __getattr__(self, name):
        if not self.have_xml:
            raise AttributeError(name)
        widget = self.xml.get_object(name)
        if widget is None:    
            raise AttributeError(name)
        return widget

    def connect(self):
        def notification_wrapper(callback):
            def inner_notification_wrapper(gui, msg):
                if "request" in msg:
                    del msg["request"]
                try:
                    return callback(**msg)
                except Exception:
                    self.error("error in notification callback %r%s:\n%s" % (callback.__name__, msg, traceback.format_exc()))
                    raise
            return inner_notification_wrapper
        self.gui.add_hook(("notification", "add_topic"), notification_wrapper(self.on_add_topic))
        self.gui.add_hook(("notification", "del_topic"), notification_wrapper(self.on_del_topic))
        self.gui.add_hook(("notification", "update_topic"), notification_wrapper(self.on_update_topic))
        #self.gui.add_hook(("notification", "update_obj_requested_state"), notification_wrapper(self._obj_requested_state_changed))

        self.update_topics()

    def _need_info_for_port(self, port, topic, cb=None):
        if port.is_client_port:
            return # no info for client-only ports!
        if not topic.has_port(port):
            return # skip this, try next

        def got_port_info_answer(msg=None, exception=None):
            if exception is not None:
                self.warning("could not get port info for port %r: %s" % (port, exception))
                return
            port.read_info(msg)
            self.on_update_topic(topic)
            if cb: cb()
        
        self.manager_proxy("call_cb", port.daemon.get_state, got_port_info_answer, port_id=port.get_id())
        
    def on_update_topics_timeout(self):
        if self.gui.notebook.get_current_page() != 2:
            return True
        for topic in self.topics_tv.get_visible():
            if topic.source_port:
                self._need_info_for_port(topic.source_port, topic)
        return True

    ## manager events
    def on_add_topic(self, topic):
        self.topics_tv.add(topic)
        return True
    def on_del_topic(self, topic):
        if topic == self.current_topic:
            self.topics_prios_btn.set_sensitive(False)
        self.topics_tv.remove(topic)
        return True
    def on_update_topic(self, topic):
        self.topics_tv.add(topic)
        if self.current_topic == topic:
            self.show_topic(topic)
        for port, inspector in self.inspectors.items():
            if getattr(inspector, "topic_name", None) == topic.name or (inspector.topic is not None and inspector.topic.name == topic.name):
                inspector.topic = topic

        return True
    def select_topic(self, topic):
        self.topics_tv.select(topic)
        return True

    def _select_subscribed_topic(self, topic, client):
        if not self.topics_tv.select(topic):
            return False
        m = self.subscribers_model
        iter = m.get_iter_first()
        while iter:
            row = m[iter]
            if row[0] == client:
                self.subscribers_tv.set_cursor(m.get_path(iter))
                return True
            iter = m.iter_next(iter)
        
        return True

    ## gui callbacks
    def on_label_activate_link(self, label, uri):
        if uri == "goto_publisher" and self.current_topic.publisher:
            self.gui.notebook.set_current_page(1)
            self.gui.cg._select_client_topic(self.current_topic.publisher, self.current_topic)
        if uri == "goto_message_def":
            import links_and_nodes
            fn = links_and_nodes.search_message_definition(self.current_topic.md_name)
            pyutils.system("emacs '%s' &" % fn)
        return True

    def open_inspector_for_port(self, port, topic, subscriber):
        if port in self.inspectors:
            self.inspectors[port].present()
        else:
            self.inspectors[port] = PortInspector(self, port, topic, subscriber=subscriber)

    def on_on_inspect_clicked(self, btn):
        if self.current_topic and self.current_topic.source_port:
            self.open_inspector_for_port(self.current_topic.source_port, self.current_topic, subscriber=None)
            
    def on_subscribers_tv_cursor_changed(self, tv):
        path, col = tv.get_cursor() # path-none-checked
        if path:
            sub = self.subscribers_model[path][3]
            self.selected_subscription = sub
            self.on_inspect_subscribed.set_sensitive(sub.port is not None)

    def on_on_inspect_subscribed_clicked(self, btn):
        self.open_inspector_for_port(self.selected_subscription.port, self.current_topic, subscriber=self.selected_subscription)

    ## worker methods
    def update_topics(self):
        # initially get topics from manager!
        self.topics = self.manager_proxy(request="get_topics")
        self.topics_tv.set(self.topics.values())
        if self.current_topic:
            self.show_topic(self.current_topic) # todo: what if it disappeared?

    def show_topic(self, topic):
        self.current_topic = topic
        if not topic:
            self.topics_prios_btn.set_sensitive(False)
            self.selected_topic_label.set_markup("selected topic: ")
            self.publisher_label.set_markup("")
            self.message_def_label.set_markup("")
            self.on_inspect.set_sensitive(False)
            self.on_inspect_subscribed.set_sensitive(False)
            self.subscribers_model.clear()
            return

        self.on_inspect.set_sensitive(True)
        self.topics_prios_btn.set_sensitive(True)
        if not hasattr(self, "last_selected_topic") or self.last_selected_topic != topic.name:
            self.last_selected_topic = topic.name
            self.selected_topic_label.set_markup("selected topic: <b>%s</b>" % topic.name)

        if topic.publisher:
            t = '<a href="goto_publisher">%s</a> on <a href="goto_daemon">%s</a>' % (
                topic.publisher.program_name, topic.source_port.daemon.host.name)
        else:
            t = escape_markup("<no publisher>")

        if t != self.last_publisher_label:
            self.last_publisher_label = t
            self.publisher_label.set_markup(t)

        t = '<a href="goto_message_def">%s</a> %d bytes' % (topic.md_name, topic.message_size)
        if t != self.last_message_def_label:
            self.last_message_def_label = t
            self.message_def_label.set_markup(t)

        subs = topic.subscribers
        if not subs:
            self.on_inspect_subscribed.set_sensitive(False)

        subs.sort(key=lambda s: s.client.program_name)

        m = self.subscribers_model

        i = 0
        iter = m.get_iter_first()

        while iter is not None or i < len(subs):
            if i == len(subs):
                # remove old row
                if not m.remove(iter):
                    iter = None
                continue
            client = subs[i].client
            rate = subs[i].rate

            if iter is None:
                # new row -> append!
                m.append((client, client.program_name, "%.1f" % rate, subs[i]))
                i += 1
                continue

            row = m[iter]
            if row[0].program_name == client.program_name:
                row[2] = "%.1f" % rate
                iter = m.iter_next(iter)
                i += 1
                continue

            if row[1] > client.program_name:
                # insert before
                m.insert_before(iter, (client, client.program_name, "%.1f" % rate, subs[i]),)
                i += 1
                continue
            
            # row[1] < client.program_name
            if not m.remove(iter):
                iter = None

        #for client, rate in topic.subscribers.iteritems():
        #    m.append((client, client.program_name, "%.1f" % rate))

    def on_subscribers_tv_row_activated(self, tv, path, col):
        m = self.subscribers_model
        sub = m[path][3]
        self.open_inspector_for_port(sub.port, self.current_topic, subscriber=sub)
        return True

    def on_topics_prios_btn_clicked(self, btn):
        #topic_priorities = reload(topic_priorities)
        if self.current_topic.name not in self.topic_priorities:
            self.topic_priorities[self.current_topic.name] = topic_priorities.topic_priorities(self, self.current_topic)
        else:
            self.topic_priorities[self.current_topic.name].present()

    def on_open_console_btn_clicked(self, btn):
        if self.nb is None:
            import pyutils.pygtksvnb.notebook
            notebook_fn = os.path.join(
                self.gui.sysconf.instance_config.default_notebook_path,
                "gui.py")
            content = """% gui
% manager_proxy
% gui.manager
% gui.manager.sysconf
"""
            self.nb = pyutils.pygtksvnb.notebook(
                filename=notebook_fn,
                new_notebook_cell_content=content,
                create_file=True,
                add_to_context=dict(
                    gui=self.gui,
                    tg=self.gui.tg,
                    manager_proxy = self.gui.manager_proxy,
                    )
            )
            self.nb.on_hide = lambda *args: True
            self.nb.show()
        self.nb.present()
    
    def on_profile_outputs_btn_toggled(self, btn):
        self.manager_proxy("enable_process_output_profiling", btn.get_active())

