"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

# warning: this is a auto-generated file by ./generate_linux_info.py
# do not edit!

policies = {
 0: 'NORMAL',
 1: 'FIFO',
 2: 'RR',
 3: 'BATCH',
 5: 'IDLE',
 1073741824: 'RESET_ON_FORK'
}

