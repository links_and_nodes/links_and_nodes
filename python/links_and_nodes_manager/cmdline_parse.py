from __future__ import print_function

import re
import sys

def read_string(data, start, allow_open_string=False):
    delim = data[start]
    #print "read string from %r" % data[start:start + 40]

    start += 1
    offset = start
    out = []
    while True:
        #print "read string search from %r" % data[offset:offset + 10]
        m = re.search(r"[%s\\]" % delim, data[offset:])
        if not m:
            if not allow_open_string:
                raise Exception("string from %r was not terminated by %s!" % (data[start:start + 40], delim))
            out.append(data[start:])
            return "".join(out), len(data)
        ch = data[offset + m.start(0)]
        #print " see %r" % ch
        if ch == delim:
            out.append(data[start:offset + m.start(0)])
            return "".join(out), offset + m.start(0) + 1
        
        # its a backslash, skip next char!
        bch = data[offset + m.start(0) + 1]
        bch = eval('"\\%s"' % bch)
        out.append(data[start:offset + m.start(0)] + bch)
        #print "new out: %r" % out
        start = offset + m.start(0) + 2
        offset += m.start(0) + 2

def cmdline_parse(cmdline):
    for cmd_ast in cmdline_parse_ast(cmdline):
        if len(cmd_ast) > 2:
            yield [arg_ast[0] for arg_ast in cmd_ast[1:-1]]
    
def cmdline_parse_ast(cmdline, allow_open_string=False):
    offset = 0
    cmd = [offset]
    while True:
        #print "in: %s" % cmdline[offset:]
        m = re.search("\S", cmdline[offset:])
        #print "  match:", m
        if m is None:
            if cmd:
                #print "   yield command"
                cmd.append(len(cmdline))
                yield cmd
            return

        p = offset + m.start(0)
        ch = cmdline[p]
        #print "  ch: %r" % ch

        if ch == ";":
            cmd.append(p)
            yield cmd
            offset = p + 1
            cmd = [offset]
            continue

        if ch in ("\"'"):
            string, offset = read_string(cmdline, p, allow_open_string=allow_open_string)
            nextch = cmdline[offset:][:1]
            #print "  got string %r, after: %r" % (string, nextch)
            if not nextch or nextch in " \r\n\t":
                cmd.append((string, p, offset))
                continue
            prefix = string
            prefix_start = p
            # continue reding...
            p = offset            
        else:
            prefix = ""
            prefix_start = p

        while True:
            #print "read ident from: %s" % cmdline[p:]
            m = re.search(r"[\s;\\]", cmdline[p:])
            #print "  match", m
            if m is None:
                arg = prefix + cmdline[p:]
                cmd.append((arg, prefix_start, len(cmdline)))
                cmd.append(len(cmdline))
                #print "   yield command"
                yield cmd
                return
            #print "found in ident: %r" % cmdline[p+m.start(0)]
            if cmdline[p+m.start(0)] != "\\":
                break
            prefix += cmdline[p:p + m.start(0)] + cmdline[p + m.start(0) + 1]
            p += m.start(0) + 2
            
        arg = prefix + cmdline[p:p + m.start(0)]
        #print "got arg: %r" % arg
        cmd.append((arg, prefix_start, p + m.start(0)))
        offset = p + m.start(0)

def test_cmdline_parse():
    tests = [
        ("hallo; flo\; welt", [["hallo"], ["flo;", "welt"]]),
        ("hallo; flo \; welt", [["hallo"], ["flo", ";", "welt"]]),
        (";hallo; flo", [["hallo"], ["flo"]]),
        ("hallo flo", [["hallo", "flo"]]),
        ("hallo flo; hallo welt", [["hallo", "flo"], ["hallo", "welt"]]),
        ('hallo "flo"', [["hallo", "flo"]]),
        ('hallo "flo"schorsch', [["hallo", "floschorsch"]]),
        ('hallo "welt & flo;" und rest', [["hallo", "welt & flo;", "und", "rest"]]),
        (r'hallo "welt & \"flo;\""', [["hallo", r'welt & "flo;"']]),
        (r"hallo 'welt flo'", [["hallo", 'welt flo']]),
        ('\'hallo "welt flo"\'', [['hallo "welt flo"']]),
        ("\"hallo 'welt flo'\"", [["hallo 'welt flo'"]]),
    ]
    for inp, out in tests:
        print("\n\ninput: %s, expect: %s" % (inp, out))
        got = list(cmdline_parse(inp))
        if got != out:
            print(" -> error: got instead %s" % got)
            sys.exit(1)
        print(" -> success")

def test_cmdline_parse_ast():
    tests = [
        "hallo; flo\; welt",
        "hallo; flo \; welt",
        'hallo "flo & welt"',
        'hallo "flo ',
        'hallo "flo hallo" welt "und sonst ',
    ]
    for inp in tests:
        print("\n\ninput: %r" % inp)
        ast = cmdline_parse_ast(inp, allow_open_string=True)
        for i, cmd_ast in enumerate(ast):
            start = cmd_ast[0]
            end = cmd_ast[-1]
            print("command %d from %d to %d: %r:" % (i, start, end, inp[start:end]))
            for arg, start, end in cmd_ast[1:-1]:
                print("  ast: %r, from %r (%d:%d)" % (arg, inp[start:end], start, end))
    
def find_completion_start(line, cursor_pos):
    """
    search start of search term before cursor_pos.
    search term can start at any word or of present with " or '

    returns
      search_start the start of the search-prefix-argument
      search_end   the start of the rest after the search string
      search       the search-prefix string (without ' or ")
      args_before  list of arguments before the search prefix
    """

    commands = list(cmdline_parse_ast(line, allow_open_string=True))
    
    for cmd in commands:
        if cmd[0] <= cursor_pos and cmd[-1] >= cursor_pos:
            break
    else:
        raise Exception("no command at this cursor pos?!: %r, pos %d" % (commands, cursor_pos))

    # work on cmd
    # find argument that start before cursor pos
    for argi, (arg, astart, aend) in enumerate(cmd[1:-1]):
        if aend >= cursor_pos:
            break
    else:
        astart = aend = cursor_pos
        arg = ""
        argi += 1    
    return astart, aend, arg, [arg_ast[0] for arg_ast in cmd[1:1 + argi]]

def test_find_completion_start():
    tests = [
        ##0123456789012345678
        ("hello worl; other command", 9, [6, 10, "worl", ["hello"]]),
        ("hello worl; other command", 10, [6, 10, "worl", ["hello"]]),
        ("hello    worl", 12, [9, 13, "worl", ["hello"]]),
        ('hello "flo & worl', 17, [6, 17, "flo & worl", ["hello"]]),
        ('hello "flo & worl"; other', 17, [6, 18, "flo & worl", ["hello"]]),
        ('command ', 8, [8, 8, "", ["command"]]),
        ('some command ', 13, [13, 13, "", ["some", "command"]]),
    ]
    for inp, cp, out in tests:
        print("\n\ninput: %s, expect: %s" % (inp, out))
        got = list(find_completion_start(inp, cp))
        if got != out:
            print(" -> error: got instead %s" % got)
            sys.exit(1)
        print(" -> success")
        

if __name__ == "__main__":
    #test_cmdline_parse()
    #test_cmdline_parse_ast()
    test_find_completion_start()
