"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

import os
import sys
import math
import numpy as np
import struct
import string
import datetime
import traceback

import pyutils

from . import config
from . import ui_tools

from .Logging import enable_logging_on_instance

import links_and_nodes as ln

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
gi.require_version('Pango', '1.0')
gi.require_version('GObject', '2.0')
from gi.repository import Gtk, GLib, Pango, GObject # noqa: E402

def get_print(c):
    c = chr(c)
    if c not in "\v\f\r\n\t" and c in string.printable:
        return c
    return "."


class PortInspector(pyutils.hooked_object):

    def _set_port(self, port, topic):
        self.port = port
        self.topic = topic
        self.disappeared = False
        self.topic_label.set_text(topic.name)
        self.port_label.set_text("%s on %s" % ("%s: %s" % port.get_id()[:2], port.daemon.host))
        
        self.md_name_label.set_text(self.topic.md_name)
        self.md_fn = self.gui.manager_proxy("search_message_definition_filename", self.topic.md_name)
        # ln.search_message_definition(self.topic.md_name)
        self.md_fn_name_label.set_text(self.md_fn)

        self.md = self._get_md(self.topic.md_name, self.md_fn)

        tv = self.md_tv
        m = self.md_model = Gtk.TreeStore(
            GObject.TYPE_STRING, # name
            GObject.TYPE_STRING, # value
            GObject.TYPE_STRING, # type-display
            GObject.TYPE_PYOBJECT, # type intern, offset, count
            )
        cols = tv.get_columns()
        for col in cols:
            tv.remove_column(col)
        tv.insert_column_with_attributes(-1, "name", Gtk.CellRendererText(), text=0)
        tv.insert_column_with_attributes(-1, "value", Gtk.CellRendererText(), text=1)
        col = tv.get_columns()[-1]
        col.set_resizable(True)
        tv.insert_column_with_attributes(-1, "type", Gtk.CellRendererText(), text=2)

        tv.set_model(m) # cursor-changed-fine
        # initially fill up model
        self._create_model()

    def __init__(self, parent, port, topic, subscriber=None):
        self.subscriber = subscriber
        self.have_xml = False
        pyutils.hooked_object.__init__(self)
        enable_logging_on_instance(self, "PortInspector")

        self.parent = parent

        self.gui = self.parent.gui
        self.manager = self.gui.manager

        self.keep_overwriting_log = False
        
        resources_dir = os.path.join(config.manager_base_dir, "resources")
        self.xml = Gtk.Builder()
        self.xml_fn = os.path.join(resources_dir, "topic_inspect_window.ui")
        self.xml.add_from_file(self.xml_fn)
        self.have_xml = True
        self.xml.connect_signals(self)

        self.md_cache = {}
        self._set_port(port, topic)

        self.inspect_window.set_title("inspect %s%s" % (topic.name, " subscriber" if self.subscriber else ""))
        self.log_only_ts_btn.set_active(False)
        self.last_thread_label_markup = None
        self.format_flat_pickle_btn.set_active(True)
        self.refresh_timer = None
        self.request_pending = False
        self.had_logging_info = False
        self.last_downloaded_file = None
        self.download_fn = topic.name        
        self.download_fn = "/tmp/log_%s" % self.download_fn.replace("/", "_")
        self.download_format = "_flat.pickle"
        self.fn_label.set_text(self.download_fn)

        self.hex_tv.get_pango_context()
        font_description = Pango.FontDescription(ui_tools.get_monospace_font_name(self.inspect_window))
        self.hex_tv.modify_font(font_description)

        self._last_hex = None
        self.get_last_packet()
        self.refresh_interval.set_value(0.5)
        self.auto_refresh.set_active(True)
        self.inspect_window.show()
        self.visible = True
        name = "pi log %s" % (self.port.get_id(), )
        class logger_wrapper(object):
            def __init__(self, proxy, name):
                self.proxy = proxy
                self.remote_obj = self.proxy("get_logger", name=name)

            def call_method(self, method_name, *args, **kwargs):
                return self.proxy("call", getattr(self.remote_obj, method_name), *args, **kwargs)
                
            def __getattr__(self, method_name):
                def method_wrapper(*args, **kwargs):
                    return self.call_method(method_name, *args, **kwargs)
                setattr(self, method_name, method_wrapper)
                return method_wrapper
            def manager_save_org(self, cb, *args, **kwargs):
                return self.proxy("call_cb", self.remote_obj.manager_save, cb, *args, **kwargs)
            
            def manager_save(self, cb, filename, **kwargs):
                # test write to target file
                with open(filename, "ab+") as fp:
                    pass
                def on_data(data=None, exception=None):
                    if exception is not None: return cb(exception=exception)
                    with open(filename, "wb") as fp:
                        fp.write(data)
                    cb()
                self.proxy("call_cb", self.remote_obj.manager_save, on_data, filename, return_with_cb=True, **kwargs)
                
        self.logger = logger_wrapper(self.gui.manager_proxy, name)
        if self.port and self.port.topic:
            self.logger.set_topics([(self.port.topic.name, 1000, 1)])

    def __getattr__(self, name):
        if not self.have_xml:
            raise AttributeError(name)
        widget = self.xml.get_object(name)
        if widget is None:    
            raise AttributeError(name)
        return widget

    def present(self):
        if self.auto_refresh.get_active():
            if self.refresh_interval.get_value() < 0.0001:
                self.refresh_interval.set_value(0)
                self.auto_refresh.set_active(False)
            else:
                self.refresh_timer = GLib.timeout_add(int(1000 * self.refresh_interval.get_value()), self.do_refresh)
        self.visible = True
        self.inspect_window.present()

    def _start_logging(self, max_samples, max_time=-1, divisor=1, only_ts=False):
        if not max_samples:
            self.error("refusing ringbuffer size of 0!")
            return True

        if self.subscriber:
            self.logger.set_topics([("subscriber:%s@%s:%s" % (self.subscriber.client.program_name, self.subscriber.rate, self.port.topic.name), max_samples, divisor)])
        else:
            self.logger.set_topics([(self.port.topic.name, max_samples, divisor)])

        self.logger.set_all_only_ts(only_ts)
        self.logger.enable(max_time=max_time)

    def on_log_start_btn_clicked(self, btn):
        max_samples = int(self.log_size_sbtn.get_value())
        divisor = int(self.divisor_spin.get_value())
        only_ts = self.log_only_ts_btn.get_active()
        return self._start_logging(max_samples, -1, divisor, only_ts)

    def on_log_start_seconds_btn_clicked(self, btn):
        max_samples = int(self.log_size_sbtn.get_value())
        divisor = int(self.divisor_spin.get_value())
        only_ts = self.log_only_ts_btn.get_active()
        max_time = float(self.log_seconds_sbtn.get_value())
        if max_time <= 0:
            self.error("refusing max_time of %r" % max_time)
            return
        return self._start_logging(max_samples, max_time, divisor, only_ts)

    def on_log_stop_btn_clicked(self, btn):
        self.logger.disable()

    def error_dialog(self, msg):
        dlg = Gtk.MessageDialog(
            self.inspect_window,
            Gtk.DialogFlags.DESTROY_WITH_PARENT,
            Gtk.MessageType.ERROR,
            Gtk.ButtonsType.OK,
            message_format=msg
            )
        dlg.set_markup(msg)
        dlg.set_property("use-markup", True)
        def answer(dlg, response_id):
            dlg.hide()
            return
        dlg.connect("response", answer)
        dlg.show()

    def on_log_download_btn_clicked(self, btn):
        """
        download and save to local file!
        local meaning gui-local!
        """

        self.log_download_btn.set_property("sensitive", False)

        fn = self.download_fn
        if self.log_with_ts_cb.get_active():
            fn += "_" + datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
        fn += self.download_format
        self.last_downloaded_file = fn

        # manager-local: exists = self.gui.manager_proxy("check_file_exists", fn)
        exists = os.path.exists(fn)
        
        if exists and not self.keep_overwriting_log:
            self.info("filename %s already exists on manager side..." % self.last_downloaded_file)
            msg = (
                "file <b>%s</b> already exist on managers filesystem!\n\n" + 
                "append a timestamp to the filename?\n" + 
                "override this file now?\n" +
                "keep overwriting this file in this session?\n" +
                "or do you want to cancel / change your filename?"
                ) % (
                self.last_downloaded_file, 
                )
            dlg = Gtk.MessageDialog(
                self.inspect_window,
                Gtk.DialogFlags.DESTROY_WITH_PARENT, 
                Gtk.MessageType.QUESTION,
                Gtk.ButtonsType.NONE,
                message_format=msg
                )
            dlg.add_button("append timestamp", 1)
            dlg.add_button("overwrite", 2)
            dlg.add_button("always overwrite", 3)
            dlg.add_button("cancel", 4)
            dlg.set_markup(msg)
            dlg.set_property("use-markup", True)
            def answer(dlg, response_id):
                dlg.hide()
                if response_id == 1: # add ts
                    self.log_with_ts_cb.set_active(True)
                    self.on_log_download_btn_clicked(self.log_download_btn)
                    return
                elif response_id == 2: # overwrite
                    pass # nothing todo
                elif response_id == 3: # keep overwriting
                    self.keep_overwriting_log = True
                elif response_id == 4: # cancel
                    self.log_download_btn.set_property("sensitive", True)
                    return                
                self.start_download()
                return
            
            dlg.connect("response", answer)
            dlg.show()
            return
        
        elif exists and self.keep_overwriting_log:
            self.info("file %s already exists but user chose to always overwrite..." % self.last_downloaded_file)

        try:
            self.start_download()
        except:
            self.log_download_btn.set_property("sensitive", True)
            self.error_dialog("could not download ln_daemon_log:\n%s" % sys.exc_info()[1])
            self.error("could not start download of ln_daemon log!\n%s" % traceback.format_exc())

    def start_download(self):
        self.info("downloading data...")
        self.on_retrieve_log()
        
    def on_retrieve_log(self):
        fn = self.last_downloaded_file
        def _on_finish(exception=None):
            self.log_download_btn.set_property("sensitive", True)
            if exception is not None:
                self.error_dialog("could not download ln_daemon_log:\n%s" % exception)
                return self.error("could not manager_save() the log: %s" % exception)
            self._update_log_stat_label()
            self.info("manager_save to %r finished" % fn)
        self.logger.manager_save(_on_finish, fn, verbose=True)

    def on_fn_btn_clicked(self, btn):
        self.filechooserdialog.set_current_name(os.path.basename(self.download_fn))
        self.filechooserdialog.set_current_folder(os.path.dirname(self.download_fn))
        self.filechooserdialog.show()

    def on_filechooser_save_btn_clicked(self, btn):
        self.filechooserdialog.hide()
        self.download_fn = self.filechooserdialog.get_filename()
        if self.format_raw_btn.get_active():
            self.download_format = ".raw"
        elif self.format_flat_pickle_btn.get_active():
            self.download_format = "_flat.pickle"
        elif self.format_mat_btn.get_active():
            self.download_format = ".mat"
        elif self.format_pcap_btn.get_active():
            self.download_format = ".pcap"
        self.fn_label.set_text(self.download_fn)

    def on_filechooser_cancel_btn_clicked(self, btn):
        self.filechooserdialog.hide()

    def _get_packet(self):
        if hasattr(self, "packet") and self.packet is not None:
            return self.packet
        md_fn = self.gui.manager_proxy("search_message_definition_filename", self.topic.md_name)
        md = self.gui.manager_proxy("create_message_definition", self.topic.md_name, md_fn)
        self.packet = ln.ln_packet(self.topic.md_name, str(md.dict()), md.calculate_size(), use_numpy_arrays=True, auto_cast=True)
        return self.packet

    def _update_log_stat_label(self):
        stats = self.logger.get_statistics()
        if stats is None:
            self.log_stat_label.set_property("visible", False)
            return
        self.log_stat_label.set_property("visible", True)

        stat_str = (
            "src-ts: %s\n"
            "log-ts: %s\n" % (
                stats["src_ts"],
                stats["log_ts"]
            )
        )
        if not self.subscriber or self.subscriber.rate == -1.0:
            stat_str += "lost packets: %d" % stats["lost_packets"]
        self.log_stat_label.set_text(stat_str)

    def on_notebook1_switch_page(self, nb, move_focus, page):
        if self.notebook1.get_current_page() == 1:
            self.fill_hex_tv()        
        return True

    def fill_hex_tv(self):
        hex = self.generate_hex_listing(self._last_data, 16)
        b = self.hex_tv.get_buffer()
        #b.set_text(hex)
        #b.insert(hex)
        #s_iter = b.get_start_iter()
        #b.insert_interactive(s_iter, hex, True)
        #s_iter = b.get_iter_at_offset(len(hex))
        #b.delete(s_iter, b.get_end_iter())
        #hadj.set_value(hval)
        #vadj.set_value(vval)

        if self._last_hex is None:
            b.set_text(hex)
            return
        # compare line by line and replace/instet
        offset = 0
        for line_old, line_new in zip(self._last_hex.split("\n"), hex.split("\n")):
            ll = len(line_new) + 1
            if line_old != line_new:
                # replace
                s_iter = b.get_iter_at_offset(offset)
                e_iter = b.get_iter_at_offset(offset + ll)
                b.delete(s_iter, e_iter)
                b.insert(s_iter, line_new + "\n")
            offset += ll
        self._last_hex = hex

    def generate_hex_listing(self, data, bytes_per_row, chunk_size=4):
        rows = []
        offset = 0
        data = np.fromstring(data, dtype=np.uint8)
        while offset < len(data):
            chunks = []
            chunk_off = 0
            for chunk in range(int(math.ceil(bytes_per_row / chunk_size))):
                chunks.append(" ".join(["%02X" % b for b in data[offset+chunk_off:offset+chunk_off+chunk_size]]))
                chunk_off += chunk_size
            hexl = "  ".join(chunks)

            printables = "".join(map(get_print, data[offset:offset+bytes_per_row]))

            rows.append("%05d: %s   %s" % (offset, hexl, printables))
            offset += bytes_per_row
        return "\n".join(rows)

    def do_refresh(self):
        self.disable_refresh_timer()
        if self.port is not None and self.port.have_info is False:
            self.parent._need_info_for_port(self.port, self.topic, cb=self.do_refresh)
            return
        self.get_last_packet()
        return False

    def set_refresh_timer(self):
        self.disable_refresh_timer()
        self.refresh_timer = GLib.timeout_add(int(1000 * self.refresh_interval.get_value()), self.do_refresh)

    def disable_refresh_timer(self):
        if self.refresh_timer is not None:
            GLib.source_remove(self.refresh_timer)
        self.refresh_timer = None
        
    def maybe_restart(self):
        if self.visible and self.auto_refresh.get_active():
            if self.refresh_interval.get_value() <= 0.0001:
                self.auto_refresh.set_active(False)
                self.refresh_interval.set_value(0)
                self.disable_refresh_timer()
            else:
                self.set_refresh_timer()
        else:
            self.disable_refresh_timer()
        return True

    def get_last_packet(self):
        #print "get_last_packet", self.request_pending, self.disappeared
        #print "subscribers", self.topic.subscribers
        if self.request_pending:
            return

        if not self.disappeared:
            # does this port still exist?
            has_port = self.topic.has_port(self.port)
            #print "has_port", has_port
            if has_port is False:
                #print "pub:", self.topic.publisher
                #print "sp:", self.topic.source_port
                # neither publisher nor subscriber port - do not request update
                self.disappeared = True
                self.info("port %s disappeared while there is an opened port-inspector. will wait for same topic & role" % self.port)
                self.port = None
                self.topic_name = self.topic.name
                self.packet = None
            else:
                self.role = has_port

        if self.disappeared:
            # search for same topic & role
            topic = self.gui.manager_proxy("get_storage_item", "topics", self.topic_name)

            if topic is not None:
                if topic.name != self.topic.name:
                    self.warning("port attached to new topic %r (from %r)" % (topic.name, self.topic.name))
                    self.topic = topic                    
                self.md_name_label.set_text(self.topic.md_name)
                self.md_fn = ln.search_message_definition(self.topic.md_name)
                self.md_fn_name_label.set_text(self.md_fn)
                self.md_cache = {}
                self.md = self._get_md(self.topic.md_name, self.md_fn)
                self._create_model()
            elif self.topic is not None:
                self.warning("port disattached from topic %r" % self.topic.name)
                self.topic = None
                self.packet = None
            if self.topic:
                # have topic search port with mathing role
                if self.role == "publisher":
                    if self.topic.publisher is not None:
                        self._set_port(self.topic.source_port, self.topic)
                elif self.role[0] == "subscriber":
                    for sub in self.topic.subscribers:
                        if sub.client.program_name != self.role[1]:
                            continue
                        if sub.port is None:
                            continue
                        self._set_port(sub.port, self.topic)
                        break
            self.maybe_restart()
            return

        if self.port.daemon.endianess == "big":
            self.byte_order = ">"
        else:
            self.byte_order = "<"
        
        self.request_pending = True
        
        self.gui.manager_proxy(
            "call_cb", self.port.daemon.get_last_packet,
            self.on_last_packet_answer,
            port_id=self.port.get_id()
        )
    
    def on_last_packet_answer(self, msg=None, exception=None):
        self.request_pending = False
        if exception is not None:
            has_port = self.topic.has_port(self.port)
            if has_port:
                self.error("get_last_packet: %s" % exception)
            return
        
        self._last_data = msg["last_packet"]
        self._last_time = msg["last_packet_time"]
        self._last_count = msg["packet_count"]

        ts = datetime.datetime.fromtimestamp(self._last_time).strftime("%Y-%m-%d %H:%M:%S")
        ts += ".%s" % (("%.3f" % self._last_time).split(".")[1])
        self.stat_label.set_text("last packet time: %s, last packet count: %d" % (
                ts,
                self._last_count))

        if "logging_enabled" in msg:
            # daemon with logging info
            logging_size = int(msg["logging_size"])
            logging_count = int(msg["logging_count"])
            logging_only_ts = bool(msg["logging_only_ts"])
            logging_divisor = int(msg.get("logging_divisor", 1))
            if not self.had_logging_info:
                self.had_logging_info = True
                self.log_only_ts_btn.set_active(logging_only_ts)
                if logging_size == 0:
                    rate = 0
                    if self.subscriber:
                        if self.subscriber.rate != -1.0:
                            rate = self.subscriber.rate
                        if self.port.topic and self.port.topic.source_port:
                            port = self.port.topic.source_port
                            if hasattr(port, "measured_rate") and port.measured_rate is not None:
                                if self.subscriber.rate == -1.0:
                                    rate = port.measured_rate
                                else:
                                    rate = min(rate, port.measured_rate)
                    elif hasattr(self.port, "measured_rate") and self.port.measured_rate is not None:
                        rate = self.port.measured_rate
                    if rate == 0:
                        rate = 100
                    logging_size = int(rate * 10)
                self.log_size_sbtn.set_value(logging_size)
                self.divisor_spin.set_value(logging_divisor)

            logging_enabled = bool(int(msg["logging_enabled"]))
            if logging_enabled:
                self.logging_label.set_markup('logging: <span color="red"><b>enabled</b></span>')
            else:
                self.logging_label.set_markup("logging: disabled")
            
            if logging_size > 0:
                p = float(logging_count) / logging_size
            else:
                p = 0
            self.log_fill_pb.set_text("%d samples / %.0f%%" % (logging_count, p * 100))
            self.log_fill_pb.set_fraction(p)
        
        if self.notebook1.get_current_page() == 1:
            self.fill_hex_tv()        
        self._fill_model()

        p = self.port
        m = []
        if p.linux_tid is not None:
            m.append('thread id: <a href="#show_pl_tid">%s</a>' % p.linux_tid)
        if p.thread_handle is not None:
            m.append('thread handle: <a href="#show_pl_th">0x%x</a>' % p.thread_handle)
        m.append("started: %s" % p.source_thread_started)
        m = ", ".join(m)
        if m != self.last_thread_label_markup:
            self.last_thread_label_markup = m
            self.thread_label.set_markup(m)

        self.maybe_restart()


    def on_refresh_btn_clicked(self, btn):
        self.get_last_packet()
        return True

    def on_notebook_btn_clicked(self, btn):
        notebook_fn = os.path.join(
            self.gui.sysconf.instance_config.default_notebook_path,
            "topic %s.py" % self.topic.name)
        fields = dict(self.topic.__dict__)
        
        if self.last_downloaded_file is not None:
            fields["download_fn"] = self.last_downloaded_file
        else:
            fields["download_fn"] = self.download_fn + self.download_format
        teaser = """no_auto_exec\ntopic = %(name)r
port = clnt.subscribe(topic, rate=10)
#~~~--~~~
log_data = load(%(download_fn)r, allow_pickle=True)
ln_unpack_log_data(log_data, no_topic_prefix=True)
#~~~--~~~
# plot the log delay - time between source-timestamp and log-timestamp
clf()
grid(True)
log_delay = (_packet_log_ts - _packet_source_ts)*1e3
plot(_packet_source_ts - _packet_source_ts[0], log_delay, label="log delay[ms]")
std = log_delay.std()
ylim([log_delay.mean() - 4*std, log_delay.mean() + 4*std])
legend()
""" % fields
        self.gui.manager_proxy("open_notebook", notebook_fn, "topic %s" % self.topic.name, teaser)
        return True
    
    def on_inspect_window_delete_event(self, window, ev):
        self.disable_refresh_timer()
        self.inspect_window.hide()
        self.visible = False
        return True

    def _get_md(self, name, fn):
        mdid = name, fn
        if mdid in self.md_cache:
            return self.md_cache[mdid]
        self.md_cache[mdid] = self.gui.manager_proxy("create_message_definition", name, fn)
        return self.md_cache[mdid]

    def _create_model(self):
        m = self.md_model
        m.clear()
        offset = 0
        def add_md(md, parent, offset):
            defines = dict(md.defines)
            for ftype, fname, count in md.fields:
                if count == 1:
                    ftype_display = ftype
                else:
                    ftype_display = "%s[%s]" % (ftype, count)
                if ftype in defines:
                    for c in range(count):
                        if count > 1:
                            fn = "%s[%d]" % (fname, c)
                        else:
                            fn = fname
                        iter = m.append(parent, (fn, "", ftype_display, None))
                        child_md = self._get_md(ftype, defines[ftype])
                        offset = add_md(child_md, iter, offset)
                else:
                    m.append(parent, (fname, "", ftype_display, (ftype, offset, count)))
                    offset += count * ln.data_type_map[ftype].get_sizeof()
            return offset

        add_md(self.md, None, offset)

    def on_limit_precision_clicked(self, btn):
        self._fill_model()
        return True
    
    def _fill_model(self):
        limit_precision = self.limit_precision.get_active()
        if limit_precision:
            precision = int(self.limit_precision_value.get_value())
            def limited_float_precision(what):
                # what can be scalar float or sequence of floats
                if hasattr(type(what), "__iter__"):
                    return "[%s]" % (", ".join(map(limited_float_precision, what)))
                return "%.*f" % (precision, what)
        m = self.md_model
        def fill(parent):
            iter = m.iter_children(parent)
            while iter:
                if m.iter_has_child(iter):
                    fill(iter)
                else:
                    row = m[iter]
                    if row[3] is not None:
                        ftype, offset, count = row[3]
                        dtype = ln.data_type_map[ftype]
                        format = "%s%s%s" % (self.byte_order, count, dtype.pyformat)
                        data = self._last_data[offset:offset + count * dtype.get_sizeof()]
                        #print "have field %s at offset %s type %s count %d: %r" % (
                        #    row[0], offset, ftype, count, data)

                        if dtype.pyformat in "fd" and limit_precision:
                            display_function = limited_float_precision
                        else:
                            display_function = repr

                        if dtype.name == "char" and count > 1:
                            data = data.decode("utf-8", "replace")
                            if len(data) > 30:
                                row[1] = display_function(data[:30])[:-1] + "..."
                            else:
                                row[1] = display_function(data)
                        else:
                            data = struct.unpack(format, data)
                            if count == 1:
                                data = data[0]
                                row[1] = display_function(data)
                            elif count > 30:
                                row[1] = display_function(data[:30])[:-1] + "..."
                            else:
                                row[1] = display_function(data)
                iter = m.iter_next(iter)
        fill(None)

    def on_auto_refresh_toggled(self, btn):
        if btn.get_active():
            if self.refresh_interval.get_value() < 0.0001:
                self.refresh_interval.set_value(0)
                btn.set_active(False)
                self.disable_refresh_timer()
            else:
                self.set_refresh_timer()
        else:
            self.disable_refresh_timer()

    def on_thread_label_activate_link(self, label, uri):
        if uri == "#show_pl_tid" or (self.port.linux_tid is not None and uri == "#show_pl_th"):
            pg = self.parent.gui.pg
            pl = pg.show_process_list(host=self.port.daemon.host.name, with_threads=True)
            pl.select_tid(self.port.linux_tid)
        elif uri == "#show_pl_th" and "qnx" in self.port.daemon.architecture:
            pg = self.parent.gui.pg
            pl = pg.show_process_list(host=self.port.daemon.host.name, with_threads=True)
            qnx_tid = self.port.daemon.pid, self.port.thread_handle
            pl.select_tid(qnx_tid)
        else:
            self.error("can't go to thread: %r" % ((uri, self.port.linux_tid, self.port.thread_handle), ))

        return True

    def on_md_tv_button_press_event(self, tv, ev):
        if not ev.button == 3: 
            return False
        res = tv.get_path_at_pos(int(ev.x), int(ev.y))
        if not res: 
            return False
        path, col, xp, yp = res
        self.selected_path = path
        self.selected_value = tv.get_model()[path][0]
        self.value_popup.popup(None, None, None, None, ev.button, ev.time)
        return True

    def open_ln_topic_scope(self, topic, value):
        self.gui.manager_proxy("open_scope", topic, value)

    def _open_ln_topic_scope_for_path(self, path):
        tv = self.md_tv
        m = tv.get_model()
        name = []
        iter = m.get_iter(path)
        while iter is not None:
            name.insert(0, m[iter][0])
            iter = m.iter_parent(iter)
            if iter is None:
                break
        name = ".".join(name)
        self.open_ln_topic_scope(self.topic.name, name)
        return True

    def on_md_tv_row_activated(self, tv, path, col):
        return self._open_ln_topic_scope_for_path(path)

    def on_value_show_in_pyscope_activate(self, item):
        return self._open_ln_topic_scope_for_path(self.selected_path)

    def on_md_tv_row_expanded(self, tv, iter, path):
        m = tv.get_model()
        if m.iter_n_children(iter) == 1:
            child = m.iter_children(iter)
            tv.expand_row(m.get_path(child), False)
        return True
