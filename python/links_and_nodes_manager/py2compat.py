import sys
import inspect
import collections

is_py2 = sys.version_info < (3, 0)

if is_py2:
    unicode = unicode # noqa: F821
    long = long # noqa: F821
    raw_input = raw_input # noqa: F821
    cmp = cmp # noqa: F821
    collections.abc = collections
else:
    unicode = str
    long = int
    raw_input = input

    def cmp(x, y):
        if x < y:
            return -1
        elif x > y:
            return 1
        else:
            return 0

class PyCompatArgSpec:
    pass

def inspect_getfullargspec(fcn): # todo: remove this py3 wrapper when py2 is dropped!
    if not is_py2:
        return inspect.getfullargspec(fcn)
    # emulate getfullargspec!
    args = inspect.getargspec(fcn)
    compat_args = PyCompatArgSpec()
    compat_args.args = args.args
    compat_args.varargs = args.varargs # todo?
    compat_args.varkw = args.keywords
    compat_args.defaults = args.defaults
    return compat_args
