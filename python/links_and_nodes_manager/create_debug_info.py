from __future__ import print_function

import io
import os
import sys
import time
import stat
import pickle
import datetime
import tarfile
import traceback

import yaml

import links_and_nodes_base

from . import Logging
from . import SystemConfiguration
from . import ClientStore
from . import TopicStore
from . import DaemonStore
from . import py2compat

def get_inmem_fileobj(arg=None):
    if arg is None:
        return io.BytesIO()
    if not isinstance(arg, bytes):
        arg = arg.encode("utf-8")
    return io.BytesIO(arg)

def _pickle(obj):
    return pickle.dumps(obj, protocol=pickle.HIGHEST_PROTOCOL)

def client_id(client):
    return "%s@%s:%s" % (client.program_name, client.host.name, client.address[1])

our_dumper = yaml.SafeDumper
if not py2compat.is_py2:
    def Signals_representer(dumper, data):
        return dumper.represent_data(int(data))
    import signal
    our_dumper.add_representer(signal.Signals, Signals_representer)

class debug_info(object):
    def __init__(self, mgr, name_hint=None):
        self.mgr = mgr
        self.fp = get_inmem_fileobj()
        self.tar = tarfile.open(mode="w:bz2", fileobj=self.fp, dereference=False)
        if name_hint is None:
            name_hint = "lnm_debug_info"
        self.prefix = "%s_%s" % (name_hint, datetime.datetime.now().strftime("%Y%m%d%H%M%S"))
        
    def get(self):
        self.tar.close()
        return self.fp.getvalue()
    
    def _prefixed(self, fn):
        return "%s/%s" % (self.prefix, fn)

    def _fill_default_tarinfo(self, ti):
        cti = self.tar.gettarinfo(self.mgr.sysconf.configuration_file)
        for attr in "uid,gid,uname,gname,mode".split(","):
            setattr(ti, attr, getattr(cti, attr))
        ti.mode &= ~stat.S_IXUSR & ~stat.S_IXGRP & ~stat.S_IXOTH
    def _add_file(self, name, data):
        """
        data should by bytes
        if data is str, it will be utf-8 encoded
        """
        ti = tarfile.TarInfo(self._prefixed(name))
        self._fill_default_tarinfo(ti)
        ti.type = tarfile.REGTYPE
        ti.size = len(data)
        ti.mtime = time.time()
        self.tar.addfile(ti, fileobj=get_inmem_fileobj(data))

        
    def add_config_files(self, prefix="config"):
        sc = self.mgr.sysconf
        ti = tarfile.TarInfo(self._prefixed("main.lnc"))
        self._fill_default_tarinfo(ti)
        ti.size = 0
        ti.type = tarfile.SYMTYPE
        ti.linkname = prefix + sc.configuration_file
        self.tar.addfile(ti)
        for fn in sc.included_files:
            self.tar.add(fn, self._prefixed(prefix + fn))
        
        for md_dir in links_and_nodes_base.message_definition_dirs:
            md_dir = os.path.realpath(md_dir)
            try:
                self.tar.add(md_dir, self._prefixed(prefix + md_dir))
            except Exception:
                self.mgr.warning("could not read md-dir %r: %s" % (md_dir, sys.exc_info()[1]))

    def add_msg_defs(self, prefix="msg_defs"):
        sc = self.mgr.sysconf
        for md_dir in links_and_nodes_base.message_definition_dirs[::-1]: # in reverse, such that search order will persist in fs
            md_dir = os.path.realpath(md_dir)
            try:
                self.tar.add(md_dir, self._prefixed(prefix))
            except Exception:
                self.mgr.warning("could not read md-dir2 %r: %s" % (md_dir, sys.exc_info()[1]))

    def _flatten(self, obj):
        #warning("save all", "save obj of type %s" % obj.__class__.__name__)
        if isinstance(obj, (str, int, float)):
            return obj
        if isinstance(obj, (tuple, list)):
            return list(map(self._flatten, obj))
        if isinstance(obj, dict):
            ret = dict()
            for k, v in list(obj.items()):
                kf = self._flatten(k)
                #warning("save all", "now flatten dict value for key %r" % kf)
                vf = self._flatten(v)
                try:
                    ret[kf] = vf
                except Exception:
                    import pprint
                    pprint.pprint(obj)
                    print(repr(k))
                    print(repr(kf))
                    raise
            return ret
        
        for klass in "Process, State, Group, Host, Network, Plugin".split(", "):
            if isinstance(obj, getattr(SystemConfiguration, klass)):
                return "<%s: %s>" % (klass, obj.name)
        if isinstance(obj, SystemConfiguration.HostInterface):
            return "<HostInterface %s, %s>" % (obj.get_full_name(), obj.ip_address)
        if isinstance(obj, ClientStore.ln_service):
            svc = dict()
            svc.update(obj.__getstate__())
            del svc["name"]
            return self._flatten(svc)
        if isinstance(obj, TopicStore.Topic):
            t = obj.__getstate__()
            if t["publisher"] is not None:
                t["publisher"] = client_id(t["publisher"])
            return self._flatten(t)
        if isinstance(obj, DaemonStore.DaemonStore):
            s = obj.__getstate__()
            s["sources"] = dict(obj.sources)
            return self._flatten(s)
        if isinstance(obj, ClientStore.ClientStore):
            s = obj.__getstate__()
            del s["_log"]
            p = obj.process
            if p is not None:
                s["process"] = p.name
            s["provided_services"] = sorted(s["provided_services"].keys())
            return self._flatten(s)
        return repr(obj)
    
    def _yaml(self, data):
        return yaml.dump(self._flatten(data), Dumper=our_dumper, default_flow_style=False, width=1024)
    
    def add_defines(self, name="defines.yaml"):
        defs = self.mgr.sysconf.defines
        keys = list(defs.keys())
        keys.sort()
        
        l = []
        for key in keys:
            value = defs[key]
            l.append((key, value))
        self._add_file(name, self._yaml(dict(l)))

    def _get_config_keys(self, obj, yaml=True):
        data = dict()
        data["name"] = obj.name
        for key in obj._valid_config_keys:
            data[key] = getattr(obj, key, None)
        for key in obj._valid_config_list_keys:
            value = getattr(obj, key)
            if key == "environment":
                value = ["%s=%s" % (k, v) for k, v in value]
            data[key] = value
        if hasattr(obj, "flags"):
            data["flags"] = obj.flags
        data["config_stack"] = ["%s:%s" % (fn, ln) for fn, ln in obj._sysconf_stack]
        if yaml:
            return self._yaml(data)
        return data


    def _get_process_state(self, p):
        data = dict()
        for attr in "host,state,requested_state,pid,_errors,_indirect_errors".split(","):
            data[attr] = getattr(p, attr)            
        return self._yaml(data)
    def _get_state_state(self, p):
        data = dict()
        for attr in "host,state,requested_state,last_check".split(","):
            data[attr] = getattr(p, attr)            
        return self._yaml(data)
    
    def add_procs(self, prefix="procs"):
        sc = self.mgr.sysconf
        self._add_file("instance_config.yaml", self._get_config_keys(sc.instance_config))
        for pn in sorted(sc.processes.keys()):
            obj = sc.processes[pn]
            pdir = prefix + "/" + pn + "/"
            self._add_file(pdir + "output", b"".join(obj.last_output))
            self._add_file(pdir + "config.yaml", self._get_config_keys(obj))
            self._add_file(pdir + "state.yaml", self._get_process_state(obj))
        for pn in sorted(sc.states.keys()):
            obj = sc.states[pn]
            pdir = prefix + "/" + pn + "/"
            self._add_file(pdir + "output", b"".join(obj.last_output))
            self._add_file(pdir + "config.yaml", self._get_config_keys(obj))
            self._add_file(pdir + "state.yaml", self._get_state_state(obj))
            
    def add_groups(self, name="groups.yaml"):
        sc = self.mgr.sysconf
        for gn in sorted(sc.groups.keys()):
            g = sc.groups[gn]

        def get_toplevel_groups():
            group_parents = {}
            def add_groups(members, parent=None):
                for member in members:
                    if not isinstance(member, SystemConfiguration.Group):
                        continue
                    if group_parents.get(member) is None:
                        group_parents[member] = parent
                    add_groups(member.group_members, member)                
            add_groups(list(sc.groups.values()))
            return sorted([g.name for g, p in [i for i in iter(group_parents.items()) if i[1] is None]])

        def list_group(g):
            childs = []
            for m in g.group_members:
                if isinstance(m, SystemConfiguration.Group):
                    childs.append({m.name: list_group(m)})
                else:
                    childs.append(m.name)                    
            return childs
        groups = []
        for gn in get_toplevel_groups():
            g = sc.groups[gn]
            groups.append({g.name: list_group(g)})
        self._add_file(name, self._yaml(groups))

    def add_log(self):
        # log_head, now, level, subsystem, msg, stack
        fp = io.StringIO()
        for log_head, ts, level, subsys, msg, stack in Logging.log_buffer:
            head = "%-23.23s %-8.8s %-25.25s " % (
                ts, level, subsys)
            indent = u" " * len(head)
            if isinstance(msg, bytes):
                msg = msg.decode("utf-8")
            fp.write(u"%s%s\n" % (head, msg.strip().replace(u"\n", u"\n%s" % indent)))
        self._add_file("manager.log", fp.getvalue())

    def add_networks(self, name="networks.yaml"):
        nets = dict()
        for nn, net in self.mgr.sysconf.networks.items():
            nets[nn] = self._get_config_keys(net, yaml=False)
            nets[nn]["interfaces"] = self._flatten(net.interfaces)
        self._add_file(name, self._yaml(nets))

    def _get_host(self, host):
        data = dict()
        for attr in "change_user_to,fqdns,daemon_private_key_file,daemon_public_key_file,aliases,interfaces,daemon_start_script,additional_ssh_arguments,preferred_network,process_default_templates,arch,arbiter_port".split(","):
            data[attr] = getattr(host, attr)            
        return data

    def add_hosts(self, name="hosts.yaml"):
        hosts = dict()
        for hn, host in self.mgr.sysconf.hosts.items():
            hosts[hn] = self._get_host(host)
        self._add_file(name, self._yaml(hosts))

    def add_sysconf(self):
        self._add_file("sysconf.pickle", _pickle(self.mgr.sysconf))

    def add_manager(self):
        import inspect
        fn = inspect.getsourcefile(self.mgr.__class__)
        fn = os.path.realpath(fn)
        mgr_dir = os.path.dirname(fn)
        def check_exclude(ti):
            if ti.name.endswith(".pyc"):
                return
            return ti
        try:
            self.tar.add(mgr_dir, self._prefixed("manager"), filter=check_exclude)
        except Exception:
            self.mgr.warning("could not read mgr_dir %r: %s" % (mgr_dir, sys.exc_info()[1]))

    def add_services(self, name="services.yaml"):
        services = {}
        for client in self.mgr.iterate_clients():
            for svc in list(client.provided_services.values()):
                services[svc.name] = svc
        self._add_file(name, self._yaml(services))
    
    def add_topics(self, name="topics.yaml"):
        topics = {}
        for topic_name, topic in self.mgr.topics.items():
            topics[topic_name] = topic
        self._add_file(name, self._yaml(topics))
        
    def add_clients(self, name="clients.yaml"):        
        data = {}
        for client in self.mgr.iterate_clients():
            data[client_id(client)] = client
        data["now"] = time.time()
        self._add_file(name, self._yaml(data))
        
    def add_daemons(self, name="daemons.yaml"):        
        data = {}
        for daemon_name, daemon in self.mgr.daemons.items():
            data[daemon.host.name] = daemon
        self._add_file(name, self._yaml(data))
        
def create_debug_info(mgr, name_hint=None):
    """
    return tarball file contents that include as much of debugging information as possible.

    debugging info to include:
    - all used ln-config files & msg-def dirs
    - all known process output of all states/processes
      together with all properties of that object
    - current values of all config defines
    - as many log messages as known
    - list of known networks with all their properties
    - list of known hosts with all their properties
    - pickle of sysconf
    - list of known topics with all their properties
    - list of known service with all their properties
    - list of known clients with all their properties
    - list of known daemons with all their properties
    """
    di = debug_info(mgr, name_hint=name_hint)
    for module in "config_files,msg_defs,defines,procs,groups,log,networks,hosts,sysconf,manager,services,topics,clients,daemons".split(","):
        method = getattr(di, "add_" + module)
        try:
            method()
        except Exception:
            tb = traceback.format_exc()
            mgr.error("could not append %r to debug-info:\n%s" % (module, tb))
            di._add_file("error_%s.tb" % module, tb)
    return di.get()
