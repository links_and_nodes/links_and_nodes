from __future__ import print_function

import sys
import traceback
import os

# https://docs.python.org/2/reference/datamodel.html#types

def start(tb=None):
    import inspect
    import readline
    import atexit

    histfile = os.path.join(os.path.expanduser("~"), ".tb_helper")
    if os.path.isfile(histfile):
        readline.read_history_file(histfile)
        # default history len is -1 (infinite), which may grow unruly
        readline.set_history_length(1000)
    atexit.register(readline.write_history_file, histfile)

    def usage():
        print ("commands:\n"
           "  u - go one stack frame Up\n"
           "  d - go one stack frame Down\n"
           "  l - print local variable names\n"
           "  p <EXPR> - evaluate python <EXPR> in this frame and print result\n"
           "  e <EXPR> - execute python <EXPR> in this frame and print result\n"
           "  bt - print backtrace\n"
           "  q - quit tb_helper\n"
           "  press just enter to repeat last command\n"
        )
    
    if tb is None:
        t = sys.exc_info()
        tb = t[2]
    print(sys.exc_info()[1])
    print("press enter to print stacktrace!")
    input()
    traceback.print_tb(tb)

    first = True
    stack = []
    last_i = "down"
    last_tb = None
    while True:
        frame = tb.tb_frame
        line = tb.tb_lineno
        fn = inspect.getfile(frame.f_code)
        print("%s, line %d" % (fn, line))
        if tb != last_tb:
            last_tb = tb
            srcs, first = inspect.getsourcelines(frame)
            for idx, f in enumerate(srcs):
                if first + idx == line:
                    m = ">>>>"
                else:
                    m = ":   "
                sys.stdout.write("%4d%s %s" % (first + idx, m, f))

        if first:
            first = False
            usage()

        i = input("%d>> " % len(stack))
        i = i.strip()
        
        if i == "":
            i = last_i

        if i == "q":
            return
        if i.startswith("p "):
            e = i[2:].strip()
            print(e)
            try:
                ret = eval(e, frame.f_locals, frame.f_locals)
                print(repr(ret))
            except Exception:
                print(traceback.format_exc())
        elif i.startswith("e ") or i.startswith("exec ") :
            e = i.split(" ", 1)[0].strip()
            print(e)
            try:
                exec(e, frame.f_locals)
            except Exception:
                print(traceback.format_exc())
        elif i == "bt":
            traceback.print_tb(tb)
        elif i == "btf":
            if stack:
                traceback.print_tb(stack[0])
            else:
                traceback.print_tb(tb)
        elif i == "down" or i == "d":
            stack.append(tb)
            tb = tb.tb_next
        elif i == "up" or i == "u":
            if stack:
                tb = stack[-1]
                del stack[-1]
            else:
                print("already on top")
        elif i == "locals" or i == "l":
            l = frame.f_locals
            keys = list(l.keys())
            keys.sort()
            N = 120
            c = 0
            print("locals:")
            for k, item in enumerate(keys):
                s = str(item)
                if c + len(s) > N:
                    sys.stdout.write("\n")
                    c = 0
                if k != 0:
                    sys.stdout.write(", ")
                sys.stdout.write(s)
                c += len(s)
            print()
        else:
            print("invalid input %r" % i)
            continue
        last_i = i

if __name__ == "__main__":
    # test
    maxdepth = 100
    def funA():
        lb1 = maxdepth
        funB()
        lb2 = maxdepth
        funC()
    def funB():
        lb = maxdepth
        funC()
    def funC():
        global maxdepth
        maxdepth -= 1
        if maxdepth == 0:
            raise Exception("stop here")
        funA()
        
    try:
        funA()
    except Exception:
        start() # start tb helper
