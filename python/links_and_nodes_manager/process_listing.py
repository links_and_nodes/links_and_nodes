"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

from __future__ import print_function

import pprint
import os
import datetime
import signal
from operator import itemgetter
import functools

from . import config

from .CommunicationUtils import get_cpu_affinity_mask
from .Logging import enable_logging_on_instance

from .py2compat import unicode, cmp

from . import qnx_info
from . import linux_info

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
from gi.repository import GObject, Gtk, GLib # noqa: E402

class process_list(object):
    _signal_field = {
        "qnx-intel-gcc335": "pid",
        "linux-intel-gcc4": "tid",
        "vxworks": "pid",
        }
    _tid_field = {
        "qnx-intel-gcc335": "tid",
        "linux-intel-gcc4": "tid",
        "vxworks": "tid",
        }
    _sched_field = {
        "qnx-intel-gcc335": "policy",
        "linux-intel-gcc4": "sched",
        "vxworks": None,
        }
    _prio_field = {
        "qnx-intel-gcc335": "real_priority",
        "linux-intel-gcc4": "rtprio",
        "vxworks": "priority",
        }
    _last_cpu_field = {
        "qnx-intel-gcc335": "last_cpu",
        "linux-intel-gcc4": "processor",
        "vxworks": "cpu",
        }
    _filter_field = {
        "qnx-intel-gcc335": "path",
        "linux-intel-gcc4": ("cmd", "cmdline"),
        "vxworks": "name",
        }
    _filter_field2 = {
        "qnx-intel-gcc335": "pid",
        "linux-intel-gcc4": ("tgid", "tid"),
        "vxworks": "tid",
        }
    _aliases = {
        "processor": "cpu",
        "last_cpu": "cpu",
        "nlwp": "#thr",
        "num_threads": "#thr",
        }

    _preferred_order = {
        "vxworks": [
            "pid",
            "tid",
            "rtpId",
            "name",
            "obj_name",
            "priority",
            "cpu_affinity",
            "cpu",
            "status",
            "errno",
            "currentCtx",
            "owner_id",
            "task_owner",
        ],
        "qnx-intel-gcc335": [
            "pid",
            "tid",
            "parent",
            "sibling",
            "child",
            
            "last_cpu",
            "num_threads",
            "syscall",
            "last_chid",
            "state",
            
            "proc_prio",
            "priority",
            "real_priority",
            "policy",
            
            "sutime",
            "path",
            
            "utime",
            "stime",
            "start_time",
            
            "stksize",

            "proc_start_time",

            "proc_flags",
            "tid_flags",

            "uid",
            "gid",
            "umask",
            "what",
            "why"
            
            ],

        "linux-intel-gcc4": [
            # process ids
            'tgid', # process
            'tid',  # thread 
            'ppid', 

            'euser', 
            

            # state
            'processor', 
            'nlwp', 
            'wchan',
            'state', 

            'utime', 
            'stime', 

            'cmd', 

            # priorities
            'nice', 
            'priority', 
            'rtprio', 
            'sched', 

            'start_time', 

            # memory info
            'size', 'resident', 'share', 
            'vm_size', 'vm_rss', 'vm_stack', 'vm_data', 'vm_exe', 'vm_lib', 'vm_lock', 

            'dt', 

            'cmdline', 


            # other id's
            'exit_signal', 
            'flags', 
            'pgrp', 
            'session', 
            'tpgid', 
            'tty', 

            # usage timings
            'cutime', 
            'cstime', 
            'maj_flt', 'min_flt', 

            # user/group ids/names
            'euid', 
            'fuid', 'fuser', 
            'ruid', 'ruser', 
            'suid', 'suser', 

            'egid', 'egroup', 
            'fgid', 'fgroup', 
            'rgid', 'rgroup', 
            'sgid', 'sgroup', 


            ]
        }
    _tooltips = {
        "qnx-intel-gcc335": {
            "proc_start_time": "start-time of process",
            "start_time": "start-time of thread",
            "utime": "user time for complete process",
            "stime": "system time for complete process",
            "cutime": "terminted children user time",
            "cstime": "terminted children system time",
            "proc_priority": "process base priority",
            "sutime": "thread system + user running time",


            "priority": "The priority the thread is actually running at (e.g. its priority may have been boosted).",
            "real_priority": "The actual priority the thread would be at with no boosting and so on.",
            "policy": "The scheduling policy",
            "syscall": "The last system call",
            "last_cpu": "The processor the thread last ran on.",
            "last_chid": "The ID of the last channel this thread received a message on.",
            "tid_flags": "The thread flags",

            },

        "linux-intel-gcc4": {

            'cmd': "basename of executable file in call to exec(2)",
            'cmdline': "command line string vector (/proc/#/cmdline)",
            "environ": "environment string vector (/proc/#/environ)",
            'cstime': "cumulative stime of process and reaped children",
            'cutime': "cumulative utime of process and reaped children",
            'dt': "# dirty pages",
            'egid': "eff. group id",
            'egroup': "eff. group",
            'euid': "eff. user id",
            'euser': "eff. user",
            'exit_signal': "",
            'fgid': "file system gid",
            'fgroup': "file system group",
            'flags': "kernel flags for the process",
            'fuid': "file system uid",
            'fuser': "file system user",
            'maj_flt': "# of majer page faults",
            'min_flt': "# of minor page faults",
            'nice': "standard unix nice level of process",
            'nlwp': "number of threads",
            'pgrp': "process group id",
            'ppid': "pid of parent process",
            'priority': "kernel scheduling priority",
            'processor': "current (or most recent?) CPU",
            'resident': "number of resident set (non-swapped) pages (4k)",
            'rgid': "real group id",
            'rgroup': "real group",
            'rtprio': "real-time priority",
            'ruid': "real user id",
            'ruser': "real user",
            'sched': "scheduling class",
            'session': "session id",
            'sgid': "saved group id",
            'sgroup': "saved group",
            'share': "number of pages of shared (mmap'd) memory",
            'size': "total # of pages of memory",
            'start_time': "start time of process",
            'state': "process state (S=sleeping)",
            'stime': "kernel-mode CPU time accumulated by process",
            'suid': "saved user id",
            'suser': "saved user",
            'tgid': "task group ID, the POSIX PID (see also: tid)",
            'tid': "task id, the POSIX thread ID (see also: tgid)",
            'tpgid': "terminal process group id",
            'tty': "full device number of controlling terminal",
            'utime': "user-mode CPU time accumulated by process",
            'vm_data': "data size",
            'vm_exe': "executable size",
            'vm_lib': "library size (all pages, not just used ones)",
            'vm_lock': "locked pages size",
            'vm_rss': "resident set size",
            'vm_size': "size of virtual memory",
            'vm_stack': "stack size",
            'wchan': "address of kernel wait channel proc is sleeping in"
            },
        }

    """
    to remove
    vsize replaced by vm_size
    
    """

    _formatters = {
        "qnx-intel-gcc335": {
            "start_time": "_nsec_time_to_string",
            "proc_start_time": "_nsec_time_to_string",
            "sutime": "_from_nsecs",
            "utime": "_from_nsecs",
            "stime": "_from_nsecs",
            "cutime": "_from_nsecs",
            "cstime": "_from_nsecs",

            "proc_flags": "_to_hex",

            "policy": "_from_qnx_sched_policy",
            "state": "_from_qnx_state",
            "syscall": "_from_qnx_syscall",

            "tid_flags": "_from_qnx_thread_flags",
            "flags": "_from_qnx_debug_flags",
            },
        
        "linux-intel-gcc4": {
            'start_time': "_unix_time_to_string",
            "flags": "_to_hex",
            "tty": "_to_hex",
            "vm_data": "_from_kbytes",
            "vm_exe": "_from_kbytes",
            "vm_lib": "_from_kbytes",
            "vm_lock": "_from_kbytes",
            "vm_size": "_from_kbytes",
            "vm_rss": "_from_kbytes",
            "vm_stack": "_from_kbytes",
            "utime": "_from_jiffies",
            "stime": "_from_jiffies",
            "cutime": "_from_jiffies",
            "cstime": "_from_jiffies",
            "sched": "_from_linux_sched_policy",
            },
        "vxworks": {
            "pid": "_to_hex",
            "tid": "_to_hex",
            "rtpId": "_to_hex",
            "entry": "_to_hex",
            "status": "_from_vxworks_status",
            "cpu_affinity": "_from_affinity_mask",
            "owner_id": "_to_hex",
            "errno": "_to_hex",
            "task_owner": "_to_hex",
            "currentCtx": "_to_hex",
        }
    }

    _arch_aliases = {
        "vxworks6.8-x86-gcc4.x": "vxworks",
        "sled11-x86-gcc4.x": "linux-intel-gcc4",
        "sled11-x86_64-gcc4.x": "linux-intel-gcc4",
        "osl42-x86_64": "linux-intel-gcc4",
        "osl15-x86_64": "linux-intel-gcc4",
        "arm-angstrom-linux-gnueabi": "linux-intel-gcc4",
        "powerpc-fsl-linux": "linux-intel-gcc4",
        "qnx6.5-x86-gcc4.x": "qnx-intel-gcc335",
        "qnx6.3-x86-gcc3.3": "qnx-intel-gcc335",
        "qnx6.4-powerpc-gcc4.x": "qnx-intel-gcc335", ##uhh?
        "ubuntu12.04-armhf-gcc4.x": "linux-intel-gcc4",
    }

    def __init__(self, parent, host, with_threads=False):
        enable_logging_on_instance(self, "PL %s" % host)
        self.parent = parent
        self.host = host
        self._timer = None
        self._sort_refresh_timer = None

        self.xml = Gtk.Builder()
        resources_dir = os.path.join(config.manager_base_dir, "resources")
        self.xml_fn = os.path.join(resources_dir, "process_list_window.ui")
        self.xml.add_from_file(self.xml_fn)
        self.xml.connect_signals(self)
        m = self.policy_model = Gtk.ListStore(GObject.TYPE_STRING)
        cb = self.policy_cb
        cb.set_model(m) # cursor-changed-fine
        cr = Gtk.CellRendererText()
        cb.pack_start(cr, True)
        cb.add_attribute(cr, "text", 0)
        self.policy_arch = None

        self.threads_btn.set_active(with_threads)
        self.set_prio_dialog.connect("response", self.on_set_prio_response)

        self.processlist_popup = self.xml.get_object("processlist_popup")
        self.processlist_window.set_title("%s process list" % host)
        self.processlist_window.show()

        self._refresh_in_progess = False
        self._is_tv_initialized = False

        self.selected_task = None
        self.selected_task_is_tid = False
        self._sort_field = None
        self._sort_desc = False
        self.last = None
        self.last_filter = None
        self._architecture = None

        s = self.pltv.get_selection()
        s.set_mode(Gtk.SelectionMode.MULTIPLE)

        self.refresh()

    def __getattr__(self, name):
        if hasattr(self, "xml"):
            widget = self.xml.get_object(name)
            if widget is not None:
                return widget
        raise AttributeError(name)

    def present(self, with_threads=False):
        if with_threads and not self.threads_btn.get_active():
            self.threads_btn.set_active(True)
            self.refresh()
        self.processlist_window.present()

    def on_processlist_window_delete_event(self, *args):
        if self._timer is not None:
            GLib.source_remove(self._timer)
        self.processlist_window.hide()
        del self.xml
        del self.parent.process_lists[self.host]
        return True

    def on_refresh_btn_clicked(self, btn):
        self.refresh()

    def _status(self, msg):
        self.status_label.set_text(msg)
        while Gtk.events_pending():
            Gtk.main_iteration_do(False)
        
    def next_refresh(self):
        if self._sort_refresh_timer is not None:
            GLib.source_remove(self._sort_refresh_timer)
            self._sort_refresh_timer = None
        if self._refresh_in_progess: 
            return True
        self.refresh()
        return self.auto_refresh_spin.get_value() > 0.0

    def refresh(self):
        if self._refresh_in_progess: 
            self._status("retrieving process list...")
            return
        self._refresh_in_progess = True        
        self._status("retrieving process list...")
        # send request and register for answer...
        self.parent.manager_proxy(
            "retrieve_process_list",
            host=self.host, show_threads=self.threads_btn.get_active()
        )

    def on_query_tooltip(self, widget, x, y, keyboard_tip, tooltip):
        bin_x, bin_y = widget.convert_widget_to_bin_window_coords(x, y)
        result = widget.get_path_at_pos(bin_x, bin_y)
        if result is None:
            return False
        path, column, cell_x, cell_y = result
        if column is None:
            return False
        if column not in self.tooltips:
            return False
        tooltip.set_text(self.tooltips[column])
        widget.set_tooltip_cell(tooltip, None, column, None)
        return True

    @staticmethod
    def _from_qnx_sched_policy(v):
        return qnx_info.policies.get(v, str(v))

    @staticmethod
    def _from_linux_sched_policy(v):
        return linux_info.policies.get(v, str(v))

    @staticmethod
    def _from_vxworks_status(v):
        vxworks_status = {
            0: "READY",
            2: "PENDING",
            4: "DELAY",
            6: "PENDING+T",
            
            }
        return vxworks_status.get(v, v)

    @staticmethod
    def _from_affinity_mask(v):
        if isinstance(v, (str, unicode)):
            raise Exception("invalid cpu affinity: %r" % v)
        cpus = []
        for i in range(32):
            tv = 2 ** i
            if v & tv:
                cpus.append(str(i))
        return ",".join(cpus)

    def _from_qnx_state(self, v):
        return qnx_info.states.get(v, str(v))

    def _from_qnx_syscall(self, v):
        return qnx_info.syscalls.get(v, str(v))

    def _from_qnx_thread_flags(self, v):
        f = []
        for k, n in qnx_info.thread_flags.items():
            if k & v:
                f.append(n)
        return ", ".join(f)
    def _from_qnx_debug_flags(self, v):
        f = []
        for k, n in qnx_info.debug_flags.items():
            if k & v:
                f.append(n)
        return ", ".join(f)
    
    def _unix_time_to_string(self, v):
        return datetime.datetime.fromtimestamp(v).strftime("%Y-%m-%d %H:%M")

    def _nsec_time_to_string(self, v):
        return datetime.datetime.fromtimestamp(v / 1e9).strftime("%Y-%m-%d %H:%M")

    @staticmethod
    def _to_hex(v):
        if v < 0xffff:
            return "0x%04x" % v
        if v < 0xffffffff:
            return "0x%08x" % v
        try:
            return "0x%x" % v
        except Exception:
            return v

    def _from_bytes(self, v):
        if v < 2**10:
            return "%dB" % v
        if v < 2**20:
            return ("%.3f" % (v / 2.**10)).rstrip(".0") + "kB"
        if v < 2**30:
            return "%.3fMB" % (v / 2.**20)
        return "%.3fGB" % (v / 2.**30)
    def _from_kbytes(self, v):
        return self._from_bytes(v * 1024)
    
    def _duration_from_seconds(self, v):
        if v < 3600:
            m = int(v / 60)
            s = v - m * 60
            return "%02d:%05.2f" % (m, s)
        if v < 3600 * 24:
            h = int(v / 3600)
            v -= h * 3600
            m = int(v / 60)
            v -= m * 60
            s = v
            return "%02d:%02d:%05.2f" % (h, m, s)
        d = int(v / 3600 / 24)
        v -= d * 3600 * 24
        h = int(v / 3600)
        v -= h * 3600
        m = int(v / 60)
        v -= m * 60
        s = v
        return "%d Days, %02d:%02d:%05.2f" % (d, h, m, s)
            
    def _from_jiffies(self, v):
        s = v / 100.
        return self._duration_from_seconds(s)

    def _from_nsecs(self, v):
        return self._duration_from_seconds(v / 1e9)

    def _init_tv(self, headers):
        self._is_tv_initialized = True
        keys = list(headers)
        def mycmp(a, b):
            a_in = a in self._preferred_order
            b_in = b in self._preferred_order
            if not a_in and not b_in:
                return cmp(a, b)
            if a_in and not b_in:
                return -1
            if not a_in and b_in:
                return 1
            ai = self._preferred_order.index(a)
            bi = self._preferred_order.index(b)
            if ai < bi:
                return -1
            if ai == bi:
                return 0
            return 1
        keys.sort(key=functools.cmp_to_key(mycmp))
        self._keys = keys
        self._key_map = []
        for k in self._keys:
            self._key_map.append(headers.index(k))

        types = [GObject.TYPE_STRING] * len(keys)
        self.model = Gtk.TreeStore(*types)
        tv = self.pltv

        self.tooltips = {}
        tv.set_property("has-tooltip", True)
        tv.connect("query-tooltip", self.on_query_tooltip)
        self.formatters = {}
        self._tvcs = []
        for i, k in enumerate(self._keys):
            cr = Gtk.CellRendererText()
            title = self._aliases.get(k, k).replace("_", "__")
            tvc = Gtk.TreeViewColumn(title)
            tvc.pack_start(cr, True)
            tvc.set_attributes(cr, text=i)
            tvc.set_sizing(Gtk.TreeViewColumnSizing.GROW_ONLY)
            tvc.set_clickable(True)
            tvc.connect("clicked", self.on_header_clicked, k)
            self._tvcs.append(tvc)
            tv.append_column(tvc)
            if k in self._tooltips.get(self._architecture, {}):
                self.tooltips[tvc] = self._tooltips[self._architecture][k]
            if k in self._formatters:
                self.formatters[k] = getattr(self, self._formatters[k])
            else:
                self.formatters[k] = str
        self.model.clear()
        self._last_rows = 0


    def on_header_clicked(self, tvc, k):
        if k == self._sort_field:
            if not self._sort_desc:
                self._sort_desc = True
            else:
                i = self._keys.index(self._sort_field)
                self._tvcs[i].set_property("sort-indicator", False)
                self._sort_field = None
        else:
            # change sort field index
            if self._sort_field is not None:
                i = self._keys.index(self._sort_field)
                self._tvcs[i].set_property("sort-indicator", False)
            self._sort_field = k
            self._sort_desc = False
        if self._sort_field is not None:
            i = self._keys.index(self._sort_field)
            if self._sort_desc:
                self._tvcs[i].set_property("sort-order", Gtk.SortType.DESCENDING)
            else:
                self._tvcs[i].set_property("sort-order", Gtk.SortType.ASCENDING)
            self._tvcs[i].set_property("sort-indicator", True)

        if self._timer is None:
            if self._sort_refresh_timer is not None:
                GLib.source_remove(self._sort_refresh_timer)
            self._sort_refresh_timer = GLib.timeout_add(1000, self.refresh)
            
    @staticmethod
    def unpack_vxworks_processlist(pl):
        # unpack vxworks processlist
        lines = pl.strip().split("\n")

        procs = []
        [procs.extend([eval(field) for field in line.split(",")]) for line in lines[1:]]

        pl = [
            lines[0].split(","), 
            procs # []
        ]
        return pl

    def _set_process_list(self, pl, architecture):
        self._refresh_in_progess = False
        if pl is None:
            self.error("got None-processlist!")
            return
        if self._architecture is None:
            # select arch
            self._architecture = process_list.get_arch(architecture)
            self._formatters = self._formatters[self._architecture]
            self._preferred_order = self._preferred_order[self._architecture]
            self._arch_policies = {}
            if self._architecture == "vxworks":
                self.policy_model = None
            else:
                m = self.policy_model
                m.clear()
                if "qnx" in self._architecture:
                    policies = dict(qnx_info.policies)
                elif "linux" in self._architecture:
                    policies = dict(linux_info.policies)
                else:
                    policies = {}
                for pvalue, policy in policies.items():
                    if not policy.startswith("SCHED_"):
                        policy = "SCHED_" + policy
                    m.append((policy, ))
                    self._arch_policies[policy] = pvalue
        org_pl = pl
        if self._architecture == "vxworks":
            pl = self.unpack_vxworks_processlist(pl)
        self.current = pl
        try:
            self._set_current()
        except Exception:
            print("org_pl:\n%s" % pprint.pformat(org_pl))
            raise

    def _set_current(self):
        pl = self.current
        if not self._is_tv_initialized:
            if not pl:
                self._status("daemon was built without support for process-lists!")
                return
            self._init_tv(pl[0])
            first_time = True
        else:
            first_time = False

        pl = pl[1]
        m = len(self._keys)
        n = int(len(pl) / m)
        pl = [pl[i * m:(i + 1) * m] for i in range(n)]

        filter_text = self.filter_entry.get_text()
        if filter_text:
            def do_filter(pl, filter_text, filter_fields):
                if not isinstance(filter_fields, tuple):
                    filter_fields = (filter_fields, )
                options = [self._key_map[self._keys.index(field)] for field in filter_fields]
                if isinstance(filter_text, str):
                    pl = [p for p in pl if any([filter_text in str(p[opt]) for opt in options])]
                else:
                    pl = [p for p in pl if filter_text in [p[opt] for opt in options]]
                return pl
            if filter_text.isdigit():
                filter_text = int(filter_text)
                filter_fields = self._filter_field2[self._architecture]
            else:
                filter_fields = self._filter_field[self._architecture]
            pl = do_filter(pl, filter_text, filter_fields)

        if self._architecture == "vxworks":
            # pre-sort by pid, tid
            idx = self._keys.index("tid")
            pl.sort(key=itemgetter(self._key_map[idx]))
            idx = self._keys.index("pid")
            pl.sort(key=itemgetter(self._key_map[idx]))
            

        if self._sort_field is not None:
            if self._sort_desc:
                rev = " descending"
            else:
                rev = ""
            self._status("%d processes sorted by %s%s" % (len(pl), self._sort_field, rev))
            self._sort_field_index = self._keys.index(self._sort_field)
            pl.sort(key=itemgetter(self._key_map[self._sort_field_index]), reverse=self._sort_desc)
        else:
            self._status("%d processes" % len(pl))

        #self.pltv.set_model(None)
        #self.model.clear()
        had_changes = False
        found_selected_task = None
        task_id_pid_i = None
        if self.selected_task_is_tid:
            task_id_i = self._keys.index(self._tid_field[self._architecture])
            if type(self.selected_task) == tuple and "linux" in self._architecture:
                self.selected_task = self.selected_task[1] # tid sufficient
            if type(self.selected_task) == tuple:
                task_id_pid_i = self._keys.index(self._signal_field[self._architecture])                
        else:
            task_id_i = self._keys.index(self._signal_field[self._architecture])
        i = 0
        for i, p in enumerate(pl):
            if task_id_pid_i is None:
                found = p[self._key_map[task_id_i]] == self.selected_task 
            else:
                found = p[self._key_map[task_id_pid_i]] == self.selected_task[0] and p[self._key_map[task_id_i]] == self.selected_task[1]
            if found:
                found_selected_task = i
            if self.last and len(self.last) > i and self.last[i] == p:
                continue
            had_changes = True
            row = []
            for ki, k in enumerate(self._keys):
                v = p[self._key_map[ki]]
                try:
                    row.append(self.formatters[k](v))
                except Exception:
                    print("formatter %r complained about value %r" % (self.formatters[k], v))
                    raise
            if i >= self._last_rows:
                self.model.append(None, row)
                self._last_rows += 1
            else:
                self.model[(i, )] = row
        while self._last_rows > i + 1:
            had_changes = True
            self.model.remove(self.model.get_iter((i + 1, )))
            self._last_rows -= 1
        if not len(pl):
            self.model.clear()
            self._last_rows = 0
            
        self.last = pl
        
        if first_time:
            self.ignore_cursor_change = True
            self.pltv.set_model(self.model) # cursor-changed-fine
            self.ignore_cursor_change = False
        
        if had_changes and self.selected_task is not None and found_selected_task is not None:
            # reselect the correct task!
            path = Gtk.TreePath((found_selected_task, ))
            if path != self.selected_task_path:
                self.pltv.set_cursor(path)
                self.selected_task_path = path
            
        #self.pltv.set_property("fixed-height-mode", True)

    def on_auto_refresh_spin_changed(self, btn):
        interval = btn.get_value()
        if self._refresh_in_progess: 
            return True
        if self._timer is not None:
            GLib.source_remove(self._timer)
            self._timer = None
        if interval > 0:
            self._timer = GLib.timeout_add(int(interval * 1000), self.next_refresh)
        return True

    def on_pltv_cursor_changed(self, tv):
        if self.ignore_cursor_change:
            return
        path, col = tv.get_cursor() # path-none-checked
        if path:
            i = self._keys.index(self._signal_field[self._architecture])
            self.selected_task = eval(self.model[path][i]) # get task_id
            self.selected_task_path = path
        else:
            self.selected_task = None

    def on_send_sig_activate(self, item):
        #n = item.get_name()
        n = Gtk.Buildable.get_name(item)
        if n.startswith("send_"):
            i = self._keys.index(self._signal_field[self._architecture])
            s = self.pltv.get_selection()
            m, paths = s.get_selected_rows()
            for path in paths:
                tid = m[path][i]
                signo = int(getattr(signal, n.split("_", 1)[1].upper()))
                self.parent.manager_proxy("signal_pid", host=self.host, pid=tid, signo=signo)

    class set_prio_task(object):
        def finished(self):
            self.pl.set_prio_dialog.hide()
            if not self.res:
                return
            res = "\n".join(self.res)
            dlg = Gtk.MessageDialog(
                self.pl.processlist_window,
                Gtk.DialogFlags.DESTROY_WITH_PARENT, 
                Gtk.MessageType.ERROR,
                Gtk.ButtonsType.OK,
                message_format="error(s) setting prio/policy for these pid's:\n%s" % res
            )
            dlg.run()
            dlg.hide()
            self.pl.set_prio_dialog.show()

        def on_response(self, exception=None):
            if exception is not None:
                self.res.append("pid %s tid %s: %s" % (self.pid, self.tid, exception))
            self.set_next_prio()

        def set_next_prio(self):
            if not self.pids:
                return self.finished()
            self.pid, self.tid = self.pids.pop(0)
            self.pl.parent.manager_proxy(
                "mgr_cb",
                "set_prio", self.on_response,
                host=self.pl.host,
                pid=self.pid, tid=self.tid, prio=self.prio, policy=self.pvalue
            )
    
    def on_set_prio_response(self, dialog, resp):
        if resp == 0:
            # cancel
            self.set_prio_dialog.hide()
            return
        
        task = self.set_prio_task()        
        task.pl = self
        task.pids = list(self.set_prio_pids)
        task.prio = int(self.prio_adjustment.get_value())
        if self.policy_model is not None:
            m = self.policy_model
            iter = self.policy_cb.get_active_iter()
            task.pvalue = self._arch_policies[m[iter][0]]
        else:
            task.pvalue = -1
        task.res = []
        task.set_next_prio()

    def on_set_prio(self, item):
        pids = []
        s = self.pltv.get_selection()
        m, paths = s.get_selected_rows()
        i = self._keys.index(self._signal_field[self._architecture])
        tid_i = self._keys.index(self._tid_field[self._architecture])
        sf = self._sched_field[self._architecture]
        if sf is None:
            sched_i = None
        else:
            sched_i = self._keys.index(sf)
        prio_i = self._keys.index(self._prio_field[self._architecture])
        sched = None
        prio = None
        for path in paths:
            pid = m[path][i]
            tid = m[path][tid_i]
            pids.append((pid, tid))
            if sched_i is not None:
                sched = m[path][sched_i]
            prio = m[path][prio_i]
        if sched_i is not None and sched and not sched.startswith("SCHED_"):
            sched = "SCHED_" + sched
        if self.policy_model is not None:
            m = self.policy_model
            iter = m.get_iter_first()
            while m.iter_is_valid(iter):
                mp = m[iter][0]
                if mp == sched:
                    self.policy_cb.set_active_iter(iter)
                    break
                iter = m.iter_next(iter)
        else:
            self.policy_cb.set_sensitive(False)
        self.prio_adjustment.set_value(float(prio))
        self.set_prio_label.set_markup('set scheduling policy / priority of pid <b>%s</b>' % (
            ",".join(["%s/%s" % p for p in pids])))
        self.set_prio_dialog.set_transient_for(self.processlist_window)
        self.set_prio_pids = pids
        self.set_prio_dialog.show()

    def on_pltv_button_press_event(self, tv, ev):
        if ev.button == 3:
            self.processlist_popup.popup(None, None, None, None, ev.button, ev.time)
            return True
        return False

    def on_filter_entry_activate(self, entry):
        self._set_current()

    def on_filter_entry_editing_done(self, entry):
        self._set_current()
            
    def select_pid(self, pid):
        if pid is None:
            return
        if isinstance(pid, (str, unicode)):
            pid = eval(pid)
        self.selected_task = pid
        self.selected_task_path = None
        self.selected_task_is_tid = False
        if not hasattr(self, "model"):
            return False
        m = self.model
        has_rows = False
        iter = m.get_iter_first()
        i = self._keys.index(self._signal_field[self._architecture])
        while iter and m.iter_is_valid(iter):
            has_rows = True
            if eval(m[iter][i]) == pid:
                path = m.get_path(iter)
                self.pltv.set_cursor(path, None)
                return True
            iter = m.iter_next(iter)
        if has_rows:
            # we have rows but cant find pid -> remove selection
            s = self.pltv.get_selection()
            s.unselect_all()
        return False

    def select_tid(self, pid, tid=None):
        if pid is None:
            return
        if tid is not None:
            pid = (pid, tid)
        if type(pid) == tuple and self._architecture is not None and "linux" in self._architecture:
            pid = pid[1]
        self.selected_task = pid
        self.selected_task_path = None
        self.selected_task_is_tid = True
        if not hasattr(self, "model"):
            return False
        m = self.model
        has_rows = False
        iter = m.get_iter_first()
        i = self._keys.index(self._tid_field[self._architecture])
        if type(pid) == tuple:
            si = self._keys.index(self._signal_field[self._architecture])
        else:
            si = None
        while iter and m.iter_is_valid(iter):
            has_rows = True
            if si is None:
                si_ok = int(m[iter][i]) == int(pid)
            else:
                si_ok = int(m[iter][si]) == int(pid[0]) and int(m[iter][i]) == int(pid[1])
            if si_ok:
                path = m.get_path(iter)
                self.pltv.set_cursor(path, None)
                return True
            iter = m.iter_next(iter)
        if has_rows:
            # we have rows but cant find pid -> remove selection
            s = self.pltv.get_selection()
            s.unselect_all()
        return False

    class set_affinity_task(object):
        def finish(self):
            self.pl.set_affinity_dialog.hide()
            if not self.res:
                return
            result = "\n".join(self.res)
            dlg = Gtk.MessageDialog(
                self.pl.processlist_window, 
                Gtk.DialogFlags.DESTROY_WITH_PARENT, 
                Gtk.MessageType.ERROR,
                Gtk.ButtonsType.OK,
                message_format="error(s) setting affinity 0x%02x for these pid's:\n%s" % (
                    self.mask, result)
            )
            dlg.run()
            dlg.hide()
            self.pl.set_affinity_dialog.show()
            
        def on_response(self, exception=None):
            if exception is not None:
                self.res.append("pid %s tid %s: %s" % (self.pid, self.tid, exception))
            self.next_pid()
                
        def next_pid(self):
            if not self.pids:
                return self.finish()
            self.pid, self.tid = self.pids.pop(0)
            self.pl.parent.manager_proxy(
                "mgr_cb",
                "set_cpu_affinity", self.on_response,
                host=self.pl.host, pid=self.pid, tid=self.tid, affinity=self.mask)
    
    def on_set_affinity_dialog_response(self, dialog, resp):
        if resp == 0:
            self.set_affinity_dialog.hide()
            return
        task = self.set_affinity_task()
        task.pl = self
        task.pids = list(self.set_affinity_pids)
        task.mask = get_cpu_affinity_mask(self.cpu_id_entry.get_text().strip())
        task.res = []
        task.next_pid()

    def on_set_affinity(self, item):
        pids = []
        s = self.pltv.get_selection()
        m, paths = s.get_selected_rows()
        i = self._keys.index(self._signal_field[self._architecture])
        tid_i = self._keys.index(self._tid_field[self._architecture])
        for path in paths:
            pid = m[path][i]
            tid = m[path][tid_i]
            pids.append((pid, tid))
        self.set_affinity_pids = pids
        self.set_affinity_label.set_text("set cpu affinity for pid('s) %s" % ", ".join(["%s/%s" % p for p in pids]))
        self.set_affinity_dialog.set_transient_for(self.processlist_window)
        self.cpu_id_entry.grab_focus()
        self.set_affinity_dialog.show()

    @staticmethod
    def get_arch(architecture):
        arch = process_list._arch_aliases.get(architecture, None)
        if arch:
            return arch
        if "linux" in architecture or "debian" in architecture or "ubuntu" in architecture or architecture.startswith("osl"):
            return "linux-intel-gcc4" # its a linux...
        if architecture not in process_list._formatters:
            # unknwon arch, assume linux/intel?
            return "linux-intel-gcc4"
        return architecture
    @staticmethod
    def get_sched_prio_cpu_from_list(plist, architecture):
        arch = process_list.get_arch(architecture)
        ti = dict(list(zip(plist[0], plist[1])))
        sf = process_list._sched_field[arch]
        if sf is not None:
            try:
                sched_field = process_list._sched_field[arch]
                sched_value = ti[sched_field]
                formatter_name = process_list._formatters[arch][sched_field]
                formatter = getattr(process_list, formatter_name)
                sched = formatter(sched_value)
                if sched.startswith("SCHED_"):
                    sched = sched[6:]
            except Exception:
                print("could not format sched for field", sched_field)
                pprint.pprint(plist)
                pprint.pprint(ti)
                print(sched_value)
                raise
        else:
            sched = None
            sched_value = None
        try:
            prio = ti[process_list._prio_field[arch]]
        except Exception:
            pprint.pprint(ti)
            print("plist:\n%s" % pprint.pformat(plist))
            raise
        cpu = ti[process_list._last_cpu_field[arch]]
        return sched, sched_value, prio, cpu
