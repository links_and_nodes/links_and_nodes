import re
import traceback
import ast
import re
import os
from collections import OrderedDict

import gi
gi.require_version('GLib', '2.0')
from gi.repository import GLib # noqa: E402

from . import Logging

__all__ = [ "Container" ]


def replace_escape_sequences(s):
    def replace_escape(match):
        return ast.literal_eval("'%s'" % match.group(0))
    one_chars = r"\\'" + '"abfnrtv'
    escape_pattern = r"\\(?:[%s]|x[0-9A-Fa-f]{2}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8}|N\{[^}]+\}|[0-7]{1,3})" % one_chars
    return re.sub(escape_pattern, replace_escape, s)

class Script:
    # pickling:
    to_pickle = "container,name,lines,current_line,mode"
    def __getstate__(self):
        state = {}
        for attr in self.to_pickle.split(","):
            attr = attr.strip()
            state[attr] = getattr(self, attr)
        return state
    def __setstate__(self, state):
        self.__dict__.update(state)

    @staticmethod
    def _parse_source(src, strip_name=True):
        lines = list(map(str.strip, src.strip().split("\n")))
        name = None
        if strip_name:
            for line in lines:
                if line.startswith("# "):
                    name = line[2:]
                    del lines[0]
                    break
                break
        return name, lines

    def __init__(self, container, name, lines=None, from_file=None):
        self.container = container
        self.name = name
        self.manager = self.container.process._sysconf.manager
        Logging.enable_logging_on_instance(self, "%s: io_script %s" % (self.container.process.name, self.name))
        if from_file is None:
            self.lines = lines
        else:
            with open(from_file, "r") as fp:
                _, self.lines = Script._parse_source(fp.read(), strip_name=False)
        self.current_line = 0
        self.mode = "stopped"
        self._wait_text = None
        self._scheduled_id = None
        self._output_pos_flushed = 0
        self._if_stack = []

    def _flush_output(self):
        # if not already done, register for process output
        self._output_pos_flushed = self.container._output_pos_latest
        self.container.want_output(self)

    def check_output(self):
        if not self.mode.startswith("wait"):
            return
        out_start, out = self.container.get_output(self._output_pos_flushed)
        if self.mode == "wait":
            if self._wait_text in out:
                # wait done!
                pos = out.index(self._wait_text)
                self._output_pos_flushed = out_start + pos + len(self._wait_text)
                self._switch(self._wait_before_mode)
                self._schedule_exec_next()
        elif self.mode == "wait_regex":
            m = re.search(self._wait_text, out)
            if m is not None:
                # wait done!
                self._output_pos_flushed = out_start + m.end(0)
                self._switch(self._wait_before_mode)
                self._schedule_exec_next()
        elif self.mode == "wait_if":
            # test all if branches for a match!
            for block_start, what, exp in self._wait_text[:-1]:
                if what == "regex":
                    m = re.search(exp, out)
                    if m is not None:
                        # wait done!
                        self._output_pos_flushed = out_start + m.end(0)
                        break
                else: # fixed
                    if exp in out:
                        # wait done!
                        pos = out.index(exp)
                        self._output_pos_flushed = out_start + pos + len(exp)
                        break
            else:
                return # no match!
            self._switch(self._wait_before_mode)
            self.current_line = block_start - 1
            self._schedule_exec_next()
            return

    def restart(self):
        self.debug("restarted")
        self.do_play(0)

    def _switch(self, new_mode):
        if self.mode == new_mode:
            return
        self.mode = new_mode
        self.debug("switched to %r" % self.mode)
        self._notify_gui()

    def _notify_gui(self):
        self.manager._notify_gui(
            request="io_script_update",
            pname=self.container.process.name,
            script_name=self.name,
            mode=self.mode,
            current_line=self.current_line
        )

    def is_executing(self, mode=None):
        if mode is None:
            mode = self.mode
        return mode != "stopped"

    def do_play(self, line=None):
        if line is not None:
            self.current_line = line
        self._if_stack = []
        self._switch("playing")
        self._flush_output()
        self.execute_current_line()

    def do_step(self, line=None, inc=False):
        if line is not None:
            self.current_line = line
        self._if_stack = []
        self._switch("stepping" + (" inc" if inc else ""))
        self._flush_output()
        self.execute_current_line()

    def do_stop(self):
        self._switch("stopped")
        self.container.no_longer_want_output(self)

    def do_wait(self, text):
        self._wait_text = text
        self._wait_before_mode = self.mode
        self._switch("wait")

    def do_wait_regex(self, text):
        self._wait_text = re.compile(text, re.M)
        self._wait_before_mode = self.mode
        self._switch("wait_regex")

    def do_if(self, if_block):
        self._wait_text = if_block
        self._wait_before_mode = self.mode
        self._if_stack.append(if_block)
        self._switch("wait_if")

    def execute_current_line(self):
        line = self.lines[self.current_line]
        if line[:1] != "#":
            try:
                self._execute(line)
            except:
                self.error("error executing line %r:\n%s" % (line, traceback.format_exc()))
                self.do_stop()
        else:
            self._schedule_exec_next()

    def _parse_line(self, line):
        sep = ": "
        if sep in line:
            cmd, data = line.split(": ", 1)
        else:
            cmd, data = line, None

        if data:
            if cmd.endswith("_eval"):
                cmd = cmd.rsplit("_", 1)[0]
                data_nl = data = replace_escape_sequences(data)
            else:
                data_nl = data + "\n"
        else:
            data_nl = data
        return cmd, data, data_nl

    def _read_if_structure(self, cmd, data, l):
        def add_search(cmd, data, start):
            if cmd.endswith("_regex"):
                return (start, "regex", re.compile(data, re.M))
            return (start, "fixed", data)
        if_block = [
            add_search(cmd, data, l + 1)
        ]
        l += 1
        while l < len(self.lines):
            cmd, data, data_nl = self._parse_line(self.lines[l])
            if cmd == "elif" or cmd == "elif_regex":
                if_block.append(add_search(cmd, data, l + 1))
            elif cmd == "endif":
                if_block.append(l + 1)
                break
            elif cmd == "if" or cmd == "if_regex": # nexted if!
                nested_if = self._read_if_structure(cmd, data, l)
                l = nested_if[-1] # just skip it as if it was a single instruction
                continue
            l += 1
        else:
            raise Exception("`if`-block starting at line %d is not terminated by `endif`!" % (if_block[0][0] - 1))
        return if_block

    def _find_end_of_if_structure(self, l):
        first_line = l
        # l is a "elif" command!
        l += 1
        while l < len(self.lines):
            cmd, data, data_nl = self._parse_line(self.lines[l])
            if cmd == "elif" or cmd == "elif_regex":
                pass
            elif cmd == "endif":
                return l + 1 # found end!
            elif cmd == "if" or cmd == "if_regex": # nexted if!
                nested_if = self._read_if_structure(cmd, data, l)
                l = nested_if[-1] # just skip it as if it was a single instruction
                continue
            l += 1
        raise Exception("`if`-block including line %d is not terminated by `endif`!" % first_line)

    def _execute(self, line):
        self.debug("execute: %r" % line)
        cmd, data, data_nl = self._parse_line(self.lines[self.current_line])
        p = self.container.process

        if cmd == "pause":
            return self.do_stop()
        elif cmd == "send":
            self._flush_output()
            self.manager.send_stdin(p.name, data_nl)
        elif cmd == "insert":
            data_nl = data_nl.replace("\n", "\n\r") # this might be unwanted (unlikely)
            self.manager.inject_output(p, data_nl)
        elif cmd == "wait":
            return self.do_wait(data)
        elif cmd == "wait_regex":
            return self.do_wait_regex(data)
        elif cmd == "if" or cmd == "if_regex":
            # read complete if-elif-endif structure, remember patterns and line numbers!
            if_block = self._read_if_structure(cmd, data, self.current_line)
            return self.do_if(if_block)
        elif cmd.startswith("elif") or cmd == "endif":
            if not self._if_stack:
                if cmd.startswith("elif"):
                    self.current_line = self._find_end_of_if_structure(self.current_line) - 1
            else:
                # jump to end of this if-block
                if_block = self._if_stack.pop(-1)
                self.current_line = if_block[-1] - 1
        else:
            raise Exception("unknown script command: %r" % cmd)

        # schedule execution of next line
        self._schedule_exec_next()


    def _schedule_exec_next(self):
        if self.mode == "stepping":
            self.do_stop()
            return
        if self.current_line + 1 == len(self.lines):
            self.do_stop()
            return
        self.current_line += 1
        if self.mode == "stepping inc":
            self.do_stop()
            return
        self._schedule_exec_current()

    def _schedule_exec_current(self):
        if self._scheduled_id is None:
            self._scheduled_id = GLib.idle_add(self._scheduled_exec_current)

    def _scheduled_exec_current(self):
        self._scheduled_id = None
        self._notify_gui()
        self.execute_current_line()
        return False

class Container:
    def __init__(self, process):
        self.process = process
        self.scripts = OrderedDict()
        self.was_just_started = False
        self._output_pos_latest = 0
        self._outputs = []
        self._need_output = []
        self._max_output_len = 1024

    def add_script(self, src, name_override=None):
        name, lines = Script._parse_source(src)
        if name_override:
            name = name_override
        elif name is None:
            name = "script %d" % (len(self.scripts) + 1, )
        if name in self.scripts:
            raise Exception("there is already a script named %r!" % name)
        self.scripts[name] = Script(self, name, lines=lines)

    def add_script_file(self, fn, name_override=None):
        if name_override:
            name = name_override
        else:
            parts = fn.split(os.sep)
            for i in range(1, len(parts)):
                name = os.sep.join(parts[-i:])
                if name not in self.scripts:
                    break
            else:
                name = fn
        if name in self.scripts:
            raise Exception("there is already a script named %r!" % name)
        self.scripts[name] = Script(self, name, from_file=fn)

    def register_start(self):
        self.was_just_started = True

    def check_autostart(self):
        if not self.was_just_started:
            return
        self.was_just_started = False
        script = self.scripts.get("autostart")
        if not script:
            return
        script.restart()

    def want_output(self, script):
        if script not in self._need_output:
            self._need_output.append(script)

    def no_longer_want_output(self, script):
        if script in self._need_output:
            self._need_output.remove(script)
        if not self._need_output:
            self._outputs = []

    def check_output(self, out):
        """
        out is expected to be of type bytes
        """
        if not self._need_output:
            return
        # todo: out might be incomlpete encoded char-sequence
        try:
            out = out.decode(self.process.output_encoding)
        except:
            self.warning("not all %s: %r" % (self.process.output_encoding, out))
            out = out.decode(self.process.output_encoding, "replace")

        self._outputs.append((self._output_pos_latest, out))
        self._output_pos_latest += len(out)
        for script in self._need_output:
            script.check_output()

        # do we need to trim/drop output?
        if not len(self._outputs):
            return

        output_len = self._outputs[-1][0] + len(self._outputs[-1][1]) - self._outputs[0][0]
        if output_len < self._max_output_len:
            return # nope
        # yes, can we drop first line?
        while True:
            candidate_len = len(self._outputs[0][1])
            if output_len - candidate_len < self._max_output_len:
                break # nope, keep overhang!
            # yes, drop this line!
            output_len -= candidate_len
            del self._outputs[0]
        output_len = self._outputs[-1][0] + len(self._outputs[-1][1]) - self._outputs[0][0]

    def get_output(self, req_start):
        if not self._outputs:
            return self._output_pos_latest, ""

        if req_start < self._outputs[0][0]:
            req_start = self._outputs[0][0]

        for i, (start, line) in enumerate(self._outputs):
            end = start + len(line)
            if end <= req_start:
                continue
            skip = req_start - start
            ret = [ line[skip:] ]
            ret.extend([out for _, out in self._outputs[i + 1:] ])
            break
        else:
            return self._output_pos_latest, ""
        return req_start, "".join(ret)

    def __bool__(self):
        return len(self) > 0

    def __len__(self):
        return len(self.scripts)

    def stop(self):
        """
        stop any running io_script
        """
        for script in self._need_output:
            script.do_stop()
