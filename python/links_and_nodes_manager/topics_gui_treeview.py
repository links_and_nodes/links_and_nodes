import os

import gi
gi.require_version('Gtk', '3.0')
#gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
from gi.repository import Gtk, Gdk, Pango, GObject # noqa: E402
#from gi.repository import GObject, Gtk, GLib,  # noqa: E402

class TopicsGuiTreeView:
    def __init__(self, topics_gui):
        self.topics_gui = gui = topics_gui

        self.display_tree = os.getenv("LNM_TOPICS_DISPLAY_FLAT", "n")[:1].lower() not in ("1", "y", "t")
        self.cached_iters = dict() # topic-name -> iter
        self.expanded_topic_name_parts = set() # topic-name-part

        tv = self.tv = gui.topics_tv
        tv.set_enable_tree_lines(True)
        tv.set_headers_visible(True)

        self.model = Gtk.TreeStore(
            GObject.TYPE_PYOBJECT,
            GObject.TYPE_STRING, # topic-name(-part)
            GObject.TYPE_STRING, # publisher
            GObject.TYPE_STRING, # N-subscribers
            GObject.TYPE_STRING, # publisher rate
            GObject.TYPE_STRING, # published packets
            )

        crtext = Gtk.CellRendererText()
        crtext.set_property("ellipsize", Pango.EllipsizeMode.END)
        tv.insert_column_with_attributes(-1, "topic", crtext, text=1)
        col = tv.get_columns()[-1]
        col.set_resizable(True)
        col.set_property("expand", True)
        self.topics_col = col

        tv.insert_column_with_attributes(-1, "publisher", Gtk.CellRendererText(), text=2)
        tv.insert_column_with_attributes(-1, "subscribers", Gtk.CellRendererText(), text=3)
        tv.insert_column_with_attributes(-1, "publisher rate", Gtk.CellRendererText(), text=4)
        tv.insert_column_with_attributes(-1, "published pkts", Gtk.CellRendererText(), text=5)
        tv.set_model(self.model)
        tv.connect("row-expanded", self.on_row_expanded)
        tv.connect("row-collapsed", self.on_row_collapsed)

        gui.on_topics_tv_cursor_changed = self.on_cursor_changed
        gui.on_topics_tv_row_activated = self.on_row_activated

        gui.topics_tv_display_cb.connect("changed", self.on_display_cb_changed)
        gui.tree_expand_all.connect("clicked", self.on_expand_all_clicked)
        gui.tree_collapse_all.connect("clicked", self.on_collapse_all_clicked)

        gui.topics_tv_display_cb.set_active_id("tree" if self.display_tree else "flat")
        self._redisplay()

    def _redisplay(self):
        gui = self.topics_gui
        gui.tree_expand_all.set_visible(self.display_tree)
        gui.tree_collapse_all.set_visible(self.display_tree)

        path, _ = self.tv.get_cursor() # path-none-checked
        selected_topic_name = None
        if path:
            selected_topic = self.model[path][0]
            if selected_topic:
                selected_topic_name = selected_topic.name
        topics = [topic for topic, iter in self._iter_topics()]
        self.model.clear()
        self.cached_iters = dict()
        self.set(topics)
        if selected_topic_name:
            iter = self._find_topic_iter(selected_topic_name)
            path = self.model.get_path(iter)
            self.tv.expand_to_path(path)
            self.tv.set_cursor(path)
        return True

    def on_display_cb_changed(self, cb):
        self.display_tree = cb.get_active_id() == "tree"
        self._redisplay()
        return True

    def on_expand_all_clicked(self, btn):
        self.tv.expand_all()
        return True

    def on_collapse_all_clicked(self, btn):
        self.tv.collapse_all()
        self.expanded_topic_name_parts = set()
        return True

    def on_row_expanded(self, tv, iter, path):
        topic = self.model[iter][0]
        if topic:
            self.cached_iters[topic.name] = iter
            tnp = topic.name
        else:
            tnp = self._get_iter_name(iter)
        self.expanded_topic_name_parts.add(tnp)
        # does it only have one child?
        n = self.model.iter_n_children(iter)
        if n == 1:
            tv.expand_row(self.model.get_path(self.model.iter_children(iter)), False)

    def on_row_collapsed(self, tv, iter, path):
        topic = self.model[iter][0]
        if topic:
            self.cached_iters[topic.name] = iter
            tnp = topic.name
        else:
            tnp = self._get_iter_name(iter)
        self.expanded_topic_name_parts.discard(tnp)

    def on_row_activated(self, tv, path, col):
        m = self.model
        topic = m[path][0]
        if topic is None:
            # expand this row
            event = Gtk.get_current_event()
            _, state = event.get_state()
            open_all = state & Gdk.ModifierType.SHIFT_MASK

            if not self.tv.row_expanded(path) or open_all:
                self.tv.expand_to_path(path)
                if open_all:
                    self.tv.expand_row(path, True)
            else:
                self.tv.collapse_row(path)
            return
        if topic.publisher:
            self.topics_gui.open_inspector_for_port(topic.source_port, topic, subscriber=None)
        return True

    def on_cursor_changed(self, tv):
        path, col = tv.get_cursor() # path-none-checked
        if not path:
            return True
        topic = self.model[path][0]
        self.topics_gui.show_topic(topic)
        return True

    def _get_iter_name(self, iter, before=None):
        if before is None:
            before = []
        before.insert(0, self.model[iter][1])
        parent = self.model.iter_parent(iter)
        if not parent:
            return ".".join(before)
        return self._get_iter_name(parent, before)

    def _iter_topics(self, parent=None, only_visible=False):
        m = self.model
        iter = m.iter_children(parent)
        while iter and m.iter_is_valid(iter):
            topic = m[iter][0]
            if only_visible:
                path = m.get_path(iter)
                is_expanded_or_final = self.tv.row_expanded(path) or topic is not None
            if not only_visible or is_expanded_or_final:
                if topic is not None:
                    yield topic, iter
                for what in self._iter_topics(iter, only_visible=only_visible):
                    yield what
            iter = m.iter_next(iter)

    def _rescan_cached_iters(self):
        self.cached_iters = dict() # invalidate cache
        for topic, iter in self._iter_topics():
            self.cached_iters[topic.name] = iter

    def _get_topic_row(self, topic):
        if topic.source_port and topic.source_port.measured_rate is not None:
            src_rate = "%.1f" % topic.source_port.measured_rate
            src_count = str(topic.source_port.packet_count)
        else:
            src_rate = ""
            src_count = ""
        if topic.publisher:
            pub = topic.publisher.program_name
        else:
            pub = "<no publisher>"
        if self.display_tree:
            name_part = topic.name.rsplit(".", 1)[-1]
        else:
            name_part = topic.name
        return topic, name_part, pub, str(len(topic.subscribers)), src_rate, src_count

    def _find_topic_iter(self, topic_name):
        iter = self.cached_iters.get(topic_name)
        if iter is not None:
            return iter

        m = self.model

        if self.display_tree:
            name_path = topic_name.split(".")
        else:
            name_path = [topic_name]

        def search_child(parent, name):
            iter = m.iter_children(parent)
            while iter and m.iter_is_valid(iter):
                row = m[iter]
                if row[1] == name:
                    return iter
                if row[1] > name:
                    return  None
                iter = m.iter_next(iter)

        parent = None
        is_new = False
        for np in name_path:
            new_iter = search_child(parent, np)
            parent = new_iter

        return new_iter

    def set(self, topics):
        self._rescan_cached_iters()
        seen = set()
        for topic in topics:
            self.add(topic)
            seen.add(topic.name)
        to_del = set(self.cached_iters.keys()).difference(seen)
        if to_del:
            to_del.sort()
            for topic_name in td[::-1]:
                iter = self.cached_iters[topic_name]
                m.remove(iter)
            self.cached_iters = dict()

    def add(self, topic):
        new_cols = self._get_topic_row(topic)

        iter = self.cached_iters.get(topic.name)
        if iter is not None:
            self.model[iter] = new_cols
            return

        # add new/update
        m = self.model
        if self.display_tree:
            name_path = topic.name.split(".")
        else:
            name_path = [topic.name]

        def search_add_child(parent, name):
            iter = m.iter_children(parent)
            while iter and m.iter_is_valid(iter):
                row = m[iter]
                if row[1] == name:
                    return iter, False
                if row[1] > name: # insert before
                    return m.insert_before(parent, iter), True
                iter = m.iter_next(iter)
            return m.insert_before(parent, None), True
        
        parent = None
        is_new = False
        for np in name_path:
            new_iter, this_is_new = search_add_child(parent, np)
            if this_is_new:
                m[new_iter][1] = np
            is_new = is_new or this_is_new
            parent = new_iter

        if is_new:
            self.cached_iters = dict() # invalidate cache
        self.cached_iters[topic.name] = new_iter
        m[new_iter] = new_cols
        if is_new: # check to expand
            for k in range(len(name_path) - 1):
                new_iter = m.iter_parent(new_iter)
                tnp = ".".join(name_path[:-k - 1])
                if tnp in self.expanded_topic_name_parts:
                    path = m.get_path(new_iter)
                    self.tv.expand_row(path, False)

    def remove(self, topic):
        iter = self._find_topic_iter(topic.name)
        if not iter:
            return False
        m = self.model
        while True:
            parent = m.iter_parent(iter)
            m.remove(iter)
            if not parent or m.iter_n_children(parent) > 0:
                break
            iter = parent
        self.cached_iters = dict() # invalidate cache
        return True

    def select(self, topic):
        iter = self._find_topic_iter(topic.name)
        if not iter:
            return False
        path = self.model.get_path(iter)
        self.tv.expand_to_path(path)
        self.tv.set_cursor(path)
        return True

    def get_visible(self):
        for topic, iter in self._iter_topics(only_visible=True):
            yield topic
