"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

import os
import sys
import time
import weakref
import threading
import traceback

from . import CommunicationUtils
from .Logging import enable_logging_on_instance, warning, info

def with_lock(fcn):
    def locked_wrapper(self, *args, **kwargs):
        self.lock.acquire()
        try:
            return fcn(self, *args, **kwargs)
        finally:
            self.lock.release()
    return locked_wrapper

do_check_for_long_waiters = os.getenv("LN_MANAGER_CONDITIONS_CHECK_FOR_LONG_WAITERS", "y").lower()[:1] in "yt1"

class Condition(object):
    _all_lock = threading.RLock()
    _all = {}
    _long_wait_time = 5
    _check_thread = None
    _last_shash = 0
    @staticmethod
    def _register(cond):
        Condition._all_lock.acquire()
        ref = weakref.ref(cond, Condition._unregister)
        Condition._all[id(ref)] = ref
        if len(Condition._all) == 1 and (Condition._check_thread is None or not Condition._check_thread.is_alive()) and do_check_for_long_waiters:
            if Condition._check_thread is not None:
                Condition._check_thread.join() # be nice                
            Condition._check_thread = threading.Thread(target=Condition._check_long_waiters)
            Condition._check_thread.daemon = True
            Condition._check_thread.start()
        Condition._all_lock.release()
    @staticmethod
    def _unregister(ref):
        Condition._all_lock.acquire()
        del Condition._all[id(ref)]
        Condition._all_lock.release()
    @staticmethod
    def _check_long_waiters():
        while True:
            time.sleep(Condition._long_wait_time / 2.0)
            had_some = Condition._check_long_waiters_once()
            if not had_some:
                break
    @staticmethod
    def _check_long_waiters_once():
        had_some = False
        msgs = []
        shash = 0
        Condition._all_lock.acquire()
        for cond_ref in list(Condition._all.values()):
            cond = cond_ref()
            if cond:
                msg_hash = cond._check_for_long_waiters()
                if msg_hash:
                    msgs.append(msg_hash[0])
                    shash += msg_hash[1]
                had_some = True
        Condition._all_lock.release()
        if msgs:
            if Condition._last_shash != shash:
                warning("Conditions", "\n".join(msgs))
                Condition._last_shash = shash
        elif Condition._last_shash != 0:
            Condition._last_shash = 0
            info("Conditions", "all unblocked")
        return had_some
    
    def __init__(self, name=None, request_cb=None, debug_enabled=False, holder=None):
        if name is None:
            name = "%#x" % id(self)
        enable_logging_on_instance(self, "Condition %r" % name)
        self.name = name
        self.debug_enabled = debug_enabled
        self.lock = threading.RLock()

        # a condition can either be is_reached OR is_failed!
        self.is_reached = False
        #self.is_failed = False
        # as soon as one of them is set, the Condition is "final"

        self.fail_message = None
        self.waiters = []
        
        self.is_requested = False
        self.request_cb = request_cb

        self.holder = holder
        Condition._register(self)
        
    @with_lock
    def _check_for_long_waiters(self):
        now = time.time()
        long_waiters = []
        shash = 0
        for cb, wait_start in self.waiters:
            wait_time = now - wait_start
            if wait_time >= Condition._long_wait_time:
                long_waiters.append((cb, wait_time))
                shash += hash(cb)
                shash += hash(wait_start)
        if not long_waiters:
            return
        msgs = [ "%r has %d waiters waiting for a long time:" % (self.name, len(long_waiters)) ]
        for i, (cb, wait_time) in enumerate(long_waiters):
            if isinstance(cb, CommunicationUtils.callback):
                cstack = "".join(traceback.format_stack(f=cb.creation_stack)[:-1])
                info = "\n     ".join(("\ncallback creation stack:\n%s" % cstack).split("\n"))
            else:
                info = ""
            msgs.append("  %d: since %3ds: %s%s" % (i + 1, wait_time, cb, info))
        
        return "\n".join(msgs), shash
        
    @with_lock
    def reset(self):
        """
        use this method with caution! existing / blocked waiters will not be notified!
        """
        self.is_requested = False
        self.is_reached = False
        #self.is_failed = False
        self.waiters = [] # (cb, wait-start)
        
    @with_lock
    def request(self, **kwargs):
        if self.is_requested:
            if self.debug_enabled: self.debug("request: was already requested!")
            return
        self.is_requested = True
        if self.debug_enabled: self.debug("request: is now requested!")
        if self.request_cb:
            if isinstance(self.request_cb, Condition):
                self.request_cb.request(**kwargs)
            else:
                self.request_cb(**kwargs)
        
    @with_lock
    def call(self, cb=None, **kwargs):
        """
        we want condition!
        call callable cb as soon as condition is met (or failed), or now if its already met(or failed)!
        check whether we can/need todo something to cause condition!
        """
        if not self.is_reached: #  and not self.is_failed:
            if cb: self.waiters.append((cb, time.time()))
            if self.debug_enabled: self.debug("call: is not yet reached!")
            skip_request = len(kwargs) == 1 and kwargs.get("skip_request")
            if not skip_request:
                self.request(**kwargs)
            return
        
        if self.debug_enabled: self.debug("call: was already %s!" % ("reached" if self.is_reached else "failed"))
        try:
            cb()
        except Exception:
            if hasattr(cb, "_on_exception"):
                cb._on_exception(sys.exc_info()[1])
            else:
                raise
        
    @with_lock
    def trigger(self, on_exception=None):
        """
        condition is now met! call all registered callbacks!
        """
        #self.is_failed = False
        if self.is_reached: # was already reached!
            if self.debug_enabled: self.debug("trigger: was already reached!")
            return
        self.is_reached = True
        if self.debug_enabled: self.debug("trigger: is now reached!")
        to_call = self.waiters
        self.waiters = []
        while to_call:
            cb, wait_start = to_call.pop(0)
            try:
                cb()
            except Exception:
                if hasattr(cb, "_on_exception"):
                    cb._on_exception(sys.exc_info()[1])
                else:
                    self.error("error triggering condition %s:\n%s" % (self.name, traceback.format_exc()))

    @with_lock
    def trigger_fail(self, fail_message=None, on_exception=None):
        """
        condition failed! call all registered callbacks!
        """
        self.is_reached = False
        #if self.is_failed: # was already reached!
        #    if self.debug_enabled: self.debug("trigger: was already failed!")
        #    return
        self.fail_message = fail_message
        #self.is_failed = True
        self.debug("trigger: is now failed! (%s)" % fail_message)
        while self.waiters:
            cb, wait_start = self.waiters.pop(0)
            try:
                cb()
            except Exception:
                if hasattr(cb, "_on_exception"):
                    cb._on_exception(sys.exc_info()[1])
                else:
                    traceback.print_exc()
        self.is_requested = False

    @with_lock
    def get_reached_or_fail(self):
        #if self.is_failed:
        #    return False
        if self.is_reached:
            return True
        return None
    
    @with_lock
    def is_final(self):
        #if self.is_failed or self.is_reached:
        if self.is_reached:
            return True
        return False
