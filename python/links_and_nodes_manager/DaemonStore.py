"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

from __future__ import print_function

import re
import sys
import traceback
import socket
import platform
import subprocess
import time
import os
import inspect
import pprint
from . import config
from . import tools
import pyutils
import pyutils.vx

import pyutils.line_assembler

try:
    import fcntl
    have_fcntl = True
except Exception:
    have_fcntl = False

try:
    import pyparsing
except ImportError:
    # use private pyparsing copy
    import links_and_nodes_manager.private_pyparsing.pyparsing as pyparsing

from .CommunicationUtils import arbiter_connection, callback, check_for_display_unix_socket, get_max_shm_version_from_lib_version, get_xcookie, get_xauthority, ignore_cb, needs_finish_callback, tracked_object, CommunicationModule

from .PortStore import  shm_port, udp_port, tcp_port
from .Logging import enable_logging_on_instance
from .Condition import Condition

from .py2compat import unicode, is_py2, inspect_getfullargspec

import links_and_nodes
import links_and_nodes_base

import gi
gi.require_version('GLib', '2.0')
from gi.repository import GLib # noqa: E402

PROTOCOL_VERSION = 0 # there we had no proto version
PROTOCOL_VERSION = 1 # added props dict to processes
PROTOCOL_VERSION = 2 # added retrieve_process_list
PROTOCOL_VERSION = 3 # prio and policy
PROTOCOL_VERSION = 4 # max_output_frequency
PROTOCOL_VERSION = 5 # short port list
PROTOCOL_VERSION = 6 # tcp forwards
PROTOCOL_VERSION = 7 # udp with counters
PROTOCOL_VERSION = 8 # svn info
PROTOCOL_VERSION = 9 # request_id's
PROTOCOL_VERSION = 10 # n-elements in shm
PROTOCOL_VERSION = 11 # tcp_forward listen_port -2 to search free xforwarding port 
PROTOCOL_VERSION = 12 # change_user_to
PROTOCOL_VERSION = 13 # stack_size
PROTOCOL_VERSION = 14 # resource_limit
PROTOCOL_VERSION = 15 # tcp ports
PROTOCOL_VERSION = 16 # alternate_sockets
PROTOCOL_VERSION = 17 # set_state_property
PROTOCOL_VERSION = 18 # same as 17 but listen_port not in (-1, -2) works
PROTOCOL_VERSION = 19 # log divisor
PROTOCOL_VERSION = 20 # able to swap endianess
PROTOCOL_VERSION = 21 # manager can lock previously unlocked daemon
PROTOCOL_VERSION = 22 # delete tcp forwards
PROTOCOL_VERSION = 23 # fixed endianess typo
PROTOCOL_VERSION = 24 # set debug_enabled, shm ver 2 support
PROTOCOL_VERSION = 25 # support blocking output, change_user_to with group_name
PROTOCOL_VERSION = 26 # tcp forward to unix socket
PROTOCOL_VERSION = 27 # use_execvpe
PROTOCOL_VERSION = 28 # retrieve_log_token
PROTOCOL_VERSION = 29 # tcp-forward send-source magic
PROTOCOL_VERSION = 30 # removed state auto_check_interval, min_check_interval, need_check_after_up
PROTOCOL_VERSION = 31 # sends process output via bytes-string-literals
PROTOCOL_VERSION = 32 # test for firewall
PROTOCOL_VERSION = 33 # term_rows, term_cols with start_process
PROTOCOL_VERSION = 34 # disconnect_because_of_new_manager & replaced_manager

debug_repr_output_log = os.getenv("LN_DEBUG_REPR_OUTPUT_LOG", None)

class runtime_function_parser(object):
    def __init__(self, functions):
        self.functions = functions
        pp = pyparsing
        pp.ParserElement.enablePackrat()
        pp.ParserElement.setDefaultWhitespaceChars(" ")
        dblargs = pp.Forward()
        dblargs << pp.dblQuotedString + pp.Optional(pp.Literal(",").suppress() + pp.Optional(pp.White()).suppress() + dblargs)
        args = pp.Forward()
        arg = pp.CharsNotIn("),")
        args << arg + pp.Optional(pp.Literal(",").suppress() + pp.Optional(pp.White()).suppress() + args)
        fcn_name = pp.Or(list(map(pp.Literal, list(functions.keys()))))
        fcn = (
            pp.Literal("$(").suppress()
            + fcn_name
            + pp.Optional(
                pp.White().suppress()
                + pp.Group(pp.Or([dblargs, args])))
            + pp.Literal(")").suppress())
        self.parser = fcn

    def scan(self, input):
        for ret, start, end in self.parser.scanString(input):
            fcn = ret[0]
            arglist = []
            if len(ret) > 1:
                args = ret[1]
                for token in args:
                    if token[0] == '"':
                        arglist.append(token[1:-1])
                    else:
                        arglist.append(token)
            yield (fcn, arglist, start, end)

    def replace(self, inp, error):
        outp = []
        offset = 0
        for fcn, args, start, end in self.scan(inp):
            if start:
                outp.append(inp[offset:start])
            try:
                ret = self.functions[fcn](*args)
            except Exception:
                error("error calling runtime function %r with args %r:\n%s" % (fcn, args, traceback.format_exc()))
                ret = "<error calling %s(%s)>" % (fcn, repr(args)[1:-1])
            outp.append(ret)
            offset = end
        if offset != 0:
            outp.append(inp[offset:])
        if not outp:
            return inp
        return "".join(outp)

def check_error_only(cb):
    """
    check for- & pass exception
    or call cb without any arguments
    """
    def _check_error_only(ret=None, exception=None):
        if exception is not None: return cb(exception=exception)
        return cb()
    return _check_error_only

def create_connection(org_fcn, condition_name="when_register_finished"):
    """
    also uses @needs_finish_callback!
    will create connection is not already connected!

    org_fcn can also be a string, in which case it is the name of the condition to wait for!
    e.g. "when_connected" or "when_register_finished" (default)
    """
    if isinstance(org_fcn, str):
        def create_connection_for_cond(fcn):
            return create_connection(fcn, org_fcn)
        return create_connection_for_cond
    
    fcn = needs_finish_callback(org_fcn)
    args = inspect_getfullargspec(org_fcn)
    try:
        cb_index = args.args.index("cb")
    except Exception:
        raise Exception("function %s() uses @create_connection but has no 'cb' argument: %s" % (org_fcn.__name__, args.args))
    
    def with_connection_wrapper(self, *args, **kwargs):
        cb = args[cb_index - 1]
        cond = getattr(self, condition_name)
        
        if cond.get_reached_or_fail() is True:
            fcn(self, *args, **kwargs)
        else:
            self.debug("%s(%s, %s) needs connection, but is not yet done" % (org_fcn.__name__, args, kwargs))
            def _with_daemon(daemon):
                cond = getattr(self, condition_name)
                if cond.get_reached_or_fail() is not True:
                    msg = "%s() needs daemon %s, but connect failed: %s" % (org_fcn.__name__, condition_name, cond.fail_message)
                    self.error(msg)
                    return cb(exception=msg)
                fcn(daemon, *args, **kwargs)                
            self.manager.with_daemon_for_host(
                self.host, condition_name,
                _with_daemon
            )
    return with_connection_wrapper

def report_exception_in_cb(org_fcn):
    """
    expects first positional argument args[1] to be a callback that will be called like cb(exception=...) on exception!
    (args[0] is assumed to be 'self')
    """
    def _exception_reporter(*args, **kwargs):
        try:
            org_fcn(*args, **kwargs)
        except Exception:
            cb = args[1]
            args_str = []
            if len(args) > 1:
                args_str.append(repr(args[1:])[1:-1])
            if kwargs:
                args_str.append(repr(kwargs)[1:-1])
            args_str = ", ".join(args_str)
            cb(exception="error calling %s(%s):\n%s" % (org_fcn.__name__, args_str, traceback.format_exc()))
    return _exception_reporter

def needs_connection(fcn):
    """
    only check that we have a connection, otherwise throw!
    """
    def have_connection_wrapper(self, *args, **kwargs):
        if not self.is_connected():
            msg = "%s needs to be called with an established daemon connection!" % fcn.__name__
            self.error(msg)
            raise Exception(msg)
        return fcn(self, *args, **kwargs)
    return have_connection_wrapper

class DaemonStore(pyutils.hooked_object, tracked_object):
    update_via_unpickle = False
        # pickling:
    to_pickle = "endianess,svn_revision,library_version,protocol_version,architecture,is_modified,host,register_is_finished,registered,connected,up,had_connection_refused,tcp_forwards,processes,ports,pid,have_shm_version,have_release_all_resources,uid,gid,USER_env,HOME_env,USER,HOME,SHELL,user_real_name".split(",")
    def __getstate__(self):
        state = {}
        for attr in self.to_pickle:
            attr = attr.strip()
            state[attr] = getattr(self, attr, None)
        return state
    def __setstate__(self, state):
        self.manager = None
        self.__dict__.update(state)
        #print "daemonstore set state:"
        #pprint.pprint(state)
        self._init()
    def _init(self):
        pyutils.hooked_object.__init__(self)
        self.com = None
        self._hook_queue = []        
        if "name" not in self.host.__dict__:
            enable_logging_on_instance(self, "host %x" % id(self.host)) # .name :: TODO :: pickling! host object not jet unpickled!!!
            # delay init with real name until unpickling is finished!
            GLib.idle_add(self.__init)
        else:
            self.__init()
    def __init(self):
        enable_logging_on_instance(self, self.host.name)
        if self.update_via_unpickle:
            self.update_instance(self.__dict__)
        return False

    def get_instance_id(self):
        return self.host.name

    def __init__(self, manager, host_name):
        self.architecture = "<unknown_arch>"
        self.endianess = "unknown"
        self.manager_connect_is_running = False
        self.connect_is_running = False
        self.register_is_finished = False
        self.register_finished_time = 0
        self.manager = manager
        self.host = self.manager.sysconf.get_host(host_name)
        tracked_object.__init__(self)
        self._init()
        self.__init()

        self.registered = False
        self.registered_is_finished = False
        self.connected = False
        self.up = False # whether ln_daemon process is running, only really useful while it is starting up and not in background
        self.had_connection_refused = False
        self.com = None
        self.expect_disconnection = False
        self.was_locked = False
        self.list_processes_response_after_start = None

        self.xcookie = None # do we already know the cookie of our x-connection?
        self.xforwarding = None # is there a valid xauthority for x-forwarding? format: DISPLAY, XAUTHORITY, DISPLAY_HOME
        self._last_tcp_forwards = {}
        self._no_firewall_from = set() # of Host's

        self.next_remove_port_queue_worker = None

        # data for attached process (if there is one)
        self.subprocess = None
        self.pid = -1
        self.collected_stdouterr = [] # list of lines of bytes

        # request queing!
        self.response_listeners = {}
        self.request_counter = 0

        self._forwards_destroyer = None
        
        self._init_resource_registries()

        def does_need_connection(needing_condition, allow_start=True):
            def _on_response(exception=None):
                if exception is not None:
                    self.debug("get_daemon_connection failed: %s" % exception)
                    needing_condition.trigger_fail(fail_message=exception)
                    self.fail_conditions(fail_message=exception)
                    return
                return
            self.manager.get_daemon_connection(self, _on_response, allow_start=allow_start)

        cond_debug_enabled = True
        def add_condition(name, needs_connection=True):
            cond = Condition(self.host.name + " " + name, debug_enabled=cond_debug_enabled)
            if needs_connection:
                cond.request_cb = callback(does_need_connection, cond)
            setattr(self, name, cond)
        add_condition("when_registered") # self.registered = True, after daemon got "register_daemon" success response
        add_condition("when_connected") # after when_registered, self.up = True, after list_processes test, before "on_daemon_connect" hook
        add_condition("when_receiver_ports_known") # triggered by "on_daemon_connect" hook, after list_processes, before transport-check
        add_condition("when_sender_ports_known") # after when_non_sender_ports_known
        add_condition("when_register_finished") # in self.register_finished(), self.register_is_finished = True, after manager has restored all resources after "on_daemon_connect" hook

        add_condition("when_ssh_startup_done", needs_connection=False) # after ssh startup command terminated
        
        rt_functions = dict(
            own_ip_in_network=self.rtfcn_own_ip_in_network,
            own_ip_for=self.rtfcn_own_ip_for,
            get_ip_from_to=self.rtfcn_get_ip_from_to,
        )
        self.runtime_function_parser = runtime_function_parser(rt_functions)
        
    def _init_resource_registries(self):
        # initialize internal storage
        self.tcp_forwards = {}
        self.processes = []
        self.ports = []
        self.sources = {} # list of (daemon, port) = src_port of avaliable sources on this daemon!
        # change to:        dict of port -> list of (src_port of this daemon, rate of this port)

        self.shms = {} # dict of [topic-name, rate] of local availiable shm ports

        # callbacks for blocking methods
        self.state_change_callbacks = []
        self.remove_port_queue = []
        
        # for running processes
        self._started_processes = {}
        self._shell_commands = {} # pid -> { "output": [] }

        self._transportations_in_work = {}
        self.existing_ports = []
        
        self.ignore_port_removes = False
        self._forwards_to_destroy = {}

    def __str__(self):
        return "<daemon@%s/%d>" % (self.host.name, self.pid)
    def __repr__(self):
        return str(self)

    def short_id(self):
        return "%s" % self.host.name

    def get_collected_stdouterr(self, encoding="utf-8"):
        outerr = b"\n".join(self.collected_stdouterr)
        outerr = outerr.decode(encoding, "replace")
        # here outerr has mixed \r\n AND \n for newline
        outerr = outerr.replace("\r\n", "\n") # all newlines are \n
        return outerr

    @needs_finish_callback
    def start_daemon_process(self, cb, use_ln_daemon_from_home=False):
        """
        async method, provide cb to get called when finished

        calls cb() or cb(exception=...) on error
        """

        # first try to start via ssh
        arbiter_port = self.host.arbiter_port or config.DAEMON_PORT
        pub_key_file = self.host.daemon_public_key_file or self.manager.sysconf.instance_config.daemon_public_key_file

        if use_ln_daemon_from_home:
            self._is_ln_daemon_from_home_trial = True
            daemon_start_script = os.path.expanduser("~/.ln_daemon/start")
        else:
            self._is_ln_daemon_from_home_trial = False
            daemon_start_script = self.host.daemon_start_script
            if daemon_start_script and daemon_start_script.lower().strip() == "none":
                self.error("host's daemon_start_script is explicitly set to %r -- refusing to try to start daemon!" % daemon_start_script)
                raise Exception("daemon_start_script is None -- will not try to start daemon!")
        
            if daemon_start_script is None:
                daemon_start_script = self.manager.sysconf.instance_config.daemon_start_script
        
        self.guessed_GSSAPIServerIdentity = False
        
        if self.host == self.manager.host:
            self.debug("trying to start local daemon!")
            try:
                if platform.system() == "Windows":
                    daemon_binary = config.daemon_binary.replace("$(ARCH)", "windows-intel-mingw32")
                    cmd = r"%s.exe -d -l c:\temp\ln_daemon.log " % (
                        daemon_binary.replace("/", "\\"))
                else:
                    cmd = daemon_start_script
            except Exception:
                self.error("error trying to generate local-daemon-start command:\n%s\nfrom %r" % (
                    traceback.format_exc(), daemon_start_script))
                raise
            if pub_key_file is not None and pub_key_file.lower() != "none" and not links_and_nodes.NO_DAEMON_AUTH:
                cmd += " -public_key_file " + pub_key_file
            if self.manager.isolated_test_mode:
                cmd += " --isolated-test"
            else:
                cmd += " -ap %d" % arbiter_port
            return self._start_daemon_process_bh(cb, cmd)
        
        # remote host
        self.debug("trying to start daemon via ssh")
        try:
            cmd = daemon_start_script
        except Exception:
            self.error("error trying to generate local-daemon-start command:\n%s\nfrom %r" % (
                traceback.format_exc(),
                daemon_start_script))
            raise
        if pub_key_file is not None and pub_key_file.lower() != "none" and not links_and_nodes.NO_DAEMON_AUTH:
            cmd += " -public_key_file " + pub_key_file
        cmd += " -ap %d" % arbiter_port

        if self.host.ssh_binary:
            ssh_binary = self.host.ssh_binary
        else:
            ssh_binary = self.manager.sysconf.instance_config.ssh_binary

        ssh_args = self.host.get_ssh_args_string()
        
        # is that remote host in a common network with the manager?
        if self.manager.host.is_direct_reachable(self.host):
            # yes!
            sshcmd = "%s %s %s '%s'" % (
                ssh_binary,
                ssh_args,
                self.manager.host.try_to_get_fqdn_from(self.host),
                cmd)
            return self._start_daemon_process_bh(cb, sshcmd)
        
        # remote is not direct-reachable!
        # request tunnel!
        ssh_fwd_name = "connection to sshd on %s" % (self.host)
        def _start_daemon_with_fwd(fw_entry=None, exception=None):
            if exception is not None:
                msg = "could not establish tcp forward for ssh port!\n%s" % exception
                return cb(exception=msg)

            def _do_start(gssapi_guess=None):
                self.debug("connect to sshd at %s:%s via %s:%s" % (
                    self.host, config.SSH_PORT, 
                    fw_entry[0], fw_entry[1]))
                if gssapi_guess:
                    self.guessed_GSSAPIServerIdentity = gssapi_guess
                    additional_ssh_arguments_inner = " -o GSSAPIServerIdentity=%s" % gssapi_guess
                else:
                    additional_ssh_arguments_inner = ""
                sshcmd = "%s %s %s %s -p %s '%s'" % (
                    ssh_binary, 
                    ssh_args,
                    additional_ssh_arguments_inner,
                    fw_entry[0],
                    fw_entry[1],
                    cmd)
                self._start_daemon_process_bh(cb, sshcmd)
            
            # ssh connection will have to go through tunnel. GSSAPIServerIdentity from ssh will be probably wrong.
            # if not specified by user, try to call dig on the last-hop before or provide the internal host-name as identity.
            if "GSSAPIServerIdentity" in ssh_args:
                return _do_start()
            last_hop = self._last_tcp_forwards[ssh_fwd_name][0]
            # (terminated for loopback not possible without daemon connection...)
            last_hop_host = last_hop[0]
            last_hop_target_ip = last_hop[1]
            
            def _on_dig_output(output=None, exception=None):
                if exception is not None:
                    self.warning("tried to guess GSSAPIServerIdentity by doing a reverse lookup with dig on %s failed: %s" % (
                        last_hop_host, exception))
                    return _do_start(self.host.name)
                return _do_start(output)
            self.debug("will try to find GSSAPI-Server-Identity for ssh connection (towards ip %s) from %s:\n%s" % (
                last_hop_target_ip, last_hop_host,
                pprint.pformat(self._last_tcp_forwards[ssh_fwd_name])))
            return self.manager.get_daemon(last_hop_host).reverse_lookup(_on_dig_output, last_hop_target_ip)

        self.establish_tcp_forward(
            _start_daemon_with_fwd,
            self.manager.host, self.host, config.SSH_PORT, True, ssh_fwd_name
        )

    @create_connection
    def reverse_lookup(self, cb, ip):
        """
        execute 'dig -x IP +short' on host to get a hostname for ip
        calls cb(hostname) or cb(exception=...) on error
        """
        self.debug("trying to do reverse lookup of %r" % ip)
        
        cmd = "dig -x %s +short" % ip
        
        def _with_dig(ret=None, exception=None):
            if exception is not None:
                return cb(exception="starting dig failed: %s" % exception)
            retval, output = ret
            if retval != 0:
                self.debug("%r retval %d, output: %r" % (cmd, retval, output))
                return cb(exception="dig failed: %s" % output)
            
            return cb(output.strip("\n."))
        self.popen_shell_command(_with_dig, cmd, "dig %s" % ip)

    
    @create_connection
    def popen_shell_command(self, cb, cmdline, name="unnamed", env=None, chdir=None, change_user=None):
        """
        will call cb((retval, output)) or cb(exception=...)
        """
        def _with_pid(shcmd=None, exception=None):
            if exception is not None:
                return cb(exception="starting command %s failed: %s" % (name, exception))
            # now wait for pid to terminate and collect its output!
            
            def _on_termination(shcmd):
                output = b"".join(shcmd["output"])
                output = output.decode("utf-8")
                retval = shcmd["retval"] # terminated_by_signal, status        
                self.debug("popen command %s exited with retval %d and output:\n%s" % (name, retval, output))
                return cb((retval, output))
            
            shcmd["terminate_callback"] = _on_termination
            
        return self.start_shell_command(_with_pid, cmdline, name, env, chdir, change_user)
    
    @create_connection
    def start_shell_command(self, cb, cmdline, name="unnamed", env=None, chdir=None, change_user=None):
        """
        will start given cmdline on this daemon.
        calls cb(shcmd) or cb(exception=...)
        shcmd is dict(output=[], pid, ...)
        """
        environment = dict(
            PATH="/bin:/usr/bin:/sbin:/usr/sbin",
        )
        if env:
            environment.update(env)
        if chdir is None:
            chdir = "/tmp"
        
        args = dict(
            name=name, 
            cmdline=cmdline,
            environment=list(environment.items()),
            change_directory=chdir, 
            
            start_in_shell=True, 
            no_pty=True,            
            term_timeout=2,            
            max_output_frequency=10,
            log_output=True,
            policy=self.get_sched_policy_int(-1),
            priority=0            
        )
        
        if self.protocol_version >= 25:
            args["blocking_output"] = True
        
        if change_user is not None:
            if self.protocol_version < 12:
                self.error("daemon is too old (version %d, need 12) to change user to %r" % (self.protocol_version, change_user))
            else:
                if ":" in change_user and self.protocol_version < 25:
                    self.warning("daemon is too old to change user AND group to %r! (version %d, need 25)" % (change_user, self.protocol_version))
                    change_user = change_user.split(":", 1)[0]
                args["change_user_to"] = change_user
        
        def _finalize_start_shell_command(response=None, exception=None):
            if exception is not None:
                return cb(exception=exception)
            pid = response["pid"]
            shcmd = self._shell_commands[pid] = dict(output=[], pid=pid)
            return cb(shcmd)
        self.send_request("start_process", _finalize_start_shell_command, **args)
        
    @needs_finish_callback
    def _start_daemon_process_bh(self, cb, cmd):
        self.debug(cmd)
        # Starting subprocess
        self.had_connection_refused = False
        self.have_arbiter_error = False
        self.up = False
        if platform.system() == "Windows":
            do_close_fds = False
        else:
            do_close_fds = True

        # is that a good idea:
        self.when_ssh_startup_done.reset()
        #if self.when_ssh_startup_done.is_requested:
        #    self.debug("ssh startup already requested...")
        #    return

        self._daemon_start_cmd = cmd

        self.collected_stdouterr.append(("\nlnm exec: %s\n" % cmd).encode("utf-8"))
        p = self.subprocess = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=do_close_fds)
        if have_fcntl:
            fcntl.fcntl(p.stdout, fcntl.F_SETFL, os.O_NDELAY)
            fcntl.fcntl(p.stderr, fcntl.F_SETFL, os.O_NDELAY)

        self.stdout_la = pyutils.line_assembler(self.stdout_line)
        self.stderr_la = pyutils.line_assembler(self.stderr_line)
        self.stdout_la.expect_bytes()
        self.stderr_la.expect_bytes()
        
        if platform.system() == "Windows":
            self.warning("python's subprocess.Popen() :: poll() seems to fail on windows. simply sleeping 2.5 seconds. please consider starting the daemon by other means on the target host!")
            time.sleep(2.5)
            self.up = True
            return cb()
            
        GLib.io_add_watch(p.stdout, GLib.IO_IN | GLib.IO_HUP, self.process_output, "stdout")
        GLib.io_add_watch(p.stderr, GLib.IO_IN | GLib.IO_HUP, self.process_output, "stderr")
        GLib.timeout_add(250, self._check_daemon_start_cmd)
        
        self._start_daemon_status = None
        self.register_error = None
        self.when_ssh_startup_done.call(callback(self._start_daemon_process_bh2, cb))

    def _check_daemon_start_cmd(self):
        status = self.subprocess.poll()
        if status is None:
            # still running
            return True
        
        # process terminated
        if self._start_daemon_status is None:
            self._start_daemon_status = status
            self.info("daemon startup exited with status %d" % status)
            self.when_ssh_startup_done.trigger()
        return False
    
    def process_output(self, fd, why, channel):
        data = fd.read(1500)
        if not data:
            self._check_daemon_start_cmd()
            return False
        la = getattr(self, "%s_la" % channel)
        la.write(data)
        return True

    def stdout_line(self, line):
        self.debug("stdout: %s" % line)
        self.collected_stdouterr.append(line)
        if self.manager.host == self.host and self.manager.isolated_test_mode:
            split = line.split(b"daemon listening on port ", 1)
            if len(split) == 2:
                self.daemon_port = int(split[1].split(b" ", 1)[0])
                self.debug("got daemon's isolated-test-port: %r" % self.daemon_port)
                self.up = True
            return
        if b"arbiter registration succeeded" in line:
            self.up = True
        if b"error from arbiter" in line:
            self.have_arbiter_error = True
    
    def stderr_line(self, line):
        self.collected_stdouterr.append(line)
        if b"Connection refused" in line:
            self.had_connection_refused = True
        self.debug("stderr: %s" % line)
        
    def _start_daemon_process_bh2(self, cb):
        if self.up:
            return cb()

        if self.manager.host == self.host and self.manager.isolated_test_mode:
            return cb(exception="could not start isolated-test ln-daemon!")

        if self.guessed_GSSAPIServerIdentity:
            self.warning("WARNING: we tried to reach host %r via a tcp-tunnel, but the daemon connection failed!\n"
                         "maybe the ssh client got a permission denied because of an invalid GSSAPIServerIdentity?\n"
                         "we guessed %r!\n"
                         "maybe you want to specify another name in your hosts section:\n"
                         "  hosts\n"
                         "  %s: ..., additional_ssh_arguments: -o GSSAPIServerIdentity=better-hostname\n"
                         % (self.host.name, self.guessed_GSSAPIServerIdentity, self.host.name))
        
        if self.register_error is not None:
            return cb(exception="daemon registration failed:\n%s" % self.register_error)
        
        if self.had_connection_refused:
            self.info("got connection refused. probably there is no ssh daemon on this host, trying telnet!")
        if self.have_arbiter_error:
            self.info("daemon had error from arbiter, again trying to query arbiter for daemon-port!")
        
        if self.have_arbiter_error:
            try:
                return self.connect(cb)
            except Exception:
                return cb(exception="still get error from arbiter!?: %s" % str(sys.exc_info()[1]))

        if self._start_daemon_status != 0 and "cissy run" in self._daemon_start_cmd:
            # assume 127 - cissy: command not found
            # try starting $HOME/.ln_daemon/start instead
            self.info("detected failed daemon-start trial with 'cissy run'. trying ln_daemon from home...")
            self.start_daemon_process(cb, use_ln_daemon_from_home=True)
            return

        def _mention_failed_ssh(exception=None):
            if exception is not None:
                if self._is_ln_daemon_from_home_trial:
                    # mention to user that "cissy run" & start-from-home failed -> hwo to install ln_daemon in home
                    self.manager.add_gui_question("ln_daemon_connect_error", self.host.name, dict(
                        hostname=self.host.name,
                        output=self.get_collected_stdouterr(),
                        is_ln_daemon_from_home_trial=self._is_ln_daemon_from_home_trial
                    ))
                return cb(exception="tried to connect via ssh and telnet")
            return cb()
            
        # try via telnet to a vxworks machine!
        if self.manager.host.is_direct_reachable(self.host):
            host_ip = self.manager.host.get_ip_from(self.host)
            return self._start_daemon_process_telnet(_mention_failed_ssh, host_ip, config.TELNET_PORT)
        def _start_daemon_with_fwd_telnet(fw_entry=None, exception=None):
            if exception is not None:
                msg = "could not establish tcp forward for telnet port!"
                return cb(exception=msg)
            return self._start_daemon_process_telnet(_mention_failed_ssh, fw_entry[0], fw_entry[1])
        self.establish_tcp_forward(
            _start_daemon_with_fwd_telnet,
            self.manager.host, self.host, config.TELNET_PORT, True, "connection to telnetd on %s" % (self.host),            
        )

    @needs_finish_callback
    def _start_daemon_process_telnet(self, cb, ip, port):
        self.info("trying to connect to telnet port on ip %s, port %s host %r" % (ip, port, self.host))
        try:
            vx = pyutils.vx.vx_telnet(ip, port)
        except Exception:
            return cb(exception="failed to connect to telnet daemon: %s" % (sys.exc_info()[1]))
        vx.prompts = ["[vxWorks *]# ", "-> "] # todo is '->' right?

        # catch common case that a tcp forward accepted the connection but the remote did not accept
        try:
            vx.wait_prompt() # todo timeout?
        except Exception:
            return cb(exception="failed to connect to telnet daemon: %s" % (sys.exc_info()[1]))
        
        # check vxworks version
        answer = vx.run_command('version')
        version = None
        varch = "x86"
        for line in answer.split('\n'):
            if 'VxWorks' in line and 'version' in line:
                idx = line.find('version')
                tmp = line[idx:]
                version = tmp.split()[1].strip('.')
                if "armv7" in line.lower():
                    varch = "armv7"
        if version is None:
            version = '6.7'
            self.warning("no version information in output of 'version': %r\nassuming %s" % (answer, version))
        else:
            self.info("vxworks version: %r, arch: %r" % (version, varch))
            if version.count(".") == 2:
                if version == "6.9.3":
                    self.info("using major.minor.patch for vxworks 6.9.3 to build target arch: %r" % version)
                else:
                    version = ".".join(version.split(".")[:2])
                    self.info("only using major.minor to build target arch: %r" % version)

        arch = "vxworks%s-%s-gcc4.x" % (version, varch)
        #have_close_fd = self.load_vx_module(vx, "/home/rtosvx/devel/root/%s/lib/modules/x86-vxworks6.9smp-gnu4.x/rtphook.o" % arch, "rtphook.o")
        
        daemon_start = config.telnet_daemon_start.replace("$(ARCH)", arch)
        self.info("waiting for daemon to startup successfully...")
        output = vx.run_command(daemon_start, wait=False)
        #regexes = ["daemon listening on port", prompt_regrex]
        #regexes = ["arbiter registration", prompt_regrex]
        #index, match, text = tn.expect(regexes, timeout=5)
        #if index == -1:
        #    raise Exception("could not start daemon on %s! got no/unknown output via telnet:\n%r" % (self.host, text))
        #if index == 0:
        #    self.info("daemon seems to be running: %r" % text)
        #    tn.close()
        #    return
        #raise Exception("could not start daemon on %s! please check output:\n%s" % (self.host, text))
        #if "arbiter registration" in output:
        #    return True
        #
        what, out = vx.wait_string(["succeeded", "[vxWorks *]#"])
        if "succeeded" in what:
            return cb()
        cb(exception="failed to start daemon on %s %s:\n%s" % (self.host, arch, out))

    def load_vx_module(self, vx, module_file, module_name, init_commands):
        if isinstance(init_commands, (str, unicode)):
            init_commands = [init_commands]
        # check if module is already loaded
        output = vx.run_command("module list", do_print=False)
        for line in output.split("\n"):
            line = line.strip()
            if not line:
                continue
            if line.startswith("MODULE NAME"):
                continue
            if line.startswith("---"):
                continue
            fields = re.split("[ \t]", line)
            if fields[0] == module_name:
                return True # module already loaded, nothing todo...
        # module not loaded - load!
        self.info("module %r not loaded:\n%s" % (module_name, output))
        if not os.path.isfile(module_file):
            raise Exception("error, module file %r for not-loaded module %r does not exist!" % (module_file, module_name))
        self.info("loading module %r" % module_file)
        output = vx.run_command("module load %s" % module_file, do_print=False)
        self.info("loaded module:\n%s" % output)
        for cmd in init_commands:
            self.info("executing init command %r" % cmd)
            output = vx.run_command(cmd, do_print=False)
            self.info("%s" % output)
        return True

    @create_connection("when_connected")
    def new_tcp_forward(self, cb, to_ip, to_port, listen_ip=None, keep_listening=False, data=None, listen_port=-1, send_source=False):
        """
        to_port can be unsigned int(tcp port) or string(other protocols/target-types, to_ip is ignored in this case)

        to_port can start with "unix:" to use a unix domain socket as target.
        
        will call cb(port) on success or cb(exception=...) on error
        """
        
        if listen_port is None:
            listen_port = -1
        if listen_port == -2 and self.protocol_version < 11:
            raise Exception("this daemon uses too old protocol version %d (need atleast 11)."
                            "it can not search for a free x-forwarding port!" % self.protocol_version)
        if listen_port not in (-1, -2) and self.protocol_version < 18:
            self.error("you requested a listen_port of %r - but your daemon is too old and has a bug. please update!" % listen_port)

        args = dict(
            to_ip=to_ip,
            to_port=to_port,
            listen_ip=listen_ip,
            listen_port=listen_port,
            keep_listening=keep_listening,
            data=repr(data))
        
        if self.protocol_version >= 29:
            args["send_source"] = send_source
        elif send_source:
            self.warning("this ln_daemon is too old to send source-information with tcp-forwardings!")

        def _have_forward(response=None, exception=None):
            if exception is not None:
                return cb(exception="%s: failed to create new_tcp_forward to %s:%s:\n%s" % (
                    self.host, to_ip, to_port, exception))
            return cb(response["listen_port"])            
        self.send_request("new_tcp_forward", _have_forward, **args)

    @create_connection("when_connected")
    def get_tcp_forward(self, cb, to_ip, to_port, listen_ip, keep_listening, name, listen_port=None, send_source=False, check_only=False):
        """
        checks first whether we already have a matching forward, if not it will call .new_tcp_forward().
        if check_only is True do only return whether or not this forward already exists!
        
        will call cb(port) on finish or cb(exception=...) on error
        """
        #if listen_port is None:
        forward_key = "%s:%s at %s %s %s" % (to_ip, to_port, listen_ip, keep_listening, name)
        #else:
        #    forward_key = "%s:%s at %s %s %s %r" % (to_ip, to_port, listen_ip, keep_listening, name, listen_port)
        port = self.tcp_forwards.get(forward_key)
        if check_only:
            cb(port)
            return
        
        if port is None:
            self.debug("forward_key %r not found - have those:" % forward_key)
            k = list(self.tcp_forwards.keys())
            k.sort()
            have_conflicting = None
            to_delete = []
            for kk in k:
                self.debug("            %r" % kk)
                kk_name = kk.split(" ", 4)[4]
                if " from " not in kk_name:
                    continue
                full_kk_name = kk_name
                kk_name, kk_port = kk_name.rsplit(" from ", 1)
                try:
                    if "/" in kk_port:
                        kk_port = kk_port.rsplit("/", 1)[1]
                    kk_port = int(kk_port)
                except Exception:
                    pass
                if kk_port == listen_port:
                    self.info("have conflicting forward at listening_port %s: %r" % (kk_port, kk_name))
                    if self.protocol_version < 22:
                        raise Exception("have conflicting tcp_forward and can not remove on that old daemon. you will have to restart that daemon!")
                    if full_kk_name == name:
                        self.info("conflicting forward was for same purpose ... remove that forward!")
                        to_delete.append(name)
                        continue
                    # it was for another purpose. still needed?
                    needed_by = self.is_forward_still_needed(kk_name)
                    if needed_by:
                        raise Exception("can not create tcp forward %r because %r used by %r already uses that port!" % (name, kk_name, needed_by))
                    to_delete.append(full_kk_name)

            def _delete_next():
                if not to_delete:
                    # try again!
                    return self.get_tcp_forward(cb, to_ip, to_port, listen_ip, keep_listening, name, listen_port, send_source)
                name_to_del = to_delete.pop(0)
                def _on_delete_answer(found=None, exception=None):
                    return _delete_next()
                self.delete_tcp_forward(_on_delete_answer, name_to_del)
            if to_delete:
                return _delete_next()
        
        if port is None or (listen_port not in (None, -1, -2) and port != listen_port):
            def _on_response(port=None, exception=None):
                if exception is not None:
                    msg = "could not create forward %r: %s" % (forward_key, exception)
                    self.error(msg)
                    return cb(exception=msg)
                self.debug("new forward: %r at %d" % (forward_key, port))
                self.tcp_forwards[forward_key] = port
                return cb(port)
            
            return self.new_tcp_forward(
                _on_response,
                to_ip, to_port, listen_ip=listen_ip, listen_port=listen_port,
                keep_listening=keep_listening, data=dict(name=name), send_source=send_source
            )
        
        self.debug("reused forward: %r at %d" % (forward_key, port))
        if forward_key in self._forwards_to_destroy:
            del self._forwards_to_destroy[forward_key]
        cb(port)
        
    @create_connection
    def delete_tcp_forward(self, cb, name, only_if_known=False):
        """
        will call cb(found) or cb(exception=...)
        found will be True / False
        """

        if self.protocol_version < 22:
            raise Exception("can not delete tcp forward %r on host %r because that daemon is too old!" % (name, self.host.name))
        
        search = " %s" % name
        def _on_answer(response=None, exception=None):
            if exception is not None:
                msg = "%s: failed to delete tcp_forward %r:\n%s" % (self.host, name, exception)
                self.error(msg)
                return cb(exception=msg)
            for forward_key, port in list(self.tcp_forwards.items()):
                if forward_key.endswith(search):
                    del self.tcp_forwards[forward_key]
            return cb(bool(response["found"]))
        if only_if_known:
            found = False
            for forward_key, port in list(self.tcp_forwards.items()):
                if forward_key.endswith(search):
                    found = True
                    self.debug("will remove forward_key %r" % (forward_key, ))
            if not found:
                cb(False)
                return
        self.send_request("delete_tcp_forward", _on_answer, data=repr(dict(name=name)))

    # no @create_connection, will be used inside arbiter connection across multiple hops
    @report_exception_in_cb
    def establish_tcp_forward(
            self, cb, from_host, to_host, to_port, keep_listening, name,
            from_port=None, terminate_at_loopback=False, terminate_at_loopback_ip="127.0.0.1", created_forwards=None, send_source=False,
            need_full_daemon_registration=False, listen_ip="127.0.0.1", check_only=False, do_delete=False
    ):
        """
        if need_full_daemon_registration is True: find route, then make sure that we have a fully-registered 
        daemon connection to each host before starting to create new forwards!
        if check_only is True, do only check if all needed forwards are in place! -- if not call cb(None)
        if do_delete is True, do delete forwards on needed hosts instead of creating them! -- will call cb(None) when done
        
        will call cb((ip, port)) or cb(exception=...)
        """
        
        self.debug("establish_tcp_forward from_host %s from_port %r to_host %s to_port %s, terminate_at_loopback_ip: %r, terminate_at_loopback: %r, keep_listening: %r, check_only: %s" % (
            from_host, from_port, to_host, to_port, terminate_at_loopback_ip, terminate_at_loopback, keep_listening, check_only))
        if created_forwards is None:
            created_forwards = {}
        # find route over host interfaces to target
        route = from_host.search_route_to(to_host)
        
        rev_route = route[::-1] ## remove from_host start interface
        # go route backward and create tcp_forwarder objects pointing to to_host:to_port
        outgoing = None
        tcp_forwards = []
        if terminate_at_loopback:
            target_ip = terminate_at_loopback_ip
            target_port = to_port
            
        for step, hop_interface in enumerate(rev_route):
            if step == 0 and not terminate_at_loopback:
                # target interface at to_host
                target_ip = hop_interface.ip_address
                target_port = to_port
                continue
            if outgoing is None:
                outgoing = hop_interface
                continue
            incoming_ip = hop_interface.host.get_ip_from(outgoing.host)
            tcp_forwards.append(
                # on host,      to ip,     to port,     listen on interface
                [outgoing.host, target_ip, target_port, incoming_ip])
            target_ip = incoming_ip
            target_port = None # currently unknown
            outgoing = hop_interface
        if terminate_at_loopback:
            # add from_host lo to l
            tcp_forwards.append(
                # on host,      to ip,     to port,     listen on interface
                [outgoing.host, target_ip, target_port, listen_ip])

        if do_delete:
            verb = "delete"
            check_only = False
        elif check_only:
            verb = "check"
        else:
            verb = "establish"
        
        msgs = ["tcp forwards to %s:" % verb]
        for host, to_ip, to_port, listen_ip in tcp_forwards:
            msgs.append("on %s, listen at %s forward to %s:%s" % (host, listen_ip, to_ip, to_port))
        self.debug("\n".join(msgs))

        if len(tcp_forwards) == 0:
            return cb(exception="list of tcp forward hops is empty")
        self._last_tcp_forwards[name] = tcp_forwards
        
        def _establish_tcp_forward_step(step, host_port=None):
            if step == len(tcp_forwards):
                if do_delete:
                    return cb()
                ip, port = from_host.get_ip_from(host_port[0]), host_port[1]
                self.debug("forward entry at %s:%d" % (ip, port))
                return cb((ip, port))
            
            host, to_ip, to_port, listen_ip = tcp_forwards[step]
            if to_port is None and not do_delete:
                to_port = host_port[1]
            self.info("contact daemon on host %s to %s forward to %s:%s" % (host, verb, to_ip, to_port))
            if host == from_host and from_port is not None:
                listen_port = from_port
            else:
                listen_port = None

            def _establish_tcp_forward_daemon_step(daemon):
                #self.debug("wait for daemon on %s for next tcp forward responded!" % host)
                if daemon.when_connected.get_reached_or_fail() is not True:
                    msg = "could not reach daemon %s: %s" % (daemon.host, daemon.when_connected.fail_message)
                    return cb(exception=msg)
                if not do_delete:
                    def _got_new_to_port(new_to_port=None, exception=None):
                        if exception is not None:
                            return cb(exception=exception)
                        if check_only and new_to_port is None:
                            cb(None)
                            return 
                        created_forwards[host.name] = name
                        if not check_only: self.debug("new forward is listening on port %s" % new_to_port)
                        return _establish_tcp_forward_step(step + 1, host_port=(daemon.host, new_to_port))

                    daemon.get_tcp_forward(
                        _got_new_to_port,
                        to_ip, to_port, listen_ip, keep_listening, name,
                        listen_port=listen_port, send_source=send_source,
                        check_only=check_only
                    )
                else: # delete this forward
                    def _delete_done(found=None, exception=None):
                        if exception is not None:
                            return cb(exception=exception)
                        created_forwards[host.name] = name
                        return _establish_tcp_forward_step(step + 1)
                    daemon.delete_tcp_forward(
                        _delete_done,
                        name,
                        only_if_known=True
                    )                    
                return

            #self.debug("wait for daemon on %s for next tcp forward" % host)
            self.manager.with_daemon_for_host(
                host, "when_connected",
                _establish_tcp_forward_daemon_step
            )

        if not check_only and not need_full_daemon_registration:
            return _establish_tcp_forward_step(0)
        
        # ...quickly do full connect to each and every host on route!
        def _connect_daemon(step):
            if step == len(tcp_forwards):
                return _establish_tcp_forward_step(0)
            host = tcp_forwards[step][0]
            #self.debug("wait for daemon on %s" % host)
            def _register_finished(daemon):
                if check_only and not daemon.is_connected():
                    return cb(None) # nope, tunnel is DOWN!
                return _connect_daemon(step + 1)
            self.manager.with_daemon_for_host(
                host, "when_register_finished",
                _register_finished
            )
        _connect_daemon(0)
        
    @needs_finish_callback
    def connect(self, cb):
        """
        connect to arbiter running on this host and ask for matching userid!

        cb will be called with cb(ret=..., exception=...)
        """
        self.connect_is_running = True

        if self.manager.isolated_test_mode and self.host == self.manager.host:
            # connect new, local, private ln_daemon
            # we already now its port number!
            self.debug("connect to isloated test-daemon")
            return self.connect_daemon(cb, self.daemon_port)

        arbiter_port = self.host.arbiter_port or config.DAEMON_PORT
        if self.manager.host.is_direct_reachable(self.host):
            self.debug("connect to daemon-arbiter at %s - %s:%s to request daemon for uid %s" % (self.host, self.manager.host.get_ip_from(self.host), arbiter_port, self.host.uid))
            return self._connect_bh((self.manager.host.get_ip_from(self.host), arbiter_port), cb)
        
        self.debug("this arbiter is not directly reachable from %s" % self.manager.host)

        def _got_arbiter_tcp_forward(arbiter_address=None, exception=None):
            if exception is not None:
                return cb(exception=exception)
            self.debug("connect to daemon-arbiter at %s:%s via %s:%s to request daemon for uid %s" % (
                self.host, arbiter_port, 
                arbiter_address[0], arbiter_address[1], self.host.uid))
            self._connect_bh(arbiter_address, cb)
        
        self.establish_tcp_forward(
            _got_arbiter_tcp_forward,
            self.manager.host, self.host, arbiter_port, True,
            "connection to arbiter on %s" % (self.host))
        
    def _connect_bh(self, arbiter_address, cb):
        try:
            arbiter = arbiter_connection(arbiter_address[0], arbiter_address[1])
        except Exception:
            self.debug("arbiter connection not successful:\n%s" % traceback.format_exc())
            return cb(exception=str(sys.exc_info()[1]))
        # first try host.uid
        self.daemon_port = None
        uid = None
        try:
            self.daemon_port = arbiter.query_daemon_port(uid=self.host.uid)
            uid = self.host.uid
        except Exception:
            if self.manager.sysconf.instance_config.do_not_try_uids_of_clients:
                self.connect_is_running = False
                arbiter.close()
                return cb(exception=str(sys.exc_info()[1]))
            self.debug("failed: %s" % traceback.format_exc())
            
        if self.daemon_port:
            self.connect_is_running = False
            arbiter.close()
            self.debug("arbiter reported daemon_port %s for expected uid %s" % (self.daemon_port, uid))            
            return self.connect_daemon(cb, self.daemon_port)
            
        # then try uid's of connected clients, IFF not disabled
        tested_uids = []
        clients_to_try = list(self.manager.iterate_clients_on_host(self.host, return_all=True))

        def _try_connect_answer(ret=None, exception=None):
            if exception is None:
                self.connect_is_running = False
                arbiter.close()
                return cb(ret)
            if "daemon needs authentification via key file" in exception:
                uid = tested_uids[-1]
                
                self.connect_is_running = False
                arbiter.close()
                return cb(exception=(
                    "there is a daemon running for uid %s but it is locked!\n"
                    "restart that daemon without locking it or provide the needed private key to access it!\n"
                    "(e.g. by specifying  'daemon_private_key_file' in the hosts-section of %s or your instance-section)") % (
                        uid, self.host))
                
            _try_client_uid()
            
        def _try_client_uid():
            while True:
                if not clients_to_try:
                    self.connect_is_running = False
                    arbiter.close()
                    return cb(exception="there is no daemon running for host's configured uid %s nor uid's of connected clients (%s)!" % (
                        self.host.uid, ",".join(map(str, tested_uids))))
                    
                clnt = clients_to_try.pop(0)
                if clnt.uid in tested_uids:
                    continue
                tested_uids.append(clnt.uid)
                
                try:
                    self.info("trying user %s from client %r" % (clnt.uid, clnt.program_name))
                    self.daemon_port = arbiter.query_daemon_port(uid=clnt.uid)
                    uid = clnt.uid
                except Exception:
                    self.debug("failed: %s" % str(sys.exc_info()[1]))
                    continue
                
                # try to connect
                try:
                    self.debug("arbiter reported daemon_port %s for uid %d, try to connect..." % (self.daemon_port, uid))
                    return self.connect_daemon(_try_connect_answer, self.daemon_port)
                except Exception:
                    self.debug("failed: %s" % str(sys.exc_info()[1]))
                    continue
        _try_client_uid()
    
    @needs_finish_callback
    def _establish_manager_reverse_forward(self, cb):
        """
        calls either cb() or cb(exception=...)
        """
        mgr_port = self.manager.sysconf.instance_config.manager_port

        def _daemon_reverse_forward_finished(address=None, exception=None):
            if exception is not None:
                return cb(exception=exception)
            self.manager_reverse = address
            self.manager_reverse_port = mgr_port
            self.debug("manager_reverse: %s" % (self.manager_reverse, ))
            cb()
        
        self.debug("need to establish reverse forward to manager port %s!" % mgr_port)
        self.establish_tcp_forward(
            _daemon_reverse_forward_finished,
            self.host, self.manager.host, mgr_port, True,
            "reverse mgr forward to manager on %s:%s" % (self.manager.host, mgr_port),
            send_source=True
        )
        
    @needs_finish_callback
    def connect_daemon(self, cb, daemon_port):
        """
        connect to the daemon running on this host at given port!
        calls cb() or cb(exception=...)
        """
        if self.manager.host.is_direct_reachable(self.host):
            ip_address = self.manager.host.get_ip_from(self.host)
            self.manager_reverse = None
            self.upstream_daemon = None
            return self._connect_daemon_bh(cb, ip_address, daemon_port)
        
        self.debug("this daemon is not directly reachable from %s" % self.manager.host)
        fw_name = "connection to daemon on %s:%s" % (self.host, daemon_port)
        def _daemon_forward_finished(address=None, exception=None, fw_name=fw_name):
            if exception is not None:
                return cb(exception=exception)

            last_hop = self._last_tcp_forwards[fw_name][0]
            self.upstream_daemon = self.manager.get_daemon(last_hop[0])

            forward_host, forward_port = address
            self._establish_manager_reverse_forward(callback(self._connect_daemon_bh, cb=cb, ip_address=forward_host, daemon_port=forward_port))
            
        self.establish_tcp_forward(
            _daemon_forward_finished,
            self.manager.host, self.host, daemon_port, False,
            fw_name)

    @needs_finish_callback
    def _connect_daemon_bh(self, cb=None, ip_address=None, daemon_port=None, exception=None):
        if exception is not None:
            self.error("failed to establish manager reverse forward: %s" % exception)
            
        self.info("connect to daemon at %s:%s" % (ip_address, daemon_port))
        self.socket = socket.socket()
        self.socket.settimeout(5)
        try:
            self.socket.connect((ip_address, daemon_port))
        except Exception:
            self.error("failed to connect daemon at %s:%s: %s" % (ip_address, daemon_port, sys.exc_info()[1]))
            cb(exception=str(sys.exc_info()[1]))
            return
            
        self.socket.settimeout(None)
        if platform.uname()[0] != "Windows":
            self.socket.setsockopt(socket.IPPROTO_TCP, socket.SO_KEEPALIVE, 1)
        self.register_is_finished = False
        self._disconnect_reason = None
        self.com = CommunicationModule(self.socket, self, self.got_request)
        self.com.set_binary_eval_keys(["output", "key_challange"]) # ugly hack to be py3 compatible with old daemons!
        self.connected = True
        self.register_error = None
        self.when_registered.call(callback(self._connect_daemon_bh2, cb))
        # self.com.blocking_read() is done by io handler?!
        
    @needs_finish_callback
    def _connect_daemon_bh2(self, cb):
        if self.register_error is not None:
            return cb(exception="daemon registration failed:\n%s" % self.register_error)
        if not self.connected:
            return cb(exception="lost connection to daemon while waiting for registration1")
        
        self.up = True
        # if daemon was locked, test whether its unlocked!
        self.list_processes_response_after_start = None

        def _connect_done(cb=cb):
            self.when_connected.trigger()
            self.call_hook("on_daemon_connect")
            cb(True)
            
        # test whether daemon accepted our unlock or lock-trial:
        def _list_processes_answer(processes=None, exception=None, cb=cb):
            if exception is not None:
                if self.was_locked:
                    msg = "_connect_daemon_bh2() check for passed key challange: %s" % exception
                else:
                    msg = "_connect_daemon_bh2() check for lock-request: %s" % exception
                self.debug(msg)
                return cb(exception=msg)
            self.list_processes_response_after_start = processes
            _connect_done()
        self._list_processes(_list_processes_answer)

    def daemon_register_daemon(self, **request):
        try:
            return self.real_daemon_register_daemon(**request)
        except Exception:
            self.register_error = traceback.format_exc()
            self.error("failed to register:\n%s" % self.register_error)
            return False

    def real_daemon_register_daemon(self, **request):
        self.pid = request["pid"]
        if request.get("protocol_version", 0) < 10:
            self.error("error: daemon running on host %s is running old protocol version %d. we need at least %d - please restart the daemon!" % (
                    self.host, request.get("protocol_version", 0), PROTOCOL_VERSION))
        if request.get("protocol_version", 0) < PROTOCOL_VERSION:
            self.warning("daemon running on host %s is running older protocol version %d then we %d - please consider restarting the daemon!" % (
                    self.host, request.get("protocol_version", 0), PROTOCOL_VERSION))
        self.protocol_version = request.get("protocol_version", 0)
        self.library_version = request.get("library_version", 0)
        self.max_shm_version = get_max_shm_version_from_lib_version(self.library_version)
        if self.max_shm_version >= 3 and self.manager.sysconf.instance_config.no_shmv3:
            self.debug("daemon could do shmv%d, but instance-flags request limit to v2" % self.max_shm_version)
            self.max_shm_version = 2
        self.have_glibc_atleast_2_25 = request.get("have_glibc_atleast_2_25") # either None (unknown), 0(old glibc), 1(new glibc)
            
        self.svn_revision = request.get("svn_revision", 0)
        self.is_modified = request.get("is_modified", 0)
        if self.host.arch is None:
            self.architecture = request["architecture"]
        else:
            self.architecture = self.host.arch
        
        self.is_linux   = request.get("is_linux",
            "linux" in self.architecture or "sled" in self.architecture or "ubuntu" in self.architecture or self.architecture.startswith("osl"))
        self.is_android = request.get("is_android", "android" in self.architecture)
        self.is_qnx     = request.get("is_qnx",     "qnx"     in self.architecture)
        self.is_vxworks = request.get("is_vxworks", "vxworks" in self.architecture)
        self.is_windows = request.get("is_windows", "win"     in self.architecture)
        
        for what in "uid,gid,USER_env,HOME_env,USER,HOME,SHELL,user_real_name".split(","):
            setattr(self, what, request.get(what, None))
        if "endianess" in request:
            self.endianess = request["endianess"]
        elif "endianness" in request:
            self.endianess = request["endianness"]
        else:
            if "-x86-" in self.architecture or "-x86_64-" in self.architecture or "-armv7-" in self.architecture or self.architecture.startswith("arm-"):
                self.endianess = "little"
            elif "ppc" in self.architecture or "powerpc" in self.architecture:
                self.endianess = "big"
            else:
                self.endianess = "little"
                self.warning("unknown endianess for architecture %r. assuming %r" % (self.architecture, self.endianess))

        self.have_shm_version = self.protocol_version >= 24
        self.have_release_all_resources = self.protocol_version >= 24

        if self.manager_reverse is not None:
            mgr = dict(
                manager_ip=self.manager_reverse[0],
                manager_port=self.manager_reverse[1])
        else:
            mgr = dict(
                manager_ip=self.host.get_ip_from(self.manager.host),
                manager_port=self.manager.sysconf.instance_config.manager_port)

        is_expected_uid = self.host.uid == self.uid or self.host.uid == self.USER
        do_lock_daemon = is_expected_uid or self.manager.sysconf.instance_config.do_lock_daemons_with_unexpected_uids
        pub_key_file = self.host.daemon_public_key_file or self.manager.sysconf.instance_config.daemon_public_key_file
        self.was_locked = "key_challange" in request
        self._locked_by_info = ""
        if self.was_locked:
            message = request["key_challange"]
            self._locked_by_info = request.get("locked_by", "")
            if self._locked_by_info:
                locked_by_info = self._locked_by_info + " "
            else:
                locked_by_info = ""
            self.debug("daemon is locked by public key %s-> requests key-challange for message %r" % (locked_by_info, message))

            if self.protocol_version < 31:
                self.register_error = (
                    "daemon is locked and uses old (< 31) protocol!\n"
                    "you will have to manually kill that daemon (e.g. via `ssh %s killall -TERM ln_daemon`)\n"
                    "if automatic ln_daemon-start does not work for you, try to start `ssh %s /volume/software/common/packages/links_and_nodes/0.13.25/bin/osl153-x86_64/ln_daemon`"
                ) % (self.host, self.host)
                self.error(self.register_error)
                self.when_registered.trigger()
                return False # give up
            pub_key_file = self.host.daemon_public_key_file or self.manager.sysconf.instance_config.daemon_public_key_file
            if pub_key_file == "never":
                self.register_error = (
                    "daemon needs authentification via key file %s-- but you requested to never lock this daemon!\n"
                    "kill existing daemon (`killall -TERM ln_daemon`) and try again or provide the needed private-key!\n"
                    "abort daemon connection!"
                ) % locked_by_info
                self.when_registered.trigger()
                return False
            key_file = self.host.daemon_private_key_file or self.manager.sysconf.instance_config.daemon_private_key_file
            if key_file is None or key_file.lower() == "none":
                self.register_error = (
                    "daemon needs authentification via key file %s-- but there is no daemon_private_key_file defined!\n"
                    "kill existing daemon (`killall -TERM ln_daemon`) and try again or provide the needed private-key!\n"
                    "abort daemon connection!"
                ) % locked_by_info
                self.when_registered.trigger()
                return False
            self.debug("using private key file %r to sign challange" % key_file)
            try:
                mgr["signature"] = links_and_nodes.dsa_dss1_sign_message(key_file.encode("utf-8"), message)
            except Exception:
                self.register_error = (
                    "daemon needs authentification via key file %s-- but trying to sign with key from file %s failed:\n"
                    "%s\n"
                    "kill existing daemon (`killall -TERM ln_daemon`) and try again or provide the correct private-key!\n"
                ) % (locked_by_info, key_file, sys.exc_info()[1])
                self.when_registered.trigger()
                return False
            self.debug("computed signature: %r" % mgr["signature"])
        elif pub_key_file and pub_key_file.lower() != "none" and pub_key_file != "never":
            if not do_lock_daemon:
                self.info("daemon is not locked but has unexpected uid %r != %r. will not lock it!" % (self.uid, self.host.uid))
            elif links_and_nodes.NO_DAEMON_AUTH:
                self.info("daemon IS NOT LOCKED by public key but we are compiled WITH NO_DAEMON_AUTH -> will NOT try to lock!")
            else:
                self.info("daemon WAS NOT YET LOCKED by public key -> will now send a lock request!")
                try:
                    with open(pub_key_file, "rb") as fp:
                        pub_key_contents = fp.read().decode("utf-8")
                except Exception:
                    self.register_error = "daemon was not yet locked. tried to read public-key-file %r but this failed:\n%s" % (
                        (pub_key_file, traceback.format_exc))
                    return False
                mgr["lock_with_pub_key"] = pub_key_contents
        elif pub_key_file == "never":
            if self.protocol_version < 34:
                self.warning("daemon is too old to be set to never-lock!")
            elif not do_lock_daemon:
                self.info("daemon is not locked but has unexpected uid %r != %r. will not set it to never-lock!" % (self.uid, self.host.uid))
            else:
                mgr["lock_with_pub_key"] = "never"

        if "lock_with_pub_key" in mgr:
            mgr["locked_by"] = "user %s on %s via %s instance %r" % (
                tools.get_login(), self.manager.host,
                mgr["manager_ip"] + ":" + str(mgr["manager_port"]),
                self.manager.sysconf.instance_config.name
            )
        self.debug("daemon registered itself! sending register-response!")
        self.com.send(
            response="register_daemon", 
            success="success", 
            manager_protocol=PROTOCOL_VERSION,
            instance_name=self.manager.sysconf.instance_config.name,
            **mgr)
        
        self.registered = True
        self.when_registered.trigger()
        
        return False # wants to be removed

    def inject_state_output(self, state_name, output, encode=False, **kwargs):
        state = self.manager.sysconf.states[state_name]
        if encode:
            output = output.encode(state.output_encoding)
        self.manager.add_process_output(state, output, daemon=self)

    def daemon_state_process_started(self, **r):
        state_name = r["name"]
        self.info("state %r: %s command started with pid %s" % (state_name, r["state_cmd"], r["pid"]))
        omsg = "[%s] %-5.5s command started\n" % (r["pid"], r["state_cmd"])
        self.inject_state_output(state_name, omsg, state_cmd=r["state_cmd"], encode=True)
        return True

    def daemon_disconnect_reason(self, request, reason):
        self._disconnect_reason = reason
        self.warning(reason) # warning only, error will be reported on eof

    def daemon_replaced_manager(self, request, other):
        self.warning("this manager connection replaced an already existing manager at %r that uses the same `instance_name`!" % other)

    def daemon_process_output(self, **response):
        """
        daemon reported that given process did output some bytes!
        response["output"] is bytes-string from process!
        """
        #self.debug("daemon_process_output: %r" % (response, ))
        #self._queue_call(callback(self.call_hook, "process_output", msg))

        pid = response["pid"]
        state_cmd = response.get("state_cmd")
        if state_cmd is not None:
            # output from a state command!
            self.info("state %r: %r cmd output: %r" % (response["name"], state_cmd, response["output"]))
            self.inject_state_output(response["name"], response["output"], state_cmd=state_cmd)
            return True

        if pid in self._shell_commands:
            return self._shell_commands[pid]["output"].append(response["output"])
        if pid in self._started_processes:
            obj = self._started_processes[pid]
        elif response["name"] in self.manager.sysconf.processes:
            obj = self.manager.sysconf.processes[response["name"]]
        else:
            self.warning("got output from unknown process %r pid %s on %s: %r" % (
                response["name"],
                response["pid"],
                self.host,
                response["output"]))
            return
        self.manager.add_process_output(obj, response["output"], daemon=self)

    def daemon_process_terminated(self, **r):
        """
        daemon reported that a process did terminate!
        """
        #self._queue_call(callback(self.call_hook, "process_terminated", msg))
        if r.get("state_cmd"):
            state_name = r["name"]
            self.info("state %r: %s command with pid %s terminated with retval %s" % (
                    state_name, r["state_cmd"], r["pid"], r["retval"]))
            omsg = "[%s] %-5.5s command terminated with return value %s" % (
                r["pid"], r["state_cmd"], r["retval"])
            self.inject_state_output(state_name, omsg, state_cmd=r["state_cmd"], encode=True)
            return True

        self.info("process on %s with pid %s terminated: retval: %s, status: %d, terminated_by_signal: %s" % (
            self.host, r["pid"], r["retval"], r["status"], r["terminated_by_signal"]))
        return self._terminate_process(r["pid"], r)

    def _terminate_process(self, pid, r=None):
        shcmd = self._shell_commands.get(pid)
        if shcmd:
            shcmd.update(r)
            terminate_callback = shcmd.get("terminate_callback")
            if terminate_callback:
                terminate_callback(shcmd)
            del self._shell_commands[pid]
            return
        
        if pid not in self._started_processes:
            return True
        obj = self._started_processes[pid]
        del self._started_processes[pid]
        if r is not None:
            retval, terminated_by_signal, status = r.get("retval"), r.get("terminated_by_signal"), r.get("status")
        else:
            retval, terminated_by_signal, status = None, None, None
        self.call_hook("process_terminated", obj, retval, terminated_by_signal, status)
        #self._queue_call(self.call_hook, "process_terminated", obj, r.get("retval"), r.get("terminated_by_signal"), r.get("status"))
        return True

    def reset_conditions(self):
        self.warning("resetting all conditions!")
        self.when_registered.reset()
        self.when_connected.reset()
        self.when_receiver_ports_known.reset()
        self.when_register_finished.reset()
    def fail_conditions(self, fail_message=None):
        self.warning("failing all conditions!")
        self.when_registered.trigger_fail(fail_message=fail_message)
        self.when_connected.trigger_fail(fail_message=fail_message)
        self.when_receiver_ports_known.trigger_fail(fail_message=fail_message)
        self.when_sender_ports_known.trigger_fail(fail_message=fail_message)
        self.when_register_finished.trigger_fail(fail_message=fail_message)
        
    def inform_dead(self, why=""):
        show_question = False
        if self._disconnect_reason is not None:
            why = self._disconnect_reason
            self.error("lost connection to daemon! %s" % why)
            mark_as = "daemon error..."
            show_question = True
        elif self.expect_disconnection:
            post = (": %s" % why) if why else ""
            self.info("lost connection to daemon as expected" + post)
            mark_as = "stopped"
        else:
            if (self.com is None or self.com._received_bytes == 0) and not self.manager.host.is_direct_reachable(self.host):
                self.error("this daemon uses a tcp-forward which immediately closed the connection.\nthis is a hint that the next hop after that forward was not reachable!")
                mark_as = None
            else:
                self.error("lost connection to daemon! %s" % why)
                mark_as = "daemon error..."
                show_question = True

        self._init_resource_registries()
        self.connected = False

        # do not trigger fail here.
        # we want to do .call() followed by .connect() to work!
        # do trigger_fail on other respective places!
        self.fail_conditions()
        
        self.up = False
        self.com = None
        self.fd = None

        for pname, p in self.manager.sysconf.processes.items():
            if hasattr(p, "host") and p.host and p.host == self.host:
                if p.state not in (None, "stopped"):
                    self.manager._update_process_state(p, mark_as)
                elif p.state is not None:
                    self.manager._update_process_state(p, None)

        for sname, s in self.manager.sysconf.states.items():
            if hasattr(s, "host") and s.host and s.host == self.host:
                s.request_active = False
                self.manager._update_state_state(s, self.host, None)

        first = True
        error_response = {
            "success": "error lost connection to daemon %s: %s" % (self.host, why)
        }
        for hn, hl in self.hooks.items():
            if type(hn) == tuple and hn[0] == "response" and len(hl) and hn[1] not in ("state_notification", ):
                if first:
                    self.warning(" there were still requests waiting for responses - but daemon connection is lost!")
                    first = False
                for h, hto in hl.items():
                    self.warning("  %s: by %r to %s" % (hn, h, hto))
                error_response["response"] = hn[1]
                self.call_hook(hn, error_response)

        if show_question:
            self.manager.add_gui_question("lost_daemon_connection", self.host.name, dict(hostname=self.host.name, why=why))


    def get_topic_scheduling(self, topic):
        sched_on = topic.get_scheduling_settings_for(self.host)
        if "policy" in sched_on:
            # resolve policy with known arch
            sched_on = dict(sched_on)
            sched_on["policy"] = self.get_sched_policy_int(sched_on["policy"])
            
        return sched_on

    def add_source(self, remote_port, local_port, rate, reliable_transport):
        """
        daemon.sources[remote_port] = [ local-port-rate-rel-tuple ]
        for any remote_port we use self.sources to remember finished
        transports from that port to some local port (with transport characteristics
        like rate & reliable).
        that way we can reuse already existing transports
        """
        new_source = local_port, float(rate), reliable_transport
        existing_sources = self.sources.get(remote_port)
        if existing_sources is None:
            self.sources[remote_port] = [new_source]
        else:
            existing_sources.append(new_source)

    def remove_source(self, remote_port, local_port):
        self.debug("remove_source(\n  remote: %r,\n  local : %r)" % (remote_port, local_port))
        if remote_port == local_port:
            # port itself is removed -> also delete all other uses of this port!
            src_to_remove = []
            for rport, lports in self.sources.items():
                to_del = []
                for i, (lport, rate, reliable_transport) in enumerate(lports):
                    if lport == local_port:
                        to_del.append(i)
                to_del.reverse()
                for i in to_del:
                    del lports[i]
                if not lports:
                    src_to_remove.append(rport)
            for sport in src_to_remove:
                del self.sources[sport]
        existing_sources = self.sources.get(remote_port)
        if existing_sources is None:
            self.debug("this remote_port is not listed in our sources")
            return        
        existing_sources = [src for src in existing_sources if src[0] != local_port]
        if not existing_sources:
            self.debug("there are no other local_ports for this remote_port!")
            del self.sources[remote_port]
        else:
            self.debug("there are %d other local_ports for this remote_port!" % len(existing_sources))
            self.sources[remote_port] = existing_sources

    def find_source(self, remote_port, needed_rate, needed_reliable_transport):
        # find source port on this daemon with rate >= needed_rate
        # or return None
        needed_rate = float(needed_rate)
        existing_sources = self.sources.get(remote_port)
        if existing_sources is None:
            return None
        best_error = None
        best_local_port = None
        best_local_rate = None
        full_rate = None
        full_rate_version_match = False
        integral_sources = []
        def shm_version_match(p1, p2):
            if not hasattr(p1, "shm_version"):
                return True
            if not hasattr(p2, "shm_version"):
                return True
            return p1.shm_version == p2.shm_version
        
        for local_port, local_rate, reliable_transport in existing_sources:
            self.debug("local %s rate %r reliable %r -- need rate %r, reliable %r" % (
                local_port, local_rate, reliable_transport,
                needed_rate, needed_reliable_transport))
            if reliable_transport != needed_reliable_transport:
                continue # only accept matching transports!
            
            if local_rate == needed_rate and shm_version_match(local_port, remote_port):
                self.debug("  exact match!")
                return local_port, local_rate # exact match!
            if local_rate == -1.0:
                if not full_rate_version_match:
                    full_rate_version_match = shm_version_match(local_port, remote_port)
                    full_rate = local_rate
                    full_rate_port = local_port
                continue
            if needed_rate == -1.0:
                # only accept full-rate
                continue
            if local_rate < needed_rate:
                continue
            if (int(local_rate*10) % int(needed_rate*10)) == 0:
                # take as integral multiple!
                self.debug("  take as close enough...")
                integral_sources.append((local_rate, local_port))
                continue
            this_error = local_rate / float(needed_rate)
            this_error = this_error - int(this_error)
            if best_error is None or this_error < best_error:
                best_error = this_error
                self.debug("  new best error %r" % best_error)
                best_local_port = local_port
                best_local_rate = local_rate
        if integral_sources:
            integral_sources.sort()
            return integral_sources[0][1], integral_sources[0][0]
        if best_error is None and full_rate is not None:
            self.debug("  found full-rate port!")
            best_error = 1
            best_local_port = full_rate_port
            best_local_rate = -1.0
        if best_error is None:
            return None
        return best_local_port, best_local_rate

    @create_connection
    def new_shm_port(self, cb, client, topic, rate, buffers, max_shm_version, is_reliable):
        """
        calls cb(new_port) or cb(exception=...)
        """
        is_reliable = bool(is_reliable)
        rate = float(rate)
        if max_shm_version > self.max_shm_version:
            self.debug("this client wants to do shmv%d but daemon allows only shmv%d" % (max_shm_version, self.max_shm_version))
            max_shm_version = self.max_shm_version

        if self.have_glibc_atleast_2_25 and max_shm_version < 3:
            self.error("client '%s' wants to use shmv%d but is started on a host with glibc >= 2.25 with broken pshared-mutexes! please rebuild client against newer LN-library!" % (
                client.program_name, max_shm_version))
        
        # do we already have this topic at this rate on this daemon?
        shm = topic.name, rate
        existing_port = self.shms.get(shm)

        if existing_port:
            self.debug(("have existing port\n%s\n"
                        "existing_port.rate %r == rate %r %s\n"
                        "existing_port.shm_version %r <= max_shm_version %r %s\n"
                        "existing_port.is_reliable %r == is_reliable %r %s") % (
                            existing_port,
                            existing_port.rate, rate, existing_port.rate == rate,
                            existing_port.shm_version, max_shm_version, existing_port.shm_version <= max_shm_version,
                            existing_port.is_reliable, is_reliable, existing_port.is_reliable == is_reliable))
        accept_reliable = existing_port and ((existing_port.is_reliable and existing_port.daemon == self) or existing_port.is_reliable == is_reliable)
        if existing_port and existing_port.rate == rate and existing_port.shm_version == max_shm_version and accept_reliable:
            port = existing_port
            self.debug("new_shm_port shm (%r, %r, buffers %d, shm_version %d) already avaliable on this daemon -> reuse (wanted buffers %d, shm_version %d)!" % (
                topic.name, rate, port.buffers, port.shm_version,
                buffers, max_shm_version))
            
            # reuse port, make sure that its not in the delete queue!
            self.delete_from_remove_port_queue(port)
            
            do_enlarge = False
            if port.buffers < buffers:
                self.debug("existing port has less buffers!")
                have_subscribed_ports = []
                largest_buffers = buffers
                for sub in topic.subscribers:
                    self.debug("  subscriber at rate %s, buffers: %s, %s, port: %s" % (sub.rate, sub.buffers, sub.client, sub.port))
                    if sub.port is not None and sub.port != "in progress":
                        have_subscribed_ports.append(sub)
                    if sub.buffers > largest_buffers:
                        largest_buffers = sub.buffers
                if not have_subscribed_ports:
                    self.debug("try to enlarge buffers from %s to %s" % (port.buffers, largest_buffers))
                    def finish_buffer_enlarge2(topic, new_port, org_cb):
                        self.debug("finish_buffer_enlarge2 from %r to %r" % (port, new_port))
                        # 3. delete old shm
                        self.remove_port(port, check_transport_source=False)
                        org_cb(new_port)                      
                    def finish_buffer_enlarge(new_port=None, exception=None, org_cb=None):
                        if exception is not None:
                            self.warning("failed to create enlarged shm:\n%s\nfallback to original port!" % exception)
                            return org_cb(port)
                        self.debug("finish_buffer_enlarge from %r to %r" % (port, new_port))
                        # 2. notify publisher client of change
                        topic.publisher.do_republish(
                            topic, new_port,
                            callback(finish_buffer_enlarge2, org_cb=org_cb))
                        # todo: republish takes time, wait for success response before removing port and before calling callback!                        

                    cb = callback(finish_buffer_enlarge, org_cb=cb)
                    do_enlarge = True
                    buffers = largest_buffers
                    # 1. create new shm
                else:
                    self.debug("already have subscribed ports. can not enlarge buffers...\n%r" % have_subscribed_ports)
            
            if not do_enlarge:
                return cb(port)
        
        # create new shm port!
        if rate == -1.0:
            direction = "source" # no rate limiter needed
        else:
            direction = "destination" # with rate limiter in ln_daemon

        p = shm_port(
            self, direction,
            topic.generate_source_port_name(client, self, buffers, rate, is_reliable),
            topic.message_size, topic, buffers, max_shm_version, is_reliable=is_reliable
        )
        if rate != -1.0:
            p.rate = rate
            kwargs = dict(rate=p.rate)
        else:
            p.rate = -1.0
            kwargs = dict()
            # check for scheduling settings in this topic for this host
            kwargs.update(self.get_topic_scheduling(topic))

        def _eval_answer(msg=None, exception=None):
            if exception is not None:
                msg = "failed to create new shm port: %s" % exception
                self.error(msg)
                return cb(exception=msg)
            self.ports.append(p)
            self.add_source(p, p, -1, True) # direct source!
            self.debug("remember %r as direct self source" % (p))
            self.shms[topic.name, rate] = p
            cb(p)

        try:
            if p.size <= 0:
                raise Exception("invalid element_size %r" % p.size)
            if buffers <= 0:
                raise Exception("invalid n_buffers %r" % buffers)
            if p.size * buffers > config.max_allowed_topic_shm_size:
                raise Exception("element_size %r times %r buffers is larger than max_allowed_topic_shm_size %r" % (p.size, buffers, config.max_allowed_topic_shm_size))
        except:
            msg = "shm port %r: %s" % (p.name, str(sys.exc_info()[1]))
            self.error(msg)
            return cb(exception=msg)

        if p.size * buffers > config.warn_topic_shm_size:
            self.warning("shm port %r needs large amount of memory: %.1f kB" % (p.name, p.size * buffers / 1024.))

        self.send_request(
            "new_shm_%s" % direction,
            _eval_answer,
            shm_name=p.name, 
            element_size=p.size,
            n_elements=buffers,
            shm_version=max_shm_version,
            **kwargs
        )

    @needs_connection
    def can_swap_endianess(self):
        return self.protocol_version >= 20

    @create_connection
    def new_source(self, size, source_port, cb, proto="udp", preferred_network=None, swap_endianess=None):
        """
        create a udp/tcp receiver port
        
        calls cb(new_port) or cb(exception=...)
        """
        if source_port.daemon == self:
            self.error("have udp receiver as source for local port!")
        
        self.debug("new-source (receiver) port at preferred_network: %s" % preferred_network)
        # todo: for qnx use preferred_network to decide which network stack to use!
        args = dict(
            size=size,
        )
        args.update(self.get_topic_scheduling(source_port.topic))
        
        if swap_endianess:
            if not self.can_swap_endianess():
                raise Exception("error, daemon on %s has protocol version %d which does not support endianess swapping!" % (
                    self.host, self.protocol_version))
            args["swap_endianess"] = swap_endianess
        
        if preferred_network:
            source_interface = preferred_network.get_interface_for_host(self.host)
        else:
            # prefer interface which do not require alternate_socket!
            source_interface = self.host.interfaces[0]
            for interface in self.host.interfaces[1:]:
                if interface.alternate_socket is None:
                    source_interface = interface
                    break
        
        if source_interface.alternate_socket:
            if self.protocol_version < 16:
                self.error("daemon is too old (version %d, need 16) to specify alternate interface socket!" % (self.protocol_version))
            else:
                args["alternate_socket"] = source_interface.alternate_socket
        
        def _eval_answer(msg=None, exception=None):
            if exception is not None:
                msg = "failed to create new %s source: %s" % (proto, exception)
                self.error(msg)
                return cb(exception="%s: %s" % (self.host.name, msg))
            port = msg["port"]
            self.debug("got %s receiver port %r" % (proto, port))
            if proto == "udp":
                p = udp_port(self, "source", port[1], size, source_port.topic)
                reliable_transport = False
            elif proto == "tcp":
                p = tcp_port(self, "source", port[1], size, source_port.topic)
                reliable_transport = True
            p.interface = source_interface
            p.publisher_topic = source_port.__dict__.get("publisher_topic")
            self.ports.append(p)
            self.add_source(p, p, -1, reliable_transport) # direct source!
            self.debug("remember %r as direct self source" % p)
            cb(p)
        
        self.send_request(
            "new_%s_receiver" % proto, 
            _eval_answer, 
            **args
        )

    @create_connection
    def new_destination(self, target_interface, target_port, size, rate, topic, cb, proto="udp", swap_endianess=None):
        """
        calls cb(new_port) or cb(exception=...)
        """
        target_host = target_interface.ip_address
        
        if proto == "udp":
            p = udp_port(self, "destination", (target_host, target_port), size, topic)
        elif proto == "tcp":
            p = tcp_port(self, "destination", (target_host, target_port), size, topic)
        else:
            raise Exception("unknown destination protocoll %r" % proto)
        
        p.rate = float(rate)
        
        # todo: for qnx use target_interface to decide which network stack to use!
        source_interface = target_interface.network.get_interface_for_host(self.host)
        self.debug("new-destination (sender) port at target_interface: %s from %s" % (target_interface, source_interface))
        args = dict(
            target_host=target_host,
            target_port=target_port,
            size=p.size,
            rate=p.rate
        )
        if swap_endianess:
            if not self.can_swap_endianess():
                raise Exception("error, daemon on %s has protocol version %d which does not support endianess swapping!" % (
                    self.host, self.protocol_version))
            args["swap_endianess"] = swap_endianess
        if source_interface.alternate_socket:
            if self.protocol_version < 16:
                self.error("daemon is too old (version %d, need 16) to specify alternate interface socket!" % (self.protocol_version))
            else:
                args["alternate_socket"] = source_interface.alternate_socket
        
        def _eval_answer(msg=None, exception=None):
            if exception is not None:
                return cb(exception=exception)
            self.debug("got %s sender port %r" % (proto, msg["port"], ))
            self.ports.append(p)
            # no source, because we don't know who will send to this udp receiver!
            return cb(p)
        
        self.send_request("new_%s_sender" % proto, _eval_answer, **args)

    @create_connection
    def transport_port(self, source_port, target_rate, cb, reliable_transport, route=None):
        """
        source_port is on other daemon. 
        create network bridge to get local-source for this remote source_port!

        decide route once and then pass to further connect_ports() calls.
        
        calls cb() or cb(exception=...)
        """
        self.info("create transport for port %r at rate %r, reliable %r" % (source_port, target_rate, reliable_transport))
        # create a udp or tcp bridge from source-daemon to local daemon
        source = source_port, target_rate, reliable_transport
        if source not in self._transportations_in_work:
            self._transportations_in_work[source] = []
        self._transportations_in_work[source].append(cb)
        if len(self._transportations_in_work[source]) > 1:
            # we are not the 1st. do nothing, first one will call us!
            self.info("transport creation for %s already in progress... wait..." % source_port)
            return

        # -> create local udp/tcp receiver
        #    -> create udp/tcp sender
        #       -> link source to udp/tcp sender
        #          -> register udp receiver as local source. call all self._transportations_in_work[source]

        if reliable_transport:
            self.info("reliable transport requested...")
            proto = "tcp"
        else:
            proto = "udp"

        def notify_cbs(*args, **kwargs):
            # now call all self._transportations_in_work[source]
            for user_cb in self._transportations_in_work[source]:
                user_cb(*args, **kwargs)
            del self._transportations_in_work[source]
            self.debug("finished transport creation of port %s" % source_port)
            
        # decide route
        # connect all daemons on route
        # create all needed network bridges

        topic = source_port.topic
        if topic is None:
            self.error("topic of source_port %r is None!" % source_port)
        if route is None:
            route = self.host.search_route_to(source_port.daemon.host, for_topic=topic)
            
            if len(route) == 1:
                msg = "transport_port: source port %s is on localhost!?" % source_port
                self.error(msg)
                return notify_cbs(exception=msg)
            
        our_interface = route.pop(0)
        next_interface = route[0]
            
        # first entry is leaving interface
        # 2nd and following are intermediate interfaces
        # last ist source_daemon interface

        # create reciever on this, and sender on next hop, then next_hop.connect to source port!
        def _have_next_daemon(daemon):
            next_daemon = daemon
            if not next_daemon.is_connected():
                msg = "failed to connect to daemon on %s: %s" % (next_daemon.host, next_daemon.when_register_finished.fail_message)
                self.error(msg)
                return notify_cbs(exception=msg)
            swap_endianess = topic.decide_endianess_swap(self, next_daemon)
            
            def _create_receiver_answer(receiver_port=None, exception=None):
                if exception is not None:
                    return notify_cbs(exception=exception)
                swap_endianess = topic.decide_endianess_swap(next_daemon, self)
                
                def _create_sender_answer(sender_port=None, exception=None):
                    if exception is not None:
                        return notify_cbs(exception=exception)
                    receiver_port.sender = sender_port
                    sender_port.receiver = receiver_port

                    def _connect_answer(exception=None):
                        if exception is not None:
                            return notify_cbs(exception=exception)
                        self.add_source(source_port, receiver_port, target_rate, reliable_transport) # obvious, valid use-case
                        notify_cbs()
                    next_daemon.connect_ports(sender_port, source_port, _connect_answer, reliable_transport, route=route)
                    
                next_daemon.new_destination(
                    our_interface, receiver_port.port, source_port.size, target_rate, topic,
                    _create_sender_answer, 
                    proto=proto, 
                    swap_endianess=swap_endianess
                )
                
            self.new_source(
                source_port.size, source_port,
                _create_receiver_answer, 
                proto=proto, preferred_network=our_interface.network,
                swap_endianess=swap_endianess
            )
            
        self.manager.with_daemon_for_host(
            next_interface.host, "when_register_finished",
            _have_next_daemon
        )            
        return
    
    @create_connection
    def connect_ports(self, destination_port, source_port, cb, reliable_transport, route=None):
        """
        calls cb() or cb(exception=...)

        destination_port is on this daemon.

        checks whether the given source port is already on this daemon
        (with usable rate abd transport)
        if not, call transport_port to get it here!

        then tell daemon to connect local-source to destination_port
        """                                     
        self.debug("connect from port %r to port %r, needed_rate: %r, reliable_transport: %s" % (
            source_port, destination_port, destination_port.rate, reliable_transport))
        
        if destination_port.daemon != self:
            raise Exception("connect_ports() called with invalid destination_port that is not in this daemon!")
        
        if source_port.daemon == self:
            if source_port not in self.sources:
                raise Exception("source_port %r is not in list of sources for this daemon!" % source_port)
            local_source_port, local_rate = source_port, -1.0
        else:
            if not reliable_transport and source_port.size > 64 * 1024 - 64:
                self.info("source port is bigger than ~64kB (%d bytes) -> enforce reliable transport!" % (source_port.size, ))
                reliable_transport = True
            
            # use forwarded port
            local_source_port_rate = self.find_source(source_port, destination_port.rate, reliable_transport)
            if local_source_port_rate is None:
                # this source is not yet forwarded to us with sufficient rate!
                self.debug("source %r is not yet avaliable on this daemon" % source_port)

                def again(exception=None):
                    if exception is not None:
                        self.error("could not connect destination port %s on %s with source port %s from %s because port-transportation failed: %s" % (
                                destination_port, self.host, source_port, source_port.daemon.host, exception))
                        return cb(exception=exception)
                    # now do the local connection!
                    return self.connect_ports(destination_port, source_port, cb, reliable_transport)

                self.transport_port(
                    source_port, destination_port.rate,
                    again,
                    reliable_transport, route=route)
                return # delayed until port is transported....
            local_source_port, local_rate = local_source_port_rate
        
        if local_source_port.port_type == "shm" and destination_port.port_type == "shm" and local_source_port.name == destination_port.name:
            self.warning("source and target ports are the same shm! (are object equal? %s)" % (local_source_port == destination_port))
            return cb()
        
        self.debug("source %r is already available on this daemon at %r with rate %r" % (source_port, local_source_port, local_rate))
        
        if destination_port in local_source_port.destinations:
            self.debug("local_source_port %r already connected to destination_port %r" % (
                    local_source_port, destination_port))
            return cb()
        self.debug("add destination %s to source port %s" % (destination_port.get_id(), local_source_port.get_id()))
        local_source_port.destinations.append(destination_port)
        
        def _eval_answer(msg=None, exception=None):
            if exception is not None:
                local_source_port.destinations.remove(destination_port)
                msg = "could not connect local_source_port %r with destination %r: %s" % (
                        local_source_port, destination_port, exception)
                self.error(msg)
                return cb(exception=msg)
            destination_port.publisher_topic = local_source_port.__dict__.get("publisher_topic")
            destination_port.source = local_source_port
            cb()
        
        self.send_request(
            "connect",
            _eval_answer,
            source=local_source_port.get_id(), 
            target=destination_port.get_id()
        )
        
    def check_remove_transport_source_port(self, p, delete_port=True):
        self.debug("check_remove_transport_source_port(%s, delete_port=%s)" % (p, delete_port))
        if p.direction != "source":
            raise Exception("this can not be a transport-source-port because its not a source port: %s" % (p, ))

        if p.port_type not in ("udp", "tcp"):
            return
        if p.destinations:
            self.debug("still listeners left: %s" % (p.destinations, ))
            return
        if hasattr(p, "sender"):
            transport_source = p.sender.source
            self.debug("have sender/transport_source: %s" % transport_source)
            if transport_source in self.sources:
                self.remove_source(transport_source, p)
            else:
                pass # this source is just an intermediate transport source from a different host!
            self.debug("removing transport source port %s from transport source %s" % (p, transport_source))
            p.sender.daemon.remove_port(p.sender, check_transport_destination=False)
        if delete_port:
            self.remove_port(p, check_transport_source=False)
        
    def check_remove_transport_destination_port(self, p):
        self.debug("check_remove_transport_destination_port(%s)" % (p))
        if p.direction != "destination" and p.port_type != "shm":
            raise Exception("this can not be a transport-destination-port because its not a destination port: %s" % (p, ))

        if p.port_type not in ("udp", "tcp"):
            self.debug("not a udp or tcp port!")
            return False
        
        self.remove_source(p.source, p)
        self.debug("removing transport destination port %s from transport source %s" % (p, p.source))

        self.debug("removing sender %s of transport destination port %s" % (p, p.source))
        p.source = None
        self.remove_port(p, check_transport_destination=False)
        if hasattr(p, "receiver") and p.receiver:
            self.debug("removing receiver %s of transport destination port %s" % (p.receiver, p.source))
            p.receiver.daemon.remove_port(p.receiver, check_transport_source=False)
        return True

    def queue_remove_port(self, p, timeout=0.5):
        if self.ignore_port_removes:
            self.debug("ignore queue to remove port %s" % p)
            return
        self.debug("queue to remove port %s in %.2fs" % (p, timeout))
        deadline = time.time() + timeout
        self.remove_port_queue = [t for t in self.remove_port_queue if t[1] != p]
        self.remove_port_queue.append((deadline, p))
        if self.next_remove_port_queue_worker is None or self.next_remove_port_queue_worker > deadline:
            self.next_remove_port_queue_worker = deadline
            if hasattr(self, "next_remove_port_queue_worker_id") and self.next_remove_port_queue_worker_id is not None:
                GLib.source_remove(self.next_remove_port_queue_worker_id)
                self.next_remove_port_queue_worker_id = None
            self.next_remove_port_queue_worker_id = GLib.timeout_add(int(timeout * 1000), self._remove_port_queue_worker)

    def delete_from_remove_port_queue(self, p):
        self.remove_port_queue = [t for t in self.remove_port_queue if t[1] != p]
        if not self.remove_port_queue:
            if hasattr(self, "next_remove_port_queue_worker_id") and self.next_remove_port_queue_worker_id is not None:
                GLib.source_remove(self.next_remove_port_queue_worker_id)
                self.next_remove_port_queue_worker_id = None
    
    def _remove_port_queue_worker(self):
        to_del = []
        now = time.time()
        next = None
        for i, (deadline, port) in enumerate(self.remove_port_queue):
            if deadline < now:
                to_del.append(i)
                continue
            if next is None or deadline < next:
                next = deadline
        to_del.reverse()
        for i in to_del:
            deadline, port = self.remove_port_queue[i]
            del self.remove_port_queue[i]
            self.remove_port(port)
        
        self.next_remove_port_queue_worker = next
        if next is not None:
            self.next_remove_port_queue_worker_id = GLib.timeout_add(int((next - now) * 1000), self._remove_port_queue_worker)
        else:
            self.next_remove_port_queue_worker_id = None
        return False

    def remove_port(self, p, check_transport_source=True, check_transport_destination=True, as_subscriber=False):
        self.debug("remove_port(\n"
                   "  %s,\n"
                   "  check_transport_source=%s,\n"
                   "  check_transport_destination=%s,\n"
                   "  as_subscriber=%s)" % (
                       p, check_transport_source, check_transport_destination, as_subscriber))
        
        if p.is_client_port:
            self.debug("it's a client_port!")
            # assume removed!
            if p in self.sources and not as_subscriber:
                self.debug("it's in our sources and as_subscriber is False -> remove_source(p, p)!")
                self.remove_source(p, p)
            return
        
        # dict of [topic, rate] of local availiable shm ports
        while True:
            for topic_rate, port in self.shms.items():
                if port == p:
                    self.debug("found port in shms for topic %r and rate %r" % (topic_rate[0], topic_rate[1]))
                    del self.shms[topic_rate]
                    break
            else:
                break
        
        still_exists = p in self.ports
        did_remove = False
        if p.direction == "source":
            to_check = p.destinations
            all_ports = [ip.get_id() for ip in to_check]
            unique_ports = set(all_ports)
            if len(all_ports) != len(unique_ports):
                self.error("there are ports that appear multiple times as destination for port %s:\n%s" % (
                    p.get_id(),
                    "\n".join(["%d: %s" % (i, ip.get_id()) for i, ip in enumerate(to_check)]))
                )
            p.destinations = []
            for destination in to_check:
                if still_exists:
                    self.send_request("disconnect", ignore_cb, source=p.get_id(), target=destination.get_id())
                # modify list while iterating? p.destinations.remove(destination)
                if check_transport_destination:
                    self.check_remove_transport_destination_port(destination)
                destination.source = None            
            if check_transport_source:
                self.check_remove_transport_source_port(p, delete_port=False)
        if (p.direction == "destination" or p.port_type == "shm") and p.source:
            self.debug("port has source port %r, disconnect!" % p.source)
            if still_exists:
                self.send_request("disconnect", ignore_cb, source=p.source.get_id(), target=p.get_id())
            p.source.destinations.remove(p)
            if check_transport_source:
                self.check_remove_transport_source_port(p.source, delete_port=True)
            if check_transport_destination:
                did_remove = self.check_remove_transport_destination_port(p) # will call remove_port on p
                #did_remove = True
            p.source = None

        if self.up:
            if not did_remove and p in self.ports:
                self.ports.remove(p)
                did_remove = True
                self.send_request("delete_port", ignore_cb, port=p.get_id())
                p.was_deleted = True
        elif still_exists:
            self.warning("ignoring delete_port because daemon %#x is down!" % id(self))
        
        self.remove_source(p, p)

    def _stop(self):
        if self.com is None:
            raise Exception("this DaemonStore instance has no connection!")        
        self.com.send(method="stop")
    def _hook_idle_cb(self):
        if len(self._hook_queue) == 0:
            if self.hook_idle is not None:
                GLib.source_remove(self.hook_idle)
            self.hook_idle = None
            return False
        cb, args = self._hook_queue.pop(0)
        try:
            cb(*args)
        except Exception:
            self.error("error while calling request-cb %r(%r):\n%s" % (
                cb, args,
                traceback.format_exc()))
        if hasattr(cb, "creation_stack"):
            del cb.creation_stack
        if hasattr(cb, "fcn"):
            del cb.fcn
        return True
        
    def _queue_call(self, cb, *args):
        self._hook_queue.append((cb, args))
        if not hasattr(self, "_hook_idle") or self._hook_idle is None:
            self.hook_idle = GLib.idle_add(self._hook_idle_cb)
        return True

    def got_request(self, msg):
        #self.info("got request from daemon:\n%s" % (msg))
        request = msg.get("request")
        if request is not None:
            #had_hook = self._queue_call_hook(("request", msg["request"]), msg)
            method_name = "daemon_%s" % request
            if not hasattr(self, method_name):
                self.error("received unknown daemon-request: %r\n%r" % (msg["request"], pprint.pformat(msg)))
                had_hook = False
            else:
                method = getattr(self, method_name)
                ## first work on receive queue!
                #while len(self._hook_queue):
                #    self._hook_idle_cb()
                if self.registered and request != "disconnect_reason":
                    self._queue_call(callback(method, **msg))
                else:
                    # do synchronous!
                    method(**msg)
                #try:
                #    method(**msg)
                #except Exception:
                #    self.error("error executing request-from-daemon handler %r for request:\n%s\n%s" % (
                #        method, pprint.pformat(msg), traceback.format_exc()))
                had_hook = True
        elif "response" in msg:
            resp = msg["response"]
            request_id = msg.get("request_id")
            if request_id is not None:
                cb = self.response_listeners.get(request_id)
                if cb is not None:
                    del self.response_listeners[request_id]
                    #self.debug("queued cb call %r for %r" % (cb, msg))
                    self._queue_call(cb, msg)
                    #self._hook_idle_cb() # call now directly!
                    had_hook = True
                else:
                    had_hook = False                
            else:
                where = ("response", resp)
                if where in self.hooks and len(self.hooks[where]):
                    had_hook = True
                    self._queue_call(callback(self.call_hook, where, msg))
                else:
                    had_hook = False
        else:
            self.error("received unknown message type:\n%s" % pprint.pformat(msg))
            return
        if had_hook:
            return
        self.warning("received request and no-one had registered to it!\n%s" % pprint.pformat(msg))
        if "response" in msg and msg.get("success") == "success":
            # silently ignore success message
            #self.warning("received successful response - but noone cared!\n%s" % pprint.pformat(msg))
            return

    def _send(self, request, **data):
        if self.com is None:
            raise Exception("this DaemonStore instance has no connection!")
        data["request_id"] = self.request_counter
        self.request_counter += 1

        try:
            self.com.send(request=request, **data)
        except Exception:
            # todo: try to reconnect and try to send again?!
            self.error("could not send request to daemon %s:\n%s" % (self.host, traceback.format_exc()))
            raise
        return data["request_id"]
    
    def send(self, request, **data):
        if not self.is_connected():
            raise Exception("daemon is not connected -- request:\n%s" % pprint.pformat(data))
        return self._send(request=request, **data)

    def call_on_response(self, request_id, cb, *cbargs, **cbkwargs):
        if isinstance(cb, callback) and not cbargs and not cbkwargs:
            self.response_listeners[request_id] = cb
        else:
            self.response_listeners[request_id] = callback(cb, *cbargs, **cbkwargs)

    def _send_request(self, request, cb, **kwargs):
        """
        calls cb(response) or cb(exception=...)
        """
        if "blocking" in kwargs:
            raise Exception("can not send blocking request anymore!")
        
        if cb is None:
            self.error("cb argument should not be None - stack:\n%s" % "".join(traceback.format_stack()))

        def _on_response(msg):
            if msg["success"].startswith("error"):
                error = msg["success"].split(" ", 1)[1]
                self.error("error response from %s request: %s" % (request, error))
                return cb(exception=error)
            cb(msg)
        
        request_id = self._send(request=request, **kwargs)
        self.call_on_response(request_id, _on_response)
    
    @create_connection("when_connected")
    def send_request(self, request, cb, **kwargs):
        """
        calls cb(response) or cb(exception=...)
        """
        return self._send_request(request, cb, **kwargs)

    # process management
    def _get_cmdline(self, cmdline):
        if self.registered and self.is_windows:
            if cmdline.startswith("/home/"):
                args = cmdline.split(" ") # todo: hack! what if spaces in program name!
                first = args[0]
                first = first[len("/home/"):].replace("/", "\\")
                if "." not in first:
                    first += ".exe"
                cmdline = "z:\\%s %s" % (first, " ".join(args[1:]))
        return cmdline


    def get_xcookie(self):
        if self.xcookie is not None:
            return self.xcookie
        self.xcookie = get_xcookie()
        return self.xcookie

    def get_xport(self, display=None):
        if display is None:
            display = os.getenv("DISPLAY")
            if display is None:
                raise Exception("can not get X port when there is no DISPLAY env-variable!")
        dpy = display.split(":", 1)[1]
        if "." in dpy:
            dpy = dpy.split(".", 1)[0]
        return 6000 + int(dpy)
    
    def get_xhost(self, display=None):
        if display is None:
            display = os.getenv("DISPLAY")
            if display is None:
                raise Exception("can not get X host when there is no DISPLAY env-variable!")
        dpy = display.split(":", 1)[0]
        if not dpy.strip():
            return "localhost"
        return dpy
    
    def rtfcn_own_ip_in_network(self, network):
        net_names = []
        for net in self.host.get_networks():
            if net.name == network:
                break
            net_names.append(net.name)
        else:
            raise Exception("our host %r has no interface in network %r! networks: %s" % (self.host, network, ", ".join(net_names)))
        return net.get_ip_for_host(self.host)
    
    def rtfcn_own_ip_for(self, node_or_host):
        other_host = self.manager.sysconf.get_host_for_node(node_or_host, create=False)
        return other_host.get_ip_from(self.host)
    
    def rtfcn_get_ip_from_to(self, node_or_host1, node_or_host2):
        from_host = self.manager.sysconf.get_host_for_node(node_or_host1, create=False)
        to_host   = self.manager.sysconf.get_host_for_node(node_or_host2, create=False)
        return from_host.get_ip_from(to_host)
    
    def replace_runtime_vars_for_obj(self, obj, data):
        data = self.runtime_function_parser.replace(data, self.error)
        
        if "$(ARCH)" in data:
            arch = self.architecture
            if hasattr(obj, "arch") and obj.arch is not None:
                arch = obj.arch
            data = data.replace("$(ARCH)", arch)

        def env_from_gui_or_local(data, envname, default=None):
            what = "$(gui_%s)" % envname
            if what in data:
                pctx = obj.requested_state_ctx() if hasattr(obj, "requested_state_ctx") and obj.requested_state_ctx is not None else None
                if pctx is None:
                    value = os.getenv(envname, default)
                else:
                    value = getattr(pctx, envname)
                if value is None:
                    raise Exception("gui-context variable %r is not avaliable!" % what)
                data = data.replace(what, str(value))
            return data

        data = env_from_gui_or_local(data, "DISPLAY")
        data = env_from_gui_or_local(data, "XAUTHORITY", default=get_xauthority())
        what = "$(gui_ip)"
        if what in data:
            pctx = obj.requested_state_ctx() if hasattr(obj, "requested_state_ctx") and obj.requested_state_ctx is not None else None
            if pctx is None:
                value = "127.0.0.1"
            else:
                value = pctx.address[0]
            if value is None:
                raise Exception("gui-context variable %r is not avaliable!" % what)
            data = data.replace(what, str(value))
        data = data.replace("$(manager_host)", str(self.manager.sysconf.instance_config.manager_host))
        data = data.replace("$(manager_port)", str(self.manager.sysconf.instance_config.manager_port))
        for what in "uid,gid,USER_env,HOME_env,USER,HOME,SHELL,user_real_name".split(","):
            search = "$(%s)" % what
            if search in data:
                value = getattr(self, what)
                if value is None:
                    raise Exception("runtime variable %r is not avaliable from this daemon!" % what)
                data = data.replace(search, str(value))
        return data

    def _fixup_ln_manager_runtime_var(self, ctx, ln_manager_value):
        search = "$(LN_MANAGER)"
        ctx.cmdline = ctx.cmdline.replace(search, ln_manager_value)
        for i, (key, value) in enumerate(ctx.environment):
            if search in value:
                ctx.environment[i] = key, value.replace(search, ln_manager_value)

    def get_xauthority_fn(self, fn):
        return "/tmp/xauth_%s_%s" % (
            self.manager.sysconf.instance_config.name.replace("/", " "),
            fn
        )

    def _get_x11_forward_name(self):
        return "x11fw to %s %s" % (self.manager.host.name, os.getenv("DISPLAY"))

    def get_sched_policy_int(self, policy):
        if isinstance(policy, int):
            return policy
        
        if policy.isdigit():
            return int(policy)
        
        if policy.lower().startswith("sched_"):
            policy = policy.split("_", 1)[1]
        
        if not self.registered or self.is_linux: # assume linux if no connection yet
            from . import linux_info
            info = linux_info
        elif self.registered and self.is_qnx:
            from . import qnx_info
            info = qnx_info
        elif self.registered and self.is_vxworks:
            from . import vxworks_info
            info = vxworks_info
        else:
            raise Exception("don't have a map of possible policies for arch %r can't interpret policy %r" % (self.architecture, policy))
        
        def norm(pn):
            pnn = pn.lower()
            if pnn.startswith("sched_"):
                pnn = pnn.split("_", 1)[1]
            return pnn
        
        for pv, pn in info.policies.items():
            if norm(pn) == norm(policy):
                break
        else:
            raise Exception("unknown process policy: %r select one from these: %s" % (policy, list(info.policies.values())))
        
        return pv

    @needs_finish_callback
    def _get_manager_address(self, cb, dump_only=False):
        """
        return manager address for clients running on this daemons host

        might need a reverse tunnel
        """
        mgr_port = self.manager.sysconf.instance_config.manager_port

        if self.manager.host.is_direct_reachable(self.host):
            return cb((self.host.get_ip_from(self.manager.host), mgr_port))

        # not directly reachable...
        if self.is_connected():
            if self.manager_reverse is not None and self.manager_reverse_port == mgr_port:
                return cb(self.manager_reverse)
            
            if dump_only:
                return cb(("via_tunnel", "tunnel_port"))
            
            # change manager port!
            def _have_forward(exception=None):
                if exception is not None:
                    return cb(exception=exception)
                return cb(self.manager_reverse)
            return self._establish_manager_reverse_forward(_have_forward)
        
        # not yet connected...
        if dump_only:
            return cb(("via_tunnel", "tunnel_port"))

        def _connect_daemon_answer(daemon):
            if not self.is_connected():
                return cb(exception="failed to connect this daemon!")            
            return self._get_manager_address(cb)
        
        # connect now!
        self.manager.with_daemon_for_host(
            self.host, "when_register_finished",
            _connect_daemon_answer
        )

    class startup_ctx(object):
        pass
    
    @needs_finish_callback
    def start_process(self, cb, p, dump_only=False):
        """
        calls cb() or cb(exception=...) on error
        """
        ctx = DaemonStore.startup_ctx()
        ctx.cb = cb
        ctx.p = p
        ctx.dump_only = dump_only
        p.last_incomplete_line = b""
        
        ctx.replace_runtime_vars = lambda data: self.replace_runtime_vars_for_obj(ctx.p, data)

        if dump_only:
            self.debug("dump startup commands for process %r" % (p.name, ))
            ctx.dump = []
        else:
            self.debug("start process %r" % (p.name, ))

        if self.registered and self.is_vxworks and not p.no_pty:
            self.warning("vxworks does not support pseudo-terminals. forcing 'no_pty' flag!")
            p.no_pty = True

        if p.command_override:
            cmdline = ctx.replace_runtime_vars(p.command_override)
        else:
            cmdline = ctx.replace_runtime_vars(p.exec_command)
        ctx.cmdline = self._get_cmdline(cmdline)
        
        if ctx.cmdline.startswith("ln_notebook"):
            ctx.cmdline = os.path.join(config.manager_base_dir, ctx.cmdline)
        
        for entry in self.manager.sysconf.start_tools:
            if type(entry) != tuple:
                key, value = entry, entry
            else:
                key, value = entry
            if key != p.start_tool:
                continue
            if value is None:
                break
            p.no_pty = False
            if "qnx6.5" in self.architecture:
                value = value.replace("/usr/bin/gdb", "/usr/qnx650/host/qnx6/x86/usr/bin/gdb")
            magic = "$(CMD)"
            if magic in value:
                value = value.replace(magic, ctx.cmdline)
            else:
                ctx.cmdline = "%s %s" % (value, ctx.cmdline)
            self.debug("using start tool %s - resulting command line:\n%s" % (p.start_tool, ctx.cmdline))
            break
        
        if p.change_directory is None:
            ctx.change_directory = ""
        else:
            ctx.change_directory = ctx.replace_runtime_vars(p.change_directory)

        if p.priority is None:
            ctx.priority = 0
        else:
            ctx.priority = p.priority
            
        if p.policy is None:
            policy = -1
        else:
            policy = p.policy
        ctx.policy = self.get_sched_policy_int(policy)

        ctx.environment = list(p.environment)
        if p.ln_debug:
            ctx.environment.append(("LN_DEBUG", "1"))

        ctx.have_ln_manager = False
        ctx.have_ln_message_definition_dirs = False
        ctx.have_term = False
        ctx.have_program_name = False
        ctx.have_home = False
        for i, (key, value) in enumerate(ctx.environment):
            if key == "TERM":
                ctx.have_term = True
            if key == "LN_MANAGER":
                ctx.have_ln_manager = True
                ctx.have_ln_manager_value = value
            if key == "LN_MESSAGE_DEFINITION_DIRS":
                ctx.have_ln_message_definition_dirs = i
            if key == "LN_PROGRAM_NAME":
                ctx.have_program_name = True
            if key == "HOME":
                ctx.have_home = True
            ctx.environment[i] = ctx.replace_runtime_vars(key), ctx.replace_runtime_vars(value)

        if not ctx.have_program_name:
            # add name of program!
            display_name = p.display_name or p.name
            ctx.environment.append(("LN_PROGRAM_NAME", display_name))
            
        md_dirs = ":".join(links_and_nodes_base.message_definition_dirs)
        if ctx.have_ln_message_definition_dirs is False:
            ctx.environment.append(("LN_MESSAGE_DEFINITION_DIRS", md_dirs))
        else:
            i = ctx.have_ln_message_definition_dirs
            ctx.environment[i] = (ctx.environment[i][0], ctx.environment[i][1] + ":" + md_dirs)

        for what in "topics", "services":
            val = getattr(p, "map_" + what)
            if val:
                ctx.environment.append(("LN_MAP_%s" % what.upper(), "|".join("%s|%s" % m for m in val))) # checked

        self._start_process_bh1(ctx)
        
    def _start_process_bh1(self, ctx):
        """
        check whether we need to create x-forwarding to current gui-context
        """
        ctx.forward_x11_to_manager = False        
        if not ctx.p.forward_x11_to_gui:
            return self._start_process_bh2(ctx) # -> nope, proceed to check for x-forwarding to manager

        # we need to forward x11 connection to current gui! (which might be manager or remote-gui)

        pctx = ctx.p.requested_state_ctx() if hasattr(ctx.p, "requested_state_ctx") and ctx.p.requested_state_ctx is not None else None
        if pctx is None:
            ctx.display_host = self.manager.host
            ctx.forward_x11_to_manager = True
            self.debug("forward_x11_to_gui: gui context is manager")
            return self._start_process_bh2(ctx) # -> gui is manager, proceed to establish x-forwarding to manager
        
        gui_ip = pctx.address[0]
        ctx.display_host = self.manager.sysconf.get_host(gui_ip, create=True)
        if ctx.display_host == self.host:
            # local display
            self.debug("forward_x11_to_gui: gui context is at %s which is local to this process" % ctx.display_host)
            ctx.environment.append(("DISPLAY", ctx.replace_runtime_vars("$(gui_DISPLAY)")))
            ctx.environment.append(("XAUTHORITY", ctx.replace_runtime_vars("$(gui_XAUTHORITY)")))
            return self._start_process_bh3(ctx) # -> gui runs on process' host, provide same DISPLAY/XAUTH info as gui has!

        # we need to create a forward to gui's DISPLAY (which is not the manager's display and not on process'-host
        # do forward display to display_host
        # remote host!
        self.debug("forward_x11_to_gui: gui context is at %s - %s which is REMOTE to this process" % (
            ctx.display_host, ctx.p.requested_state_ctx().DISPLAY))
        self.debug("this process requests x11-forwarding to gui_host %s and is not on the manager host: %s" % (
            ctx.display_host,
            self.manager.host))
        # get free display number on remote side / 
        # establish tcp forwarding from remote side display to local display!
        # what is the local x-port?
        xport = self.get_xport(display=ctx.p.requested_state_ctx().DISPLAY)
        xhost = self.get_xhost(display=ctx.p.requested_state_ctx().DISPLAY)
        if xhost in ("localhost", "127.0.0.1") or ctx.display_host.has_alias(xhost):
            self.debug("forward_x11_to_gui: gui display's host %r is at gui host" % xhost)
            xhost = ctx.display_host
        else:
            self.debug("forward_x11_to_gui: gui display's host %r is not gui host!" % xhost)
            xhost = self.manager.sysconf.get_host(xhost, create=True)
        
        ctx.forward_name = "x11fw to %s %s %s" % (xhost.name, ctx.p.requested_state_ctx().DISPLAY, xport)
        if self.host.is_direct_reachable(xhost):
            if self.manager.sysconf.instance_config.allow_x11_tcp_connect:
                self.debug("forward_x11_to_gui: gui display's host %r is direct reachable!" % xhost)
                ctx.entry_display = "%s:%s" % (self.host.get_ip_from(xhost), xport - 6000)
                return self._start_process_bh11(ctx)
            reason = "tcp connects to xserver are not allowed"
        else:
            reason = "gui display's host %r is NOT direct reachable" % xhost

        # here we might be able to use the DISPLAY_UNIX_SOCKET of the gui!
        if xhost == ctx.display_host and ctx.p.requested_state_ctx().DISPLAY_UNIX_SOCKET is not None:
            xport = "unix:%s" % ctx.p.requested_state_ctx().DISPLAY_UNIX_SOCKET
            self.debug("forward_x11_to_gui: gui display can be reached via %r" % xport)
            
        def _x11_forward_finished(entry=None, exception=None):
            if exception is not None:
                self.error("failed to establish x11 forwarding: %s" % exception)
            else:
                self.debug("x11 forward entry: %r" % (entry, ))
                entry_ip, entry_port = entry
                ctx.entry_display = ":%s" % (entry_port - 6000)
            return self._start_process_bh11(ctx)
            
        self.debug("forward_x11_to_gui: %s - create forwards!" % reason)
        self.establish_tcp_forward(
            _x11_forward_finished,
            self.host,
            xhost, xport,
            keep_listening=True,
            name=ctx.forward_name,
            from_port=-2,
            terminate_at_loopback=True
        )
    
    def _start_process_bh11(self, ctx):
        """
        finish x-forwarding to gui-display by adding x-cookie to process's xauth-db
        """
        
        # get cookie: xauth list $DISPLAY
        cookie = ctx.p.requested_state_ctx().xcookie
        XAUTHORITY = self.get_xauthority_fn(ctx.forward_name)
        # add cookie on remote side: xauth add :9 . 0bc352230de7f83ffc4ca7dfd5db7b53

        def _on_answer(msg=None, exception=None):
            if exception is not None:
                self.error("failed to xauth cookie to remote side: %s" % exception)
            
            if ctx.entry_display.startswith(":"):
                ctx.entry_display = "localhost%s" % (ctx.entry_display)

            ctx.environment.append(("DISPLAY", ctx.entry_display))
            ctx.environment.append(("XAUTHORITY", XAUTHORITY))

            self._start_process_bh3(ctx) # no need to check for x-forwarding to manager, so skip _bh2()
        
        self.send_request(
            "add_xauth",
            _on_answer,
            DISPLAY=ctx.entry_display,
            XAUTHORITY=XAUTHORITY,
            key_type=cookie[0],
            key_hex=cookie[1]
        )

    @report_exception_in_cb
    def _establish_xforwarding(self, cb):
        """
        fill self.xforwarding because of p.forward_x11 or ctx.forward_x11_to_manager

        this will try to make a conneciton to the unix domain socket of manager's xserver.

        calls cb(exception=...) on error or cb(self.xforwarding)
        """
        if self.xforwarding is not None:
            return cb(self.xforwarding)

        manager_display = os.getenv("DISPLAY")
        
        if self.host == self.manager.host:
            # local host!
            entry_display = manager_display
            XAUTHORITY = get_xauthority()
            DISPLAY_HOME = os.getenv("HOME")
            if not os.path.isfile(XAUTHORITY):
                self.warning("XAUTHORITY file %r does not exist!?" % XAUTHORITY)
            self.xforwarding = entry_display, XAUTHORITY, DISPLAY_HOME        
            return cb(self.xforwarding)
        
        #if self.host != self.manager.host:
        # remote host!
        self.debug("this process requests x11-forwarding to manager x-server and is not on the manager host:\n%s\n%s" % (
            self.host, self.manager.host))
        # get free display number on remote side / 
        # establish tcp forwarding from remote side display to local display!
        # what is the local x-port?
        xport = self.get_xport(manager_display) # this assumes that managers display is "localhost:XY"
        tcp_hint = manager_display.startswith("localhost:") # when ssh forwarded...
        x_unix_socket = check_for_display_unix_socket(manager_display) # this assumes that managers display is ":XY"
        xhost = self.manager.host
        forward_name = self._get_x11_forward_name()

        def _establish_xforwarding_step2(daemon):
            manager_daemon = daemon
            if not manager_daemon.is_connected():
                msg = "could not establish x11 forwarding step2: could not connect daemon on manager host!"
                self.error(msg)
                return cb(exception=msg)
            manager_daemon_supports_forwards_to_unix_sockets = manager_daemon.protocol_version >= 26

            def _establish_xforwarding_step3(entry=None, exception=None):
                if exception is not None:
                    msg = "could not establish x11 forwarding step3: %s" % exception
                    self.error(msg)
                    return cb(exception=msg)
                
                self.debug("x11 forward entry: %r" % (entry, ))
                entry_ip, entry_port = entry
                entry_display = ":%s" % (entry_port - 6000)

                # get cookie: xauth list $DISPLAY
                cookie = self.get_xcookie()            

                XAUTHORITY = self.get_xauthority_fn(forward_name)
                # add cookie on remote side: xauth add :9 . 0bc352230de7f83ffc4ca7dfd5db7b53
                def _on_answer(msg=None, exception=None):
                    if exception is not None:
                        msg = "failed to add xauth cookie: %s" % exception
                        self.error(msg)
                        return cb(exception=msg)
                    DISPLAY_HOME = None # can not provide...
                    if entry_display.startswith(":"):
                        self.xforwarding = "localhost%s" % (entry_display), XAUTHORITY, DISPLAY_HOME
                    else:
                        self.xforwarding = entry_display, XAUTHORITY, DISPLAY_HOME
                    return cb(self.xforwarding)
                
                self.send_request(
                    "add_xauth",
                    _on_answer,
                    DISPLAY=entry_display,
                    XAUTHORITY=XAUTHORITY,
                    key_type=cookie[0],
                    key_hex=cookie[1]
                )
            
            if ((not manager_daemon_supports_forwards_to_unix_sockets or self.manager.sysconf.instance_config.allow_x11_tcp_connect)
                and self.host.is_direct_reachable(xhost)):

                self.debug("forward_x11: gui display's host %r is direct reachable!" % xhost)
                return _establish_xforwarding_step2(self.host.get_ip_from(xhost), xport)
                
            if not self.manager.sysconf.instance_config.allow_x11_tcp_connect:
                self.debug("forward_x11_to_manager: tcp connects to xserver are not allowed.")
            if not manager_daemon_supports_forwards_to_unix_sockets or x_unix_socket is None:
                if x_unix_socket is None:
                    self.debug("x11-forward: manager does not use unix-domain-socket for %r" % manager_display)
                else:
                    self.info("daemon running on manager does not support tcp-forwards to X11-socket! - try to forward to X-tcp-port!")
                target = xport
            else:
                target = "unix:%s" % x_unix_socket
                
            self.establish_tcp_forward(
                _establish_xforwarding_step3,
                self.host, self.manager.host,
                target,
                keep_listening=True,
                name=forward_name,
                from_port=-2,
                terminate_at_loopback=True
            )            
        
        self.manager.with_daemon_for_host(
            self.manager.host, "when_register_finished",
            _establish_xforwarding_step2
        )
        
    def _start_process_bh2(self, ctx):
        """
        check whether we need forwarding to managers X-connection

        (either because user specified forward_x11_to_manager-process-flag or because forward_x11-process-flag resolved to manager's gui)
        """
        if not ctx.p.forward_x11 and not ctx.forward_x11_to_manager:
            # nope, no forwarding needed
            return self._start_process_bh3(ctx)

        # yes, create forwarding to manager's x-connection
        def _have_manager_xforwarding(ret=None, exception=None):
            if exception is not None:
                msg = "can not start process %r because we failed to get x-forwarding to manager: %s" % (ctx.p.name, exception)
                self.error(msg)
                return ctx.cb(exception=msg)
            self._start_process_bh21(ctx)
        
        self._establish_xforwarding(_have_manager_xforwarding)
        
    def _start_process_bh21(self, ctx):
        entry_display, XAUTHORITY, DISPLAY_HOME = self.xforwarding
        
        o = 0
        for i, (key, value) in enumerate(ctx.environment):
            if key in ("DISPLAY", "XAUTHORITY"):
                self.warning("process %r has flag forward_x11 but you specified a %s variable. it will be overwritten!" % (
                    ctx.p.name, key))
                del ctx.environment[i - o]
                o += 1
        ctx.environment.append(("DISPLAY", entry_display))
        ctx.environment.append(("XAUTHORITY", XAUTHORITY))

        if not ctx.have_home and DISPLAY_HOME and not ctx.p.set_user_and_home:
            # provide home directory for x-process to be able to read font cache from $HOME/.cache/fontconfig/...
            ctx.environment.append(("HOME", DISPLAY_HOME))

        self._start_process_bh3(ctx)
        
    def _start_process_bh3(self, ctx):
        """
        x-forwarding is done.
        set some more env vars and decide manager address for this process
        """
        
        p = ctx.p

        if p.set_user_and_home:
            user = self.USER or self.USER_env
            if user:
                ctx.environment.append(("USER", user))
            else:
                self.error("process flag set_user_and_home is set, but daemon could not provide valid USER!")
            home = self.HOME or self.HOME_env
            if home:
                ctx.environment.append(("HOME", home))
            else:
                self.error("process flag set_user_and_home is set, but daemon could not provide valid HOME!")

        if not ctx.have_term and not p.no_pty:
            ctx.environment.append(("TERM", "xterm"))

        if self.manager.sysconf.instance_config.manager_port is not None and not ctx.have_ln_manager:
            def _have_manager_address(address=None, exception=None):
                if exception is not None:
                    msg = "can not start process %r because we failed to get manager address: %s" % (ctx.p.name, exception)
                    self.error(msg)
                    return ctx.cb(exception=msg)
                mgr_ip, mgr_port = address
                ln_manager = "%s:%s" % (mgr_ip, mgr_port)
                ctx.environment.append(("LN_MANAGER", ln_manager))
                self._fixup_ln_manager_runtime_var(ctx, ln_manager)
                return self._start_process_bh31(ctx)
            self._get_manager_address(_have_manager_address, dump_only=ctx.dump_only)
        else:
            if ctx.have_ln_manager:
                self._fixup_ln_manager_runtime_var(ctx, ctx.have_ln_manager_value)
            self._start_process_bh31(ctx)
    
    def _start_process_bh31(self, ctx):
        change_user = self.host.change_user_to
        p = ctx.p
        if p.change_user_to:
            change_user = p.change_user_to
            
        if ctx.dump_only:
            dump = ctx.dump
            arch = self.architecture
            if p.host != self.manager.host:
                # need remote login!
                if "linux" in arch or "qnx" in arch:
                    rl = "ssh"
                elif "vxworks" in arch:
                    rl = "telnet"
                else:
                    rl = "stand_up_from_your_chair_and_walk_to_console_of_pc"
                dump.append("%s %s" % (rl, p.host.name))
                if "vxworks" in arch:
                    dump.append("# ignore 'not found' error!!\ncmd")

            if "vxworks" in arch:
                for n, v in ctx.environment:
                    if " " in v:
                        v = '"%s"' % v
                    dump.append("setenv %s %s" % (n, v))
                export_cmd = "setenv"
            else:
                if "win" in arch:
                    export_cmd = "set"
                else:
                    export_cmd = "export"
                for n, v in ctx.environment:
                    if " " in v:
                        v = '"%s"' % v
                    if "win" not in arch and len(v) > 82 and os.pathsep in v:
                        dump.append("")
                        dump.append("%s %s=\\" % (export_cmd, n))
                        parts = v.strip().strip(os.pathsep).split(os.pathsep)
                        for i, line in enumerate(parts):
                            if i + 1 < len(parts):
                                dump.append(line + os.pathsep + "\\")
                            else:
                                dump.append(line)
                    else:
                        dump.append("%s %s=%s" % (export_cmd, n, v))
            
            if ctx.change_directory:
                if "vxworks" in arch:
                    dump.append('cd "%s"' % ctx.change_directory)
                else:
                    if " " in ctx.change_directory or '"' in ctx.change_directory:
                        dump.append("cd '%s'" % ctx.change_directory)
                    else:
                        dump.append("cd %s" % ctx.change_directory)

            if ctx.policy != -1:
                dump.append("# set scheduling policy %r" % ctx.policy)
            if ctx.priority not in (-1, 0):
                dump.append("# set scheduling priority %r" % ctx.priority)
            if p.affinity != -1:
                dump.append("# set scheduling affinity %r" % p.affinity)

            if change_user is not None:
                dump.append("su %s -c '%s'" % (change_user, "%s" % ctx.cmdline))
            else:
                dump.append("%s" % ctx.cmdline)
            
            if p.use_vte:
                out = "\r\n".join(dump)
            else:
                out = "\n".join(dump)
            return ctx.cb("\r\n" + out + "\r\n")

        args = dict(
            name=p.name, 
            cmdline=ctx.cmdline, 
            start_in_shell=p.start_in_shell, 
            no_pty=p.no_pty,
            policy=ctx.policy,
            priority=ctx.priority,
            affinity=p.affinity,
            environment=ctx.environment, 
            change_directory=ctx.change_directory, 
            term_timeout=p.term_timeout,
            max_output_frequency=p.max_output_frequency,
            log_output=True,
            buffer_size=int(p.output_buffer_size),
        )
        
        if self.protocol_version >= 27:
            args["use_execvpe"] = p.use_execvpe
        elif p.use_execvpe:
            self.warning("daemon is too old to support use_execvpe flag! (version %d, need 27)" % self.protocol_version)
        
        if p.blocking_output:
            if self.protocol_version < 25:
                self.warning("daemon is too old to support blocking process output! (version %d, need 25)" % self.protocol_version)
            else:
                args["blocking_output"] = True
        elif self.protocol_version >= 25:
            args["blocking_output"] = False
        
        if change_user is not None:
            if self.protocol_version < 12:
                self.error("daemon is too old (version %d, need 12) to change user to %r" % (self.protocol_version, change_user))
            else:
                if ":" in change_user and self.protocol_version < 25:
                    self.warning("daemon is too old to change user AND group to %r! (version %d, need 25)" % (change_user, self.protocol_version))
                    change_user = change_user.split(":", 1)[0]
                args["change_user_to"] = change_user

        if p.stack_size is not None:
            if self.protocol_version < 13:
                self.error("daemon is too old (version %d, need 13) to specify stack_size" % (self.protocol_version))
            else:
                args["stack_size"] = p.stack_size

        if p.resource_limits:
            if self.protocol_version < 14:
                self.error("daemon is too old (version %d, need 14) to specify resource_limits" % (self.protocol_version))
            else:
                args["resource_limits"] = p.resource_limits

        if hasattr(p, "_last_window_size"):
            if not p.no_pty and self.protocol_version >= 33:
                args["term_rows"], args["term_cols"] = p._last_window_size

        self._start_process_pid = None
        
        def _finalize_start_process(response=None, exception=None):
            if exception is not None:
                # error starting process!
                self._start_process_pid = False
                self._start_process_error = exception
                p._last_error = exception
                return ctx.cb(exception=exception)
            
            self._start_process_pid = response["pid"]
            self._started_processes[response["pid"]] = p
            p.pid = response["pid"]
            if "start_time" in response:
                p.start_time = response["start_time"]
            else:
                self.warning("daemon didn't sent start time!")
                p.start_time = time.time()
            p.stop_time = None
            p.daemon = self
            ctx.cb()        
        self.send_request("start_process", _finalize_start_process, **args)

    @create_connection
    def stop_process(self, cb, pid, term_signal):
        """
        calls either cb() or eb(exception=...)
        """
        self._stopped_process = False
        def _finalize_stop_process(response=None, exception=None):
            if exception is not None:
                msg = "received error to stop pid %s: %s" % (pid, exception)
                self.error(msg)
                self._stopped_process = pid
                self._stop_success = True
                self.manager._terminate_process(self, pid)
            if response and "unknown process" in response.get("info", ""):
                # there will be no process_terminated - message because this daemon didn't started this process
                # synthesize!
                self._terminate_process(pid)
            if exception is not None:
                return cb(exception=msg)
            return cb()
        self.send_request("stop_process", _finalize_stop_process, pid=pid, term_signal=int(term_signal))

    @create_connection
    def signal_process(self, cb, pid, signo):
        if pid in (None, "0", "-1"):
            raise Exception("invalid pid %r" % (pid, ))
        #self.add_hook(("response", "signal_process"), _finalize_stop_process, arg=pid)
        self.send_request("send_signal", cb, pid=pid, signo=signo)

    def is_forward_still_needed(self, name):
        for state_name, state in self.manager.sysconf.states.items():
            forward_name = state.needed_forwards.get(self.host.name)
            if forward_name == name:
                self.debug("state %r still needs forward %r!" % (state_name, name))
                return state_name
        # todo: is it needed for a daemon connection?
        # todo: is it needed for a reverse manager connection?
        return None

    def request_tunnel_state(self, cb, up_down, state_name, cmds):
        # its a tunnel state!
        tunnel_name = "tcp-tunnel-state %s\0 to %s/%s:%s from %s/%s" % (
            state_name,
            cmds["tunnel_exit_host"], cmds["tunnel_exit_target_ip"], cmds["tunnel_exit_target_port"],
            self.host.name, cmds["tunnel_entry_port"])

        created_forwards = {} # host -> forward_name
        tunnel_args = dict(
            from_host=self.host, from_port=cmds["tunnel_entry_port"],
            to_host=cmds["tunnel_exit_host"], to_port=cmds["tunnel_exit_target_port"],
            keep_listening=True,
            name=tunnel_name,                 
            terminate_at_loopback=True,
            terminate_at_loopback_ip=cmds["tunnel_exit_target_ip"],
            created_forwards=created_forwards,
            need_full_daemon_registration=True,
            listen_ip="0.0.0.0",
        )

        if up_down == "UP":
            def _tunnel_up_fw_finished(address=None, exception=None):
                if exception is not None:
                    msg = "could not create tunnel state forward: %s" % exception
                    self.error(msg)
                    return cb(exception=msg)
                forward_host, forward_port = address
                self.info("tcp-tunnel-state %r entry at %s:%s" % (state_name, forward_host, forward_port))
                self.manager.sysconf.states[state_name].needed_forwards = created_forwards
                self.manager._got_state_notification(self, dict(name=state_name, up_down="UP"))
                return cb()

            return self.establish_tcp_forward(
                _tunnel_up_fw_finished,
                **tunnel_args
            )

        if up_down == "DOWN":
            state = self.manager.sysconf.states[state_name]
            def _tunnel_down_finished(ret=None, exception=None):
                self.manager._got_state_notification(self, dict(name=state_name, up_down="DOWN"))
                if exception is not None:
                    msg = "could not DOWN tunnel state: %s" % exception
                    self.error(msg)
                    return cb(exception=msg)
                state.needed_forwards = {}
                return cb()
            return self.establish_tcp_forward(
                _tunnel_down_finished,
                do_delete=True,
                **tunnel_args
            )

        if up_down == "CHECK":
            def _tunnel_check_finished(address=None, exception=None):
                if exception is not None:
                    msg = "could not check tunnel state: %s" % exception
                    self.error(msg)
                    self.manager._got_state_notification(self, dict(name=state_name, up_down="DOWN"))
                    cb(exception=msg)
                    return 
                if address is None:
                    # tunnel is down
                    up_down = "DOWN"
                else:
                    forward_host, forward_port = address
                    up_down = "UP"
                    self.info("tcp-tunnel-state %r UP, entry at %s:%s" % (state_name, forward_host, forward_port))
                    self.manager.sysconf.states[state_name].needed_forwards = created_forwards
                self.manager._got_state_notification(self, dict(name=state_name, up_down=up_down))
                cb()
                return 

            return self.establish_tcp_forward(
                _tunnel_check_finished,
                check_only=True,
                **tunnel_args
            )

        raise Exception("invalid tunnel state requested: %r" % up_down)
    
    @create_connection
    def request_state(self, cb, up_down, state_name, cmds, os_state):
        """
        will call cb(msg=None) or cb(exception=...)
        """
        if "tunnel_exit_target_ip" in cmds:
            return self.request_tunnel_state(cb, up_down, state_name, cmds)
        
        replace_runtime_vars = lambda data: self.replace_runtime_vars_for_obj(self.manager.sysconf.states[state_name], data) # noqa: E731
        if up_down == "UP" and not cmds["up"]:
            raise Exception("can not UP this state %r because there is no UP-command defined!" % state_name)
        if up_down == "DOWN" and not cmds["down"]:
            raise Exception("can not DOWN this state %r because there is no UP-command defined!" % state_name)

        #self.debug("request start of process %s: cmd %s env %s" % (name, cmdline, environment))
        arch_cmds = {}
        for name, cmdline in cmds.items():
            if cmdline is None:
                arch_cmds[name] = ""
                continue
            cmdline = replace_runtime_vars(cmdline)
            if hasattr(self, "architecture") and "win" in self.architecture:
                if cmdline.startswith("/home/"):
                    first, rest = cmdline.split(" ", 1) # todo: hack! what if spaces in program name!
                    first = first[len("/home/"):].replace("/", "\\")
                    cmdline = "z:\\%s.exe %s" % (first, rest)
            arch_cmds[name] = cmdline
        #self.debug("request state %s %s: cmds:\n%s" % (state_name, up_down, pprint.pformat(arch_cmds)))

        def _eval_answer(msg=None, exception=None):
            if exception is not None:
                msg = "request_state %r returned: %s" % (state_name, exception)
                self.error(msg)
                return cb(exception=msg)
            cb(msg)
        
        if up_down == "CHECK":
            up_down = ""

        if self.protocol_version < 30:
            args = dict(
                min_check_interval=5,
                auto_check_interval=0,
                need_check_after_up=False
            )
        else:
            args = dict()
            
        self.send_request(
            "request_state",
            _eval_answer,
            name=state_name, 
            up_down=up_down,
            check=arch_cmds["check"],
            up=arch_cmds["up"],
            down=arch_cmds["down"],
            os_state=os_state,
            **args
        )        

    @create_connection
    def set_process_ready(self, cb, pid):
        if pid not in self._started_processes:
            # pid no longer running, no need to mark ready!
            return
        self.send_request("set_process_ready", cb, pid=pid)

    @create_connection
    def set_process_property(self, cb, pid, name, value):
        self.send_request(
            "set_process_property", cb,
            pid=pid,
            name=name,
            value=value
        )
        
    @create_connection
    def set_state_property(self, cb, state_name, name, value):
        self.send_request(
            "set_state_property", cb,
            state_name=state_name,
            name=name,
            value=value)

    def _list_processes(self, cb):
        """
        calls cb(processes) or cb(exception=...)
        """
        def _answer(msg=None, exception=None):
            if exception is not None: return cb(exception=exception)
            procs = msg["processes"]
            if not is_py2 and self.protocol_version < 31:
                # daemon did send process output as string-literls and eval() in CommunicationUtils
                # did create a string(unicode) object from it, expecting that the process-output
                # is actually utf-8 -- which can not be safely assumed!
                # newer daemons will send process output as byte-strings instead so that eval() does not
                # have to assume any encoding.
                # here the bad stuff already happend, we can only try to undo the harm, which might not always work!
                new_procs = []
                for pname, pcommand, ppid, is_ready, start_time, old_process_output, props in procs:
                    # old_process_output is now str, but it should be bytes!
                    p = self.manager.sysconf.processes.get(pname)
                    if p is None:
                        old_process_output = b"" # drop output
                    else:
                        # try to undo utf-8 encoding done by string-literal-evaluation
                        try:
                            old_process_output = old_process_output.encode("utf-8")
                        except Exception:
                            old_process_output = b""
                    new_procs.append(
                        (pname, pcommand, ppid, is_ready, start_time, old_process_output, props)
                    )
                procs = new_procs
            cb(procs)
        self._send_request("list_processes", _answer)
    
    @create_connection
    def list_processes(self, cb):
        self._list_processes(cb)
        
    @create_connection
    def retrieve_process_list(self, cb, show_env=False, show_threads=False, filter_string=""):
        """
        calls cb(process_list) or cb(exception=...)
        """
        def _finalize(msg=None, exception=None):
            if exception is not None:
                msg = "failed to retrieve process list: %s" % exception
                self.error(msg)
                return cb(exception=msg)
            cb(msg["process_list"])

        if not is_py2 and self.protocol_version < 31:
            raise Exception(("daemon is running old protocol version %s which sends process lists that can not be read by python3!\n"
                            "please restart a newer daemon! (need atleast version 31)") % self.protocol_version)

        self.send_request(
            "retrieve_process_list", 
            _finalize,
            show_env=show_env, 
            show_threads=show_threads,
            filter_string=filter_string
        )
    
    @create_connection
    def get_sched_prio_cpu_for(self, cb, pid, tid):
        """
        calls cb(ret) or cb(exception=...)
        """
        if tid is None:
            raise Exception("can not get sched, prio, cpu for tid of %r" % tid)
        filters = []
        filters.append(("tid", "==", tid))
        if "linux" not in self.architecture: # on linux tid is unique
            filters.append(("pid", "==", pid)) # all others need pid as well

        filter_string = "\0".join(["%s:%s:%s" % (key, op, value) for key, op, value in filters])

        def _process_answer(plist=None, exception=None):
            from . import process_listing
            if exception is not None:
                return cb(exception=exception)
            # has to have atleast length 2 - header and one thread
            
            org_plist = plist
            if "vxworks" in self.architecture:
                plist = process_listing.process_list.unpack_vxworks_processlist(plist)
            
            if len(plist) < 2 or (len(plist) == 2 and len(plist[1]) == 0):
                return cb(exception="filter did not match any threads: %r" % filter_string)
            
            if len(plist) > 2:
                return cb(exception="filter matched multiple threads: %r %d threads:\n%s" % (
                    filter_string,
                    len(plist),
                    pprint.pformat(plist)))
            try:
                ret = process_listing.process_list.get_sched_prio_cpu_from_list(plist, self.architecture)
            except Exception:
                self.error("org plist: %r\n%s" % (org_plist, pprint.pformat(plist)))
                return cb(exception=str(sys.exc_info()[1]))
            return cb(ret)

        self.retrieve_process_list(_process_answer, show_env=False, show_threads=True, filter_string=filter_string)

    @create_connection
    def get_state(self, cb, port_id):
        """
        calls cb(response) or cb(exception=...)
        """
        self.send_request("get_state", cb, port=port_id)
    
    @create_connection
    def get_last_packet(self, cb, port_id):
        """
        calls cb(msg) or cb(exception=...) on error
        """
        self.send_request("get_last_packet", cb, port=port_id)

    @create_connection
    def set_prio(self, cb, pid, tid, prio, policy=-1):
        """
        calls cb() or cb(exception=...)
        """
        if pid in (None, "0", "-1"):
            raise Exception("invalid pid %r" % (pid, ))
        if isinstance(pid, (str, unicode)):
            pid = eval(pid)
        if isinstance(tid, (str, unicode)):
            tid = eval(tid)
        self.send_request("set_prio", check_error_only(cb), pid=pid, tid=tid, prio=prio, policy=policy)

    @create_connection
    def set_cpu_affinity(self, cb, pid, tid, affinity):
        """
        calls cb() or cb(exception=...)
        """
        if pid in (None, "0", "-1"):
            raise Exception("invalid pid %r" % (pid, ))
        if isinstance(pid, (str, unicode)):
            pid = eval(pid)
        if isinstance(tid, (str, unicode)):
            tid = eval(tid)
        self.send_request("set_affinity", check_error_only(cb), pid=pid, tid=tid, affinity=affinity)

    @needs_connection
    def send_stdin(self, pid, text):
        if pid in (None, 0, "0", "-1"):
            raise Exception("invalid pid %r" % (pid, ))
        if isinstance(pid, (str, unicode)):
            pid = eval(pid)
        self.send("send_stdin", pid=pid, text=text)
        
    @needs_connection
    def send_winch(self, pid, rows, cols):
        if pid in (None, 0, "0", "-1"):
            raise Exception("invalid pid %r" % (pid, ))
        if isinstance(pid, (str, unicode)):
            pid = eval(pid)
        self.send("send_winch", pid=pid, rows=rows, cols=cols)

    @create_connection
    def set_debug_enabled(self, cb, enabled=True):
        """
        calls cb() or cb(exception=...)
        """
        if self.protocol_version < 24:
            raise Exception("this daemon does not support the set_debug-request!")
        self.send_request("set_debug", check_error_only(cb), debug_enabled=enabled)

    @create_connection
    def release_all_resources(self, cb):
        """
        calls cb() or cb(exception=...)
        """
        if not self.have_release_all_resources:
            raise Exception("this daemon does not support the release_all_instance_resources-request!")
        
        self.info("requesting daemon on %s to release all instance resources!" % self.host)
        self.ignore_port_removes = True

        def _on_answer(resp=None, exception=None):
            if exception is not None:
                return cb(exception=exception)
            for pid in list(self._started_processes.keys()):
                self._terminate_process(pid)
            self._init_resource_registries()
            return cb()
        
        self.send_request("release_all_instance_resources", _on_answer)

    def daemon_tcp_forward_change(self, **r):
        fw = r["forward"]
        n_connections, last_change = fw[6:6+2]
        forward_key, name = self.get_forward_key(fw, with_name=True)
        self.tcp_forward_change(forward_key, name, n_connections, last_change, is_in_destroy=r["is_in_destroy"])
        return True

    def get_forward_key(self, fw, with_name=False):
        to_ip, to_port, listen_ip, listen_port, keep_listening, data = fw[:6]

        data = eval(data)
        name = data["name"]
        keep_listening = bool(keep_listening)
        forward_key = "%s:%s at %s %s %s" % (to_ip, to_port, listen_ip, keep_listening, name)
        if with_name:
            return forward_key, name
        return forward_key

    def tcp_forward_change(self, forward_key, name, n_connections, last_change, is_in_destroy=False):
        self.debug("tcp_forward_change: n_conns %s, last_change %s fw: %s" % (
            n_connections, last_change, forward_key))
        
        if n_connections > 0 or is_in_destroy:
            if forward_key in self._forwards_to_destroy:
                del self._forwards_to_destroy[forward_key]
            return
        
        # leave reverse manager forwards alone
        if "reverse mgr forward to manager" in forward_key:
            return
        
        # leave arbiter forwards alone
        if "arbiter" in forward_key:
            return

        # leave x11 forwards alone
        if self._get_x11_forward_name() in forward_key:
            return
        
        if self.protocol_version < 22:
            return # can not delete forwards anyway...

        # is it a tunnel state that is still requested to be up?
        is_needed_by_state = False        
        for state in list(self.manager.sysconf.states.values()):
            if state.state != "UP":
                continue
            if name == state.needed_forwards.get(self.host.name):
                self.debug("state %s needs this forward!" % state.name)
                is_needed_by_state = True
        if is_needed_by_state:
            return
        
        # plan to remove this forward
        self.debug("this forward seems to be no longer needed. queue removal...")
        remove_unused_forward_after = 20
        self._forwards_to_destroy[forward_key] = time.time() + remove_unused_forward_after, name

        if self._forwards_destroyer is None:
            self._forwards_destroyer = GLib.timeout_add(1000, self._destroy_forwards)

    def _destroy_forwards(self):
        now = time.time()
        to_destroy = []
        for key, (when, name) in self._forwards_to_destroy.items():
            if when <= now:
                to_destroy.append((key, name))

        def _destroy_next(found=None, exception=None):
            if not to_destroy:
                if self._forwards_to_destroy and self._forwards_destroyer is None:
                    self._forwards_destroyer = GLib.timeout_add(1000, self._destroy_forwards)
                    return False                
                return len(self._forwards_to_destroy) != 0
            key, name = to_destroy.pop(0)
            if key in self._forwards_to_destroy:
                del self._forwards_to_destroy[key]
            self.delete_tcp_forward(_destroy_next, name)
        return _destroy_next()
    
    def register_finished(self):
        self.connect_is_running = False
        self.register_is_finished = True
        self.register_finished_time = time.time()
        self._fw_test_ports = None
        self.info("daemon registration finished")
        self.when_register_finished.trigger()
        self.call_hook("when_register_finished")

        if self.host != self.manager.host:
            if self.upstream_daemon is None: # direct reachable...
                upstream = self.manager.get_daemon(self.manager.host)
            else:
                upstream = self.upstream_daemon
            # initiate test for incoming firewall on self.upstream_daemon
            def _response(exception=None):
                pass # could be used for forther chaining
            upstream._start_test_for_firewall(_response, self)

    @create_connection
    def remove_log_tokens(self, cb, tokens):
        """
        calls cb() or cb(exception=...)
        """
        if self.protocol_version < 28:
            return cb()
        self.send_request("remove_log_tokens", check_error_only(cb), tokens=tokens)

    @create_connection
    def retrieve_log_token(self, cb, port):
        """
        calls cb(msg) or cb(exception=...)
        """
        if self.protocol_version < 28:
            raise Exception("this daemon is too old for log tokens! atleast protocol_version 28 is needed (this has %d)", self.protocol_version)
        return self.send_request("retrieve_log_token", cb, port=port)
    
    def is_connected(self):
        return self.when_register_finished.get_reached_or_fail() is True
    
    def _list_ports(self, cb, with_last_packet=False):        
        """
        calls cb(response) or cb(exception=...)
        """
        if with_last_packet:
            with_last_packet = True
        else:
            with_last_packet = False
        return self._send_request("list_ports", cb, with_last_packet=with_last_packet)
    
    @create_connection("when_connected")
    def list_ports(self, cb, with_last_packet=False):        
        """
        calls cb(response) or cb(exception=...)
        """
        return self._list_ports(cb, with_last_packet)
    
    def _list_tcp_forwards(self, cb):
        """
        calls cb(response) or cb(exception=...)
        """
        return self._send_request("list_tcp_forwards", cb)
    
    @create_connection
    def list_tcp_forwards(self, cb):
        return self._list_tcp_forwards(cb)

    @create_connection
    def start_logging(self, cb, port_id, max_samples, max_time, only_ts=False, divisor=1):
        """
        calls cb(response) or cb(exception=...)
        """
        return self.send_request("start_logging", cb, port=port_id, max_samples=max_samples, max_time=max_time, only_ts=only_ts, divisor=divisor)
    
    @create_connection
    def stop_logging(self, cb, port_id):
        """
        calls cb(response) or cb(exception=...)
        """
        return self.send_request("stop_logging", cb, port=port_id)
    @create_connection
    def retrieve_log(self, cb, port_id):
        """
        calls cb(response) or cb(exception=...)
        """
        return self.send_request("retrieve_log", cb, port=port_id)
    @create_connection("when_connected")
    def is_tcp_forward_source_port(self, cb, port):
        """
        is tcp-port 'port' the outgoing sockname of a tcp forward?
        if so call cb(caddr). if not call cb(False)  or cb(exception=...) on error
        caddr is the peername of the incoming client connection on that daemon for this forward
        """
        def _answer(resp=None, exception=None):
            if exception is not None: return cb(exception=exception)
            forwarded_from = resp["forwarded_from"]
            if not forwarded_from: # empty string
                return cb(False) # nope
            cb(forwarded_from) # yes!
        return self.send_request("is_tcp_forward_source_port", _answer, port=port)

    @create_connection
    def _start_test_for_firewall(self, cb, from_daemon):
        # we know that from_daemon finished registration
        # we explicitely reguested that self is connected.

        # is there a firewall against incoming connections on self.host when coming from from_daemon.host?
        if from_daemon.host in self._no_firewall_from:
            return # we already know there is none...

        if self.host.skip_fw_test:
            return # we are not interrested in testing whether self has a firewall

        timeout = 5
        too_old = None
        required_protocol_version = 32
        for daemon in self, from_daemon:
            if daemon.protocol_version < required_protocol_version:
                too_old = daemon
                self.warning("can not test for inbound-firewall on %s coming from %s because daemon on %s is too old (has protocol version %d we need %d)" % (
                    self.host, from_daemon.host,
                    daemon.host, daemon.protocol_version, required_protocol_version))
        if too_old is not None:
            return cb(exception="%s daemon is too old" % too_old.host)

        self.debug("start test for inbound-firewall on %s coming from %s" % (self.host, from_daemon.host))

        target_ip = from_daemon.host.get_ip_from(self.host)
        remote_id = id(from_daemon.host) % 0x7fffffff # 31 bit id for that daemon

        def success(cb=cb, remote_id=remote_id):
            if from_daemon.host in self._no_firewall_from:
                return
            self._no_firewall_from.add(from_daemon.host)
            self.debug("no firewall on %s from %s" % (self.host, from_daemon.host))
            cb()

        def timeout_result(remote_id=remote_id, cb=cb):
            msg = "timed out waiting for firewall-test results on %s:" % self.host
            udp_ok = ("udp", remote_id) in self._fw_test_results
            tcp_ok = ("tcp", remote_id) in self._fw_test_results
            if udp_ok and tcp_ok:
                return False
            if not udp_ok:
                msg += "\n it looks like incoming UDP packets are dropped!"
            if not tcp_ok:
                msg += "\n it looks like incoming TCP connections are dropped!"
            msg += "\nadd `skip_fw_test:true` to %s's host-line to disable this check!" % self.host
            self.error(msg)
            cb(exception=msg)
            return False

        def error_result(error, cb=cb):
            msg = "error testing for firewall on %s: %s" % (self.host, error)
            if "timeout connecting" in error:
                msg += "\n   it looks like there is a firewall active! (incoming TCP connections are DROP'ed)"
            if "connection refused" in error.lower():
                msg += "\n   it looks like there is a firewall active! (incoming TCP connections are REFUSE'd)"
            msg += "\nadd `skip_fw_test:true` to %s's host-line to disable this check!" % self.host
            self.error(msg)
            cb(exception=msg)

        def check_for_success(remote_id=remote_id, success=success):
            udp_ok = ("udp", remote_id) in self._fw_test_results
            tcp_ok = ("tcp", remote_id) in self._fw_test_results
            if udp_ok and tcp_ok:
                # both results already arrived and are ok
                success() # done
                return True # done
            return False

        def test_answer(resp=None, exception=None, remote_id=remote_id, success=success, timeout_result=timeout_result, error_result=error_result):
            if exception:
                return error_result(exception)
            # from_daemon reported successful test. did we see UDP & TCP responses?
            if not check_for_success():
                # wait 5 more seconds for report from our ln_daemon!
                GLib.timeout_add(int(timeout * 1e3), timeout_result)

        def start_test(target_ip=target_ip, cb=cb, remote_id=remote_id):
            udp_port, tcp_port = self._fw_test_ports
            self.debug("starting fw-test from %s remote_id %d\n" % (from_daemon.host, remote_id))
            return from_daemon.send_request(
                "test_for_fw", test_answer,
                ip=target_ip,
                udp_port=udp_port,
                tcp_port=tcp_port,
                remote_id=remote_id,
                timeout=timeout
            )

        def incoming_test_result(daemon, resp, check_for_success=check_for_success):
            this = resp["kind"], resp["remote_id"]
            self.debug("incoming fw-test result: %r" % (this, ))
            self._fw_test_results.add(this)
            check_for_success()
            return True

        if self._fw_test_ports is None:
            # first open receiver ports
            def answer(resp=None, exception=None, cb=cb):
                if exception:
                    return cb(exception="could not open fw-test ports:\n%s" % exception)
                self._fw_test_ports = resp["udp_port"], resp["tcp_port"]
                self._fw_test_results = set() # of (kind, remote_id)
                self.add_hook(("response", "fw_test_result"), incoming_test_result)
                start_test()
            return self.send_request("open_fw_test_ports", answer)
        else:
            # remove (partial) old test results for this remote_id
            self._fw_test_results.discard(("udp", remote_id))
            self._fw_test_results.discard(("tdp", remote_id))
            # start test
            start_test()

def test_runtime_function_parser():
    functions = dict(
        own_ip_in_network=lambda *args: "<own ip in net %r>" % (args, ),
        own_ip_for=lambda *args: "<own ip %r>" % (args, ),
    )
    def error_reporter(msg):
        print("error: %s" % msg)
    parser = runtime_function_parser(functions)

    inputs = """
$(own_ip_in_network)
$(own_ip_in_network)_
_$(own_ip_in_network)
add environment: OWN_IP_FOR_NETAB=$(own_ip_in_network)other crap
add environment: OWN_IP_FOR_NETAB=$(own_ip_in_network "AB") stufff
add environment: OWN_IP_FOR_NETBC=$(own_ip_in_network "BC")$(own_ip_for nodeA)combined
add environment: OWN_IP_FOR_NETAB2=$(own_ip_in_network AB)between$(own_ip_for nodeA)
add environment: OWN_IP_FOR_NODEA=$(own_ip_for nodeA)
add environment: OWN_IP_FOR_NODEB=$(own_ip_for nodeB)
add environment: OWN_IP_FOR_HOSTA=$(own_ip_for hostA)
""".strip().split("\n")
    for inp in inputs:
        print()
        print("test: %r" % inp)
        outp = parser.replace(inp, error_reporter)
        print("      %r" % outp)
        
if __name__ == "__main__":
    test_runtime_function_parser()
