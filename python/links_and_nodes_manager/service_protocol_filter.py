"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

import os
import socket
import struct
import traceback
import time

import links_and_nodes as ln

from .Logging import enable_logging_on_instance
from .ClientStore import ClientStore
from .CommunicationUtils import callback

import gi
gi.require_version('GLib', '2.0')
from gi.repository import GLib # noqa: E402

class service_protocol_filter_client(object):
    def __init__(self, parent, fd, address):
        self.parent = parent
        self.fd = fd
        self.address = address
        self.fd.setblocking(False)
        self.data_queue = ""
        self.io_watch = GLib.io_add_watch(self.fd, GLib.IO_IN, self.read_data)
        
        self.debug = self.parent.do_debug
        self.state = "idle"

        self.service_fd = socket.socket()
        self.service_fd.connect(self.parent.service_address)
        self.service_fd.setblocking(False)
        self.service_io_watch = GLib.io_add_watch(self.service_fd, GLib.IO_IN, self.read_data_from_service)

    def read_data_from_service(self, fd, why):
        data = self.service_fd.recv(8192)
        if self.debug: self.parent.debug("%s: got data from provider: %r" % (self.parent.service.name, data, ))
        if data == "":
            self.parent.info("service provider at %s closed connection in state %s." % (self.parent.service_address, self.state))
            return self._destruct()
        try:
            self.fd.send(data)
        except Exception:
            self.parent.error("error processing service response data:\n%s" % traceback.format_exc())
            return self._destruct()
        return True
    
    def _destruct(self):
        GLib.source_remove(self.io_watch)
        GLib.source_remove(self.service_io_watch)
        self.fd.close()
        self.service_fd.close()
        self.parent.remove_client(self)
        return False
        
    def read_data(self, fd, why):
        data = self.fd.recv(8192)
        if self.debug: self.parent.debug("%s: got data from client %r: %r" % (self.parent.service.name, self.address, data))
        if not data:
            self.parent.info("client from %s:%d closed connection in state %s." % (self.address[0], self.address[1], self.state))
            return self._destruct()
        try:
            return self.process_client_data(data)
        except Exception:
            self.parent.error("error processing client request data:\n%s" % traceback.format_exc())
            return self._destruct()

    def process_client_data(self, data):
        if self.state == "forward_data":
            n_new_data = min(self.request_bytes_left, len(data))
            self.service_fd.send(data[:n_new_data])
            self.request_bytes_left -= n_new_data
            if self.log_services: self.log_data[1].append(data[:n_new_data])
            if self.request_bytes_left > 0:
                return True # not yet finished
            # request finished
            if self.log_services:
                # now call all log service!
                request_data = "".join(self.log_data[1])
                if self.parent.service.provider.get_endianess() == "little":
                    endianess = 0
                else:
                    endianess = 1
                log_data = dict(
                    client_name="<manager forwarded>",
                    service_name=self.parent.service.name,
                    service_interface=self.parent.service.interface,
                    item_type=0,
                    endianess=endianess,
                    completion_time=self.log_data[2],
                    transfer_time=time.time() - self.log_data[2],
                    request_time=0,
                    peer_address="%s:%d" % self.address,
                    client_id=ln.LN_MANAGER_CLIENT_ID,
                    request_id=self.log_data[0][3],
                    thread_id=os.getpid(),
                    data=request_data
                    )
                def log_service_finished(resp, log_service):
                    self.parent.debug("log_service %r finished: %r" % (log_service.name, resp.error_message))
                    return False
                
                for log_service in self.log_services:
                    self.parent.debug("call log_service %r" % log_service)
                    log_service = ClientStore.get_service(self.parent.manager, log_service)
                    log_service.send_request(
                        "call",
                        callback(log_service_finished, log_service),
                        service_request_header=self.log_data[0][1:],
                        **log_data)
                    
            data = data[n_new_data:]
            self.state = "idle"
            self.data_queue = ""
        
        if self.state == "idle":
            # read size of request (4 byte)
            self.data_queue += data
            if len(self.data_queue) < 4:
                return True # wait for more data
            data = self.data_queue
            request_size = struct.unpack(self.parent.provider_endianess + "I", data[:4])[0]
            self.request_bytes_left = request_size - 4
            new_request_size = request_size + 2 + 4 + 4
            header = new_request_size, ln.LN_LIBRARY_VERSION, ln.LN_MANAGER_CLIENT_ID, self.parent.manager.next_request_id
            self.log_services = self.parent.service.provider.get_all_log_services(self.parent.service.name)
            if self.log_services:
                self.log_data = [header, [], time.time()]
            hdr = struct.pack(self.parent.provider_endianess + "IHII", *header)
            self.parent.manager.next_request_id += 1
            
            self.service_fd.send(hdr)
            self.state = "forward_data"
            return self.process_client_data(data[4:])
        return True
        
class service_protocol_filter(object):
    def __init__(self, manager, service, old_version, new_version):
        """
        this requires that we have a connection to the provider's daemon!
        """
        enable_logging_on_instance(self, "service_protocol_filter")
        self.do_debug = False
        self.manager = manager
        self.service = service
        self.old_version = old_version
        self.new_version = new_version

        self.listen_fd = socket.socket()
        self.listen_fd.listen(10)
        self.listen_ip, self.listen_port = self.listen_fd.getsockname()
        
        self.clients = []
        if self.old_version < 1:
            self.old_client_library_version = 12
        else:
            raise Exception("old service protocol version %d not supported!" % self.old_version)

        self.provider_endianess = None # not yet known, wait for get_address_for() request!
        
        def service_protocol_filter_bh(ip_port=None, exception=None):
            if exception is not None:
                msg = "could not get service port on manager: %s" % exception
                self.error(msg)
                return
            
            self.service_address = ip_port
            self.debug("listening on %s:%d to filter for %s at %s:%d" % (
                self.listen_ip, self.listen_port,
                self.service.name,
                self.service_address[0], self.service_address[1]
                ))
            self.io_watch = GLib.io_add_watch(self.listen_fd, GLib.IO_IN, self.accept_client)
            
        self.service.get_port_at(self.manager.host, service_protocol_filter_bh)
        
    def _destroy(self):
        GLib.source_remove(self.io_watch)
        self.listen_fd.close()
        while self.clients:
            self.clients[0]._destruct()
        self.clients = []

    def accept_client(self, fd, why):
        fd, address = self.listen_fd.accept() 
        ip_address, port = address
        self.debug("new connection from %s:%s" % (ip_address, port))
        try:
            self.clients.append(
                service_protocol_filter_client(self, fd, address))
        except Exception:
            self.parent.error("error connecting to service provider:\n%s" % traceback.format_exc())            
        return True

    def remove_client(self, client):
        self.clients.remove(client)
        
    def get_address_for(self, client, cb):
        """
        return ip address and port reachable for client to get to this protocol filter
        """

        if self.provider_endianess is None:
            # connect to provider daemon to be sure of provider's endianess
            def _set_endianess(daemon):
                if not daemon.is_connected():
                    return cb(exception="can not provide service-protocol-filter because we failed to start daemon on %s!" % self.service.provider.host)
                if daemon.endianess == "little":
                    self.provider_endianess = "<"
                else:
                    self.provider_endianess = ">"
                return self.get_address_for(client, cb)
            return self.manager.with_daemon_for_host(self.service.provider.host, "when_register_finished", _set_endianess)
        
        if client.host.is_direct_reachable(self.manager.host):
            cb((client.host.get_ip_from(self.manager.host, avoid_alternate_socket=True), self.listen_port))
            return
        
        self.manager.get_daemon(client.host).establish_tcp_forward(
            cb,
            client.host, self.manager.host, self.listen_port, True,
            "service protocol filter from %d for %s" % (self.old_version, self.service.name)
        )
