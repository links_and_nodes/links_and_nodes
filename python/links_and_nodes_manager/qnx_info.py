# warning: this is a auto-generated file by generate_qnx_info.py
# do not edit!

thread_flags = {
 65536: 'INTR_PENDING',
 131072: 'DETACHED',
 1048576: 'THREADS_HOLD',
 4194304: 'UNBLOCK_REQ',
 16777216: 'ALIGN_FAULT',
 33554432: 'SSTEP',
 67108864: 'ALLOCED_STACK',
 134217728: 'NOMULTISIG',
 268435456: 'FROZEN',
 2147483648: 'IOPRIV'
}

syscalls = {
 0: 'NOP',
 1: 'TRACE_EVENT',
 2: 'RING0',
 3: 'SPARE1',
 4: 'SPARE2',
 5: 'SPARE3',
 6: 'SPARE4',
 7: 'SYS_CPUPAGE_GET',
 8: 'SYS_CPUPAGE_SET',
 9: 'SYS_SPARE1',
 10: 'MSG_CURRENT',
 11: 'MSG_SENDV',
 12: 'MSG_SENDVNC',
 13: 'MSG_ERROR',
 14: 'MSG_RECEIVEV',
 15: 'MSG_REPLYV',
 16: 'MSG_READV',
 17: 'MSG_WRITEV',
 18: 'MSG_READWRITEV',
 19: 'MSG_INFO',
 20: 'MSG_SEND_PULSE',
 21: 'MSG_DELIVER_EVENT',
 22: 'MSG_KEYDATA',
 23: 'MSG_READIOV',
 24: 'MSG_RECEIVEPULSEV',
 25: 'MSG_VERIFY_EVENT',
 26: 'SIGNAL_KILL',
 27: 'SIGNAL_RETURN',
 28: 'SIGNAL_FAULT',
 29: 'SIGNAL_ACTION',
 30: 'SIGNAL_PROCMASK',
 31: 'SIGNAL_SUSPEND',
 32: 'SIGNAL_WAITINFO',
 33: 'SIGNAL_SPARE1',
 34: 'SIGNAL_SPARE2',
 35: 'CHANNEL_CREATE',
 36: 'CHANNEL_DESTROY',
 37: 'CHANCON_ATTR',
 38: 'CHANNEL_SPARE1',
 39: 'CONNECT_ATTACH',
 40: 'CONNECT_DETACH',
 41: 'CONNECT_SERVER_INFO',
 42: 'CONNECT_CLIENT_INFO',
 43: 'CONNECT_FLAGS',
 44: 'CONNECT_SPARE1',
 45: 'CONNECT_SPARE2',
 46: 'THREAD_CREATE',
 47: 'THREAD_DESTROY',
 48: 'THREAD_DESTROYALL',
 49: 'THREAD_DETACH',
 50: 'THREAD_JOIN',
 51: 'THREAD_CANCEL',
 52: 'THREAD_CTL',
 53: 'THREAD_SPARE1',
 54: 'THREAD_SPARE2',
 55: 'INTERRUPT_ATTACH',
 56: 'INTERRUPT_DETACH_FUNC',
 57: 'INTERRUPT_DETACH',
 58: 'INTERRUPT_WAIT',
 59: 'INTERRUPT_MASK',
 60: 'INTERRUPT_UNMASK',
 61: 'INTERRUPT_SPARE1',
 62: 'INTERRUPT_SPARE2',
 63: 'INTERRUPT_SPARE3',
 64: 'INTERRUPT_SPARE4',
 65: 'CLOCK_TIME',
 66: 'CLOCK_ADJUST',
 67: 'CLOCK_PERIOD',
 68: 'CLOCK_ID',
 69: 'CLOCK_SPARE2',
 70: 'TIMER_CREATE',
 71: 'TIMER_DESTROY',
 72: 'TIMER_SETTIME',
 73: 'TIMER_INFO',
 74: 'TIMER_ALARM',
 75: 'TIMER_TIMEOUT',
 76: 'TIMER_SPARE1',
 77: 'TIMER_SPARE2',
 78: 'SYNC_CREATE',
 79: 'SYNC_DESTROY',
 80: 'SYNC_MUTEX_LOCK',
 81: 'SYNC_MUTEX_UNLOCK',
 82: 'SYNC_CONDVAR_WAIT',
 83: 'SYNC_CONDVAR_SIGNAL',
 84: 'SYNC_SEM_POST',
 85: 'SYNC_SEM_WAIT',
 86: 'SYNC_CTL',
 87: 'SYNC_MUTEX_REVIVE',
 88: 'SCHED_GET',
 89: 'SCHED_SET',
 90: 'SCHED_YIELD',
 91: 'SCHED_INFO',
 92: 'SCHED_SPARE1',
 93: 'NET_CRED',
 94: 'NET_VTID',
 95: 'NET_UNBLOCK',
 96: 'NET_INFOSCOID',
 97: 'NET_SIGNAL_KILL',
 98: 'NET_SPARE1',
 99: 'NET_SPARE2',
 100: 'BAD'
}

states = {
 0: 'DEAD',
 1: 'RUNNING',
 2: 'READY',
 3: 'STOPPED',
 4: 'SEND',
 5: 'RECEIVE',
 6: 'REPLY',
 7: 'STACK',
 8: 'WAITTHREAD',
 9: 'WAITPAGE',
 10: 'SIGSUSPEND',
 11: 'SIGWAITINFO',
 12: 'NANOSLEEP',
 13: 'MUTEX',
 14: 'CONDVAR',
 15: 'JOIN',
 16: 'INTR',
 17: 'SEM',
 18: 'WAITCTX',
 19: 'NET_SEND',
 20: 'NET_REPLY',
 24: 'MAX'
}

debug_flags = {
 1: 'STOPPED',
 2: 'ISTOP',
 16: 'IPINVAL',
 32: 'ISSYS',
 64: 'SSTEP',
 128: 'CURTID',
 256: 'TRACE_EXEC',
 512: 'TRACE_RD',
 1024: 'TRACE_WR',
 2048: 'TRACE_MODIFY',
 65536: 'RLC',
 131072: 'KLC',
 262144: 'FORK',
 983040: 'MASK'
}

policies = {
 1: 'SCHED_FIFO', 2: 'SCHED_RR', 3: 'SCHED_OTHER', 4: 'SCHED_SPORADIC'
}

signals = {
 'SIGCONT': 25, 'SIGSTOP': 23
}

