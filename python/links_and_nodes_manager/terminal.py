# This file is part of links and nodes.
# 
# links and nodes is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# links and nodes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
# 
# Copyright 2019 DLR e.V., Florian Schmidt

import os
import sys
import termios
import atexit
import signal
import threading
try:
    import queue
except ImportError:
    import Queue as queue

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
from gi.repository import GLib, Gtk # noqa: E402

class terminal(object):
    color_sequences = dict(
        reset="\x1b[0m",
        red="\x1b[31m",
        green="\x1b[32m",
        yellow="\x1b[33m",
        blue="\x1b[34m",
        magenta="\x1b[35m",
        cyan="\x1b[36m",
        white="\x1b[37m",
        bright_black="\x1b[90m",
        bright_red="\x1b[91m",
        bright_green="\x1b[92m",
        bright_yellow="\x1b[93m",
        bright_blue="\x1b[94m",
        bright_magenta="\x1b[95m",
        bright_cyan="\x1b[96m",
        bright_white="\x1b[97m",
    )
    csi_sequences = dict(
        cursor_down = "B",
        cursor_up = "A",
        cursor_right = "C",
        cursor_left = "D",
        cursor_pgup = "5~",
        cursor_pgdown = "6~",
        cursor_pos1 = "H",
        cursor_end = "F",
        cursor_ctrl_pos1 = "1;5H~",
        
        delete = "3~",

        F5 = "15~",
        F6 = "17~",
        F7 = "18~",
        F8 = "19~",
        F9 = "20~",
        F10 = "21~",
        F11 = "23~",
        F12 = "24~",
    )
    csi_reports = {
        "R": "cursor-position",
        "n": "status",
        "t": "window-state",
    }
    input_sequences = dict(
        csi="\x1b[",
        osc="\x1b]",

        backspace = "\x7f",
        tab = "\t",

        F1 = "\x1bOP",
        F2 = "\x1bOQ",
        F3 = "\x1bOR",
        F4 = "\x1bOS",
        
        Ca = "\x01",
        Cb = "\x02",
        Cc = "\x03",
        Cd = "\x04",
        Ce = "\x05",
        Ck = "\x0b",
        Cl = "\x0c",
        Cr = "\x12",
        Cx_Cx = "\x18\x18",
        Cg_Cg_Cg = "\x07\x07\x07",

    )
    
    # https://en.wikipedia.org/wiki/ANSI_escape_code#Escape_sequences
    # better: https://www.xfree86.org/4.8.0/ctlseqs.html#Bracketed Paste Mode
    def __init__(self, stdin=None, stdout=None, history_fn=None):
        if stdin is None:
            stdin = sys.stdin
        if stdout is None:
            stdout = sys.stdout
        self.stdin = stdin
        self.stdout = stdout

        self.debug_enabled = os.getenv("LN_TERM_DEBUG", "")
        self.debug_enabled = self.debug_enabled and self.debug_enabled[:1] in "1yt"

        self.always_color = os.getenv("LN_TERM_COLOR", "")
        self.always_color = self.always_color and self.always_color[:1] in "1yta"
        
        self.stdout_isatty = self.stdout.isatty()
        self.stdout_encoding = "utf-8" # we simply enforce this!

        import encodings
        self.stdin_decoder = encodings.utf_8.IncrementalDecoder()
        def _get_char_via_incremental_decoder():
            """ will return str of len 1"""
            while True:
                byte = os.read(self.stdin.fileno(), 1)
                if not byte:
                    self.debug("incremental-decoder: EOF!")
                    return ""
                if self.debug_enabled: self.debug("incremental-decoder: code-unit %r" % (byte, ))
                decoded = self.stdin_decoder.decode(byte, False)
                if decoded != "":
                    break
                self.debug("byte %r is kept in decoder, not yet deoced\n" % byte)
            return decoded
        self._get_char = _get_char_via_incremental_decoder

        self._init_decset()

        self.do_restore_prompt = True
        self.prompt_active = False
        self.input_queue = [] # contains str/unicode characters!
        self.cursor_pos = 0 # counts who many unicode characters are displayed in prompt-line!
        self._kill_ring = []
        self._kill_ring_pos = None
        self.had_eof = False

        self._possible_input_sequences = list(terminal.input_sequences.items())
        self._possible_input_sequences.sort(key=lambda s: len(s[1]))
        self._csi_sequences_rev = dict([(seq, name) for name, seq in terminal.csi_sequences.items()])
        self._isp = 0 # input sequence position
        self._isbl = [] # input sequence backlog
        self._isout = set()
        self._is_csi = False
        self.csi_reports = dict()
        self._wait_for_csi_report = False

        self._tab_callback = None
        self._resize_callback = None
        self._on_input = None
        self._is_first_tab = True

        if history_fn is None:
            self._history_fn = os.path.expanduser("~/.ln_manager_history")
        else:
            self._history_fn = history_fn
        self.history_read()
        
        self.need_restore_buffer = False
        if self.stdout_isatty:
            self.saved_attr = termios.tcgetattr(self.stdout.fileno())
            atexit.register(self.restore)

        self.rows, self.cols = self.get_rows_cols()
        signal.siginterrupt(signal.SIGWINCH, False)        
        signal.signal(signal.SIGWINCH, self._on_window_size_change)
        self.debug("self.stdout_isatty: %r" % self.stdout_isatty)

    def in_color(self, which, msg):
        if (not self.always_color and not self.stdout_isatty) or not which.strip():
            return msg
        seq = terminal.color_sequences[which]
        reset = terminal.color_sequences["reset"]
        return seq + msg + reset

    def _on_window_size_change(self, signo, stack):
        self.rows, self.cols = self.get_rows_cols()
        if self._resize_callback:
            self._resize_callback(self.rows, self.cols)
            
    def set_tab_callback(self, cb):
        self._tab_callback = cb
    
    def set_resize_callback(self, cb):
        self._resize_callback = cb
    
    def restore(self):
        if not self.stdout_isatty:
            return
        if self.need_restore_buffer:
            self.disable_alternative_buffer()
        termios.tcsetattr(self.stdout.fileno(), termios.TCSANOW, self.saved_attr)
        
    def get_rows_cols(self):
        if not self.stdout_isatty:
            return None, None
        import fcntl
        import struct
        res = fcntl.ioctl(self.stdout.fileno(), termios.TIOCGWINSZ, b"\x00" * 4)
        return struct.unpack("hh", res)

    def write(self, what, flush=False):
        """
        `what` is to be expected bytes
        if `what` is not bytes, it will be encoded with stdout's encoding
        """
        if not isinstance(what, bytes):
            what = what.encode(self.stdout_encoding)
        #print("\nwrite: %r, %r" % (what, self.stdout_isatty))
        if self.stdout_isatty:
            os.write(self.stdout.fileno(), what)
        else:
            os.write(self.stdout.fileno(), what)
            if flush:
                self.stdout.flush()
        
    def _tty_write(self, what):
        if not self.stdout_isatty:
            return
        self.write(what, flush=True)
        
    def _esc_write(self, what):
        """
        write escape character followed by ``what``
        """        
        self._tty_write("\033%s" % what)
        
    def full_terminal_reset(self):
        self._esc_write("c")
        
    def _csi_write(self, what):
        """
        write control sequence introducer followed by ``what``
        """        
        self._esc_write("[%s" % what)
        
    def _osc_write(self, what):
        """
        write operating system control followed by ``what``
        """        
        self._esc_write("]%s" % what)

    def set_window_title(self, what, with_icon=False):        
        if with_icon:
            mode = 0
        else:
            mode = 2
        self._osc_write("%d;%s\x07" % (mode, what.encode(self.stdout_encoding)))

    def get_cursor_position(self):
        self.csi_reports["cursor-position"] = None
        self._wait_for_csi_report = True
        self._csi_write("6n")
        while self.csi_reports["cursor-position"] is None:
            Gtk.main_iteration_do(True)
        self._wait_for_csi_report = False
        return self.csi_reports["cursor-position"]
    
    def get_window_title(self):
        # doesn't seem to work on xterm, noting is reported!
        self._window_title = None
        self._csi_write("21t")
        while self._window_title is None:
            Gtk.main_iteration_do()
        return self._window_title

    def set_n_rows(self, rows):
        if rows < 24:
            raise Exception("rows must be atleast 24! (not %s)" % rows)
        self._csi_write("%dt" % rows)

    def set_scrolling_region(self, top=None, bottom=None):
        if top is None and bottom is None:
            # reset to full window
            self._csi_write("r")
        else:
            self._csi_write("%s;%sr" % (top, bottom))
        
    def set_x_property(self, prop, value=None):
        if value is None:
            self._osc_write("3;%s\x07" % prop) # delete property
        else:
            self._osc_write("3;%s=%s\x07" % (prop, value))
        
    def set_character_attribute(self, attr):
        self._csi_write("%sm" % (attr))
        
    def soft_terminal_reset(self):
        self._csi_write("!p")
        
    def cursor_back(self, n=1):
        self._csi_write("%dD" % n)
    def cursor_forward(self, n=1):
        self._csi_write("%dC" % n)
    def set_cursor(self, row, col):
        self._csi_write("%d;%dH" % (row, col))

    def _init_decset(self):
        self._decset = {
            "application cursor keys": 1, # Application Cursor Keys (DECCKM) 
            "decanm": 2, # Designate USASCII for character sets G0-G3 (DECANM), and set VT100 mode. 
            "132 column mode": 3, # 132 Column Mode (DECCOLM) 
            "smooth scroll": 4, # Smooth (Slow) Scroll (DECSCLM) 
            "reverse video": 5, # Reverse Video (DECSCNM) 
            "origin mode": 6, # Origin Mode (DECOM) 
            "wraparound mode": 7, # Wraparound Mode (DECAWM) 
            "auto-repeat keys": 8, # Auto-repeat Keys (DECARM) 
            "send mouse xy on button press": 9, # Send Mouse X & Y on button press. See the section Mouse Tracking. 
            "show toolbar": 10, # Show toolbar (rxvt) 
            "blinking cursor": 12, # Start Blinking Cursor (att610) 
            "print form feed": 18, # Print form feed (DECPFF) 
            "set print extent to full screen": 19, # Set print extent to full screen (DECPEX) 
            "show cursor": 25, # Show Cursor (DECTCEM) 
            "show scrollbar": 30, # Show scrollbar (rxvt). 
            "font-shifting functions": 35, # Enable font-shifting functions (rxvt). 
            "tektronix mode ": 38, # Enter Tektronix Mode (DECTEK) 
            "allow 80x132 mode ": 40, # Allow 80x132 Mode 
            "more(1) fix": 41, # more(1) fix (see curses resource) 
            "nation replacement character sets": 42, # Enable Nation Replacement Character sets (DECNRCM) 
            "margin bell ": 44, # Turn On Margin Bell 
            "reverse-wraparound mode ": 45, # Reverse-wraparound Mode 
            "logging": 46, # Start Logging (normally disabled by a compile-time option) 
            "alternate screen buffer": 47, # Use Alternate Screen Buffer (unless disabled by the titeInhibit resource) 
            "application keypad": 66, # Application keypad (DECNKM) 
            "backarrow key sends backspace": 67, # Backarrow key sends backspace (DECBKM) 
            "send mouse xy on button press and release": 1000, # Send Mouse X & Y on button press and release. See the section Mouse Tracking. 
            "hilite mouse tracking": 1001, # Use Hilite Mouse Tracking. 
            "cell motion mouse tracking": 1002, # Use Cell Motion Mouse Tracking. 
            "all motion mouse tracking": 1003, # Use All Motion Mouse Tracking. 
            "scroll to bottom on tty output": 1010, # Scroll to bottom on tty output (rxvt). 
            "scroll to bottom on key press": 1011, # Scroll to bottom on key press (rxvt). 
            "special modifiers for alt and numlock keys": 1035, # Enable special modifiers for Alt and NumLock keys. 
            "esc when meta modifies a key": 1036, # Send ESC when Meta modifies a key (enables the metaSendsEscape resource). 
            "del from the editing-keypad delete key": 1037, # Send DEL from the editing-keypad Delete key 
            "alternate screen buffer clear": 1047, # Use Alternate Screen Buffer (unless disabled by the titeInhibit resource) 
            "cursor in decsc": 1048, # Save cursor as in DECSC (unless disabled by the titeInhibit resource) 
            "cursor in decsc and use alternate screen buffer clear": 1049, # Save cursor as in DECSC and use Alternate Screen Buffer, clearing it first (unless disabled by the titeInhibit resource). This combines the effects of the 1 0 4 7 and 1 0 4 8 modes. Use this with terminfo-based applications rather than the 4 7 mode. 
            "sun function-key mode": 1051, # Set Sun function-key mode. 
            "hp function-key mode": 1052, # Set HP function-key mode. 
            "sco function-key mode": 1053, # Set SCO function-key mode. 
            "legacy keyboard emulation (x11r6)": 1060, # Set legacy keyboard emulation (X11R6). 
            "sun/pc keyboard emulation of vt220 keyboard": 1061, # Set Sun/PC keyboard emulation of VT220 keyboard. 
            "bracketed paste mode": 2004, # Set bracketed paste mode.
        }
    def dec_private_mode(self, which, enable=True):
        if enable:
            set_or_reset = "h"
        else:
            set_or_reset = "l"
        which = self._decset.get(which, which)
        self._csi_write("?%d%s" % (which, set_or_reset))
        
    def set_bracketed_paste_mode(self, en):
        self.dec_private_mode("bracketed paste mode", en)
        
    def enable_alternative_buffer(self):
        self.dec_private_mode("cursor in decsc and use alternate screen buffer clear", True)
        self.need_restore_buffer = True
        
    def disable_alternative_buffer(self):
        self.dec_private_mode("cursor in decsc and use alternate screen buffer clear", False)
        self.need_restore_buffer = False
        
    def disable_mouse_stuff(self):
        stuff = """
        send mouse xy on button press
        send mouse xy on button press and release
        hilite mouse tracking
        cell motion mouse tracking
        all motion mouse tracking
        bracketed paste mode
        """
        for thing in stuff.strip().split("\n"):
            thing = thing.strip()
            self.dec_private_mode(thing, False)
        
    def erase_in_line(self, n=0):
        """
        n = 0: clear from cursor to end of line
        n = 1: clear from cursor to beginning of line
        n = 2: clear entire line
        """
        self._csi_write("%dK" % n)
    def erase_in_display(self, n=0):
        """
        n = 0: clear from cursor to end of screen
        n = 1: clear from cursor to beginning of screen
        n = 2: clear entire screen
        n = 3: clear entire screen and delete all liens sved in scrollback buffer
        """
        self._csi_write("%dJ" % n)

    def trim_visible_length(self, line, first_visible_col, n_cols):
        output = []
        self.n_hidden_chars = 0
        self.n_visible_chars = 0
        def add_non_escape(what):
            #print "visible: %r" % what
            if self.n_hidden_chars < first_visible_col:
                to_hide = first_visible_col - self.n_hidden_chars
                if to_hide > len(what):
                    to_hide = len(what)
                what = what[to_hide:]
                self.n_hidden_chars += to_hide
            left_to_show = n_cols - self.n_visible_chars
            if len(what) > left_to_show:
                what = what[:left_to_show]
            self.n_visible_chars += len(what)
            output.append(what)
        def add_escape(what):
            #print "non-visible: %r" % what
            output.append(what)
        offset = 0
        while offset < len(line):
            p = line.find("\033[", offset)
            if p == -1:
                add_non_escape(line[offset:])
                break
            if p != offset:
                add_non_escape(line[offset:p])
            e = p + 2

            if line[e:e+6] == "?1049h":
                offset = e + 6
                add_escape(line[p:offset])
            else:
                while e < len(line) and line[e] not in "mH":
                    e += 1
                if e >= len(line):
                    raise Exception("unknown escape sequence at %r in line %r" % (line[p:p+10], line))
                offset = e + 1
                add_escape(line[p:offset])
                
        return "".join(output)
    
    def enable_echo(self, enabled):
        if not self.stdout_isatty:
            return
        iflag, oflag, cflag, lflag, ispeed, ospeed, cc = termios.tcgetattr(self.stdout.fileno())
        if enabled:
            lflag |= termios.ECHO
        else:
            lflag &= ~termios.ECHO
        new_attr = [iflag, oflag, cflag, lflag, ispeed, ospeed, cc]
        termios.tcsetattr(self.stdout.fileno(), termios.TCSANOW, new_attr)
    def set_interrupt_char(self, value):
        self.set_cc(termios.VINTR, value)
    def disable_control_chars(self):
        if not self.stdout_isatty:
            return
        iflag, oflag, cflag, lflag, ispeed, ospeed, cc = termios.tcgetattr(self.stdout.fileno())
        bell = b"\x00"
        cc[termios.VINTR] = bell
        cc[termios.VSTOP] = bell
        cc[termios.VSUSP] = bell        
        new_attr = [iflag, oflag, cflag, lflag, ispeed, ospeed, cc]
        termios.tcsetattr(self.stdout.fileno(), termios.TCSANOW, new_attr)
        
    def set_cc(self, which, value):
        if not self.stdout_isatty:
            return
        iflag, oflag, cflag, lflag, ispeed, ospeed, cc = termios.tcgetattr(self.stdout.fileno())
        cc[which] = value
        new_attr = [iflag, oflag, cflag, lflag, ispeed, ospeed, cc]
        termios.tcsetattr(self.stdout.fileno(), termios.TCSANOW, new_attr)
    def enable_canonical(self, enabled):
        if not self.stdout_isatty:
            return
        iflag, oflag, cflag, lflag, ispeed, ospeed, cc = termios.tcgetattr(self.stdout.fileno())
        if enabled:
            lflag |= termios.ICANON
        else:
            lflag &= ~termios.ICANON
            cc[termios.VMIN] = 1
            cc[termios.VTIME] = 0
        new_attr = [iflag, oflag, cflag, lflag, ispeed, ospeed, cc]
        termios.tcsetattr(self.stdout.fileno(), termios.TCSANOW, new_attr)
        
    def output(self, what, flush=False):
        if not isinstance(what, bytes):
            what = what.encode(self.stdout_encoding) # assuming terminal can do utf-8
        self._hide_prompt()
        self.write(what, flush=self.prompt_active or flush)
        self._restore_prompt()
        
    def _hide_prompt(self):
        if not self.prompt_active:
            return
        # as first try, just clear this line
        #self.write("\r", flush=True)

        if not self.do_restore_prompt:
            n_back = self.cursor_pos
        else:
            n_back = self.cursor_pos + len(self.prompt_string)
        if n_back:
            self.cursor_back(n_back)
            
        self.erase_in_line(0)
        
        
    def _restore_prompt(self):
        if not self.prompt_active:
            return
        input_data = "".join(self.input_data)
        if not self.do_restore_prompt:
            if self.stdout_isatty:
                self.write("%s" % input_data, flush=True)
            return
        self.write("%s%s" % (self.prompt_string, input_data), flush=True)
        # move cursor back to self.cursor_pos
        n_back = len(input_data) - self.cursor_pos
        if n_back > 0:
            self.cursor_back(n_back)
        
    def _prompt_answer(self):
        input_data = "".join(self.input_data)
        self.prompt_active = False
        self.history_add()
        self.debug("giving prompt answer: %r" % input_data)
        self.prompt_answer(input_data)

    def prompt_answer(self, input_data):
        """
        input_data is expected to be unicode str
        """
        self.output("please overwrite the terminal.prompt_answer(line) method: %r\n" % input_data)
        
    def prompt(self, prompt=""):
        self.input_data = [] # contains collected characters from input (str/unicode)
        self.prompt_active = True
        self.prompt_string = prompt
        self.cursor_pos = 0
        self.debug("prompt is now active!\n")
        if prompt:
            self.write(prompt.encode(self.stdout_encoding), flush=True)
        
        # replay stored input:
        while self.input_queue:
            ch = self.input_queue.pop(0)
            self._read_char(ch)
            if not self.prompt_active:
                return

        if self.had_eof:
            self.debug("already saw EOF and input queue is empty... command quit!")
            self.prompt_answer("quit")
            return

    def debug(self, msg):
        if not self.debug_enabled:
            return
        self.output("DBG: %s\n" % msg)

    def stop_reader(self):
        self._reader.keep_running = False
        print("press enter to stop input reader!")
        self._reader.join()
        print("reader stopped.")
    def start_reader(self, method="GLib"):
        self._reader_input_queue = queue.Queue()
        class terminal_reader(threading.Thread):
            def __init__(self, term):
                self.term = term
                threading.Thread.__init__(self)
                self.daemon = True
                self.keep_running = True
            def run(self):
                while self.keep_running:
                    data = self.term._get_char()
                    #self.term.debug("reader: got %r from stdin" % data)
                    self.notify(data)
                    if not data:
                        #self.term.debug("...terminal reader thread exiting")
                        break
        self._reader = terminal_reader(self)
        if method == "GLib":
            def GLib_input_in_main_thread():
                chars = []
                while True:
                    try:
                        char = self._reader_input_queue.get(block=False)
                    except Exception:
                        break
                    chars.append(char)
                if chars:
                    self.read_input(chars)
                return False
            def notify_via_GLib(data):
                was_empty = self._reader_input_queue.empty()
                self._reader_input_queue.put(data)
                self.debug("notify about new input: %r" % (data, ))
                #if was_empty: todo: needed for race with py2 on jenkins?
                GLib.idle_add(GLib_input_in_main_thread)
            self._reader.notify = notify_via_GLib
        else:
            raise Exception("unknown reader notification method %r!" % method)
        self._reader.start()

    def read_input(self, data):
        """
        data is expected to be one or more str(/unicode) characters
        """
        #if self.debug_enabled:
        #    import traceback
        #    self.output("".join(traceback.format_stack()))
        self.debug("read_input: %r" % (data, ))
        for char in data:
            if not char:
                if not self.stdin.isatty():
                    self.had_eof = True
                    self.debug("saw EOF on stdin!")
                if self.prompt_active:
                    self.debug("sending quit as prompt answer!")
                    self.prompt_answer("quit")
                return
            self._read_char(char)
        if hasattr(self, "_after_terminal_hook"):
            self._after_terminal_hook()
    
    def check_for_input_sequence(self, ch):
        if self._is_csi:
            # automatic parsing of csi input!

            # derived rules for end of input-sequence:
            #  - uppercase char!
            #  - a tilde "~"!
            #  - lower case char? (presumibly after ? intro)

            self._csi_input.append(ch)
            finished = (
                (ch.isalpha() and ch.isupper())
                or ch == "~"
                or (ch.isalpha() and ch.islower()))
            if not finished:
                self.debug("csi not finished: %r, %r" % (ch, ch.isalpha()))
                return ("want-more", )
            seq = "".join(self._csi_input)
            self._csi_input = []
            self._is_csi = False
            detected = self._csi_sequences_rev.get(seq)
            self.debug("csi seq: %r, detected: %r" % (seq, detected))
            if detected is not None:
                return ("match", detected)

            report_type = terminal.csi_reports.get(seq[-1:])
            self.debug("csi report seq: %r, type: %r" % (seq[-1:], report_type))
            if report_type is not None:
                seq = seq[:-1]                    
                if report_type == "window-state":
                    seq = seq.split(";")
                    window_state_types = {
                        1: ("window-iconified", lambda v: False),
                        2: ("window-iconified", lambda v: True),
                        3: ("window-position", lambda v: list(map(int, v[1:]))),
                        4: ("window-pixel-size", lambda v: list(map(int, v[1:]))),
                        8: ("window-text-chars", lambda v: list(map(int, v[1:]))),
                        9: ("window-screen-chars", lambda v: list(map(int, v[1:]))),
                    }
                    decoder = window_state_types.get(int(seq[0]))
                    if decoder is not None:
                        report_type = decoder[0]
                        value = decoder[1](seq)
                    else:
                        value = seq
                    self.csi_reports[report_type] = value
                elif report_type == "cursor-position":
                    self.csi_reports[report_type] = list(map(int, seq.split(";")))
                else:
                    self.csi_reports[report_type] = seq
                return ("match", ("csi-report", report_type, self.csi_reports[report_type]))
            return ("match", ("csi", seq))
        
        self._isbl.append(ch)
        #self.output("isbl: %r, pos: %s\n" % (self._isbl, self._isp))
        for n, s in self._possible_input_sequences:
            if n in self._isout:
                continue
            if len(s) > self._isp and s[self._isp:self._isp+1] == ch:
                self._isp += 1
                if self._isp == len(s): # complete match!
                    #self.output(" matched %r: %r\n" % (n, s))
                    self._isbl = []
                    self._isout = set()
                    self._isp = 0
                    if n == "csi":
                        self._is_csi = True
                        self._csi_input = []
                        return ("want-more", )
                    return ("match", n)
                return ("want-more", )
            else:
                self._isout.add(n)
        # no match, flush all!
        ret = "".join(self._isbl)
        self._isbl = []
        self._isout = set()
        self._isp = 0
        self._is_csi = False
        return ("no-match", ret)

    def history_read(self):
        self._before_history_input = None
        self._history = [] # contains str/uncode inputs
        try:
            with open(self._history_fn, "rb") as fp: # file stores utf-8
                for line in fp:
                    self._history.append(line.rstrip(b"\n").decode("utf-8"))
        except Exception:
            pass
        self._history_fp = open(self._history_fn, "a+b")
        self._history_pos = len(self._history)
    
    def history_add(self):
        data = "".join(self.input_data)
        self._before_history_input = None
        if (self._history and self._history[-1] == data) or data.strip() == "":
            self._history_pos = len(self._history)
            return
        self._history.append(data)
        self._history_fp.write(b"%s\n" % data.encode("utf-8"))
        self._history_fp.flush()        
        self._history_pos = len(self._history)

    def _set_input_data(self, data):
        """ `data` is expected to be str/unicode """
        if isinstance(data, bytes):
            data = data.decode(self.stdout_encoding)
        self.input_data = list(data)
    def history_up(self):
        if not self._history_pos:
            # nope
            self.bell()
            return
        self._history_pos -= 1
        if self._before_history_input is None and self.input_data and self._history_pos == len(self._history) - 1:
            self._before_history_input = "".join(self.input_data)
        
        self._hide_prompt()
        self._set_input_data(self._history[self._history_pos])
        self.cursor_pos = len(self.input_data)
        self._restore_prompt()
        
    def history_down(self):
        if self._history_pos >= len(self._history):
            # nope
            self.bell()
            return
        self._history_pos += 1
        self._hide_prompt()
        if self._history_pos == len(self._history):
            if self._before_history_input is None:
                self.input_data = [] # show empty line
            else:
                self._set_input_data(self._before_history_input)
                self._before_history_input = None
        else:
            self._set_input_data(self._history[self._history_pos])
        self.cursor_pos = len(self.input_data)
        self._restore_prompt()
        
    def bell(self):
        self._tty_write("\x07")
        
    def _read_char(self, ch):
        """
        `ch` is expected to be str/unicode
        """
        if not self.prompt_active and not self._wait_for_csi_report:
            if self.debug_enabled:
                self.debug("have char %r, prompt not active... store for later..." % ch)
                import time
                time.sleep(0.01)
            self.input_queue.append(ch)
            return
        if self.debug_enabled:
            self.debug("processing char %r" % ch)
        #print "\ngot char %r\n" % ch; return
        #  OSC l title ST
        
        match = self.check_for_input_sequence(ch)
        #print("\nmatch: %r" % (match, ))
        if match[0] == "want-more":
            self.debug("wait for more input sequence data...")
            return # assume part of invisible control char

        if self._wait_for_csi_report:
            self.debug("waiting for CSI report... ignore input!? match: %r" % (match, ))
            return
        
        if match[0] == "match":
            self.debug("have csi match: %r" % (match, ))
            match = match[1]

            if match[0] == "csi-report":
                self.output("got csi-report from terminal: %s\n" % (match[1:], ))
                return
            
            if match == "tab":
                self.process_tab()
                return
            else:
                self._is_first_tab = True

            if match == "Cc" and self._on_input == self.process_reverse_search:
                self.process_reverse_search_abort()
                return
            if match == "Cr":
                if self._on_input == self.process_reverse_search:
                    self.process_reverse_search_again()
                else:
                    self.process_reverse_search_start()
                return
            elif self._on_input == self.process_reverse_search:
                if match == "backspace":
                    self.process_reverse_search_backspace()
                    return
                self.process_reverse_search_done()

            if match == "backspace":
                if self.cursor_pos and self.input_data:
                    del self.input_data[self.cursor_pos - 1]
                    self.cursor_pos -= 1
                    to_write = "".join(self.input_data[self.cursor_pos:]) + " "
                    self.cursor_back(1)
                    self.write(to_write)
                    self.cursor_back(len(to_write))
                else:
                    self.bell()
                    
            elif match == "delete":
                if self.cursor_pos < len(self.input_data):
                    del self.input_data[self.cursor_pos]
                    to_write = "".join(self.input_data[self.cursor_pos:])
                    self.write(to_write)
                    self.erase_in_line(0)
                    if to_write:
                        self.cursor_back(len(to_write))
                else:
                    self.bell()

            elif match == "cursor_left":
                if self.cursor_pos:
                    self.cursor_back(1)
                    self.cursor_pos -= 1
                else:
                    self.bell()
            elif match == "cursor_right":
                if self.cursor_pos < len(self.input_data):
                    self.cursor_forward(1)
                    self.cursor_pos += 1
                else:
                    self.bell()
                
            elif match == "cursor_up":
                self.history_up()
            elif match == "cursor_down":
                self.history_down()

            elif match == "Cc":
                self.write("\n", flush=True)
                self.input_data = []
                self.cursor_pos = 0
                self._restore_prompt()
                
            elif match == "Ck":
                # rest of line in kill ring!
                to_write = "".join(self.input_data[self.cursor_pos:])
                self._kill_ring.append(to_write)
                self.input_data = self.input_data[:self.cursor_pos]
                self.erase_in_line(0)
                    
            elif match == "Cl":
                # redraw
                self._hide_prompt()
                self._restore_prompt()

            elif match == "Cd":
                self._hide_prompt()
                self._set_input_data("quit")
                self.cursor_pos = 0
                self._restore_prompt()
                self.stdout.write("\n")
                self.prompt_answer("quit")

            elif match == "cursor_pos1" or match == "Ca":
                # begin of line
                if self.cursor_pos:
                    self.cursor_back(self.cursor_pos)
                    self.cursor_pos = 0
            elif match == "cursor_end" or match == "Ce":
                # end of line
                n_fwd = len(self.input_data) - self.cursor_pos
                if n_fwd:
                    self.cursor_forward(n_fwd)
                    self.cursor_pos = len(self.input_data)
                
            else:
                self.output("not interpreted input sequence: %s\n" % (match, ))
            return
        
        # assume non-match visible chars!
        echo_input = self.stdout_isatty
        self.debug("assuming non-csi visible character: %r" % match[1])
        for i in range(len(match[1])):
            ch = match[1][i:i+1]
            if self._on_input:
                self.debug("calling _on_input %r for %r" % (self._on_input, ch))
                if self._on_input(ch):
                    continue
                else:
                    return
            if echo_input:
                self.write(ch, flush=True)
            if ch == "\n": # \n should not be in our input sequences!
                self.debug("calling _prompt_answer")
                self._prompt_answer()
                return
            if self.cursor_pos == len(self.input_data): # at end of line # todo
                self.input_data.append(ch)
            else:
                # within line
                self.input_data.insert(self.cursor_pos, ch)
                if echo_input:
                    to_redraw = "".join(self.input_data[self.cursor_pos + 1:])
                    self.write(to_redraw)
                self.cursor_back(len(to_redraw))
            self.cursor_pos += 1 # assume visible char!
            
    def process_reverse_search_start(self):
        self._hide_prompt()
        self.prompt_string_before_history_search = self.prompt_string, self.input_data, self.cursor_pos
        self.prompt_string = "(reverse-i-search)`': "
        self._restore_prompt()
        self._on_input = self.process_reverse_search
        self._last_search_result = "".join(self.input_data)
        self._search_term = []
        self._last_search_pos = len(self._history)
        
    def process_reverse_search(self, ch):
        if ch == "\n":
            self.process_reverse_search_done()
            self.stdout.write("\n")
            self._prompt_answer()
            return False

        self._search_term.append(ch)

        return self.process_reverse_search_again(allow_same=True)

    def process_reverse_search_backspace(self):
        if self._search_term:
            del self._search_term[-1]
        if self._search_term:
            self.process_reverse_search_again(allow_same=True)
    
    def process_reverse_search_again(self, allow_same=False):
        search_term = "".join(self._search_term)
        mode = "reverse-i-search"
        p = self._last_search_pos
        #self.output("start search at %d\n" % p)
        while p > 0:            
            if p == len(self._history):
                p -= 1
            #self.output(" check search at %d\n" % p)
            f = self._history[p].find(search_term)
            if f != -1 and (allow_same or self._last_search_pos != p):
                allow_same = False
                #self.output("found search term at %d\n" % p)
                self.cursor_pos = f
                self._last_search_result = self._history[p]
                self._set_input_data(self._last_search_result)
                self._last_search_pos = p
                break
            p -= 1
        else:
            mode = "failed reverse-i-search"
        self._hide_prompt()
        self.prompt_string = "(%s)`%s': " % (mode, search_term)
        self._restore_prompt()
        return True
        
    def process_reverse_search_done(self):
        self._hide_prompt()
        self._history_pos = self._last_search_pos
        self.prompt_string, input_data, cursor_pos = self.prompt_string_before_history_search
        self._restore_prompt()
        self._on_input = None

    def process_reverse_search_abort(self):
        self.write("\n", flush=True)
        self.prompt_string, input_data, cursor_pos = self.prompt_string_before_history_search
        self.input_data = input_data
        self.cursor_pos = len(input_data)
        self._restore_prompt()
        self._on_input = None
        
    def _output_table(self, items):
        if not self.stdout_isatty:
            for item in items:
                self.output("%s\n", item)
            return
        rows, cols = self.get_rows_cols()
        ml = 0
        for item in items:
            if len(item) > ml:
                ml = len(item)
        ml += 3
        n_cols = int((cols - 1) / ml)
        if n_cols < 1:
            n_cols = 1
        i = 0
        need_nl = False
        while i < len(items):
            for c in range(n_cols):
                if i == len(items):
                    break
                self.write("%-*.*s" % (ml, ml, items[i]))
                need_nl = True
                i += 1
            self.write("\n")
            need_nl = False
        if need_nl:
            self.write("\n")
        self._restore_prompt()
    
    def process_tab(self):
        if not self._tab_callback:
            return

        tab_command = self._tab_callback("".join(self.input_data), self.cursor_pos, self._is_first_tab)
        self._is_first_tab = False

        if not tab_command:
            return
        
        if tab_command[0] == "opts":
            self.write("\n") # keep original command line
            self._output_table(tab_command[1])
        elif tab_command[0] == "replace":
            replacement, new_cursor_pos = tab_command[1:]
            self._hide_prompt()
            self._set_input_data(replacement)
            self.cursor_pos = new_cursor_pos
            self._restore_prompt()
