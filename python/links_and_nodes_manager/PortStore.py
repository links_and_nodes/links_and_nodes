"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

class port(object):
    def __init__(self, daemon, direction, port_type, size, topic, is_client_port=False):
        self.daemon = daemon
        self.direction = direction
        self.port_type = port_type
        self.size = size
        self.is_client_port = is_client_port
        self.have_info = False
        self.topic = topic
        
        self.linux_tid = None
        self.thread_handle = None
        self.source_thread_started = False
        
        if self.direction == "source":
            self.destinations = []
            self.measured_rate = None
        if self.direction == "destination" or self.port_type == "shm":
            # shm is always source and destination!
            self.source = None
            self.rate = -1.0
        elif self.direction != "source":
            raise Exception("invalid port direction: %r" % direction)

        self.was_deleted = False

    def read_info(self, msg):
        self.have_info = True
        #print "port info from", msg
        if self.direction == "destination":
            # sink port
            self.packet_count = msg["count"]
            self.rate = msg["rate"]
            self.measured_source = msg["source"]
        else:
            # source port
            self.packet_count = msg["count"]
            self.measured_rate = msg["rate"]
            self.measured_destinations = msg.get("connected_sinks", [])
        if msg.get("src_source_thread_started"):
            self.linux_tid = msg.get("src_linux_tid")
            self.thread_handle = msg.get("src_thread_handle")
            self.source_thread_started = True
        else:
            self.linux_tid = msg.get("linux_tid")
            self.thread_handle = msg.get("thread_handle")
            self.source_thread_started = bool(msg.get("source_thread_started", False))

    def __str__(self):
        if self.port_type == "shm":
            port_name = self.name
        elif self.port_type in ("udp", "tcp"):
            port_name = str(self.port)
        else:
            port_name = ""
        return "<%s %s %r on %s>" % (self.port_type, self.direction, port_name, self.daemon.host)

    def __repr__(self):
        return self.__str__()

class shm_port(port):
    def __init__(self, daemon, direction, name, size, topic, buffers, shm_version, is_client_port=False, is_reliable=True):
        port.__init__(self, daemon, direction, "shm", size, topic, is_client_port=is_client_port)
        self.name = name
        self.buffers = buffers
        self._subscriptions = []
        self.have_publisher = False
        self.publisher_topic = None
        self.shm_version = shm_version
        self.is_reliable = is_reliable
        
    def add_subscription(self, sub):
        self._subscriptions.append(sub)
    def del_subscription(self, sub):
        if sub in self._subscriptions:
            self._subscriptions.remove(sub)
            self.check_remove()
    def have_subscriptions(self):
        return len(self._subscriptions) > 0
    def set_publisher(self):
        self.have_publisher = True
    def del_publisher(self):
        self.have_publisher = False
        self.check_remove()
    def check_remove(self):
        # is this source port used by local subscribers?
        if not self.have_subscriptions() and not self.have_publisher:
            self.daemon.queue_remove_port(self) # todo: cleanup transported ports!
        if self.publisher_topic:
            self.publisher_topic.check_del_topic()

    def get_id(self):
        if self.daemon.have_shm_version:
            return "shm", self.name, self.shm_version
        return "shm", self.name

class udp_port(port):
    def __init__(self, daemon, direction, address, size, topic, is_client_port=False):
        port.__init__(self, daemon, direction, "udp", size, topic, is_client_port=is_client_port)
        self.port = address
        self.interface = None

    def get_id(self):
        if self.direction == "source":
            return "udp", self.port # expect just port number
        return "udp", self.port[0], self.port[1] # expect hostname and port number

class tcp_port(port):
    def __init__(self, daemon, direction, address, size, topic, is_client_port=False):
        port.__init__(self, daemon, direction, "tcp", size, topic, is_client_port=is_client_port)
        self.port = address
        self.interface = None

    def get_id(self):
        if self.direction == "source":
            return "tcp", self.port # expect just port number
        return "tcp", self.port[0], self.port[1] # expect hostname and port number
            
