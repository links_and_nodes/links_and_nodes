"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

from __future__ import print_function

import os
import sys
import pyutils.line_assembler
import subprocess
import pprint
import traceback
import time
import socket
import platform
from . import config
import re
import inspect

from .Logging import debug, error, enable_logging_on_instance, is_at_least_log_level
from .py2compat import unicode, is_py2, inspect_getfullargspec

import gi
gi.require_version('GLib', '2.0')
from gi.repository import GLib # noqa: E402
from .tools import get_default_glib_mainloop # noqa: E402

def check_for_display_unix_socket(display):
    host, dpy = display.split(":", 1)
    is_local = host == ""
    if not is_local:
        return None
    if "." in dpy:
        dpy, screen = dpy.split(".", 1)        
    fn = "/tmp/.X11-unix/X%s" % dpy
    if not os.path.exists(fn):
        return None
    return fn

def get_max_shm_version_from_lib_version(library_version):
    if library_version < 12:
        return 1
    if library_version < 15:
        return 2
    return 3

def get_xauthority():
    XAUTHORITY = os.getenv("XAUTHORITY")
    if XAUTHORITY is None:
        home = os.path.expanduser("~")
        XAUTHORITY = os.path.join(home, ".Xauthority")
    return XAUTHORITY

def get_xcookie():
    cmd = "xauth list $DISPLAY"
    out = exec_cmd(cmd)
    line = out.strip()
    if not line:
        raise Exception("failed to get X11 Authorization entry for current display %s!" % os.getenv("DISPLAY"))
    line = line.split("\n")[0]
    try:
        display, type, hkey = re.split(" +", line)
    except Exception:
        raise Exception("invalid/unknown format of own X11 Authorization entry: %r (expect 'display type key')" % (line))
    debug("CommUtils", "got own X11 authorization entry / xcookie type %r hexkey %r" % (type, hkey))
    cookie = type, hkey
    return cookie

def escape_markup(value):
    if not isinstance(value, (str, unicode)):
        value = str(value)
    replaces = [
        ("<", "&lt;"),
        (">", "&gt;"),
        ("&", "&amp;"),
    ]
    for s, r in replaces:
        if s in value:
            value = value.replace(s, r)
    return value

def import_plugin_package(pkg_name):
    import imp
    module_name = os.path.join(pkg_name, "__init__.py")
    if module_name[0] not in "./":
        module_name = os.path.join(os.path.dirname(__file__), "plugins", module_name)
    if not os.path.isfile(module_name):
        raise Exception("gui_plugin file %r not found!" % module_name)
    with open(module_name, "r") as fp:
        pathname = os.path.realpath(module_name)
        description = (".py", "rb", imp.PY_SOURCE)
        mod = imp.load_module(pkg_name, fp, pathname, description)
    return mod

class tracked_object(object):
    instances = {}
    @staticmethod
    def get_instance(instance_id):
        return tracked_object.instances[instance_id]
    def update_instance(self, state):
        instance_id = self.get_instance_id()
        obj = self.instances.get(instance_id)
        if obj is None:
            self.instances[instance_id] = self
            return
        obj.__dict__.update(state)
    def __init__(self):
        self.instances[self.get_instance_id()] = self
    def __deinit__(self):
        instance_id = self.get_instance_id()
        if instance_id in self.instances:
            del self.instances[instance_id]

def get_stack():
    try:
        raise ZeroDivisionError
    except ZeroDivisionError:
        return sys.exc_info()[2].tb_frame.f_back

def ignore_cb(*args, **kwargs):
    return
    
class callback(object):
    class chain(object):
        def __init__(self, *cbs):
            self.cbs = cbs
        def __call__(self, *args, **kwargs):
            for cb in self.cbs:
                cb(*args, **kwargs)
        
    def __init__(self, fcn, *args, **kwargs):
        self.fcn = fcn
        self.args = args
        self.kwargs = kwargs
        self.creation_stack = get_stack() # "".join(traceback.format_stack()[:-1])        
    
    def __str__(self):
        return "cb %s(%s, %s)" % (self.fcn, repr(self.args)[1:-1], repr(self.kwargs)[1:-1])
    def __repr__(self):
        return str(self)

    def __call__(self, *args, **kwargs):
        try:
            if not args and not kwargs:
                args1, args2 = [], {}
                return self.fcn(*self.args, **self.kwargs)
            args1 = list(args)
            args1.extend(self.args)
            args2 = dict(self.kwargs)
            args2.update(kwargs)
            return self.fcn(*args1, **args2)
        except Exception:
            cstack = "".join(traceback.format_stack(f=self.creation_stack)[:-1])
            raise Exception("error while calling callback\n%s(%s, %s)\ncreated at:\n%s\ncall-stack:\n%s\nException:\n%s" % (
                str(self.fcn), repr(args1)[1:-1], repr(args2)[1:-1],
                cstack,
                "".join(traceback.format_stack()[:-1]),
                traceback.format_exc()))

class async_return(object):
    def __init__(self):
        self.value = None
        self.have_value = False

    def __call__(self, *args):
        self.value = args
        self.have_value = True

def sync_with_finish_callback(fcn):
    def _with_finish_cb_decorator(self, cb, *args, **kwargs):
        try:
            ret = fcn(self, *args, **kwargs)
        except Exception:
            cb(exception=str(sys.exc_info()[1]))
            return
        cb(ret)
    return _with_finish_cb_decorator

def format_exception():
    exc = "".join(traceback.format_exception_only(sys.exc_info()[0], sys.exc_info()[1]))
    return exc.strip()

_check_callbacks = os.getenv("LN_CHECK_CB", "0")[0].lower() in "1ty"
def needs_finish_callback(fcn):
    """
    functions decorated with this have a positional argument cb
    
    cb is a user-provided callback function. it must be of this type:
    fcn(ret=None, exception=None)
    only one of ret or exception will be non-None!
    if exception is not None, it has to be a valid Exception object that 
    could be used with "raise"

    function needs to call cb on its own! only if function itself throws
    an exception this wrapper will catch it and pass it via the exception 
    keyword argument

    return value of fcn has no meaning!
    """
    # find cb
    args = inspect_getfullargspec(fcn)
    cb_index = args.args.index("cb")

    def fcn_with_finish_cb(self, *args, **kwargs):
        if cb_index - 1 < len(args):
            cb = args[cb_index - 1]
        else:
            cb = kwargs["cb"]
        if _check_callbacks:
            if not callable(cb):
                raise Exception("called function %s with non-callable cb of %r!" % (fcn.__name__, cb))
            if isinstance(cb, callback):
                ccb = cb.fcn
                if ccb.__name__ == "fcn_with_finish_cb":
                    ccb = ccb.__closure__[1].cell_contents
            else:
                ccb = cb
            cb_args = inspect_getfullargspec(ccb)
            if not cb_args.varkw and "exception" not in cb_args.args:
                sfn = inspect.getsourcefile(ccb)
                lines, lineno = inspect.getsourcelines(ccb)
                raise Exception("callback %s(%s) does not have exception kwarg!\n%s:%s" % (ccb.__name__, ", ".join(cb_args.args), sfn, lineno))
            n_args = len(cb_args.args)
            if cb_args.defaults is None:
                n_defaults = 0
            else:
                n_defaults = len(cb_args.defaults)
            if n_defaults < n_args:
                if n_defaults:
                    no_kwargs = cb_args.args[:-n_defaults]
                else:
                    no_kwargs = cb_args.args
                if tuple(no_kwargs) != ("self", ):
                    error("@needs_finish_callback", "callback %s has non keyword arguments: %s!" % (ccb, no_kwargs))
        try:
            ret = fcn(self, *args, **kwargs)
        except Exception:
            exception = format_exception()
            error("needs_finish_callback", "exception calling %s(%s, %s):\n%s" % (fcn.__name__, args, kwargs, traceback.format_exc()))            
            try:
                return cb(exception=exception)
            except Exception:
                error("needs_finish_callback", "exception trying to report exception\n%r\nfor %s(%s, %s):\n%s" % (
                    exception, fcn.__name__, args, kwargs, traceback.format_exc()))
                raise
                
        if ret is not None:
            self.error("needs_finish_callback-member-function %r did return non-None!" % fcn.__name__)
    return fcn_with_finish_cb

        
def get_cpu_affinity_mask(cpu_id_list):
    if type(cpu_id_list) == int:
        return cpu_id_list
    if isinstance(cpu_id_list, (str, unicode)) and cpu_id_list.startswith("0x"):
        cpu_id_list = eval(cpu_id_list)
        if type(cpu_id_list) in (int, ):
            return cpu_id_list
    if isinstance(cpu_id_list, (str, unicode)):
        cpu_ids = [int(cpu_id.strip()) for cpu_id in cpu_id_list.split(",")]
    elif type(cpu_id_list) in (tuple, list):
        cpu_ids = cpu_id_list
    else:
        cpu_ids = [cpu_id_list]
    mask = 0
    for cpu_id in cpu_ids:
        mask = mask | 1 << cpu_id
    return mask

def is_ip(host):
    return host and host.replace(".", "").isdigit()

def get_network_ip(ip, netmask):
    zero_bits = 32 - netmask
    ip_words = list(map(int, ip.split(".")))
    w = len(ip_words) - 1
    while zero_bits >= 8:
        ip_words[w] = 0
        zero_bits -= 8
        w -= 1
    if zero_bits:
        zero_mask = (1 << zero_bits) - 1
        ip_words[w] = ip_words[w] & ~zero_mask
    return ".".join(map(str, ip_words))

def try_to_get_fqdn(ip_address):
    """
    parameters:
      `ip_address`: dotted-decimal ip-address or hostname or fqdn

    return value:
       either a found fqdn as returned by the name-resolver OR the input `ip_address` if no further name was found
    """
    if config.ln_debug:
        print("trying to lookup name for %r..." % ip_address)
    try:
        ret = socket.gethostbyaddr(ip_address)[0] # we are only interested in the `name`-part
        if config.ln_debug:
            print("trying to lookup name for %r...found %r" % (ip_address, ret))
        return ret
    except Exception:
        if config.ln_debug:
            print("trying to lookup name for %r...nothing found" % (ip_address))
        return ip_address

def try_to_get_ip(name):
    if config.ln_debug:
        print("trying to lookup ip for %r..." % name)
    try:
        ret = socket.gethostbyname(name)
        if config.ln_debug:
            print("trying to lookup ip for %r...found %r" % (name, ret))
        return ret
    except Exception:
        if config.ln_debug:
            print("trying to lookup ip for %r...nothing found" % (name))
        return name

class every(object):
    """
    this object "returns True" in the specivied interval of seconds.
    e = every(2) # means become true every 2 seconds
    while True:
        if e: # this calls the __nonzero__ operator and becomes true once every 2 seconds
            print "do something"
    """

    def __init__(self, seconds):
        self.interval = seconds
        self.last = time.time()
        self.start = self.last

    def __bool__(self):
        now = time.time()
        if self.last is None:
            self.last = now
            return True
        if now >= self.last + self.interval:
            self.last = now
            return True
        return False

    def duration(self):
        return time.time() - self.start

def exec_cmd(cmd, shell=True, exc_on_error=True, with_returncode=False):
    env = dict(os.environ)
    env["LANG"] = "C"
    p = subprocess.Popen(
        cmd,
        shell=shell,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        env=env
        )
    out, err = p.communicate()
    out = out.decode("utf-8")
    err = err.decode("utf-8")
    if err.strip() and exc_on_error:
        raise Exception("error executing command %r:\n%s" % (cmd, err))

    if not exc_on_error:
        if not with_returncode:
            return out, err
        return out, err, p.returncode

    if not with_returncode:
        return out
    return out, p.returncode

def ping(host, count=2, interval=0.2, timeout=1):
    if platform.system() == "Windows":
        import math
        interval = int(math.ceil(interval))
        cmd = ["ping", "-n", str(count), "-w", str(timeout*1000), host]
    else:
        cmd = ["ping", "-i", str(interval), "-c", str(count), "-w", str(timeout), host]
    out, err = exec_cmd(cmd, shell=False, exc_on_error=False)
    if err:
        error("ping", "error from ping:\n%s" % err)
        return False
    answers = 0
    for line in out.split("\n"):
        debug("ping", line)
        line = line.strip()
        if "bytes from" in line or ": Bytes=" in line:
            answers += 1
    return answers

def no_unicode_daemon_repr(data):
    if isinstance(data, (tuple, list)):
        # daemon does not care whether its list or tuple
        return "(%s)" % (",".join([no_unicode_daemon_repr(item) for item in data]))
    if isinstance(data, unicode):
        data = data.encode("utf-8")
    return repr(data)

def dict_to_msg(dict):
    if "__binary_fields" in dict:
        binary_fields = dict["__binary_fields"]
        del dict["__binary_fields"]
    else:
        binary_fields = []
    data = []
    for skey, val in list(dict.items()):
        key = skey.encode("utf-8")
        if skey in binary_fields:
            line = b"%s: data %d\n%s\n" % (key, len(val), val)
        elif not is_py2 and isinstance(val, bytes):
            # don't send b""-prefixed string...
            line = b"%s: %s\n" % (key, repr(val)[1:].encode("ascii"))
        else:
            if is_py2:
                # do not try to send unicode repr's to other side!
                val_repr = no_unicode_daemon_repr(val)
            else:
                val_repr = repr(val).replace("\n", " ").encode("utf-8")
            line = b"%s: %s\n" % (key, val_repr)
        data.append(line)
    data.append(b"\n")
    data = b"".join(data)
    #print("send message:\n%r\n" % data)
    return data

def msg_to_dict(msg):
    d = {}
    for line in msg:
        key, value = line.split(": ", 1)
        if not value.strip():
            #self.warning("ignore request-line without value: %r in request\n%s" % (line, "\n".join(msg)))
            continue
        if not is_py2 and value[-1:] == b"L" and value[:-1].isdigit():
            # this is a long with an 'L' postfix that py3 can no longer understand...
            value = value[:-1]
        try:
            d[key] = eval(value)
        except Exception:
            raise Exception("received invalid python expression for key\n%r\n%r\n%s\nin msg: %r" % (
                key, value,
                sys.exc_info()[1], msg))
    return d

class arbiter_connection(object):
    def __init__(self, host, port=None):
        self.host = host
        if port is None:
            self.port = config.DAEMON_PORT
        else:
            self.port = port
        self.fd = socket.socket()
        self.fd.settimeout(5)
        try:
            self.fd.connect((self.host, self.port))
        except Exception:
            raise Exception("can not connect to arbiter on host %s:%s %s" % (self.host, self.port, sys.exc_info()[1]))
        self.connected = True
        self.had_data = False
        self.la = pyutils.line_assembler(self._on_line)
        self.la.expect_bytes()
        self.io_watch = GLib.io_add_watch(self.fd, GLib.IO_IN, self._on_data)

    def query_daemon_port(self, uid=None):
        if not self.connected:
            raise Exception("arbiter not connected!")
        if uid is None:
            if platform.system() == "Windows":
                uid = 0
            else:
                uid = os.getuid()
        self.fd.send(b"query_daemon_port %r\n" % uid)
        line = self.wait_line(sync=True)
        msg = line.split(b" ", 1)
        arg = eval(msg[1])
        if msg[0] != b"ok":
            raise Exception("%s: %s" % (msg[0], arg))
        return arg

    def close(self):
        self.connected = False
        GLib.source_remove(self.io_watch)
        if hasattr(self, "fd"):
            self.fd.close()
            del self.fd

    def _on_data(self, fd, why):
        try:
            data = self.fd.recv(1024 * 2)
        except Exception:
            GLib.source_remove(self.io_watch)
            self.close()
            if self.had_data:
                # do not mark this as error when it could be a tcp-forwarded connection
                report_as = error
            else:
                report_as = debug
            report_as("arbiter_connection", "lost connection to arbiter:\n%s" % traceback.format_exc())
            return False
        if not data:
            GLib.source_remove(self.io_watch)
            self.connected = False
            raise Exception("lost connection to arbiter!")
        if not self.had_data:
            self.had_data = True
        self.la.write(data)
        return True

    def wait_line(self, sync=False):
        self._have_line = False
        while not self._have_line and self.connected:
            if sync:
                self._on_data(self.fd, GLib.IO_IN)
            else:
                get_default_glib_mainloop().get_context().iteration(True)
        if not self.connected:
            raise Exception("lost connection to arbiter while waiting for line!")
        return self._line

    def _on_line(self, line):
        self._line = line
        self._have_line = True

def describe_socket(sock):
    try:
        local = ":".join(map(str, sock.getsockname()))
        remote = ":".join(map(str, sock.getpeername()))
    except Exception:
        try:
            return "bad-sock-fd-%d" % sock.fileno()
        except Exception:
            return "bad-sock-%#x" % id(sock)
    if sock.type == socket.SOCK_STREAM:
        prefix = "tcp-"
    elif sock.type == socket.SOCK_DGRAM:
        prefix = "udp-"
    return "%ssocket %s -> %s" % (prefix, local, remote)

class CommunicationModule(object):
    ignore_requests = set(["get_last_packet", "get_state", "process_output", "send_stdin", "call_service", "retrieve_process_list"])

    def __init__(self, fd, owner, request_callback=None, *callback_args):
        self.owner = owner
        if hasattr(self.owner, "_log_subsystem"):
            owner_name = self.owner._log_subsystem
        elif hasattr(self.owner, "name"):
            owner_name = self.owner.name
        else:
            owner_name = str(self.owner)
        enable_logging_on_instance(self, owner_name)
        self.line_assembler = pyutils.line_assembler(self.got_line)
        self.line_assembler.expect_bytes()
        self.msg = {}
        self.binary_data_len = None
        self.message = ""
        self.callback_args = callback_args
        self.request_callback = request_callback
        self.fd = fd
        self._received_bytes = 0
        self.io_watch = GLib.io_add_watch(fd, GLib.IO_IN | GLib.IO_HUP, self.got_data)
        self.communication_status = "running"
        self._binary_eval_keys = set()

    def set_binary_eval_keys(self, keys):
        self._binary_eval_keys.update(keys)

    def disconnect(self, inform_owner=False):
        GLib.source_remove(self.io_watch)
        self.communication_status = "closed"
        self.do_inform_dead(self.fd, inform_owner=inform_owner)

    def _get_sorted_list(self, data, pprint=False):
        kl = list(data.keys())
        kl.sort()
        dbg_msg = []
        for to_front in "success,request,response,method,answer_to".split(","):
            if to_front not in kl:
                continue
            i = kl.index(to_front)
            del kl[i]
            kl.insert(0, to_front)
        if pprint and "__binary_fields" in data:
            binary_fields = data["__binary_fields"]
            #print("have binary_fields: %r" % (binary_fields, ))
        else:
            binary_fields = []
            
        for k in kl:
            if pprint:
                if k in binary_fields and data[k] is not None:
                    v = "data %d" % len(data[k])
                elif k == "process_list":
                    v = repr(data[k])[:80] + "...'"
                else:
                    v = repr(data[k])
                    if len(v) > 512:
                        v = v[:512] + "...'"
            else:
                v = repr(data[k])
            dbg_msg.append("  %s: %s" % (k, v))
        return dbg_msg

    def do_inform_dead(self, fd, inform_owner=True):
        sock_desc = describe_socket(fd)
        if fd:
            fd.close()
        self.communication_status = "closed"
        if inform_owner:
            if hasattr(self.owner, "inform_dead"):
                self.owner.inform_dead("lost connection to %s\n%s" % (self.owner, sock_desc))
            else:
                self.warning("owner %r has no inform_dead() method!")
        self.fd = None
        return False

    def send(self, **data):
        if (is_at_least_log_level("debug") 
            and data.get("request") not in self.ignore_requests
            and data.get("method") not in self.ignore_requests
            ):
            try:
                self.debug("sending to %s:\n%s" % (self.owner, "\n".join(self._get_sorted_list(data, pprint=True))))
            except Exception:
                self.error("failed to generate log message pretty printing this data:\n%s" % (pprint.pformat(data)))
        data = dict_to_msg(data)
        #print("send:\n%s\n" % data)
        try:
            data_sent = self.fd.send(data)
        except Exception:
            self.error("error sending data to %s:\n%s" % (self.owner, traceback.format_exc()))
            self.do_inform_dead(self.fd)
            raise
        if data_sent != len(data):
            self.error("wanted to send %d bytes but self.fd.send() returned only %d!" % (
                len(data), data_sent))

    def blocking_read(self):
        self.got_data(self.fd, GLib.IO_IN)
        
    def got_data(self, fd, why):
        try:
            data = fd.recv(1024*8)
            #print("got comm data: %r" % data)
            #self.debug("received data: %x, %r" % (why, data))
        except Exception:
            #self.error("exception in fd.recv():\n%s\n" % traceback.format_exc())
            #raise
            self.debug("got exception on socket recv - closing...\n%s" % traceback.format_exc())
            return self.do_inform_dead(fd)
            
        if data == b"":
            self.debug("got eof on fd %s!" % fd)
            return self.do_inform_dead(fd)
        self._received_bytes += len(data)
        if self.binary_data_len is not None:
            #print "still receiving binary data..."
            if len(data) >= self.binary_data_len:
                #print "last packet"
                self.binary_data.append(data[:self.binary_data_len])
                data = data[self.binary_data_len:]
                self._finish_binary_data()
            else:
                self.binary_data.append(data)
                self.binary_data_len -= len(data)
                #print "still need to receive %d bytes of binary data..." % (self.binary_data_len, )
                return True
        if not data:
            return True
        #print "write %d to line assembler" % len(data)
        self.line_assembler.write(data)
        return True
    
    def _finish_binary_data(self):
        self.msg[self.binary_data_key] = b"".join(self.binary_data)[:-1]
        #print "got binary data of length %d: %r" % (len(self.msg[self.binary_data_key]), self.msg[self.binary_data_key])
        self.binary_data_len = None
        del self.binary_data
    
    def got_line(self, line):
        #print("got line: %s" % line)
        if line:
            try:
                key, value = line.split(b": ", 1)
            except Exception:
                self.error("invalid request-line without separator: %r in request\n%s" % (line, pprint.pformat(self.msg)))
                return True
            if not is_py2:
                key = key.decode("utf-8") # key's are known to be utf-8
            if not value.strip():
                self.warning("ignore request-line without value: %r in request\n%s" % (line, pprint.pformat(self.msg)))
            elif value.startswith(b"data "):
                self.binary_data_len = int(value.split(b" ", 1)[1]) + 1 # daemon always puts newline after key-value pairs
                #print "receive binary data of length %d bytes\n" % self.binary_data_len
                self.binary_data = []
                self.binary_data.append(self.line_assembler.get_rest(self.binary_data_len))
                #print "already had %d bytes in line assembler buffer" % len(self.binary_data[0])
                self.binary_data_len -= len(self.binary_data[0])
                #print "self.binary_data_len left: %d" % self.binary_data_len
                self.binary_data_key = key
                if not self.binary_data_len:
                    # already finished
                    #print "finish data!"
                    self._finish_binary_data()
                    return True
                return False # dont process further lines
            else:
                if not is_py2 and key == "output" and value[:1] != "b":
                    value = b"b" + value
                if key in self._binary_eval_keys and (is_py2 and value[:1] != "b"):
                    value = b"b" + value
                if key == "user_real_name" and value[:1] not in ("b", b"b"):
                    value = b"b" + value
                if not is_py2 and value[-1:] == b"L" and value[:-1].isdigit():
                    # this is a long with an 'L' postfix that py3 can no longer understand...
                    value = value[:-1]                    
                try:
                    self.msg[key] = eval(value)
                except Exception:
                    self.error("received invalid python expression for key\n%r\n%r\n%s\nin msg: %r\nis_py2: %s, line: %r" % (
                        key, value,
                        sys.exc_info()[1],
                        self.msg,
                        is_py2,
                        line
                    ))
            return True
        # empty line -> request finished
        self.message = self.msg
        self.msg = {}
        
        if self.request_callback:
            #print str(self.request_callback)
            msg = self.message
            if (is_at_least_log_level("debug") 
                and msg.get("request") not in self.ignore_requests 
                and msg.get("response") not in self.ignore_requests
                and msg.get("method") not in self.ignore_requests
                and msg.get("answer_to") not in self.ignore_requests
            ):
                self.debug("received from %s:\n%s" % (self.owner.short_id(), "\n".join(self._get_sorted_list(msg, pprint=True))))
            try:
                self.request_callback(msg, *self.callback_args)
            except Exception:
                self.error("error calling request_callback %r(%r) for message:\n%s\n%s" % (
                    self.request_callback, self.callback_args,
                    pprint.pformat(msg),
                    traceback.format_exc()))
        
        return True     
