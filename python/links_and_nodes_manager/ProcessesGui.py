"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

import os
import time
import math
import signal
import pprint
import re
import socket
import datetime
import traceback

import numpy as np

from . import config
from . import ui_tools
from . import SystemConfiguration
from . import colorize

from .CommunicationUtils import escape_markup
from .Logging import enable_logging_on_instance
from .process_listing import process_list
from .process_output import process_output

import pyutils

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
gi.require_version('Pango', '1.0')
gi.require_version('Vte', '2.91')
gi.require_version('GObject', '2.0')
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import GObject, Gtk, Gdk, GLib, GdkPixbuf, Pango # noqa: E402
try:
    from gi.repository import Vte
    have_vte = True
except Exception:
    have_vte = False

def get_display_name(obj):
    if obj.display_name:
        return obj.display_name
    return obj.name

class ProcessesGui(pyutils.hooked_object):
    def __init__(self, gui):
        global resources_dir
        pyutils.hooked_object.__init__(self)
        enable_logging_on_instance(self, "ProcessesGui")
        self.state_buttons_enabled = False
        self.gui = gui
        self.manager_proxy = self.gui.manager_proxy

        self._tv_update_timer = None
        self.current_vte = None
        self.current_p = None
        self.process_lists = {}
        self.process_output_positions = {}
        self._save_last_pos_source = None
        self._watch_for_gdb_prompt = set()

        resources_dir = os.path.join(config.manager_base_dir, "resources")
        # load processes gui
        self.xml_fn = os.path.join(resources_dir, "ln_manager_processes.ui")
        self.xml = Gtk.Builder()
        self.xml.add_from_file(self.xml_fn)
        self.xml.connect_signals(self)

        self.scrolledwindow1.connect("realize", self.gui.on_resize)

        vb = Gtk.VBox()
        self.main_vbox.reparent(vb)
        self.gui.add_notebook_page("processes", vb, show=False)

        self.req_start_btn_img.set_from_icon_name("gtk-yes", Gtk.IconSize.BUTTON)

        self.model = Gtk.TreeStore(GObject.TYPE_PYOBJECT)
        self.tv.set_model(self.model) # cursor-changed-fine
        self.tv.set_enable_tree_lines(True)
        self.tv.set_search_column(0)
        def mycmp(m, col, key, iter):
            p = m[iter][col]
            if p.display_name.startswith(key):
                return False
            if p.name.startswith(key):
                return False
            return True
        self.tv.set_search_equal_func(mycmp)
        self.tv.set_enable_search(True)
        self.tv.connect("row-expanded", self.on_tv_row_expand)
        self.tv.connect("button-press-event", self.process_tree_button_press)
        # read pixbufs
        self.rotations = []
        for i in range(5):
            fn = "rotate%ds.png" % i
            pb = GdkPixbuf.Pixbuf.new_from_file(os.path.join(resources_dir, fn))
            self.rotations.append(pb)
        self.images = {}
        for c in "unknown",:
            self.images[c] = GdkPixbuf.Pixbuf.new_from_file(os.path.join(resources_dir, "%s.png" % c))

        #cr1 = gtk.CellRendererPixbuf()
        #cr1.set_property("stock-size", gtk.ICON_SIZE_MENU)

        #tvc = gtk.TreeViewColumn()
        cr2 = Gtk.CellRendererPixbuf()
        cr2.set_property("stock-size", Gtk.IconSize.MENU)
        cr2.set_property("xalign", 1)
        self._state_pixbuf = cr2
        #tvc.pack_start(cr2, True)
        #tvc.set_alignment(1)
        #tvc.set_cell_data_func(cr2, self.tv_pb_data_func, "state")
        #self.tv.append_column(tvc)

        #tvc = gtk.TreeViewColumn()
        cr = Gtk.CellRendererPixbuf()
        self._rotate_cr = cr
        #tvc.pack_start(cr, False)
        #tvc.set_cell_data_func(cr, self.tv_rotate_data_func)
        #self.tv.append_column(tvc)

        tvc = Gtk.TreeViewColumn()
        tvc.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)
        tvc.pack_start(cr2, False)
        tvc.pack_start(cr, False)

        cr = self._warning_cr = Gtk.CellRendererPixbuf()
        tvc.pack_start(cr, False)        
        cr = self._error_cr = Gtk.CellRendererPixbuf()
        tvc.pack_start(cr, False)
        cr = self._text_cr = Gtk.CellRendererText()
        tvc.pack_start(cr, True)
        tvc.set_cell_data_func(cr, self.tv_text_data_func)
        self.tv.append_column(tvc)
        #self.tv.set_grid_lines(gtk.TREE_VIEW_GRID_LINES_BOTH)
        #self.tv.set_expander_column(tvc)

        self.tv.set_headers_visible(False)

        cb = self.start_with_cb
        cb.set_model(self.start_with_model) # cursor-changed-fine
        cell = Gtk.CellRendererText()
        cb.pack_start(cell, True)
        cb.add_attribute(cell, 'text', 0)

        self.notifications_model = Gtk.TreeStore(GObject.TYPE_STRING, GObject.TYPE_STRING, GObject.TYPE_PYOBJECT)
        self.notifications_tv.set_model(self.notifications_model) # cursor-changed-fine
        tv = self.notifications_tv
        #cr = gtk.CellRendererPixbuf()
        #cr.set_property("stock-size", gtk.ICON_SIZE_MENU)
        #tv.insert_column_with_attributes(-1, "icon", cr, stock_id=0)
        #tv.insert_column_with_attributes(-1, "description", gtk.CellRendererText(), text=1)
        tvc = Gtk.TreeViewColumn()
        cr = Gtk.CellRendererPixbuf()
        self.notifications_tv_pb_cr = cr
        tvc.pack_start(cr, False)
        tvc.add_attribute(cr, "stock_id", 0)
        cr = Gtk.CellRendererText()
        tvc.pack_start(cr, False)
        tvc.add_attribute(cr, "text", 1)
        tv.append_column(tvc)
        
        self._process_state_da_size = None
        da = self.process_status_da
        da.connect("size-allocate", self.update_process_status_display_size_allocate)
        da.connect("draw", self.draw_process_status_display)
        da.add_events(Gdk.EventMask.BUTTON_PRESS_MASK)
        da.connect("button-press-event", self.process_status_display_button_press)
        da.connect("query-tooltip", self.process_status_display_query_tooltip)
        da.set_has_tooltip(True)
        self._da_size_allocate = None
        self.on_changed_color_scheme(gui, gui.color_scheme)

        self._da_pss = 15
        self.ignore_ignore_dep_change = False
        self._sb_adj = None
        self._update_all_like_idle = None
        self._update_all_like_set = set()
        self._update_process_log_list = []
        self.stdins_to_send = []
        self._timer_targets = {}
        self.ignore_process_command_changed = False
        
        b = self.log_tv.get_buffer()
        b.create_tag("red", foreground="red", weight=Pango.Weight.BOLD)
        b.create_tag("underlined", underline=Pango.Underline.SINGLE)

        self.log_tv.connect("button-press-event", self.on_log_tv_double_clicked)

        m = self.process_deps_model = Gtk.ListStore(GObject.TYPE_PYOBJECT, GObject.TYPE_STRING)
        tv = self.process_deps_tv
        tvc = self._process_deps_pb_tvc = Gtk.TreeViewColumn()
        cr = Gtk.CellRendererPixbuf()
        tvc.pack_start(cr, False)
        tvc.set_cell_data_func(cr, self.process_deps_data_func)
        tv.append_column(tvc)
        tv.insert_column_with_attributes(-1, "", Gtk.CellRendererText(), text=1)
        tv.set_model(m) # cursor-changed-fine

        m = self.process_dep_of_model = Gtk.ListStore(GObject.TYPE_PYOBJECT, GObject.TYPE_STRING)
        tv = self.process_dep_of_tv
        tvc = self._process_dep_of_pb_tvc = Gtk.TreeViewColumn()
        cr = Gtk.CellRendererPixbuf()
        tvc.pack_start(cr, False)
        tvc.set_cell_data_func(cr, self.process_deps_data_func)
        tv.append_column(tvc)
        tv.insert_column_with_attributes(-1, "", Gtk.CellRendererText(), text=1)
        tv.set_model(m) # cursor-changed-fine

        m = self.state_deps_model = Gtk.ListStore(GObject.TYPE_PYOBJECT, GObject.TYPE_STRING)
        tv = self.state_deps_tv
        tvc = self._state_deps_pb_tvc = Gtk.TreeViewColumn()
        cr = Gtk.CellRendererPixbuf()
        tvc.pack_start(cr, False)
        tvc.set_cell_data_func(cr, self.process_deps_data_func)# same as process
        tv.append_column(tvc)
        tv.insert_column_with_attributes(-1, "", Gtk.CellRendererText(), text=1)
        tv.set_model(m) # cursor-changed-fine

        m = self.process_dep_of_model
        tv = self.state_dep_of_tv
        tvc = self._state_dep_of_pb_tvc = Gtk.TreeViewColumn()
        cr = Gtk.CellRendererPixbuf()
        tvc.pack_start(cr, False)
        tvc.set_cell_data_func(cr, self.process_deps_data_func)
        tv.append_column(tvc)
        tv.insert_column_with_attributes(-1, "", Gtk.CellRendererText(), text=1)
        tv.set_model(m) # cursor-changed-fine

        GLib.timeout_add(1000, self._set_uptime)
        self._ignore_po_btn = False

        self.io_script_m = m = Gtk.ListStore(str)
        tv = self.io_script_tv
        tv.set_model(m)
        tv.append_column(Gtk.TreeViewColumn("line", Gtk.CellRendererText(), text=0))

        self.gui.add_hook("color_scheme_changed", self.on_changed_color_scheme)

    def connect(self):
        self.sysconf = self.gui.sysconf

        self._process_output_positions_fn = os.path.join(
            os.path.dirname(self.sysconf.configuration_file),
            ".%s_process_window_positions" % os.path.basename(self.sysconf.configuration_file)
            )
        self._load_process_output_positions()

        if self.sysconf.instance_config.documentation is not None:
            self.instance_help_btn.show()
        
        if self.sysconf.instance_config.vte_font is None:
            self._log_text_view_font = ui_tools.get_monospace_font_name(self.main_window, 9)
        else:
            self._log_text_view_font = self.sysconf.instance_config.vte_font
        self.set_font(self._log_text_view_font)

        if self.sysconf.instance_config.vte_font is None:
            self.vte_font_description = self.font_description
        else:
            vf = self.sysconf.instance_config.vte_font
            if not vf:
                self.vte_font_description = None
            else:
                self.vte_font_description = Pango.FontDescription(vf)


        m = self.display_type_cb_model = Gtk.ListStore(GObject.TYPE_STRING)
        self.display_type_cb.set_model(m) # cursor-changed-fine
        cb = self.display_type_cb
        cr = Gtk.CellRendererText()
        cb.pack_start(cr, True)
        cb.add_attribute(cr, "text", 0)
        m.append(("flat listing", ))
        m.append(("per physical host", ))
        m.append(("per logical node", ))
        m.append(("per group", ))
        if self.sysconf.groups:
            self.display_type_cb.set_active(3)
        else:
            self.display_type_cb.set_active(0)
        def notification_wrapper(callback):
            def inner_notification_wrapper(gui, msg):
                kwargs = msg
                if "request" in kwargs:
                    kwargs = dict(kwargs)
                    del kwargs["request"]
                try:
                    return callback(**kwargs)
                except Exception:
                    self.error("error in notification callback %r%s:\n%s" % (callback.__name__, msg, traceback.format_exc()))
                    raise
            return inner_notification_wrapper
        self.gui.add_hook(("notification", "update_obj_state"), notification_wrapper(self._obj_state_changed))
        self.gui.add_hook(("notification", "update_obj_requested_state"), notification_wrapper(self._obj_requested_state_changed))
        self.gui.add_hook(("notification", "gui_question"), notification_wrapper(self._gui_question))
        self.gui.add_hook(("notification", "process_started"), notification_wrapper(self._process_started))
        self.gui.add_hook(("notification", "process_output"), notification_wrapper(self._process_output))
        self.gui.add_hook(("notification", "new_process_warning"), notification_wrapper(self._new_process_warning))
        self.gui.add_hook(("notification", "update_state_state"), notification_wrapper(self._state_state_changed))
        self.gui.add_hook(("notification", "set_process_info"), notification_wrapper(self.set_process_info))
        self.gui.add_hook(("notification", "process_errors"), notification_wrapper(self._process_errors_update))
        self.gui.add_hook(("notification", "check_for_indirect_errors"), notification_wrapper(self._check_for_indirect_errors))
        self.gui.add_hook(("notification", "new_process_list"), notification_wrapper(self.new_process_list))
        self.gui.add_hook(("notification", "add_process"), notification_wrapper(self.add_process))
        self.gui.add_hook(("notification", "new_config"), notification_wrapper(self.on_new_config))
        self.gui.add_hook(("notification", "update_service"), notification_wrapper(self.on_update_service))
        self.gui.add_hook(("notification", "io_script_update"), notification_wrapper(self.on_io_script_update))

        self.update_process_status_display()

    def on_changed_color_scheme(self, gui, scheme):
        def parse(c):
            rgba = Gdk.RGBA()
            rgba.parse(c)
            ret = rgba.red, rgba.green, rgba.blue
            return ret
        self._da_gcs = {
            1: parse(scheme["ready"]),
            0.5: parse(scheme["starting"]),
            "orange": parse(scheme["warning"]),
            0: parse(scheme["stopped_or_error"]),
            -1: parse(scheme["inactive"]),
            -1.5: parse(scheme["unknown"]),
        }

        def transform_tpl(tpl_name, rgb): # this expects templates to be rgba!
            if not hasattr(self, "_pixbuf_color_cache"):
                self._pixbuf_color_cache = {}
            key = (tpl_name, tuple(rgb))
            pb = self._pixbuf_color_cache.get(key)
            if pb is not None:
                return pb
            if not hasattr(self, "_pixbuf_template_cache"):
                self._pixbuf_template_cache = {}

            # get numpy array
            tpl = self._pixbuf_template_cache.get(tpl_name)
            if tpl is None:
                # use pixbuf loader
                pb = GdkPixbuf.Pixbuf.new_from_file(os.path.join(resources_dir, "%s.png" % tpl_name))
                tpl = np.frombuffer(pb.get_pixels(), dtype=np.uint8).reshape((pb.get_height(), pb.get_width(), 4))
                self._pixbuf_template_cache[tpl_name] = tpl
            # colorize
            tpl = colorize.rgba_colorize(tpl, np.array(np.array(rgb) * 255, dtype=np.uint8))
            # back to pixbuf
            height, width, channels = tpl.shape
            rowstride = width * channels
            data = np.ascontiguousarray(tpl).tobytes()
            gbytes_data = GLib.Bytes.new(data)
            pb = GdkPixbuf.Pixbuf.new_from_bytes(gbytes_data, GdkPixbuf.Colorspace.RGB, True, 8, width, height, rowstride)
            self._pixbuf_color_cache[key] = pb
            return pb
        
        tpl_name = "proc_tpl"
        mapping = [ # for process display
            ("red", "stopped_or_error"),
            ("yellow", "starting"),
            ("green", "ready"),
            ("gray", "inactive"),
            # ("unknown",) always the same, its not just a color but a question mark
        ]
        for im_name, scheme_name in mapping:
            rgb = parse(scheme[scheme_name])
            self.images[im_name] = transform_tpl(tpl_name, rgb)
        mapping = [ # for state display
            (("UP", True),        "state_up_tpl", "ready"),
            (("UP", False),       "state_up_tpl", "stopped_or_error"),
            (("DOWN", True),      "state_dn_tpl", "ready"),
            (("DOWN", False),     "state_dn_tpl", "stopped_or_error"),
            ("state_unknown",    "state_unknown", "unknown"),
        ]
        for im_name, tpl_name, scheme_name in mapping:
            rgb = parse(scheme[scheme_name])
            self.images[im_name] = transform_tpl(tpl_name, rgb)

        self._update_process_req_start_stop_btn()
        self.tv.queue_draw()
        self.process_status_da.queue_draw()
        self.on_tv_cursor_changed(self.tv, force_update=True)
        return True

    def hide(self):
        self.log_nb.set_show_tabs(False)
        self.switch_notebook.hide()

    def on_log_tv_double_clicked(self, tv, ev):
        if ev.type != Gdk.EventType._2BUTTON_PRESS or ev.button != 1 or not ev.state & Gdk.ModifierType.CONTROL_MASK:
            return False
        x, y = tv.window_to_buffer_coords(Gtk.TextWindowType.TEXT, ev.x, ev.y)
        iter, line_start = tv.get_line_at_y(int(y))
        y, yo = tv.get_line_yrange(iter)
        iter2, line_start = tv.get_line_at_y(y + yo)
        b = tv.get_buffer()
        line = b.get_text(iter, iter2, True)
        # search for file and line-number information!
        if 'File "' in line and "line " in line and " in " in line:
            # python traceback line
            m = re.search("File .(.*?)., line (.*?), in", line)
            if m:
                fn, line_number = m.groups()
                if (not fn.startswith("/") or fn.startswith("./")) and self.current_p.change_directory is not None:
                    fn = os.path.join(self.current_p.change_directory, fn)
                line_number = int(line_number)
                ret = pyutils.system("emacsclient -n --socket-name server +%d '%s'" % (line_number, fn))
                if ret:
                    pyutils.system("emacs +%d '%s' &" % (line_number, fn))
        return True

    def set_font(self, font_name):
        self.log_tv.get_pango_context()
        self.font_description = Pango.FontDescription(font_name)
        self.log_tv.modify_font(self.font_description)

    def update_process_status_display_size_allocate(self, da, r):
        #print "size allocate:", r.x, r.y, r.width, r.height
        self._da_size_allocate = r.width, r.height
        self.update_process_status_display()

    def draw_rectangle(self, cr, color, filled, x, y, w, h):
        cr.set_source_rgb(*color)
        cr.rectangle(x, y, w, h)
        if filled:
            cr.fill()
        else:
            cr.stroke()
        
    def draw_process_status_display(self, da, cr):
        rows, cols = self._process_state_da_size
        s = self._da_pss
        p = 1
        pad = 3
            
        for row in range(rows):
            for col in range(cols):
                state = self._process_state_field[row, col]
                if state == -2:
                    break
                x = col * s
                y = row * s
                if state <= 1:
                    self.draw_rectangle(cr, self._da_gcs[state], True, x, y, s - p, s - p)
                elif state <= 2:
                    # with warning
                    state = state - 1
                    self.draw_rectangle(cr, self._da_gcs[state], True, x, y, s - p, s - p)
                    self.draw_rectangle(cr, self._da_gcs["orange"], True, x + pad, y + pad, s - p - pad * 2, s - p - pad * 2)
                elif state <= 3:
                    # with indirect error
                    state = state - 2
                    self.draw_rectangle(cr, self._da_gcs[state], True, x, y, s - p, s - p)
                    self.draw_rectangle(cr, self._da_gcs["orange"], True, x + pad, y + pad, s - p - pad * 2, s - p - pad * 2)
                else:
                    # with error
                    state = state - 3
                    self.draw_rectangle(cr, self._da_gcs[state], True, x, y, s - p, s - p)
                    self.draw_rectangle(cr, self._da_gcs[0], True, x + pad, y + pad, s - p - pad * 2, s - p - pad * 2)
        return True

    def process_status_display_find_process_at(self, x, y):
        s = self._da_pss
        col = int(x / s)
        row = int(y / s)
        rows, cols = self._process_state_da_size
        i = row * cols + col
        try:
            pn = self._psd_pns[i]
            return self.sysconf.processes[pn]
        except Exception:
            return
        
    def process_status_display_query_tooltip(self, da, x, y, keyboard, tt):
        p = self.process_status_display_find_process_at(x, y)
        if p:
            tt.set_text(p.name)
            return True
        return False
    
    def process_status_display_button_press(self, da, ev):
        p = self.process_status_display_find_process_at(ev.x, ev.y)
        if not p:
            return
        if ev.type == Gdk.EventType.BUTTON_PRESS and ev.button == 1:
            # select!
            self._select_process(p)
        if ev.type == Gdk.EventType._2BUTTON_PRESS and ev.button == 1:
            # start!
            self._select_process(p)
            if p.state not in ("started", "ready"):
                self._request_state(p, "start")
            else:
                self._request_state(p, "stop")

    def process_tree_button_press(self, widget, ev):
        model, row = self.tv.get_selection().get_selected()
        if not row:
            return
        p = self.model[row][0]
        if not p or not hasattr(p, "state"):
            return
        if ev.type == Gdk.EventType._2BUTTON_PRESS and ev.button == 1:
            # start!
            if p.state not in ("started", "ready"):
                self._request_state(p, "start")
            else:
                self._request_state(p, "stop")
        if ev.type == Gdk.EventType._2BUTTON_PRESS and ev.button == 3:
            # stop!
            self._request_state(p, "stop")
        if ev.type == Gdk.EventType._2BUTTON_PRESS and ev.button == 2:
            # re-start!
            if p.state in ("started", "ready"):
                self._request_stop_start(p)
            else:
                self._request_state(p, "start")

    def update_process_status_display(self):
        """
        redraw state-led's
        """
        da = self.process_status_da
        if self._da_size_allocate is None:
            return

        w, h = self._da_size_allocate
        pns = []
        for p in list(self.sysconf.processes.values()):
            if p.no_state_led:
                continue
            pns.append(p.name)
        N = len(pns)

        per_row = math.floor(w / self._da_pss)
        rows = int(math.ceil(N / per_row))
        cols = int(per_row)
        da.set_size_request(-1, rows * self._da_pss)

        new_shape = rows, cols
        if self._process_state_da_size != new_shape:
            self._process_state_da_size = new_shape
            self._process_state_field = np.ones((rows, cols)) * -2
        else:
            self._process_state_field[:] = -2

        pns.sort()
        self._psd_pns = pns
        for i, pn in enumerate(pns):
            p = self.sysconf.processes[pn]
            if p.no_state_led:
                continue

            if p.state and p.state.startswith("stopping"):
                state = 1
            elif (p.state == "started" and not p.has_ready_state()) or p.state == "ready":
                state = 1
                if len(p._errors): state += 3
                elif len(p._indirect_errors): state += 2
                elif p._warning_is_active: state += 1
            elif p.state == "started" and p.has_ready_state():
                state = 0.5
                if len(p._errors): state += 3
                elif len(p._indirect_errors): state += 2
                elif p._warning_is_active: state += 1
            elif (p.state is not None and "error" in p.state) or p.last_requested_state not in (None, "stop") or len(p._errors):
                state = 0
            elif p.state is None or p.state == "unknown":
                state = -1.5
            else:
                state = -1

            row = int(i / cols)
            col = i - (row * cols)
            self._process_state_field[row, col] = state
        da.queue_draw()

    def __getattr__(self, name):
        if hasattr(self, "xml"):
            widget = self.xml.get_object(name)
            if widget is not None:
                return widget
        raise AttributeError(name)

    def process_deps_data_func(self, tvc, cr, m, iter, data):
        p = m[iter][0]
        pb = self._get_pb_for_state(p)
        cr.set_property("pixbuf", pb)
        
    def tv_text_data_func(self, tvc, cr, m, iter, data):
        p = m[iter][0]
        pb = None
        cr = self._text_cr
        p_class_name = p.__class__.__name__
        is_flat_listing = self.display_type == "flat_listing"
        if type(p) is str:
            cr.set_property("markup", "<b>%s</b>" % (p, ))
        elif p_class_name == "Group":
            if p.display_name:
                n = p.display_name
            else:
                n = p.name
            cr.set_property("markup", "<b>%s</b>/" % (n, ))
        elif p_class_name == "State":
            if p.display_name and not is_flat_listing:
                n = p.display_name
            else:
                n = p.name
            cr.set_property("markup", "s: <b>%s</b> on %s" % (n, p.node))
        else:
            sn = p.host
            if sn is None:
                sn = "None"
            if p.display_name and not is_flat_listing:
                n = p.display_name
            else:
                n = p.name
            cr.set_property("markup", "<b>%s</b> on %s" % (escape_markup(n), escape_markup(sn)))
        stock_id = None
        if p_class_name == "Process" and p.state in ("started", "ready", "starting..."):
            all_deps_up = True
            #stock_id = "gtk-dialog-warning"
        is_process_or_state = p_class_name in ("Process", "State")
        if is_process_or_state and (p._warning_is_active or p._indirect_errors):
            stock_id = "edit-clear"
        self._warning_cr.set_property("icon-name", stock_id)
        self._warning_cr.set_property("stock-size", Gtk.IconSize.MENU)
        stock_id = None
        if is_process_or_state and p._errors:
            stock_id = "edit-clear" # a depends_on of this proc has a problem
            for r, dep in p._errors:
                if not r.startswith("dep"):
                    stock_id = "dialog-error"
                    break
                if r.startswith("depends_on_restart"):
                    stock_id = Gtk.STOCK_REFRESH # a dep. of this proc has a problem
                    break
        self._error_cr.set_property("icon-name", stock_id)
        self._error_cr.set_property("stock-size", Gtk.IconSize.MENU)
        
        #self.tv_pb_data_func
        pb = None
        if p_class_name == "Process":
            pb = self._get_pb_for_state(p)
        elif p_class_name == "State":
            pb = self._get_pb_for_state(p)
        self._state_pixbuf.set_property("pixbuf", pb)

        # self.tv_rotate_data_func
        pb = None
        if p_class_name == "Process":
            r = p.outputs_seen % len(self.rotations)
            pb = self.rotations[r]
        self._rotate_cr.set_property("pixbuf", pb)

    def states_match(self, req_state, state):
        stopped = set(["stopped", "stop", None])
        started = set(["started", "start", "ready", None])
        return (req_state in stopped and state in stopped) or (req_state in started and state in started)

    def tv_rotate_data_func(self, tvc, cr, m, iter):
        p = m[iter][0]
        pb = None
        if p.__class__.__name__ == "Process":
            r = p.outputs_seen % len(self.rotations)
            pb = self.rotations[r]
            cr.set_property("pixbuf", pb)
        else:
            cr.set_property("pixbuf", None)

    def _get_pb_for_state(self, p):
        if p is None:
            return None
        if p.__class__.__name__ == "State":
            req_state = p.requested_state
            if req_state is None:
                req_state = p.state
            if req_state not in ("UP", "DOWN"):
                pb = self.images["state_unknown"]
            else:
                pb = self.images[req_state, req_state == p.state]
            return pb
        state = p.state
        if state and state.startswith("stopping"):
            if p.has_ready_state() and p._was_ready:
                state = "ready"
            else:
                state = "started"

        if state == "starting...":
            pb_name = "yellow"
        elif state in ("start", "started"):
            if not p.has_ready_state():
                pb_name = "green"
            else:
                pb_name = "yellow"
        elif state == "ready":
            pb_name = "green"
        elif (p.state is not None and "error" in p.state) or p.last_requested_state not in (None, "stop"):
            pb_name = "red"
        elif p.state is None or p.state == "unknown":
            pb_name = "unknown"
        else:
            pb_name = "gray"

        #self.debug("state_color %r: %r(%s) %r" % (p.name, p.state, p.last_requested_state, pb_name))
        return self.images[pb_name]

    def tv_pb_data_func(self, tvc, cr, m, iter, what):
        p = m[iter][0]
        pb = None
        if p.__class__.__name__ == "Process":
            pb = self._get_pb_for_state(p)
        elif p.__class__.__name__ == "State":
            pb = self._get_pb_for_state(p)
        cr.set_property("pixbuf", pb)

    def redisplay(self):
        m = getattr(self, ("redisplay_%s" % self.display_type))
        #self.frame_label.hide()
        self.frame.set_sensitive(False)
        return m()

    def redisplay_flat_listing(self):
        m = self.model
        m.clear()
        pns = list(self.sysconf.processes.items())
        pns.sort(key=lambda p: p[1].name)
        parent = None
        for pn, p in pns:
            m.append(parent, (p, ))
        pns = list(self.sysconf.states.items())
        pns.sort(key=lambda p: p[1].name)
        for pn, p in pns:
            m.append(parent, (p, ))

    def redisplay_per_logical_node(self):
        m = self.model
        m.clear()
        parent = None
        nodes = {}
        pns = list(self.sysconf.processes.keys())
        pns.sort()
        for p in [self.sysconf.processes[k] for k in pns]:
            sn = p.node
            if sn is None:
                sn = "None"
            if sn not in nodes:
                nodes[sn] = [p]
            else:
                nodes[sn].append(p)
        pns = list(self.sysconf.states.keys())
        pns.sort()
        for p in [self.sysconf.states[k] for k in pns]:
            sn = p.node
            if sn not in nodes:
                nodes[sn] = [p]
            else:
                nodes[sn].append(p)
        nks = list(nodes.keys())
        nks.sort()
        for node in nks:
            parent = m.append(None, (node, ))
            for p in nodes[node]:
                m.append(parent, (p, ))

    def redisplay_per_physical_host(self):
        m = self.model
        m.clear()
        parent = None
        nodes = {}

        pns = list(self.sysconf.processes.keys())
        pns.sort()
        for p in [self.sysconf.processes[k] for k in pns]:
            host = p.host
            if host is None:
                host_name = "None"
            else:
                host_name = host.name
            if host_name not in nodes:
                nodes[host_name] = [p]
            else:
                nodes[host_name].append(p)
        pns = list(self.sysconf.states.keys())
        pns.sort()
        for p in [self.sysconf.states[k] for k in pns]:
            host = p.host
            if host.name not in nodes:
                nodes[host.name] = [p]
            else:
                nodes[host.name].append(p)
        nks = list(nodes.keys())
        nks.sort()
        for node in nks:
            parent = m.append(None, (node, ))
            for p in nodes[node]:
                m.append(parent, (p, ))

    def on_tv_row_expand(self, tv, iter, path):
        if self.display_type != "per_group":
            return False
        # check wether there are childs which also need to be expanded!
        m = self.model
        childs = m.iter_children(iter)
        while childs:
            g = m[childs][0]
            if hasattr(g, "members") and g.show_opened:
                tv.expand_row(m.get_path(childs), False)
            childs = m.iter_next(childs)
        return True
    def redisplay_per_group(self):
        m = self.model
        tv = self.tv
        m.clear()
        groups = []

        gns = list(self.sysconf.groups.keys())
        gns.sort()
        for gn in gns:
            g = self.sysconf.groups[gn]
            groups.append([gn, g, False]) # assume no parent
        
        for i, (gn, g, have_parent) in enumerate(groups):
            # check if this group is named as member in some other group
            for ogn, og, ohave_parent in groups:
                if ogn == gn:
                    continue
                if g in og.group_members:
                    have_parent = True
                    break
            groups[i][2] = have_parent
        # now just start with groups without parents and display childs
        def insert_groups(g, this_parent, seen=None):
            if seen is None:
                seen = [g]
            parent = m.append(this_parent, (g, ))
            for mm in g.group_members:
                if hasattr(mm, "members"):
                    if mm in seen:
                        continue
                    new_seen = list(seen)
                    new_seen.append(mm)
                    insert_groups(mm, parent, new_seen)
                else:
                    m.append(parent, (mm, ))
            if g.show_opened:
                tv.expand_row(m.get_path(parent), False)
        for gn, g, have_parent in groups:
            if have_parent:
                continue
            insert_groups(g, None)
        
    def on_display_type_cb_changed(self, cb):
        # todo: on change of display type record current expand/collapse position and restore
        # always try to show first match of currently selected process on display change!
        #self.display_type = cb.get_active_text().replace(" ", "_")
        act = cb.get_active()
        if act == -1:
            return
        self.display_type = self.display_type_cb_model[act][0].replace(" ", "_")
        self.redisplay()

    def on_tv_row_activated(self, tv, path, col):
        m = self.model
        obj = m[path][0]
        iter = m.get_iter(path)
        if m.iter_has_child(iter):
            if tv.row_expanded(path):
                tv.collapse_row(path)
            else:
                tv.expand_row(path, False)
        return True
            
    def _update_process_log_scrollbar(self):
        if self.follow_check.get_active():
            def do_adjustment():
                adj = self.log_sw.get_vadjustment()
                adj.set_value(adj.get_upper() - adj.get_page_size())
                self._sb_adj = None
                return False
            if self._sb_adj is None:
                self._sb_adj = GLib.idle_add(do_adjustment)

    def try_get_unicode_output(self, p, encoded):
        try:
            unicode_text = encoded.decode(p.output_encoding)
        except Exception:
            self.warning("process output of %r is not completely %s: %r!" % (p.name, p.output_encoding, encoded))
            unicode_text = encoded.decode(p.output_encoding, "replace")
        return unicode_text

    def set_process_log(self, p):
        if not hasattr(p, "output"): p.output = b"" # for gui
        unicode_text = self.try_get_unicode_output(p, p.output)

        if p.use_vte and have_vte:
            if not hasattr(p, "vte"):
                self.init_vte(p)
                p.vte.feed(unicode_text.encode("utf-8")) # only feed utf-8 to vte
            if unicode_text == "":
                p.vte.reset(True, True)
        else:
            b = self.log_tv.get_buffer()
            b.set_text(unicode_text)
        self._update_process_log_list = []
        self._update_process_log_scrollbar()

    def update_process_log(self, p, to_add, to_remove):
        """
        `to_add` is encoded bytes-string from process
        `to_remove` is number of bytes?? to remove from output
        """
        b = self.log_tv.get_buffer()
        #self.log_tv.set_buffer(None)
        end = b.get_end_iter()
        #self.info("add to buffer: %r, remove %d bytes" % (to_add, to_remove))
        if not hasattr(p, "_in_exception"):
            p._in_exception = False
        if len(to_add) and to_add[-1] == b"\n":
            line_list = to_add[:-1].split(b"\n")
        else:
            line_list = to_add.split(b"\n")
        # assume always one trailing newline in to_add!
        for line in line_list:
            #self.info(" line to add to buffer: %r" % line)
            if not p._in_exception and b"Traceback (most recent call last):" in line:
                p._in_exception = True
                p._in_exception_start = line.find(b"Traceback")
            elif p._in_exception and line[p._in_exception_start:].strip() == b"":
                p._in_exception = False                
            line_with_newline = line + b"\n"

            line_with_newline = self.try_get_unicode_output(p, line_with_newline)

            if p._in_exception:
                tags = ["red"]
                #if line.lstrip().startswith("File"):
                #    tags.append("underlined")
                b.insert_with_tags_by_name(end, line_with_newline, *tags)
            else:
                b.insert(end, line_with_newline)
        if to_remove:
            b.delete(b.get_start_iter(), b.get_iter_at_offset(to_remove)) # todo: buffer contains unicode, not bytes
        #self.log_tv.set_buffer(b)
        self._update_process_log_scrollbar()

    def has_present_window(self, p):
        if not hasattr(p, "clients") or not p.clients:
            return False
        # does it have a present_window service?
        for client in p.clients:
            for service in list(client.provided_services.values()):
                if service.interface == "ln/present_window":
                    # yes!
                    return True
        # no.
        return False

    def _update_process_req_start_stop_btn(self):
        self.req_stop_btn_image.set_from_pixbuf(self.images["red"])
        if self.current_p is None:
            self.req_start_btn_img.set_from_pixbuf(self.images["green"])
            return

        p = self.current_p
        if p.requested_state == "start":
            start_active = True
            stop_active = False
        elif p.requested_state == "stop":
            start_active = False
            stop_active = True
        else:
            start_active = False
            stop_active = False

        self.state_buttons_enabled = False
        self.req_start_btn.set_active(start_active)
        self.req_stop_btn.set_active(stop_active)
        self.state_buttons_enabled = True

        if start_active:
            self.req_start_btn_img.set_from_pixbuf(self.images["green"])
        else:
            if p.state in ("started", "ready"):
                self.req_start_btn_img.set_from_icon_name("gtk-refresh", Gtk.IconSize.BUTTON)
            else:
                self.req_start_btn_img.set_from_pixbuf(self.images["green"])

    def update_process_display(self, p=None, choose_first_page=False):
        if p is None:
            path, col = self.tv.get_cursor() # path-none-checked
            if not path:
                self.current_p = None
                return
            p = self.model[path][0]
        self.current_p = p
        self.update_warnings_model()
        self.log_nb.set_show_tabs(False) # was used for mlog
        self.log_nb.set_current_page(0)
        self.process_name.set_text(p.name)
        
        self.ignore_process_command_changed = True
        if p.command_override:
            cmd = p.command_override
        else:
            cmd = p.command
        if cmd is None:
            self.process_command.set_text("unknown")
        else:
            self.process_command.set_text(cmd)
        self.ignore_process_command_changed = False
        
        self.process_node.set_text("%s -> %s" % (p.node, p.host))
        #self.process_depends_on.set_text(", ".join(p.depends_on))
        #self.process_depends_on_restart.set_text(", ".join(p.depends_on_restart))
        self.process_state.set_text("unknown")

        self.process_cmd_box.set_property("visible", True)
        self.state_cmd_box.set_property("visible", False)

        self.ignore_ignore_dep_change = True
        self.ignore_dep_check.set_active(p.ignore_dep)
        self.enable_ln_debug_cb.set_active(p.ln_debug)
        
        # search matching start-tool
        cb = self.start_with_cb
        m = self.start_with_model
        m.clear()
        for entry in self.sysconf.start_tools:
            if type(entry) != tuple:
                m.append((entry, ))
            else:
                m.append((entry[0], ))
        if p.start_tool is None:
            cb.set_active(0)
        else:
            iter = m.get_iter_first()
            while iter:
                if m[iter][0] == p.start_tool:
                    cb.set_active_iter(iter)
                    break
                iter = m.iter_next(iter)
            else:
                cb.set_active(0)
        self.freeze_btn.set_active(p.frozen)
        self.follow_check.set_active(p.follow)

        self.ignore_ignore_dep_change = False

        self.req_stop_btn.set_sensitive(not p.disable_stop)
        self.ignore_dep_check.set_sensitive(len(p.depends_on) > 0 or len(p.depends_on_restart) > 0 or (hasattr(p, "start_before") and len(p.start_before) > 0))
        self.documentation_btn.set_property("visible", p.documentation is not None)
        self.bug_btn.set_property("visible", p.bugtracker_url is not None)

        if p.description is not None:
            self.description_label.set_text("invalid markup!")
            markup = re.sub("<br/?> ?", "\n", p.description)
            self.description_label.set_markup(markup)
            self.description_label.set_property("visible", True)
        else:
            self.description_label.set_property("visible", False)

        def update_state(img, label, state):
            if state is None:
                label.set_text("")
            else:
                label.set_text(state)
            if state == "start" or state == "started":
                if not p.has_ready_state():
                    si = self.images["green"]
                    is_ready = True
                else:
                    si = self.images["yellow"]
            elif state == "ready":
                si = self.images["green"]
                is_ready = True
            elif state == "stop" or state == "stopped" or state == "" or (state and "error" in state):
                si = self.images["red"]
            else:
                img.hide()
                si = None
            if si is not None:
                img.set_from_pixbuf(si)
                img.show()
        # state display
        update_state(self.process_state_img, self.process_state, p.state)
        # requested state display
        update_state(self.process_state_request_img, self.process_state_request, p.requested_state)
        self._update_process_req_start_stop_btn()
        if choose_first_page:
            self.process_table.set_current_page(0)

        self.is_client_btn.set_sensitive(bool(p.clients))
        self.show_gui_btn.set_sensitive(self.has_present_window(p))

        # fill dependencies
        self._fill_dependencies(p, self.process_deps_model)
        self._fill_dependency_ofs(p, self.process_dep_of_model)

        if p.pid is None:
            self.process_pid.set_text("")
        else:
            if hasattr(p, "daemon") and "vxworks" in p.daemon.architecture:
                pid = "0x%x" % p.pid
            else:
                pid = str(p.pid)
            self.process_pid.set_text(pid)
        self._set_uptime()
        has_window = hasattr(self.current_p, "process_output_window") and self.current_p.process_output_window and not self.current_p.process_output_window.hidden
        if has_window:
            self.current_p.process_output_window.update_process_command()
        self._ignore_po_btn = True
        self.show_po_btn.set_active(has_window)
        self._ignore_po_btn = False
        # update notifications
        m = self.notifications_model
        m.clear()
        def set_errors(parent_iter, p, depth=0):
            if depth > 20:
                return
            for reason, arg in p._errors:
                if reason.startswith("depends_on_restart"):
                    stock_id = Gtk.STOCK_REFRESH
                    t = "%s: %s: %s" % (p.name, reason, arg.name)
                    do_recurse = arg
                    click_target = p
                elif reason.startswith("depends_on"):
                    stock_id = "edit-clear"
                    t = "%s: %s: %s" % (p.name, reason, arg.name)
                    do_recurse = arg
                    click_target = p
                else:
                    stock_id = "dialog-error"
                    t = "%s: %s: %s" % (p.name, reason, arg)
                    do_recurse = None
                    click_target = p
                new_iter = m.insert(parent_iter, -1, (stock_id, t, click_target))
                if do_recurse is not None:
                    if set_errors(new_iter, do_recurse) == 0:
                        # this dep. has no errors!? propably because it is already restarted and we also need to restart...
                        # what is the state of the other process?
                        started = arg.state in ("ready", "started")
                        if started:
                            # other proc seems to be running - explain:
                            if reason == "depends_on_restart" and p._needs_restart:
                                # add explanationatory entry
                                m.insert(new_iter, -1, ("dialog-error", "%r has been started more recently than this one!" % arg.name, arg))
                        else:
                            # other proc is not running!
                            if arg.has_ready_state():
                                wanted_state = "ready"
                            else:
                                wanted_state = "started"
                            m.insert(new_iter, -1, ("dialog-error", "%r is not in %s state: %s!" % (arg.name, wanted_state, arg.state), arg))
            for op in p._indirect_errors:
                stock_id = "edit-clear"
                t = "%s: indirect: process %s has problems" % (p.name, op.name, )
                new_iter = m.insert(parent_iter, -1, (stock_id, t, p))
                set_errors(new_iter, op, depth=depth+1)

            return len(p._errors)
        set_errors(None, p)
        self.notifications_tv.expand_all()
        
        # handle vte display
        if p.use_vte and have_vte:
            if not hasattr(p, "vte"):
                self.init_vte(p)
            else:
                self.set_vte_style(p.vte)
            self.log_sw.set_property("visible", False)
            if self.current_vte != p.vte:
                if self.current_vte:
                    self.output_vbox.remove(self.current_vte)
                self.output_vbox.pack_start(p.vte, True, True, 0)
                p.vte.show_all()
                p.vte.set_size_request(100, 100)
                self.current_vte = p.vte
        else:
            self.log_sw.set_property("visible", True)
            if self.current_vte:
                self.output_vbox.remove(self.current_vte)
            self.current_vte = None

        if p.io_scripts:
            if getattr(self, "_gui_io_script_last_p", None) != p:
                self._gui_io_script_last_p = p
                self.io_script_cb.remove_all()
                activate = 0
                activate_script = None
                last_active = getattr(p, "_gui_io_script_last_active", None)
                executing_id = None
                executing_script = None
                p.io_script_cb_cache = dict()
                for i, (name, script) in enumerate(p.io_scripts.scripts.items()):
                    executing = script.is_executing(getattr(script, "_gui_mode", None))
                    self.io_script_cb.append(name, name)
                    if executing:
                        executing_id = i
                        executing_script = script
                    if name == last_active:
                        activate = i
                        activate_script = script
                    p.io_script_cb_cache[name] = i
                if executing_id is not None:
                    activate = executing_id
                    activate_script = executing_script
                self.io_script_cb.set_active(activate)
                self.update_io_script_display(activate_script)
                self.process_table.get_nth_page(3).show_all()
        else:
            self._gui_io_script_last_p = None
            self.process_table.get_nth_page(3).hide()

    def on_start_with_cb_changed(self, cb):
        sub = cb.get_active_iter()
        if sub is None:
            return True
        if self.current_p.start_tool == self.start_with_model[sub][0]:
            return True
        self.current_p.start_tool = self.start_with_model[sub][0]
        self.manager_proxy(request="set_process_attribute", pname=self.current_p.name, attribute="start_tool", value=self.current_p.start_tool)
        return True

    def _fill_dependencies(self, p, m):
        m.clear()
        if hasattr(p, "depends_on_restart"):
            deps = list(p.depends_on)
            deps.extend(p.depends_on_restart)
        else:
            deps = p.depends_on
        def others(this_one, all):
            for one in all:
                if one != this_one:
                    yield one
            
        def iter_deps(deps):
            for dep in deps:
                if type(dep) != list:
                    yield dep, dep
                else:
                    selected_provider, providers = dep
                    for alt_dep in providers:
                        selected_mark = "*" if alt_dep == selected_provider else ""
                        yield alt_dep, "%s%s or one of (%s)" % (alt_dep, selected_mark, ",".join(others(alt_dep, providers)))
        
        for dep, dep_name in iter_deps(deps):
            if dep in self.sysconf.processes:
                obj = self.sysconf.processes[dep]
            elif dep in self.sysconf.states:
                obj = self.sysconf.states[dep]
            elif dep[0] == "!" and dep[1:] in self.sysconf.states:
                obj = self.sysconf.states[dep[1:]]
            else:
                obj = None
            m.append((obj, dep_name))

    def _fill_dependency_ofs(self, p, m):
        m.clear()
        def depends_on_name(other, name):
            if name in other.depends_on:
                return True
            if hasattr(other, "depends_on_restart") and name in other.depends_on_restart:
                return True
            return False
        def depends_on(other):
            if depends_on_name(other, p.name):
                return True
            if p.__class__.__name__ == "State":
                if depends_on_name(other, "!" + p.name):
                    return True
            return False
        for state_name, state in self.sysconf.states.items():
            if depends_on(state):
                m.append((state, state_name))
        for process_name, process in self.sysconf.processes.items():
            if depends_on(process):
                m.append((process, process_name))

    def update_state_display(self, p=None, choose_first_page=False):
        if p is None:
            path, col = self.tv.get_cursor() # path-none-checked
            if not path:
                self.current_p = None
                return
            p = self.model[path][0]
        self.current_p = p
        self.state_name.set_text(get_display_name(p))

        self.process_cmd_box.set_property("visible", False)
        self.state_cmd_box.set_property("visible", True)

        self.ignore_process_command_changed = True
        for which in "check,up,down".split(","):
            ov = p.command_overrides.get(which)
            if ov is None:
                ov = getattr(p, which)
            if ov is None:
                ov = ""
            getattr(self, "state_%s_cmd" % which).set_text(ov)
        self.ignore_process_command_changed = False
        
        self.state_node.set_text("%s -> %s" % (p.node, p.host))
        self.state_current_state_label.set_text(p.state)
        if p.requested_state is None:
            self.state_requested_state_label.set_text("")
        else:
            self.state_requested_state_label.set_text(p.requested_state)

        # fill dependencies
        self._fill_dependencies(p, self.state_deps_model)
        self._fill_dependency_ofs(p, self.process_dep_of_model)

        def update_state(img, label, state):
            if state is None:
                label.set_text("")
            else:
                label.set_text(state)
            if state == "start" or state == "started":
                if not p.has_ready_state():
                    si = self.images["green"]
                else:
                    si = self.images["yellow"]
            elif state == "ready":
                si = self.images["green"]                
            elif state == "stop" or state == "stopped" or state == "" or (state and "error" in state):
                si = self.images["red"]
            else:
                img.hide()
                si = None
            if si is not None:
                img.set_from_pixbuf(si)
                img.show()
        # state display
        #update_state(self.process_state_img, self.process_state, p.state)
        # requested state display
        #update_state(self.process_state_request_img, self.process_state_request, p.requested_state)
        if p.requested_state == "UP":
            start_active = True
            stop_active = False
        elif p.requested_state == "DOWN":
            start_active = False
            stop_active = True
        else:
            start_active = False
            stop_active = False
        self.state_buttons_enabled = False
        self.state_req_up_btn.set_active(start_active)
        self.state_req_down_btn.set_active(stop_active)
        self.state_buttons_enabled = True

        # fill dependencies
        m = self.process_deps_model
        m.clear()
        deps = list(p.depends_on)
        for dep in deps:
            if dep in self.sysconf.processes:
                obj = self.sysconf.processes[dep]
            elif dep in self.sysconf.states:
                obj = self.sysconf.states[dep]
            else:
                obj = None
            m.append((obj, dep))

        has_window = hasattr(self.current_p, "process_output_window") and self.current_p.process_output_window and not self.current_p.process_output_window.hidden
        self._ignore_po_btn = True
        self.show_po_btn.set_active(has_window)
        self._ignore_po_btn = False

        # handle vte display
        self.log_sw.set_property("visible", True)
        if self.current_vte:
            self.output_vbox.remove(self.current_vte)
        self.current_vte = None

    def _set_uptime(self):
        if self.current_p is None:
            return True
        p = self.current_p
        if not hasattr(p, "start_time"):
            return True
        if p.start_time is None:
            uptime = None
        elif p.stop_time is None:
            uptime = time.time() - p.start_time
            uptime = uptime
        else:
            uptime = p.stop_time - p.start_time

        if uptime is not None:
            if uptime < 60:
                t = "%.0fs" % uptime
            elif uptime < 3600:
                t = "%02d:%02d" % (int(uptime / 60.), uptime % 60)
            elif uptime < 24 * 3600:
                t = "%02d:%02d:%02d" % (int(uptime / 3600.), int((uptime % 3600) / 60.), uptime % 3600 % 60)
            else:
                t = "%d days, %02d:%02d:%02d" % (
                    int(uptime / (3600. * 24.)),  # days
                    int(uptime % (3600 * 24) / 3600.),  # hours
                    int((uptime % (3600 * 24) % 3600) / 60.),  # minuets
                    uptime % (3600 * 24) % 3600 % 60) # seconds
        else:
            t = ""
        self.process_uptime.set_text(t)
        return True
    
    def on_is_client_btn_clicked(self, btn):
        self.gui.notebook.set_current_page(1)
        self.gui.cg._select_client(self.current_p.clients[0])
        return True

    def on_open_nb_btn_clicked(self, btn):
        p = self.current_p
        if p.notebook is not None:
            notebook_fn = p.notebook
        else:
            notebook_fn = os.path.join(self.sysconf.instance_config.default_notebook_path, p.name + ".py")
        teaser = """process_name = %r
%% clnt.name
""" % p.name
        self.gui.manager_proxy("open_notebook", notebook_fn, p.name, teaser)
        return True

    def on_tv_cursor_changed(self, tv, force_update=False):
        path, col = tv.get_cursor()
        if not path:
            # nothing selected!
            #self.frame_label.hide()
            self.frame.set_sensitive(False)
            self.switch_notebook.hide()
            return
        obj = self.model[path][0]
        if type(obj) == str:
            #self.frame_label.hide()
            #self.frame.set_sensitive(False)
            #return
            self.frame_label.set_markup("Node: <b>%s</b>" % obj)
            # is daemon connected?
            host = self.sysconf.get_host_for_node(obj)
            is_daemon_connected = self.manager_proxy(request="is_daemon_connected", host=host)
            if is_daemon_connected:
                daemon = self.manager_proxy(request="get_daemon", host=host)
            self.stop_daemon_btn.set_sensitive(is_daemon_connected)
            self.start_daemon_btn.set_sensitive(not is_daemon_connected)
            self.release_all_resources_btn.set_sensitive(is_daemon_connected and daemon.have_release_all_resources)
            self.edit_grp_cfg.hide()
            
            self.log_nb.set_show_tabs(False)
            self.start_daemon_btn.show()
            self.stop_daemon_btn.show()
            self.release_all_resources_btn.show()
            self.all_states_hbox.hide()
            self.switch_notebook.set_current_page(2)
            self.current_p = None
            self.current_group = obj
            b = self.log_tv.get_buffer()
            b.set_text("")

        elif obj.__class__.__name__ == "Group":
            self.log_nb.set_show_tabs(False)
            self.frame_label.set_markup("Group: <b>%s</b>" % get_display_name(obj))
            self.switch_notebook.set_current_page(2)
            self.start_daemon_btn.hide()
            self.stop_daemon_btn.hide()
            self.release_all_resources_btn.hide()
            self.edit_grp_cfg.show()
            # do we have states in this group?
            def has_states(obj):
                for member in obj.group_members:
                    if member.__class__.__name__ == "State":
                        return True
                    if member.__class__.__name__ == "Group":
                        return has_states(member)
                return False
            if has_states(obj):
                self.all_states_hbox.show()
            else:
                self.all_states_hbox.hide()
            self.current_p = None
            b = self.log_tv.get_buffer()
            b.set_text("")
        elif obj.__class__.__name__ == "State":
            self.log_nb.set_show_tabs(False)
            self.frame_label.set_markup("State: <b>%s</b>" % get_display_name(obj))
            self.switch_notebook.set_current_page(1)
            self.update_state_display(obj)
            self.set_process_log(obj)
        elif obj != self.current_p or force_update:
            n = get_display_name(obj)
            if len(n) > 25:
                n = n[:25] + "..."
            self.frame_label.set_markup("Process: <b>%s</b>" % n)
            self.switch_notebook.set_current_page(0)
            self.update_process_display(obj, choose_first_page=True)
            self.set_process_log(obj)
            if obj._warning_is_active and not obj.no_warning_window:
                obj._warning_dlg.present()
        #else:
        #    print "other object type class name: %r" % obj.__class__.__name__
        #self.frame_label.show()
        self.switch_notebook.show()
        self.frame.set_sensitive(True)

    def _get_current_obj(self):
        path, col = self.tv.get_cursor()
        if not path:
            return None
        return self.model[path][0]

    def _for_all_states(self, obj, what):
        for member in obj.group_members:
            if member.__class__.__name__ == "State":
                self._request_state(member, what)
            if member.__class__.__name__ == "Group":
                return self._for_all_states(member, what)

    def on_req_up_all_btn_clicked(self, btn):
        self._for_all_states(self._get_current_obj(), "UP")
    def on_req_check_all_btn_clicked(self, btn):
        self._for_all_states(self._get_current_obj(), "CHECK")
    def on_req_down_all_btn_clicked(self, btn):
        self._for_all_states(self._get_current_obj(), "DOWN")

    def _tv_update_timer_cb(self):
        self.tv.queue_draw()
        if not self._need_tv_update_timer:
            self._tv_update_timer = None
            return False
        self._need_tv_update_timer = False
        return True
        
    def _update_all_like(self, obj, parent=None, force=False):
        self._need_tv_update_timer = True
        if self._tv_update_timer is None:
            self._tv_update_timer = GLib.timeout_add(100, self._tv_update_timer_cb)
        return True

    def _select_process(self, obj, parent=None):
        iter = self.model.iter_children(parent)
        while iter:
            if self.model[iter][0] == obj:
                path = self.model.get_path(iter)
                self.tv.expand_to_path(path)
                self.tv.set_cursor(path, None)
                return True
            elif self.model.iter_has_child(iter):
                if self._select_process(obj, iter):
                    return True
            iter = self.model.iter_next(iter)
        return False

    def request_state(self, req_state, disable_gdb_run=False):
        path, col = self.tv.get_cursor()
        if not path:
            # nothing selected!
            return
        iter = self.model.get_iter(path)
        obj = self.model[iter][0]
        self._request_state(obj, req_state, disable_gdb_run=disable_gdb_run)

    def _request_state(self, obj, req_state, is_group_action=False, disable_gdb_run=False):
        if obj.__class__.__name__ == "State":
            self.manager_proxy(request="set_process_state_request", ptype=obj.__class__.__name__, pname=obj.name, requested_state=req_state)
            obj.requested_state = req_state
            self._update_all_like(obj)
            return
        if req_state == "stop":
            self._hide_warning(obj)
        if obj.requested_state == req_state:
            return
        if req_state == "start" and obj.start_tool and "gdb" in obj.start_tool and not obj.use_vte and have_vte:
            obj.use_vte = True
            self.set_process_log(obj)

        if not is_group_action and req_state == "start" and obj.state in ("started", "ready"):
            if obj.start_tool == "gdb" and not obj.disable_gdb_rerun:
                cmds = ["\003", "\003", "run\r", "y\r"]
                self.info("trying to do gdb re-run instead of restarting gdb for %r!\nsending to stdin: %r\nspecify disable_gdb_rerun process flag to disable this" % (
                    obj.name, cmds))
                self.queue_stdins_to_send(obj, cmds, 0.1)
                return
            req_state = "stop"
            obj.restart_requested = True
        else:
            obj.restart_requested = False
        if req_state == "start" and obj.state not in ("started", "ready") and obj.start_tool == "gdb" and not (obj.disable_gdb_run or disable_gdb_run):
            self._watch_for_gdb_prompt.add(obj.name)
            
        if req_state == "stop" and obj.__class__.__name__ == "Process" and obj.disable_stop:
            self.warning("stop disabled for %r" % obj)
            return
        obj.last_requested_state = req_state
        if obj.__class__.__name__ == "Process":
            self.manager_proxy(
                request="set_process_state_request",
                ptype=obj.__class__.__name__,
                pname=obj.name,
                requested_state=req_state,
                ln_debug=obj.ln_debug
            )
            if self.current_p == obj:
                if req_state == "stop":
                    obj._indirect_errors = []
                self.update_process_display(obj)
            self.propagate_process_update(obj)
        obj.requested_state = req_state
        self._update_all_like(obj)

    def propagate_process_update(self, p):
        has_window = hasattr(p, "process_output_window") and p.process_output_window
        if not has_window:
            return
        p.process_output_window.update_process()

    def _request_stop_start(self, obj):
        self.debug("request stop start %r" % obj.name)
        def delayed_start(self):
            if obj.state != "stopped":
                self.debug("not yet stopped: %r" % obj.state)
                return True
            self._request_state(obj, "start")
            return False
        if obj.state == "stop":
            return delayed_start()
        # first stop!
        req_state = "stop"
        self._hide_warning(obj)
        self.add_hook(("state_change", obj), delayed_start, "request_stop_start")
        self._request_state(obj, req_state)
            

    def group_action(self, action):
        path, col = self.tv.get_cursor()
        if not path:
            # nothing selected!
            return
        iter = self.model.get_iter(path)
        obj = self.model[iter][0]
        if type(obj) == str:
            self._node_action(obj, action)
        else:
            self._group_action(obj, action)

    def _group_action(self, obj, action):
        for member in obj.group_members:
            if member.__class__.__name__ == "Process":
                self._request_state(member, action, is_group_action=True)
            elif member.__class__.__name__ == "Group":
                self._group_action(member, action)
        
    def _node_action(self, node, action):
        processes = []
        if self.display_type == "per_physical_host":
            for pn, p in self.sysconf.processes.items():
                if p.host.name == node:
                    processes.append(p)
        if self.display_type == "per_logical_node":
            for pn, p in self.sysconf.processes.items():
                if p.node == node:
                    processes.append(p)
        for p in processes:
            self._request_state(p, action, is_group_action=True)
        
    # callbacks from user:
    def on_req_start_btn_pressed(self, btn, ev):
        self.start_btn_shift_pressed = bool(ev.state & Gdk.ModifierType.SHIFT_MASK)
        return False
    
    def on_req_start_btn_toggled(self, btn):
        if not self.state_buttons_enabled:
            return
        active = btn.get_active()
        if active:
            self.req_stop_btn.set_active(False)
            if self.freeze_btn.get_active():
                self.freeze_btn.set_active(False)
            self.request_state("start", disable_gdb_run=self.start_btn_shift_pressed)
        else:
            self.request_state(None)

    def on_req_stop_btn_toggled(self, btn):
        if not self.state_buttons_enabled:
            return
        active = btn.get_active()
        if active:
            self.req_start_btn.set_active(False)
            self.request_state("stop")
        else:
            self.request_state(None)

    def on_state_req_up_btn_toggled(self, btn):
        if not self.state_buttons_enabled:
            return
        active = btn.get_active()
        if active:
            self.state_req_down_btn.set_active(False)
            self.request_state("UP")
        else:
            self.request_state(None)
    def on_state_req_down_btn_toggled(self, btn):
        if not self.state_buttons_enabled:
            return
        active = btn.get_active()
        if active:
            self.state_req_up_btn.set_active(False)
            self.request_state("DOWN")
        else:
            self.request_state(None)
    def on_state_check_btn_clicked(self, btn):
        if not self.state_buttons_enabled:
            return
        self.request_state("CHECK")

    def on_req_start_all_btn_clicked(self, btn):
        self.group_action("start")

    def on_req_stop_all_btn_clicked(self, btn):
        self.group_action("stop")

    def on_log_tv_button_press_event(self, tv, ev):
        if ev.button == 3:
            self.log_tv_popup.popup(None, None, None, None, ev.button, ev.time)
            return True
        return False

    def _append_separator(self, p):
        line = "\n--- separator %s ---\n\n" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        if p.use_vte:
            line = line.replace("\n", "\r\n")
        self._append_output(p, line.encode(p.output_encoding))

    def on_log_tv_activate(self, item):
        #action = item.get_name()
        action = Gtk.Buildable.get_name(item)
        if action == "log_tv_clear" and self.current_p:
            self.current_p.output = b""
            self.current_p.output_lines = 0
            self.current_p._in_exception = False
            self.set_process_log(self.current_p)
            has_window = hasattr(self.current_p, "process_output_window") and self.current_p.process_output_window and not self.current_p.process_output_window.hidden
            if hasattr(self.current_p, "process_output_window"):
                self.current_p.process_output_window.set_process_log()
        if action == "log_tv_append_sep" and self.current_p:
            self._append_separator(self.current_p)
        if action == "copy_item" and self.current_p:
            p = self.current_p
            if p.use_vte and have_vte:
                p.vte.copy_clipboard_format(Vte.Format.TEXT)
            else:
                prim = Gtk.Clipboard.get(Gdk.SELECTION_PRIMARY) # might get ANY primary selection... good enough...
                text = prim.wait_for_text()
                cb = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
                cb.set_text(text, len(text))
        if action == "paste_item" and self.current_p:
            p = self.current_p
            if p.use_vte and have_vte:
                p.vte.paste_clipboard()
            else:
                cb = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
                text = cb.wait_for_text()
                if text:
                    self.manager_proxy("send_stdin", pname=p.name, text=text)

    def on_dump_start_btn_clicked(self, btn):
        obj = self.current_p
        self.manager_proxy(request="dump_startup_commands", ptype=obj.__class__.__name__, pname=obj.name)

    def on_ignore_dep_check_toggled(self, btn):
        if self.ignore_ignore_dep_change:
            return True
        obj = self.current_p
        obj.ignore_dep = btn.get_active()
        self.manager_proxy(request="set_process_ignore_dep", pname=obj.name, ignore_dep=obj.ignore_dep)
        return True

    def on_instance_help_btn_clicked(self, btn):
        doc = self.sysconf.instance_config.documentation
        self._show_doc(doc)
        
    def on_documentation_btn_clicked(self, btn):
        self._show_doc(self.current_p.documentation)
    def on_bug_btn_clicked(self, btn):        
        self._show_url(self.current_p.bugtracker_url)

    def _show_doc(self, doc):
        cmd = "xdg-open '%s'" % doc
        pyutils.system(cmd)

    # callbacks from manager:
    def _state_state_changed(self, state_name, host, up_down):
        s = self.sysconf.states[state_name]
        s.state = up_down
        #self.update_process_status_display()
        self._update_all_like(s)
        if self.current_p == s:
            self.update_state_display(s)
        return True

    def _obj_state_changed(self, obj_type, obj_name, obj_state):
        if obj_type == "Process":
            p = self.sysconf.processes[obj_name]
            p.state = obj_state
            if p.state == "ready":
                p._was_ready = True
            if p.state == "stopped":
                p._was_ready = False
                if p.restart_requested:
                    p.restart_requested = False
                    self._request_state(p, "start")

            self.update_process_status_display()
            self._update_all_like(p)

            path, col = self.tv.get_cursor()
            if path:
                iter = self.model.get_iter(path)
                self.model.row_changed(path, iter)
            if self.current_p == p:
                self.update_process_display(p)
            self.call_hook(("state_change", p))
            self.propagate_process_update(p)
        elif obj_type == "State":
            p = self.sysconf.states[obj_name]
            p.state = obj_state
            #self.update_process_status_display()
            self._update_all_like(p)
            path, col = self.tv.get_cursor()
            if path:
                iter = self.model.get_iter(path)
                self.model.row_changed(path, iter)
            if self.current_p == p:
                self.update_state_display(p)
            self.call_hook(("state_change", p))
        return True

    def _obj_requested_state_changed(self, obj_type, obj_name, obj_requested_state):
        if obj_type == "Process":
            p = self.sysconf.processes[obj_name]
            p.requested_state = obj_requested_state
            self._update_all_like(p)
            if self.current_p == p:
                self.update_process_display(p)
            self.propagate_process_update(p)
        elif obj_type == "State":
            p = self.sysconf.states[obj_name]
            p.requested_state = obj_requested_state
            self._update_all_like(p)
            if self.current_p == p:
                self.update_state_display(p)
        return True

    def _display_warning(self, p, line):
        p._warning_is_active = True
        self._update_all_like(p)
        self.update_process_status_display()
        if p.no_warning_window:
            return
        msg = (
            "WARNING: process <b>%s</b>:\n" + 
            "%s\n" +
            "please solve the problem and close this window!"
            ) % (
            p.name, 
            line
            )
        dlg = Gtk.MessageDialog(
                self.main_window, 
                Gtk.DialogFlags.DESTROY_WITH_PARENT, 
                Gtk.MessageType.WARNING,
                Gtk.ButtonsType.OK,
                message_format=msg
                )
        dlg.set_markup(msg)
        dlg.set_property("use-markup", True)
        def answer(dlg, response_id, p, self):
            self._hide_warning(p)
        dlg.connect("response", answer, p, self)
        p._warning_dlg = dlg
        dlg.show()

    def _hide_warning(self, p):
        if not p._warning_is_active:
            return
        if not p.no_warning_window and hasattr(p, "_warning_dlg"):
            p._warning_dlg.hide()
        p._warning_is_active = False
        # todo: remove warning mark!
        self._update_all_like(p)
        self.update_process_status_display()
        if not p.no_warning_window and hasattr(p, "_warning_dlg"):
            del p._warning_dlg

    def on_problems_cancel_clicked(self, btn):
        self.reload_problems_window.hide()
        self.manager_proxy(request="gui_question_answer", question="config_problems", apply=False)
    
    def on_problems_ok_clicked(self, btn):
        self.reload_problems_window.hide()
        self.manager_proxy(request="gui_question_answer", question="config_problems", apply=True)
        
    def _gui_question(self, question, q):
        self.debug("received gui question:\n%s" % pprint.pformat(q))
        if question == "ln_daemon_connect_error":
            hdr = "could not start the ln_daemon on <b>%s</b> for you.\n\n" % q["hostname"]
            if "is not pingable" in q["output"]:
                msg = (
                    "it looks like one required host is down / not pingable!\n"
                    "please make sure the below mentioned host is powered-on and has a network connection!"
                    )
            elif "could not establish tcp forward" in q["output"]:
                msg = "that host is not directly reachable and we were not able to connect to all required gateway hosts!"
            elif "needs authentification via key file" in q["output"] or "key_challange not passed" in q["output"]:
                msg = "there is a deamon running, but that deamon is locked!\n"
                if "but there is no daemon_private_key_file defined" in q["output"]:
                    msg += "but you configured not to use daemon authentication for this host!\n"
                elif "key_challange not passed" in q["output"]:
                    msg += "you provided a private-key file, but that was not the expected one -- not the one the daemon was locked with!\n"
                msg += (
                    "you can try to kill that daemon by logging in on the below mentioned host and running\n"
                    '    <span font_family="monospace">killall -TERM ln_daemon</span>\n'
                    "then try again!\n"
                    )

                url = "https://rmc-github.robotic.dlr.de/pages/common/links_and_nodes/config_hosts.html#daemon-private-key-file"
                msg += (
                    'please also compare the <a href="%s">LN-Documentation for daemon_private_key_file</a>[1].\n'
                    "\n"
                    "[1] %s"
                ) % (url, url)
            elif "Missing default profile:" in q["output"]:
                url = "https://rmc-github.robotic.dlr.de/pages/tools/cissy/master/install.html"
                msg = (
                    "it looks like you need login to that host and run\n"
                    '    <span font_family="monospace">cissy setup</span>\n'
                    'please check <a href="%s">the official cissy documentation</a>[1] for how to install cissy properly.\n'
                    "\n"
                    "[1] %s"
                    ) % (url, url)
            elif q["is_ln_daemon_from_home_trial"]:
                msg = (
                    "your `daemon_start_script`-command uses `cissy run`, but that did not work out on that host.\n"
                    "maybe cissy is not available on %s -- if that host has your $HOME directory mounted, you can try to use your current\n"
                    "host (%s) to install the ln_daemon instead. there is no guarantee that it works, but you can give it a try -- please\n"
                    "run this command:\n"
                    '    <span font_family="monospace">cissy run -k -p "links_and_nodes_manager/[~2 >=2.3]@common/stable" export_ln_daemon</span>\n'
                    "then try again to connect to that host!"
                ) % (q["hostname"], socket.gethostname())
            elif "set to never-lock" in q["output"]:
                msg = "there is a deamon running, but that deamon is set to never-lock!\n"
                msg += "but you configured a key file for this daemon -- so you want to have this daemon locked!\n"
                msg += (
                    "you can try to kill that daemon by logging in on the below mentioned host and running\n"
                    '    <span font_family="monospace">killall -TERM ln_daemon</span>\n'
                    "then try again!\n"
                    )
                url = "https://rmc-github.robotic.dlr.de/pages/common/links_and_nodes/config_hosts.html#daemon-public-key-file"
                msg += (
                    'please also compare the <a href="%s">LN-Documentation for daemon_public_key_file</a>[1].\n'
                    "\n"
                    "[1] %s"
                ) % (url, url)
            else:
                msg = "please check the information below!"
            msg = hdr + msg
            dlg = Gtk.MessageDialog(
                self.main_window,
                Gtk.DialogFlags.DESTROY_WITH_PARENT,
                Gtk.MessageType.ERROR,
                Gtk.ButtonsType.OK,
                message_format=msg
            )
            dlg.set_resizable(True)
            dlg.set_default_size(-1, 640)
            dlg.set_markup(msg)
            box = dlg.get_message_area()
            first = box.get_children()[0]
            first.set_selectable(True)
            first.set_line_wrap(False)

            sw = Gtk.ScrolledWindow()
            vte = Vte.Terminal()
            box.pack_start(sw, True, True, 0)
            self._init_vte(vte, None, with_event_callbacks=False)
            vte.set_input_enabled(False)
            feed = q["output"]
            red = "31"
            green = "32"
            to_color = [
                ("lnm exec:", green),
                ("Sending command:", green),
                ("bash:", red),
                ("command not found", red),
                ("No such file or directory", red),
                ("Missing default profile", red),
                ("[ERROR]", red),
                ("Exception:", red),
                ("RuntimeError:", red),
                ("conans.errors.ConanException", red),
            ]
            for search, color in to_color:
                feed = feed.replace(search, "\033[%sm%s\033[0m" % (color, search))
            feed = feed.replace("\n", "\r\n")
            vte.feed(feed.encode("utf-8"))
            sw.add(vte)
            sw.show_all()
            cbox = box.get_parent()
            box_parent = cbox.get_parent()
            box_parent.child_set_property(cbox, "expand", True)
            box_parent.child_set_property(cbox, "fill", True)

            def copy_to_clipboard(lbl, href):
                cb = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
                text = "```\n%s\n```\n" % q["output"]
                cb.set_text(text, len(text))
                return True
            lbl = Gtk.Label()
            lbl.set_markup('<a href="copy">copy all this to clipboard</a>')
            lbl.set_property("halign", Gtk.Align.START)
            lbl.connect("activate-link", copy_to_clipboard)
            box.pack_start(lbl, False, False, 0)
            lbl.show()

            def answer(dlg, response_id, self, question, q):
                dlg.hide()
                self.manager_proxy(request="gui_question_answer", question=question, q=q)
            dlg.connect("response", answer, self, question, q)
            dlg.show()
        elif question == "lost_daemon_connection":
            if "will be closed" in q.get("why", ""):
                info = q["why"]
            else:
                info = (
                    "possible causes: %s was powered off, daemon was killed by a user, network got unplugged or a bug of the daemon.\n\n"
                    "please use the 'save all' button on the upper-left of the process-tab to store diagnostic information to a file and create an issue at\n<a href=\"%s\">%s</a>!\n"
                ) % (q["hostname"], config.ln_issues_url, config.ln_issues_url)
            msg = ("error! lost connection to daemon on host %s!\n" % q["hostname"]) + info
            dlg = Gtk.MessageDialog(
                self.main_window, 
                Gtk.DialogFlags.DESTROY_WITH_PARENT, 
                Gtk.MessageType.ERROR,
                Gtk.ButtonsType.OK,
                message_format=msg
            )
            dlg.set_markup(msg)
            def answer(dlg, response_id, self, question, q):
                dlg.hide()
                self.manager_proxy(request="gui_question_answer", question=question, q=q)
            dlg.connect("response", answer, self, question, q)
            dlg.show()
        elif question == "config_problems":
            tb = self.problems_tb
            msgs = []
            problems = q
            self.reload_problems = problems
            for problem in problems:
                if problem[0].startswith("no problem:"):
                    continue
                if problem[0] == "process disappeared":
                    pname, host = problem[1:]
                    item = (
                        "problem: running process not mentioned in new config!\n"
                        "process: %r on %r\n"
                        "solution: the process will be stopped") % (
                        pname, host)
                elif problem[0] == "process moved host":
                    pname, old_host, new_host = problem[1:]
                    item = (
                        "problem: running process moved to different host!\n"
                        "process: %r on %r is supposed to move to %r\n"
                        "solution: the process will be stopped on %r") % (
                            pname, old_host, new_host, old_host)
                elif problem[0] == "manager port changed":
                    old_port, new_port = problem[1:]
                    item = (
                        "warning: by loading this config the manager port will change from %s to %s.\n"
                        "already running clients might still try to connect to the old port!\n"
                        "solution: make sure all clients are stopped/restarted") % (
                            old_port, new_port)
                else:
                    item = (
                        "problem: %s\n"
                        "unknown details: %s") % (
                        problem[0],
                        pprint.pformat(problem[1:]))
                msgs.append(item)
            tb.set_text("\n\n".join(msgs))
            self.reload_problems_window.show()
        elif question == "config_error":
            msg = "error loading config!\n%s" % q
            dlg = Gtk.MessageDialog(
                self.main_window, 
                Gtk.DialogFlags.DESTROY_WITH_PARENT, 
                Gtk.MessageType.ERROR,
                Gtk.ButtonsType.OK,
                message_format=msg
                )
            dlg.set_markup(msg)
            def answer(dlg, response_id, question, q):
                dlg.hide()
                self.manager_proxy(request="gui_question_answer", question=question, q=q)
            dlg.connect("response", answer, question, q)
            dlg.show()
        elif question == "start_process":
            msg = (
                "process <b>%s</b> shall be started!\n" + 
                "it has no <i>command</i> assigned.\n" +
                "could you please try to start it?\n" +
                "was it successfull?"
                ) % (
                q["obj_name"], 
                )
            dlg = Gtk.MessageDialog(
                self.main_window, 
                Gtk.DialogFlags.DESTROY_WITH_PARENT, 
                Gtk.MessageType.QUESTION,
                Gtk.ButtonsType.YES_NO,
                message_format=msg
                )
            dlg.set_markup(msg)
            dlg.set_property("use-markup", True)
            def answer(dlg, response_id, question, q):
                dlg.hide()
                started = response_id == Gtk.ResponseType.YES
                self.manager_proxy(request="gui_question_answer", question=question, obj_name=q["obj_name"], started=started)
            dlg.connect("response", answer, question, q)
            dlg.show()
        elif question == "stop_process":
            msg = (
                "process <b>%s</b> shall be stopped!\n" + 
                "it has no <i>command</i> assigned.\n" +
                "could you please stop it?"
                ) % (
                q["obj_name"], 
                )
            dlg = Gtk.MessageDialog(
                self.main_window, 
                Gtk.DialogFlags.DESTROY_WITH_PARENT, 
                Gtk.MessageType.QUESTION,
                Gtk.ButtonsType.OK,
                message_format=msg
                )
            dlg.set_markup(msg)
            dlg.set_property("use-markup", True)
            def answer(dlg, response_id, question, q):
                dlg.hide()
                self.manager_proxy(request="gui_question_answer", question=question, obj_name=q["obj_name"])
            dlg.connect("response", answer, question, q)
            dlg.show()
        a = time.time()
        while time.time() - a < 1:
            Gtk.main_iteration_do(False)
        return True

    def _process_started(self, name):
        p = self.sysconf.processes[name]
        self.propagate_process_update(p)
        has_window = hasattr(p, "process_output_window") and p.process_output_window and not p.process_output_window.hidden
        if has_window:
            p.process_output_window.process_started()
        return True

    def _process_output(self, name, output, state_cmd=None):
        """
        `output` is expected to be encoded bytes-string!
        """
        #self.debug("process output from %s: %r" % (name, output))
        if state_cmd is None:
            p = self.sysconf.processes[name]
        else:
            p = self.sysconf.states[name]
        if state_cmd is not None or p.remove_color_codes:
            to_remove = [
                b"\033\[.*?(m|J|h|H)",
                b"\033\]2;",
                ]
            for ex in to_remove:
                output = re.sub(ex, b"", output)
            output = output.replace(b"\007", b"")
        self._append_output(p, output, state_cmd=state_cmd)
        return True
        
    def update_warnings_model(self, try_update=False):
        p = self.current_p
        m = self.warnings_model
        if not try_update:
            m.clear()
        # update
        iter = m.get_iter_first()
        cw = 0
        while iter:
            if m[iter][0] != p._warnings_log[cw][0]:
                if not m.remove(iter):
                    iter = m.get_iter_first()
            else:
                cw += 1
                iter = m.iter_next(iter)
        for new_date, new_msg in p._warnings_log[cw:]:
            m.append((new_date, new_msg))
        
    def _new_process_warning(self, name, warnings):
        p = self.sysconf.processes[name]
        p._warnings_log = warnings
        if self.current_p == p:
            # update warnings log!
            self.update_warnings_model(try_update=True)
            
        if warnings and not p._warning_is_active:
            for wd, wm in warnings:
                if not hasattr(p, "since_last_warning") or wd > p.since_last_warning:
                    self._display_warning(p, wm)
                    break        
        p.since_last_warning = warnings[-1][0]
        return True

    def on_terminal_commit(self, terminal, text, length, p):
        if not p.dont_translate_backspace_to_delete and text == "\x08":
            # Backspace(\b, ^H, 8) -> Delete(^?, 127)
            text = "\x7f"
        self.manager_proxy(request="send_stdin", pname=p.name, text=text)

    def on_terminal_resize(self, terminal, allocation, p):
        rows = terminal.get_row_count()
        cols = terminal.get_column_count()
        new_size = rows, cols
        if hasattr(p, "_last_window_size") and p._last_window_size == new_size:
            return False
        if p.state in ("started", "ready"):
            self.manager_proxy("send_winch", pname=p.name, rows=rows, cols=cols)
        p._last_window_size = new_size
        return False

    def on_log_tv_key_press_event(self, tv, ev):
        text = None
        p = self.current_p
        if ev.keyval == 65288: # backspace
            if not p.dont_translate_backspace_to_delete:
                # Backspace(\b, ^H, 8) -> Delete(^?, 127)
                text = "\x7f"
            else:
                text = "\x08"
        else:
            text = ev.string
        if text:
            self.manager_proxy("send_stdin", pname=p.name, text=text)
        return True

    def set_vte_style(self, term):
        from . import color_styles
        if self.current_p and hasattr(self.current_p, "vte_color_style"):
            style = getattr(color_styles, self.current_p.vte_color_style)
        else:
            style = getattr(color_styles, self.sysconf.instance_config.vte_color_style)
        def get_style(style):
            style = "[%s]" % style.replace("{", "[").replace("}", "]").strip()
            style = eval(style)
            style = [Gdk.RGBA(*[float(rgb) / 0xffff for rgb in c[1:]]) for c in style]
            return style
        color_style = get_style(style)
        term.set_colors(None, None, color_style)

    def _init_vte(self, term, p, button_press=None, with_event_callbacks=True):
        term.set_encoding("utf-8")
        if self.vte_font_description:
            term.set_font(self.vte_font_description)
        if p is not None:
            term.set_scrollback_lines(p.max_output_lines)
        #term.set_cursor_shape(vte.CURSOR_SHAPE_IBEAM) # vte.CURSOR_SHAPE_UNDERLINE
        term.set_cursor_shape(Vte.CursorShape.BLOCK)
        #term.set_cursor_blink_mode(vte.VTE_CURSOR_BLINK_ON)
        term.set_cursor_blink_mode(Vte.CursorBlinkMode.ON)
        #term.set_emulation("xterm")
        if with_event_callbacks:
            term.connect("commit", self.on_terminal_commit, p)
            term.connect("size-allocate", self.on_terminal_resize, p)
            if button_press is None:
                button_press = self.on_log_tv_button_press_event
            term.connect("button-press-event", button_press)
        term.set_scroll_on_output(True)
        self.set_vte_style(term)
        term.set_size_request(100, 100)

    def init_vte(self, p):
        p.vte = Vte.Terminal()
        self._init_vte(p.vte, p)

    def _append_output(self, p, to_add, state_cmd=None):
        """
        `to_add` is encoded bytes-string from process
        """

        if p.use_vte and not have_vte:
            p.use_vte = False
        if state_cmd is None and not p.no_pty:
            # with pty
            # (states never have a pty)
            if not p.use_vte:
                # without vte
                # 1.3.1
                # 1.3.2
                to_add = to_add.replace(b"\r\n", b"\n")
                to_add = to_add.replace(b"\r", b"\n")
        else:
            # without pty or state_cmd
            if state_cmd is None and p.use_vte:
                # with vte
                # (states never have a vte)
                to_add = to_add.replace(b"\n", b"\r\n")
            else:
                # without vte or state_cmd
                to_add = to_add.replace(b"\r\n", b"\n")
                to_add = to_add.replace(b"\r", b"\n")

        if state_cmd is not None:
            ts = str(datetime.datetime.now())[:23].encode(p.output_encoding)
            to_add = b"%s %s\n" % (ts, to_add.replace(b"\n", b"\n " + (b" " * len(ts))).strip())
            
        if not hasattr(p, "output"): p.output = b"" # for gui
        original_len = len(p.output)
        p.output += to_add
        p.output_lines += to_add.count(b"\n")
        p.outputs_seen += 1
        start_from = 0
        while p.output_lines > p.max_output_lines and start_from < len(p.output):
            new_start_from = p.output.find(b"\n", start_from)
            if new_start_from == -1:
                break
            start_from = new_start_from + 1
            p.output_lines -= 1
        if start_from > 0:
            p.output = p.output[start_from:]

        to_remove = original_len - (len(p.output) - len(to_add))
        has_window = hasattr(p, "process_output_window") and p.process_output_window and not p.process_output_window.hidden

        if state_cmd is None:
            new_lines = to_add.split(b"\n")

            if p.name in self._watch_for_gdb_prompt and new_lines[-1] == b"(gdb) ":
                self._watch_for_gdb_prompt.remove(p.name)
                cmds = ["run\r"]
                self.info("trying to do gdb run for %r!\nsending to stdin: %r\nspecify disable_gdb_run process flag to disable this" % (p.name, cmds))
                self.queue_stdins_to_send(p, cmds, 0.1)

            if p.line_highlight_regexes and (p.line_highlight_fg_color or p.line_highlight_bg_color):
                had_matches = False
                for line_i, line in enumerate(new_lines):
                    for i, regex in enumerate(p.line_highlight_regexes):
                        if re.search(regex, self.try_get_unicode_output(p, line)):
                            break # match!
                    else:
                        continue
                    had_matches = True
                    # highlight line:
                    code = b""
                    if p.line_highlight_fg_color:
                        code += b"%d" % (30 + int(p.line_highlight_fg_color))
                    if p.line_highlight_bg_color:
                        if code:
                            code += b";"
                        code += b"%d" % (30 + int(p.line_highlight_bg_color))
                    new_lines[line_i] = b"\x1b[%sm%s\x1b[0m" % (code, line)
                if had_matches:
                    to_add = b"\n".join(new_lines)
        if has_window:
            p.process_output_window.update_process_log(p, to_add, to_remove)
        if state_cmd is None and p.use_vte and have_vte:
            if not hasattr(p, "vte"):
                self.init_vte(p)
            if not p.frozen:
                unicode_text = self.try_get_unicode_output(p, to_add)
                p.vte.feed(unicode_text.encode("utf-8")) # 1.2.1, we want to only feed utf-8 to the terminal!
        elif self.current_p == p:
            # todo: do this via idle handler!
            #stime = time.time()
            if not hasattr(p, "frozen") or not p.frozen:
                self.update_process_log(p, to_add, to_remove)
            #etime = time.time() - stime
            #print "update_process_log, to_add %d, to_remove: %d in %0.3fs" % (len(to_add), to_remove, etime)
            #self._update_process_log_list.append((p, to_add, to_remove))
        #self._update_all_like_set.add(p)
        self._update_all_like(p)
        if self._update_all_like_idle is None:
            #self._update_all_like_idle = gobject.idle_add(self._do_update_all_like_idle)
            #self._update_all_like_idle = gobject.timeout_add(250, self._do_update_all_like_idle)
            pass

    def _do_update_all_like_idle(self):
        if not self._update_all_like_set and not self._update_process_log_list:
            self._update_all_like_idle = None
            return False
        for p in self._update_all_like_set:
            self._update_all_like(p)
        ato_add = []
        ato_remove = 0
        for p, to_add, to_remove in self._update_process_log_list:
            if p != self.current_p:
                continue
            #self.update_process_log(p, to_add, to_remove)
            ato_add.append(to_add)
            ato_remove += to_remove
        if ato_add:
            self.update_process_log(self.current_p, "".join(ato_add), to_remove)
        self.update_process_log_list = []
        self._update_all_like_set = set()
        #self._update_all_like_idle = None
        #return False
        return True

    def signal_process(self, signo):
        obj = self.current_p
        self.manager_proxy(request="signal_process", pname=obj.name, signo=signo)
        
    def signal_name_process(self, signame):
        obj = self.current_p
        self.manager_proxy(request="signal_name_process", pname=obj.name, signame=signame)
    
    def on_hup_btn_clicked(self, btn):
        self.signal_process(int(signal.SIGHUP))

    def on_int_btn_clicked(self, btn):
        self.signal_process(int(signal.SIGINT))

    def on_stop_btn_clicked(self, btn):
        self.signal_name_process("SIGSTOP")

    def on_cont_btn_clicked(self, btn):
        self.signal_name_process("SIGCONT")

    def on_signal_btn_clicked(self, btn):
        signo = int(self.signal_entry.get_text())
        self.signal_process(signo)
        
    def on_connect_all_daemons_button_clicked(self, btn):
        self.manager_proxy(request="connect_all_daemons")
    #def on_stop_all_connected_daemons_button_clicked(self, btn):
    #    self.manager_proxy(request="stop_all_daemons")
    def on_save_all_btn_clicked(self, btn):
        dlg = Gtk.FileChooserDialog(
            "save debug info", self.gui.dialog, action=Gtk.FileChooserAction.SAVE,
            buttons=(
                Gtk.STOCK_CANCEL, Gtk.ResponseType.REJECT,
                Gtk.STOCK_OK, Gtk.ResponseType.ACCEPT)
        )
        ret = dlg.run()
        dlg.hide()
        if ret != Gtk.ResponseType.ACCEPT:
            return True
        fn = dlg.get_filename()
        bn = os.path.basename(fn)
        if "." in bn:
            bn = bn.rsplit(".", 1)[0]
        else:
            fn += ".tar.bz2"
        debug_info = self.manager_proxy(request="create_debug_info", name_hint=bn)
        if debug_info:
            with open(fn, "wb") as fp:
                fp.write(debug_info)
        
    def on_lnm_bug_btn_clicked(self, btn):
        self._show_url(config.ln_issues_url)
        
    def _show_url(self, url):
        cmd = "xdg-open '%s'" % url
        pyutils.system(cmd)

    def on_state_deps_tv_row_activated(self, tv, path, tvc):
        return self.on_process_dp_row_activated(tv, path, tvc, self.state_deps_model, self._state_deps_pb_tvc)
    def on_state_dep_of_tv_row_activated(self, tv, path, tvc):
        return self.on_process_dp_row_activated(tv, path, tvc, self.process_dep_of_model, self._state_dep_of_pb_tvc)
    def on_process_deps_tv_row_activated(self, tv, path, tvc):
        return self.on_process_dp_row_activated(tv, path, tvc, self.process_deps_model, self._process_deps_pb_tvc)
    def on_process_dep_of_tv_row_activated(self, tv, path, tvc):
        return self.on_process_dp_row_activated(tv, path, tvc, self.process_dep_of_model, self._process_dep_of_pb_tvc)
    def on_process_dp_row_activated(self, tv, path, tvc, m, pb_tvc):
        p = m[path][0]
        on_pb = tvc == pb_tvc
        if on_pb:
            if p.__class__.__name__ == "State":
                if p.state == "UP":
                    self._request_state(p, "DOWN")
                else:
                    self._request_state(p, "UP")
            else:
                if p.state in ("started", "ready"):
                    self._request_state(p, "stop")
                else:
                    self._request_state(p, "start")
        else:
            self._select_process(p)

    def set_process_info(self, name, pid, start_time, stop_time):
        p = self.sysconf.processes[name]
        p.pid = pid
        p.start_time = start_time
        p.stop_time = stop_time
        if self.current_p == p:
            self.update_process_display(p)
        self.propagate_process_update(p)
        return True
    
    def _indirect_error(self, op, method, p, depth=0):
        if depth > 10:
            return
        if method == "add":
            if p not in op._indirect_errors:
                op._indirect_errors.append(p)
        else:
            if p in op._indirect_errors:
                op._indirect_errors.remove(p)
        if self.current_p == op:
            self.update_process_display(op)
        self._update_all_like(op)
        # back recursion
        has_dep_errors = bool(len(op._indirect_errors))
        for p in op._needed_by:
            if has_dep_errors:
                self._indirect_error(p, "add", op, depth=depth+1)
            elif not has_dep_errors:
                self._indirect_error(p, "del", op, depth=depth+1)

    def _check_for_indirect_errors(self, pnames):
        for pname, p in self.sysconf.processes.items():
            has_dep_errors = False
            for reason, arg in p._errors:
                if reason.startswith("depen"):
                    has_dep_errors = True
                    break
            for op in p._needed_by:
                if op.__class__.__name__ != "Process": continue
                if op.state not in ("started", "ready"):
                    continue
                # notify about indirect dep problem
                if has_dep_errors:
                    self._indirect_error(op, "add", p)
        self.update_process_status_display()
        return True

    def _process_errors_update(self, errs):
        #print "\nprocess errors update!"
        for pname, perrs in errs.items():
            #print "process %r has %d errors" % (pname, len(perrs))
            p = None
            for what in "processes, states".split(", "):
                p = getattr(self.sysconf, what).get(pname)
                if p is not None:
                    break
            if p is None:
                raise Exception("%r is neither a State nor a Process!" % pname)
            has_window = hasattr(p, "process_output_window") and p.process_output_window
            if has_window and perrs:
                # raise process output window
                p.process_output_window.present(only_if_not_fully_visible=True)
            self._update_all_like(p)
            if self.current_p == p:
                getattr(self, "update_%s_display" % p.__class__.__name__.lower())(p)
            has_dep_errors = False
            for reason, arg in perrs:
                if reason.startswith("depen"):
                    has_dep_errors = True
                    break
            #print "process %r has dependency errors: %s" % (pname, has_dep_errors)
            for op in p._needed_by:
                if op.__class__.__name__ not in ("Process", "State"): continue
                if op.state not in ("started", "ready", "UP", "DOWN"):
                    continue
                #print "  this process is needed by %r" % (op.name)
                # notify about indirect dep problem
                if has_dep_errors and p not in op._indirect_errors:
                    self._indirect_error(op, "add", p)
                elif not has_dep_errors and p in op._indirect_errors:
                    self._indirect_error(op, "del", p, depth=1)
                if self.current_p == op:
                    getattr(self, "update_%s_display" % op.__class__.__name__.lower())(op)
            if self.current_p == p:
                getattr(self, "update_%s_display" % p.__class__.__name__.lower())(p)
        self.update_process_status_display()
        return True

    def on_notifications_tv_row_activated(self, tv, path, tvc):
        row = self.notifications_model[path]
        reason = row[1]
        p = row[2]

        self.debug("%r reason: %r" % (p.name, reason))

        pos, size = tvc.cell_get_position(self.notifications_tv_pb_cr)
        rect = tv.get_cell_area(path, tvc)
        gdk_window, x, y = Gdk.Display.get_default().get_window_at_pointer()
        x, ty = tv.convert_bin_window_to_tree_coords(x, 0)
        on_pb = x < rect.x + size

        if not on_pb:
            self._select_process(p)
        else:
            if p.state in ("started", "ready"):
                if "depends_on_restart" in reason:
                    self._request_stop_start(p)
                else:
                    self._request_state(p, "stop")
            else:
                self._request_state(p, "start")


    def on_show_pl_btn_clicked(self, btn):
        self.show_process_list()

    def show_process_list(self, host=None, with_threads=False, select_tid=None):
        if host is None:
            host = self.current_p.host.name
        if isinstance(host, SystemConfiguration.Host):
            host = host.name
        if host not in self.process_lists:
            self.process_lists[host] = process_list(self, host, with_threads=with_threads)
        else:
            self.process_lists[host].present(with_threads=with_threads)
        pl = self.process_lists[host]
        if select_tid:
            pl.select_tid(select_tid[0], select_tid[1])
        return pl

    def new_process_list(self, host, process_list, architecture):
        if host not in self.process_lists:
            self.error("got processlist for host %r / %s - but nobody cares!" % (host, architecture))
            return True
        self.process_lists[host]._set_process_list(process_list, architecture)
        return True
    
    def on_show_po_btn_clicked(self, btn):
        if self._ignore_po_btn:
            return True
        if self.current_p:
            self.show_process_output_window(btn.get_active())

    def on_show_gui_btn_clicked(self, btn, process=None):
        if process is None:
            process = self.current_p
        self.manager_proxy("show_process_gui", process.name)

    def show_process_output_window(self, wants_window):
        has_window = hasattr(self.current_p, "process_output_window") and self.current_p.process_output_window
        if has_window and not wants_window:
            self.current_p.process_output_window.hide()
        if not wants_window:
            return True
        if has_window:
            self.current_p.process_output_window.present()
        else:
            self.current_p.process_output_window = process_output(self, self.current_p, self.process_output_positions.get(self.current_p.name))

    def on_show_po_btn_button_press_event(self, btn, ev):
        if ev.button == 3:
            if not self.show_po_btn.get_active():
                self.show_po_btn.set_active(True)
            else:
                self.show_process_output_window(True)
        return False

    def on_show_pid_in_pl_btn_clicked(self, btn):
        pl = self.show_process_list()
        pl.select_pid(self.current_p.pid)

    def _store_process_output_positions(self):
        self._save_last_pos_source = None
        with open(self._process_output_positions_fn, "w") as fp:
            fp.write(pprint.pformat(self.process_output_positions))
        return False
    def _load_process_output_positions(self):
        try:
            fp = open(self._process_output_positions_fn, "r")
        except Exception:
            return
        try:
            self.process_output_positions = eval(fp.read())
        except Exception:
            self.error("invalid window-position file: %r\n%s" % (self._process_output_positions_fn, traceback.format_exc()))
        fp.close()
    def store_process_output_position(self, pname, pos):
        if self.process_output_positions.get(pname) == pos:
            return
        self.process_output_positions[pname] = pos
        if self._save_last_pos_source:
            GLib.source_remove(self._save_last_pos_source)
        self._save_last_pos_source = GLib.timeout_add(2500, self._store_process_output_positions)

    def on_stop_daemon_btn_clicked(self, btn):
        host = self.sysconf.get_host_for_node(self.current_group)
        self.manager_proxy(request="stop_daemon", daemon_or_host=host)
        self.on_tv_cursor_changed(self.tv)
        return True

    def on_start_daemon_btn_clicked(self, btn):
        host = self.sysconf.get_host_for_node(self.current_group)
        self.manager_proxy(request="try_to_connect_daemon", host=host)
        self.on_tv_cursor_changed(self.tv)
        return True

    def on_log_nb_switch_page(self, nb, page, page_num):
        return True

    def on_enable_ln_debug_cb_clicked(self, btn):
        if self.ignore_ignore_dep_change:
            return True
        obj = self.current_p
        obj.ln_debug = btn.get_active()
        self.manager_proxy(request="set_process_attribute", pname=obj.name, attribute="ln_debug", value=obj.ln_debug)

    def on_freeze_btn_toggled(self, btn):
        if self.ignore_ignore_dep_change:
            return True
        self.current_p.frozen = btn.get_active()
        if self.current_p.frozen:
            self.current_p.frozen_output = self.current_p.output
        else:
            self.set_process_log(self.current_p)
        
    def on_follow_btn_toggled(self, btn):
        if self.current_p:
            self.current_p.follow = btn.get_active()
            if hasattr(self.current_p, "vte"):
                self.current_p.vte.set_scroll_on_output(self.current_p.follow)
                # seems to have no effect...
        return False

    def add_process(self, process, group_name=None):
        if self.gui.manager is None:
            self.sysconf.processes[process.name] = process
        if self.display_type == "per_group":
            if group_name is None:
                return True
            def search_groups(parent):
                iter = self.model.iter_children(parent)
                while iter:
                    if self.model[iter][0].name == group_name:
                        self.model.append(iter, (process, ))
                    if self.model.iter_has_child(iter):
                        search_groups(iter)
                    iter = self.model.iter_next(iter)
            search_groups(None)
        elif self.display_type == "per_physical_host":
            if process.host is None:
                return True
            iter = self.model.iter_children(None)
            host_name = process.host.name + "/"
            while iter:
                if self.model[iter][0] == host_name:
                    self.model.append(iter, (process, ))
                    break
                iter = self.model.iter_next(iter)
        elif self.display_type == "per_logical_node":
            iter = self.model.iter_children(None)
            node_name = process.node + "/"
            while iter:
                if self.model[iter][0] == node_name:
                    self.model.append(iter, (process, ))
                    break
                iter = self.model.iter_next(iter)
        elif self.display_type == "flat_listing":
            iter = self.model.iter_children(None)
            while iter:
                if self.model[iter][0].name > process.name:
                    self.model.insert_before(None, iter, (process, ))
                    break
                iter = self.model.iter_next(iter)
            else:
                self.model.append(None, (process, ))
            
        return True
    
    def on_reload_btn_clicked(self, btn):
        self.manager_proxy(request="reload_config")

    def on_new_config(self, new_config):
        if self.current_p:
            pname = self.current_p.name
        else:
            pname = None
        self.sysconf = new_config
        self.redisplay()
        if pname in self.sysconf.processes:
            self.current_p = self.sysconf.processes[pname]
            self._select_process(self.current_p)
        else:
            self.current_p = None
        self.on_tv_cursor_changed(self.tv, force_update=True)
        return True

    def open_file(self, fn, line_no=None):
        # find editor
        editor = os.getenv("EDITOR", "/usr/bin/gedit")
        if line_no is None:
            return pyutils.system("%s '%s' &" % (editor, fn))

        is_emacs = "emacs" in editor.lower()
        if is_emacs:
            return pyutils.system("%s +%d '%s' &" % (editor, line_no, fn))
        pyutils.system("%s '%s' +%d &" % (editor, fn, line_no))

    def on_edit_config_btn_pressed(self, btn):
        if len(self.sysconf.included_files) <= 1:
            self.have_popup = False
            return True
        def activate(menuitem, fn):
            if menuitem:
                self.config_file_menu.popdown()
            self.open_file(fn)

        if hasattr(self, "config_file_menu"):
            m = self.config_file_menu
            m.forall(lambda w: m.remove(w))
        else:
            self.config_file_menu = Gtk.Menu()
            m = self.config_file_menu
            def on_detach(*args):
                self.debug("on_detach: %r" % (args, ))
            m.attach_to_widget(self.edit_config_btn, on_detach)
        for fn in self.sysconf.included_files:
            item = Gtk.MenuItem(label=fn, use_underline=False)
            item.connect("activate", activate, fn)
            m.append(item)
            item.show()
        m.popup(None, None, None, None, 1, 0)
        self.have_popup = True
        return True

    def on_edit_config_btn_clicked(self, btn):
        if not self.have_popup:
            self.open_file(self.sysconf.configuration_file)
        return True

    def on_update_service(self, client, service_name):
        if self.current_p and hasattr(client, "process") and client.process == self.current_p:
            self.show_gui_btn.set_sensitive(self.has_present_window(client.process))
        return True

    def on_edit_grp_cfg_pressed(self, btn):
        path, col = self.tv.get_cursor()
        if not path: # nothing selected!
            return
        iter = self.model.get_iter(path)
        return self.on_show_cfg_btn_pressed(btn, obj=self.model[iter][0])
    
    def on_edit_grp_cfg_clicked(self, btn):
        path, col = self.tv.get_cursor()
        if not path: # nothing selected!
            return
        iter = self.model.get_iter(path)
        return self.on_show_cfg_btn_clicked(btn, obj=self.model[iter][0])

    def on_show_cfg_btn_pressed(self, btn, obj=None):
        if obj is None:
            obj = self.current_p
        if obj is None:
            return
        if obj._sysconf_stack and len(obj._sysconf_stack) > 1:
            def activate(menuitem, fn, line):
                if menuitem:
                    self.config_file_item_menu.popdown()
                self.open_file(fn, line_no=line)

            if hasattr(self, "config_file_item_menu"):
                m = self.config_file_item_menu
                m.forall(lambda w: m.remove(w))
            else:
                m = self.config_file_item_menu = Gtk.Menu()
                def on_detach(*args):
                    self.debug("on_detach: %r" % (args, ))
                m.attach_to_widget(btn, on_detach)
            for fn, line in obj._sysconf_stack:
                item = Gtk.MenuItem(label="%s:%s" % (fn, line), use_underline=False)
                item.connect("activate", activate, fn, line)
                m.append(item)
                item.show()
            m.popup(None, None, None, None, 1, 0)
            self.have_item_popup = True
        else:
            self.have_item_popup = False
        return True

    def on_show_cfg_btn_clicked(self, btn, obj=None):
        if self.have_item_popup:
            return
        
        if obj is None:
            obj = self.current_p
        if obj is None:
            return
        if obj._sysconf_start_line is None:
            return
        fn, ln = obj._sysconf_start_line
        self.open_file(fn, line_no=ln)
        return True

    def on_release_all_resources_btn_clicked(self, btn):
        host = self.sysconf.get_host_for_node(self.current_group)
        self.manager_proxy("release_all_daemon_resources", daemon_or_host=host)

    def queue_stdins_to_send(self, p, blocks, interval):
        ts = time.time()
        for block in blocks:
            ts += interval
            self.stdins_to_send.append((ts, p, block))
        self.stdins_to_send.sort()
        self.process_stdins_to_send()

    def process_stdins_to_send(self):
        if not self.stdins_to_send:
            return False

        # check next deadline
        ts = time.time()
        if self.stdins_to_send[0][0] <= ts:
            dl, p, block = self.stdins_to_send.pop(0)
            self.info("send stdin: %r" % block)
            self.manager_proxy("send_stdin", pname=p.name, text=block)
            if not self.stdins_to_send:
                return False

        # get next deadline
        next_step = self.stdins_to_send[0][0] - ts
        if next_step <= 0:
            next_step = 0.01
        next_dl = int(next_step * 1000) + 5
        GLib.timeout_add(next_dl, self.process_stdins_to_send)
        
        return False

    def _on_timer(self, target, target_args):
        if target in self._timer_targets:
            del self._timer_targets[target]
        try:
            target(*target_args)
        except Exception:
            traceback.print_exc()
        return False

    def _restart_timer(self, to, target, *target_args):
        tid = self._timer_targets.get(target)
        if tid is not None:
            GLib.source_remove(tid)
        if to == 0:
            self._on_timer(target, target_args)
        else:
            self._timer_targets[target] = GLib.timeout_add(int(to * 1000), self._on_timer, target, target_args)


    def _set_process_command_override(self):
        cmd = self.process_command.get_text()
        self.current_p.command_override = cmd
        self.manager_proxy("set_process_command_override", pname=self.current_p.name, override=cmd)

    def on_process_command_activate(self, entry):
        self._restart_timer(0, self._set_process_command_override)
    def on_process_command_changed(self, entry):
        if self.ignore_process_command_changed:
            return
        self._restart_timer(0.5, self._set_process_command_override)
    def on_process_command_key_press(self, entry, ev):
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Return:
            self._restart_timer(0, self._set_process_command_override)
            self.request_state("start")
        return False


    def _set_check_cmd_override(self, which):
        cmd = getattr(self, "state_%s_cmd" % which).get_text()
        self.current_p.command_overrides[which] = cmd
        self.manager_proxy("set_state_command_override", sname=self.current_p.name, which=which, override=cmd)

    def on_state_cmd_activate(self, entry):
        name = Gtk.Buildable.get_name(entry)
        which = name.split("_", 2)[1]
        self._restart_timer(0, self._set_check_cmd_override, which)
    def on_state_cmd_changed(self, entry):
        if self.ignore_process_command_changed:
            return
        name = Gtk.Buildable.get_name(entry)
        which = name.split("_", 2)[1]
        self._restart_timer(0.5, self._set_check_cmd_override, which)
    def on_state_cmd_key_press(self, entry, ev):
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Return:
            name = Gtk.Buildable.get_name(entry)
            which = name.split("_", 2)[1]
            self._restart_timer(0, self._set_check_cmd_override, which)
            self.request_state(which.upper())
        return False

    def get_active_io_script(self):
        active = self.io_script_cb.get_active_id()
        if active:
            return active

    def on_io_script_cb_changed(self, cb):
        m = self.io_script_m
        m.clear()
        active = self.get_active_io_script()
        if not active:
            return
        script = self.current_p.io_scripts.scripts[active]
        self.current_p._gui_io_script_last_active = script.name
        active_line = getattr(script, "_gui_current_line", script.current_line)
        active_iter = None
        for i, line in enumerate(script.lines):
            iter = m.append((line, ))
            if i == active_line:
                active_iter = iter
        tv = self.io_script_tv
        if active_iter is not None:
            tv.set_cursor(m.get_path(active_iter))
        else:
            tv.set_cursor(m.get_path(m.get_iter_first()))

    def update_io_script_display(self, script=None):
        """
        update display of currently selected process io-scripts
        """
        p = self.current_p
        if script is None:
            script_name = self.get_active_io_script()
            if not script_name:
                return
            script = p.io_scripts.scripts[script_name]
        current_line = getattr(script, "_gui_current_line", 0)
        is_executing = script.is_executing(getattr(script, "_gui_mode", "stopped"))
        is_ready = p.state == "ready" or (not p.has_ready_state() and p.state == "started")

        self.io_script_toolbar.set_sensitive(is_ready)
        m = self.io_script_cb.get_model()
        m[(p.io_script_cb_cache[script.name], )][0] = script.name + (" (executing)" if is_executing else "")
        if self.get_active_io_script() != script.name:
            self.io_script_cb.set_active(p.io_script_cb_cache[script.name])
        self.io_script_cb.set_sensitive(not is_executing)

        tv = self.io_script_tv
        tv.set_cursor((current_line, ))
        tv.set_sensitive(not is_executing)
        self.io_script_toggle_execute_ignore = True
        btn = self.io_script_toggle_execute
        btn.set_active(is_executing)
        if is_executing:
            btn.set_stock_id("gtk-media-stop")
            N = self.io_script_m.iter_n_children(None)
            self.process_io_script_state.set_markup("io_script: %s (%.0f%%)" % (script.name, (current_line + 1) / float(N) * 100))
            self.process_io_script_state.show()
        else:
            btn.set_stock_id("gtk-media-play")
            self.process_io_script_state.hide()

        self.io_script_toggle_execute_ignore = False

    def on_io_script_update(self, pname, script_name, mode, current_line):
        #print("io script update: %r, %r %r %r" % (
        #    pname, script_name, mode, current_line))
        if pname == self.current_p.name:
            p = self.current_p
        else:
            p = self.sysconf.processes[pname]
        script = p.io_scripts.scripts[script_name]
        is_executing = script.is_executing(mode)
        if is_executing:
            p._gui_io_script_last_active = script_name
        script._gui_mode = mode
        script._gui_current_line = current_line
        if p != self.current_p:
            return
        self.update_io_script_display(script)
        return True

    def on_io_script_toggle_execute_clicked(self, btn):
        if getattr(self, "io_script_toggle_execute_ignore", False):
            return
        want_exec = btn.get_active()
        active = self.get_active_io_script()
        if not active:
            return
        if not want_exec:
            self.manager_proxy("stop_io_script", pname=self.current_p.name, script=active)
            return

        tv = self.io_script_tv
        path, col = tv.get_cursor()
        line = path[0]
        self.manager_proxy("start_io_script", pname=self.current_p.name, script=active, line=line)

    def on_io_script_restart_clicked(self, btn):
        active = self.get_active_io_script()
        if not active:
            return
        tv = self.io_script_tv
        tv.set_cursor((0, ))
        self.manager_proxy("start_io_script", pname=self.current_p.name, script=active, line=0)

    def on_io_script_single_step_clicked(self, btn):
        active = self.get_active_io_script()
        if not active:
            return
        tv = self.io_script_tv
        path, col = tv.get_cursor()
        self.manager_proxy("start_io_script", pname=self.current_p.name, script=active, line=path[0], single=True, single_inc=True)

    def on_io_script_tv_row_activated(self, tv, path, col):
        active = self.get_active_io_script()
        if not active:
            return
        p = self.current_p
        is_ready = p.state == "ready" or (not p.has_ready_state() and p.state == "started")
        if not is_ready:
            return
        self.manager_proxy("start_io_script", pname=self.current_p.name, script=active, line=path[0], single=True)
