# This file is part of links and nodes.
# 
# links and nodes is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# links and nodes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
# 
# Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon


import re
import os
import sys
import traceback
import argparse
import fnmatch
from collections import OrderedDict

from links_and_nodes_base.glib_mainloop import GLibMainloop

from . import ManagerConnection
from . import Logging
from . import terminal
from . import cmdline_parse
from .CommunicationUtils import callback, ignore_cb, check_for_display_unix_socket, get_xauthority, get_xcookie
from .py2compat import unicode
from .tools import get_default_glib_mainloop

import gi
gi.require_version('GLib', '2.0')
from gi.repository import GLib # noqa: E402

def cmd_aliases(*list_of_aliases):
    def cmd_alias_decorator(method):
        method.aliases = list_of_aliases
        return method
    return cmd_alias_decorator

class argument(object):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        if args[0][0] == "-" and "action" not in self.kwargs:
            self.kwargs["action"] = "store_true"

def format_dict(data, indent_level=0, prefix=None):
    ret = []
    indent = " " * indent_level
    if prefix is None:
        prefix = indent
    for key, value in data.items():
        if isinstance(value, (dict, OrderedDict)):
            ret.append("%s%s:\n%s" % (indent, key, format_dict(value, indent_level=indent_level + 1)))
        elif isinstance(value, list) and len(value) > 0 and isinstance(value[0], (dict, OrderedDict)):
            ret.append("%s%s:" % (indent, key))
            for item in value:
                ret.append("%s%s" % (indent, format_dict(item, indent_level=indent_level + 1, prefix="%s -" % indent)))
        else:
            if "handle" in key:
                ret.append("%s%s: %#x" % (indent, key, value))
            else:
                ret.append("%s%s: %r" % (indent, key, value))
    return prefix + ("\n%s" % indent).join(ret)

def completion(sequence=None, arguments=None):
    """
    decorator to provide completion hints
    """
    if sequence is None:
        sequence = []
    def completion_decorator(fcn):
        fcn.completion_hints = sequence
        if not arguments:
            return fcn
        p = argparse.ArgumentParser(
            fcn.__name__[4:], description=fcn.__doc__,
            add_help=False
        )
        for arg in arguments:
            action = p.add_argument(*arg.args, **arg.kwargs)
            if "-" in action.dest and action.metavar is None:
                action.metavar = action.dest
                action.dest = action.dest.replace("-", "_")
        fcn.arg_parser = p
        return fcn
    return completion_decorator
class ArgumentError(Exception):
    pass

class ManagerConsole(ManagerConnection.ManagerConnection):
    def __init__(self, manager=None, connect=None, mi=False):
        super(ManagerConsole, self).__init__(manager, connect,
                                         mainloop=GLibMainloop(loop=get_default_glib_mainloop()))
        if manager:
            self.manager.console = self

        self.mi = mi
        self.variables = {}
            
        self._prompt_input_idle = None
        self._prompt_input_queue = []
        self._print_commands = False
        self._log_output_enabled = False
        
        self._blocking_commands_running = set()
        self._blocking_waits_with_timeout = {}
        
        self._enabled_outputs = set()
        self._enabled_outputs_encoding = dict()
        self._attached_to = None
        self._outputs_after_detach = [] # only used for notifications [(what, msg)]
        self._enabled_notifications = set(["update_obj_state", "process_errors", "new_process_warning"])

        self._notification_aliases = dict(
            state="update_obj_state",
            requested_state="update_obj_requested_state",
            process_info="set_process_info",
            errors="process_errors",
            warnings="new_process_warning",
        )

        self._register_cmd_aliases()
        
        self._running_states = "starting...", "started", "ready"
        self._started_states = "started", "startup error"
        self._stopped_states = "stopped",
        self.state_colors = {
            "unknown": "white",
            "ready": "green",
            "started": "green",
            "started_towards_ready": "bright_yellow",
            "starting...": "yellow",
        }
        
        Logging.stdout_write = self.output
        self.collected_output = []
        self.cmd_log(argparse.Namespace(enable="disable"))
        
        self.connect_to_manager()
        if self.connect:
            DISPLAY = os.getenv("DISPLAY")
            self.request(("connection", "set_member"), "DISPLAY", DISPLAY)

            if DISPLAY is not None:
                have_display_unix_socket = check_for_display_unix_socket(DISPLAY)
                self.request(("connection", "set_member"), "DISPLAY_UNIX_SOCKET", have_display_unix_socket)

                XAUTHORITY = get_xauthority()
                if os.path.isfile(XAUTHORITY):
                    self.request(("connection", "set_member"), "XAUTHORITY", XAUTHORITY)

                xcookie = get_xcookie()
                self.request(("connection", "set_member"), "xcookie", xcookie)
            else:
                pass
                #self.request(("connection", "set_member"), "DISPLAY_UNIX_SOCKET", None)
                ##self.request(("connection", "set_member"), "XAUTHORITY", None)
                #self.request(("connection", "set_member"), "xcookie", None)
                
            
        self._set_sysconf(self.manager_proxy(request="get_system_configuration"))

        histfile = os.path.join(
            os.path.expanduser("~"), ".ln_manager_console_history_%s" % self.sysconf.instance_config.name)
        
        self.term = terminal.terminal(history_fn=histfile)
        self.term.do_restore_prompt = not self.mi
        self._reset_terminal(erase=False)
        self.term.set_tab_callback(self.on_tab)
        self.term.set_resize_callback(self.on_resize)
        
        self.term._after_terminal_hook = lambda: self.call_hook("after_terminal_input")
        self.term.start_reader()

        self.set_window_title("lnm: %s at %s" % (
            self.sysconf.instance_config.name,
            self.sysconf.instance_config.manager))
        
        self.group_path = []
        
        # is there a toplevel group?
        all_groups = self.get_toplevel_groups()
        if all_groups:
            self.cmd_cd([all_groups[0]])

        self.output("\nln_manager console. type \"help\" for more information or \"quit\" or Ctrl-D to exit.\n")
        self.term.prompt_answer = self.on_prompt_input

        if "--console-exec" in sys.argv:
            p = sys.argv.index("--console-exec")
            cmd = sys.argv[p + 1]
            self.on_prompt_input(cmd)
        else:
            self.prompt()

    def in_error_color(self, msg):
        return self.term.in_color("bright_red", msg)
    def in_warning_color(self, msg):
        return self.term.in_color("bright_yellow", msg)

    def _set_sysconf(self, sysconf):
        self.sysconf = sysconf
        self._obj_containers = dict(
            Process=self.sysconf.processes,
            Group=self.sysconf.groups,
            State=self.sysconf.states
        )
        
    def collect_output(self, what, flush=False):
        self.collected_output.append(what)
        
    def output(self, what, flush=False):
        self.term.output(what, flush=flush)

    def prompt(self):
        gp = "/".join(self.group_path)
        if self.mi:
            self.term.prompt("## ready: %s ##\n" % gp)
        else:
            self.term.prompt("%s>>> " % gp)
        
    def error(self, msg):
        self._notification_msg("error", msg)

    def log_output(self, ts, level, src, msg, stack):
        if not self._log_output_enabled:
            return
        self._notification_msg("log", "%s %s %s: %s\n" % (ts.strftime("%Y-%m-%d %H:%M:%S"), level, src, msg))

    def _notification_msg(self, what, msg):
        if self._attached_to is None:
            if self.mi:
                self.mi_notify(what, msg)
            else:
                if "error" in what:
                    what = self.in_error_color(what)
                elif "warning" in what:
                    what = self.in_warning_color(what)
                self.output("%s: %s\n" % (what, msg))
        else:
            self._outputs_after_detach.append((what, msg))
        
    def _show_notification(self, msg):
        if self._attached_to is not None:
            return
        request = msg.get("request")
        show = "all" in self._enabled_notifications or request in self._enabled_notifications
        if not show:
            return

        if request == "update_obj_state":
            return # will be printed in _update_object_state
        
        if request == "process_errors":
            for pname, error_list in msg["errs"].items():
                for error in error_list:
                    if error[0] == "terminated":
                        reason = "process terminated unexpectedly: %s" % error[1]
                    else:
                        reason = "%s: %s" % error
                    self._notification_msg("process error", "%s: %s" % (pname, reason))
                    self.call_hook(("process error", pname), pname, reason)
            return
        if request == "new_process_warning":
            pname = msg["name"]
            warning = msg["warnings"][-1] # only show last
            reason = "%s: %s" % warning
            self._notification_msg("process warning", "%s: %s" % (pname, reason))
            return
        
        nm = dict(msg)
        what = repr(nm)
        if "request" in nm:            
            del nm["request"]
        nmk = list(nm.keys())
        nmk.sort()
        nm = ", ".join(["%s=%r" % (k, nm[k]) for k in nmk])
        self._notification_msg(request, nm)

    def _update_object_state(self, msg):        
        obj = self._find_obj(msg)
        if obj is not None:
            obj.state = msg["obj_state"]
        
        self.call_hook("object_state_change", msg["obj_name"], msg["obj_state"])
        
        show = "all" in self._enabled_notifications or "update_obj_state" in self._enabled_notifications        
        if not show:
            return

        state = msg["obj_state"]
        if state is None:
            state = "unknown"
        color = self._get_state_color(msg["obj_type"], state, has_ready_state=self._has_ready_state(obj))
        state = self.term.in_color(color, state)
        self._notification_msg("state change", "%s: %s" % (msg["obj_name"], state))
        
    def _update_state_state(self, msg):
        state_obj = self._find_state(msg["state_name"])
        state = msg["up_down"]
        if state is None:
            state = "unknown"
        color = self._get_state_color("State", state, has_ready_state=False)
        state = self.term.in_color(color, state)
        self._notification_msg("state change", "%s: %s" % (msg["state_name"], state))
        self.call_hook("object_state_change", msg["state_name"], state)

    def on_notification(self, msg):
        """
        only called from lnm_remote with remote connection
        """
        self._gui_notification(self, msg)

    def _gui_notification(self, manager_or_self, msg):
        #self.output("got gui notification: %r\n" % msg)
        self._show_notification(msg)
        
        request = msg.get("request")
        if request == "update_obj_state":
            self._update_object_state(msg)
        if request == "update_state_state":
            self._update_state_state(msg)
        if request == "set_process_info":
            p = self.sysconf.processes.get(msg["name"])
            if p is None:
                self.output("warning: received process info for unknown process %r!\n" % msg["name"])
            else:
                p.__dict__.update(msg) # not so nice because of the "request" field

        elif request == "new_config":
            self._set_sysconf(msg["new_config"])
            
        elif request == "log_msg":
            for msg_item in msg["msgs"]:
                self.log_output(*msg_item[1:])
            return True

        if request == "process_output":
            name = msg["name"]
            output = msg["output"]
            if self._attached_to is None:
                if name in self._enabled_outputs:
                    if self.mi:
                        self.mi_notify("process output %s" % name, output.decode(self._enabled_outputs_encoding.get(name, "utf-8")))
                    else:
                        self.output(output)
            self.call_hook(("process_output", name), output)
            return True
        
        self.call_hook(("notification", request), msg)
        return True

    def debug(self, msg):
        self.mi_notify("log", msg)

    def on_prompt_input(self, cmdline, sync=False, insert_at=None):
        """
        cmdline is supposed to be str
        """
        self.debug("console: on_prompt_input: %r, insert_at: %r, sync: %r" % (cmdline, insert_at, sync))
        try:
            for cmd in cmdline_parse.cmdline_parse(cmdline):
                if insert_at is not None:
                    self._prompt_input_queue.insert(insert_at, cmd)
                    insert_at += 1
                else:
                    self._prompt_input_queue.append(cmd)
        except Exception:
            self.output("error reading command line:\n%s\n" % traceback.format_exc())
            return self._on_prompt_input_idle()
        self.debug("prompt_input_queue len: %d, self._prompt_input_idle: %r" % (
            len(self._prompt_input_queue),
            self._prompt_input_idle
        ))

        if sync:
            return self._on_prompt_input_idle()
            
        if self._prompt_input_idle is None:
            self._prompt_input_idle = GLib.idle_add(self._on_prompt_input_idle)

    def _on_prompt_input_idle(self):
        self.debug("_on_prompt_input_idle len %d" % len(self._prompt_input_queue))
        if self._prompt_input_queue:
            cmd = self._prompt_input_queue.pop(0)

            if cmd[0] == "disable-print-commands":
                self._print_commands = False
            else:
                if isinstance(cmd, tuple):
                    # internal command
                    if cmd[0] == "stop_blocking":
                        self._stop_blocking_command(cmd[1])
                    else:
                        self.output("unknown internal command: %r" % (cmd, ))
                else:
                    #self.output("# got command: %s\n\n" % cmd)
                    self.debug("cmd: %r" % (cmd, ))
                    if cmd:
                        if self._print_commands:
                            if self.mi:
                                self.mi_notify("exec-cmd", "%r" % " ".join(map(repr, cmd)))
                            else:
                                self.output("# execute command: %s\n" % " ".join(map(repr, cmd)))
                        self._execute_command(cmd)
        else:
            if not self.mi:
                self.output("idle called without command from stack:\n%s\n" % "".join(traceback.format_stack()))
        
        if not self._blocking_commands_running and len(self._prompt_input_queue) == 0:
            #self.output("showing prompt because no more blocking commands and queue is empty:\n%s\n" % "".join(traceback.format_stack()))
            self.prompt()

        if len(self._prompt_input_queue) == 0 or self._blocking_commands_running:
            # wait for next command or until all blocking commands finish
            #self.output("stop idle: either cmd input queue is empty (%d) or we have blocking commands! (%s)\n" % (
            #    len(self._prompt_input_queue), bool(self._blocking_commands_running)))
            self._prompt_input_idle = None
            return False # wait either for new command or until blcking command finishes
        #self.output("no more blocking commands! queue len: %d\n" % len(self._prompt_input_queue))
        return True # process next command

    def argument_error(self, message):
        raise ArgumentError(message)
    
    def _execute_command(self, cmd):
        #self.output("execute command: %r\n" % cmd)
        self.debug("_execute_command: %r" % (cmd, ))

        if len(cmd) > 1:
            verb, args = cmd[0], cmd[1:]
        else:
            verb, args = cmd[0], []

        if Logging.stdout_write == self.collect_output:
            # leave markers in log which command was executed when
            self.collect_output("\n# console command: %s\n\n" % cmd)

        method = getattr(self, "cmd_%s" % verb, None)
        if method is None:
            method = self._cmd_aliases.get(verb)
        if method is None:
            if self.mi:
                self.mi_error("unknown command %r" % cmd)
            else:
                self.output("unknown / invalid command: %r, enter 'help' for list of valid commands!\n" % cmd)
            return
        
        try:
            if hasattr(method, "arg_parser"):
                method.arg_parser.error = lambda msg: self.argument_error(msg)
                try:
                    opts = method.arg_parser.parse_args(args)
                except ArgumentError as e:
                    if not self.mi:
                        self.output("%s" % method.arg_parser.format_usage())
                    self.mi_error("%s: %s" % (method.arg_parser.prog, str(e)))
                    return
                method(opts)
            elif args:
                method(args)
            else:
                method()
        except SystemExit:
            raise
        except Exception:
            self.mi_error("executing %r:\n%s" % (cmd, traceback.format_exc()))
        
    def trim(self, docstring):
        if not docstring:
            return ''
        # Convert tabs to spaces (following the normal Python rules)
        # and split into a list of lines:
        lines = docstring.expandtabs().splitlines()
        # Determine minimum indentation (first line doesn't count):
        indent = sys.maxsize
        for line in lines[1:]:
            stripped = line.lstrip()
            if stripped:
                indent = min(indent, len(line) - len(stripped))
        # Remove indentation (first line is special):
        trimmed = [lines[0].strip()]
        if indent < sys.maxsize:
            for line in lines[1:]:
                trimmed.append(line[indent:].rstrip())
        # Strip off trailing and leading blank lines:
        while trimmed and not trimmed[-1]:
            trimmed.pop()
        while trimmed and not trimmed[0]:
            trimmed.pop(0)
        # Return a single string:
        return '\n'.join(trimmed)

    def _register_cmd_aliases(self):
        self._cmd_aliases = {}
        for name in dir(self):
            if name.startswith("cmd_"):
                method = getattr(self, name)
                if hasattr(method, "aliases"):
                    for alias in method.aliases:
                        self._cmd_aliases[alias] = method
        
    def _get_possible_commands(self, with_aliases=False):
        possible_commands = []
        for name in dir(self):
            if name.startswith("cmd_"):
                possible_commands.append(name[4:])
                if with_aliases:
                    method = getattr(self, name)
                    if hasattr(method, "aliases"):
                        possible_commands.extend(method.aliases)
        possible_commands.sort()
        return possible_commands

    def mi_response(self, body):
        if not self.mi:
            self.output(body)
            return
        if "\n" in body:
            self.output("### response #\n%s\n###\n" % body)
        else:
            self.output("## response # %s\n" % body)
    def mi_notify(self, what, body):
        if body.count("\n") > 1:
            self.output("### notify # %s #\n%s\n###\n" % (what, body))
        else:
            self.output("## notify # %s # %s\n" % (what, body.rstrip("\n")))
    def mi_error(self, msg):
        if not self.mi:
            # instead print it human readable
            self.output("error: %s\n" % msg)
            return
        
        if "\n" in msg:
            self.output("### error #\n%s\n###\n" % msg)
        else:
            self.output("## error # %s\n" % msg)
        
    def cmd_help(self, args=None):
        """ show this help """

        if args is not None:
            name = args[0]
            mname = "cmd_%s" % name
            method = getattr(self, mname, None)
            if method is not None:
                if hasattr(method, "arg_parser"):
                    help = method.arg_parser.format_help() + "\n"
                else:
                    help = self.trim(method.__doc__) + "\n"
                self.mi_response(help)
                return
            self.mi_error("unknown command %r!" % name)

        possible_commands = []
        nl = 0
        for name in self._get_possible_commands():
            method = getattr(self, "cmd_%s" % name)
            possible_commands.append((name, method.__doc__.lstrip().split("\n")[0]))
            tl = len(name)
            if tl > nl:
                nl = tl
        possible_commands = "\n".join(["  %-*.*s   %s" % (nl, nl, name, doc) for name, doc in possible_commands])
        if self.mi:
            self.mi_response("possible commands:\n%s" % possible_commands)
            return
        self.output("""usage: help [command]

possible commands:
%s
""" % possible_commands)

    @cmd_aliases("exit")
    def cmd_quit(self):
        """ terminate this console (and possibly ln_manager) """
        sys.stdout.write("\n")
        self.debug("do call sys.exit(0)!!")
        sys.exit(0)

    def cmd_pdb(self):
        """ call pdb.set_trace() """
        self.term.enable_echo(True)
        import pdb
        pdb.set_trace()
        self.term.enable_echo(False)
        
    @completion(["group"])
    def cmd_cd(self, target):
        """
        select a new group as the currently selected group

        use "cd .." to select the parent group
        """
        target = target[0]
        
        if target == "..":
            if self.group_path:
                del self.group_path[-1]
            return
        if target == "/":
            self.group_path = []
            return

        if self.group_path:
            target_abs = "/".join(self.group_path) + "/" + target
        else:
            target_abs = target
        if target_abs not in self.sysconf.groups:
            self.mi_error("unknown group %r!" % target)
            return
        
        self.group_path.append(target)
    
    def get_toplevel_groups(self):
        all_groups = list(self.sysconf.groups.keys())
        all_groups.sort()
        all_groups.sort(key=lambda n: "/" in n)
        return all_groups

    def _get_state_color(self, obj_type, state, has_ready_state=False):
        if obj_type == "Process" and has_ready_state and state == "started":
            state = "started_towards_ready"
        if state is None:
            state = "unknown"
        return self.state_colors.get(state, "")

    def _has_ready_state(self, obj):
        return hasattr(obj, "has_ready_state") and obj.has_ready_state()

    def _is_ready(self, obj, state=None):
        """
        return True if process has ready state defined AND reached it,
        also return True if process has no ready state defiend but is started
        """
        if state is None:
            state = obj.state
        if self._has_ready_state(obj):
            return state == "ready"
        return state == "started"
    
    @completion(arguments=[argument("-R", "--recursive", help="recursively list all processes and groups")])
    def cmd_ls(self, opts):
        """
        list child processes and groups of current group

        entries that have a * attached have their output enabled.
        """

        items = []

        def get_group_states(g):
            states = []
            for p in g.group_members:
                cn = p.__class__.__name__
                if cn == "Group":
                    states.extend(get_group_states(p))
                    continue
                state = p.state
                if state in ("<unknown>", None):
                    state = "unknown"
                if cn == "Process":
                    color = self._get_state_color(cn, state, has_ready_state=self._has_ready_state(p))
                    states.append((state, color))
                elif cn == "State":
                    color = self._get_state_color(cn, "%s/%s" % (state, p.requested_state))
                    states.append((state, color))
            return states
        def list_group(g, indent=0):
            grp_abs = g.name
            indent_spaces = " " * indent
            
            # add processes inside this group
            childs = []
            for p in g.group_members:
                n = p.display_name
                cn = p.__class__.__name__
                if cn == "Group":
                    type_hint = "/"
                    states = get_group_states(p)
                    state_count = dict()
                    for key in states:
                        if key not in state_count:
                            state_count[key] = 1
                        else:
                            state_count[key] += 1
                    keys = list(state_count.keys())
                    keys.sort()
                    states = []
                    for state, color in keys:
                        if state is None:
                            state = "unknown"
                        states.append("%s:%s" % (self.term.in_color(color, state), state_count[key]))
                    state = ", ".join(states)
                elif cn == "State":
                    type_hint = "@"
                    state = p.state
                elif cn == "Process":
                    type_hint = ""
                    state = p.state
                else:
                    type_hint = ""
                    state = p.state

                if state is None:
                    state = "unknown"
                
                color = self._get_state_color(cn, state, has_ready_state=self._has_ready_state(p))
                state = " (%s)" % self.term.in_color(color, state)

                if p.name in self._enabled_outputs:
                    type_hint += "*"
                
                items.append("%s%s%s%s" % (indent_spaces, n, type_hint, state))
                if opts.recursive and cn == "Group":
                    list_group(p, indent=indent + len(g.display_name) + 1)
                

        if self.group_path:
            grp_abs = "/".join(self.group_path)
            g = self.sysconf.groups[grp_abs]
            list_group(g)
        else:
            for grp_abs in self.get_toplevel_groups():
                if "/" in grp_abs:
                    continue
                g = self.sysconf.groups[grp_abs]
                items.append("%s/" % g.display_name)
                list_group(g, indent=len(g.display_name) + 1)
            
        self.mi_response("\n".join(items) + "\n")

    def _find_obj(self, msg, complain=True):
        container = self._obj_containers.get(msg["obj_type"])
        if container is None:
            if complain: self.mi_error("unknown object type %r!" % msg["obj_type"])
            return None
        obj = container.get(msg["obj_name"])
        if obj is None:
            if complain: self.mi_error("unknown %s %r!" % (msg["obj_type"].lower(), msg["obj_name"]))
            return None
        return obj
        
    def _find(self, container, process, singular, complain=True):
        gp = list(self.group_path)
        while gp:
            grp_abs = "/".join(gp)
            proc_abs = grp_abs + "/" + process

            p = container.get(proc_abs)
            if p is not None: return p
            del gp[-1]
        proc_abs = process
        p = container.get(proc_abs)
        if p is not None: return p        
        if complain: self.mi_error("unknown %s %r!" % (singular, process))
        
    def _find_process(self, name, complain=True):
        return self._find(self.sysconf.processes, name, "process", complain=complain)
    def _find_state(self, name, complain=True):
        return self._find(self.sysconf.states, name, "state", complain=complain)
    def _find_group(self, name, complain=True):
        return self._find(self.sysconf.groups, name, "group", complain=complain)
    def _find_any(self, name):
        p = self._find_process(name, complain=False)
        if p is not None: return p
        p = self._find_group(name, complain=False)
        if p is not None: return p
        p = self._find_state(name, complain=False)
        if p is not None: return p
        self.mi_error("unknown object of name %r!" % name)
    def _find_process_or_group(self, name):
        p = self._find_process(name, complain=False)
        if p is not None: return p
        p = self._find_group(name, complain=False)
        if p is not None: return p
        self.mi_error("unknown process or group of name %r!" % name)

    #@completion(["opts-parser", "process-or-group"])
    @completion(["process-or-group"],
                arguments=[
                    argument("-a", "--attach", help="attach to that process after start"),
                    argument("-i", "--inline", help="show process output inline on this console"),
                    argument("-nb", "--non-blocking", help="do not wait until process is started"),
                    argument("-r", "--ready", dest="wait_for_ready", help="wait until process reports ready state"),
                    argument("-t", "--timeout", default=10, metavar="N", action="store", type=float, help="wait timeout in seconds"),
                    argument("process-or-group", nargs="+", help="the name of a process or group to start"),
                ])
    def cmd_start(self, opts):
        """
        start one or more processes or groups
        """
        
        if opts.inline: opts.attach = False

        procs = []
        started_processes = dict()
        for pn in opts.process_or_group:
            p = self._find_process_or_group(pn.strip())
            if p is None: return

            is_group = p.__class__.__name__ == "Group"
            if is_group:
                members = self._get_group_members(p)
                procs.extend(members)
                for gpname in members:
                    gp = self._find_process_or_group(gpname)
                    self._enabled_outputs_encoding[gp.name] = gp.output_encoding
            else:
                procs.append(p.name)
                self._enabled_outputs_encoding[p.name] = p.output_encoding
        all_procs = list(procs)

        if opts.inline:
            self._enabled_outputs.update(procs)
        else:
            self._enabled_outputs = self._enabled_outputs.difference(procs) # disable inline output for these procs!
            if len(all_procs) == 1 and opts.attach:
                if all_procs[0] in self._enabled_outputs:
                    self._enabled_outputs.remove(all_procs[0])
        
        myself = ("start", tuple(opts.process_or_group))

        have_state_change = None
        if not opts.non_blocking or opts.wait_for_ready:
            def on_timeout():
                self.mi_error("timeout waiting for start of these procs: %r" % procs)

            self._blocking_wait_with_timeout(opts.timeout, myself)

            self._saw_startup = False
            # todo: this should be a Job object
            def on_state_change(msg, name, state):
                if state is None:
                    return
                if name in procs:
                    p = started_processes.get(name)
                    if not p:
                        return True
                    if (state in self._started_states or state == "ready" or "error" in state):
                        #self.output("start %r: new state is %r\n" % (name, state))
                        #if "error" not in state:
                        #    p._saw_startup = True
                        if opts.wait_for_ready: # only for processes
                            #if p._saw_startup and "error" in state:
                            if "error" in state:
                                self.mi_error("failed to start %r" % p.name)
                                self._stop_blocking_wait_with_timeout(myself)
                                self.remove_hooks_to(myself)
                                return False
                            if not self._is_ready(p):
                                return True # continue to wait
                        procs.remove(name)
                        if len(procs) == 0:
                            self._saw_startup = True
                            if opts.attach and len(all_procs) == 1:
                                def _delayed_attach(pname=all_procs[0]):
                                    self._execute_command(["attach", pname])
                                    return False
                                GLib.idle_add(_delayed_attach)
                                self._start_blocking_command(("attached", p.name))
                            self._stop_blocking_wait_with_timeout(myself)
                            self.remove_hooks_to(myself)
                            return False
                        
                return True # continue to wait
            self.add_hook("object_state_change", on_state_change, from_who=myself)
            have_state_change = on_state_change

        def on_process_error(con, pname, error):
            p = started_processes.get(pname)
            if not p:
                return False
            self.mi_error("failed to start %r: %s" % (pname, error))
            self._stop_blocking_wait_with_timeout(myself)
            self.remove_hooks_to(myself)
            return False
            
        for pn in all_procs:
            p = self._find_process(pn)
            #p._saw_startup = False
            started_processes[p.name] = p
            if opts.wait_for_ready:
                self.add_hook(("process error", p.name), on_process_error, from_who=myself)
            self._req_state(p, "start")
            if have_state_change:
                have_state_change(dict(), p.name, p.state)

        
    @completion(["process-or-group"],
                arguments=[
                    argument("-w", "--wait", help="wait for processes to stop"),
                    argument("-t", "--timeout", default=15, metavar="N", action="store", type=float, help="wait timeout in seconds"),
                    argument("process-or-group", nargs="+", help="the name of a process or group to stop"),
                ])
    def cmd_stop(self, opts):
        """
        stop a process or all processes of a group
        """

        procs = []
        for pn in opts.process_or_group:
            p = self._find_process_or_group(pn.strip())
            if p is None: return

            is_group = p.__class__.__name__ == "Group"
            if is_group:
                procs.extend(self._get_all_processes_within_group(p, only_names=True))
            else:
                procs.append(p.name)
                
        for pn in procs:
            p = self._find_process(pn)
            self._req_state(p, "stop")

        if opts.wait:
            opts.start = opts.ready = False
            self.cmd_wait(opts)
        
    def _get_group_members(self, g):
        """
        recursively return all (non-group) members of given group
        """
        ret = []
        for member in g.group_members:
            if member.__class__.__name__ == "Group":
                ret.extend(self._get_group_members(member))
            else:
                ret.append(member.name)
        return ret
        
    def _get_all_processes_within_group(self, g, only_names=False):
        """
        recursively return all (non-group) processes of given group
        """
        ret = []
        for member in g.group_members:
            cn = member.__class__.__name__
            if cn == "Group":
                ret.extend(self._get_all_processes_within_group(member, only_names=only_names))
            elif cn == "Process":
                if only_names:
                    ret.append(member.name)
                else:
                    ret.append(member)
        return ret
        
    def _req_state(self, obj, req_state):
        if obj.__class__.__name__ == "Group":
            for member in obj.group_members:
                if member.__class__.__name__ in ("Group", "Process"):
                    self._req_state(member, req_state)
            return
        self.manager_proxy(
            "set_process_state_request",
            ptype=obj.__class__.__name__,
            pname=obj.name,
            requested_state=req_state,
            ln_debug=getattr(obj, "ln_debug", False)
        )
        obj.last_requested_state = req_state
        obj.requested_state = req_state

    @completion(arguments=[
        argument("enable", choices=["enable", "disable"], default=None, nargs="?", help="whether to enable/disable log message display") ])
    def cmd_log(self, opts):
        """ 
        enable / disable log message display 
        """

        if opts.enable is None:
            self.mi_response("".join(self.collected_output))
            self.collected_output = []
        else:
            enable = opts.enable == "enable"
            self._log_output_enabled = enable
            if enable:
                if self.mi:
                    Logging.stdout_write = lambda msg, flush=False: self.mi_notify("log", msg.rstrip("\n"))
                else:
                    Logging.stdout_write = self.output
            else:
                self.collected_output = []
                Logging.stdout_write = self.collect_output
        
    @completion(["file"], arguments=[argument("file", nargs="+", help="which files to read")])    
    def cmd_source(self, opts):
        """
        read a script file and execute commands as if they were entered on the console
        """

        insert_pos = 0
        self._print_commands = True
        for fn in opts.file:
            if not os.path.isfile(fn):
                self.mi_error("file %r not found!" % fn)
                return

            myself = ("source", fn)
            with open(fn, "r") as fp:
                for line in fp:
                    line = line.strip()
                    if not line:
                        continue
                    if line[0] == "#":
                        #self.output("%s\n" % line)
                        continue
                    before = len(self._prompt_input_queue)
                    self.on_prompt_input(line, sync=False, insert_at=insert_pos)
                    added = len(self._prompt_input_queue) - before
                    insert_pos += added
        self.on_prompt_input("disable-print-commands", sync=False, insert_at=insert_pos)
    
    @completion(["on_off"], arguments=[argument("on_off", default="on", help="whether to print commands")])    
    def cmd_set_print_commands(self, opts):
        """
        enables or disabled echoing of incoming commands
        """
        self._print_commands = opts.on_off.lower() in ("yes", "true", "on", "1")
    
    def _start_blocking_command(self, hint):
        self._blocking_commands_running.add(hint)
    def _stop_blocking_command(self, hint, show_prompt=True):
        self._blocking_commands_running.remove(hint)
        if self._blocking_commands_running:
            return
        
        if self._prompt_input_queue and self._prompt_input_idle is None:
            # restart command executor idle
            self._prompt_input_idle = GLib.idle_add(self._on_prompt_input_idle)
        elif show_prompt:
            # immideately show prompt
            self.prompt()
            
    def _blocking_wait_with_timeout(self, timeout, hint, on_timeout_cb=None, is_error=True):
        def on_timeout():
            if is_error: self.mi_error("blocking_wait of %r timed out!" % (hint, ))
            self._stop_blocking_command(hint)
            self.remove_hooks_to(hint)
            if on_timeout_cb:
                on_timeout_cb()
            return False        
        timeout_id = GLib.timeout_add(int(float(timeout) * 1000), on_timeout)
        self._blocking_waits_with_timeout[hint] = timeout_id
        self._start_blocking_command(hint)
        return timeout_id
    
    def _stop_blocking_wait_with_timeout(self, hint, show_prompt=True):
        timeout_id = self._blocking_waits_with_timeout[hint]
        del self._blocking_waits_with_timeout[hint]
        GLib.source_remove(timeout_id)            
        self._stop_blocking_command(hint, show_prompt=show_prompt)

    class ManagerCall(object):
        def __init__(self, con, call, cb, kwargs):
            self.con = con
            self.call = call
            self.cb = cb
            self.__dict__.update(kwargs)
            self.myself = (call, tuple(kwargs.items()))
            self._is_waiting = False
            self._unblock_show_prompt = False
            self.con._blocking_wait_with_timeout(4, self.myself, callback(self, exception="timeout"))
            self.con.manager_proxy("mgr_cb", call, self, **kwargs)
            self._unblock_show_prompt = True

        def __call__(self, info=None, exception=None):
            if exception is not None:
                self.cb(self, exception=exception)
            else:
                self.cb(self, info=info)
            self.con._stop_blocking_wait_with_timeout(self.myself, show_prompt=self._unblock_show_prompt)

    def manager_call(self, call_name, cb, **kwargs):
        return self.ManagerCall(self, call_name, cb, kwargs)

    @completion(["process-or-state"], arguments=[
        argument("-f", "--follow", help="to keep following the output until user presses enter"),
        argument("-n", "--n-lines", default=15, type=int, action="store", help="how many lines to display (default 15)"),
        argument("process-or-state", help="which files to read")])
    def cmd_tail(self, opts):
        """
        show last lines of process output
        """
        
        p = self._find_process(opts.process_or_state)
        if not p:
            return

        data = self.manager_proxy("get_process_output_lines", p.name, opts.n_lines)
        opts.follow = opts.follow and not self.mi
        if opts.follow:
            self.output("press ENTER to stop listing output from this process!\n")
            
        self.mi_response(data.decode(p.output_encoding))
        
        if opts.follow:
            def on_output(con, output):
                self.output(output.decode(p.output_encoding))
                return True
            myself = ("tail", p.name)
            self.add_hook(("process_output", p.name), on_output, from_who=myself)

            # now wait for ENTER
            def on_terminal_input(con):
                if "\n" not in self.term.input_queue:
                    return True
                # stop blocking command!
                self.term.input_queue = []
                self.remove_hooks_to(myself)
                self._stop_blocking_command(myself)
                return False
            
            self._start_blocking_command(myself)
            self.add_hook("after_terminal_input", on_terminal_input, from_who=myself)

    @completion(["process-or-state"], arguments=[
        argument("process-or-state", help="show output of this process/state")])
    def cmd_output(self, opts):
        """
        enable display of process output in this console for give process/state.
        use ``silence <process-name>`` to stop it again.
        """
        p = self._find_process(opts.process_or_state)
        if not p:
            return
        self._enabled_outputs_encoding[p.name] = p.output_encoding
        self._enabled_outputs.add(p.name)
        
    @completion(["process-or-state"], arguments=[
        argument("process-or-state", nargs="?", help="silence output of this process/state. if not given, silence all processes.")])
    def cmd_silence(self, opts   ):
        """
        disable display of process output in this console for give process/state.
        """

        if opts.process_or_state is None:
            self._enabled_outputs = set()
            return

        p = self._find_process(opts.process_or_state)
        if not p:
            return        
        self._enabled_outputs.remove(p.name)

    def _get_name(self, p):
        if hasattr(p, "display_name"):
            return p.display_name
        return p.name
    def _get_objects_starting_with(self, kind, prefix):
        cp_ret = []

        containers = []

        def add_files(prefix, base_dir=None, search_prefix=None):
            if base_dir is None:
                if len(prefix) > 1:
                    base_dir = os.path.join(*prefix[:-1])
                    return add_files(prefix[-1], base_dir, base_dir)
                base_dir = "."
                prefix = prefix[0]
                search_prefix = ""
            entries = os.listdir(base_dir)
            entries.sort()
            for e in entries:
                if e in (".", ".."):
                    continue
                p = os.path.join(base_dir, e)
                is_dir = os.path.isdir(p)
                dir_postfix = os.sep if is_dir else ""
                is_final = not is_dir
                if e.startswith(prefix):
                    cp_ret.append((os.path.join(search_prefix, e) + dir_postfix, is_final))
        
        if kind == "any":
            containers = list(self._obj_containers.values())
            add_files(prefix.split(os.sep))
            
        elif kind == "any-object":
            containers = list(self._obj_containers.values())
            
        elif kind == "process":
            containers = [
                self._obj_containers["Process"],
            ]
        
        elif kind == "process-or-group":
            containers = [
                self._obj_containers["Process"],
                self._obj_containers["Group"]
            ]
        
        elif kind == "group":
            containers = [
                self._obj_containers["Group"]
            ]
        
        elif kind == "process-or-state":
            containers = [
                self._obj_containers["Process"],
                self._obj_containers["State"]
            ]
        
        elif kind == "file":
            add_files(prefix.split(os.sep))

        if containers:
            current_group = "/".join(self.group_path)
            if current_group:
                g = self.sysconf.groups[current_group]
                def search_group(g, prefix, path):
                    for p in g.group_members:
                        n = self._get_name(p)
                        if not prefix or n.startswith(prefix[0]):
                            np = list(path)
                            np.append(n)
                            is_group = p.__class__.__name__ == "Group"
                            is_final = not is_group
                            if kind != "group" or is_group:
                                cp_ret.append(("/".join(np), is_final))
                            if is_group: # recurse
                                search_group(p, prefix[1:], np)
                search_group(g, prefix.split("/"), [])
                cp_ret.sort()

            ret = []
            for cont in containers:
                for key in list(cont.keys()):
                    if key.startswith(prefix):
                        is_group = cont[key].__class__.__name__ == "Group"
                        is_final = not is_group
                        ret.append((key, is_final))
            ret.sort()
            cp_ret.extend(ret)
            
        return cp_ret
        
    def on_tab(self, line, cursor_pos, is_first):
        """
        line is str/unicode
        """
        at_end = len(line) == cursor_pos

        options = []

        sstart, send, search, args_before = cmdline_parse.find_completion_start(line, cursor_pos)
        before = line[:sstart]
        after = line[send:]            
        #self.output("complete: search: %r, args_before: %r\n" % (search, args_before))
        
        do_complete_command = len(args_before) == 0
        
        if do_complete_command:
            for cmd in self._get_possible_commands(with_aliases=True):
                if cmd.startswith(search):
                    options.append((cmd, True))
        else:
            completion_hints = [ "any-object" ]
            command = args_before[0]
            method = getattr(self, "cmd_%s" % command.strip(), None)
            if method:
                completion_hints = getattr(method, "completion_hints", completion_hints)
                arg_parser = getattr(method, "arg_parser", None)
            
            for what in completion_hints:
                options = self._get_objects_starting_with(what, search)
                break
                
        if len(options) == 0:
            self.term.bell()
            return
        
        if len(options) > 1:
            # do all options start with a prefix longer than our search term?
            common_prefix_len = None
            for opt, is_final in options:
                if common_prefix_len is None:
                    common_prefix_len = len(options[0][0])
                    common_prefix = options[0][0]
                    continue
                while not opt.startswith(common_prefix):
                    common_prefix_len -= 1
                    common_prefix = common_prefix[:common_prefix_len]
                if common_prefix_len == 0:
                    break
            if common_prefix_len > len(search):
                found = options[0][0][:common_prefix_len]
                cursor_pos = common_prefix_len
                if " " in found:
                    found = '"%s' % found
                    cursor_pos += 1
                new_line = before + found + after
                self.term.bell()
                return ("replace", new_line, len(before) + cursor_pos)
            if is_first:
                self.term.bell()
                return
            return ("opts", [opt[0] for opt in options])
        
        # search command starting with before        
        found, is_final = options[0]
        cursor_pos = len(found)
        if " " in found:
            #if is_final:
            found = '"%s"' % found
            cursor_pos += 2
            #else:
            #    found = '"%s' % found
            #    cursor_pos += 1
        #if is_final:
        found += " "
        cursor_pos += 1
        new_line = before + found + after
        return ("replace", new_line, len(before) + cursor_pos)

    def on_resize(self, rows, cols):
        if self._attached_to:
            self._transfer_terminal_size(self._attached_to, rows_cols=(rows, cols))
    
    def _transfer_terminal_size(self, p, rows_cols=None, force=False):
        if rows_cols is None:
            rows, cols = self.term.get_rows_cols()
        else:
            rows, cols = rows_cols
        if p.state not in self._running_states:
            return False
        new_size = rows, cols
        if force:
            self.manager_proxy("send_winch", pname=p.name, rows=rows+1, cols=cols)
        self.manager_proxy("send_winch", pname=p.name, rows=rows, cols=cols)
        p._last_window_size = new_size

    def _set_process_property(self, p, prop_name, prop_value):
        if p.state not in self._running_states:
            return
        self.manager_proxy("mgr_cb", "set_process_property", ignore_cb, pname=p.name, pid=p.pid, name=prop_name, value=prop_value)
        
    @completion(["process-or-state"], arguments=[
        argument("process-or-state", help="attach to this process/state")])
    def cmd_attach(self, opts):
        """
        attach current console to named process

        input & output will be connected to that process.
        type "C-a d" to detach again and return to this prompt.
        """

        p = self._find_process(opts.process_or_state)
        if not p:
            return

        self._before_attach_cursor_pos = self.term.get_cursor_position()
        self._attached_to = p
        self.term.enable_alternative_buffer()        
        self.term.erase_in_display(2)

        N = 500
        data = self.manager_proxy("get_process_output_lines", p.name, N)
        self.output(data)
        
        def on_output(con, output):
            #self.output("\r\nfrom child: %r\n" % output, flush=True)
            self.output(output, flush=True)
            return True
        myself = ("attached", p.name)
        self.add_hook(("process_output", p.name), on_output, from_who=myself)

        # now wait for C-a d
        def on_terminal_input(con):
            #self.output("from stdin: %r, self.prompt_active: %s\n" % (
            #    self.term.input_queue, self.term.prompt_active))
            while self.term.input_queue:
                ch = self.term.input_queue.pop(0)

                if self._in_attach_escape:
                    if ch == "\x01": # C-a
                        self._in_attach_escape = False
                        #self.output("forward char: %r\n" % ch)
                        self.manager_proxy("send_stdin", pname=self._attached_to.name, text=ch)
                        continue
                    if ch == "d": # detach
                        # stop blocking command!
                        self.remove_hooks_to(myself)
                        self._reset_after_detach()
                        self._set_process_property(p, "no_output_rate_limit", 0)
                        to_notify = self._outputs_after_detach
                        self._outputs_after_detach = []
                        self._attached_to = None
                        for what, msg in to_notify:
                            self._notification_msg(what, msg)
                        self._stop_blocking_command(myself)
                        return False
                    self._in_attach_escape = False
                    continue
                if ch == "\x01": # C-a
                    self._in_attach_escape = True
                    #self.output("hold char: %r\n" % ch)
                    continue
                # forward to process!
                #self.output("send normal char: %r\n" % ch)
                self.manager_proxy("send_stdin", pname=self._attached_to.name, text=ch)
            return True
            
        self._start_blocking_command(myself)
        self._in_attach_escape = False
        self.term.set_window_title(p.name)
        if p.state in self._running_states:
            self._transfer_terminal_size(p, force=True)
            self._set_process_property(p, "no_output_rate_limit", 1)
            
        self.add_hook("after_terminal_input", on_terminal_input, from_who=myself)

    def cmd_notify(self, args=None):
        """
        select which notifications should be displayed

        usage: notify [{[+],-}item...]

        possible items:
          all              all notifications
          state            updates about process/state/group's started state (enabled by default)
          requested_state  updates about process/state/group's requested started state
          process_info     updates about process's pid and start/stop time
          errors           error messages (enabled by default)
          warnings         warning messages from processes (enabled by default)
        """

        if args is None:
            self.term._output_table(list(self._notification_aliases.keys()))
            return
        for arg in args:
            if arg.startswith("-"):
                arg = arg[1:]
                method = self._enabled_notifications.remove
            else:
                method = self._enabled_notifications.add
            arg = self._notification_aliases.get(arg, arg)
            method(arg)

    def set_window_title(self, title):
        self._window_title = title
        self.term.set_window_title(self._window_title)
        
    def _reset_after_detach(self):
        #self.term.disable_alternative_buffer()
        #self.term.full_terminal_reset()
        self._reset_terminal(erase=False)
        self.term.set_window_title(self._window_title)
        row, col = self._before_attach_cursor_pos
        self.term.set_cursor(row, 1)
        
    def _reset_terminal(self, erase=True, hard=False):
        self.term.restore()
        if hard:
            self.term.full_terminal_reset()
            self.term.set_scrolling_region()
        #rows, cols = self.term.get_rows_cols()
        #self.term.set_scrolling_region(1, rows-5)
        self.term.enable_echo(False)
        self.term.enable_canonical(False)
        self.term.disable_control_chars()
        self.term.set_bracketed_paste_mode(False)
        self.term.disable_mouse_stuff()
        if not hard and erase:
            self.term.erase_in_display(2)
            self.term.set_cursor(1, 1)
        
        
    def cmd_reset(self):
        """
        try to restore terminal to a sane state
        """
        self._reset_terminal(erase=True, hard=True)

    @completion(arguments=[
        argument("expr", nargs="+", help="python string to print")])
    def cmd_csi(self, opts):
        """
        write control sequence to terminal
        """
        for arg in opts.expr:
            arg = eval('"%s"' % arg.replace("\"", "\\\""))
            #self.output("sending CSI %r\n" % arg)
            self.term._csi_write(arg)
    
    @completion(arguments=[
        argument("expr", nargs="+", help="python string to print")])
    def cmd_osc(self, opts):
        """
        write operating system control sequence to terminal
        """
        for arg in opts.expr:
            arg = eval('"%s"' % arg.replace("\"", "\\\""))
            #self.output("sending OSC %r\n" % arg)
            self.term._osc_write(arg)
        
    @completion(arguments=[
        argument("expr", nargs="+", help="python expression to print")])
    def cmd_print(self, opts):
        """
        eval arguments as python expression and raw-write to stdout
        """
        for expr in opts.expr:
            args = eval(expr)
            if isinstance(args, tuple):
                args = " ".join(map(str, args))
            if not isinstance(args, (str, unicode)):
                args = str(args)
            self.term.write(args, flush=True)

    def _eval(self, string):
        out = []
        offset = 0
        #self.output("eval %r\n" % string)
        for m in re.finditer(r"(\$\w+)", string):
            varname = m.group(1)[1:]
            replace = str(self.variables.get(varname, ""))
            out.append(string[offset:m.start(0)])
            offset = m.end(0)
            out.append(replace)
        if not out:
            return string
        out.append(string[offset:])
        #self.output("eval result: %r\n" % out)
        return "".join(out)

    @completion(arguments=[
        argument("name", help="name of variable"), 
        argument("value", nargs="+", help="value to set")])
    def cmd_set(self, opts):
        """
        set a variable to given value
        """
        name = self._eval(opts.name)
        value = self._eval(" ".join(opts.value))
        if name.startswith("sysconf."): # special!
            member = name.split(".", 1)[1]
            self.manager_proxy("set_sysconf_attribute", member, eval(value))
            self._set_sysconf(self.manager_proxy(request="get_system_configuration"))
            return
        self.variables[name] = value
        #self.output("set %r = %r\n" % (name, value))
        
    def cmd_echo(self, args=None):
        """
        print arguments as string to stdout

        always outputs a newline at the end,
        replaces $variables by their values.
        """
        if args is None:
            args = []
        skip_newline = args and args[0] == "-n"
        if skip_newline:
            del args[0]        
        s = self._eval(" ".join(args))
        self.term.write(s, flush=True)
        if not skip_newline:
            self.term.write("\n")
        
    def cmd_reload(self):
        """
        trigger reload of system configuration
        """
        self.manager_proxy(request="reload_config")
        self._set_sysconf(self.manager_proxy(request="get_system_configuration"))
        
    @completion(["process"],
                arguments=[
                    argument("--stop", action="store_true", default=True, help="wait until all are stopped (default)"),
                    argument("--start", action="store_true", help="wait until all are started"),
                    argument("--ready", action="store_true", help="wait until all are ready"),
                    argument("-t", "--timeout", default=15, metavar="N", action="store", type=float, help="timeout after n seconds"),
                    argument("process-or-group", nargs="+", help="the name of a process to wait for"),
                ])
    def cmd_wait(self, opts):
        """
        wait for a process (or list of) to start or terminate
        """
        def is_done(p, state=None):
            if state is None:
                state = p.state
            if opts.ready:                
                return self._is_ready(p, state)
            if opts.start:
                return state in self._started_states
            return state in self._stopped_states
            
        error = False
        my_procs = {}
        for proc in opts.process_or_group:
            p = self._find_process_or_group(proc.strip())
            if p is None:
                error = True
                break
            
            if p.__class__.__name__ == "Group":
                np = self._get_all_processes_within_group(p, only_names=False)
            else:
                np = [p]

            for cp in np:
                if is_done(cp):
                    self.mi_response("process %s already in state %s" % (cp.name, cp.state))
                    continue
                my_procs[cp.name] = cp
        if error or not my_procs:
            return
        
        myself = ("wait", opts.timeout, tuple(my_procs.keys()))
        def on_timeout():
            if self.mi:
                self.mi_response("wait: timed out after %.1f seconds waiting for these procs:\n%s" % (
                    opts.timeout,
                    "\n".join([p.name for p in list(my_procs.values())])
                ))
            else:
                self.output("wait: timed out after %.1f seconds waiting for these procs:\n" % (opts.timeout))
                self.term._output_table([p.name for p in list(my_procs.values())])

        def on_state_change(msg, name, state):
            p = my_procs.get(name)
            if p is None:
                return True
            if is_done(p, state):
                #self.output("process now stopped: %s with %r\n" % (name, state))
                del my_procs[name]
            if my_procs:
                return True # wait for others
            self._stop_blocking_wait_with_timeout(myself)
            return False
        self.add_hook("object_state_change", on_state_change, from_who=myself)
        
        self._blocking_wait_with_timeout(opts.timeout, myself, on_timeout)
            
    @completion(arguments=[
                    argument("seconds", metavar="N", type=float, help="number of seconds"),
                ])
    def cmd_sleep(self, opts):
        """
        sleep for given number of seconds
        """
        myself = ("sleep", opts.seconds)
        
        self._blocking_wait_with_timeout(opts.seconds, myself, is_error=False)
    
    @completion(["topic", "topic-field"],
                arguments=[
                    argument("-nb", "--non-blocking", help="do not wait until process is started"),
                    argument("-t", "--timeout", default=15, metavar="N", action="store", type=float, help="wait timeout in seconds"),            
                    argument("topic", help="the name of the topic"),
                    argument("field", help="the fieldname to show in scope"),
                ])
    def cmd_start_pyscope(self, opts):
        """
        start a pyscope for named topic fields
        """
        scope_name = self.manager_proxy("open_scope", opts.topic, opts.field)
        self.mi_response("scope_name: %r" % scope_name)
        self.variables["scope_name"] = scope_name
        
        self._set_sysconf(self.manager_proxy(request="get_system_configuration"))
        if not opts.non_blocking:
            opts.process_or_group = [ scope_name ]
            opts.ready = True
            self.cmd_wait(opts)
    
    @completion(["file"],
                arguments=[
                    argument("-nb", "--non-blocking", help="do not wait until process is started"),
                    argument("-t", "--timeout", default=15, metavar="N", action="store", type=float, help="wait timeout in seconds"),            
                    argument("filename", help="where to store notebook contents to"),
                    argument("related-object-name", help="give a hint what this notebook is good for"),
                ])
    def cmd_start_notebook(self, opts):
        """
        start a notebook
        """
        notebook_name = self.manager_proxy("open_notebook", opts.filename, opts.related_object_name)
        self.mi_response("notebook_name: %r" % notebook_name)
        self.variables["notebook_name"] = notebook_name
        
        self._set_sysconf(self.manager_proxy(request="get_system_configuration"))
        if not opts.non_blocking:
            opts.process_or_group = [ notebook_name ]
            opts.ready = True
            self.cmd_wait(opts)

    @completion(["file"],
                arguments=[
                    argument("filename", help="name of .tar.bz2 file to write"),
                ])
    def cmd_save_debug_info(self, opts):
        """
        save as much debug info as possible to given .tar.bz2 file
        """
        debug_info = self.manager_proxy(request="create_debug_info")
        if debug_info:
            with open(opts.filename, "wb") as fp:
                fp.write(debug_info)
        else:
            raise Exception("debug info could not be generated!")
        
    @completion(["host", "host"],
                arguments=[
                    argument("from_host", help="host to start route from"),
                    argument("to_host", help="host to terminate route at"),
                ])            
    def cmd_test_route(self, opts):
        """
        search a route between two hosts
        """
        from_host = self.sysconf.get_host(opts.from_host)
        to_host = self.sysconf.get_host(opts.to_host)
        
        route = from_host.search_route_to(to_host)
        ret = []
        for hop, intf in enumerate(route):
            ret.append("   %2d. via %s/%s to network %s\n" % (
                hop, intf.host, intf.name, intf.network.name))
        self.mi_response("\n".joint(ret))
        self.variables["route_len"] = len(route)
        self.variables["route_hops"] = ", ".join([intf.host.name for intf in route])
        
    @completion(["topic-name"],
                arguments=[
                    argument("topic-name", help="name of topic to retrieve scheduling info for"),
                    argument("-t", "--timeout", default=15, metavar="N", action="store", type=float, help="timeout after n seconds"),
                ])            
    def cmd_get_topic_scheduling_info(self, opts):
        """
        output scheduling information for all threads of a given topic
        """
        myself = ("get_topic_scheduling_info", opts.topic_name)
        
        def _on_answer(rows=None, exception=None):
            if exception is not None:
                #traceback.print_stack()
                self.mi_error("getting scheduling info for topic %r: %s\n" % (opts.topic_name, exception))
                if exception == "timeout":
                    return
            else:
                self.mi_response("\n".join([str(row) for row in rows]))
            self._stop_blocking_wait_with_timeout(myself)

        self._blocking_wait_with_timeout(opts.timeout, myself, callback(_on_answer, exception="timeout"))
        self.manager_proxy("mgr_cb", "get_topic_scheduling_info", _on_answer, opts.topic_name, host_as_string=True)
    
    @completion(["host", "signal", "pid"],
                arguments=[
                    argument("host", help="host/node name"),
                    argument("signal", default="15", help="which signal to send"),
                    argument("pid", type=int, help="process id"),
                ])            
    def cmd_kill(self, opts):
        """
        send a signal to a process on named host/node
        """
        self.manager_proxy("signal_pid", opts.host, opts.pid, int(opts.signal)) # for now no signal aliases

    @completion(["topic-pattern"],
                arguments=[
                    argument("topic-pattern", nargs="*", help="limit output to given topic-patterns"),
                ])
    def cmd_get_topics(self, opts):
        """
        list known topics
        """
        names = self.manager_proxy("get_topic_names")
        names.sort()
        ret = []
        for name in names:
            if opts.topic_pattern:
                for pattern in opts.topic_pattern:
                    if fnmatch.fnmatch(name, pattern):
                        break
                else:
                    continue
            ret.append(name)
        self.mi_response("\n".join(ret))

    @completion(["topic-name"],
                arguments=[
                    argument("topic-name", help="name of topic to show info for"),
                ])
    def cmd_show_topic(self, opts):
        """
        show info for named topic
        """
        class on_answer(object):
            def __init__(self, con, topic_name):
                self.con = con
                self.topic_name = topic_name
                self.myself = ("show_topic", topic_name)
                self.con._blocking_wait_with_timeout(2, self.myself, callback(self, exception="timeout"))
            def __call__(self, info=None, exception=None):
                if exception is not None:
                    self.con.mi_error("getting info for topic %r: %s\n" % (self.topic_name, exception))
                    self.con._stop_blocking_wait_with_timeout(self.myself)
                    return

                self.con.mi_response("%s\n" % format_dict(info))
                self.con._stop_blocking_wait_with_timeout(self.myself)
        self.manager_proxy("mgr_cb", "get_topic_info", on_answer(self, opts.topic_name), opts.topic_name)

    @completion(["state"],
                arguments=[
                    argument("--up", "-u", action="store_true", help="request state UP"),
                    argument("--down", "-d", action="store_true", help="request state DOWN"),
                    argument("--check", "-c", action="store_true", default=True, help="check state"),
                    argument("--timeout", "-t", default=15, metavar="N", action="store", type=float, help="timeout after n seconds"),
                    argument("state", help="the name of a state"),
                ])
    def cmd_state(self, opts):
        """
        request a given state to be UP or DOWN or CHECK
        """
        state_name = opts.state.strip()
        s = self._find_state(state_name)
        if opts.up:
            req = "UP"
        elif opts.down:
            req = "DOWN"
        elif opts.check:
            req = "CHECK"
        self._req_state(s, req)

        myself = ("state", opts.timeout, (state_name, ))
        def on_timeout():
            self.mi_response("state: timed out after %.1f seconds waiting for state: %s" % (
                opts.timeout,
                state_name
            ))
        def on_state_change(msg, name, state):
            if name != s.name:
                return True # ignore
            if req != "CHECK" and state != req and "error" not in state.lower():
                return True # keep waiting
            self.mi_response(state)
            self._stop_blocking_wait_with_timeout(myself)
            return False
        self.add_hook("object_state_change", on_state_change, from_who=myself)

        self._blocking_wait_with_timeout(opts.timeout, myself, on_timeout)

    @completion(["message-definition"],
                arguments=[
                    argument("msg-def", nargs="+", help="the name of the message-definition"),
                ])
    def cmd_show_md(self, opts):
        """
        show contents of a named message definition
        """
        self.mi_response("%s\n" % self.manager_proxy("get_md_info", tuple(opts.msg_def)))
