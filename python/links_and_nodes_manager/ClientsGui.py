"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

import os
import datetime
import numpy
import traceback

from . import config

import pyutils
import links_and_nodes

from .Logging import enable_logging_on_instance

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Pango', '1.0')
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import GObject, Gtk, Pango, GLib, GdkPixbuf # noqa: E402


manager_base_dir = os.path.dirname(os.path.realpath(__file__))

class ClientsGui(pyutils.hooked_object):
    def __init__(self, gui):
        self.have_xml = False
        pyutils.hooked_object.__init__(self)
        enable_logging_on_instance(self, "ClientsGui")
        self.gui = gui
        self.manager_proxy = self.gui.manager_proxy

        # load processes gui
        resources_dir = os.path.join(config.manager_base_dir, "resources")
        self.xml = Gtk.Builder()
        self.xml_fn = os.path.join(resources_dir, "ln_manager_clients.ui")
        self.xml.add_from_file(self.xml_fn)
        self.have_xml = True
        self.xml.connect_signals(self)
        
        vb = Gtk.VBox()
        self.vpaned1.reparent(vb)
        self.gui.add_notebook_page("clients", vb, show=False)

        tv = self.clients_tv
        tv.set_enable_tree_lines(True)
        tv.set_headers_visible(False)
        m = self.clients_model = Gtk.TreeStore(
            GObject.TYPE_PYOBJECT,
            GObject.TYPE_STRING,
            GObject.TYPE_OBJECT
            )

        #tv.insert_column_with_attributes(-1, "client", gtk.CellRendererText(), text=1)
        cr2 = Gtk.CellRendererPixbuf()
        cr2.set_property("stock-size", Gtk.IconSize.MENU)
        cr2.set_property("xalign", 1)
        cr = Gtk.CellRendererText()
        tvc = Gtk.TreeViewColumn()
        tvc.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)
        tvc.pack_start(cr2, False)
        tvc.pack_start(cr, False)

        tvc.add_attribute(cr2, "pixbuf", 2)
        tvc.add_attribute(cr, "text", 1)
        tv.append_column(tvc)

        tv.set_model(m) # cursor-changed-fine

        tv = self.clientlog_tv
        tv.set_headers_visible(True)
        m = self.clientslog_model = Gtk.ListStore(
            GObject.TYPE_STRING, # ts
            GObject.TYPE_STRING, # msg
            )
        value_renderer = Gtk.CellRendererText()
        value_renderer.set_property("yalign", 0)
        tv.insert_column_with_attributes(-1, "timestamp", value_renderer, text=0)
        col = tv.get_columns()[-1]
        tv.insert_column_with_attributes(-1, "message", Gtk.CellRendererText(), text=1)
        tv.set_model(m) # cursor-changed-fine

        tv = self.service_definition_tv
        m = self.service_definition_model = Gtk.TreeStore(
            GObject.TYPE_STRING, # name
            GObject.TYPE_STRING, # type
            )
        tv.set_headers_visible(False)
        tv.insert_column_with_attributes(-1, "name", Gtk.CellRendererText(), text=0)
        tv.insert_column_with_attributes(-1, "type", Gtk.CellRendererText(), text=1)
        tv.set_model(m) # cursor-changed-fine

        self.client_frame.hide()
        self.client_frame.set_property("visible", False)
        self.topic_frame.hide()
        self.service_frame.hide()

        resources_dir = os.path.join(config.manager_base_dir, "resources")
        self.pub_pm = GdkPixbuf.Pixbuf.new_from_file(os.path.join(resources_dir, "publisher.png"))
        self.sub_pm = GdkPixbuf.Pixbuf.new_from_file(os.path.join(resources_dir, "subscriber.png"))
        self.svc_pm = GdkPixbuf.Pixbuf.new_from_file(os.path.join(resources_dir, "service.png"))

        # variables
        self.skip_ln_debug_click = False
        self.current_client = None
        self.parameter_windows = {}

    def connect(self):
        self.sysconf = self.gui.sysconf
        def notification_wrapper(callback):
            def inner_notification_wrapper(gui, msg):
                if "request" in msg:
                    msg = dict(msg) # cheap copy
                    del msg["request"]
                try:
                    return callback(**msg)
                except Exception:
                    self.error("error in notification callback %r%s:\n%s" % (callback.__name__, msg, traceback.format_exc()))
                    raise
            def to_string(self):
                return "<inner_notification_wrapper for %s>" % (callback.__name__,)
            inner_notification_wrapper.__str__ = to_string
            return inner_notification_wrapper
        #self.gui.add_hook(("notification", "add_topic"), notification_wrapper(self.on_add_topic))
        self.gui.add_hook(("notification", "del_topic"), notification_wrapper(self.on_del_topic))
        self.gui.add_hook(("notification", "update_service"), notification_wrapper(self.on_update_service))
        self.gui.add_hook(("notification", "update_topic"), notification_wrapper(self.on_update_topic))
        self.gui.add_hook(("notification", "update_client"), notification_wrapper(self.on_update_client))
        self.gui.add_hook(("notification", "del_client"), notification_wrapper(self.on_del_client))
        self.gui.add_hook(("notification", "new_config"), notification_wrapper(self.on_new_config))
        self.gui.add_hook(("notification", "client_log"), notification_wrapper(self.on_new_client_log))
        #self.gui.add_hook(("notification", "update_obj_requested_state"), notification_wrapper(self._obj_requested_state_changed))
        #self.update_services()

        self.client_frame.hide()
        self.topic_frame.hide()
        self.service_frame.hide()
        self.service_windows = {}

        # synthesize add client events for existing clients
        clients = self.gui.manager_proxy("get_clients")
        for client in clients:
            self.on_update_client(client)
        
        # topics & services
        seen = set()
        for client in clients:
            #self.published = {}
            for tn, subs in client.subscribed.items():
                if tn in seen:
                    continue
                seen.add(tn)
                self.on_update_topic(subs[0].topic)
            for tn, topic in client.published.items():
                if tn in seen:
                    continue
                seen.add(tn)
                self.on_update_topic(topic)
            for service_name, service in client.provided_services.items():
                self.on_update_service(client, service_name)

        tv = self.diff_tv
        tv.get_pango_context()
        font_description = Pango.FontDescription(self.gui.sysconf.instance_config.vte_font)
        tv.modify_font(font_description)
        
    def on_new_config(self, new_config):
        self.sysconf = new_config
        return True

    def __getattr__(self, name):
        if not self.have_xml:
            raise AttributeError(name)
        widget = self.xml.get_object(name)
        if widget is None:    
            raise AttributeError(name)
        return widget

    def update_services(self):
        pass

    def search_client(self, client, return_before=False):
        if isinstance(client, tuple):
            program_name, host, pid = client
        else:
            program_name = client.program_name
            
        m = self.clients_model
        iter = m.get_iter_first()
        before = None
        while iter:
            row = m[iter]
            if before is None and row[0].program_name > program_name:
                before = iter
            if row[0].program_name == program_name:
                if return_before:
                    return iter, before
                return iter
            iter = m.iter_next(iter)
        if return_before:
            return None, before
        return None

    def foreach_client(self):
        m = self.clients_model
        iter = m.get_iter_first()
        while iter:
            row = m[iter]
            yield row[0], iter
            iter = m.iter_next(iter)

    def on_update_client(self, client):
        # search matching process
        if self.gui.pg.current_p == client.process:
            self.gui.pg.is_client_btn.set_sensitive(True)

        m = self.clients_model
        iter, before = self.search_client(client, return_before=True)
        if iter:
            # only update name
            m[iter][0] = client
            m[iter][1] = client.program_name
            return True
        # not found add!
        m.insert_before(None, before, (client, client.program_name, None))
        #m.append(None, (client, client.program_name))
        path, col = self.clients_tv.get_cursor() # path-none-checked
        if not path:
            self.clients_tv.set_cursor((0, ))
        return True
    
    def on_del_client(self, client):
        iter = self.search_client(client)
        if not iter:
            #print "on_del_client: client not found"
            return True
        m = self.clients_model
        m.remove(iter)
        if self.gui.pg.current_p == client.process:
            self.gui.pg.is_client_btn.set_sensitive(False)
            if self.gui.pg.current_p: self.gui.pg.current_p.client = None
        if client == self.current_client:
            self.client_frame.hide()
            self.topic_frame.hide()
            self.service_frame.hide()
            self.current_client = None
        return True

    def _append_child(self, m, parent, what, text, pb=None):
        childs = m.iter_children(parent)
        while childs:
            if m[childs][1] > text:
                m.insert_before(parent, childs, (what, text, pb))
                return
            childs = m.iter_next(childs)
        m.append(parent, (what, text, pb))

    def on_update_service(self, client, service_name):
        iter = self.search_client(client)
        if not iter:
            self.warning("client %r not found in clients gui!" % client)
            return True
        m = self.clients_model
        m[iter][0] = client
        search = ("service", service_name)
        childs = m.iter_children(iter)
        text = service_name
        while childs:
            if m[childs][0] == search:
                # service still provided?
                if service_name in client.provided_services:
                    m[childs][1] = text
                else:
                    # remove!
                    m.remove(childs)
                return True
            childs = m.iter_next(childs)
        # else append
        self._append_child(m, iter, search, text, self.svc_pm)
        #m.append(iter, (search, text))
        return True

    def on_update_topic(self, topic):
        def _for_client(client, pm):
            iter = self.search_client(client)
            if not iter:
                self.warning("client %r not found in clients gui!\n" % (client, ))
                return True
            m = self.clients_model
            search = ("topic", topic)
            childs = m.iter_children(iter)
            text = topic.name
            while childs:
                if m[childs][0] == search:
                    m[childs][1] = text
                    return
                childs = m.iter_next(childs)
            # else append
            self._append_child(m, iter, search, text, pm)
            return
        if topic.publisher:
            _for_client(topic.publisher, self.pub_pm)
        sub_clients = topic.get_subscribed_clients()
        for client in sub_clients:
            _for_client(client, self.sub_pm)
        # check whether we need to remove it as subscribed port
        m = self.clients_model
        search = ("topic", topic)
        for client, iter in self.foreach_client():
            childs = m.iter_children(iter)
            while childs:
                # still published or subscribed by this client?
                if m[childs][0] == search and not (topic.publisher == client or client.address in [clnt.address for clnt in sub_clients]):
                    # remove!
                    if not m.remove(childs):
                        break
                else:
                    childs = m.iter_next(childs)
        return True
    def on_del_topic(self, topic):
        def _for_client(client):
            iter = self.search_client(client)
            m = self.clients_model
            search = ("topic", topic)
            childs = m.iter_children(iter)
            while childs:
                if m[childs][0] == search:
                    m.remove(childs)
                    break
                childs = m.iter_next(childs)
        if topic.publisher:
            _for_client(topic.publisher)
        for client in topic.get_subscribed_clients():
            _for_client(client)
        return True

    def _show_client(self, client, with_services=True):
        self.current_client = client
        self.name_entry.set_text(client.program_name)
        self.pid_entry.set_text(str(client.pid))
        self.node_entry.set_text(client.host.name if client.host else "")
        self.peer_entry.set_text("%s:%s" % client.address) # checked
        self.goto_proc_button.set_sensitive(client.process is not None)
        self.skip_ln_debug_click = True
        self.enable_ln_debug_btn.set_active(client.ln_debug)
        self.skip_ln_debug_click = False
        
        if client.is_modified:
            modified = ' <a href="#show_client_mod">(mod)</a>'
        else:
            modified = ""
        self.client_ver_label.set_markup("library version: %s,\nsvn rev: %d%s" % (
            client.library_version,
            client.svn_revision,
            modified))

        daemon = client._last_known_daemon
        if daemon and hasattr(daemon, "registered") and daemon.registered:
            if daemon.is_modified:
                modified = ' <a href="#show_daemon_mod">(mod)</a>'
            else:
                modified = ""
            self.daemon_ver_label.set_markup("proto: %s, lib: %s,\nsvn rev: %d%s" % (
                daemon.protocol_version,
                daemon.library_version,
                daemon.svn_revision,
                modified))
        else:
            self.daemon_ver_label.set_markup("not yet connected")
            
        if with_services:
            self.vbox1.set_child_packing(self.client_frame, True, True, 0, Gtk.PackType.START)
            vb = self.client_services_vbox
            vb.foreach(lambda w: vb.remove(w))
            
            # does it have parameters?
            has_parameters = False
            for service_name, service in client.provided_services.items():
                if service.interface in ("ln/parameters/query", "ln/parameters/query_dict"):
                    has_parameters = True
                    break
            if has_parameters:
                btn = Gtk.Button("show/edit parameters")
                btn.connect("clicked", self.show_parameters)
                vb.pack_start(btn, False, True, 0)

            service_names = list(client.provided_services.keys())
            service_names.sort()

            for service_name in service_names:
                service = client.provided_services[service_name]

                btn = Gtk.Button(service_name.replace("_", "__"))
                btn.connect("clicked", self.execute_service, service)
                vb.pack_start(btn, False, True, 0)

            self.client_frame.show_all()
        else:
            self.vbox1.set_child_packing(self.client_frame, False, True, 0, Gtk.PackType.START)
            self.client_frame.show_all()
            self.client_services_frame.hide()
        self.show_client_log(client)

    def _select_client(self, client):
        iter = self.search_client(client)
        if not iter:
            return False
        path = self.clients_model.get_path(iter)
        self.clients_tv.set_cursor(path)
        self.clients_tv.expand_row(path, True)
        return True

    def _select_client_topic(self, client, topic):
        iter = self.search_client(client)
        if not iter:
            return False
        childs = self.clients_model.iter_children(iter)
        while childs:
            row = self.clients_model[childs]
            if row[0] == ("topic", topic):
                path = self.clients_model.get_path(childs)
                self.clients_tv.expand_to_path(path)
                self.clients_tv.set_cursor(path)
                return
            childs = self.clients_model.iter_next(childs)

    def on_clients_tv_cursor_changed(self, tv):
        path, col = tv.get_cursor() # path-none-checked
        if not path:
            self.client_frame.hide()
            self.topic_frame.hide()
            self.service_frame.hide()
            return True
        m = self.clients_model
        iter = m.get_iter(path)
        row = m[iter]
        if len(path) == 1:
            # client
            client = row[0]
            self._show_client(client)
            self.topic_frame.hide()
            self.service_frame.hide()
        elif len(path) == 2:
            client = m[path[:-1]][0]
            self.current_client = client
            self._show_client(client, False)
            # topic or service
            which, obj = row[0]
            if which == "topic":
                topic = obj
                self.current_topic = topic
                self.topic_name_entry.set_text(topic.name)
                self.topic_def_entry.set_text(topic.md_name)
                if topic.publisher == client:
                    self.role_entry.set_text("publisher")
                elif topic.name in client.subscribed:
                    subscriptions = client.subscribed[topic.name]
                    rates = [sub.rate for sub in subscriptions]
                    if len(rates) == 1:
                        self.role_entry.set_text("subscriber, rate %r" % rates.pop())
                    else:
                        self.role_entry.set_text("subscriber, rates %s" % (", ".join(map(repr, rates))))
                else:
                    self.role_entry.set_text("<unknown!?>")
                self.md_file_entry.set_text(topic.md_fn)
                self.md_file_entry.set_position(len(topic.md_fn))
                        
                self.topic_frame.show_all()
                self.service_frame.hide()
            elif which == "service":
                service = client.provided_services[obj]
                self.current_service = service
                self.service_name_entry.set_text(obj)
                self.service_interface_entry.set_text(service.interface)
                self.service_port_entry.set_text(str(service.port))
                self.service_socket_entry.set_text(str(service.unix_socket))
                self.topic_frame.hide()

                m = self.service_definition_model
                m.clear()
                md, md_fn = self.gui.manager_proxy("search_and_create_message_definition", service.interface)
                self.service_md_file_entry.set_text(md_fn)
                self.service_md_file_entry.set_position(len(md_fn))
                def append_struct(fields, iter):
                    for ft, fn, fc in fields:
                        if fc > 1:
                            dft = ft + "[%d]" % fc
                        else:
                            dft = ft
                        child = m.append(iter, (fn, dft))
                        if fn in md.defines:
                            append_struct(child, md.defines[fn])
                append_struct(md.fields, m.append(None, ("request", "")))
                append_struct(md.resp_fields, m.append(None, ("response", "")))
                self.service_definition_tv.expand_all()
                self.service_frame.show_all()
                
    def on_goto_proc_button_clicked(self, btn):
        self.gui.notebook.set_current_page(0)
        self.gui.pg._select_process(self.current_client.process)
        return True

    def on_goto_topic_button_clicked(self, btn):
        self.gui.notebook.set_current_page(2)
        if self.current_topic.publisher == self.current_client:
            self.gui.tg.select_topic(self.current_topic)
        else:
            self.gui.tg._select_subscribed_topic(self.current_topic, self.current_client)

        return True
        
    def on_clients_tv_row_activated(self, tv, path, col):
        m = self.clients_model
        iter = m.get_iter(path)
        row = m[iter]
        if len(path) == 1:
            # client
            client = row[0]
            if client.process:
                self.gui.notebook.set_current_page(0)
                self.gui.pg._select_process(client.process)
        elif len(path) == 2:
            client = m[path[:-1]][0]
            # topic or service
            which, obj = row[0]
            if which == "topic":
                topic = obj
                self.gui.notebook.set_current_page(2)
                if topic.publisher == client:
                    self.gui.tg.select_topic(topic)
                else:
                    self.gui.tg._select_subscribed_topic(topic, client)
            elif which == "service":
                service = client.provided_services[obj]
                self.on_goto_service_button_clicked(None)
        return True
    
    def on_goto_service_button_clicked(self, btn, force_reload=False, do_exec=False):
        notebook_fn = os.path.join(
            self.gui.sysconf.instance_config.default_notebook_path,
            "service_client %s.py" % self.current_service.name)

        def get_field_hints(interface):
            req_hints = []

            md, _, size, signature, md_dict = self.gui.manager_proxy("search_and_create_message_definition", interface, with_size_signature_and_dict=True)
            svc = links_and_nodes.service_wrapper(
                "some_name",
                interface, repr(md_dict),
                signature, None, None)

            def show_fields(md, obj, prefix):
                all_field_names = set([field[1] for field in md.fields])
                for field in md.fields:
                    field_type = field[0]
                    field_name = field[1]
                    if field_name.endswith("_len") and field_name[:-4] in all_field_names:
                        continue
                    default_value = getattr(obj, field_name)

                    is_pointer = field_type[-1:] == "*"
                    if is_pointer:
                        field_type = field_type[:-1]
                    non_primitive = md.define_abs_names.get(field_type)
                    is_npy_array = type(default_value) == numpy.ndarray

                    if is_npy_array: # only for non-primitives
                        default_value = "np.array(%r, dtype=np.%s)" % (default_value.tolist(), default_value.dtype)
                        req_hints.append("%s%s = %s" % (prefix, field_name, default_value))
                        continue
                    if not non_primitive: # scalars
                        if default_value == "":
                            default_value = '""'
                        else:
                            default_value = repr(default_value)
                        req_hints.append("%s%s = %s # %s" % (prefix, field_name, default_value, field_type))
                        continue
                    # non-primitives, fixed-list-of-non-primitives, pointer-to-non-primitives
                    field_md, _ = self.gui.manager_proxy("search_and_create_message_definition", non_primitive)
                    if not isinstance(default_value, list):
                        # single non-primitive
                        if field_type == "pyobject":
                            req_hints.append("%s%s = %s # %s" % (prefix, field_name, default_value, field_type))
                        else:
                            show_fields(field_md, getattr(obj, field_name), "%s%s." % (prefix, field_name))
                        continue
                    if is_pointer:
                        if field_type == "pyobject":
                            req_hints.append("%s%s = %r # list of %s's" % (prefix, field_name, default_value, field_type))
                            continue
                        req_hints.append("%s%s = %s # list of %s" % (prefix, field_name, default_value, field_type))

                        if prefix == "svc.req.": # first level
                            pre = prefix
                            req_hints.append("# %s_item = svc.req.%s()" % (field_name, links_and_nodes.get_packet_ctor_name(field_type)))
                            show_fields(field_md, svc.req._field_ctors[field_name](), "# %s_item." % field_name)
                        else:
                            pre = prefix[2:] # without `# `
                            ctor_name = links_and_nodes.get_packet_ctor_name(field_type)
                            req_hints.append("# %s_item = %s%s()" % (field_name, pre, ctor_name))
                            show_fields(field_md, getattr(obj, ctor_name)(), "# %s_item." % field_name)

                        req_hints.append("# %s%s.append(%s_item)" % (pre, field_name, field_name))
                        continue
                    if field_type == "pyobject":
                        req_hints.append("%s%s = %r # %d %s's" % (prefix, field_name, default_value, len(default_value), field_type))
                        continue
                    for idx, item in enumerate(default_value):
                        show_fields(field_md, getattr(obj, field_name)[idx], "%s%s[%s]." % (prefix, field_name, idx))


            show_fields(md, svc.req, "svc.req.")
            return "\n".join(req_hints), md
        req_field_hints, md = get_field_hints(self.current_service.interface)
        resp_field_names = set([field[1] for field in md.resp_fields])
        if "error_message_len" in resp_field_names:
            error_handling = """
if svc.resp.error_message_len:
    raise Exception(svc.resp.error_message)
"""
        else:
            error_handling = ""
        
        teaser = """import pprint
import numpy as np
import links_and_nodes as ln

# clnt = ln.client("service_client %(name)s")
svc = clnt.get_service("%(name)s", "%(md)s", mainloop=ln.GLibMainloop())
print(clnt.describe_message_definition("%(md)s"))
#~~~--~~~
%(fields)s

svc.call() # blocking call
%(error_handling)s
pprint.pprint(svc.resp)""" % dict(
    name=self.current_service.name,
    md=self.current_service.interface,
    fields=req_field_hints,
    error_handling=error_handling
    )
        self.gui.manager_proxy("open_notebook", notebook_fn, "service %s" % self.current_service.name, teaser)
        return True

    def on_client_pl_btn_clicked(self, btn):
        client = self.current_client
        pl = self.gui.pg.show_process_list(host=client.host)
        pl.select_pid(client.pid)

    def execute_service(self, btn, service):
        self.current_service = service
        md, fn = self.gui.manager_proxy("search_and_create_message_definition", service.interface)
        self.on_goto_service_button_clicked(None, force_reload=False, do_exec=not len(md.fields))
            
    def on_diff_window_delete_event(self, window, ev):
        self.diff_window.hide()
        return True

    def show_parameters(self, btn):
        from . import parameters_window
        #parameters_window = reload(parameters_window)
        pwin = self.parameter_windows.get(self.current_client)
        if pwin is None:
            pwin = self.parameter_windows[self.current_client] = parameters_window.parameters_window(self, client_name=self.current_client.program_name)
        pwin.present()
        return True
    
    def on_kick_btn_clicked(self, btn):
        self.gui.manager_proxy("kick_client", self.current_client)
        return True

        
    def _do_adjustment(self):
        adj = self.clientlog_sw.get_vadjustment()
        old = adj.get_value()
        adj.set_value(adj.get_upper() - adj.get_page_size())
        if old == adj.get_value():
            self._sb_adj = None
            return False
        return True

    def queue_log_display(self):
        if not hasattr(self, "_sb_adj") or self._sb_adj is None:
            self._sb_adj = GLib.idle_add(self._do_adjustment)
        return True

    def on_new_client_log(self, client, ts, msg):
        if client._log[-1] != (ts, msg):
            client._log.append((ts, msg))
        if client != self.current_client:
            return True
        ts = datetime.datetime.fromtimestamp(ts)
        
        m = self.clientslog_model
        m.append((str(ts)[:23], msg))

        N = m.iter_n_children(None)
        while N > self.gui.sysconf.instance_config.max_log_items:
            m.remove(m.get_iter_first())
            N -= 1
        
        self.queue_log_display()
        return True

    def show_client_log(self, client=None):
        if client is None:
            client = self.current_client
        m = self.clientslog_model
        m.clear()
        for ts, msg in client._log:
            ts = datetime.datetime.fromtimestamp(ts)
            m.append((str(ts)[:23], msg))
        
        self.queue_log_display()
        return True

    def on_enable_ln_debug_btn_clicked(self, btn):
        if self.skip_ln_debug_click:
            return True
        if not self.current_client:
            return False
        if self.current_client.library_version < 6:
            # not supported
            return False        
        self.current_client.ln_debug = btn.get_active()
        if self.current_client.ln_debug:
            enable = 1
        else:
            enable = 0
        self.gui.manager_proxy(
            "send_request",
            self.current_client, "enable_debug", None,
            enable=enable)
        return True
