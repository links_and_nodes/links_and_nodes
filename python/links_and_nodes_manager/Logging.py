"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

from __future__ import print_function

import traceback
import datetime
import sys
import pyutils
import threading
import os

import gi
gi.require_version('GLib', '2.0')
# GLib is imported later on when it is used (rarely)

levels = "debug, info, warning, error".split(", ")

stdout_level = "debug"
#stdout_level = "info"

file_name = None
file_level = "debug"
file_fp = None
file_flush = False
file_size_limit = None
file_keep_count = 0
manager = None

log_buffer = []
max_N_log_buffer = 1024

log_head = 0
log_pops = 0

log_flags = set()
if os.getenv("LN_DEBUG", "")[:1].lower() in ("1", "y", "t"):
    log_flags.add("store_stack")

def stdout_write(what, flush=False):
    sys.stdout.write(what)
    if flush:
        sys.stdout.flush()

def get_log_buffer(from_id):
    global log_buffer, log_pops
    return log_buffer[from_id - log_pops:]

class trigger_log_update_class(pyutils.hooked_object):
    def __init__(self):
        pyutils.hooked_object.__init__(self)
        self._new_head = None
        self._lock = threading.RLock()
        self._gui_pipe = None
        self._gui_pipe_is_signaled = False

    def notify(self, log_head):
        """
        should only be called from within gui thread
        """
        try:
            self.call_hook("log_update", log_head)
        except Exception:
            print("log_update hook caused exception:\n%s" % traceback.format_exc())

    def check_notify(self):
        self._lock.acquire()
        if self._new_head:
            self.notify(self._new_head)
            self._new_head = None
        self._lock.release()

    def set_new_head(self, log_head):
        self._lock.acquire()
        self._new_head = log_head
        if self._gui_pipe: # trigger _on_gui_pipe in gui thread!
            if not self._gui_pipe_is_signaled:
                os.write(self._gui_pipe[1], b".")
                self._gui_pipe_is_signaled = True
        self._lock.release()

    def gobject_check(self,):
        from gi.repository import GLib
        self._gui_pipe = os.pipe()
        GLib.io_add_watch(self._gui_pipe[0], GLib.IOCondition.IN, self._on_gui_pipe)
    def _on_gui_pipe(self, fd, why):
        self._lock.acquire()
        if self._gui_pipe_is_signaled:
            os.read(self._gui_pipe[0], 1)
            self._gui_pipe_is_signaled = False
        self._lock.release()
        self.check_notify()
        return True

trigger_log_update = trigger_log_update_class()

def append_log(now, level, subsystem, msg, stack=None):
    global file_fp, log_buffer, log_head, log_pops, log_flags
    if stack is None and "store_stack" in log_flags:
        stack = "".join(traceback.format_stack()[:-3])
    else:
        stack = None
    log_buffer.append((log_head, now, level, subsystem, msg, stack)) # "%s: %s" % (log_head, msg)))
    while len(log_buffer) > max_N_log_buffer:
        log_buffer.pop(0)
        log_pops += 1
    trigger_log_update.set_new_head(log_head)
    log_head += 1

_lock = threading.RLock()
def log_message(level, subsystem, msg):
    global file_fp, log_buffer, log_head, log_pops
    now = datetime.datetime.now()
    _lock.acquire()
    append_log(now, level, subsystem, msg)

    head = "%-7.7s %-18.18s " % (
        level,
        "%s:" % subsystem)
    msg = msg.rstrip().replace("\r", "")
    if "\n" in msg:
        spaces = " " * len(head)
        ls_msg = head + msg.replace("\n", "\n%s" % spaces)
    else:
        ls_msg = head + msg

    level_id = levels.index(level)
    stdout_level_id = levels.index(stdout_level)
    file_level_id = levels.index(file_level)

    if level_id >= stdout_level_id:
        ts = "%-22.22s" % now
        head = "%s %s" % (ts, head)
        if "\n" in msg:
            spaces = " " * len(head)
            ls_msg = head + msg.replace("\n", "\n%s" % spaces)
        else:
            ls_msg = head + msg
        if level == "error":
            color = "1"
        else:
            color = None
        if color is not None:
            stdout_write("\033[3%sm" % color, False)
        stdout_write("%s\n" % ls_msg, color is None)
        if color is not None:
            stdout_write("\033[39m", True)
    if level_id >= file_level_id and (file_name or file_fp):
        if not file_fp:
            dn = os.path.dirname(os.path.realpath(file_name))
            if not os.path.isdir(dn):
                os.mkdir(dn)
            file_fp = open(file_name, "a+b")
        ts = "%-22.22s" % now
        head = "%s %s" % (ts, head)
        if "\n" in msg:
            spaces = " " * len(head)
            ls_msg = head + msg.replace("\n", "\n%s" % spaces)
        else:
            ls_msg = head + msg
        # convert str to bytes when using python3
        file_fp.write(b"%s\n" % ls_msg.encode("utf-8"))
        if file_flush: file_fp.flush()
        if file_size_limit and manager: manager._register_logfile_check(sys.modules[log_message.__module__])
    #if level == "error":
    #    time.sleep(0.5)
    _lock.release()

def debug(subsystem, msg):
    log_message("debug", subsystem, msg)
def info(subsystem, msg):
    log_message("info", subsystem, msg)
def warning(subsystem, msg):
    log_message("warning", subsystem, msg)
def error(subsystem, msg):
    log_message("error", subsystem, msg)

def is_at_least_log_level(level):
    level_id = levels.index(level)
    stdout_level_id = levels.index(stdout_level)
    file_level_id = levels.index(file_level)
    return level_id >= stdout_level_id or level_id >= file_level_id

def enable_logging_on_instance(instance, subsystem):
    instance._log_subsystem = subsystem
    for level in levels:
        def get_logger(level):
            def logger(msg, level=level):
                return log_message(level, subsystem, msg)
            return logger
        setattr(instance, level, get_logger(level))
    #if instance.__class__.__name__ != subsystem:
    #    instance.debug("new %s instance: %s" % (instance.__class__.__name__, subsystem))
    #else:
    #    instance.debug("new %s instance" % (instance.__class__.__name__))


def log_exception(fcn):
    """
    decorator that executes function and logs exception via self.error
    """
    def log_exception_wrapper(*args, **kwargs):
        try:
            return fcn(*args, **kwargs)
        except Exception:
            args[0].error("exception in %s:\n%s" % (fcn, traceback.format_exc()))
    return log_exception_wrapper
