"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

import os
import fnmatch
import getpass

import gi
gi.require_version('GLib', '2.0') 
from gi.repository import GLib


def glob(curdir, patterns):
    includes = []
    excludes = []
    # split patterns in includes and excludes and make them all absolute
    for pattern in patterns:
        ptype = includes
        if pattern.startswith("!"):
            ptype = excludes
            pattern = pattern[1:]
        if not pattern:  # drop empty patterns
            continue
        if not os.path.isabs(pattern):
            pattern = os.path.abspath(os.path.join(curdir, pattern))
        pattern = pattern.split(os.path.sep)
        if pattern[0] == '':
            pattern.pop(0)  # remove first empty string on unix style paths
        ptype.append(pattern)
    # find a common base path that is not ambiguous
    patterns = includes + excludes
    if not patterns:
        return []
    testroot = patterns[0][:]
    for pattern in patterns[1:]:
        for i in range(min(len(testroot), len(pattern))):
            if pattern[i] != testroot[i]:
                testroot = testroot[:i]
                break
    common_base = []
    for i, e in enumerate(testroot):
        if '*' in e or '?' in e or '[' in e:
            break
        common_base.append(e)
    for pattern in patterns:
        for e in common_base:
            pattern.pop(0)
    rootdir = os.path.join(os.getcwd().split(os.sep)[0] + os.sep, *common_base)
    return _glob(rootdir, includes, excludes, set())


def _match(part, patterns):
    p_next = []
    match = False
    for pat in patterns:
        npat = pat[1:]
        if not pat:  # explicitly matched pattern
            match = True
            continue
        if pat[0] == '**':
            p_next.append(pat)
        if fnmatch.fnmatch(part, pat[0]):
            if npat:
                p_next.append(npat)
            else:
                match = True
    return match, p_next


def _glob(curdir, includes, excludes, links):
    matches = []
    if os.path.isdir(curdir):
        files = os.listdir(curdir)
    else:
        files = [curdir]
    for fn in files:
        excluded, n_excludes = _match(fn, excludes)
        if excluded:
            continue
        included, n_includes = _match(fn, includes)
        p = os.path.join(curdir, fn)
        if included and os.path.isfile(p):
            matches.append(p)
            continue
        if n_includes:
            if os.path.islink(p):
                abslink = os.path.realpath(p)
                if abslink in links:
                    continue
                links.add(abslink)
            if os.path.isdir(p):
                matches.extend(_glob(p, n_includes, n_excludes, links))
    return matches

def get_default_glib_mainloop():
    if not hasattr(get_default_glib_mainloop, 'mainloop'):
        get_default_glib_mainloop.mainloop = GLib.MainLoop(None)
    return get_default_glib_mainloop.mainloop

def get_login():
    try:
        return getpass.getuser()
    except:
        pass
    try:
        return os.getlogin()
    except:
        pass
    return "unknown"
