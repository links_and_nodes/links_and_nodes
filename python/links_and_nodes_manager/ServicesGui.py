"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

import os
import fnmatch
import traceback

from . import config

from .Logging import enable_logging_on_instance

import pyutils

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import GObject, Gtk, GdkPixbuf, GLib # noqa: E402

manager_base_dir = os.path.dirname(os.path.realpath(__file__))

class ServicesGui(pyutils.hooked_object):
    def __init__(self, gui):
        self.have_xml = False
        pyutils.hooked_object.__init__(self)
        enable_logging_on_instance(self, "ServicesGui")
        self.gui = gui
        self.manager_proxy = self.gui.manager_proxy

        self._refilter_timeout = None

        # load processes gui
        resources_dir = os.path.join(config.manager_base_dir, "resources")
        self.xml = Gtk.Builder()
        self.xml_fn = os.path.join(resources_dir, "ln_manager_services.ui")
        self.xml.add_from_file(self.xml_fn)
        self.have_xml = True
        self.xml.connect_signals(self)

        m = self.pattern_model = Gtk.ListStore(GObject.TYPE_STRING)
        patterns = self.load_patterns()
        self.pattern = patterns[0]        
        self.pattern_cb.set_model(m) # cursor-changed-fine
        #self.pattern_cb.set_text_column(0)
        for i, pattern in enumerate(patterns):
            m.append((pattern, ))        
            if pattern == self.pattern:
                self.pattern_cb.set_active(i)
        vb = Gtk.VBox()
        self.main.reparent(vb)
        self.gui.add_notebook_page("services", vb, show=False)

        tv = self.services_tv
        tv.connect("row-collapsed", self.on_row_collapsed)
        tv.set_enable_tree_lines(True)
        tv.set_headers_visible(False)

        m = self.services_model = Gtk.TreeStore(
            GObject.TYPE_PYOBJECT,
            GObject.TYPE_STRING,
            GObject.TYPE_OBJECT
            )
        self.filtered_model = m.filter_new()
        self.filtered_model.set_visible_func(self.filter_func)

        #tv.insert_column_with_attributes(-1, "client", gtk.CellRendererText(), text=1)
        cr2 = Gtk.CellRendererPixbuf()
        cr2.set_property("stock-size", Gtk.IconSize.MENU)
        cr2.set_property("xalign", 1)
        cr = Gtk.CellRendererText()
        tvc = Gtk.TreeViewColumn()
        tvc.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)
        tvc.pack_start(cr2, False)
        tvc.pack_start(cr, False)

        tvc.add_attribute(cr2, "pixbuf", 2)
        tvc.add_attribute(cr, "text", 1)
        tv.append_column(tvc)

        tv.set_model(self.filtered_model) # cursor-changed-fine

        resources_dir = os.path.join(config.manager_base_dir, "resources")
        self.svc_pb = GdkPixbuf.Pixbuf.new_from_file(os.path.join(resources_dir, "service.png"))
        self.had_user_interaction = False

    def connect(self):
        def notification_wrapper(callback):
            def inner_notification_wrapper(gui, msg):
                if "request" in msg:
                    msg = dict(msg) # cheap, but preserved original dict!
                    del msg["request"]
                try:
                    return callback(**msg)
                except Exception:
                    self.error("error in notification callback %r%s:\n%s" % (callback.__name__, msg, traceback.format_exc()))
                    raise
            return inner_notification_wrapper
        #self.gui.add_hook(("notification", "del_topic"), notification_wrapper(self.on_del_topic))
        self.gui.add_hook(("notification", "update_service"), notification_wrapper(self.on_update_service))
        #self.gui.add_hook(("notification", "update_topic"), notification_wrapper(self.on_update_topic))
        #self.gui.add_hook(("notification", "update_client"), notification_wrapper(self.on_update_client))
        self.gui.add_hook(("notification", "del_client"), notification_wrapper(self.on_del_client))
        self.sysconf = self.gui.sysconf

        # synthesize events for existing services
        clients = self.gui.manager_proxy("get_clients")
        for client in clients:
            for service_name, service in client.provided_services.items():
                self.on_update_service(client, service_name)

    def __getattr__(self, name):
        if not self.have_xml:
            raise AttributeError(name)
        widget = self.xml.get_object(name)
        if widget is None:    
            raise AttributeError(name)
        return widget

    def _refilter_timeout_cb(self):
        if self._refilter_timeout_delay_one_more:
            self._refilter_timeout_delay_one_more = False
            return True
        self.filtered_model.refilter()
        self._refilter_timeout = None
        return False

    def _trigger_refilter(self):
        if self._refilter_timeout is None:
            self._refilter_timeout_delay_one_more = False
            self._refilter_timeout = GLib.timeout_add(200, self._refilter_timeout_cb)
            return
        self._refilter_timeout_delay_one_more = True
        return

    def on_update_service(self, client, service_name):
        service = client.provided_services.get(service_name)
        service_deleted = service is None
        
        service_path = service_name.split(".")
        iter = self.get_service_iter(service_path)
        if iter is not None:
            # found
            if service_deleted:
                # remove!
                self.remove_service_iter(iter)
            self._trigger_refilter()
            return True
        if service_deleted:
            self._trigger_refilter()
            return True
        # add!
        iter = self.add_service_path(service_path, service)
        self._trigger_refilter()
        return True

    def on_del_client(self, client):
        # remove all service entries which reference this client!
        m = self.services_model
        def remove_services(parent):
            iter = m.iter_children(parent)
            while iter:
                service = m[iter][0]
                if service and service.provider == client:
                    if not m.remove(iter):
                        iter = None
                elif m.iter_has_child(iter):
                    iter = remove_services(iter)
                else:
                    iter = m.iter_next(iter)
            if parent and not m.iter_has_child(parent):
                if not m.remove(parent):
                    return None # parent is invalid now
                return parent
            if parent is None:
                return None
            return m.iter_next(parent)

        remove_services(None)
        return True

    def remove_service_iter(self, iter):
        m = self.services_model
        def remove(iter):
            parent = m.iter_parent(iter)
            m.remove(iter)
            if parent is None or m.iter_has_child(parent):
                return
            remove(parent)
        remove(iter)

    def get_service_iter(self, path):
        m = self.services_model
        def find(path, parent):
            if len(path) == 0:
                return parent
            iter = m.iter_children(parent)
            while iter:
                if m[iter][1] == path[0]:
                    # found!
                    return find(path[1:], iter)
                iter = m.iter_next(iter)
            return None # not found
        return find(path, None)

    def add_service_path(self, path, service):
        m = self.services_model
        def add(path, parent):
            iter = m.iter_children(parent)
            while iter:
                if (m[iter][1] > path[0] and len(path) == 1 and not m.iter_has_child(iter)) or (m[iter][1] > path[0] and len(path) > 1 and m.iter_has_child(iter)) or (len(path) == 1 and m.iter_has_child(iter)):
                    # insert before!
                    if len(path) == 1:
                        return m.insert_before(parent, iter, (service, path[0], self.svc_pb))
                    iter = m.insert_before(parent, iter, (None, path[0], None))
                    return add(path[1:], iter)
                if m[iter][1] == path[0]:
                    # found!
                    return add(path[1:], iter)
                iter = m.iter_next(iter)
            if len(path) == 1:
                # append!
                return m.append(parent, (service, path[0], self.svc_pb))
            iter = m.append(parent, (None, path[0], None))
            return add(path[1:], iter)
        iter = add(path, None)
        if not self.had_user_interaction:
            path = m.get_path(iter)
            self.services_tv.expand_to_path(path)
        return iter

    def on_services_tv_row_activated(self, tv, path, col):
        svc = self.filtered_model[path][0]
        if svc:
            self.gui.cg.execute_service(None, svc)
        return True

    def on_row_collapsed(self, tv, iter, path):
        self.had_user_interaction = True


    def on_collapse_all_btn_clicked(self, btn):
        self.services_tv.collapse_all()
        return True
    def on_expand_all_btn_clicked(self, btn):
        self.services_tv.expand_all()
        return True

    def on_pattern_cb_changed(self, entry):
        self.pattern = self.pattern_cb.get_active_text()
        if hasattr(self, "filtered_model"):
            self.filtered_model.refilter()
    def on_pattern_cb_entry_activate(self, entry):
        self.update_pattern_model()
        self.filtered_model.refilter()
        
    def update_pattern_model(self):
        self.pattern = self.pattern_cb.get_active_text()
        m = self.pattern_model
        # search text
        iter = m.get_iter_first()
        if m[iter][0] == self.pattern:
            # nothing todo
            return
        # search further, remove if exists, prepent at index 0
        while iter:
            if m[iter][0] == self.pattern:
                # found! -> remove!
                m.remove(iter)
                break
            iter = m.iter_next(iter)
        # insert at top!
        m.insert(0, (self.pattern, ))
        
        patterns = []
        iter = m.get_iter_first()
        while iter:
            patterns.append(m[iter][0])
            iter = m.iter_next(iter)
        self.save_patterns(patterns)
    
    def save_patterns(self, patterns):
        if patterns == self._last_patterns:
            return
        with open(os.path.expanduser("~/.ln_service_patterns.%s" % self.sysconf.instance_config.name), "w") as fp:
            fp.write(repr(patterns))
        self._last_patterns = patterns

    def load_patterns(self):
        try:
            with open(os.path.expanduser("~/.ln_service_patterns.%s" % self.sysconf.instance_config.name), "r") as fp:
                self._last_patterns = eval(fp.read())
        except Exception:
            self._last_patterns = ["*"]
        return self._last_patterns
    
    def filter_func(self, m, iter, data):
        def some_child_matches(iter):
            childs_iter = m.iter_children(iter)
            while childs_iter:
                if m[childs_iter][0] is None:
                    if some_child_matches(childs_iter):
                        return True
                elif fnmatch.fnmatch(m[childs_iter][0].name, self.pattern):
                    return True
                childs_iter = m.iter_next(childs_iter)
            return False
        if m[iter][0] is None:
            return some_child_matches(iter)
        if not fnmatch.fnmatch(m[iter][0].name, self.pattern):
            return False
        return True
