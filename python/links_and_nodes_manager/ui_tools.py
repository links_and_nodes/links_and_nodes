"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2022 DLR e.V., Florian Schmidt
"""

_system_fonts = None
def list_system_fonts(win):
    global _system_fonts
    if _system_fonts is None:
        context = win.create_pango_context()
        _system_fonts = sorted([fam.get_name() for fam in context.list_families()])
    return _system_fonts

def window_has_font(win, search):
    for fn in list_system_fonts(win):
        if search.lower() in fn.lower():
            return True # probably
    return False # nope

def get_monospace_font_name(window, size=10):
    if window_has_font(window, "Misc Fixed"):
        return "Misc Fixed medium %s" % size
    return "monospace %s" % size
