# This file is part of links and nodes.
# 
# links and nodes is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# links and nodes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
# 
# Copyright 2019 DLR e.V., Florian Schmidt

import threading
import traceback
import os

import gi
gi.require_version('GLib', '2.0')

class Thread(threading.Thread):
    def __init__(self, job_to_execute):
        threading.Thread.__init__(self)
        self.job_to_execute = job_to_execute
        self.gui_cb = None
        self.retval = None
        self.exception = None
        
    def on_finish_call_in_glib_thread(self, cb):
        """
        should be called from within the gui thread,
        before the thread is started!
        """
        from gi.repository import GLib
        self.gui_cb = cb
        self.gui_pipe = os.pipe()
        self.gui_source = GLib.io_add_watch(self.gui_pipe[0], GLib.IOCondition.IN, self._on_gui_pipe)
        
    def _on_gui_pipe(self, fd, why):
        self.join()
        os.close(self.gui_pipe[0])
        os.close(self.gui_pipe[1])
        self.gui_source = None
        self.gui_cb()
        return False

    def run(self):
        try:
            self.retval = self.job_to_execute()
        except Exception:
            self.exception = traceback.format_exc()
        
        if self.gui_cb: # trigger _on_gui_pipe in gui thread!
            os.write(self.gui_pipe[1], ".")
