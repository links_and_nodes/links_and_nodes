"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

from __future__ import print_function

import os
import sys
import re
import subprocess
import time
import datetime
import glob
import socket
import struct
import pprint
import signal
import weakref
import sysconfig
import traceback
from collections import OrderedDict

import numpy as np

import links_and_nodes as ln
from links_and_nodes_base import pickle_socket_wrapper

import pyutils
import pyutils.line_assembler

from . import config
from . import Logging

from .CommunicationUtils import async_return, import_plugin_package, callback, ignore_cb, needs_finish_callback, ping
from .DaemonStore import DaemonStore, debug_repr_output_log
from .ClientStore import ClientStore
from .TopicStore import  Topic
from .PortStore import  shm_port, udp_port, tcp_port
from .SystemConfiguration import SystemConfiguration, Process
from .Logger import Logger
from .Logging import enable_logging_on_instance, log_exception
from .service_protocol_filter import service_protocol_filter

import gi
gi.require_version('GLib', '2.0')
from gi.repository import GLib # noqa: E402

process_requested_state_to_real_state = {
    "start": "started",
    "stop": "stopped",
}    

class StartupException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg

class gui_connection(object):
    def __init__(self, manager, fd, address):
        self.manager = manager
        self.fd = fd
        self.fd.setblocking(1)
        self.pickle_fd = pickle_socket_wrapper(self.fd)
        self.address = address
        self.peer = ":".join(map(str, self.address))
        self.io_watch = GLib.io_add_watch(self.fd, GLib.IO_IN, self.on_data_accounting)
        self._enabled_notifications = None # all
        self.manager.add_hook("notify_gui", self._gui_notification, from_who=self)
        self.last_log_id = -1
        #self.enable_log_update()

        self.log_idle = None
        self.queue_log_display(None, self.last_log_id)
        
        self.last_accounting_output = time.time()
        self.accounting_n_calls = 0
        self.accounting_n_seconds = 0
        self.accounting_n_calls_total = 0
        self.accounting_n_seconds_total = 0

        self.try_again_timer = None
        self.XAUTHORITY = None
        self.DISPLAY_UNIX_SOCKET = None

    def __repr__(self):
        return "<gui_connection from %s>" % self.peer
    
    def enable_log_update(self):
        Logging.trigger_log_update.add_hook("log_update", self.queue_log_display, from_who=self)

    def queue_log_display(self, obj, log_head):
        if self.log_idle is None:
            self.log_idle = GLib.idle_add(self.send_log)
        return True
    
    def send_log(self):
        self.log_idle = None
        msgs = Logging.get_log_buffer(self.last_log_id+1)
        if not msgs:
            return False
        self.last_log_id = msgs[-1][0]
        self._gui_notification(self, dict(request="log_msg", msgs=msgs))
        return False
    
    def remove(self):
        Logging.trigger_log_update.remove_hooks_to(self)
        if self in self.manager.gui_connections:
            self.manager.gui_connections.remove(self)
        GLib.source_remove(self.io_watch)
        self.fd.close()
        self.manager.remove_hooks_to(self)
        
    def _gui_notification(self, manager, msg, drop_ok=False):
        # send pickle
        if self._enabled_notifications is not None and msg.get("request") not in self._enabled_notifications:
            return True
        try:
            self.pickle_send(("notify", msg), drop_ok=drop_ok)
        except Exception:
            Logging.trigger_log_update.remove_hooks_to(self)
            self.manager.error("error pickling gui_notification:\n%s\n%s\nremoving connection!" % (
                pprint.pformat(msg),
                traceback.format_exc()))
            self.remove()
        return True

    def pickle_send(self, data, drop_ok=False):
        try_again = self.pickle_fd.send(data, drop_ok=drop_ok)
        if try_again and not self.try_again_timer:
            self.manager.debug("start try again timer!")
            self.try_again_timer = GLib.timeout_add(100, self._try_again)

    def _try_again(self):
        try_again = self.pickle_fd._process_send_queue()
        if not try_again:
            GLib.source_remove(self.try_again_timer)
            self.try_again_timer = None
            return False
        return True

    def on_data_accounting(self, fd, why):
        start = time.time()
        ret = self.on_data(fd, why)
        now = time.time()
        d = now - start
        self.accounting_n_calls += 1
        self.accounting_n_seconds += d
        since_last_accounting = now - self.last_accounting_output
        if since_last_accounting > 10:
            self.accounting_n_calls_total += self.accounting_n_calls
            self.accounting_n_seconds_total += self.accounting_n_seconds
            self.last_accounting_output = now
            self.manager.info("connection from %r: %d calls, %.3f seconds (%.2f calls/s, usage: %.1f%%)" % (
                self.address,
                self.accounting_n_calls_total, self.accounting_n_seconds_total,
                self.accounting_n_calls / since_last_accounting, self.accounting_n_seconds / since_last_accounting * 100))
            
            self.accounting_n_calls = 0
            self.accounting_n_seconds = 0
        return ret

    def set_manager_member(self, member, value):
        self.manager.debug("gui: manager.%s = %r" % (member, value))
        setattr(self.manager, member, value)

    def set_member(self, member, value):
        self.manager.debug("gui: connection.%s = %r" % (member, value))
        setattr(self, member, value)

    def on_data(self, fd, why):
        # blocking read pickle        
        try:
            r = self.pickle_fd.recv()
        except Exception:
            if "reset by peer" in str(sys.exc_info()[1]):
                r = b""
            else:
                Logging.trigger_log_update.remove_hooks_to(self)
                self.manager.error("error on gui connection:\n%s\nclosing connection" % traceback.format_exc())
                self.remove()
                return False
        if r == b"":
            Logging.trigger_log_update.remove_hooks_to(self)
            self.manager.info("gui at %s closed connection" % (self.address, ))
            self.remove()
            return False
            
        if self.manager.current_gui_ctx != self:
            self.manager.current_gui_ctx = self
            self.manager.debug("set current_gui_ctx to remote-gui for %r" % (r[:3], ))
        if r[0] == "proxy":
            request_id, request, args, kwargs = r[1:]
            if type(request) == tuple and len(request) == 2 and request[0] == "connection":
                request = request[1]
                obj = self
            else:
                obj = self.manager
            try:
                ret = getattr(obj, request)(*args, **kwargs)
                ret = "answer", request_id, ret
            except Exception:
                self.manager.error("exception while processing manager gui request from %s:\n%s\n%s" % (
                    self.address,
                    r[1:],
                    traceback.format_exc()))
                ret = "exception", request_id, traceback.format_exc()
            # send pickle
            try:
                self.pickle_send(ret)
            except Exception:
                self.manager.error("error pickling gui answer:\n%s\n%s" % (
                    pprint.pformat(ret),
                    traceback.format_exc()))
                ret = "exception", request_id, traceback.format_exc()
                self.pickle_send(ret)
        elif r[0] == "call":
            obj_type, obj_id, request_id, method, args, kwargs = r[1:]
            klass = eval(obj_type)
            obj = klass.get_instance(obj_id)
            try:
                ret = getattr(obj, method)(*args, **kwargs)
                ret = "answer", request_id, ret
            except Exception:
                self.manager.error("exception while processing manager gui call-request from %s:\n%s\n%s" % (
                    self.address,
                    r[1:],
                    traceback.format_exc()))
                ret = "exception", request_id, traceback.format_exc()
            # send pickle
            try:
                self.pickle_send(ret)
            except Exception:
                self.manager.error("error pickling gui call-answer:\n%s\n%s" % (
                    pprint.pformat(ret),
                    traceback.format_exc()))
                ret = "exception", request_id, traceback.format_exc()
                self.pickle_send(ret)
        elif r[0] == "call_cb":
            obj_type, obj_id, request_id, method, args, kwargs = r[1:]
            klass = eval(obj_type)
            obj = klass.get_instance(obj_id)
            def _cb(ret=None, exception=None):
                if exception is not None:
                    ret = "exception", request_id, exception
                else:
                    ret = "answer", request_id, ret
                try:
                    self.pickle_send(ret)
                except Exception:
                    self.manager.error("error pickling gui call-answer:\n%s\n%s" % (
                        pprint.pformat(ret),
                        traceback.format_exc()))
                    ret = "exception", request_id, traceback.format_exc()
                    self.pickle_send(ret)
            try:
                getattr(obj, method)(_cb, *args, **kwargs)
            except Exception:
                self.manager.error("exception while processing manager gui call_cb-request from %s:\n%s\n%s" % (
                    self.address,
                    r[1:],
                    traceback.format_exc()))
                _cb(exception=traceback.format_exc())
        elif r[0] == "mgr_cb":
            request_id, method, args, kwargs = r[1:]
            def _cb(ret=None, exception=None, request_id=request_id):
                if exception is not None:
                    ret = "exception", request_id, exception
                else:
                    ret = "answer", request_id, ret
                try:
                    self.pickle_send(ret)
                except Exception:
                    self.manager.error("error pickling gui call-answer:\n%s\n%s" % (
                        pprint.pformat(ret),
                        traceback.format_exc()))
                    ret = "exception", request_id, traceback.format_exc()
                    self.pickle_send(ret)
            try:
                getattr(self.manager, method)(_cb, *args, **kwargs)
            except Exception:
                self.manager.error("exception while processing manager gui mgr_cb-request from %s:\n%s\n%s" % (
                    self.address,
                    r[1:],
                    traceback.format_exc()))
                _cb(exception=traceback.format_exc())
        elif r[0] == "send_request":
            try:
                obj_type, obj_id, request_id, req, kwargs = r[1:]
                klass = eval(obj_type)
                obj = klass.get_instance(obj_id)
                def _on_request_response(*args, **kwargs):
                    self.pickle_send(("request_response", request_id, args, kwargs))
                obj.send_request(req, _on_request_response, **kwargs)
            except Exception:
                self.manager.error("error sending request requested from gui:\n%s\n%s" % (
                    pprint.pformat(r),
                    traceback.format_exc()))
        else:
            self.manager.error("received unknown request from manager gui at %s:\n%s" % (
                self.address,
                pprint.pformat(r)))
        return True
        
class Manager(pyutils.hooked_object):
    """
    Manager Process that manages the TOPIC mapping and control the DAEMONS
    """
    # pickling:
    not_to_pickle = set("gui_connections".split(","))
    def __getstate__(self):
        state = {}
        for attr, value in self.__dict__.items():
            if attr in self.not_to_pickle: continue
            state[attr] = getattr(self, attr)
        return state
    def __setstate__(self, state):
        self.__dict__.update(state)

    def __init__(self, arguments):
        # init logging
        pyutils.hooked_object.__init__(self)
        enable_logging_on_instance(self, "Manager")
        Logging.trigger_log_update.gobject_check()
        self.info("ln_manager %s" % config.version)
        self.info("python%s.%s from %s on %s" % (
            sys.version_info[0], sys.version_info[1],
            os.path.realpath(sys.executable),
            sysconfig.get_platform()
        ))

        self.host = None # own host -> filled in read_config
        if struct.pack("H", 0xaabb)[0] == '\xbb':
            self.host_byte_order = "little"
        else:
            self.host_byte_order = "big"
        
        self._parse_arguments(arguments)

        self._notify_gui_idle = None
        self._notify_gui_events = []
        self._logfiles_to_check = set()
        self.objects_with_output = []
        self.output_idle = None
        self.logfile_check_timer = None
        self.gui_connections = []
        self.current_gui_ctx = None
        self.daemons = {} # host -> daemon
        self.processes = []
        self.clients = [] # list of ClientStore objects, clients only have host attribute, no daemon attribute!
        self._new_errors = set()

        # logical objects:
        self.topics = {} # topic_name -> topic
        self.loggers = {}

        self.socket_client = None
        self.io_watch = None
        self.next_request_id = 1 # for fake service requests sent from manager
        
        self._open_socket()

        if self.sysconf.instance_config.pid_file:
            pid = os.getpid()
            self.debug("write pid file %r, pid %s" % (self.sysconf.instance_config.pid_file, pid))
            with open(self.sysconf.instance_config.pid_file, "w") as fp:
                fp.write("%s" % pid)
        
        # process management
        self.process_output_profiling_timer = None
        self._state_check_idle_id = None
        self._state_check_timeout_id = None
        self._states_to_check = []
        self._new_states_to_check = [] # when others are locked
        self._states_locked = False
        self._gui_questions_pending = set()        
        self.scheduled_state_check_deadline = None
        self.scheduled_state_check_id = None
        
        # process startup
        def startup(start):
            self.info("requesting start of %r" % start)
            if start in self.sysconf.groups:
                for obj in self.sysconf.groups[start].group_members:
                    startup(obj.name)
                return
            if start in self.sysconf.processes:
                ptype = "Process"
                state = "start"
            elif start in self.sysconf.states:
                ptype = "State"
                state = "UP"
            else:
                self.error("%r does not name a Process/State or Group!" % start)
                return
            self.set_process_state_request(ptype, start, state)
        for start in self.startups:
            startup(start)
        
        self.events_to_publish = []
        self.event_publisher = None

        signal.signal(signal.SIGHUP, self._on_sighup)

        self.init_plugins()

    def _on_sighup(self, signo, frame):
        GLib.idle_add(self._on_sigup_idle)
    def _on_sigup_idle(self):
        self.info("received SIGHUP, flushing all files!")

        for name, obj in self.sysconf.processes.items():
            if obj.output_logfile is not None and hasattr(obj, "output_logfile_fp") and obj.output_logfile_fp is not False:
                self.info("  process %s" % name)
                obj.output_logfile_fp.flush()

        if Logging.file_fp:
            Logging.file_fp.flush()
        self.info("flush done.")
        return False

    def init_plugins(self):
        self.plugin_packages = {}
        self.plugins = {}
        for pname, plugin in self.sysconf.plugins.items():
            try:
                mod = self.plugin_packages[pname] = import_plugin_package(plugin.plugin_package)
                self.debug("successfully imported module file %r" % plugin.plugin_package)
            except Exception:
                self.error("failed to import plugin %r:\n%s" % (pname, traceback.format_exc()))
                continue
            try:
                self.plugins[pname] = mod.plugin(self, plugin)
            except Exception:
                self.error("failed to initialize gui_plugin %r:\n%s" % (pname, traceback.format_exc()))
                continue

    def _get_unbound_socket(self):
        s = socket.socket()
        try:
            import fcntl
            flags = fcntl.fcntl(s, fcntl.F_GETFD)
            fcntl.fcntl(s, fcntl.F_SETFD, flags | fcntl.FD_CLOEXEC)
        except Exception:
            pass
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s

    def _open_socket_any_port(self):
        s = self._get_unbound_socket()
        s.listen(1)
        return s

    def _open_socket(self, port=None):
        if self.io_watch is not None:
            GLib.source_remove(self.io_watch)
            self.io_watch = None
        
        if self.socket_client is not None:
            self.socket_client.close()
            self.socket_client = None
            
        if port is None:
            port = self.sysconf.instance_config.manager_port
        
        if port is None:
            # no ln communication possible
            self.warning("ln communication disabled!")
            self.address_client = None
            self.socket_client = None
            self.io_watch = None
            return
        
        self.address_client = "", port # listen on all interfaces
        if self.isolated_test_mode:
            self.socket_client = self.isolated_test_socket
        else:
            self.socket_client = self._get_unbound_socket()
            try:
                self.socket_client.bind(self.address_client)
            except Exception:
                self.error("could not bind to address %s:\n%s\n"
                           "if you know what you are doing you can start the ln_manager\n"
                           "with another manager-port-number:\n"
                           "  ln_manager -p XYZ -c '%s'\n\n"
                           "is there another ln_manager running on this port?\ntry executing this command:\n"
                           "  netstat -nap | grep %s\n"
                           "or\n"
                           "  ss -nlp | grep %s"
                           % (
                               self.address_client,
                               traceback.format_exc(),
                               config.configuration_file,
                               self.address_client[1],
                               self.address_client[1]
                           ))
                pyutils.system("ss -nlp | grep %s" % self.address_client[1])
                sys.exit(0)
            self.socket_client.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            # set maximum pending connection requests
            self.socket_client.listen(1)
        self.info("listening on '%s:%d' for ln-clients!" % (self.sysconf.instance_config.manager_host, self.address_client[1]))
        # io callbacks
        self.io_watch = GLib.io_add_watch(self.socket_client, GLib.IO_IN, self.accept_client)
        
    def print_interfaces(self):
        import platform
        if "linux" not in platform.system().lower():
            raise Exception("todo: can only print interfaces on linux machines...")
        p = subprocess.Popen("/sbin/ip addr list", shell=True, stdout=subprocess.PIPE)
        out, err = p.communicate()
        interface = None # name, ip
        interfaces = []
        for line in out.split("\n"):
            line = line.rstrip()
            if line and line[0] not in " \t\r\n":
                # new interface
                n, name, rest = line.split(":", 2)
                name = name.strip()
                if name == "lo":
                    continue
                interface = [name, None]
                interfaces.append(interface)
                continue
            if not line:
                interface = None
                continue
            if not interface:
                continue
            if "inet " in line:
                ip = line.split("inet ", 1)[1]
                ip = ip.split(" ", 1)[0]
                if "/" in ip:
                    ip = ip.split("/", 1)[0]
                interface[1] = ip
        self.info("%s: %s" % (
            platform.uname()[1],
            ", ".join(["%s:%s" % (name, ip) for name, ip in interfaces])))
        
    def _usage(self):
        print (
            "\n"
            "usage: ln_manager [-h] ( -c config_filename [MANAGER_OPTIONS] | --connect HOST_OR_IP:PORT )\n"
            "\n"
            "\t-h                  show this usage info and exit\n"
            "\t-c config_filename  read configuration from given file\n"
            "\n"
            "MANAGER_OPTIONS:\n"
            "\t-i instance_name       overwrite configured the name of this ln-instance!\n"
            "\t-p PORT                overwrite configured manager listening port\n"
            "\t--log-level LEVEL      sets the log-level. valid values are: debug, info, warning, error\n"
            "\t--start NAME           starts process/state or group NAME after manager startup (can be specified multiple times)\n"
            "\t--without-gui          start manager without gui\n"
            "\t--present              present manager window after startup\n"
            "\t--profile              collect cProfile data and write to ln_manager.stats\n"
            "\t--show-stats fn        read cProfile file fn and print stats\n"
            "\t--console              start command console\n"
            "\t--console-exec CMD     start console and execute CMD\n"
            "\t--mi-console           start command console in machine-interface-mode\n"
            "\t--export-hosts SSHCFG  read given ln-config file (maybe only hosts.inc.lnc?) and generate ssh config file\n"
            "\t--isolated-test        use unique, generated instance-name & manager-port and use private ln_daemon on localhost\n"
            
            "\n"
            "\t--connect HOST_OR_IP:PORT  start only a gui connected to an already running manager at HOST_OR_IP:PORT\n"
            )

    def _parse_arguments(self, args):
        instance_name = None
        skip = 0
        self.startups = []
        self.with_gui = True
        self.do_present = False
        self.overwrite_port = None
        self.start_console = False
        self.isolated_test_mode = False
        export_hosts = None
        for i, arg in enumerate(args):
            if skip:
                skip -= 1
                continue
            if arg == "--isolated-test":
                self.isolated_test_mode = True
                continue
            if arg == "--present":
                self.do_present = True
                continue
            if arg == "--without_gui" or arg == "--without-gui":
                self.with_gui = False
                self.info("starting manager without gui")
                continue
            if arg == "-i":
                skip = 1
                instance_name = args[i + 1]
                continue
            if arg == "--interfaces":
                sys.exit(self.print_interfaces())
            if arg == "-p" or arg == "--port":
                skip = 1
                self.overwrite_port = int(args[i + 1])
                continue
            if arg == "-c" or arg == "--config":
                skip = 1
                config.configuration_file = args[i + 1]
                continue
            if arg == "--log-level":
                skip = 1
                level = args[i + 1]
                from . import Logging
                Logging.levels.index(level)
                Logging.stdout_level = level
                continue
            if arg == "--start":
                skip = 1
                self.startups.append(args[i + 1])
                continue
            if arg == "--profile":
                import cProfile
                self.profile = cProfile.Profile()
                self.profile.enable()
                continue
            if arg == "--console" or arg == "--mi-console":
                self.start_console = True
                continue
            if arg == "--console-exec":
                self.start_console = True
                skip = 1
                continue
            if arg == "--show_stats" or arg == "--show-stats":
                import pstats
                fn = args[i + 1]
                stats = pstats.Stats(fn)
                stats.strip_dirs()
                #stats.sort_stats("calls")
                #stats.sort_stats("cumulative")
                stats.sort_stats("time")
                #stats.sort_stats("pcall")
                stats.print_stats()
                #import pdb
                #pdb.set_trace()
                sys.exit(0)
            if arg == "--export-hosts":
                skip = 1
                export_hosts = args[i + 1]
                continue
            self._usage()
            sys.exit(0)
        
        if export_hosts:
            self.sysconf = SystemConfiguration(self, only_export_hosts=True)
            self.sysconf.export_hosts(export_hosts)
            sys.exit(0)

        if self.isolated_test_mode:
            # open listening socket and get free port from OS
            self.isolated_test_socket = self._open_socket_any_port()
            self.overwrite_port = self.isolated_test_socket.getsockname()[1]
            self.info("isolated-test mode on manager port %r" % self.overwrite_port)

        self.sysconf = SystemConfiguration(self)
        self.host = self.sysconf.manager_host
        if instance_name is not None:
            self.sysconf.instance_config.name = instance_name
        #self.sysconf.print_out()
        if self.sysconf.instance_config.log_file:
            from . import Logging
            Logging.file_name = self.sysconf.instance_config.log_file
            Logging.file_size_limit = self.sysconf.instance_config.log_file_size_limit
            Logging.file_keep_count = self.sysconf.instance_config.log_file_keep_count
            Logging.manager = self

    def before_exit(self):
        if self.sysconf.instance_config.pid_file:
            self.debug("removing pid file %r" % self.sysconf.instance_config.pid_file)
            try:
                os.unlink(self.sysconf.instance_config.pid_file)
            except Exception:
                pass
        
    def resolve_dynamic_host(self, node_name, ctx=None):
        if node_name == "<gui_ip>":
            # find host for this ip!
            if ctx is None:
                gui_ip = "127.0.0.1"
            else:
                gui_ip = ctx.address[0]
            host = self.sysconf.get_host(gui_ip, create=True)
            self.debug("resolved <gui_ip> node to host %r" % host)
            return host
        raise Exception("unknown dynamic node %r" % node_name)

    def is_daemon_connected(self, host):
        """
        accepts host_name or host
        """
        try:
            host = self.sysconf.get_host(host, create=False)
        except Exception:
            return False
        daemon = self.get_daemon(host, create=False)
        if not daemon:
            return False
        return daemon.is_connected()
    
    def create_daemon(self, host):
        # this is a new daemon
        daemon = self.daemons[host] = DaemonStore(self, host)
        self.debug("created new daemon for host %r at %#x" % (host, id(daemon)))
        daemon.add_hook("on_daemon_connect", self._got_daemon_connection)
        daemon.add_hook("process_terminated", self.on_process_terminated)
        daemon.add_hook(("response", "state_notification"), self._got_state_notification)
        return daemon

    def get_daemon(self, host, create=True):
        """
        accepts host_name or host

        might create host, if its not already existing.
        returns daemon if we have one for this host 
        otherwise create one if create == True
        """
        host = self.sysconf.get_host(host, create=True, add_to_default_network=True)
        daemon = self.daemons.get(host)
        if not daemon and create:
            daemon = self.create_daemon(host)
        return daemon
    
    def get_or_create_daemon(self, host):
        """
        accepts host_name or host,
        return exiting or create new daemon.
        new daemon is not neccessarily connected
        """
        return self.get_daemon(host, create=True)
    
    # no, otherwise each lambda needs an exception kwarg @needs_finish_callback
    def with_daemon_for_host(self, host, condition, cb, allow_start=True):
        """
        do only use this if you check for daemon.is_connected() in your callback!
        daemon can NEVER be None!
        
        host is name or host
        try to connect daemon. if allowed also start it.

        cb has to check whether daemon is actually connected!
        ( like in DaemonStore.py/create_connection()/with_connection_wrapper()/_with_daemon() )

        rule of thumb:
        if daemon.method(...) has a @create_connection then just do a self.get_daemon(host).method(...)
        instead of using this method!
        """
        daemon = self.get_daemon(host)
        condition = getattr(daemon, condition)
        condition.call(callback(cb, daemon=daemon), allow_start=allow_start)

    def try_to_connect_daemon(self, host):
        daemon = self.get_daemon(host)
        self.get_daemon_connection(daemon, ignore_cb, allow_start=True)

    def _get_tunnel(self, which):
        t = self.sysconf.states.get(which)
        if t is None:
            t = self.sysconf.processes.get(which)
            if t is None:
                raise Exception("there is no state or process named %r!" % which)
            is_proc = True
        else:
            is_proc = False
        return t, is_proc
    
    def tunnel_is_up(self, which):
        return self.is_ready(which)

    def _check_tunnel_state(self, args, mgr, p, old_state):
        which, cb = args
        self.debug("tunnel state %r is %r" % (which, p.state))
        if self.is_ready(which) or "error" in p.state:
            cb()
            return False
        return True
    
    def require_tunnel(self, which, cb):
        t, is_proc = self._get_tunnel(which)
        self.add_hook(("state_changed", which), self._check_tunnel_state, arg=(which, cb))
        requested_state = "start" if is_proc else "UP"
        self.set_requested_state(t, requested_state)
        
    @needs_finish_callback
    def get_daemon_connection(self, daemon, cb, allow_start=True):
        """
        try to connect to daemon

        async method, will return immediately. 
        will call cb with cb(exception=None)
        """
        if daemon.when_connected.is_final():
            if daemon.when_connected.get_reached_or_fail() is True:
                self.debug("daemon when_connected for %s is already reached, not connecting/starting" % daemon.host)                
                return cb() # daemon already connected
            self.error("daemon when_connected failed: %s" % daemon.when_connected.fail_message)
            return cb(exception="daemon when_connected failed: %s" % daemon.when_connected.fail_message)
        
        if daemon.manager_connect_is_running:
            self.debug("connect to daemon %s is already running" % daemon.host)
            #self.debug("connect to daemon %s is already running\nstack from first connect-type:\n%s" % (daemon.host, daemon.first_connect_stack))
            return
        
        daemon.manager_connect_is_running = True
        #daemon.first_connect_stack = "".join(traceback.format_stack())

        if not daemon.host.requires_tunnel:
            return self.get_daemon_connection_bh(daemon, cb, allow_start)
        
        if self.tunnel_is_up(daemon.host.requires_tunnel):
            daemon.debug("required tunnel %r is already up" % daemon.host.requires_tunnel)
            return self.get_daemon_connection_bh(daemon, cb, allow_start)
        
        daemon.info("this daemons needs tunnel %r to be UP before trying to connect" % daemon.host.requires_tunnel)
        def on_tunnel_answer(daemon, cb, allow_start):
            which = daemon.host.requires_tunnel
            if not self.tunnel_is_up(which):
                return cb(exception="could not get tunnel %r up!" % which)
            daemon.info("tunnel %r is now up!" % which)
            self.get_daemon_connection_bh(daemon, cb, allow_start)
        self.require_tunnel(daemon.host.requires_tunnel, callback(on_tunnel_answer, daemon, cb, allow_start))

    def get_daemon_connection_bh(self, daemon, cb, allow_start):
        def _connect_answer(ret=None, exception=None):
            if not exception:
                daemon.manager_connect_is_running = False
                return cb()
            self.debug("daemon.connect() returned exception: %s" % exception)
            have_arbiter = False # assume there is no arbiter running on that host
            daemon.connect_is_running = False
            te = str(exception)

            if "key_challange not passed" in te or "but it is locked" in te:
                daemon.manager_connect_is_running = False
                return cb(exception=exception)
            
            if "was set to never-lock" in te:
                daemon.manager_connect_is_running = False
                return cb(exception=exception)

            if "daemon registration failed" in te:
                daemon.manager_connect_is_running = False
                return cb(exception=exception)
            
            if "no daemon running for" in te:
                # there is a running arbiter - just no daemon for our uid!
                have_arbiter = True
                msg = te
                if msg.startswith("error: "):
                    msg = msg.split(": ", 1)[1]
                self.info("no daemon with our user-id running on host %s: %s" % (daemon.host, msg))
            else:
                self.info("no daemon-arbiter running on host %s: %s" % (daemon.host, exception))
                if "not pingable" in exception:
                    daemon.manager_connect_is_running = False
                    return cb(exception=exception)
            
            if not allow_start:
                self.info("will not try to start a daemon...")
                daemon.manager_connect_is_running = False
                return cb(exception="will not try to start...")
            self._get_daemon_connection_bh(daemon, cb, have_arbiter)

        if self.isolated_test_mode and daemon.host == self.host:
            # start private ln_daemon
            return self._get_daemon_connection_bh(daemon, cb, True)
        
        return daemon.connect(_connect_answer)
    
    def _get_daemon_connection_bh(self, daemon, cb, have_arbiter):
        # now try to start a daemon:
        if not have_arbiter and self.host != daemon.host:
            # connect failed, so we test whether the host is up
            self.debug("trying to ping host %s" % daemon.host)
            try:
                ip = self.host.get_ip_from(daemon.host)
            except Exception:
                self.debug("host %s is not reachable from %s. ping not possible - assume up!" % (daemon.host, self.host))
                ip = None
            if ip and not ping(ip):
                daemon.manager_connect_is_running = False
                raise Exception("host %s is not pingable at %s!" % (daemon.host, ip))
            # host seems to be up, try to start via ssh
            self.debug("host %s seems to be up at %s." % (daemon.host, ip))

        # host is up, so start daemon for our uid!
        def on_start_daemon_return(ret=None, exception=None):
            if exception is not None:
                daemon.manager_connect_is_running = False
                self.stop_daemon(daemon.host)
                if cb: cb(exception="could not start daemon on %s: %s" % (daemon.host, exception))
                return
            
            # daemon now started, try to connect
            def _connect_answer(ret=None, exception=None):
                if exception is not None:
                    daemon.manager_connect_is_running = False
                    return cb(exception="could not connect to daemon on host %s: %s" % (daemon.host, exception))
                if cb: cb()
                return
            
            daemon.connect(_connect_answer)
            
        self.info("trying to start daemon on host %s" % (daemon.host))
        daemon.start_daemon_process(on_start_daemon_return)

    @log_exception
    def _got_daemon_connection(self, daemon):
        """
        called via on_daemon_connect hook
        calls daemon.register_finished() and (("have_daemon", daemon.host.name), daemon)-hook at end.
        should also call trigger_fail() for important daemon conditions!        
        """
        self._got_daemon_connection2(daemon, daemon.list_processes_response_after_start)
        return True

    def _got_daemon_connection2(self, daemon, processes):
        self._new_errors = set()
        seen_processes = set()
        check_for_errors = []
        for pp in processes:
            if len(pp) != 7:
                   self.error("len of pp: %d we want %d" % (len(pp), 7))
            pname, pcommand, ppid, is_ready, start_time, old_process_output, props = pp
            seen_processes.add(pname)
            daemon.debug("already running: %r\n"
                       "  command: %r\n"
                       "  pid: %r, props: %r\n"
                       "  is_ready: %r, start_time: %r\n"
                       "  output: %r\n" % (
                           pname,
                           pcommand,
                           ppid,
                           props,
                           is_ready,
                           start_time,
                           old_process_output
                       )
            )
            # search matching process!
            matching_process = None
            if pname not in self.sysconf.processes:
                self.warning("there is an unknown process running on daemon %s: %s\nadding to list of processes" % (daemon.host, (pname, pcommand, ppid)))

                process = self.sysconf.processes[pname] = Process(self.sysconf, pname)
                process._read_config("command", pcommand)
                process._read_config("node", str(daemon.host))
                process._finalize_config()
                # possibly add to groups
                for gn, g in self.sysconf.groups.items():
                    if g._possibly_add_process(process):
                        self.debug("notify gui for process add %r %r" % (gn, process.name))
                        self._notify_gui(request="add_process", process=process, group_name=gn)
                self._notify_gui(request="add_process", process=process, group_name=None)
            p = self.sysconf.processes[pname]
            p.daemon = daemon
            check_for_errors.append(p)
            p.start_time = start_time
            if props.get("needs_restart") is not None:
                op = props.get("needs_restart")
                if op in p.depends_on_restart and op in self.sysconf.processes:
                    opp = self.sysconf.processes[op]
                    self._add_error(p, ("depends_on_restart", opp))
                    p._needs_restart = True
            p.register_output(old_process_output)
            self._notify_gui(request="process_output", name=p.name, output=old_process_output)
            if (p.state == "stopped" or p.state is None):
                # our state is stopped, remote state is running
                if p.host != daemon.host:
                    self.warning("we found process %s running on daemon %s: but we want to have it on a different host: %s" % (
                            pname, daemon.host, p.host))
                daemon._started_processes[ppid] = p
                p.pid = ppid
                if is_ready:
                    self._update_process_state(p, "ready")
                else:
                    self._update_process_state(p, "started")
                continue
            if daemon.host == p.host:
                continue
            # our state is already running
            self.error("we found process %s running on daemon %s: but we want have it already running on a different host: %r (state there: %r)" % (
                    pname, daemon.host, p.host, p.state))
            # todo: shall we stop this remote process
        # mark unmentioned processes as stopped!
        for pname, p in self.sysconf.processes.items():
            if p.node is None:
                self.warning("process %r has no selected node!" % pname)
                continue
            if p.host != daemon.host:
                continue
            if pname in seen_processes:
                continue
            # only processes which are not mentioned
            self._update_process_state(p, "stopped")

        # state autostarts
        for state_name, state in self.sysconf.states.items():
            if not state.autostart:
                #self.debug("autostart state %r - not autostart" % state_name)
                continue
            if not state.node:
                #self.debug("autostart state %r - no nodes %r" % (state_name, state.node))
                continue
            #self.debug("autostart state %r on nodes %r. this connect: %r" % (state.name, state.node, daemon.name))
            if daemon.host != state.host:
                continue
            self.info("autostart state %r - request UP!" % state_name)
            self._request_state(state, "UP", daemon.host, needed_by="autostart state %r" % state_name, recurse=False)

        for p in check_for_errors:
            self._check_has_all_needed(p)

        if self._new_errors:
            errs = {}
            for obj in self._new_errors:
                errs[obj.name] = obj._errors
            self._notify_gui(request="process_errors", errs=errs)
        self._notify_gui(request="check_for_indirect_errors", pnames=[p.name for p in check_for_errors])

        # retrieve list of ports!
        daemon._list_ports(callback(self._got_daemon_connection21, daemon=daemon), with_last_packet=False)
        
    def _got_daemon_connection21(self, response=None, exception=None, daemon=None):
        if exception is not None:
            self.error("could not finish daemon connection for host %s: failed to list ports: %s" % (
                daemon.host, exception))
            daemon.when_register_finished.trigger_fail()
            daemon.when_receiver_ports_known.trigger_fail()
            return
        
        daemon.existing_ports = response["ports"]
        daemon.debug("have %d existing ports on this daemon!" % len(daemon.existing_ports))
        port_linkage = []
        for port in daemon.existing_ports:
            try:
                port_id, direction, message_size, rate = port[:4]
            except Exception:
                self.error("invalid existing port description on %s: %r" % (daemon.name, port))
                daemon.when_register_finished.trigger_fail()
                daemon.when_receiver_ports_known.trigger_fail()
                return
            if direction == "SOURCE_PORT":
                dir = "source"
            else:
                dir = "destination"
            if port_id[0] == "shm":
                shm_name = port_id[1]
                if len(port_id) == 2:
                    shm_version = 1
                else:
                    shm_version = port_id[2]
                n_buffers = int(shm_name.split("_", 2)[1])
                p = shm_port(daemon, dir, shm_name, message_size, None, n_buffers, shm_version) # WARNING: topic not known!
                if dir == "source":
                    daemon.add_source(p, p, -1, True) # local source!
                    daemon.debug("adding local source: port %s" % (p, ))
            elif port_id[0] == "udp" and len(port_id) == 2:
                # udp receiver
                address = port_id[1]
                p = udp_port(daemon, dir, address, message_size, None) # WARNING: topic not known!
                daemon.add_source(p, p, -1, False) # local source!
                daemon.debug("adding local source: port %s" % (p, ))
            elif port_id[0] == "tcp" and len(port_id) == 2:
                # tcp receiver
                address = port_id[1]
                p = tcp_port(daemon, dir, address, message_size, None) # WARNING: topic not known!
                daemon.add_source(p, p, -1, True) # local source!
                daemon.debug("adding local source: port %s" % (p, ))
            elif port_id[0] == "udp" and len(port_id) == 3:
                # udp sender
                address = port_id[1:]
                p = udp_port(daemon, dir, address, message_size, None) # WARNING: topic not known!
            elif port_id[0] == "tcp" and len(port_id) == 3:
                # tcp sender
                address = port_id[1:]
                p = tcp_port(daemon, dir, address, message_size, None) # WARNING: topic not known!
            else:
                daemon.error("unknown port_id %r!" % (port_id, ))
                continue
            if dir == "destination":
                p.rate = rate
            if port[4]:
                port_linkage.append((p, port[4]))
            daemon.ports.append(p)
            daemon.debug("created port %s" % (p, ))
        # connects source with sink ports on a daemon
        for p, linkage in port_linkage:
            if p.direction == "source":
                destination_port_ids = linkage
                # search&link destinations
                for destination_port_id in destination_port_ids:
                    for op in daemon.ports:
                        daemon.debug("op-get_id: %r" % (op.get_id(), ))
                        if op.get_id() == destination_port_id:
                            if op not in p.destinations:
                                self.debug("add destination1 %s to source port %s" % (op.get_id(), p.get_id()))
                                p.destinations.append(op)
                            op.source = p
                            if op.port_type == "shm" and p.port_type == "udp": # mark shm as unreliable!
                                op.daemon.debug("port1 %s has source %s -> mark as unreliable!" % (op, p))
                                op.is_reliable = False
                            break
                    else:
                        daemon.error("daemon says source port %s is linked to %s. but this port does not exist!" % (
                                p, destination_port_id))
            else:
                source_port_id = linkage
                for op in daemon.ports:
                    if op.get_id() == source_port_id:
                        p.source = op
                        if p.port_type == "shm" and op.port_type == "udp": # mark shm as unreliable!
                            p.daemon.debug("port2 %s has source %s -> mark as unreliable!" % (p, op))
                            p.is_reliable = False
                        if p not in op.destinations:
                            self.debug("add destination2 %s to source port %s" % (p.get_id(), op.get_id()))
                            op.destinations.append(p)
                        break
                else:
                    daemon.error("daemon says destination port %s is linked to %s. but this port does not exist!" % (
                            p, source_port_id))
        
        transportations = []
        ports = daemon.ports
        daemon.ports = []
        ports_to_check = []
        for port in ports:
            if port.port_type in ("udp", "tcp") and port.direction == "destination":
                ports_to_check.append(port)
            else:
                daemon.ports.append(port)
        
        daemon.when_receiver_ports_known.trigger()
        
        source_daemon = daemon
        needed_daemons = []
        waited_for_daemons = []
        source_daemon._checked_all_senders = False
        
        # connects udp/tcp senders with receivers
        def _get_daemons(daemons):
            return ", ".join([ndaemon.host.name for ndaemon in daemons])
        def _print_needed():
            source_daemon.debug("DCW: still have %d daemons to wait for: %#x, %s" % (len(needed_daemons), id(needed_daemons), _get_daemons(needed_daemons)))
        
        def _check_done(daemon, done_cb):
            if not daemon._checked_all_senders:
                return daemon.debug("DCW: not yet _checked_all_senders...")                
            if not needed_daemons:
                daemon.debug("DCW: now all needed daemons have their senders: %s" % _get_daemons(waited_for_daemons))
                return done_cb()
            for d in needed_daemons:
                if not hasattr(d, "_checked_all_senders") or not d._checked_all_senders:
                    break
            else:
                daemon.debug("DCW: all remaining daemons (%s) are _checked_all_senders! continue!" % _get_daemons(needed_daemons))
                return done_cb()
            _print_needed()
            
        def _needed_daemon_senders_ready(target_daemon, daemon, done_cb):
            if target_daemon.when_sender_ports_known.get_reached_or_fail() is not True:
                daemon.warning("DCW: could not successfully connect to daemon on %s: %s" % (
                    target_daemon.host, target_daemon.when_sender_ports_known.fail_message))
            if target_daemon in needed_daemons: needed_daemons.remove(target_daemon)
            _check_done(daemon, done_cb)
            
        def check_port(daemon, source_daemon, port, done_cb):
            """
            target_daemon might be None, because of allow_start=False & a failed try to connect a running daemon on target_host
            """
            target_daemon = daemon
            target_host = target_daemon.host
            daemon = source_daemon
            daemon.debug("DCW: check_port %s: target_daemon on %s/%s is receiver_ports_known!" % (port, target_host, target_daemon.host))

            if target_daemon.when_receiver_ports_known.get_reached_or_fail() is not True:
                # no daemon running on target_host!
                daemon.warning("DCW: no daemon running on target host %s, remove udp sender %r" % (target_host, port))
            else:
                # target_daemon knows its receiver ports (when_receiver_ports_known)
                target_port_nr = port.port[1]
                for target_port in target_daemon.ports:
                    if target_port.port_type not in ("udp", "tcp"):
                        continue
                    if target_port.direction != "source":
                        continue
                    if target_port.port != target_port_nr:
                        continue
                    # match!
                    daemon.debug("DCW: target_daemon on %s has matching receiver %s" % (target_host, target_port))
                    daemon.ports.append(port)
                    target_port.sender = port
                    port.receiver = target_port
                    transportations.append((target_port, port, port.rate, target_port.port_type)) # receiver, sender
                    
                    if target_daemon not in waited_for_daemons:
                        waited_for_daemons.append(target_daemon)
                        daemon.debug("DCW: need to wait for daemon %r" % target_daemon.host.name)
                        needed_daemons.append(target_daemon)
                        
                        target_daemon.when_sender_ports_known.call(
                            callback(_needed_daemon_senders_ready, target_daemon, daemon, done_cb))
                    
                    return check_ports(daemon, done_cb)
                daemon.warning("DCW: target_daemon on %s doesn't have matching receiver port for %s. remove udp sender %r" % (target_host, target_port_nr, port))
            daemon.ports.append(port) # just add here such that remove will work
            daemon.remove_port(port, check_transport_source=False)
            return check_ports(daemon, done_cb)
        
        def check_ports(source_daemon, done_cb):
            if not ports_to_check:
                source_daemon.debug("DCW: processed all ports to check")
                source_daemon._checked_all_senders = True
                return _check_done(source_daemon, done_cb)
            port = ports_to_check.pop(0)
            # check this port!
            target_host = self.sysconf.get_host(port.port[0], create=True, add_to_default_network=True, reachable_from=daemon.host)
            source_daemon.debug("DCW: check port %s on target_host %s" % (port, target_host))

            #source_daemon.debug("need daemon on host %s for port %s" % (target_host, port))
            self.with_daemon_for_host(
                target_host, "when_receiver_ports_known",
                callback(check_port, source_daemon=source_daemon, port=port, done_cb=done_cb),
                allow_start=False)
        check_ports(source_daemon, callback(self._check_transportations, daemon=daemon, transportations=transportations))
        
    def _check_transportations(self, daemon, transportations):
        # assume all internal and udp ports are linked! -> generate transported sources!
        #
        # objective is to remove old, unused ports and to mark all receiver ports as source for their respective source-ports so that
        # they can be reused for new connections!
        #
        # we want to call (daemon).add_source(source, provider, receiver_rate, reliable_transport)
        # for each non-receiver source we find on all daemons that receive!
        daemon.when_sender_ports_known.trigger()        
        for receiver, sender, receiver_rate, port_type in transportations:
            if not receiver.destinations:
                receiver.daemon.error("daemon has receiver port %s without destinations! remove!" % receiver)
                receiver.daemon.remove_port(receiver, check_transport_source=False)
                continue
            if not sender.source:
                sender.daemon.error("daemon has sender port %s without source! remove!=" % sender)
                continue
            source = sender.source
            if source.port_type in ("udp", "tcp"):
                continue # do only transport shm ports!
            def push_sources(provider, source, receiver_rate, port_type=port_type): # its ok for this fcn to use for-loop-variables (its sync)
                if provider == source:
                    # do nothing?
                    return
                reliable_transport = provider.port_type == "tcp"
                provider.daemon.debug("adding source: port %s provides %s" % (provider, source))
                provider.daemon.add_source(source, provider, receiver_rate, reliable_transport) # remote source!
                if provider.port_type != port_type or provider.direction != "source":
                    return
                # provider is a udp receiver/source!
                for connected_port in provider.destinations:
                    #provider.daemon.info(" -> connected port: %s" % connected_port)
                    if connected_port.port_type != port_type:
                        continue
                    if connected_port.direction != "destination":
                        daemon.error("    port %s is listed as target but is not a destination!?" % (connected_port))
                        continue
                    # connected to a udp sender!
                    # find target port of udp sender and add our source!
                    if not hasattr(connected_port, "receiver") or connected_port.receiver is None:
                        daemon.error("    sender port %s has no known receiver port!" % (connected_port))
                        # because this udp sender's target host is not yet connected!!
                        continue
                    push_sources(connected_port.receiver, source, receiver_rate)
            push_sources(receiver, source, receiver_rate)
        
        daemon.tcp_forwards = {}        
        def _on_tcp_forwards(response=None, exception=None):
            if exception is not None:
                self.error("could not list tcp forwards: %s" % exception)
            else:
                for fw in response["tcp_forwards"]:
                    to_ip, to_port, listen_ip, listen_port, keep_listening, data = fw[:6]
                    if daemon.protocol_version >= 24:
                        n_connections, last_change = fw[6:6+2]
                    else:
                        n_connections, last_change = 1, 0 # assume single connection 0s ago
                    forward_key, name = daemon.get_forward_key(fw, with_name=True)
                    daemon.tcp_forwards[forward_key] = listen_port
                    daemon.debug("found forward_key: %r at %d" % (forward_key, listen_port))
                    allow_remove = True
                    if name.startswith("tcp-tunnel-state "):
                        state_name = name.split(" ", 1)[1].split("\0", 1)[0]
                        allow_remove = state_name not in self.sysconf.states
                        if allow_remove:
                            self.debug("tcp-tunnel-state %r is no longer mentioned in our config" % state_name)
                    if allow_remove:
                        daemon.tcp_forward_change(forward_key, name, n_connections, last_change)
                daemon.debug("have %d tcp_forwards on this daemon!" % len(daemon.tcp_forwards))
            self.mark_daemon_register_finished(daemon)

        if daemon.protocol_version >= 6:
            daemon._list_tcp_forwards(_on_tcp_forwards)
        else:
            self.mark_daemon_register_finished(daemon)

    def mark_daemon_register_finished(self, daemon):
        daemon.register_finished()
        self.call_hook(("have_daemon", daemon.host.name), daemon)

        # reset errors on processes of this daemon!
        for pname, p in self.sysconf.processes.items():
            if hasattr(p, "host") and p.host and p.host == daemon.host:
                self._reset_errors_for(p)
        
        for sname, state in self.sysconf.states.items():
            if hasattr(state, "host") and state.host and state.host == daemon.host:
                state.request_active = False
        
        if self.sysconf.instance_config.check_all_states_after_daemon_connect:
            # request CHECK on all states of this host!
            had_log = False
            for sname, state in self.sysconf.states.items():
                if hasattr(state, "host") and state.host and state.host == daemon.host:
                    if not had_log:
                        had_log = True
                        self.debug("checking all states of newly connected daemon %s" % daemon.host)
                    
                    # do not trigger connection to other hosts
                    is_tunnel_state_across_hosts = False
                    cmds = state.get_commands()
                    if "tunnel_exit_target_ip" in cmds:
                        exit_host = cmds["tunnel_exit_host"]
                        is_tunnel_state_across_hosts = exit_host != daemon.host
                    
                    if not is_tunnel_state_across_hosts:
                        self._request_state(state, "CHECK", daemon.host, "daemon connect")
        
        
    def accept_client(self, fd, why):
        """
        accept a new client connection. no daemon is needed for this
        """

        fd, address = self.socket_client.accept()
        
        class early_client(object):
            def __init__(self, mgr, fd, address):
                self.mgr = mgr
                self.magic = []
                self.magic_left = 4
                self.fd = fd
                self.fd.setblocking(0)
                self.address = address
                self.address_route = [(address, self.fd.getsockname(), None)] # first entry's out-addr does not exist!
                self.in_read_source_route = False
                self.trust_address_route = True
                
            def on_data(self, fd, why):
                if self.in_read_source_route:
                    ch = fd.recv(1)
                    if ch == b"":
                        self.mgr.debug("lost early ln_client connection from %s" % self.address_route)
                        return False
                    if ch != b"\0":
                        self.magic.append(ch)
                        return True
                    ar = b"".join(self.magic)
                    ar = ar.decode("utf-8").split(":")
                    caddr, iaddr, daddr = list(zip(ar[::2], list(map(int, ar[1::2]))))
                    self.mgr.debug("got forward marker %s -> %s -fw-> %s" % (
                        caddr, iaddr, daddr))
                    exp_daddr = self.address_route[-1][1] # in-addr of last entry!
                    if daddr != exp_daddr:
                        self.self.mgr.warning("reported dst-addr %s does not match last in-addr %s! do not trust address-route!" % (
                            daddr, exp_daddr))
                        self.trust_address_route = False
                    self.address_route.append((caddr, iaddr, daddr))
                    
                    # now restart reading client magic!
                    self.in_read_source_route = False
                    self.magic = []
                    self.magic_left = 4
                    return True
                
                nd = fd.recv(self.magic_left)
                if nd == b"":
                    self.mgr.debug("lost early ln_client connection from %s" % self.address_route)
                    return False
                self.magic.append(nd)
                self.magic_left -= len(nd)
                if self.magic_left > 0:
                    self.mgr.debug("want %d more magic bytes from early client connection..." % self.magic_left)
                    return True # wait for more
                
                # magic complete!
                magic = b"".join(self.magic)
                # read magic to detect gui!
                if magic == b"GUI\0":
                    conn = gui_connection(self.mgr, self.fd, self.address)
                    self.mgr.info("got gui connection: %r" % conn)
                    self.mgr.gui_connections.append(conn)
                    return False
                elif magic == b"FWD\0": # new-style forwarded connection! read source route!
                    self.in_read_source_route = True
                    # read until next \0 char!
                    self.magic = []
                    return True
                
                # old-style client. will need to ask daemons to know where this client is coming from!
                # create client with unknown host route!                
                self.mgr.create_client(self.fd, magic, self.address_route, trust_address_route=self.trust_address_route)
                return False
        
        ip_address, port = address
        self.debug("new ln_client connection from %s:%s" % (ip_address, port))
        
        ec = early_client(self, fd, address)
        GLib.io_add_watch(fd, GLib.IO_IN, ec.on_data)

        return True

    def _get_host_route(self, address_route, reachable_from=None):
        if reachable_from is None:
            reachable_from = self.host
        host_route = []
        for caddr, iaddr, daddr in address_route:
            host = self.sysconf.get_host(caddr[0], create=True, add_to_default_network=True, reachable_from=reachable_from)
            host_route.append((host, caddr)) # source-host, source socket
            reachable_from = host
        return host_route
        
    def create_client(self, fd, magic, address_route, trust_address_route):
        """
        if trust_address_route is False only address_route[0] is valid!
        """
        self.debug("create client with address route (trusted: %s):\n%s" % (trust_address_route, pprint.pformat(address_route)))
        client = ClientStore(self, fd, address_route, trust_address_route, magic)
        self.clients.append(client)
        return client

    def remove_client(self, client):
        try:
            self.clients.remove(client)
        except Exception:
            return
        self._notify_gui(request="del_client", client=client)

    def iterate_clients_on_host(self, host, return_all=False):
        for client in self.clients:
            if client.host == host:
                yield client
        if not return_all:
            return
        for client in self.clients:
            if client.host == host:
                continue
            yield client # also return all others

    def iterate_clients(self):
        return iter(self.clients)
        
    # communication from manager to gui
    def _notify_gui(self, **args):
        self._notify_gui_events.append(args)
        #self.debug("notify_gui enqueue: %r" % (args, ))
        if self._notify_gui_idle is None:
            self._notify_gui_idle = GLib.timeout_add(50, self._notify_gui_idle_cb)
    def _notify_gui_idle_cb(self):
        #a = time.time()
        while self._notify_gui_events:
            args = self._notify_gui_events.pop(0)
            #self.debug("notify_gui exec: %r" % (args, ))
            self.call_hook("notify_gui", args)
        self._notify_gui_idle = None
        #b = time.time() - a
        #if b > 0.005:
        #    self.debug("notify_gui exec took %.0fms" % (b * 1e3))

        return False


    # process management
    def _update_state(self, obj, state):
        self.debug("_update_state to %s to %s" % (obj.name, state))

        
    def process_start_on_ready(self, obj):
        for opname in obj.start_on_ready:
            if opname in self.sysconf.processes:
                p = self.sysconf.processes[opname]
                self.set_requested_state(p, "start", request_check=False)
            elif opname in self.sysconf.states or (opname.startswith("!") and opname[1:] in self.sysconf.states):
                if opname[0] == "!":
                    up_down = "DOWN"
                    opname = opname[1:]
                else:
                    up_down = "UP"
                p = self.sysconf.states[opname]
                self.set_requested_state(p, up_down, request_check=False)
            self.request_state_check(p)

    def _update_process_state(self, obj, state):
        """
        this one set the new internal state
        and notify's the gui of the state change
        """
        if obj.state != state:
            if obj.state is None and state == "stopped":
                method = self.debug
            else:
                method = self.info
            method("process %r changes state from %r to %r" % (obj.name, obj.state, state))
            if state == "ready" and obj.start_on_ready:
                #self.info("process %r has start_on_ready items..." % obj.name)
                self.process_start_on_ready(obj)

        old_state = obj.state
        obj.state = state
        if state == "stopped":
            obj._last_error = None
        self._notify_gui(request="update_obj_state", obj_type=obj.__class__.__name__, obj_name=obj.name, obj_state=obj.state)

        # check whether we can release current request!
        old_requested_state = obj.requested_state
        if obj.requested_state == "start" and obj.state in ("started", "ready"):
            obj.requested_state = None
        if obj.requested_state == "stop" and obj.state == "stopped":
            obj.requested_state = None
            obj._last_terminate = None
        if old_requested_state != obj.requested_state:
            self._notify_gui(request="update_obj_requested_state", obj_type=obj.__class__.__name__, obj_name=obj.name, obj_requested_state=None)

        # check to remove "depends_on"-errors!
        if obj.state == "ready" or (obj.state == "started" and not obj.has_ready_state()):
            obj.io_scripts.check_autostart()
            self._check_depends_on(obj)

        self.call_hook(("state_changed", obj.name), obj, old_state)

    def set_requested_state(self, obj, state, request_check=True, ctx=None, silent=False):
        if ctx is None:
            ctx = self.current_gui_ctx

        old_ctx = getattr(obj, "requested_state_ctx", None)
        if old_ctx != ctx:
            self.debug("changed req-state-context from %r to %r" % (old_ctx, ctx))
            if ctx is not None:
                obj.requested_state_ctx = weakref.ref(ctx)
            else:
                obj.requested_state_ctx = None
        
        if obj.requested_state != state:
            self.debug("set requested state from %r to %r for %r" % (obj.requested_state, state, obj.name))
            if state in ("UP", "DOWN") and "error" in obj.state.lower():
                self._update_state_state(obj, obj.host, None)
            obj.requested_state = state
            obj.requested_state_time = time.time()
            if state == "stop" and hasattr(obj, "io_scripts"):
                obj.io_scripts.stop()
            self._notify_gui(
                request="update_obj_requested_state",
                obj_type=obj.__class__.__name__,
                obj_name=obj.name,
                obj_requested_state=state)
        elif not silent:
            self.debug("requested state for %r was already %r\n%s" % (obj.name, state, "".join(traceback.format_stack())))
        
        if request_check:
            self.request_state_check(obj)
        
    def set_process_state_request(self, ptype, pname, requested_state, ln_debug=False):
        """
        called from gui with object name
        """
        #print "set process state request", ptype, pname, requested_state
        if ptype == "Process":
            obj = self.sysconf.processes[pname]
            obj.ln_debug = ln_debug
        elif ptype == "State":
            obj = self.sysconf.states[pname]
            obj.last_check = 0 # disable min_check_interval test
        elif ptype == "Group":
            obj = self.sysconf.groups[pname]
                
        if obj.state is None or obj.state == "stopped" and requested_state == "start":
            obj._last_terminate = None
            obj.started_before = []
        
        self.set_requested_state(obj, requested_state)

    def request_state_check(self, obj, verbose=False):
        """
        request a state check for object obj.
        this function will not block, it will add the obj to the list of objects to be 
        checked and register an idle handler if there is not yet one.
        """
        #if not self._states_locked:
        #    #if verbose: self.info("request_state_check: states not locked, add to states_to_check list!")
        #    self._states_to_check.append(obj) # add to list of objects to check
        #else:
        #    #self.error("states are locked when trying to register state check!")
        #    #if verbose: self.info("request_state_check: states locked, add to new_states_to_check list!")
        if obj not in self._new_states_to_check:
            self._new_states_to_check.append(obj)
        self._state_check_idle()


    def add_process_output(self, obj, output, daemon=None):
        """
        `output` is expected to be encoded bytes-string
        """
        if obj.output_logfile is not None:
            if not hasattr(obj, "output_logfile_fp"):
                try:
                    obj.output_logfile_fp = open(obj.output_logfile, "a+b")
                except Exception:
                    self.error("could not open output_logfile %r for process %r:\n%s" % (
                        obj.output_logfile, obj.name, traceback.format_exc()))
                    obj.output_logfile_fp = False
            if obj.output_logfile_fp is not False:
                if debug_repr_output_log and getattr(obj, "display_name", "") in debug_repr_output_log:
                    obj.output_logfile_fp.write(b"%r\n" % output)
                    obj.output_logfile_fp.flush()
                else:
                    obj.output_logfile_fp.write(output)
                self._register_logfile_check(obj)

        if daemon and obj.has_ready_state() and obj.ready_regex is not None and obj.state not in ("ready", "stopping..."):
            if not hasattr(obj, "ready_regex_pattern"):
                obj.ready_regex_pattern = re.compile(obj.ready_regex.encode(obj.output_encoding))
            if not hasattr(obj, "last_incomplete_line"):
                obj.last_incomplete_line = b""
            # treat \r the same as \n
            ready_output = obj.last_incomplete_line + output.replace(b"\r", b"\n")
            lines = ready_output.split(b"\n")
            for line in lines:
                if re.search(obj.ready_regex_pattern, line):
                    self._update_process_state(obj, "ready")
                    if obj.pid:
                        daemon._queue_call(daemon.set_process_ready, ignore_cb, obj.pid)
                    obj.last_incomplete_line = b""
                    self.request_state_check(obj)
                    break
            else:
                if not ready_output.endswith(b"\n"):
                    if len(lines) > 1:
                        obj.last_incomplete_line = lines[-1]
                    else:
                        obj.last_incomplete_line += lines[-1]
                        max_len = 1024
                        if len(obj.last_incomplete_line) > max_len:
                            obj.last_incomplete_line = obj.last_incomplete_line[-max_len:]
                else:
                    obj.last_incomplete_line = b""

        if hasattr(obj, "warning_regex") and obj.warning_regex is not None:
            if not hasattr(obj, "warning_regex_pattern"):
                obj.warning_regex_pattern = re.compile(obj.warning_regex.encode(obj.output_encoding))
            p = obj
            notify_new_warning = False
            for new_line in output.split(b"\n"):
                if re.search(p.warning_regex_pattern, new_line):
                    decoded_new_line = new_line.decode(obj.output_encoding).strip()
                    if p.warning_msg is None:
                        text = decoded_new_line
                    else:
                        text = "%s\n(%s)" % (p.warning_msg, decoded_new_line)
                    p._warnings_log.append((datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), text))
                    self.warning("process %s:\n%s" % (p.name, text))
                    while len(p._warnings_log) > 100:
                        del p._warnings_log[0]
                    notify_new_warning = True
            if notify_new_warning:
                self._notify_gui(request="new_process_warning", name=p.name, warnings=p._warnings_log)

        output_len = len(output)
        if self.process_output_profiling_timer is not None:
            obj.total_output_lines += output.count(b"\n")
            obj.total_output_bytes += output_len

        obj.register_output(output) # todo?

        if obj.output_to_display is not None:
            if b"terminated with" not in output and obj.output_bytes > obj.max_queued_output_bytes and not obj.no_skip_display:
                obj.output_to_display = b"\r\n\r\n((skipped display of %d bytes of output))\r\n\r\n%s" % (obj.output_bytes, output)
                obj.output_bytes = len(obj.output_to_display)
            else:
                obj.output_to_display += output
                obj.output_bytes += output_len
        else:
            obj.output_to_display = output
            obj.output_bytes = output_len
            self.objects_with_output.append(obj)
        if self.output_idle is None:
            self.output_idle = GLib.idle_add(self.push_output)

    def push_output(self):
        if self.objects_with_output:
            obj = self.objects_with_output.pop(0)
            if obj.__class__.__name__ == "State":
                kwargs = dict(state_cmd="unknown")
            else:
                kwargs = dict()
            self._notify_gui(request="process_output", name=obj.name, output=obj.output_to_display, **kwargs)
            obj.output_to_display = None
            obj.output_bytes = 0
        if not len(self.objects_with_output):
            self.output_idle = None
            return False
        return True

    def inject_output(self, obj, text, **kwargs):
        """
        `text` is expected to be unicode (or decoded py2-str)
        """
        data = text.encode(obj.output_encoding)
        self.add_process_output(obj, data)

    def _request_state(self, state, up_down, host, needed_by=None, recurse=True, force_check=False):
        self.debug("state %r on host %s requesting %s, current: %r, force_check: %r" % (state.name, state.host, up_down, state.state, force_check))
        #if host != state.host:
        #    self.error("state stored host is %r. requested host is %r!" % (state.host, host))
        host = state.host

        if up_down != state.state and up_down != state.requested_state:
            self.set_requested_state(state, up_down, request_check=False)

        # check dependencies!
        # (remove current state if it is a depends_on_restart and dependency is not UP)
        # taken from processes
        if needed_by is None:
            needed_by = [state.name]
        else:
            needed_by = list(needed_by)
            needed_by.append(state.name)

        all_deps_started = True
        if up_down == "CHECK":
            deps_on = []
        else:
            deps_on = list(state.depends_on)
            deps_on.extend(state.depends_on_restart)

            if "error" in state.state.lower():
                raise StartupException("requesting state %r UP/DOWN reported error!" % state.name)

        for dep in deps_on:
            if dep in self.sysconf.processes:
                if dep in needed_by:
                    self.info("ignoring circular dependency from %r back to %r" % (state.name, dep))
                    continue
                p = self.sysconf.processes[dep]
                try:
                    is_started = self._process_start(p, needed_by)
                except StartupException as e:
                    raise StartupException("can not query state %r because one of its dependencies can not start:\n%s" % (state.name, e.msg))
                self.debug("state %r needs dependency %r started: %s" % (state.name, dep, is_started))
                if not is_started:
                    #self.debug("dependency process %r is not yet started!" % p.name)
                    all_deps_started = False
                    #return False # not all dependencies fullfilled yet!
                #self.debug("dependency process %r is started!" % p.name)
                continue
            if dep in self.sysconf.states or (dep.startswith("!") and dep[1:] in self.sysconf.states):
                if dep[0] == "!":
                    dep_up_down = "DOWN"
                    dep = dep[1:]
                else:
                    dep_up_down = "UP"
                if dep in needed_by:
                    self.info("ignoring circular dependency from %r back to state:%r" % (state.name, dep))
                    continue
                p = self.sysconf.states[dep]
                try:
                    is_started = self._request_state(p, dep_up_down, host, needed_by)
                except StartupException as e:
                    raise StartupException("can not quest state %r because one of its state dependencies can not start:\n%s" % (state.name, e.msg))
                self.debug("state %r needs dependency state %r %s: %s" % (state.name, dep, dep_up_down, is_started))
                if not is_started:
                    #self.debug("dependency process %r is not yet started!" % p.name)
                    all_deps_started = False
                    #return False # not all dependencies fullfilled yet!
                #self.debug("dependency process %r is started!" % p.name)
                continue
        if not all_deps_started:
            self.debug("---\nstill dependencies missing for state %r\n---" % state.name)
            return False
        if state.depends_on:
            self.debug("---\nall dependencies for state %r are fullfilled!\n---" % state.name)

        
        if not force_check and len(state._errors):
            self.debug("force_check of state %r because there were errors" % state.name)
            force_check = True
            
        self._reset_errors_for(state)
        
        if state.last_check == 0:
            force_check = True # no last check or check via gui!
        

        if not force_check:
            age = time.time() - state.last_check
            if age > state.min_check_interval:
                self.debug("force_check of state %r because last check is already %.1fs seconds ago" % (state.name, age))
                force_check = True
                state.state = "unknown"
        
        if not force_check and state.state == up_down:
            return True # finished!

        daemon = self.get_daemon(host)

        if state.request_active:
            self.debug("state %r: want %r, but still need to wait for state_notification of last request (since %.1fs)..." % (
                state.name, up_down, time.time() - state.last_check))
            return False
            
        if force_check or state.state not in ("UP", "DOWN"):
            up_down = "CHECK"
        
        if up_down == "CHECK" and not state.is_recheck_allowed(daemon.register_finished_time):
            age = time.time() - state.last_check
            rem = state.min_check_interval - age
            self.debug("state %r: re-check not yet allowed. waiting for min_check_interval %.1fs..." % (state.name, rem))
            return False
        
        state.last_check = time.time()
        state.last_command = up_down
        state.request_active = True # until we get a state notification
        
        def _on_answer(msg=None, exception=None):
            if exception is not None:
                state.request_active = False
                self.error("error requesting state %r to %r: %s" % (state.name, up_down, exception))
                self.inject_output(state, "error requesting state %r:\n%s\n" % (up_down, exception), state_cmd=up_down)

                self._update_state_state(state, daemon.host, "error")
                self.set_requested_state(state, None, request_check=False)

        daemon.request_state(
            _on_answer,
            up_down,
            state.name,
            state.get_commands(), 
            state.os_state
        ) # daemon will notify us of a state change - no sync response needed!
        return False # report as not reached!

    def add_gui_question(self, question_type, key, info):
        question = question_type, key
        if question in self._gui_questions_pending:
            self.debug("gui question %r is already pending." % (question, ))
            return False
        self._gui_questions_pending.add(question)
        info["question_key"] = key
        self._notify_gui(request="gui_question", question=question_type, q=info)

    def _is_process_started(self, obj):
        """
        is process started?
        (not necessarily "ready")
        """
        return obj.state in ("started", "ready")

    def _is_process_ready(self, obj):
        """
        did process reach its "final" "up" state?
        (either "started" or "ready")
        """
        if obj.state == "started":
            return not obj.has_ready_state() # finished
        if obj.state == "ready":
            return True # finished
        return False

    def _is_state_ready(self, state_name):
        """
        is state with given name in required-state?
        required-state is "DOWN" if state_name starts with "!"
        otherwise "UP"
        """
        if state_name.startswith("!"):
            required_state = "DOWN"
            state_name = state_name[1:]
        else:
            required_state = "UP"
        try:
            state = self.sysconf.states[state_name]
        except Exception:
            raise Exception("unknown state of name %r!" % state_name)
        return state.state == required_state

    def is_started(self, name):
        """
        is process or state of given name in its "started" state?
        for states "started" is equal to "ready"
        (this function used to require "ready" from processes!)
        """
        p = self.sysconf.processes.get(name)
        if p:
            return self._is_process_started(p)
        return self._is_state_ready(name)

    def is_ready(self, name):
        """
        is process or state of given name in its "ready" state?
        """
        p = self.sysconf.processes.get(name)
        if p:
            return self._is_process_ready(p)
        return self._is_state_ready(name)

    def _process_start(self, obj, needed_by=None):
        """
        will return True is process is already started,
        otherwise will send a start-request to the daemon and return False
        """
        
        if obj.requested_state_time is None:
            obj.requested_state_time = time.time()
            obj._last_terminate = None
            obj.started_before = []

        if hasattr(obj, "dynamic_host") and obj.dynamic_host:
            pctx = getattr(obj, "requested_state_ctx", None)
            if pctx: pctx = pctx()
            obj.host = self.resolve_dynamic_host(obj.node, pctx)
            
        # start this process!
        if self._is_process_started(obj):
            is_ready = self._is_process_ready(obj)
            self.debug("process %r already started. is already ready: %r" % (obj.name, is_ready))
            return is_ready

        # not yet started, request start!
        self.set_requested_state(obj, "start", request_check=False, silent=True)
        
        if obj.state is None: # daemon not yet connected!
            obj._mark_as_stopped_if_daemon_connected()
            if obj.state is None: # get state of this proc (start daemon)
                self.request_state_check(obj)
                return False
        if obj.state == "started" and obj.has_ready_state():
            return False # wait until ready...
        if obj.state and "starting" in obj.state:
            return False # wait until start is finished!
        if obj.state and "error" in obj.state:
            post = ": %s" % obj._last_error if obj._last_error else ""
            raise StartupException("can not start %r: %s%s" % (obj.name, obj.state, post))
        if obj.requested_state == "stop":
            raise StartupException("stop requested for process %r" % obj.name)

        if obj._last_terminate is not None and not obj.no_error_on_stop and (not obj.no_error_on_successful_stop or not obj._last_stop_successful): # todo: what to do with this?! 
            if obj.auto_restart_interval is None:
                # mark as done - we are not doing automatic restarts!
                #self.debug("this process has no automatic restart!")
                self.set_requested_state(obj, None, request_check=False, silent=True)
                self.warning("process %r stopped unexpectedly before! please check and start manually!" % obj.name)
                obj._last_terminate = None
                raise StartupException("process %r stopped unexpectedly before! please check and start manually!" % obj.name)
                #return False
            elif obj._last_terminate + obj.auto_restart_interval > time.time():
                # restart interval not reached yet...
                self.debug("this process' automatic restart interval is not reached yet!")
                return False
        obj._last_terminate = None
        self._last_stop_successful = True

        if hasattr(obj, "start_before") and obj.start_before and not obj.ignore_dep:
            to_del = []
            for i, wait_for in enumerate(obj.wait_for_started_before):
                p = self.sysconf.processes[wait_for]
                had_startup_error = p.state and "startup error" in p.state
                if (p._last_stop_time is None or p._last_stop_time <= obj.requested_state_time) and not had_startup_error:
                    self.debug("%s: still have to wait for start_before %r to start/finish! (current_state: %r)" % (obj.name, wait_for, p.state))
                    continue
                if p.state != "stopped" and not had_startup_error:
                    self.debug("%s: still have to wait for start_before %r to finish! (current_state: %r)" % (obj.name, wait_for, p.state))
                    continue
                if had_startup_error:
                    self.info("%s: start_before %r had startup error. (current_state: %r)" % (obj.name, wait_for, p.state))
                    # remove startup error from this one!
                    #self._update_process_state(p, "stopped")
                    self.set_process_state_request("Process", p.name, "stop")
                else:
                    self.info("%s: start_before %r is finished. stop_successful: %s (current_state: %r)" % (obj.name, wait_for, p._last_stop_successful, p.state))
                if had_startup_error or (p.no_error_on_successful_stop and not p._last_stop_successful):
                    obj.wait_for_started_before = []
                    raise StartupException("can not start %r because start_before %r stopped unsuccessful!" % (obj.name, wait_for))
                obj.started_before.append(wait_for)
                to_del.append(i)
            
            # remove to del
            to_del.reverse()
            for td in to_del:
                del obj.wait_for_started_before[td]
            if obj.wait_for_started_before:
                return False
            
            for to_start in obj.start_before:
                if to_start in obj.started_before:
                    continue
                
                p = self.sysconf.processes[to_start]
                
                obj.wait_for_started_before.append(to_start)
                
                if p._last_stop_time and p._last_stop_time > obj.requested_state_time:
                    self.debug("start_before %s terminated %.1fs after %s state_request." % (to_start, p._last_stop_time - obj.requested_state_time, obj.name))
                    continue
                
                self.info("have to start %r before %r!" % (to_start, obj.name))
                p.requested_state_time = None
                try:
                    is_started = self._process_start(p, needed_by)
                except StartupException as e:
                    raise StartupException("can not query state %r because one of its dependencies can not start:\n%s" % (obj.name, e.msg))
                if not is_started:
                    self.debug("start_before %r is not yet started!" % p.name)
                    self.set_process_state_request("Process", to_start, "start")

        if obj.wait_for_started_before:
            return False

        # check dependencies
        all_deps_started = True
        if obj.ignore_dep:
            self.info("ignoring dependencies!")
        else:
            if needed_by is None:
                needed_by = [obj.name]
            else:
                needed_by = list(needed_by)
                needed_by.append(obj.name)
            deps = list(obj.depends_on)
            deps.extend(obj.depends_on_restart)
            def start_dep(dep):
                if dep in self.sysconf.processes:
                    if dep in needed_by:
                        self.info("ignoring circular dependency from %r back to %r" % (obj.name, dep))
                        return True
                    p = self.sysconf.processes[dep]
                    try:
                        is_started = self._process_start(p, needed_by)
                    except StartupException as e:
                        raise StartupException("can not start %r because one of its dependencies can not start:\n%s" % (obj.name, e.msg))
                    #self.debug("process %r needs dependency %r started: %s" % (obj.name, dep, is_started))
                    if not is_started:
                        self.debug("process %r needs dependency %r - not yet started!" % (obj.name, dep))
                        #all_deps_started = False
                    return is_started
                if dep in self.sysconf.states or (dep.startswith("!") and dep[1:] in self.sysconf.states):
                    if dep[0] == "!":
                        up_down = "DOWN"
                        dep = dep[1:]
                    else:
                        up_down = "UP"
                    if dep in needed_by:
                        self.info("ignoring circular dependency from %r back to state:%r" % (obj.name, dep))
                        return True
                    p = self.sysconf.states[dep]
                    try:
                        is_started = self._request_state(p, up_down, obj.host, needed_by)
                    except StartupException as e:
                        raise StartupException("can not start %r because one of its dependencies can not start:\n%s" % (obj.name, e.msg))
                    self.debug("process %r needs dependency state %r %s: %s" % (obj.name, dep, up_down, is_started))
                    return is_started
                # ignore unknown deps?
                
            for i, dep in enumerate(deps):
                if type(dep) != list:
                    if not start_dep(dep):
                        all_deps_started = False
                else:
                    selected_provider, providers = dep
                    # is any of those providers in the requested state?
                    started_providers = []
                    for p in providers:
                        if self.is_ready(p):
                            started_providers.append(p)
                    if not started_providers:
                        # if not, start the one with highest prio
                        if selected_provider is None:
                            selected_provider = deps[i][0] = providers[-1]                            
                        all_deps_started = all_deps_started and start_dep(selected_provider)
                    elif selected_provider not in started_providers:
                        deps[i][0] = started_providers[0]
            if not all_deps_started:
                self.debug("\n\n---\nstill dependencies missing for process %r\n---" % obj.name)
                return False
            if deps:
                self.debug("\n\n---\nall dependencies for process %r are fullfilled!\n---" % obj.name)

        to_wait = obj.host.try_process_start()
        if to_wait is not True:
            self._check_schedule_state_check_in(to_wait)
            return False

        self._update_process_state(obj, "starting...")
        if obj.exec_command is None:
            # dummy command, ask user to start it!
            question = ("start_process", obj.name)
            if question in self._gui_questions_pending:
                self.debug("gui question %r is already pending." % (question, ))
                return False
            self._gui_questions_pending.add(question)
            self._notify_gui(request="gui_question", question=question[0], q={"obj_name": obj.name})
            return False
        self._reset_errors_for(obj)

        p = obj
        daemon = self.get_daemon(p.host)
        def _start_process_return(exception=None):
            if exception is not None:
                self.inject_output(p, "\r\nprocess %s failed to start!\r\n%s\r\n" % (p.name, exception.replace("\n", "\r\n")))
                self._update_process_state(p, "startup error")
                return
            
            self._update_process_state(p, "started")                        
            if p.ready_time:
                if hasattr(p, "_ready_timer") and p._ready_timer is not None:
                    GLib.source_remove(p._ready_timer)
                p._ready_timer = GLib.timeout_add(int(p.ready_time * 1000), self.process_reached_ready_time, daemon, p)

            self._notify_gui(request="set_process_info", name=p.name, pid=p.pid, start_time=p.start_time, stop_time=p.stop_time)
            self._notify_gui(request="process_started", name=p.name)
            self.inject_output(p, "\r\nprocess %s started with pid %s\r\n\r\n" % (p.name, p.pid))

            self.request_state_check(p)

            if p.start_on_ready:
                has_ready_state = p.ready_time is not None or p.ready_regex is not None
                if not has_ready_state:
                    self.info("process %r has no ready state and start_on_ready items: %s" % (p.name, p.start_on_ready))
                    self.process_start_on_ready(p)

        p.io_scripts.register_start()
        daemon.start_process(_start_process_return, p)
        return False

    def _do_scheduled_state_check(self):
        self.scheduled_state_check_deadline = None
        self.scheduled_state_check_id = None
        self._state_check_inner()
        return False
    def _check_schedule_state_check_in(self, t):
        deadline = time.time() + t
        if self.scheduled_state_check_deadline is None or deadline < self.scheduled_state_check_deadline:
            self.scheduled_state_check_deadline = deadline
            if self.scheduled_state_check_id is not None:
                GLib.source_remove(self.scheduled_state_check_id)
            self.scheduled_state_check_id = GLib.timeout_add(int(t * 1000), self._do_scheduled_state_check)

    def _state_check_idle(self):
        if self._state_check_idle_id is None:
            #self.debug("added state check idle")
            self._state_check_idle_id = GLib.idle_add(self._state_check)
        #else:
        #    self.debug("added state check idle was already in!")
        return

    def _state_check_timeout(self, to):
        if self._state_check_timeout_id is None:
            #self.debug("added state check timeout %r" % to)
            self._state_check_timeout_id = GLib.timeout_add(to, self._state_check_to)
        #else:
        #    self.debug("added state check timeout was already in!")
        return

    def _state_check(self):
        if self._state_check_idle_id is not None:
            GLib.source_remove(self._state_check_idle_id)
            self._state_check_idle_id = None
            #self.debug("remove state check idle")
        try:
            return self._state_check_inner()
        except Exception:
            self.error(traceback.format_exc())
        return False

    def _state_check_to(self):
        if self._state_check_timeout_id is not None:
            GLib.source_remove(self._state_check_timeout_id)
            self._state_check_timeout_id = None
            #self.debug("remove state check timeout")
        try:
            return self._state_check_inner()
        except Exception:
            self.error(traceback.format_exc())
        return False

    def _state_check_by_timer(self):
        try:
            ret = self._state_check_inner()
        except Exception:
            self.error(traceback.format_exc())
            ret = False
        if not ret and self._state_check_timer_id is not None:
            GLib.source_remove(self._state_check_timer_id)
            self._state_check_timer_id = None
        return ret

    def _state_check_for_process(self, obj):
        """
        return True to mark object state check as "done"
        """
        #self.debug("check process state %r: requested: %r, current: %r" % (obj.name, obj.requested_state, obj.state))
        if obj.requested_state is None:
            return True

        if obj.host is not None:
            daemon = self.get_daemon(obj.host)
            daemon_ready = daemon.when_register_finished.get_reached_or_fail() is True

        if obj.host is not None and not daemon_ready: # connect daemon to get state of this object!
            if hasattr(obj, "_daemon_connect_running"):
                return False
            self.debug("process %r state is None, connect daemon to get current state" % obj.name)                
            obj._daemon_connect_running = True
            
            def check_state_is_not_none(daemon):
                del obj._daemon_connect_running
                if daemon.when_register_finished.get_reached_or_fail() is True:
                    return self._state_check()
                
                # daemon startup failed!
                msg = daemon.when_register_finished.fail_message
                daemon.error("can not get state of %r, requested_state was %r -> problem connecting daemon!\n%s" % (
                    obj.name, obj.requested_state, msg))
                self._add_error(obj, ("daemon error", "could not contact daemon!"))
                self._update_process_state(obj, "daemon error...")
                obj.requested_state = None
                self._state_check_inner()
                self._notify_gui(
                    request="update_obj_requested_state",
                    obj_type=obj.__class__.__name__, obj_name=obj.name, obj_requested_state=None
                )
                self.add_gui_question("ln_daemon_connect_error", daemon.host.name, dict(
                    hostname=daemon.host.name,
                    output=daemon.get_collected_stdouterr() + "\n" + msg,
                    is_ln_daemon_from_home_trial=False
                ))
                # daemon.reset_conditions() (trigger_fail is no longer persistent!)
            
            self.with_daemon_for_host(
                obj.host, "when_register_finished",
                check_state_is_not_none
            )
            return False
        
        if obj.requested_state == "stop" and (obj.state is None or obj.state in ("stopped", "starting...") or "error" in obj.state):
            self._update_process_state(obj, "stopped")
            self._reset_errors_for(obj)
            obj._last_terminate = None
            return True
        if obj.requested_state == "stop" and obj.state == "stopping...":
            self.debug("wait for termination notification of process %s" % obj.name)
            return False
        
        if obj.requested_state == "stop" and obj.state in ("started", "ready", "stopping deps..."):
            # stop this process
            p = obj
            dependent_procs_running = False
            if not p.dont_stop_dependent_procs:
                # first stop dependent procs
                for op in p._needed_by:
                    if op.__class__.__name__ != "Process":
                        continue
                    if p.name not in op.depends_on_restart:
                        continue
                    if op.state in (None, "stopped") or "error" in op.state:
                        # ok
                        continue
                    if op.state in ("started", "ready"):
                        # request stop!
                        self.set_process_state_request("Process", op.name, "stop")
                    # other state!
                    self.debug("depends_on_restart-process %r is still in state %r" % (op.name, op.state))
                    dependent_procs_running = True

            if dependent_procs_running:
                self._update_process_state(obj, "stopping deps...")
                self.debug("there are still processes running that need process %r... wait for them to stop" % (p.name))
                # check this state with timeout... not idle?!
                return False
            
            self._update_process_state(obj, "stopping...")
            # no more dependent procs running...
            obj._terminate_allowed = True
            if obj.host is None:
                # dummy process, ask user to stop it!
                question = ("stop_process", obj.name)
                if question not in self._gui_questions_pending:
                    self._gui_questions_pending.add(question)
                    self._notify_gui(request="gui_question", question=question[0], q={"obj_name": obj.name})
                return False
            
            def after_stop(exception=None):
                obj._stop_in_progress = False
                # do not del! otherwise terminated request does not arrive!: del daemon._started_processes[obj.pid]
                obj._last_terminate = None
                self._reset_errors_for(obj)
                # wait for terminated message! self._update_process_state(obj, "stopped")

            if hasattr(obj, "_stop_in_progress") and obj._stop_in_progress:
                self.debug("stop of %r already in progress" % obj.name)
                return False

            obj._stop_in_progress = True

            self.get_daemon(obj.host).stop_process(after_stop, obj.pid, obj.term_signal)
            return False

        if obj.requested_state != "start":
            self.warning("unknown requested state %r for %r?" % (obj.requested_state, obj.name))
            return False

        # following is only requested_state == "start"
        
        if obj.state in ("started", "ready"):
            self._update_process_state(obj, obj.state)
            return True
        
        if obj.state in (None, "stopped"):
            try:
                return self._process_start(obj)
            except StartupException as e:
                self.warning("%s" % e.msg)
                self._update_process_state(obj, "startup error...")
                return False
            except Exception:
                # unexpected Exception type...
                self.debug("call stack:\n%s" % ("".join(traceback.format_stack())))
                self.warning("could not process start of %r:\n%s" % (obj.name, traceback.format_exc()))
                self._update_process_state(obj, "startup error...")
                return False
        
        if "error" in obj.state:
            return True

        return False
            
    def _state_check_inner(self):
        #print "\n\nreal state check\n"
        # unregister idle handler
        #gobject.source_remove(self._state_check_idle)
        #self._state_check_idle = None

        # now check all objects in self._states_to_check
        done_objects = []
        if self._states_locked:
            self.warning("state_check_inner called with self._states_locked!")
            return True
        
        self._states_locked = True
        try:
            for i, obj in enumerate(self._states_to_check):
                if obj.__class__.__name__ == "Process":
                    if self._state_check_for_process(obj):
                        done_objects.append(i)
                elif obj.__class__.__name__ == "State":
                    if obj.requested_state is None:
                        done_objects.append(i)
                    else:
                        try:
                            is_started = self._request_state(obj, obj.requested_state, obj.node)
                        except Exception:
                            is_started = False
                            self.warning("could not get state of %r to %r:\n%s" % (obj.name, obj.requested_state, traceback.format_exc()))
                            self._update_process_state(obj, "startup error...")
                        if is_started:
                            obj.requested_state = None
                            self._notify_gui(request="update_obj_requested_state", obj_type=obj.__class__.__name__, obj_name=obj.name, obj_requested_state=None)
                            done_objects.append(i)

            done_objects.reverse()
            for i in done_objects:
                del self._states_to_check[i]

            had_new_states = len(self._new_states_to_check) > 0
            self._states_to_check.extend(set(self._new_states_to_check).difference(self._states_to_check))
            self._new_states_to_check = []
        finally:
            self._states_locked = False

        if self._states_to_check:
            # register timeout to check again!
            if had_new_states:
                self._state_check_idle()
            else:
                self._state_check_timeout(config.state_check_interval)
            return False
        if self._state_check_idle_id is not None:
            self._state_check_idle_id = None
            self.debug("removed state check idle id marker")

        return False

    def process_reached_ready_time(self, daemon, p):
        self._update_process_state(p, "ready")
        daemon.set_process_ready(ignore_cb, p.pid)
        self.request_state_check(p)
        return False

    def get_signal_map(self):
        if hasattr(self, "_signal_map"):
            return self._signal_map
        import signal
        smap = {}
        for name, value in signal.__dict__.items():
            if not name.startswith("SIG") or name == "SIG_IGN":
                continue
            smap[value] = name
        self._signal_map = smap
        return smap
    
    def get_signal_name(self, signo):
        return self.get_signal_map().get(signo, "SIGNAL_%s" % signo)

    def _add_error(self, obj, error):
        if error not in obj._errors:
            obj._errors.append(error)
            if hasattr(obj, "last_check"):
                obj.last_check = 0
            self._new_errors.add(obj)

    def _check_depends_on(self, obj):
        """
        obj is now avaliable, check dependencies to clear errors
        """
        changed = {}
        what = {}
        for p in obj._needed_by:
            pname = p.name
            to_del = []
            if p.__class__.__name__ not in ("Process", "State"): continue
            for i, (reason, arg) in enumerate(p._errors):
                if reason == "depends_on" and arg == obj:
                    to_del.append(i)
            # are there unsatisfied depends_on-alternatives?
            for i, dep in enumerate(p.depends_on):
                if type(dep) != list:
                    continue                
                selected_provider, providers = dep
                if obj.name not in providers:
                    continue
                if selected_provider is None or not self.is_started(selected_provider):
                    self.warning("reassign selected_provider to %r from %r for %r" % (obj.name, selected_provider, p.name))
                    p.depends_on[i][0] = obj.name
                    for k, (reason, arg) in enumerate(p._errors):
                        if reason == "depends_on" and arg.name == selected_provider:
                            to_del.append(k)
            if to_del:
                to_del.reverse()
                for i in to_del:
                    del p._errors[i]
                changed[pname] = p._errors
                what[pname] = p
        if not changed:
            return
        self._notify_gui(request="process_errors", errs=changed)
        for name, errs in changed.items():
            if not errs and pname in what:
                # this one is now error-free!
                self._check_depends_on(what[pname])
        
    def _check_is_needed(self, sp, from_p=None, depth=0):
        """
        process/state 'sp' is no longer avaliable/UP, check dependencies to set errors
        """
        needed = []
        for p in sp._needed_by:
            pname = p.name
            if p.__class__.__name__ not in ("Process", "State"): continue
            if p.state not in ("started", "ready", "UP", "DOWN"):
                #print "not in started or ready: %r" % p.state
                continue
            if sp.name in p.depends_on_restart:
                self._add_error(p, ("depends_on_restart", sp))
                if depth == 0:
                    p._needs_restart = True
                    if p.__class__.__name__ == "Process":
                        self.get_daemon(p.host).set_process_property(ignore_cb, p.pid, "needs_restart", sp.name)
                needed.append(p)
                continue
            elif sp.name in p.depends_on:
                self._add_error(p, ("depends_on", sp))
                needed.append(p)
                continue
            # sp is probably a provider
            for i, dep in enumerate(p.depends_on):
                if type(dep) != list:
                    continue                
                selected_provider, providers = dep
                if selected_provider == sp.name:
                    # is there another provider running for this?
                    running_provider = None
                    for provider in providers:
                        if self.is_ready(provider):
                            # yes!
                            running_provider = provider
                            break
                    if running_provider:
                        self.warning("reassign selected_provider to %r from %r for %r" % (running_provider, sp.name, p.name))
                        p.depends_on[i][0] = running_provider
                        continue
                    # no other provider for this -- error.
                    self._add_error(p, ("depends_on", sp))
                    needed.append(p)
                    continue
                if (selected_provider is None or not self.is_started(selected_provider)) and sp.name in providers and self.is_started(sp.name):
                    self.warning("reassign selected_provider to %r for %r" % (sp.name, p.name))
                    p.depends_on[i][0] = sp.name
                    continue
            for i, dep in enumerate(p.depends_on_restart):
                if type(dep) != list:
                    continue                
                selected_provider, providers = dep
                if selected_provider != sp.name:
                    continue
                # error!
                self._add_error(p, ("depends_on_restart", sp))
                needed.append(p)
        if needed:
            self.warning("process %r is a dependency for %r" % (sp.name, ", ".join([p.name for p in needed])))
        #for n in needed:
        #    self._check_is_needed(n, depth=depth+1)
        return needed

    def _check_has_all_needed(self, sp):
        if sp.state not in ("started", "ready"):
            return
        
        for pname in sp.depends_on:
            if pname not in self.sysconf.processes:
                continue
            p = self.sysconf.processes[pname]
            if p.state != "ready" or (p.state == "started" and not p.has_ready_state()):
                self._add_error(sp, ("depends_on", p))
        
        for pname in sp.depends_on_restart:
            if pname not in self.sysconf.processes:
                continue
            p = self.sysconf.processes[pname]
            if p.state == "ready" or (p.state == "started" and not p.has_ready_state()):
                continue
            daemon = self.get_daemon(p.host)
            if p.host != sp.host and not daemon.is_connected():
                self.info("can not check depends on restart from %r against %r yet because other daemon %r is not yet fully registered!" % (
                        sp.name, pname, p.host))
                continue
            daemon = self.get_daemon(sp.host)
            if p.state == "ready" or (p.state == "started" and not p.has_ready_state()):
                continue
            #self.error("check_has_all_needed for sp %r/%s needs process %r/%s but its state is %r" % (sp.name, sp.selected_node, p.name, p.selected_node, p.state))
            self._add_error(sp, ("depends_on_restart", p))
            sp._needs_restart = True
            if daemon.is_connected():
                daemon.set_process_property(ignore_cb, sp.pid, "needs_restart", p.name)
    
    def stop_daemon(self, daemon_or_host):        
        """
        accepts daemon, host or host_name

        if daemon is connect, send him a stop command such that he will exit
        """
        if type(daemon_or_host) == DaemonStore:
            daemon = daemon_or_host
        else:
            host = self.sysconf.get_host(daemon_or_host)
            daemon = self.get_daemon(host, create=False)
            if daemon is None:
                return
        if not daemon.is_connected():
            return
        self.info("requesting daemon on %s to quit!" % daemon.host)
        daemon.expect_disconnection = True
        try:
            daemon.send_request("quit_daemon", ignore_cb)
        except Exception:
            pass
        try:
            daemon.com.disconnect()
        except Exception:
            pass
        daemon.inform_dead()

    def stop_all_daemons(self):
        for daemon in list(self.daemons.values()):
            self.stop_daemon(daemon)
        self.daemons = {}

    
    def dump_startup_commands(self, ptype, pname):
        p = self.sysconf.processes[pname]
        daemon = self.get_or_create_daemon(p.host)
        ret = async_return()
        def _start_process_answer(out=None, exception=None):
            self.inject_output(p, out)
            ret(out)    
        daemon.start_process(_start_process_answer, p, dump_only=True)
        return ret

    def set_process_ignore_dep(self, pname, ignore_dep):
        p = self.sysconf.processes[pname]
        p.ignore_dep = ignore_dep

    def set_process_attribute(self, pname, attribute, value):
        p = self.sysconf.processes[pname]
        setattr(p, attribute, value)
        if attribute == "ln_debug":
            for client in p.clients:
                if client is not None and client.library_version >= 6:
                    # client is connected -> send enable/disable debug request
                    enable = 0
                    if value:
                        enable = 1
                    client.request_synchronous("enable_debug", enable=enable)
                    client.ln_debug = bool(enable)
            
    def get_unique_client_name(self, name):
        cnt = None
        test_name = name
        while True:
            found = False
            for c in self.iterate_clients():
                if c.program_name == test_name:
                    found = True
                    break
            if not found:
                break
            if cnt is None:
                cnt = 1
            else:
                cnt += 1
            test_name = "%s %d" % (name, cnt)
        return test_name

    def signal_name_process(self, pname, signame):
        obj = self.sysconf.processes[pname]

        def _with_daemon(daemon):
            if not daemon.is_connected():
                self.error("can not signal process %r on %s - failed to connect to daemon!" % (pname, daemon.host))
                return
            
            if "qnx" in daemon.architecture:
                from . import qnx_info
                mod = qnx_info
            elif "sled" in daemon.architecture:
                from . import linux_info
                mod = linux_info
            else:
                mod = None
            signo = None
            if mod is not None and hasattr(mod, "signals") and signame in mod.signals:
                signo = mod.signals.get(signame)
            if signo is None:
                import signal
                signo = int(getattr(signal, signame))

            if obj.pid is None or obj.pid == 0:
                raise Exception("can not signal pid %r! is the process really started?" % obj.pid)
            daemon.signal_process(ignore_cb, obj.pid, signo)
        
        self.with_daemon_for_host(
            obj.host, "when_register_finished",
            _with_daemon
        )

    def signal_process(self, pname, signo):
        obj = self.sysconf.processes[pname]
        self.get_daemon(obj.host).signal_process(ignore_cb, obj.pid, signo)

    def signal_pid(self, host, pid, signo):
        self.get_daemon(host).signal_process(ignore_cb, pid, signo)

    def set_prio(self, cb, host, pid, tid, prio, policy=-1, affinity=None):
        """
        calls cb() or cb(exception=...)
        """
        def _on_answer(ret=None, exception=None):
            if exception is not None: return cb(exception=exception)
            if affinity is not None:
                return self.set_cpu_affinity(cb, host, pid, tid, affinity)
            return cb(ret)
        self.get_daemon(host).set_prio(_on_answer, pid, tid, prio, policy)
            
    def set_cpu_affinity(self, cb, host, pid, tid, affinity):
        """
        calls cb() or cb(exception=...)
        """
        self.get_daemon(host).set_cpu_affinity(cb, pid, tid, affinity)

    def _got_state_notification(self, daemon, r):
        self.debug("received state_notification for state %s to %s" % (r.get("name"), r.get("up_down")))
        state = self.sysconf.states.get(r.get("name"))
        if state is None:
            self.warning("received UNKNOWN state notification for state %s to %s" % (r.get("name"), r.get("up_down")))
            return True
        state.request_active = False
        up_down = r.get("up_down")
        if state.last_command in ("UP", "DOWN") and state.need_check_after_up:
            self.debug("this state needs a check after UP/DOWN")
            up_down = "<unknown>"
            state.last_check = 0
            state.last_command = None
        else:
            state.last_check = time.time()
            
        self._update_state_state(state, daemon.host, up_down)
        if up_down == state.requested_state or state.requested_state == "CHECK":
            self.set_requested_state(state, None, request_check=False, silent=True)
        self._state_check_idle()
        return True

    def _update_state_state(self, state, host, up_down):
        if up_down is None:
            up_down = "<unknown>"
        if state.state != up_down:
            self.debug("state %r on host %s changes state from %r to %r" % (state.name, host, state.state, up_down))
            self.inject_output(state, "new state: %s\n" % (up_down), state_cmd="CHECK")

        old_state = state.state
        state.state = up_down
        self._notify_gui(request="update_state_state", state_name=state.name, host=host.name, up_down=up_down)
        if state.state == state.requested_state:
            state.requested_state = None
            self._notify_gui(request="update_obj_requested_state", obj_type=state.__class__.__name__, obj_name=state.name, obj_requested_state=state.requested_state)
        self.call_hook(("state_changed", state.name), state, old_state)
        self._state_check_idle()

    def _reset_errors_for(self, obj):
        had_errors = len(obj._errors)
        obj._errors = []
        if had_errors:
            self._notify_gui(request="process_errors", errs={obj.name: []})

    def gui_question_answer(self, question, **a):
        if question == "ln_daemon_connect_error":
            question_token = question
        elif question == "start_process":
            obj = self.sysconf.processes[a["obj_name"]]
            question_token = (question, obj.name)
            if a["started"]:
                self._update_process_state(obj, "started")                        
            else:
                self._update_process_state(obj, "startup error...")
        elif question == "stop_process":
            obj = self.sysconf.processes[a["obj_name"]]
            question_token = (question, obj.name)
            self._reset_errors_for(obj)
            self._update_process_state(obj, "stopped")
        elif question == "lost_daemon_connection":
            question_token = (question, a["q"]["hostname"])
        elif question == "config_error":
            question_token = (question, a["q"])
        elif question == "config_problems":
            question_token = question
            if a["apply"]:
                self.problem_answer_cb[0]()
            else:
                self.problem_answer_cb[1]()
        else:
            self.error("received unknwon gui_question-answer to question %s:\n%s" % (question, pprint.pformat(a)))
            return True

        if question_token in self._gui_questions_pending:
            self._gui_questions_pending.remove(question_token)
        return True

    def connect_all_daemons(self):
        hosts = set()
        missing = []
        for pname, p in self.sysconf.processes.items():
            if p.host is None:
                continue
            hosts.add(p.host)
        done = []
        failed = []
        def cb(host, exception=None):
            if exception is not None:
                return failed.append(host)
            done.append(host)
            if (len(done) + len(failed)) == len(hosts):
                if failed:
                    self.error("failed to connect to these daemons: %s" % ", ".join(map(str, failed)))
                else:
                    self.info("connected to all daemons: %s" % ", ".join(map(str, done)))
        for host in hosts:
            self.get_daemon_connection(self.get_daemon(host), callback(cb, host=host), allow_start=True)
        
    def retrieve_process_list(self, host, show_env=False, show_threads=False):
        daemon = self.get_daemon(host)
        def _on_response(pl=None, exception=None):
            if exception is not None:
                self.error("failed to retrieve process list: %s" % exception)
                return
            self._notify_gui(request="new_process_list", host=host, process_list=pl, architecture=daemon.architecture)
        daemon.retrieve_process_list(_on_response, show_env=show_env, show_threads=show_threads)

    def add_topic(self, t):
        self.topics[t.name] = t
        self._notify_gui(request="add_topic", topic=t)
    def del_topic(self, topic_name):
        if topic_name not in self.topics:
            self.error("topic %r is not known!\nknown topics:\n%s" % (
                topic_name,
                "\n".join(list(self.topics.keys()))))
            return
        t = self.topics[topic_name]
        self.debug("about to delete topic %r" % topic_name)
        self._notify_gui(request="del_topic", topic=t)
        del self.topics[topic_name]
        
    def update_topic(self, t):
        self._notify_gui(request="update_topic", topic=t)

    def get_topics(self):
        return self.topics
    def get_topic_names(self):
        return list(self.topics.keys())
    
    def get(self, name):
        return getattr(self, name)

    def update_service(self, client, service_name):
        self._notify_gui(request="update_service", client=client, service_name=service_name)

    def update_client(self, client):
        self._notify_gui(request="update_client", client=client)
        
    def needs_provider(self, service_or_topic_name, cb=None):
        # search whether we have a client providing this service/topic
        for client in self.iterate_clients():
            if service_or_topic_name in client.provided_services:
                self.info("needs_provider %r: client %r provides this service!" % (service_or_topic_name, client.program_name))
                return True # found service provider!
        topic = self.topics.get(service_or_topic_name)
        if topic is not None and topic.publisher is not None:
            self.info("needs_provider %r: client %r provides this topic!" % (service_or_topic_name, topic.publisher.program_name))
            return True # found publisher!
        
        # search processes and states for matching providers!
        matches = []
        for pname, p in self.sysconf.processes.items():
            if service_or_topic_name not in p.provides:
                continue
            prio = p.provides[service_or_topic_name]
            matches.append((prio, p))
        for sname, p in self.sysconf.states.items():
            if service_or_topic_name not in p.provides:
                continue
            prio = p.provides[service_or_topic_name]
            matches.append((prio, p))
        if not matches:
            self.info("needs_provider %r: no client provides this. no process or state from config-file provides this!" % (service_or_topic_name))
            raise Exception("no provider/publisher for %r found running and not mentioned config-file!" % service_or_topic_name)
        matches.sort()
        # found process/state provider for this service/topic!
        # takes highest prio
        provider = matches[-1][1]
        # todo: start provider
        if provider.__class__.__name__ == "Process":
            requested_state = "start"
        elif provider.__class__.__name__ == "State":
            requested_state = "UP"

        if requested_state == provider.state or (requested_state == "start" and provider.state == "ready"):
            self.debug("needs_provider %r: %s %r provides this. requested state %r already active!" % (service_or_topic_name, provider.__class__.__name__, provider.name, requested_state))
            return True
        
        if cb:
            # register callback
            def my_cb(args, manager, obj, old_state):
                cb, req_state = args
                self.debug("needs_provider: %r state change from %r to %r" % (obj.name, old_state, obj.state))
                ret = None
                if req_state == "start":
                    if (obj.state == "ready" or (obj.state == "started" and not obj.has_ready_state())):
                        ret = True
                    elif obj.state == "stopped":
                        ret = False
                elif req_state == "UP":
                    ret = True
                if ret is not None or "error" in obj.state:
                    if ret is None:
                        ret = False
                    self.info("needs_provider: call callback %r with %r!" % (cb, ret))
                    try:
                        cb(ret)
                    except Exception:
                        self.error(traceback.format_exc())
                    return False
                return True
            self.add_hook(("state_changed", provider.name), my_cb, arg=(cb, requested_state))
        self.debug("needs_provider %r: %s %r provides this. requested state %r!" % (service_or_topic_name, provider.__class__.__name__, provider.name, requested_state))
        self.set_process_state_request(provider.__class__.__name__, provider.name, requested_state)
        return provider

    def guess_architecture(self):
        DLRRM_HOST_PLATFORM = os.getenv("DLRRM_HOST_PLATFORM")
        if DLRRM_HOST_PLATFORM is not None:
            return DLRRM_HOST_PLATFORM
        try:
            import platform
            if platform.system() == "Linux":
                if platform.architecture()[0].startswith("32"):
                    return "sled11-x86-gcc4.x"
                return "sled11-x86_64-gcc4.x"
            return "windows-intel-mingw32"
        except Exception:
            uname = os.uname()
            if uname[0] == "QNX":
                vs = uname[2].split(".")
                return "qnx%s-x86-gcc3.3" % (".".join(vs[:2]))
        return "unknown"

    def send_stdin(self, pname, text):
        obj = self.sysconf.processes[pname]
        daemon = self.get_daemon(obj.host)
        if not daemon.is_connected():
            return
        daemon.send_stdin(obj.pid, text)
    def send_winch(self, pname, rows, cols):
        obj = self.sysconf.processes[pname]
        daemon = self.get_daemon(obj.host)
        if not daemon.is_connected():
            return
        daemon.send_winch(obj.pid, rows, cols)
    
    def get_logger(self, name, create=True):
        logger = self.loggers.get(name)
        if logger is None and create:
            logger = self.loggers[name] = Logger(self, name)
        return logger
        
    def on_process_terminated(self, daemon, obj, retval=None, terminated_by_signal=None, status=None):
        obj.stop_time = time.time()

        if obj.ready_time and hasattr(obj, "_ready_timer") and obj._ready_timer is not None:
            GLib.source_remove(obj._ready_timer)
            obj._ready_timer = None
        obj.started_before = []
        was_expected = False
        if obj.state != "stopped":
            if obj._terminate_allowed:
                # this was an expected termination!
                obj._terminate_allowed = False
                obj._last_terminate = None
                was_expected = True
            else:
                obj._last_terminate = time.time()
            self._update_process_state(obj, "stopped")                        

        if retval is None and terminated_by_signal is None:
            omsg = "\r\nprocess with pid %s terminated\r\n" % obj.pid
        elif retval is not None:
            omsg = "\r\nprocess with pid %s terminated with retval: %s\r\n" % (obj.pid, retval)
        else:
            core_dumped_msg = " (no core dumped)"
            if status is not None:
                if os.WCOREDUMP(status):
                    core_dumped_msg = "(core dumped!)"
            omsg = "\r\nprocess with pid %s terminated by signal %s with status: %d %s\r\n" % (
                obj.pid, self.get_signal_name(terminated_by_signal), status, core_dumped_msg)
        self.inject_output(obj, omsg)
        obj._last_stop_time = time.time()
        self._notify_gui(request="set_process_info", name=obj.name, pid=obj.pid, start_time=obj.start_time, stop_time=obj.stop_time)
        if hasattr(obj, "io_scripts"):
            obj.io_scripts.stop()

        # check whether there are other processes in need of this one!
        self._new_errors = set()
        if obj._last_terminate and obj.no_error_on_successful_stop and retval != 0:
            self.warning("this looks like an unsuccessful stop! process returned %r" % retval)
            reason = "terminated"
            self._add_error(obj, (reason, omsg.strip()))
            self._check_is_needed(obj)
            obj._last_stop_successful = False
        elif obj._last_terminate and not obj.no_error_on_stop and not obj.no_error_on_successful_stop:
            self.warning("process %r terminated unexpectedly! check dependencies!" % obj.name)
            reason = "terminated"
            self._add_error(obj, (reason, omsg.strip()))
            self._check_is_needed(obj)
            obj._last_stop_successful = False
        else:
            obj._last_stop_successful = True
            reason = "stopped-by-user"
            if self._check_is_needed(obj):
                self._add_error(obj, (reason, omsg.strip()))
            if not was_expected:
                if obj.start_on_successful_stop and not obj.ignore_dep:
                    for to_start in obj.start_on_successful_stop:
                        self.debug("starting start_on_successful_stop: %r" % to_start)
                        self.set_process_state_request("Process", to_start, "start")

        if self._new_errors:
            errs = {}
            for obj in self._new_errors:
                errs[obj.name] = obj._errors
            self._notify_gui(request="process_errors", errs=errs)
        
        self.request_state_check(obj)
        return True
    
    def open_scope(self, topic, value):
        p = self.sysconf.get_scope_process(topic, value)
        p._not_from_config = True
        if p.state != "ready":
            self.set_requested_state(p, "start")
        else:
            self.show_process_gui(p.name)
            
        return p.name

    def open_notebook(self, fn, related_object_name, teaser=""):
        p = self.sysconf.get_notebook_process(fn, related_object_name)
        p._not_from_config = True
        if teaser:
            for i, (n, v) in enumerate(p.environment):
                if n == "NB_TEASER":
                    del p.environment[i]
                    break
            p.environment.append(("NB_TEASER", repr(teaser)))

        if p.state != "ready":
            self.set_requested_state(p, "start")
        else:
            self.show_process_gui(p.name)
            
        return p.name

    def reload_config(self):
        old_config = self.sysconf
        # load new config
        try:
            self.info("reloading config")
            new_config = SystemConfiguration(self)
        except Exception:
            evalue = str(sys.exc_info()[1])
            
            if "error reading configuration" not in evalue:
                evalue = "error reloading config:\n%s" % traceback.format_exc()
                question = "config_error", traceback.format_exc()
            else:
                question = "config_error", evalue.split("\nException: ", 1)[1]
            
            self._gui_questions_pending.add(question)
            self._notify_gui(request="gui_question", question=question[0], q=question[1])

            self.error(evalue)
            return None

        problems = []
        #problems.append((
        #    "process disappeared", "slow publisher", "uter"))
        # warn about disappeared running processes
        # warn about running processes that changed hosts
        have_problems = False
        for pname, op in old_config.processes.items():
            if op.state not in ("started", "ready"):
                continue
            if getattr(op, "_not_from_config", False):
                problems.append((
                    "no problem: dynamically created process", pname, op))
                continue # ignore dynamically generated processes (they did not came from user config)
            if pname in new_config.processes:
                p = new_config.processes[pname]
                if p.host is None:
                    continue
                if p.host.name != op.host.name:
                    problems.append((
                        "process moved host", pname, op.host.name, p.host.name))
                    have_problems = True
                continue
            # only processes which are not mentioned
            problems.append((
                "process disappeared", pname, op.host.name))
            have_problems = True

        if self.isolated_test_mode: # do not allow change of listening port
            new_config.instance_config.manager_port = old_config.instance_config.manager_port

        # warn about changed manager address
        if old_config.instance_config.manager_port != new_config.instance_config.manager_port:
            problems.append((
                "manager port changed", old_config.instance_config.manager_port, new_config.instance_config.manager_port))
            have_problems = True

        def exchange_config(new_config, problems):
            # solve problems
            for problem in problems:
                p = problem[0]
                pargs = problem[1:]
                if p == "no problem: dynamically created process":
                    pname, op = pargs
                    new_config.processes[pname] = op
                elif p == "process disappeared":
                    # stop process!
                    pname, hostname = pargs
                    obj = self.sysconf.processes[pname]
                    if obj.pid in obj.daemon._started_processes:
                        del obj.daemon._started_processes[obj.pid]
                        obj.daemon.stop_process(ignore_cb, obj.pid, obj.term_signal)
                elif p == "process moved host":
                    # stop process!
                    pname, old_hostname, new_hostname = pargs
                    obj = self.sysconf.processes[pname]
                    del obj.daemon._started_processes[obj.pid]
                    obj.daemon.stop_process(ignore_cb, obj.pid, obj.term_signal)
                elif p == "manager port changed":
                    # close listening socket, reopen
                    self._open_socket(new_config.instance_config.manager_port)
                else:
                    raise Exception("don't know how to solve problem %r:\n%s" % (
                        p,
                        pprint.pformat(pargs)))
            # all problems solved!
            # update existing daemons -> new host!
            for old_host, daemon in list(self.daemons.items()):
                if old_host.name in new_config.hosts:
                    new_host = new_config.hosts[old_host.name]
                    daemon.host = new_host
                    del self.daemons[old_host]
                    self.daemons[new_host] = daemon
            # update process, states from old config
            for pname, p in new_config.processes.items():
                op = self.sysconf.processes.get(pname)
                p.reload_update(op)
            for pname, p in self.sysconf.processes.items():
                np = new_config.processes.get(pname)
                if np:
                    continue
                # does old p has opened window? -> close!
                if p.has_window():
                    p.process_output_window.hide()
                    del p.process_output_window

            # update topics for new topic settings! (hosts changed!)
            for tname, topic in self.topics.items():
                topic.settings = new_config.topic_settings.get(topic.name)
            
            if not self.sysconf.ignore_hosts_from_old_config:
                # save newly discovered hosts into new config
                for hname, host in self.sysconf.hosts.items():
                    if hname not in new_config.hosts:
                        new_config.hosts[hname] = host
                        # recognize networks
                        to_del = []
                        for i, intf in enumerate(host.interfaces):
                            if not intf.network:
                                self.warning("old host %s had interface %r which was NOT in any network!" % (
                                    host, intf.name))
                                to_del.append(i)
                                continue
                            new_network = new_config.networks.get(intf.network.name)
                            if not new_network:
                                self.warning("old host %s had interface %r in network %r which does not exist in newly loaded config!" % (
                                    host, intf.name, intf.network.name))
                                to_del.append(i)
                                continue
                            intf.network = new_network
                            new_network.interfaces.append(intf)
                        to_del.reverse()
                        for i in to_del:
                            del host.interfaces[i]

            # update already connected clients
            for client in self.clients:
                old_host = client.host
                if old_host.name in new_config.hosts:
                    new_host = new_config.hosts[old_host.name]
                    client.host = new_host # maybe update host route!
            
            # todo:
            # manager instance name
            
            # trigger reload of guis
            self.sysconf = new_config
            self.host = self.sysconf.manager_host
            self._notify_gui(request="new_config", new_config=self.sysconf)
            
        def ignore_config(*args):
            pass
            
        if have_problems:
            # ask user whether to solve problems
            question = "config_problems"
            self._gui_questions_pending.add(question)
            self.problem_answer_cb = callback(exchange_config, new_config, problems), callback(ignore_config)
            self._notify_gui(request="gui_question", question=question, q=problems)
        else:
            exchange_config(new_config, problems)
    
    def get_daemons(self):
        return self.daemons
    def get_clients(self):
        return self.clients
    def get_system_configuration(self):
        return self.sysconf          

    def resource_event(self, event, name, interface, client_name):
        self.enqueue_event("ln.resource_event", event=event, name=name, md=interface, client=client_name)
        
    def enqueue_event(self, event_name, **kwargs):
        #self.debug("enqueue event %r: %r" % (event_name, kwargs))
        self.events_to_publish.append((event_name, kwargs))
        if self.event_publisher is None:
            self.event_publisher = GLib.idle_add(self.do_publish_events)
            
    def do_publish_events(self):
        for event_name, args in self.events_to_publish:
            try:
                self.call_hook(event_name, args)
            except Exception:
                self.error("error calling event hooks for %r %r:\n%s" % (
                    event_name, args,
                    traceback.format_exc()))
        self.events_to_publish = []
        self.event_publisher = None
        return False

    def kick_client(self, client):
        client.disconnect()

    def get_topic(self, topic, message_definition_name):
        t = self.topics.get(topic)
        if t is None:
            t = Topic(self, topic, message_definition_name, self.sysconf.topic_settings.get(topic)) # new topic!
            self.add_topic(t)
            return t
        # check wether message definitions match! TODO: need hash from client later!
        if t.md_name != message_definition_name:
            raise Exception("you requested message_definition %r but topic %r uses %r!" % (
                message_definition_name, t.name, t.md_name))
        return t

    def exit(self):
        if hasattr(self, "profile"):
            self.profile.disable()
            self.profile.dump_stats("ln_manager.stats")
            import io
            import pstats
            s = io.StringIO()
            sortby = 'cumulative'
            ps = pstats.Stats(self.profile, stream=s).sort_stats(sortby)
            ps.print_stats()
            print(s.getvalue())
        self.before_exit()
        if self.with_gui:
            gi.require_version('Gtk', '3.0')
            from gi.repository import Gtk
            Gtk.main_quit()
        else:
            from .tools import get_default_glib_mainloop
            get_default_glib_mainloop().quit()

    def search_message_definition_filename(self, md_name):
        return ln.search_message_definition(md_name)

    def create_message_definition(self, name, fn):
        return ln.message_definition(name, fn)
    
    def search_and_create_message_definition(self, md_name, with_size_signature_and_dict=False):
        fn = self.search_message_definition_filename(md_name)
        md = self.create_message_definition(md_name, fn)
        if not with_size_signature_and_dict:
            return md, fn
        hdr = ln.message_definition_header()
        size, signature = hdr.generate_md(None, md_name, md, True, return_signature=True)
        return md, fn, size, signature, md.dict()
        
    
    def _profile_process_outputs(self, clear_only=False):
        now = time.time()
        if not clear_only:
            # display profile!
            since_last = now - self.last_outputs_profile
            stat = []
            for pname, p in self.sysconf.processes.items():
                new_lines = p.total_output_lines - p.last_total_output_lines
                new_bytes = p.total_output_bytes - p.last_total_output_bytes
                if new_lines == 0:
                    continue
                stat.append((
                    #   0               1                       2                     3                       4
                    pname, p.total_output_lines, new_lines / since_last, p.total_output_bytes, new_bytes / since_last))
            stat.sort(key=lambda row: row[0])
            stat.sort(key=lambda row: row[2])
            msg = []
            for pname, tlines, lines_ps, tbytes, bytes_ps in stat:
                msg.append("%5.1fl/s %5.1fkB/s %s (%d lines, %.0fkB)" % (
                    lines_ps, bytes_ps/1024., pname, tlines, tbytes / 1024.))
            if msg:
                self.info("process output profile (%d procs):\n%s" % (len(msg), "\n".join(msg)))
            
        # restart counters
        self.last_outputs_profile = now
        for pname, p in self.sysconf.processes.items():
            p.last_total_output_lines = p.total_output_lines
            p.last_total_output_bytes = p.total_output_bytes
        return True

    def enable_process_output_profiling(self, enable):
        if enable:
            if self.process_output_profiling_timer is None:
                self._profile_process_outputs(True)
                self.process_output_profiling_timer = GLib.timeout_add(5000, self._profile_process_outputs)
        else:
            if self.process_output_profiling_timer is not None:
                GLib.source_remove(self.process_output_profiling_timer)
                self.process_output_profiling_timer = None
        
    def release_all_daemon_resources(self, daemon_or_host):
        """
        accepts daemon, host or host_name
        """
        if isinstance(daemon_or_host, DaemonStore):
            daemon = daemon_or_host
        else:
            daemon = self.get_daemon(daemon_or_host)
        def _do_release():
            if daemon.when_register_finished.get_reached_or_fail() is not True:
                return self.error("can not release resources because daemon failed to connect!")
            def _done(exception=None):
                return
            daemon.release_all_resources(_done)
        daemon.when_register_finished.call(_do_release)

    def get_storage_item(self, storage, item):
        return getattr(self, storage).get(item)

    def get_service_protocol_filter(self, old_client, new_service, cb):
        """
        establish local tcp listening socket
        to receive connections from old_clients of same old version
        those connections then do receive old-formatted-service-requests
        which will then be translated to new_service-clients-version, 
        and then sent via tcp to new_service.

        resource side effect:
        - manager has to create listening socket (iff not avaliable for this service and this old_client-version)
        - possibly multiple tcp tunnels such that old_client can reach the new manager local tcp listening socket
        - possibly multiple tcp tunnels such that the manager can reach the tcp listening socket of new_service
        - new_service will have to bind a tcp listening socket

        on completion calls cb((ip, port)) or cb(exception=...)
        - ip_address reachable from old_client to connect to this new manager local tcp listening socket
        - port of new manager local tcp listening socket
        """

        def get_service_protocol_version(lib_version):
            if lib_version < 13:
                return 0
            return 1
            
        old_service_protocol_version = get_service_protocol_version(old_client.library_version)
        new_service_protocol_version = get_service_protocol_version(new_service.provider.library_version)

        if old_service_protocol_version == new_service_protocol_version:
            raise Exception("don't know how to translate service requests from clients of lib-version %d to services of lib-version %d!" % (
                old_client.library_version, new_service.provider.library_version))

        filter_id = old_service_protocol_version, new_service_protocol_version
        protocol_filter = new_service.service_protocol_filter.get(filter_id)

        if protocol_filter is None:
            protocol_filter = new_service.service_protocol_filter[filter_id] = service_protocol_filter(
                self, new_service, old_service_protocol_version, new_service_protocol_version)
        
        protocol_filter.get_address_for(old_client, cb)

    def stop_all_running_processes(self):
        for daemon in list(self.daemons.values()):
            for proc in list(daemon._started_processes.values()):
                self.set_process_state_request(ptype="Process", pname=proc.name, requested_state="stop")
                    
    def _check_logfiles(self):
        for p in self._logfiles_to_check:
            if hasattr(p, "output_logfile_fp"):
                # assume process
                fp = p.output_logfile_fp
                filename = p.output_logfile
                size_limit = p.output_logfile_size_limit
                keep_count  = p.output_logfile_keep_count
                def proc_fp_setter(fp, p=p):
                    p.output_logfile_fp = fp
                fp_setter = proc_fp_setter
                open_flags = "b"
            else:
                # assume logging module
                fp = p.file_fp
                filename = p.file_name
                size_limit = p.file_size_limit
                keep_count  = p.file_keep_count
                def mod_fp_setter(fp, p=p):
                    p.file_fp = fp
                    return fp
                fp_setter = mod_fp_setter
                open_flags = "b"
            
            if not fp:
                continue
            fp.flush()
            if size_limit is not None:
                try:
                    s = os.path.getsize(filename)
                except Exception:
                    self.info("logfile %r probably disappeared. -> reopen! (%s)" % (filename, str(sys.exc_info()[1])))
                    # close
                    fp.close()
                    # open new
                    fp_setter(open(filename, "a+" + open_flags))
                    continue
                if s > size_limit:
                    # close
                    fp.close()
                    if keep_count == 0:
                        # remove
                        os.unlink(filename)
                    else:
                        # rename
                        os.rename(filename, filename + ".%s" % datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
                        # delete old
                        files = glob.glob("%s.*" % filename)
                        files.sort()
                        while len(files) > keep_count:
                            os.unlink(files[0])
                            del files[0]
                    # open new
                    fp_setter(open(filename, "a+" + open_flags))
                    
        GLib.source_remove(self.logfile_check_timer)
        self._logfiles_to_check = set()
        self.logfile_check_timer = None
        return False

    def _register_logfile_check(self, obj):
        self._logfiles_to_check.add(obj)
        if self.logfile_check_timer is None:
            self.logfile_check_timer = GLib.timeout_add(10000, self._check_logfiles)
            
    def check_file_exists(self, fn):
        return os.path.exists(fn)

    def create_debug_info(self, name_hint=None):
        from . import create_debug_info
        return create_debug_info.create_debug_info(self, name_hint=name_hint)

    def set_sysconf_attribute(self, member, value):
        setattr(self.sysconf, member, value)

    def show_process_gui(self, pname):
        process = self.sysconf.processes[pname]
        if not process.clients:
            self.error("process has no ln clients!")
            return
        had_one = False
        for client in process.clients:
            for service in list(client.provided_services.values()):
                if service.interface == "ln/present_window":
                    service._svc_call_async(cb=None)
                    return
        self.error("process has no ln client with service of type 'ln/present_window'")

    @needs_finish_callback
    def get_topic_info(self, cb, topic_name):
        topic = self.topics.get(topic_name)
        if topic is None:
            return cb(exception="there is no topic of name %r" % topic_name)
        class Job(object):
            def __init__(self, mgr, topic, cb):
                self.mgr = mgr
                self.topic = t = topic
                self.cb = cb
                self.first_rate_zero = True
                subs = []
                self.info = OrderedDict(
                    name=t.name,
                    md_name=t.md_name,
                    md_file=t.md_fn,
                    publisher=t.publisher.program_name if t.publisher else "none",
                    publisher_host=t.publisher.host if t.publisher else "none",
                    source_port=topic.source_port.get_id() if topic.source_port else "none",
                    subscribers=subs
                )
                for sub in topic.subscribers:
                    subs.append(OrderedDict(
                        client=sub.client.program_name,
                        host=sub.client.host,
                        rate=sub.rate,
                        reliable_transport=sub.reliable_transport,
                        buffers=sub.buffers,
                        sub_id=sub.subscription_id
                    ))
                self._got_daemon_state = False
                self._got_last_packet = False

            def get_daemon_state(self):
                self.topic.source_port.daemon.get_state(self.read_daemon_state, port_id=self.topic.source_port.get_id())
                return False
            def read_daemon_state(self, resp, exception=None):
                if exception:
                    self.info["daemon-state"] = "error: %s" % exception
                else:
                    if self.first_rate_zero and resp["rate"] == 0.0:
                        self.first_rate_zero = False
                        GLib.timeout_add(500, self.get_daemon_state)
                        return
                    del resp["request_id"]
                    del resp["response"]
                    del resp["success"]
                    self.info["daemon-state"] = resp
                self.answer()
            def read_last_packet(self, resp, exception=None):
                if exception:
                    self.info["last-packet"] = "error: %s" % exception
                else:
                    del resp["request_id"]
                    del resp["response"]
                    del resp["success"]
                    logging = OrderedDict()
                    log_keys = "enabled, count, divisor, size, only_ts".split(", ")
                    for lkey in log_keys:
                        logging[lkey] = resp["logging_%s" % lkey]
                    self.info["logging"] = logging
                    last_packet = OrderedDict()
                    self.info["last_packet"] = last_packet
                    packet_data = None
                    for key in sorted(list(resp.keys())):
                        if key == "last_packet":
                            md = self.mgr.create_message_definition(self.topic.md_name, self.topic.md_fn)
                            packet = ln.ln_packet(self.topic.md_name, str(md.dict()), md.calculate_size(), use_numpy_arrays=True, auto_cast=True)
                            packet._data_buffer = np.frombuffer(resp[key], dtype=np.uint8)
                            packet._unpack()
                            packet_data = packet.get_dict()
                            continue
                        if key == "packet_count":
                            last_packet["count"] = resp[key]
                            continue
                        if key.startswith("last_packet_"):
                            last_packet[key.split("_", 2)[2]] = resp[key]
                            continue
                        for lkey in log_keys:
                            if key == "logging_%s" % lkey:
                                break
                        else:
                            self.info[key] = resp[key]
                    last_packet["data"] = packet_data
                self.answer()

            def answer(self):
                if self.topic.source_port:
                    if not self._got_daemon_state:
                        self._got_daemon_state = True
                        self.get_daemon_state()
                        return
                    if not self._got_last_packet:
                        self._got_last_packet = True
                        self.topic.source_port.daemon.get_last_packet(self.read_last_packet, port_id=self.topic.source_port.get_id())
                        return
                self.cb(self.info)

        Job(self, topic, cb).answer()

    def get_md_info(self, msg_def):
        seen = set()
        output = []

        if isinstance(msg_def, (list, tuple)):
            to_show = list(msg_def)
        else:
            to_show = [msg_def]

        def format_fields(md, fields, non_primitives):
            out = []
            for type, name, n_elems in fields:
                if n_elems == 1:
                    array_spec = ""
                else:
                    array_spec = "[%s]" % n_elems
                is_pointer = type[-1:] == "*"
                if is_pointer:
                    type = type[:-1]
                    show_pointer = "*"
                else:
                    show_pointer = ""
                out.append("  %s%s %s%s" % (type, show_pointer, name, array_spec))
                non_primitive = md.define_abs_names.get(type)
                if non_primitive:
                    non_primitives[type] = non_primitive
                    if non_primitive not in seen and non_primitive not in to_show:
                        to_show.append(non_primitive)
            return "\n".join(out)

        def format_md(md):
            if md.name in seen:
                return
            seen.add(md.name)
            non_primitives = {}
            output.append("name: %s" % md.name)
            output.append("file: %s" % md.fn)
            if md.is_service:
                output.append("type: service")
            else:
                output.append("type: packet")
            if md.fields:
                if md.is_service:
                    output.append("request fields:")
                else:
                    output.append("fields:")
                output.append(format_fields(md, md.fields, non_primitives))
            if md.is_service and md.resp_fields:
                output.append("response fields:")
                output.append(format_fields(md, md.resp_fields, non_primitives))
            if non_primitives:
                output.append("non primitives:")
                for td, np_ in sorted(list(non_primitives.items())):
                    output.append("  %s: %s" % (td, np_))
            output.append("---")

        while len(to_show):
            md_name = to_show.pop(0)
            md, fn = self.search_and_create_message_definition(md_name)
            format_md(md)

        return "\n".join(output[:-1])

    class get_topic_scheduling_info_job(object):
        @ln.util.args_to_self
        def __init__(self, manager, cb, topic, host_as_string):
            self.rows = []
            self.debug = lambda msg: self.manager.debug("get_topic_sched_info for %r: %s" % (topic.name, msg))

        def _get_sched_info(self, cb, daemon, pid, tid):
            self._get_sched_info_state = cb, daemon
            daemon.get_sched_prio_cpu_for(self._get_sched_info_add_arch, pid=pid, tid=tid)
        def _get_sched_info_add_arch(self, ret=None, exception=None):
            cb, daemon = self._get_sched_info_state
            if exception is not None:
                return cb(exception=exception)
            ret = list(ret)
            ret.insert(0, daemon.architecture)
            cb(tuple(ret))

        def _get_sched_info_for_port(self, cb, port):
            self._get_sched_info_for_port_state = cb, port
            return port.daemon.get_state(self._on_get_sched_info_for_port_port_state, port_id=port.get_id())
        def _on_get_sched_info_for_port_port_state(self, msg=None, exception=None):
            cb, port = self._get_sched_info_for_port_state
            if exception is not None:
                msg = "could not get thread id for port %r: %s" % (port, exception)
                self.manager.error(msg)
                return self._get_sched_info_for_port_cb(exception=msg)
            port.read_info(msg)
            if port.linux_tid is not None:
                tid = port.linux_tid
            else:
                tid = port.thread_handle
            self._get_sched_info_for_port_state = cb, port, tid
            self._get_sched_info(
                self._get_sched_info_for_port_with_pid_tid,
                port.daemon, port.daemon.pid, tid)
        def _get_sched_info_for_port_with_pid_tid(self, ret=None, exception=None):
            cb, port, tid = self._get_sched_info_for_port_state
            if exception is not None: return cb(exception=exception)
            return cb(((port.daemon.host, int(port.daemon.pid), int(tid)), ret))

        def _client_tuple(self, client):
            if self.host_as_string:
                return client.program_name, str(client.host), client.pid
            return client.program_name, client.host, client.pid

        def _check_for_subscribers(self):
            self.debug("check_for_subscribers")
            self.sub_iter = iter(self.topic.subscribers)
            self._add_next_subscriber()
        def _add_next_subscriber(self):
            try:
                sub = next(self.sub_iter)
            except StopIteration:
                self.debug("add_next_subscribers: done!")
                return self.cb(self.rows) # done

            self.debug("add_next_subscribers: %s" % sub)
            last_tid = sub.client.get_port_tids(sub, "input")
            row = ["subscriber", self._client_tuple(sub.client), sub.subscription_id, int(last_tid)]
            self.rows.append(row)
            self._add_next_subscriber_state = sub, row
            self._get_sched_info(
                self._on_add_next_subscriber_sched_info, self.manager.get_daemon(sub.client.host), sub.client.pid, last_tid)
        def _on_add_next_subscriber_sched_info(self, ret=None, exception=None):
            sub, row = self._add_next_subscriber_state
            if exception is not None:
                self.manager.error("could not get sched info for subscriber client %r: %s" % (sub.client, exception))
            else:
                row.append(ret)
            return self._add_next_subscriber()

        def _check_receiver_ports(self):
            self.debug("check_receiver_ports")
            receiver_ports = []
            sources = [ self.topic.source_port ]
            while sources:
                sport = sources.pop(0)
                for oport in sport.destinations:
                    if oport == sport:
                        continue
                    if oport.direction == "destination" and oport.port_type in ("udp", "tcp"):
                        if hasattr(oport, "receiver") and oport.receiver:
                            receiver_ports.append((oport.receiver, oport.port))
                            sources.append(oport.receiver)
            self._check_receiver_ports_state = receiver_ports
            return self._add_next_receiver_port()
        def _add_next_receiver_port(self):
            receiver_ports = self._check_receiver_ports_state
            if not receiver_ports:
                return self._check_for_subscribers()
            rport, address = receiver_ports.pop(0)
            self.debug("add_next_receiver_port: %r on address %r" % (rport, address))
            rrow = ["receiver", rport.daemon.host, rport.get_id(), address]
            self.rows.append(rrow)
            self._add_next_receiver_port_state = rport, rrow
            self._get_sched_info_for_port(self._on_add_next_receiver_port_sched_info, rport)
        def _on_add_next_receiver_port_sched_info(self, ret=None, exception=None):
            rport, rrow = self._add_next_receiver_port_state
            if exception is not None:
                self.manager.error("could not get sched info for port %r: %s" % (rport, exception))
            else:
                rrow.append(ret)
            return self._add_next_receiver_port()

        def _check_for_source_thread(self):
            # do we have a source thread?
            #  either because of different rate target on same host
            #  or because of udp sender targets
            # port has to be shm
            port = self.topic.source_port
            for oport in port.destinations:
                if oport != port:
                    # yes, there are other destination ports -> there must be a source thread!
                    self.debug("check_for_source_thread: get sched-info for %r" % (port))
                    return self._get_sched_info_for_port(self._on_check_for_source_thread_sched_info, port)
            # no source thread, receivers?
            return self._check_receiver_ports()
        def _on_check_for_source_thread_sched_info(self, ret=None, exception=None):
            port = self.topic.source_port
            self.debug("check_for_source_thread: got sched-info for %r: %r" % (port, (ret, exception)))
            if exception is not None:
                self.manager.error("could not get sched info for port %r: %s" % (port, exception))
            else:
                row = ["shm source", self.topic.publisher.host, ret]
                self.rows.append(row)
            return self._check_receiver_ports()


        def _add_publisher(self):
            if self.topic.publisher is None:
                self.debug("no publisher")
                return self._check_for_subscribers()
            p = self.topic.publisher
            last_tid = int(p.get_port_tids(self.topic, "output"))
            row = ["publisher client", self._client_tuple(p), last_tid]
            self.rows.append(row)
            self._add_publisher_state = row
            self.debug("have publisher, ask for sched-info...")
            self._get_sched_info(self._on_add_publisher_answer, self.manager.get_daemon(p.host), p.pid, last_tid)
        def _on_add_publisher_answer(self, ret=None, exception=None):
            self.debug("got publisher sched-info: %r" % ((ret, exception), ))
            row = self._add_publisher_state
            if exception is not None:
                self.manager.error("could not get sched info for publisher client: %s" % exception)
            else:
                row.append(ret)
            self._check_for_source_thread()


        def __call__(self):
            self._add_publisher()

    @needs_finish_callback
    def get_topic_scheduling_info(self, cb, topic_name, host_as_string=False):
        """
        calls cb(sched_info) or cb(exception=...)
        """
        topic = self.topics.get(topic_name)
        if topic is None:
            return cb(exception="there is no topic of name %r" % topic_name)

        job = self.get_topic_scheduling_info_job(self, cb, topic, host_as_string)
        return job()

    # main lnm  API?
    def get_obj_state(self, name):
        """
        return current- and requested state of named object (process or state)
        """
        if name in self.sysconf.processes:
            p = self.sysconf.processes[name]
            return p.state, p.requested_state
        if name in self.sysconf.states:
            p = self.sysconf.states[name]
            return p.state, p.requested_state
        raise Exception("unknown object %r" % name)

    def set_process_command_override(self, pname, override=None):
        self.sysconf.processes[pname].command_override = override
    def set_state_command_override(self, sname, which, override=None):
        """
        which is one of "up", "down", "check"
        """
        self.sysconf.states[sname].command_overrides[which] = override

    def start_io_script(self, pname, script, line, single=False, single_inc=False):
        script = self.sysconf.processes[pname].io_scripts.scripts[script]
        if not single:
            script.do_play(line=line)
        else:
            script.do_step(line=line, inc=single_inc)

    def stop_io_script(self, pname, script):
        script = self.sysconf.processes[pname].io_scripts.scripts[script]
        script.do_stop()

    def get_process_output_lines(self, pname, N=500):
        return self.sysconf.processes[pname].get_output_lines(N)

    def set_process_property(self, cb, pname, pid, name, value):
        p = self.sysconf.processes[pname]
        p.daemon.set_process_property(cb, pid, name, value)
