# This file is part of links and nodes.
# 
# links and nodes is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# links and nodes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
# 
# Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon

import sys
import pyutils
import traceback

from .ClientStore import ClientStore, ln_service
from .DaemonStore import DaemonStore

import links_and_nodes as ln

class ManagerConnection(pyutils.hooked_object, ln.lnm_remote):
    def __init__(self, manager=None, connect=None, mainloop=None):
        if manager is None and connect is None:
            raise Exception("invalid initialization! no manager and no --connect specified!")
        self.connected = False        
        pyutils.hooked_object.__init__(self)
        self.manager = manager
        self.connect = connect
        self.mainloop = mainloop

    def connect_to_manager(self):
        if not self.connect:
            self.connected = True
            self.manager.add_hook("notify_gui", self._gui_notification)
            return
        
        ln.lnm_remote.__init__(self, self.connect, debug=("--debug" in sys.argv),
                               mainloop=self.mainloop)
        self.notifications = []
        self.notification_id = None
        ClientStore.update_via_unpickle = True
        ln_service.update_via_unpickle = True
        DaemonStore.update_via_unpickle = True

    def manager_proxy(self, request, *args, **kwargs):
        if self.connect is not None:
            # request via tcp connection
            return self.request(request, *args, **kwargs)
        # directly pass to local manager object
        self.manager.current_gui_ctx = None # gui context is manager context
        try:
            if request == "get_system_configuration":
                return self.manager.sysconf          

            elif request == "set_process_state_request":
                return self.manager.set_process_state_request(**kwargs)

            elif request == "send_request":
                obj, req, cb = args
                obj.send_request(req, cb, **kwargs)
            
            elif request == "call":
                method = args[0]
                return method(*args[1:], **kwargs)
            
            elif request == "call_cb":
                method = args[0]
                cb = args[1]
                try:
                    method(cb, *args[2:], **kwargs)
                except Exception:
                    self.error("call_cb exception:\n%s" % traceback.format_exc())
                    cb(exception=str(sys.exc_info()[1]))
                
            elif request == "mgr_cb":
                cb = args[1]
                try:
                    method = getattr(self.manager, args[0])
                    method(cb, *args[2:], **kwargs)
                except Exception:
                    cb(exception=traceback.format_exc())
                
            else:
                return getattr(self.manager, request)(*args, **kwargs)
        except Exception:
            self.error("error in manager_proxy while calling\n%r(%r, **%r)\n%s\ncalled from\n%s" % (
                request, args, kwargs,
                traceback.format_exc(),
                "".join(traceback.format_stack()[:-1])
            ))
