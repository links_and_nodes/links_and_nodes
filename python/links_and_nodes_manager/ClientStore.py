"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

import struct
import fnmatch
import inspect
import time
import traceback
import re
import sys
import os
import socket
import pprint

from .coverage_dummy import only_need_coverage_for
from .CommunicationUtils import every, tracked_object, callback, get_max_shm_version_from_lib_version, CommunicationModule
from .Condition import Condition
from .TopicStore import Topic
from .Logging import enable_logging_on_instance
from .py2compat import unicode, inspect_getfullargspec

import links_and_nodes as ln
import pyutils

import gi
gi.require_version('GLib', '2.0')
from gi.repository import GLib # noqa: E402
from .tools import get_default_glib_mainloop # noqa: E402

def concat(tuples):
    for t in tuples:
        for i in t:
            yield i

class ln_service(tracked_object):
    update_via_unpickle = False
    to_pickle = "name,interface,provider,signature,md,service_id,port,fn,time_registered,unix_socket"
    def __getstate__(self):
        state = {}
        for attr in self.to_pickle.split(","):
            attr = attr.strip()
            state[attr] = getattr(self, attr)
        return state
    def __setstate__(self, state):
        self.__dict__.update(state)
        if self.update_via_unpickle:
            self.update_instance(state)

    def __init__(self, name, interface):
        self.name = name
        self.interface = interface
        self.time_registered = time.time()
        self.provider = None
        self.signature = None
        self.port = None # tcp port. None or -1 means that service provider is not yet bound to a tcp port
        self.unix_socket = None # unix domain socket name
        self.md = None
        self.fn = None
        self.service_id = None
        self.svc = None
        self.service_protocol_filter = {}
        tracked_object.__init__(self)
        
    def __deinit__(self):
        tracked_object.__deinit__(self)

    def _destroy(self):
        for fid, sfilter in self.service_protocol_filter.items():
            sfilter._destroy()
        self.service_protocol_filter = {}
        if self.name in self.provider.log_services:
            del self.provider.log_services[self.name]
            for client in self.provider.manager.iterate_clients():
                if client == self.provider:
                    continue
                client.del_service_loggers(self.name)

    def get_instance_id(self):
        return self.name

    def set_provider(self, client, signature, port, unix_socket=None):
        self.provider = client
        self.signature = signature
        self.port = port
        if unix_socket:
            self.unix_socket = unix_socket
        else:
            self.unix_socket = None
        self.service_id = "service (%r, %r, %d)" % (self.name, repr(str(self.provider.host)), self.port)

    def get_message_definition(self):
        if self.md is None:
            self.fn = ln.search_message_definition(self.interface)
            self.md = ln.message_definition(self.interface, self.fn)
        return self.md

    def get_port_at(self, host, cb):
        def get_port_at_bh(port):
            self.port = port
            if host.is_direct_reachable(self.provider.host):
                # TODO: avoid alternate sockets for now!
                ip = host.get_ip_from(self.provider.host, avoid_alternate_socket=True)
                cb((ip, port))
                return
            self.provider.manager.get_daemon(self.provider.host).establish_tcp_forward(
                cb, host, self.provider.host, self.port, True, self.service_id, send_source=False
            )
        
        if self.port in (None, -1):
            self.provider.debug("service %s does not yet have listening tcp port, request one!" % self.name)
            self.provider.request_tcp_port_for_service(self.name, get_port_at_bh)
        else:
            get_port_at_bh(self.port)

    def get_unix_socket(self, cb):
        if self.unix_socket is None:
            self.provider.debug("service %s does not yet have listening unix socket, request one!" % self.name)
            def on_got_unix_socket(unix_socket, cb):
                self.unix_socket = unix_socket
                cb(self.unix_socket)
            self.provider.request_unix_socket_for_service(self.name, callback(on_got_unix_socket, cb))
        else:
            cb(self.unix_socket)
    
    def _get_service_signature(self):
        md = self.get_message_definition()

        hdr = ln.message_definition_header()
        size, signature = hdr.generate_md(None, self.interface, md, True, return_signature=True)

        return dict(
            name=self.interface,
            fn=self.fn,
            definition=repr(md.dict()),
            signature=signature)

    def get_service(self):
        if self.svc is not None:
            return self.svc
        sig = self._get_service_signature()
        definition = sig["definition"]
        signature = sig["signature"]

        self.svc = ln.service_wrapper(self.name, self.interface, definition, signature, None, None)
        self.svc.call_async = self._svc_call_async
        return self.svc

    def _svc_call_async_cb(self, resp, cb):
        if cb is None:
            return
        # unpack response!
        data = resp["response_data"] # todo: expect bytes object here!
        self.svc.return_value = struct.unpack("I", data[:4])
        ln.service_unpack_only(self.svc, data[4:], self.provider.manager.host_byte_order != self.provider.get_endianess())
        cb(self.svc.resp)
        
    def _svc_call_async(self, cb=None, service_request_header=None):
        if self.svc is None:
            self.get_service()
        self.svc._check_request()
        request = ln.service_fill_only(self.svc)
        if self.provider.supports_request_id:
            # prepend request valid header!
            if service_request_header is None:
                service_request_header = ln.LN_LIBRARY_VERSION, ln.LN_MANAGER_CLIENT_ID, self.provider.manager.next_request_id
                self.provider.manager.next_request_id += 1
            hdr = struct.pack("=HII", *service_request_header)
            request = hdr + request
        self.provider.request(
            "call_service",
            name=self.name,
            request=request,
            __binary_fields=["request"],
            cb=callback(self._svc_call_async_cb, cb)
        )

    def send_request(self, request, cb, **kwargs):
        """
        send_request() manager gui interface

        (currently used for parameter reading inside the manager)
        """
        if request == "call":
            svc = self.get_service()
            n = "service_request_header"
            if n in kwargs:
                service_request_header = kwargs[n]
                del kwargs[n]
            else:
                service_request_header = None
            for key, value in kwargs.items():
                setattr(svc.req, key, value)
            self._svc_call_async(cb=cb, service_request_header=service_request_header)
        else:
            raise Exception("unknown service request: %r" % request)

    @staticmethod
    def get_platform_independant_signature(signature):
        try:
            req, resp = signature.split("|", 1)
        except Exception:
            raise Exception("invalid signature string: %r\n%s" % (signature, traceback.format_exc()))
        def get_sig(sig, o=0):
            #print "get_sig %r" % sig[o:]
            out = []
            if not len(sig) or sig[o] == "]":
                return out, o
            last = False
            while o < len(sig):
                if sig[o] == "[":
                    # recurse
                    s, o = get_sig(sig, o + 1)
                    # sig[o] has to be "]"
                    o += 1
                    # sig[0] is " " or "*"
                    is_complex = True
                else:
                    is_complex = False
                m = re.search("[,\]]", sig[o:])
                if m is None:
                    last = True
                    elem = sig[o:]
                    o += len(elem)
                else:
                    elem = sig[o:o+m.start(0)]
                    o += m.start(0)
                    # o should point at ',' or ']'
                #print "is_complex: %s, elem: %r" % (is_complex, elem)
                name, es, ec = elem.split(" ")
                if is_complex:
                    # ignore complex size!
                    es = "1"
                    name = s
                out.append((name, es, ec))
                if last or sig[o] == "]":
                    # end recursion
                    break
                # sig[o] has to be ","
                assert sig[o] == ","
                o += 1
            return tuple(out), o
        return get_sig(req)[0], get_sig(resp)[0]

    def compatible_signature(self, other):
        """
        check whether self.signature and other are compatible!
        byte-exact match not possible, because host-side-pointers might have different sizes!
        decode signature and ignore//recalculate pointer sizes with same/fixed size!
        """
        if not hasattr(self, "platform_independant_signature"):
            self.platform_independant_signature = ln_service.get_platform_independant_signature(self.signature)
        #print "self.platform_independant_signature"
        #pprint.pprint(self.platform_independant_signature)

        platform_independant_signature = ln_service.get_platform_independant_signature(other)
        #print "platform_independant_signature"
        #pprint.pprint(platform_independant_signature)
        return self.platform_independant_signature == platform_independant_signature
        

class ln_event_connection(object):
    def __init__(self, client, name, signature, connect_data, handler):
        self.client = client
        self.event_name = name
        self.interface, self.connect_signature = signature.split(": ", 1)
        self.handler = handler

        # dummy service to interpret connect_data
        cfn = ln.search_message_definition(self.interface)
        self.cmd = ln.message_definition(self.interface, cfn)

        self.cmd.connect_fields = self.cmd.fields
        self.cmd.call_fields = self.cmd.resp_fields

        self.cmd.resp_fields = self.cmd.connect_fields

        connect_svc = ln.service_wrapper(
            self.event_name, self.interface, self.cmd.dict(), "|" + self.connect_signature, None, None)
        need_swap = client.manager.host_byte_order != client.get_endianess()
        ln.service_unpack_only(connect_svc, connect_data, need_swap)
        self.connect_data = connect_svc.resp
        self.client.info("event %r connected with data\n%r" % (self.event_name, self.connect_data))

        # full service to call handler
        self.cmd.fields = self.cmd.call_fields
        self.cmd.resp_fields = []

        hdr = ln.message_definition_header()
        size, signature = hdr.generate_md(None, self.interface, self.cmd, True, return_signature=True)
        connect_signature, self.call_signature = signature.split("|")

        self.full_call_signature = "%s: %s" % (self.interface, self.call_signature)
        self.call_svc = ln.service_wrapper(
            self.handler, self.interface, self.cmd.dict(), self.call_signature, None, None)

        svc_interface = "ln/event/call"
        cfn = ln.search_message_definition(svc_interface)
        md = ln.message_definition(svc_interface, cfn)
        hdr = ln.message_definition_header()
        size, svc_signature = hdr.generate_md(None, svc_interface, md, True, return_signature=True)
        
        self.svc = ln.service_wrapper(
            self.handler, svc_interface, md.dict(), svc_signature, None, None)
        self.svc.req.signature = self.full_call_signature 
        
        self.client.add_manager_event_listener(self.event_name, self)

        self.event_queue = []
        self.enable_queue_processing = False
        if self.event_name == "ln.resource_event":
            # register idle handler to synth add events for existing services / topics
            # but only for add events that are not already in manager.events_to_publish
            queued_news = set()
            for event, kwargs in self.client.manager.events_to_publish:
                if event != "ln.resource_event":
                    continue
                event = kwargs["event"]
                if "_new_" in event:
                    what = event.split("_", 1)[0], kwargs["name"]
                    queued_news.add(what)
                elif "_del_" in event:
                    what = event.split("_", 1)[0], kwargs["name"]
                    if what in queued_news:
                        queued_news.remove(what)

            for client in self.client.manager.iterate_clients():
                for service_name, service in client.provided_services.items():
                    if ("service", service_name) in queued_news:
                        continue
                    self("ln.resource_event", event="service_new_provider", name=service.name, md=service.interface, client=client.program_name)
                for topic_name, topic in client.published.items():
                    if ("topic", service_name) in queued_news:
                        continue
                    self("ln.resource_event", event="topic_new_publisher", name=topic.name, md=topic.md_name, client=client.program_name)
        self.enable_queue_processing = True

        if len(self.event_queue):
            GLib.idle_add(self._do_send_next)
        
    def __call__(self, event_type, **kwargs):
        if event_type != self.event_name:
            return
        if event_type == "ln.resource_event":
            # check match
            if not fnmatch.fnmatch(kwargs["event"], self.connect_data.event_pattern):
                return
            if not fnmatch.fnmatch(kwargs["name"], self.connect_data.name_pattern):
                return
            if not fnmatch.fnmatch(kwargs["md"], self.connect_data.md_pattern):
                return
            if not fnmatch.fnmatch(kwargs["client"], self.connect_data.client_pattern):
                return
        for name, value in kwargs.items():
            setattr(self.call_svc.req, name, value)
        self.call_svc._check_request()
        event_data = ln.service_fill_only(self.call_svc)
        self.event_queue.append(event_data)
        self.client.debug("event %r name %r enqueued!\n" % (kwargs["event"], kwargs["name"]))
        if self.enable_queue_processing and len(self.event_queue) == 1:
            # first event
            self._do_send_next()

    def _do_send_next(self):
        self.svc.req.data = self.event_queue[0]
        #self.client.info("send next event from queue!\n")
        self.svc._check_request()
        request = ln.service_fill_only(self.svc)
        if self.client.supports_request_id:
            # prepend request valid header!
            hdr = struct.pack("=HII", ln.LN_LIBRARY_VERSION, ln.LN_MANAGER_CLIENT_ID, self.client.manager.next_request_id)
            self.client.manager.next_request_id += 1
            request = hdr + request
        self.client.request(
            "call_service",
            name=self.handler,
            request=request,
            __binary_fields=["request"],
            cb=self._async_cb)
        return False
                             
    def _async_cb(self, *args):
        #print "async cb: %r" % (args, )        
        del self.event_queue[0]
        if len(self.event_queue):
            #self.client.info("event async_cb, send next!\n")
            self._do_send_next()
        #else:
        #    self.client.info("event async_cb, nothing to send next!\n")

class topic_subscription(pyutils.hooked_object):
    @ln.util.args_to_self
    def __init__(self, client, topic, buffers, reliable_transport, rate, port=None, subscription_id=None):
        pyutils.hooked_object.__init__(self)
        # link to this topic as subscriber!
        client._append_topic_subscription(self)

    def set_port(self, port, connect=True, connect_cb=None):
        self.client.debug("%r set port %r" % (self, port))
        if self.port is not None:
            raise Exception("already have port %r!" % self.port)
        self.port = port
        self.port.add_subscription(self)
        
        if not connect:
            return
        
        if self.port == self.topic.source_port:
            return connect_cb() # no connection needed
        self.client.connect_topic(self, connect_cb)
        self.call_hook("have_port")

    def remove(self):
        self.client._remove_topic_subscription(self)
        if self.port:
            self.port.del_subscription(self)
            self.port = None

    def __repr__(self):
        return "<topic_subscription client %r, topic %r, buffers %r, reliable_transport %r, rate %r, port %r>" % (
            self.client.program_name, self.topic.name, self.buffers, self.reliable_transport, self.rate, self.port)

def _client_req(fcn, self, *args, **kwargs):
    method = fcn.__name__ .split("_", 1)[1]
    try:
        ret = fcn(self, *args, **kwargs)
    except Exception:
        self.error("error executing client request %s(%s):\n%s" % (
            method, str(kwargs)[1:-1], traceback.format_exc()))
        exc_type, exc_value, exc_tb = sys.exc_info()
        ret = "error " + "".join(traceback.format_exception_only(exc_type, exc_value)) + "".join(traceback.format_tb(exc_tb)[1:])
    if ret is None:
        return
    if ret is True:
        ret = "success"
    if type(ret) == str:
        ret = dict(success=ret)
    if type(ret) == dict:
        if "success" not in ret:
            ret["success"] = "success"
    ret["answer_to"] = method
    self.com.send(**ret)
    
def client_req(fcn):
    def call_client_req(self, *args, **kwargs):
        _client_req(fcn, self, *args, **kwargs)
    return call_client_req

def client_req_with_daemon(fcn):
    def with_daemon_decorator(self, *args, **kwargs):
        def _call_with_daemon(daemon):
            message = daemon.when_register_finished.fail_message
            if not daemon.is_connected():
                self.error("needed daemon connection to %s for %r failed:\n%s!" % (self.host.name, fcn.__name__, message))
                method = fcn.__name__ .split("_", 1)[1]
                return self.com.send(
                    answer_to=method,
                    success="error can't connect daemon on %s: %s" % (self.host.name, message)
                )
            _client_req(fcn, self, daemon, *args, **kwargs)

        self.manager.with_daemon_for_host(self.host, "when_register_finished", _call_with_daemon)
    return with_daemon_decorator
    
class ClientStore(tracked_object):
    last_client_id = 0
    update_via_unpickle = False
    # pickling:
    to_pickle = "client_id,supports_service_logging,supports_request_id,time_registered,ln_debug,last_activity,max_log_len,_log,last_ping,address,_last_known_daemon,program_name,pid,process,library_version,svn_revision,is_modified,subscribed,subscription_ids,published,provided_services,host,host_route"
    def __getstate__(self):
        state = {}
        for attr in self.to_pickle.split(","):
            attr = attr.strip()
            state[attr] = getattr(self, attr)
        return state
    def __setstate__(self, state):
        self.com = None
        self.manager = None
        self.__dict__.update(state)
        if self.update_via_unpickle:
            self.update_instance(state)
        self._init()
        
    def _init(self):
        enable_logging_on_instance(self, "ClientStore")
        self.wait_for_port_tid_answer = False
        self.wait_for_sync_answer = False

    def __eq__(self, other):
        if other is None or not isinstance(other, ClientStore):
            return False
        return self.address == other.address

    # does not help:
    #def __hash__(self):
    #    if not self.__dict__:
    #        return id(self)
    #    return hash(self.address)
    def __hash__(self):
        return id(self)

    def get_instance_id(self):
        return self.address

    def __init__(self, manager, fd, address_route, trust_address_route, magic):
        """
        host_route might be None, which signals that we don't know yet for sure on which host this client is running
        """
        self._init()
        self.manager = manager

        fd.setblocking(1)
        # enable socket fd's keepalive!
        fd.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
        fd.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, 5)
        fd.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, 5)

        self.address = address_route[0][0] # caddr
        self.address_route = address_route
        self.trust_address_route = trust_address_route
        
        # host not yet known, need to wait until client registers itself
        self.host_route = None
        self.host = None
        self._last_known_daemon = None
        self.when_host_known = Condition("when_host_known", debug_enabled=True)
        
        # if you need a host, ask manager to get one for you and do something with it via callback
        # (this might need the manager to connect/start and ask a lot of daemons)
                    
        self.com = CommunicationModule(fd, self, self.got_request)
        # feed magic into com-module
        self.com.line_assembler.write(magic)

        # manager internal state
        self.request_handler_id = None
        self.queued_requests = []
        self.time_registered = time.time()
                
        self.socket = fd # Connection associated with the client (used as configuration chanel for example)
        tracked_object.__init__(self)
        

        # remote client information
        self.known_service_loggers = {}
        self.warned_no_logging = set()
        self.program_name = "<unknown>" # name provided by the client program (ony for display)
        self.pid  = None # remote pid of the client process
        self.process = None
        self.library_version = None
        self.svn_revision = None
        self.is_modified = None
        self.pointer_size = 4
        self.argspecs = {}
        self.client_id = None
        self.supports_service_logging = False
        self.supports_request_id = False
        
        self.subscribed = {} # topic_name -> [topic_subscription]
        self.subscription_ids = {} # sub_id -> topic_subscription
        self.next_subscription_id = 1
        self.published = {}
        
        self.provided_services = {}
        self.requested_services = set()
        self.log_services = {}
        
        self.to_republish = []
        self.to_resubscribe = []
        self._channel_blocked = None
        self._unblock_send_queue = []
        self.registered_callbacks = {}
        
        self.last_request_id = 0

        self.last_event_connection_id = 0
        self.event_connections = {}
        self.event_listeners = {}

        self.last_ping = time.time()
        self._log = []
        self.max_log_len = 1000
        self.last_activity = time.time()

        self.ln_debug = False

        self._log_tokens = {} # daemon -> [ token ]

    def __deinit__(self):        
        # unpublish all provided services
        for name in list(self.provided_services.keys()):
            self._unregister_service(name)
        
        # unsubscribe all topics of this client!
        for sub in list(self.subscription_ids.values()):
            self.unsubscribe(sub.topic.name, subscription_id=sub.subscription_id)
        
        # unpublish all topics of this client
        for topic_name in list(self.published.keys()):
            self.unpublish(topic_name)
        
        if self.process is not None:
            try:
                self.process.clients.remove(self)
            except Exception:
                pass

        def _ignore(daemon, exception=None):
            if exception is not None:
                self.error("error removing log tokens from daemon %s:\n%s" % (daemon.host, exception))
        for daemon, tokens in self._log_tokens.items():
            daemon.remove_log_tokens(callback(_ignore, daemon), tokens)
                
        if hasattr(self, "expect_pings_id") and self.expect_pings_id:
            GLib.source_remove(self.expect_pings_id)
            self.expect_pings_id = None
        
        tracked_object.__deinit__(self)
        
        self.manager.remove_hooks_to(self)
        self.manager.remove_client(self)
        self.com = None

    def __str__(self):
        k = []
        for key in "program_name, pid, host".split(", "):
            if hasattr(self, key):
                k.append("%s: %r" % (key, getattr(self, key)))
        return "<client at %s>" % (", ".join(k))
    
    def __repr__(self):
        return str(self)

    def short_id(self):
        if self.host is not None:
            return "client/%s/%s" % (self.host, self.program_name)
        return "client/%s/%s" % (self.address, self.program_name)

    def on_check_ping(self):
        ago = time.time() - self.last_ping
        if ago > self.ping_timeout:
            self.error("expecting pings from this client every %.0f seconds. last ping is %.0f seconds ago! disconnect!" % (
                self.ping_interval, ago))
            self.disconnect()
            return False
        return True

    def disconnect(self):
        # called from user to disconnect this client
        return self.com.disconnect(inform_owner=True)

    def inform_dead(self, why=""):
        self.info("client disconnected!")
        self.__deinit__()        

    def got_request(self, request):
        if "answer_to" in request:
            # answer from client
            method = request["answer_to"]
            if self.library_version >= 3:
                request_id = request["request_id"]
                method = method, request_id
            if method in self.registered_callbacks:
                cb = self.registered_callbacks[method]
                del self.registered_callbacks[method]
                cb(request)
                return
            self.error("unknown answer %r from client:\n%s" % (method, pprint.pformat(request)))
            return
        if "method" not in request:
            self.error("unknown request from client:\n%s" % pprint.pformat(request))
            return
        # queue processing of this request
        self.queue_request(request)

    def queue_request(self, request):
        self.queued_requests.append(request)
        if self.request_handler_id is None:
            self.request_handler_id = GLib.idle_add(self.request_worker)
            
    def request_worker(self):
        if not self.queued_requests or self.com is None:
            if self.request_handler_id is not None:
                GLib.source_remove(self.request_handler_id)
                self.request_handler_id = None
            return False
        
        request = self.queued_requests.pop(0)
        try:
            self.process_request(request)
        except Exception:
            self.error("error processing request\n%s\n%s" % (pprint.pformat(request), traceback.format_exc()))
            
        return True

    def process_request(self, request):
        method = request["method"]
        function_name = "client_%s" % method
        function = getattr(self, function_name, None)

        if function is None:
            self.error("received unknown method %r" % method)
            function = None
        elif function.__name__ not in ("call_client_req", "with_daemon_decorator"):
            self.error("client request method %r is not using a valid decorator (%s)!" % (method, function.__name__))
            function = None
            
        if function is None:
            ret = dict(
                answer_to=method,
                success="error unknown method %r" % method
            )
            self.com.send(**ret)
            return
        
        request_args = dict(request)
        del request_args["method"]
        if function_name not in self.argspecs:
            args = self.argspecs[function_name] = inspect_getfullargspec(function)
        else:
            args = self.argspecs[function_name]
        if args.varkw != "kwargs":
            # no kwargs -> need to do exact match of args!
            have = list(request_args.keys())
            for key in have:
                if key in args.args:
                    continue
                self.warning("client provided request field %r (value %r) is unknown to manager - ignoring..." % (
                    key, request_args[key]))
                del request_args[key]

        function(**request_args)
            
    def log(self, msg, method=None):
        ## add to this clients log
        ts = time.time()
        self.last_activity = ts
        self._log.append((ts, msg))
        while len(self._log) > self.max_log_len:
            del self._log[0]
        self.manager._notify_gui(request="client_log", client=self, ts=ts, msg=msg)
        if method is None:
            method = self.debug
        method(msg)

    def get_endianess(self, allow_unknown=False):
        return self.endianess

    def need_daemon_log(self, level, msg):
        if self._last_known_daemon and self._last_known_daemon.when_register_finished.get_reached_or_fail() is True:
            # already connected to daemon...
            return
        level(msg)
        
    ## client requests ##
    @client_req
    def client_register_client(
            self, host, program_name, pid,
            output_ports=None, input_ports=None, input_port_infos=None, services=None, library_version=0,
            svn_revision=0, is_modified=0, pointer_size=4, have_unix_domain_sockets=0, uid=0, gid=0,
            endianess="unknown", have_glibc_atleast_2_25=None, have_sys_futex=None, have_win32=None,
            manager_host=None, manager_port=None
    ):
        self.to_republish = []
        self.to_resubscribe = []
        self.program_name = self.manager.get_unique_client_name(program_name)
        self.pid = pid
        self.library_version = library_version
        self.svn_revision = svn_revision
        self.is_modified = is_modified
        self.pointer_size = pointer_size
        self.have_unix_domain_sockets = have_unix_domain_sockets == 1
        self.uid = uid
        self.gid = gid
        self.endianess = endianess
        self.have_glibc_atleast_2_25 = have_glibc_atleast_2_25
        self.have_sys_futex = have_sys_futex
        self.have_win32 = have_win32

        self.max_shm_version = get_max_shm_version_from_lib_version(self.library_version)
            
        self.supports_service_logging = self.library_version >= 13
        self.supports_request_id = self.library_version >= 13

        self.__class__.last_client_id += 1
        self.client_id = self.__class__.last_client_id
        
        enable_logging_on_instance(self, "%s/%s" % (self.program_name, self.pid))
        enable_logging_on_instance(self.com, self._log_subsystem)
        self.log("register_client: id: %d, program_name %r, pid %r, library_version %r, pointer_size %d byte" % (
            self.client_id, self.program_name, self.pid, self.library_version, self.pointer_size))

        if self.library_version < 4:
            only_need_coverage_for("clients_older_than_4")
            self.error("error, client is too old -- please update your ln-library and recompile this client! need atleast library_version 4!")
            raise Exception("client is too old, please recompile!")

        if self.library_version < ln.LN_LIBRARY_VERSION:
            only_need_coverage_for("clients_older_than_lnm")
            self.warning("warning, client has older LN-library version than manager! (client: %s, manager: %s)" % (
                self.library_version, ln.LN_LIBRARY_VERSION))
        elif self.library_version > ln.LN_LIBRARY_VERSION:
            only_need_coverage_for("clients_newer_than_lnm")
            self.warning("warning, client has newer LN-library version than manager! (client: %s, manager: %s)" % (
                self.library_version, ln.LN_LIBRARY_VERSION))

        self.input_port_infos = input_port_infos
        self.when_host_known.call(callback(self._register_client_bh, services, output_ports, input_ports))
        
        # now find out from which host this client is coming?
        # (try to avoid contacting daemons, by using address_route & manager_host/_port info)
        if manager_port is None:
            self.trust_address_route = False
        elif self.trust_address_route:
            # check that reported manager_port matches end of address route
            mgr_addr = manager_host, manager_port
            self.trust_address_route = mgr_addr == self.address_route[-1][1] # has to match last entries in-addr!
            if not self.trust_address_route:
                self.warning("client reported manager address as %s, but until now trusted address route looks like this:\n%s" % (
                    mgr_addr,
                    pprint.pformat(self.address_route)))
        
        if self.trust_address_route:
            self.host_route = self.manager._get_host_route(self.address_route)
            self.debug("client has host route: %r" % self.host_route)
            self.host = self.host_route[-1][0]
            self.info("client connection coming from %s" % self.host)
            self._last_known_daemon = self.manager.get_daemon(self.host) # do not use this for direct method calls!
            self.when_host_known.trigger()
            return
        
        self.debug("address route for this client is not trustworthy. need to establish connection to related daemons...")
        client_ip, client_port = self.address
        host = self.manager.sysconf.get_host(client_ip, create=True, add_to_default_network=True, reachable_from=self.manager.host)
        self.host_route = [ (host, self.address) ]
        
        def _check_next_host_step(i):
            host, (client_ip, client_port) = self.host_route[i]

            def _on_is_forward_answer(forwarded_from=None, exception=None):
                if exception is not None:
                    return self.when_host_known.trigger_fail(fail_message="could not connad daemon on %s: %s" % (host, exception))
                if forwarded_from is False:
                    # client has to come from that host!
                    self.debug("client has host route: %r" % self.host_route)
                    self.host = self.host_route[-1][0]
                    self.info("client connection coming from %s" % self.host)
                    self._last_known_daemon = self.manager.get_daemon(self.host)
                    return self.when_host_known.trigger()
                # its a forwarded connection!
                self.debug("daemon on host %s reported that this is a forwarded connection coming from %s!" % (host, forwarded_from))
                nhost = self.manager.sysconf.get_host(forwarded_from[0], create=True, add_to_default_network=True, reachable_from=host)
                self.host_route.append((nhost, forwarded_from))
                return _check_next_host_step(i=i + 1)
            self.manager.get_daemon(host).is_tcp_forward_source_port(_on_is_forward_answer, client_port)
        _check_next_host_step(i=0)
        
    def _register_client_bh(self, services, output_ports, input_ports):
        """
        self.host is now valid!
        """        
        if self.when_host_known.get_reached_or_fail() is not True:
            self.error("failed to find actual host of this client! (%s)" % self.when_host_known.fail_message)
            self.com.send(
                answer_to="register_client",
                success="error could not detect from which host you are connecting!",
            )
            return
        
        if self.have_win32 is None:
            # client does not report whether its a windows client.
            # need to connect daemon to find out!
            self.need_daemon_log(self.warning,
                "client is too old and does not report whether its coming from win32. need to connect daemon!")

            def fix_have_win32(daemon):
                if not daemon.is_connected():
                    self.error("can not decide 'have_win32' because daemon connection failed!")
                    self.have_win32 = False
                else:
                    self.have_win32 = "win" in daemon.architecture
                self._check_expect_pings()

            self.manager.with_daemon_for_host(self.host, "when_register_finished", fix_have_win32)
        else:
            self._check_expect_pings()

        if self.endianess == "unknown":
            self.need_daemon_log(
                self.warning,
                "client is too old and does not report endianess, need to contact daemon to find out!")
            def fix_endianess(daemon):
                if not daemon.is_connected():
                    self.error("can not fixup client endianess because daemon connection failed!")
                    self.endianess = "little"
                else:
                    self.endianess = daemon.endianess
                    self.log("fixed client endianess to be %r" % self.endianess)
            self.manager.with_daemon_for_host(self.host, "when_register_finished", fix_endianess)
            
        self.manager.update_client(self)
        daemon = self.manager.get_daemon(self.host)
        if daemon.when_register_finished.get_reached_or_fail() is True:
            # call now
            self._on_have_daemon()
        daemon.add_hook("when_register_finished", lambda daemon: self._on_have_daemon(), from_who=self.program_name)
            
        if services:
            only_need_coverage_for("clients_reconnect_with_existing_services")
            # restore services!
            for data in services:
                service_name_patterns_to_log = ""
                if self.supports_service_logging:
                    name, interface, signature, port, unix_socket, service_name_patterns_to_log = data[:6]
                elif len(data) == 5:
                    name, interface, signature, port, unix_socket = data[:5]
                elif len(data) == 4:
                    name, interface, signature, port = data
                    unix_socket = None
                else:
                    raise Exception("client reported service state of unknown format:\n%r" % (data, ))
                try:
                    self.client_register_service(name, interface, signature, port, unix_socket=unix_socket)
                except Exception:
                    self.error("error re-registering service:\n%s" % traceback.format_exc())
                if service_name_patterns_to_log:
                    self.client_register_as_service_logger(name, service_name_patterns_to_log)

        def _client_register_finished(exception=None):
            if exception is not None:
                self.error("failed to finish client registration: %s" % exception)
                self.com.send(
                    answer_to="register_client",
                    success="error %s" % exception
                )
                return
            self.com.send(
                answer_to="register_client",
                success="success",
                program_name=self.program_name,
                manager_library_version=ln.LN_LIBRARY_VERSION,
                client_id=self.client_id
            )
                           
        if output_ports or input_ports:
            self.need_daemon_log(self.debug, "client already has ports. need to connect daemon!")
            self.manager.with_daemon_for_host(
                self.host, "when_register_finished",
                callback(
                    self._link_ports,
                    output_ports=output_ports,
                    input_ports=input_ports,
                    _client_register_finished=_client_register_finished
                )
            )
        else:
            _client_register_finished()

    def _link_ports(self, daemon, output_ports, input_ports, _client_register_finished):
        if not daemon.is_connected():
            return _client_register_finished(exception="failed to connect to daemon on %s: %s" % (daemon.host, daemon.when_register_finished.fail_message))

        def _after_input_ports(exception=None):
            if exception is not None: return _client_register_finished(exception=exception)
            if self.to_republish or self.to_resubscribe:
                self.process_republished_idle = GLib.idle_add(self.process_republishes)
            _client_register_finished()
        
        def _after_output_ports(exception=None):
            if exception is not None: return _client_register_finished(exception=exception)
            self._link_input_ports(daemon, input_ports, _after_input_ports)
        
        self._link_output_ports(daemon, output_ports, _after_output_ports)
        
    def _link_output_ports(self, daemon, output_ports, cb):
        only_need_coverage_for("clients_reconnect_with_existing_outports")
        # restore/find/request output_ports!
        
        def _process_output_ports():
            if not output_ports:
                return cb()
            
            topic, md_name, message_size, shm_name = output_ports.pop(0)
            # redo changes from publish!
            t = self.manager.topics.get(topic)
            if t is None:
                t = Topic(self.manager, topic, md_name, self.manager.sysconf.topic_settings.get(topic))
                self.manager.add_topic(t)
            else:
                # check wether message definitions match! TODO: need hash from client later!
                msg = None
                if t.md_name != md_name:
                    msg = "you requested message_definition %r but topic %r uses %r!" % (md_name, t.name, t.md_name)
                # topic already exist. does it already have an publisher?
                elif t.publisher is not None and t.publisher != self:
                    msg = "topic %r already published by %s!" % (topic, t.publisher)
                if msg:
                    self.error(msg)
                    return cb(exception=msg)
            
            self.published[topic] = t
            
            # try to find port on daemon for this topic!
            def _with_port_as_source(port=None, exception=None):
                if exception is not None: return cb(exception=exception)
                if port.size != message_size:
                    self.error("client %s reports a used output-port %s which has a different message size than reported by the daemon! -- please restart client!" % (self, shm_name))
                    msg = "port %s has a different size in daemon. please restart this client!" % shm_name
                    self.error(msg)
                    return cb(exception=msg)
                t.source_port = port
                t.set_publisher(self)
                _process_output_ports() # next port
                
            for port in daemon.ports:
                if port.port_type != "shm":
                    continue
                if port.direction != "source":
                    continue
                if port.name != shm_name:
                    continue
                # match!
                self.debug("found daemon-port %r for outport topic %r" % (port, topic))
                port.topic = t
                port.publisher_topic = t
                port.set_publisher()
                daemon.shms[t.name, -1] = port
                return _with_port_as_source(port)
            #self.error("client %s reports a used output-port %s which is not listed by the corresponding daemon! -- please restart client!" % (self, shm_name))
            #raise Exception("port %s does not exist in daemon. please restart this client!" % shm_name)
            #self.info("client %s reports a used output-port %s which is not listed by the corresponding daemon! -- please restart client!" % (self, shm_name))
            # search in direct sources:
            def _with_port(port=None, exception=None):
                if exception is not None: return _with_port_as_source(exception=exception)
                daemon.add_source(port, port, -1, True) # direct source!
                _with_port_as_source(port)

            for porta, srcs in daemon.sources.items():
                if porta.port_type == "shm" and porta.name == shm_name:
                    return _with_port(porta)
            # create client port!
            # "register_client"/output_ports does not report number of buffers used here!
            # -> does shm_name follow a known shm name-scheme?
            m = re.match("%s_([0-9]+)_[0-9-pful]+_[0-9a-f]+_.*" % self.pid, shm_name)
            if m:
                buffers = int(m.group(1)) # yes, get number of buffers from there!
            else:
                buffers = 1
            self.info("could not find daemon-port for outport shm %r topic %r -- creating new one..." % (shm_name, topic))
            t = self.manager.get_topic(topic, md_name)
            def _with_new_port(port=None, exception=None):
                if exception is not None: return _with_port(exception=exception)
                # notify client to change this port!!
                self.to_republish.append((t, port))
                _with_port(port)
            return daemon.new_shm_port(_with_new_port, self, t, -1, buffers, self.max_shm_version, is_reliable=True)
        _process_output_ports()
    
    def _link_input_ports(self, daemon, input_ports, cb):
        only_need_coverage_for("clients_reconnect_with_existing_inports")
        # restore/find/request input_ports!
        
        input_iter = iter(enumerate(input_ports))
        
        def _process_input_ports():
            try:
                port_id, (topic_name, md_name, message_size, shm_name, rate) = next(input_iter)
            except StopIteration:
                return cb()
            
            if self.input_port_infos is not None:
                subscription_id, reliable_transport, buffers = self.input_port_infos[port_id][:3]
            else:
                subscription_id = None
                reliable_transport = 0
                buffers = 1

            # redo changes from subscribe!
            topic = self.manager.get_topic(topic_name, md_name)
            sub = topic_subscription(self, topic, buffers, reliable_transport, rate)

            if not shm_name:
                return _process_input_ports()
            
            ####
            # assume daemon already has this port!
            for port in daemon.ports:
                if port.port_type != "shm":
                    continue
                if port.name != shm_name:
                    continue
                if port.size != message_size:
                    msg = "client reports a used input-port %r which has a different message size than reported by the daemon! -- please restart client!" % shm_name
                    self.error(msg)
                    return cb(exception=msg)                
                # match!
                self.debug("found daemon-port %r for topic %r" % (port, topic.name))
                sub.buffers = port.buffers
                sub.set_port(port, connect=False)
                daemon.shms[topic.name, rate] = port
                if port.topic is None:
                    port.topic = topic
                    self.debug("assigned port %s to topic %s" % (port, topic))
                return _process_input_ports()
            
            msg = "client reports a used input-port %r which is not listed by the corresponding daemon! -- please restart client!" % shm_name
            self.error(msg)
            return cb(exception=msg)
                
        _process_input_ports()
    
    def _on_have_daemon(self):
        """
        called as soon as we have a fully registered daemon connection to this clients host
        """
        daemon = self.manager.get_daemon(self.host)
        self._last_known_daemon = daemon

        # try to link process
        self.process = daemon._started_processes.get(self.pid)
        if self.process is None:
            self.manager.update_client(self)
            return True
        
        self.process.clients.append(self)
        self.ln_debug = self.process.ln_debug
        
        if self.max_shm_version >= 3 and "no_shmv3" in self.process.flags:
            self.debug("process has no_shmv3 flag, client would do %s, limiting to v2" % self.max_shm_version)
            self.max_shm_version = 2

        self.manager.update_client(self)
        return True
    
    def _check_expect_pings(self):
        self.expect_pings = self.have_win32 and self.library_version >= 6
        
        if not self.expect_pings:
            return
        
        only_need_coverage_for("clients_from_win32")
        self.ping_interval = 30
        self.debug("expecting pings from this client in %.0fs interval" % self.ping_interval)
        self.ping_timeout = self.ping_interval + 5
        self.expect_pings_id = GLib.timeout_add(int(self.ping_timeout * 1000), self.on_check_ping)
        
    def add_to_resubscribe(self, sub):
        self.to_resubscribe.append(sub)
        if not hasattr(self, "process_republished_idle"):
            self.process_republished_idle = GLib.idle_add(self.process_republishes)

    def do_delayed_init_inport(self, sub):
        def got_answer(msg, sub):
            self.debug("got answer: %r" % msg)
            # trigger reconnection
            sub.topic.set_publisher(sub.topic.publisher)
            if self.to_republish or self.to_resubscribe:
                GLib.idle_add(self.process_republishes)
        self.request(
            "delayed_init_inport",
            cb=callback(got_answer, sub),
            topic=sub.topic.name,
            shm_name=sub.port.name
        )

    def process_republishes(self):
        if hasattr(self, "process_republished_idle"):
            del self.process_republished_idle
        if self.to_republish:
            t, port = self.to_republish.pop()
            self.do_republish(t, port)
        if self.to_resubscribe:
            sub = self.to_resubscribe.pop()
            self.do_delayed_init_inport(sub)
        return False

    def do_republish(self, topic, new_port, user_cb=None):
        self.log("republish: topic %r, new_port %r" % (topic, new_port))
        def _got_answer(msg):
            self.debug("got answer: %r" % msg)
            topic.source_port = new_port
            topic.set_publisher(self)
            new_port.set_publisher()
            if self.to_republish or self.to_resubscribe:
                GLib.idle_add(self.process_republishes)
            if user_cb:
                user_cb(topic, new_port)
        self.send_request(
            "republish_outport",
            cb=_got_answer,
            topic=topic.name,
            shm_name=new_port.name
        )

    @client_req_with_daemon
    def client_publish(self, daemon, topic, message_definition_name, buffers):
        topic_name = topic
        
        self.log("publish %r, msg_def %r" % (topic_name, message_definition_name))
        
        # is this topic name already used?
        topic = self.manager.topics.get(topic_name)
        if topic is None:
            topic = Topic(self.manager, topic_name, message_definition_name, self.manager.sysconf.topic_settings.get(topic_name))
            self.manager.add_topic(topic)
        else:
            only_need_coverage_for("publish_known_topic")
            # check wether message definitions match! TODO: need hash from client later!
            if topic.md_name != message_definition_name:                
                subs = ", ".join([client.program_name for client in topic.get_subscribed_clients()])
                if topic.publisher:
                    pub = topic.publisher.program_name
                else:
                    pub = "<None>"
                raise Exception((
                    "you requested to publish topic %r\n"
                    "with message_definition %r\n"
                    "but this topic already exists and\n"
                    "uses message_definition %r!\n"
                    "subscribers: [%s]\n"
                    "publisher: [%s]\n") % (
                        topic.name, message_definition_name, 
                        topic.md_name,
                        subs, pub))
            # topic already exist. does it already have an publisher?
            if topic.publisher is not None:
                msg = "topic %r already published by %s!" % (topic_name, topic.publisher)
                self.error(msg)
                self.com.send(
                    answer_to="publish",
                    success="error %s" % msg
                )
                return

        # link this topic as publisher!
        self.published[topic_name] = topic

        max_shm_version = self.max_shm_version

        # are there already subscribers on this host? do they want a larger buffer? or a smaller shm version?
        for sub in topic.subscribers:
            only_need_coverage_for("publish_subscribed_topic")
            if sub.client.host != self.host:
                continue
            if sub.rate != -1:
                continue
            if sub.buffers > buffers:
                buffers = sub.buffers
                self.debug("there is already a subscriber with more buffers, enlarge to %s\n%s" % (buffers, sub.client))
            if sub.client.max_shm_version < max_shm_version:
                self.debug("there is already a subscriber %r which lowers max_shm_version to %d" % (sub.client.program_name, sub.client.max_shm_version))
                max_shm_version = sub.client.max_shm_version

        def _finish(port=None, exception=None):
            self.unblock_channel("publish_in_progress")
            if exception is not None:
                only_need_coverage_for("publish_new_shm_port_error")
                self.error("publish %r: %s" % (topic.name, exception))
                del self.published[topic.name]
                self.com.send(
                    answer_to="publish",
                    success="error %s" % exception
                )
                return
            topic.source_port = port
            topic.source_port.set_publisher()
            topic.set_publisher(self)
            self.com.send(
                answer_to="publish", 
                success="success", 
                message_size=port.size,
                msg_def_hash=topic.md.get_hash(),
                shm_name=port.name
            )
        self.block_channel("publish_in_progress")
        daemon.new_shm_port(_finish, self, topic, -1, buffers, max_shm_version, is_reliable=True)

    def is_blocked(self):
        return self._channel_blocked is not None
    def block_channel(self, hint):
        if self._channel_blocked:
            raise Exception("channel already blocked by %r. can not be blocked by %r" % (self._channel_blocked, hint))
        self._channel_blocked = hint
    def unblock_channel(self, hint):
        if self._channel_blocked != hint:
            raise Exception("error, wanted to unblock channel with %r but channel is locked with %r" % (hint, self._channel_blocked))
        self._channel_blocked = None
        for entry in self._unblock_send_queue:
            self.request(**entry)
        self._unblock_send_queue = []

    def queue_send_on_unblock(self, **kwargs):
        if self._channel_blocked is None:
            raise Exception("error, wanted to queue send on unblock. but channel is not blocked!")
        self._unblock_send_queue.append(kwargs)

    def get_publisher_port(self, topic):
        return topic.source_port # awkward

    def unsubscribe(self, topic_name, rate=None, shm_name=None, subscription_id=None):
        self.debug("unsubscribe topic %r, rate %r, shm_name %r, subscription_id %r" % (topic_name, rate, shm_name, subscription_id))

        if topic_name not in self.subscribed:
            raise Exception("topic %r is not subscribed!" % topic_name)
        
        if subscription_id is None:
            candidates = self.subscribed[topic_name]
            sub = None
            while True:
                if rate is None:
                    if len(candidates) > 1:
                        self.error(("this client uses an old ln-library version that does not specify which topic rate to unsubscibe!\n"
                                    "but this client has these rates subscribed: %s\n"
                                    "will return success, without really unsubscribing! update your client!") % (
                                        ", ".join([str(sub.rate) for sub in candidates])))
                        return
                    if len(candidates) == 1:
                        sub = candidates[0]
                        rate = sub.rate
                        break

                candidates = [sub for sub in candidates if sub.rate == rate]
                if not candidates:
                    raise Exception("topic %r at rate %r is not subscribed!" % (topic_name, rate))

                all_unconnected = all([sub.port is None for sub in candidates])
                if len(candidates) > 1 and shm_name is None and not all_unconnected:
                    self.error(("this client uses an old ln-library version that does not specify which topic shm to unsubscibe!\n"
                                "but this client has these shms subscribed: %s\n"
                                "will return success, without really unsubscribing! update your client!") % (
                                    ", ".join([str(sub.port) for sub in candidates])))
                    return
                if shm_name is None and all_unconnected:
                    shm_name = ""

                candidates = [sub for sub in candidates if sub.port is not None and sub.port.name == shm_name]
                if not candidates:
                    raise Exception("topic %r at rate %r with shm %r is not subscribed!" % (topic_name, rate, shm_name))

                sub = candidates.pop(0)
                break
        else:
            sub = self.subscription_ids[subscription_id]

        port = sub.port
        sub.remove()

        if port and not port.have_subscriptions() and sub.topic.source_port != port:
            self.debug("removing port because its not the topic's source_port and its not used by other clients!")
            port.daemon.remove_port(port, as_subscriber=True)

        if not sub.topic.subscribers and not sub.topic.publisher and sub.topic.name in self.manager.topics:
            self.debug("this topic has no other subscribers and no publisher -> remove!")
            self.manager.del_topic(sub.topic.name)

    @client_req
    def client_unsubscribe(self, topic_name, rate=None, shm_name=None, subscription_id=None):
        self.log("unsubscribe topic %r" % topic_name)
        self.unsubscribe(topic_name, subscription_id=subscription_id)
        self.com.send(
            answer_to="unsubscribe", 
            success="success")

    def unpublish(self, topic_name):
        if topic_name not in self.published:
            raise Exception("topic %r is not published!" % topic_name)
        topic = self.published[topic_name]
        del self.published[topic_name]
        topic.set_publisher(None)

    @client_req
    def client_unpublish(self, topic_name):
        self.log("unpublish topic %r" % topic_name)
        self.com.send(
            answer_to="unpublish",
            success="success")
        self.unpublish(topic_name)

    @client_req
    def client_get_topic_publisher_service_name(self, topic):
        self.log("get_topic_publisher_service_name for %r" % topic)
        t = self.manager.topics.get(topic)
        if t is None or t.publisher is None:
            return dict(success="error topic not yet published")
        if t.publisher.get_endianess() != self.get_endianess():
            endianess_swap = t.get_swap_endianess_description()
        else:
            endianess_swap = ""
        return dict(
            success="success", 
            service_name="%s.request_topic" % t.publisher.program_name,
            endianess_swap=endianess_swap
        )

    def _append_topic_subscription(self, sub):
        if sub.subscription_id is None:
            sub.subscription_id = self.next_subscription_id
            self.next_subscription_id += 1
        self.subscription_ids[sub.subscription_id] = sub
        subscriptions = self.subscribed.get(sub.topic.name, [])
        subscriptions.append(sub)
        self.subscribed[sub.topic.name] = subscriptions
        sub.topic.add_subscriber(sub)

    def _remove_topic_subscription(self, sub):
        subscriptions = self.subscribed.get(sub.topic.name)
        if subscriptions:
            if sub in subscriptions:
                subscriptions.remove(sub)
            if not subscriptions:
                del self.subscribed[sub.topic.name]
        sub.topic.del_subscriber(sub)
        if sub.subscription_id is not None:
            del self.subscription_ids[sub.subscription_id]

    @client_req_with_daemon
    def client_subscribe(self, daemon, topic, message_definition_name, rate=-1, reliable_transport=False, buffers=1):
        topic_name = topic
        reliable_transport = bool(reliable_transport)
        self.log("subscribe %r\nmsg_def %r\nrate %s\nreliable_transport %s\nbuffers: %s" % (
                topic_name, message_definition_name, rate, reliable_transport, buffers))

        if buffers < 1:
            self.log("error: client requested subscribe with n_buffers %r < 1 not possible! assuming 1" % buffers, method=self.error)
            buffers = 1

        topic = self.manager.get_topic(topic_name, message_definition_name)
        sub = topic_subscription(self, topic, buffers, reliable_transport, rate)

        if topic.source_port is None:
            self.info("subscribe: topic %r, no publisher yet" % (topic.name, ))
            ret = dict(
                answer_to="subscribe", 
                success="success", 
                message_size=topic.message_size,
                msg_def_hash=topic.md.get_hash(),
                shm_name="", # no shm yet, will be completed when first publisher arrives in async thread!
                has_publisher=0
            )
            if self.library_version >= 10:
                ret["subscription_id"] = sub.subscription_id
            multisubscription = len(self.subscribed[sub.topic.name]) > 1
            if self.library_version < 24 and multisubscription:
                self.log("error: old client subscribed topic %r more than once while topic is not yet published. only first subscription will receive packets! please update client's library version!" % topic_name, method=self.error)
            return ret
        self._create_subscriber_port(sub)

    def _create_subscriber_port(self, sub, delayed=False):
        if not delayed:
            self.block_channel("subscribe_in_progress")
        sub.port = "in progress"
        daemon = self.manager.get_daemon(sub.client.host)
        def _finish(port=None, exception=None):
            if not delayed:
                self.unblock_channel("subscribe_in_progress")
            
            if exception is not None:
                self.error("subscribe %r: %s" % (sub.topic.name, exception))
                sub.remove()
                if not delayed:
                    self.com.send(
                        answer_to="subscribe",
                        success="error %s" % exception
                    )
                return
            
            self.info("subscribed %r" % sub.topic.name)
            
            if sub.port == "in progress":
                sub.port = None
            
            def _on_connection(ret=None, exception=None):
                if exception is not None:
                    self.error("could not connect subscriber port: %s" % exception)
                    if not delayed:
                        sub.remove()
                        self.com.send(
                            answer_to="subscribe", 
                            success="error %s" % exception
                        )
                    return
                if sub.topic.publisher is not None:
                    has_publisher = 1
                else:
                    has_publisher = 0
                if not delayed:
                    ret = dict(
                        answer_to="subscribe", 
                        success="success", 
                        message_size=port.size,
                        msg_def_hash=sub.topic.md.get_hash(),
                        shm_name=port.name,
                        has_publisher=has_publisher
                    )
                    if self.library_version >= 10:
                        ret["subscription_id"] = sub.subscription_id
                    self.com.send(**ret)
                else:
                    self.add_to_resubscribe(sub)
            sub.set_port(port, connect=True, connect_cb=_on_connection)
        daemon.new_shm_port(_finish, self, sub.topic, sub.rate, sub.buffers, self.max_shm_version, is_reliable=sub.reliable_transport)

    def connect_topic(self, sub, cb):
        publisher = sub.topic.publisher
        publisher_port = publisher.get_publisher_port(sub.topic)
        if sub.port in (None, publisher_port):
            return cb() # nothing to do!
        if sub.rate in (-2.0, -3.0):
            return cb() # nothing to do!
        if sub.rate == -1.0 and publisher_port and publisher_port.daemon == sub.port.daemon:
            if sub.port.shm_version != publisher_port.shm_version:
                self.warning("publisher and subscriber on same host, but with different shm-versions!\n"
                             "will use ln_daemon thread to translate! (pub: v%s, sub: v%s)" % (
                    publisher_port.shm_version, sub.port.shm_version))
        sub.port.daemon.connect_ports(sub.port, publisher_port, cb, reliable_transport=sub.reliable_transport)
        
    def _inform(self,what):
        self.com.send(
            answer_to="register_client", 
            success="success")

    def _get_message_definition(self, message_definition_name):
        fn = ln.search_message_definition(message_definition_name)
        md = ln.message_definition(message_definition_name, fn)
        md_hdr = ln.message_definition_header()
        md_hdr.add_message_definition(md)
        try:
            flat_md = md_hdr.generate_sfunction_parameter(with_hash=False).split("|", 1)[1]
        except Exception:
            if "not supported by simulink" in str(sys.exc_info()[1]):
                flat_md = "<not supported by simulink>"
            elif "not allowed in topics" in str(sys.exc_info()[1]):
                flat_md = "<not supported by topics>"
            else:
                raise
        return dict(
            name=message_definition_name,
            fn=fn,
            definition=repr(md.dict()),
            flat_definition=flat_md,
            size=md.calculate_size(),
            hash=md.get_hash()
        )

    @client_req
    def client_describe_message_definition(self, md_name):
        desc = self.manager.get_md_info(md_name)
        self.com.send(
            answer_to="describe_message_definition",
            success="success",
            description=desc
        )

    @client_req
    def client_get_message_definition_for_topic(self, topic):
        self.log("get_message_definition_for_topic %r" % topic)
        t = self.manager.topics.get(topic)
        if t is None:
            raise Exception("unknown topic name %r!" % (topic))

        info = self._get_message_definition(t.md_name)

        self.com.send(
            answer_to="get_message_definition_for_topic", 
            success="success", 
            **info)

    @client_req
    def client_get_message_definition(self, md_name):
        self.log("get_message_definition %r" % md_name)
        info = self._get_message_definition(md_name)
        self.com.send(
            answer_to="get_message_definition", 
            success="success", 
            **info)

    @client_req
    def client_put_message_definition(self, md_name, md):
        self.log("put_message_definition %r:\n%s" % (md_name, md))
        real_md_name = os.path.join(self.manager.sysconf.instance_config.gen_message_definition_prefix, md_name)
        md_text = md
        md = ln.message_definition(real_md_name, text=md)
        
        # is this md_name currently in use?
        topics_to_check = set()
        did_change = True
        for topic_name, topic in self.manager.topics.items():
            if topic.md_name != real_md_name:
                continue
            if not hasattr(topic, "md"):
                fn = ln.search_message_definition(real_md_name)
                topic.md = ln.message_definition(md_name, fn)
            if topic.md.fields == md.fields and topic.md.defines == md.defines:
                did_change = False
                self.info("md %r is already known, with same content." % md_name)
            else:
                if topic.publisher is not None or topic.subscribers:
                    raise Exception("md %r is already used by topic %r, with different content:\n%s\nyour content:\n%s\npublisher: %r,\nsubscribers: %r" % (
                        md_name, topic.name,
                        topic.md.fields, md.fields, 
                        topic.publisher, topic.subscribers))
                # topic is not used...
                self.info("md %r is already known, with different content:\n%s\nbut no one uses this topic.\n" % (
                        md_name, topic.md))
                topic.md = md

        if did_change:
            # write to disk!
            # write md to temp md folder
            fn = os.path.join(self.manager.sysconf.instance_config.gen_message_definition_path, md_name)
            dn = os.path.dirname(fn)
            if not os.path.isdir(dn):
                os.makedirs(dn)
            with open(fn, "w") as fp:
                fp.write(md_text)

        self.com.send(
            answer_to="put_message_definition", 
            md_name=real_md_name,
            success="success")

    @staticmethod
    def get_service(manager, name):
        # returns None if service does not exist!
        for client in manager.iterate_clients():
            service = client.provided_services.get(name)
            if service:
                return service

    @client_req
    def client_register_service(self, name, interface, signature, port, unix_socket=None):
        self.log("register_service %r\ninterface %r\nsignature %r\nat port %r, unix_socket %r" % (
            name, interface, signature, port, unix_socket), method=self.debug)
        service = self.get_service(self.manager, name)
        if service:
            raise Exception("service with name %r already provided by client %r" % (name, service.provider))
        
        service = ln_service(name, interface)
        service.set_provider(self, signature, port, unix_socket)
        self.provided_services[name] = service
        self.manager.update_service(self, name)
        self.manager.resource_event("service_new_provider", name, interface, self.program_name)
        
        log_services = self.get_all_log_services(name)
        if not log_services:
            return True
        
        if not self.supports_service_logging:
            self.warning("service %r is enabled for logging from these log-services: %r,\n"
                        "but this service providers library version is too old!\n"
                        "service requests can not be logged!" % (name, log_services))
            return True
        
        for ls in log_services:
            if ls not in self.known_service_loggers:
                self.known_service_loggers[ls] = set()
            self.known_service_loggers[ls].add(name)
        

        return dict(log_services=",".join(log_services))

    def request_tcp_port_for_service(self, service_name, cb):
        """
        send request to client to query listening tcp port for this service,
        client may return existing port number or try to bind to a new one
        """
        return self.request(
            "request_tcp_port_for_service",
            service_name=service_name,
            cb=callback(lambda req: cb(req["tcp_port"]))
        )
    
    def request_unix_socket_for_service(self, service_name, cb):
        """
        send request to client to query listening unix socket for this service,
        client may return existing socket name or try to bind to a new one
        """
        return self.request(
            "request_unix_socket_for_service",
            service_name=service_name,
            cb=callback(lambda req: cb(req["unix_socket"]))
        )

    @client_req
    def client_unregister_service(self, name):
        self._unregister_service(name)
        return True
    
    def _unregister_service(self, name):
        self.log("unregister_service %r" % name, method=self.debug)
        service = self.get_service(self.manager, name)
        if not service:
            return
        del self.provided_services[name]
        self.manager.update_service(self, name)
        self.manager.resource_event("service_del_provider", name, service.interface, self.program_name)
        service._destroy()

    @client_req
    def client_request_service(self, name, interface, signature):
        self.log("request_service %r\ninterface %r\nsignature %r" % (
            name, interface, signature))
        service = self.get_service(self.manager, name)
        if not service:
            self.warning("client requested non-existent service %r" % name)
            return "error - there is no provider registered for service %r!" % name
        if service.interface != interface:
            raise Exception("service %r provided by client %r uses message definition name %r, you requested %r!" % (
                name, service.provider, service.interface, interface))
        if not service.compatible_signature(signature):
            raise Exception("service %r provided by client %r has incompatible signature!\nyou     : %s\nprovider: %s" % (
                    name, service.provider, signature, service.signature))

        ret = dict(
            endianess=service.provider.get_endianess()
        )
        needs_translator_address = False
        if service.provider.supports_request_id:
            # provider needs request header!!!
            if self.supports_request_id:
                ret["send_request_id"] = 1 # all fine, will communicate with version, client_id and request_id
            else:
                # problem case! new provider expecting request header, old client unable to send request header!
                self.error((
                    "this client is too old (version %d) to directly communicate with service %r from %r!\n"
                    "this clients requests will be translated by the manager resulting in very bad performance!\n"
                    "please recompile this client against a newer ln-client-library (atleast version 13)!") % (
                        self.library_version,
                        name,
                        service.provider.program_name))
                needs_translator_address = True
        else:
            # all fine, will communicate without request header
            if self.supports_request_id:
                ret["send_request_id"] = 0
        
        # is provider running on the same machine and do both clients support unix domain sockets?
        use_af_unix = (
            self.have_unix_domain_sockets and service.provider.have_unix_domain_sockets
            and self.host == service.provider.host)

        def client_request_service_bh(resp=None, exception=None):
            if exception is not None:
                msg = "could not provide service port: %s" % exception
                self.error(msg)
                return self.com.send(
                    success="error %s" % msg,
                    answer_to="request_service"
                )
            if resp is not None:
                if isinstance(resp, tuple):
                    ip_address, port = resp
                    if self.host == service.provider.host:
                        # shortcut to loopback ip
                        ip_address = "127.0.0.1"                
                    ret["address"], ret["port"] = ip_address, port
                else:
                    ret["unix_socket"] = resp
            ret["success"] = "success"
            ret["answer_to"] = "request_service"

            log_services = self.get_all_log_services(service.name)
            if log_services:
                if self.supports_service_logging:
                    for ls in log_services:
                        if ls not in self.known_service_loggers:
                            self.known_service_loggers[ls] = set()
                        self.known_service_loggers[ls].add(service.name)
                    ret["log_services"] = ",".join(log_services)
                else:
                    self.warning("service %r is enabled for logging from these log-services: %r,\n"
                                 "but this service clients library version is too old!\n"
                                 "service requests can not be logged!" % (service.name, log_services))
            self.requested_services.add(service.name)
            self.com.send(**ret)
        
        if needs_translator_address:
            def have_service_protocol_filter(translator_address=None, exception=None):
                if exception is not None:
                    msg = "failed to generate translator address for service %s: %s" % (service.name, exception)
                    self.error(msg)
                    return client_request_service_bh(exception=msg)
                self.debug("returning translator address %r" % (translator_address, ))
                ret["address"], ret["port"] = translator_address
                client_request_service_bh()
            self.manager.get_service_protocol_filter(self, service, have_service_protocol_filter)
            return
        
        if use_af_unix:
            self.debug("returning unix domain socket name")
            service.get_unix_socket(client_request_service_bh)
            return
        
        self.debug("returning tcp socket ip/port, hosts match: %s, have_af_unix: %s, provider: %s" % (
            self.host == service.provider.host,
            self.have_unix_domain_sockets,
            service.provider.have_unix_domain_sockets
            ))
        service.get_port_at(self.host, client_request_service_bh)
            

    def _get_service_signature(self, interface):
        fn = ln.search_message_definition(interface)
        md = ln.message_definition(interface, fn)

        hdr = ln.message_definition_header()
        size, signature = hdr.generate_md(None, interface, md, True, return_signature=True)
        if not isinstance(signature, (str, unicode)):
            signature = repr(signature)
        return dict(
            name=interface,
            fn=fn,
            definition=repr(md.dict()),
            signature=signature)

    @client_req
    def client_get_service_signature(self, interface):
        self.log("get_service_signature for %r" % interface)
        info = self._get_service_signature(interface)
        return info

    @client_req
    def client_get_service_signature_for_service(self, name):
        self.log("get_service_signature_for_service for %r" % name)
        service = self.get_service(self.manager, name)
        if service is None:
            raise Exception("no service with name %r found!" % name)
        info = self._get_service_signature(service.interface)
        info["interface"] = service.interface
        return info

    @client_req
    def client_ping(self):
        self.log("ping")
        self.last_ping = time.time()
        return True

    def _needs_provider_cb(self, success, for_topic):
        self.info("needs_provider: %r callback success: %r" % (for_topic, success))
        if for_topic != self._needs_service_or_topic_name:
            self.info("clients request did already timeout for this provider!")
            return
        GLib.source_remove(self._needs_provider_timeout_source)
        self.unblock_channel("need_provider_in_progress")
        self._needs_service_or_topic_name = None
        if self.com is None:
            self.info("this client is gone...")
            return
        if not success:
            self.com.send(answer_to="needs_provider", success="error startup of provider %r for %r failed!" % (self._needs_provider.name, self._needs_service_or_topic_name))
        else:
            self.com.send(answer_to="needs_provider", success="success")
        
    def _needs_provider_timeout(self):
        GLib.source_remove(self._needs_provider_timeout_source)
        self.info("needs_provider: %r timeout waiting!" % self._needs_service_or_topic_name)
        self.unblock_channel("need_provider_in_progress")
        self._needs_service_or_topic_name = None
        if self.com is None:
            self.info("this client is gone...")
            return
        self.com.send(answer_to="needs_provider", success="error startup of provider %r for %r timed out!" % (self._needs_provider.name, self._needs_service_or_topic_name))
        return False

    @client_req
    def client_needs_provider(self, service_or_topic_name, timeout=30):
        self.log("needs_provider for name %r, timeout %r" % (service_or_topic_name, timeout))

        if timeout < 0:
            timeout = 30

        if timeout != 0:
            cb = callback(self._needs_provider_cb, service_or_topic_name)
            self._needs_provider_timeout_source = GLib.timeout_add(int(timeout * 1000), self._needs_provider_timeout)
            self.block_channel("need_provider_in_progress")
        else:
            cb = None
        self._needs_service_or_topic_name = service_or_topic_name
        
        self._needs_provider = self.manager.needs_provider(service_or_topic_name, cb)
        if self._needs_provider is True:
            if timeout != 0:
                self.unblock_channel("need_provider_in_progress")
                GLib.source_remove(self._needs_provider_timeout_source)
            self.info("%r already avaliable!" % service_or_topic_name)
            return True # service/topic avaliable!
        
        self.info("%r not yet avaliable. provider process: %r, timeout: %r!" % (service_or_topic_name, self._needs_provider.name, timeout))
        
        # service/topic provider startup is requested
        if timeout == 0: # nonblocking!
            return True # startup requested...
        
        # wait for startup
        self.debug("wait until timeout or startup is finished for %r" % service_or_topic_name)
        return None # do not send response now!

    # ln_manager_helper processes
    @client_req
    def client_start_process(self, proc_name):
        self.log("start_process %r" % proc_name)
        self.manager.set_process_state_request("Process", pname=proc_name, requested_state="start")
        return True
    @client_req
    def client_stop_process(self, proc_name):
        self.log("stop_process %r" % proc_name)
        self.manager.set_process_state_request("Process", pname=proc_name, requested_state="stop")
        return True
    @client_req
    def client_is_process_started(self, proc_name):
        self.log("is_process_started %r" % proc_name)
        p = self.manager.sysconf.processes[proc_name]
        return dict(started=p.state in ("started", "ready"))
    @client_req
    def client_is_process_ready(self, proc_name):
        self.log("is_process_ready %r" % proc_name)
        p = self.manager.sysconf.processes[proc_name]
        if p.has_ready_state():
            ready = p.state == "ready"
        else:
            ready = p.state == "started"
        return dict(ready=ready)
    @client_req
    def client_get_process_state(self, proc_name):
        self.log("get_process_state %r" % proc_name)
        p = self.manager.sysconf.processes[proc_name]
        return dict(state=p.state)
    @client_req
    def client_locate_member_in_message_definition(self, md_name, member):
        self.log("locate_member_in_message_definition %r member %r" % (md_name, member))
        fn = ln.search_message_definition(md_name)
        md = ln.message_definition(md_name, fn)
        offset, member_len = md.locate_member(member)
        return dict(member_offset=offset, member_byte_len=member_len)

    #####
    def send_request(self, request, cb, **kwargs):
        """
        send_request() manager gui interface
        """
        if cb is not None:
            return self.request(request, cb=cb, **kwargs)
        return self.request_synchronous(request, **kwargs)

    def check_success(self, msg):
        if msg["success"] != "success":
            self.error("received error response:\n%s" % pprint.pformat(msg))
        
    def request(self, method, **kwargs):
        self.last_request_id += 1
        if "cb" in kwargs:
            cb = kwargs["cb"]
            del kwargs["cb"]
            # register callback
            if self.library_version < 3:
                self.registered_callbacks[method] = cb
            else:
                self.registered_callbacks[(method, self.last_request_id)] = cb
        if self.com.fd is None:
            raise Exception("this client is no longer connected!")
        return self.com.send(method=method, request_id=self.last_request_id, **kwargs)

    def request_synchronous(self, method, **kwargs):
        """
        send request and block waiting for answer while processing gtk events
        
        will throw exception if returned success != "success"
        otherwise return complete received message as dict
        """
        if self.wait_for_sync_answer:
            self.warning("already waiting for sync answer!")
            while self.wait_for_sync_answer:
                get_default_glib_mainloop().get_context().iteration(True)
        def got_answer(msg):
            self.debug("got sync answer: %r" % msg)
            self.sync_answer = msg
            return True            
        self.wait_for_sync_answer = True
        self.sync_answer = None
        self.request(method, cb=callback(got_answer), **kwargs)
        while self.sync_answer is None:
            get_default_glib_mainloop().get_context().iteration(True)
        self.wait_for_sync_answer = False
        msg = self.sync_answer
        if msg["success"] != "success":
            raise Exception(msg["success"])
        return msg
    
    def get_port_tids(self, sub, direction):
        if self.library_version < 1:
            return 0 # not supported

        if self.wait_for_port_tid_answer:
            self.warning("already waiting for get_port_tids answer!")
            while self.wait_for_port_tid_answer:
                get_default_glib_mainloop().get_context().iteration(True)
        def got_answer(msg):
            self.get_port_tid_answer = msg
            return True            
        self.wait_for_port_tid_answer = True
        self.get_port_tid_answer = None
        if direction == "output":
            # only info per topic!
            self.request(
                "get_port_tids",
                topic=sub.name,
                direction=direction,
                cb=callback(got_answer))
        elif self.library_version < 10:
            # only info per topic!
            self.request(
                "get_port_tids",
                topic=sub.topic.name,
                direction=direction,
                cb=callback(got_answer))
        else:            
            self.request(
                "get_port_tids",
                topic=sub.topic.name,
                subscription_id=sub.subscription_id,
                direction=direction,
                cb=callback(got_answer))
        e = every(2)
        while self.get_port_tid_answer is None:
            if e:
                self.warning("still waiting for answer of get_port_tids")
            get_default_glib_mainloop().get_context().iteration(True)
        self.wait_for_port_tid_answer = False
        msg = self.get_port_tid_answer
        if msg["success"] != "success":
            raise Exception(msg["success"])
        return msg["last_tid"]

    @client_req
    def client_logger_enable(self, name, topics, only_ts=False):
        self.log("logger_enable name %r topics %r, only_ts: %r" % (name, topics, only_ts))
        logger = self.manager.get_logger(name)
        for topic in topics:
            if topic[1] < 1:
                raise Exception("cowardly refuse to create empty log! you have to set max_samples > 0 for topic %s" % topic[0])
        logger.set_topics(topics)
        logger.set_all_only_ts(bool(only_ts))
        def _when_started():
            self.com.send(
                answer_to="logger_enable",
                success="success"
            )
        logger.enable(cb=_when_started)

    @client_req
    def client_logger_disable(self, name):
        self.log("logger_disable name %r" % name)
        logger = self.manager.get_logger(name, create=False)
        if not logger:
            return True
        def _when_disabled():
            self.com.send(
                answer_to="logger_disable",
                success="success"
            )
        logger.disable(cb=_when_disabled)

    @client_req
    def client_logger_download(self, name):
        self.log("logger_download %r" % name)
        logger = self.manager.get_logger(name, create=False)
        if not logger:
            raise Exception("logger of name %r does not exist!" % name)

        def _on_data(exception=None):
            binary_fields = []
            data = dict(
                __binary_fields=binary_fields,
                answer_to="logger_download",
                success="success",
            )
            for i, (topic_name, topic) in enumerate(logger.topics.items()):
                data["topic_name_%d" % i] = topic_name
                if topic.n_samples is None:
                    data["n_samples_%d" % i] = 0
                    data["sample_size_%d" % i] = 0
                    data["md_name_%d" % i] = 0
                    data["md_%d" % i] = ""
                    data["data_%d" % i] = ""
                else:
                    data["n_samples_%d" % i] = topic.n_samples
                    data["sample_size_%d" % i] = topic.sample_size
                    data["md_name_%d" % i] = topic.md_name
                    data["md_%d" % i] = str(topic.md.dict())
                    if topic.n_samples > 0:
                        data["data_%d" % i] = topic.data
                        binary_fields.append("data_%d" % i)
                    else:
                        data["data_%d" % i] = ""
            self.com.send(**data)
        logger.download(cb=_on_data)

    @client_req
    def client_logger_direct_download(self, name):
        self.log("logger_direct_download %r" % name)
        logger = self.manager.get_logger(name, create=False)
        if not logger:
            raise Exception("logger of name %r does not exist!" % name)
        logger.disable()
        data = dict(
            success="success",
            answer_to="logger_direct_download"
        )
        topics_iter = iter(logger.topics.items())
        topics_cnt = [ 0 ]
        def _get_next_token():
            try:
                topic_name, topic = next(topics_iter)
            except Exception:
                self.com.send(**data)
                return
            
            def _on_token(token):
                if token is not None:
                    i = topics_cnt[0]
                    topics_cnt[0] += 1
                    for name, value in token.items():
                        data["%s_%d" % (name, i)] = value
                _get_next_token()
            topic.get_download_token(self, _on_token)
        _get_next_token()

    @client_req
    def client_logger_manager_save(self, name, filename, format="auto"):
        self.log("logger_manager_save name %r filename %r format %r" % (name, filename, format))
        logger = self.manager.get_logger(name, create=False)
        if not logger:
            raise Exception("logger of name %r does not exist!" % name)
        
        def _on_finish(exception=None):
            if exception is not None:
                return self.com.send(
                    answer_to="logger_manager_save",
                    success="error %s" % exception
                )
            return self.com.send(
                answer_to="logger_manager_save",
                success="success"
            )
        logger.manager_save(_on_finish, filename, format)

    @client_req
    def client_find_services_with_interface(self, interface_name):
        self.log("find_services_with_interface %r" % (interface_name, ))
        services = []
        for client in self.manager.iterate_clients():
            for sname, service in client.provided_services.items():
                if service.interface == interface_name:
                    services.append(sname)
        return dict(services="\n".join(services))

    @client_req
    def client_connect_event(self, name, signature, connect_data, handler):
        """
        client wants to connect to internal event <name>
        """
        self.log("connect_event name %r signature %r connect_data %r handler %r" % (
            name, signature,
            connect_data, # todo: expect bytes object!!
            handler))
        if name == "ln.resource_event":
            self.last_event_connection_id += 1
            self.event_connections[self.last_event_connection_id] = ln_event_connection(
                self, name, signature, connect_data, handler)
            return dict(event_connection_id=self.last_event_connection_id)
        else:
            raise Exception("unknown event %r" % name)
        
    def add_manager_event_listener(self, event, cb):
        if event not in self.event_listeners:
            # register with manager once
            self.manager.add_hook(event, self.event_cb, from_who=self, arg=event)
            self.event_listeners[event] = [cb]
        else:
            self.event_listeners[event].append(cb)
    def event_cb(self, event, mgr, args):
        for cb in self.event_listeners[event]:
            cb(event, **args)
        return True

    def get_log_services(self, sname):
        for lsname, patterns in self.log_services.items():
            if lsname == sname:
                continue # do not log own calls
            for pattern in patterns:
                if pattern[:1] != "!":
                    continue
                if fnmatch.fnmatch(sname, pattern[1:]):
                    # excluded!
                    break
            else:
                # not excluded!
                for pattern in patterns:
                    if pattern[:1] == "!":
                        continue
                    if fnmatch.fnmatch(sname, pattern):
                        yield lsname
                        break

    def get_all_log_services(self, sname):
        all_log_services = set()
        for client in self.manager.iterate_clients():
            if client == self:
                continue
            all_log_services.update(client.get_log_services(sname))
        return all_log_services
                                
    @client_req
    def client_register_as_service_logger(self, logger_service_name, service_name_patterns):
        if logger_service_name not in self.provided_services:
            raise Exception("logger_service_name %r is not one of your provided services!" % logger_service_name)

        self.log_services[logger_service_name] = service_name_patterns.split(",")

        # notify all existing matching services of this log-service!
        for client in self.manager.iterate_clients():
            if client == self:
                    continue
            for sname, service in client.provided_services.items():
                lsnames = self.get_log_services(sname)
                if logger_service_name in lsnames:
                    # yes!
                    client.add_service_logger(logger_service_name, sname)
            for sname in client.requested_services:
                lsnames = self.get_log_services(sname)
                if logger_service_name in lsnames:
                    # yes!
                    client.add_service_logger(logger_service_name, sname)
        return True

    def add_service_logger(self, logger_service_name, service):
        if not self.supports_service_logging:
            if service not in self.warned_no_logging:
                self.warned_no_logging.add(service)
                self.warning("this client does not support service logging for %s" % service)
            return
        if logger_service_name not in self.known_service_loggers:
            self.known_service_loggers[logger_service_name] = set()
        self.known_service_loggers[logger_service_name].add(service)
        self.request(
            "add_service_logger",
            logger_service_name=logger_service_name,
            service_name=service,
            cb=self.check_success
        )
        
    def del_service_logger(self, logger_service_name, service):
        if not self.supports_service_logging:
            return
        if logger_service_name in self.known_service_loggers:
            self.known_service_loggers[logger_service_name].remove(service)
        self.request(
            "del_service_logger",
            logger_service_name=logger_service_name,
            service_name=service,
            cb=self.check_success
        )

    def del_service_loggers(self, logger_service_name):
        logged_services = self.known_service_loggers.get(logger_service_name)
        if not logged_services:
            return
        for ls in list(logged_services):
            self.del_service_logger(logger_service_name, ls)
