# This file is part of links and nodes.
# 
# links and nodes is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# links and nodes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
# 
# Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon

import sys
import gi

sys.setrecursionlimit(2000)

def main():
    with_console = False
    with_mi_console = "--mi-console" in sys.argv
    mgr = None
    
    if "--connect" in sys.argv[1:]:
        i = sys.argv.index("--connect")
        address = sys.argv[i + 1]
        if "--console" in sys.argv or "--console-exec" in sys.argv or with_mi_console:
            from . import ManagerConsole
            con = ManagerConsole.ManagerConsole(connect=address, mi=with_mi_console)
            with_console = True
        else:
            from . import ManagerGui
            gui = ManagerGui.ManagerGui(connect=address)
            gui.main()
    else:
        from . import Manager
        mgr = Manager.Manager(sys.argv[1:])
        if mgr.with_gui:
            from . import ManagerGui
            gui = ManagerGui.ManagerGui(mgr)
            gui.main()
        if mgr.start_console:
            from . import ManagerConsole
            con = ManagerConsole.ManagerConsole(mgr, mi=with_mi_console)
            with_console = True
    while True:
        try:
            if "--connect" in sys.argv[1:] or mgr.with_gui:
                gi.require_version('Gtk', '3.0')
                from gi.repository import Gtk
                Gtk.main()
            else:
                from .tools import get_default_glib_mainloop
                get_default_glib_mainloop().run()
            sys.exit(0)
        except KeyboardInterrupt:
            if with_console:
                # ignore
                continue
            raise

if __name__ == "__main__":
    main()
