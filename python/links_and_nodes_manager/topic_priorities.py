"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

import os

from . import process_listing
from . import config

from .Logging import enable_logging_on_instance

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
from gi.repository import GObject, Gtk, Gdk, GLib # noqa: E402

class topic_priorities(object):
    def __init__(self, parent, topic):
        enable_logging_on_instance(self, "topic_priorities")
        # ctor args
        self.parent = parent
        self.topic = topic

        # own
        self.have_xml = False
        self.rows = {}
        self.refresh_timeout = None
        self.refresh_running = False

        # derived
        self.gui = self.parent.gui
        self.manager_proxy = self.gui.manager_proxy

        # init code
        resources_dir = os.path.join(config.manager_base_dir, "resources")
        self.xml = Gtk.Builder()
        self.xml_fn = os.path.join(resources_dir, "topic_priorities.ui")
        self.xml.add_from_file(self.xml_fn)
        self.have_xml = True
        self.xml.connect_signals(self)
        #self.main_window.set_transient_for(self.gui.dialog)

        # init gui
        self.table_rows = 3
        self.main_window.set_title("%s priorities" % self.topic.name)
        self.topic_name_label.set_text(self.topic.name)
        self.viewport1.override_background_color(Gtk.StateType.NORMAL, Gdk.RGBA(1, 1, 1))

        self.present()

    def __getattr__(self, name):
        if not self.have_xml:
            raise AttributeError(name)
        widget = self.xml.get_object(name)
        if widget is None:    
            raise AttributeError(name)
        return widget

    # gui callbacks
    def on_close_btn_clicked(self, btn):
        self.hide()

    def on_main_window_delete_event(self, win, ev):
        self.hide()
        return True

    def on_refresh_btn_clicked(self, btn):
        self.refresh()

    def on_apply_btn_clicked(self, btn):
        self.apply()

    def hide(self):
        if self.refresh_timeout:
            GLib.source_remove(self.refresh_timeout)
            self.refresh_timeout = None
        self.main_window.hide()

    # implementations
    def present(self):
        self.refresh()
        self.main_window.present()

    class prio_row(object):
        arch_sched_models = {}

        @staticmethod
        def get_arch_sched_model(arch):
            arch = process_listing.process_list.get_arch(arch)
            if arch in topic_priorities.prio_row.arch_sched_models:
                return topic_priorities.prio_row.arch_sched_models[arch]
            if "qnx" in arch:
                from . import qnx_info
                p = qnx_info.policies
            elif "linux" in arch or "posix" in arch:
                from . import linux_info
                p = linux_info.policies
            else:
                #raise Exception("don't know which policies exist on arch %r" % arch)
                topic_priorities.prio_row.arch_sched_models[arch] = None
                return None
            m = Gtk.ListStore(GObject.TYPE_STRING, GObject.TYPE_INT)
            for v, n in p.items():
                if n.startswith("SCHED_"):
                    n = n[6:]
                m.append((n, v))
            topic_priorities.prio_row.arch_sched_models[arch] = m
            return m
            
        def __init__(self, parent, name):
            self.parent = parent
            self.name = name
            self.is_new = True
            
            self.role = None
            self.first_refresh = True

            self.row = self.parent.table_rows
            self.parent.table_rows += 1
            self.last_cpu = ""
            self.last_seen_cpus = set()
            self.last_sched_prio_cpu = None
            self._build_gui()

        def _destruct_gui(self):
            t = self.parent.table
            t.remove(self.role_label)
            if self.no_label_visible:
                t.remove(self.no_label)
            else:
                t.remove(self.sched_label)
                t.remove(self.prio_label)
                t.remove(self.cpu_label)
            t.remove(self.sched_cb)
            t.remove(self.prio_entry)
            t.remove(self.cpu_entry)

            # show all other rows behind us - up!
            to_shift = []
            for name, other_row in self.parent.rows.items():
                if other_row.row > self.row:
                    to_shift.append(other_row)
            to_shift.sort(key=lambda r: r.row)
            for other_row in to_shift:
                other_row.row -= 1
                other_row._reattach()

            self.parent.table_rows -= 1
            self.parent.table.resize(self.parent.table_rows, 7)
            self.parent.table.show_all()
            
        def _reattach(self):
            t = self.parent.table
            t.remove(self.role_label)
            if self.no_label_visible:
                t.remove(self.no_label)
            else:
                t.remove(self.sched_label)
                t.remove(self.prio_label)
                t.remove(self.cpu_label)
            t.remove(self.sched_cb)
            t.remove(self.prio_entry)
            t.remove(self.cpu_entry)

            self.parent.table.attach(self.role_label, 0, 1, self.row, self.row + 1, Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL, 0)
            if self.no_label_visible:
                self.parent.table.attach(self.no_label, 1, 3, self.row, self.row + 1, Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL, 0)
            else:
                for i, n in enumerate("sched,prio,cpu".split(",")):
                    n = "%s_label" % n
                    l = getattr(self, n)
                    self.parent.table.attach(l, 1 + i, 1 + i +1, self.row, self.row + 1, Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL, 0)
            self.parent.table.attach(self.sched_cb, 4, 5, self.row, self.row + 1, Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL, 0)
            self.parent.table.attach(self.prio_entry, 5, 6, self.row, self.row + 1, Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL, 0)
            self.parent.table.attach(self.cpu_entry, 6, 7, self.row, self.row + 1, Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL, 0)
            
        def _build_gui(self):
            self.parent.table.resize(self.parent.table_rows, 7)

            self.role_label = Gtk.Label()
            self.role_label.set_use_markup(True)
            self.role_label.set_alignment(0, 0.5)
            self.role_label.connect("activate-link", self.parent.activate_link, self)
            self.role_label.connect("activate_link", self.parent.activate_link, self)
            self.parent.table.attach(self.role_label, 0, 1, self.row, self.row + 1, Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL, 0)

            self.sched_label = Gtk.Label()
            self.prio_label = Gtk.Label()
            self.cpu_label = Gtk.Label()
            self.cpu_label.set_use_markup(True)
            self.no_label = Gtk.Label()
            self.no_label_visible = True
            self.parent.table.attach(self.no_label, 1, 3, self.row, self.row + 1, Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL, 0)

            self.sched_cb = Gtk.ComboBox()
            self.parent.table.attach(self.sched_cb, 4, 5, self.row, self.row + 1, Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL, 0)

            self.prio_entry = Gtk.Entry()
            self.prio_entry.set_width_chars(4)
            self.parent.table.attach(self.prio_entry, 5, 6, self.row, self.row + 1, Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL, 0)

            self.cpu_entry = Gtk.Entry()
            self.cpu_entry.set_width_chars(4)
            self.parent.table.attach(self.cpu_entry, 6, 7, self.row, self.row + 1, Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL, 0)

            self.parent.table.show_all()

        def set_role(self, role):
            if role == self.role:
                return
            self.role = role
            self.role_label.set_markup(self.role)

        def _show_no_label(self):
            if self.no_label_visible:
                return
            self.no_label_visible = True
            for n in "sched,prio,cpu".split(","):
                n = "%s_label" % n
                l = getattr(self, n)
                l.hide()
                self.parent.table.remove(l)
            self.parent.table.attach(self.no_label, 1, 3, self.row, self.row + 1, Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL, 0)
            self.no_label.show()
            self.sched_cb.set_sensitive(False)
            self.prio_entry.set_sensitive(False)
            self.cpu_entry.set_sensitive(False)

        def _hide_no_label(self):
            if not self.no_label_visible:
                return
            self.no_label_visible = False
            self.no_label.hide()
            self.parent.table.remove(self.no_label)
            for i, n in enumerate("sched,prio,cpu".split(",")):
                n = "%s_label" % n
                l = getattr(self, n)
                self.parent.table.attach(l, 1 + i, 1 + i +1, self.row, self.row + 1, Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL, 0)
                l.show()
            self.sched_cb.set_sensitive(True)
            self.prio_entry.set_sensitive(True)
            self.cpu_entry.set_sensitive(True)

        def set_sched_prio_cpu(self, architecture, sched, sched_no, prio, cpu):
            self.last_sched_prio_cpu = sched_no, prio, self.last_cpu
            #print "%s.last_sched_prio_cpu: %s" % (self, self.last_sched_prio_cpu)
            if sched is not None:
                self.sched_label.set_text(str(sched))
            else:
                self.sched_label.set_text("-")
            self.prio_label.set_text(str(prio))
            self.last_seen_cpus.add(cpu)
            m = []
            for tcpu in self.last_seen_cpus:
                if tcpu == cpu:
                    m.append('<b>%s</b>' % tcpu)
                else:
                    m.append(str(tcpu))
            self.cpu_label.set_markup(", ".join(m))
            self._hide_no_label()
            if self.first_refresh:
                self.first_refresh = False
                self.prio_entry.set_text(str(prio))
                cb = self.sched_cb
                m = self.get_arch_sched_model(architecture)
                if m is None:
                    cb.set_sensitive(False)
                else:
                    cb.set_model(m) # cursor-changed-fine
                    cr = Gtk.CellRendererText()
                    cb.pack_start(cr, True)
                    cb.add_attribute(cr, "text", 0)
                    iter = m.get_iter_first()
                    while iter and m.iter_is_valid(iter):
                        n, v = m[iter]
                        if str(sched).lower() == n.lower():
                            cb.set_active_iter(iter)
                            break
                        iter = m.iter_next(iter)

        def get_last_sched_prio_cpu(self):
            return self.last_sched_prio_cpu

        def get_new_sched_prio_cpu(self):
            cb = self.sched_cb
            m = cb.get_model()
            if m is not None:
                sched = m[cb.get_active_iter()][1]
            else:
                sched = None
            prio = self.prio_entry.get_text().strip()
            if not prio:
                prio = self.last_sched_prio_cpu[1]
            else:
                prio = eval(prio)
            self.last_cpu = self.cpu_entry.get_text()
            return sched, prio, self.last_cpu

        def set_no_label(self, no_text):
            self.no_label.set_text(no_text)
            self._show_no_label()

    def _get_row_for(self, name):
        if name not in self.rows:
            self.rows[name] = self.prio_row(self, name)
        self.mentioned_rows.add(self.rows[name])
        return self.rows[name]

    def refresh(self):
        if self.refresh_running:
            return True
        self.refresh_running = True
        self.manager_proxy("mgr_cb", "get_topic_scheduling_info", self._on_scheduling_info, topic_name=self.topic.name)
        return True
    
    def _on_scheduling_info(self, rows=None, exception=None):
        if exception is not None:
            return self.error(exception)
        
        self.mentioned_rows = set()

        did_publisher_client = False
        for srow in rows:
            row_type = srow[0]
            
            if not did_publisher_client:
                did_publisher_client = True
                row = self._get_row_for("publisher client")
                #row.publisher = self.topic.publisher

                if row_type == "publisher client":
                    client_tuple, row.last_tid = srow[1], srow[2]
                    row.client_tuple = client_tuple
                    row.host = client_tuple[1]
                    row.pid = client_tuple[2]
                    # row.port = publisher client # not really a port...
                    row.set_role('publisher <a href="show_client">%s</a> on %s\ntid <a href="show_thread">%s</a>' % (
                        client_tuple[0], client_tuple[1], row.last_tid))
                    if len(srow) > 3:
                        row.set_sched_prio_cpu(*srow[3])
                        
                    continue
                row.set_role("no publisher yet")
                row.set_no_label("no publisher yet")
                if hasattr(row, "last_tid"): del row.last_tid
            
            if row_type == "shm source": # source thread is running!
                host = srow[1]
                row = self._get_row_for("publisher")
                
                if len(srow) > 2:
                    thread_info, sched_info = srow[2]
                    row.host, row.pid, row.last_tid = thread_info
                    row.set_role('shm source on %s\ntid <a href="show_thread">%s</a>' % (host, row.last_tid))
                    row.set_sched_prio_cpu(*sched_info)
                else:
                    row.set_role('shm source on <a href="show_source">%s</a>' % host)
            
            elif row_type == "receiver":
                host, port_id, address = srow[1], srow[2], srow[3]
                port_type = port_id[0]
                row = self._get_row_for("%s receiver on %s" % (port_type, host))
                if len(srow) > 4:
                    thread_info, sched_info = srow[-1]
                    row.host, row.pid, row.last_tid = thread_info
                    row.set_role('%s receiver on %s\ntid <a href="show_thread">%s</a>\nat %s:%s' % (port_type, host, row.last_tid, address[0], address[1]))
                    row.set_sched_prio_cpu(*sched_info)
                else:
                    row.set_role('%s receiver on <a href="show_receiver">%s</a>\nat %s:%s' % (port_type, host, address[0], address[1]))

            elif row_type == "subscriber":
                client_tuple, sub_id, last_tid = srow[1], srow[2], srow[3]
                row = self._get_row_for("subscriber client %s" % (client_tuple, ))
                row.last_tid = last_tid
                row.client_tuple = client_tuple
                row.host = client_tuple[1]
                row.pid = client_tuple[2]
                row.set_role('subscriber <a href="show_client">%s</a> on %s\ntid <a href="show_thread">%s</a>' % (
                    client_tuple[0], client_tuple[1], row.last_tid))
                if len(srow) > 4:
                    row.set_sched_prio_cpu(*srow[4])

            else:
                self.warning("unknown scheduling info row: %r" % srow)
        # remove unmentioned rows!
        all_rows = set(self.rows.values())
        unmentioned_rows = all_rows.difference(self.mentioned_rows)
        for row in unmentioned_rows:
            del self.rows[row.name]
            row._destruct_gui()
            del row

        self.refresh_running = False
        v = float(self.auto_refresh_sbtn.get_value())
        if v < 0.01 and self.refresh_timeout:
            GLib.source_remove(self.refresh_timeout)
            self.refresh_timeout = None
            return False
        return True
    
    def activate_link(self, label, uri, row):
        self.debug("activate_link %r" % uri)
        if uri == "show_thread":
            pl = self.gui.pg.show_process_list(host=row.host, with_threads=True, select_tid=(row.pid, row.last_tid))
            return True
        if uri == "show_client":
            self.gui.cg._select_client(row.client_tuple)
            self.gui.notebook.set_current_page(1)
            return True
        return False        
        
    def _get_cpu_mask(self, cpu_id_list):
        if not cpu_id_list.strip():
            return None
        if cpu_id_list.startswith("0x"):
            mask = eval(cpu_id_list)
        else:
            cpu_ids = [int(cpu_id.strip()) for cpu_id in cpu_id_list.split(",")]
            mask = 0
            for cpu_id in cpu_ids:
                mask = mask | 1 << cpu_id
        return mask

    def apply(self):
        cnt = [0]
        def _show_error(ret=None, exception=None):
            if exception is not None:
                self.error(exception)
            cnt[0] -= 1
            if cnt[0] == 0:
                self.refresh()

        for name, row in self.rows.items():
            row.last_seen_cpus = set()
            last = row.get_last_sched_prio_cpu()
            new = row.get_new_sched_prio_cpu()
            if last == new:                
                continue
            sched, prio, cpu = new
            cpu_mask = self._get_cpu_mask(cpu)

            cnt[0] += 1
            self.manager_proxy(
                "mgr_cb", "set_prio", _show_error,
                host=row.host.name,
                pid=row.pid,
                tid=row.last_tid,
                prio=prio,
                policy=sched,
                affinity=cpu_mask
            )

    def on_auto_refresh_sbtn_value_changed(self, btn):
        if self.refresh_timeout:
            GLib.source_remove(self.refresh_timeout)
            self.refresh_timeout = None
        
        interval = btn.get_value()
        if interval <= 0:
            return True
        
        self.refresh_timeout = GLib.timeout_add(int(interval * 1000), self.refresh)
        return True
