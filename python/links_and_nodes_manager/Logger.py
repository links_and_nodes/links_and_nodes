"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

import os
import sys
import time
import struct
import pickle
import pprint
import tempfile
import traceback

import numpy

import links_and_nodes
from .Logging import enable_logging_on_instance
from .CommunicationUtils import callback
from . import ThreadWorker
from .CommunicationUtils import tracked_object

class LoggerTopic(object):
    def __init__(self, logger, name, log_size, divisor):
        enable_logging_on_instance(self, "Logger %s" % name)
        self.logger = logger
        self.manager = self.logger.manager
        self.name = name
        self.log_size = log_size
        self.divisor = divisor
        self.only_ts = False
        self.enabled = False

        self.port = None
        self.started_logging_port = None

        if self.name.startswith("subscriber:"):
            self.sub_client, self.sub_topic_name = self.name.split(":", 2)[1:3]
            if "@" in self.sub_client:
                self.sub_client, self.sub_client_rate = self.sub_client.rsplit("@", 1)
                self.sub_client_rate = float(self.sub_client_rate)
            else:
                self.sub_client_rate = None # any
        else:
            self.sub_client = None

        # not yet known
        self.flattened_data = None

        self.md_name = None
        self.md = None
        self._reset_data()
        self.topic_info = []

    def _reset_data(self):
        self.data = None
        self.n_samples = None
        self.data_only_ts = False
        self.sample_size = 0
        self.flattened_data = None

    def set_log_size(self, log_size):
        self.log_size = log_size

    def on_resource_event(self, mgr, args):
        if not self.enabled:
            return
        is_subscriber = self.sub_client is not None and args["event"] == "topic_new_subscriber" and args["name"] == self.sub_topic_name and args["client"] == self.sub_client
        is_publisher  = self.sub_client is None     and args["event"] == "topic_new_publisher"  and args["name"] == self.name # we want to log on publisher side
        if is_publisher or is_subscriber:
            self._start_logging(self._get_topic(), self.max_time)
        return True

    def on_subscription_got_port(self, sub):
        if not self.enabled:
            return False
        if self.sub_client is None:
            return False # should not happen

        is_subscriber = sub.topic.name == self.sub_topic_name and sub.client.program_name == self.sub_client
        if is_subscriber:
            self._start_logging(self._get_topic(), self.max_time)

        return False

    def _get_topic(self):
        if self.sub_client is None:
            return self.manager.topics.get(self.name)
        return self.manager.topics.get(self.sub_topic_name)

    def enable(self, max_time=-1, cb=None):
        if self.log_size < 1:
            raise Exception("cowardly refusing to create empty log for topic %r" % self.name)

        self.enabled = True
        self.max_time = max_time
        self.flattened_data = None
        # does this topic already exist? -> if so enable it!
        # if it does not exist, rely on manager calling our on_new_topic callback and enable then!
        self.manager.add_hook("ln.resource_event", self.on_resource_event, from_who=("lre", self))

        topic = self._get_topic()
        if not topic:
            # topic does not yet exist or has no publisher -> do nothing
            self.info("topic %r is not yet published." % self.name)
            if cb: cb()
            return
        # imm start
        self._start_logging(topic, self.max_time, cb=cb)

    def _set_port_for_topic(self, topic):
        if self.sub_client is None:
            # we want to log publisher shm
            if topic.source_port == self.started_logging_port:
                return False

            self.port = topic.source_port
        else:
            # we want to log subscriber shm
            sub = topic.find_subscriber(self.sub_client, self.sub_client_rate)
            if sub is None:
                return False # not found?!
            if sub.port == "in progress": # not yet done
                sub.add_hook("have_port", self.on_subscription_got_port, id(self))
                return False
            if sub.port == self.started_logging_port:
                return False
            self.port = sub.port

        self.md_name = topic.md_name
        self.md = topic.md
        if topic.publisher:
            self.publisher_name = topic.publisher.program_name
        else:
            self.publisher_name = None

        return True

    def _start_logging(self, topic, max_time=-1, cb=None):
        if not self._set_port_for_topic(topic):
            if cb: cb()
            return False

        def _logging_started(ret=None, exception=None):
            if exception is not None:
                self.error("could not start logging for topic %r: %s" % (topic.name, exception))
                return
            self.started_logging_port = self.port
            self.port.add_subscription(str(self))
            self.info("topic %r port %r logging enabled with %d samples, only_ts: %s, divisor: %s" % (
                self.name, self.port, self.log_size, self.only_ts, self.divisor))
            if cb: cb()

        self.port.daemon.start_logging(
            _logging_started,
            self.port.get_id(),
            self.log_size,
            max_time,
            self.only_ts,
            self.divisor
        )

    def disable(self, cb=None):
        if self.sub_client is not None:
            topic = self._get_topic()
            if topic is not None:
                sub = topic.find_subscriber(self.sub_client)
                if sub is not None:
                    sub.remove_hooks_to(id(self))

        self.manager.remove_hooks_to(("lre", self))
        self.started_logging_port = None
        self.enabled = False

        if self.port is None:
            topic = self._get_topic()
            if not topic or not self._set_port_for_topic(topic):
                self.info("port for topic %r is no longer avaliable. no disable!" % self.name)
                if cb: cb()
                return

        if self.port.was_deleted:
            if cb: cb()
            return

        if self.publisher_name is None and self.port.publisher_topic and self.port.publisher_topic.publisher:
            self.publisher_name = self.port.publisher_topic.program_name

        def _stopped(resp=None, exception=None):
            if exception is not None:
                self.error("could not stop logging for topic %r: %s" % (self.name, exception))
            else:
                self.info("topic %r port %r logging disabled" % (self.name, self.port))
            if cb: cb()
        self.port.daemon.stop_logging(_stopped, self.port.get_id())

    def download(self, cb):
        topic = self._get_topic()
        if not topic or not self._set_port_for_topic(topic):
            self.info("port for topic %r is no longer avaliable. no download!" % self.name)
            self._reset_data()
            return cb()
        start = time.time()

        def _on_answer(msg=None, exception=None):
            if exception is not None:
                self._reset_data()
                errmsg = "topic %r had error downloading log:\n%s" % (self.name, exception)
                self.error(errmsg)
                return cb(exception=msg)

            d = time.time() - start

            self.port.del_subscription(str(self))
            self.n_samples = msg["n_samples"]
            self.info("topic %r got log of %d samples after %.1fs" % (self.name, self.n_samples, d))
            self.data = msg["buffer"]
            self.data_only_ts = msg["only_ts"]
            self.sample_size = 2 * 8 + 4 + self.md.calculate_size()
            self.flattened_data = None
            return cb()

        self.port.daemon.retrieve_log(_on_answer, self.port.get_id())

    def get_download_token(self, client, callback):
        if self.port is None:
            topic = self._get_topic()
            if not topic or not self._set_port_for_topic(topic):
                self.info("port for topic %r is no longer avaliable. no download!" % self.name)
                return callback(None)

        def _get_download_token_answer(msg=None, exception=None):
            if exception is not None:
                errmsg = "topic %r had error providing download log token: %s" % (self.name, exception)
                self.manager.error(errmsg)
                return callback(exception=errmsg)

            if self.port.daemon not in client._log_tokens:
                client._log_tokens[self.port.daemon] = [ msg["token"] ]
            else:
                client._log_tokens[self.port.daemon].append(msg["token"])

            port = msg["port"]
            org_port = port

            client_host = client.host
            topic_host = self.port.daemon.host

            def _provide_download_token_answer(address=None, exception=None, msg=msg):
                if exception is not None:
                    errmsg = "could not provide download log tcp forwarding for topic %r: %s" % (self.name, exception)
                    self.manager.error(errmsg)
                    return callback(exception=errmsg)
                ip, port = address
                ip_port = "%s:%s" % (ip, port)

                self.port.del_subscription(str(self))
                ret = dict(
                    topic_name=self.name,
                    n_samples=msg["n_samples"],
                    sample_size=2 * 8 + 4 + self.md.calculate_size(),
                    md_name=self.md.name,
                    md=str(self.md.dict()),

                    ip_port=ip_port,
                    token=msg["token"],
                )
                self.info("topic %r log token:\n%s" % (self.name, pprint.pformat(ret)))
                return callback(ret)

            if client_host.is_direct_reachable(topic_host):
                ip = client_host.get_ip_from(topic_host)
                return _provide_download_token_answer((ip, port))

            # establish one-time forward!
            self.port.daemon.establish_tcp_forward(
                _provide_download_token_answer,
                client_host, self.port.daemon.host, org_port,
                keep_listening=False, name="logger_%s/%s_token_%s" % (self.logger.name, self.name, client.program_name)
            )

        self.port.daemon.retrieve_log_token(_get_download_token_answer, self.port.get_id())

    class packet_iter(object):
        def __init__(self, topic):
            self.topic = topic
            self.data = self.topic.data
            self.offset = 0
            self.sample_size = self.topic.sample_size
        def next_sample(self): # return None at end!
            if self.data is None or self.offset >= len(self.data):
                return None
            ts1, ts2, cnt = struct.unpack_from("ddI", self.data, self.offset)
            off = self.offset
            self.offset += self.sample_size
            return ts1, ts2, cnt, self.data[off + 2 * 8 + 4:off + self.sample_size]

    def iter(self):
        return LoggerTopic.packet_iter(self)

    def get_header_at(self, offset):
        return struct.unpack_from("ddI", self.data, offset)
    def get_flattened_data(self):
        if self.data is None:
            return None
        if self.flattened_data is not None:
            return self.flattened_data
        # now flatten data
        p_step = self.sample_size

        # todo: speedup by pyrex
        fields = []
        fields.append(("_packet_log_ts", links_and_nodes.data_type_map["double"], 1, 0))
        fields.append(("_packet_source_ts", links_and_nodes.data_type_map["double"], 1, 8))
        fields.append(("_packet_counter", links_and_nodes.data_type_map["uint32_t"], 1, 8 + 8))
        def add_md(md, parent, offset):
            defines = dict(md.defines)
            for ftype, fname, count in md.fields:
                if not parent:
                    real_fname = fname
                else:
                    real_fname = "%s_%s" % (parent, fname)

                if ftype in defines:
                    for c in range(count):
                        if count > 1:
                            fn = "%s[%d]" % (real_fname, c)
                        else:
                            fn = real_fname
                        child_md = links_and_nodes.message_definition(ftype, defines[ftype])
                        #child_md = self._get_md(ftype, defines[ftype])
                        offset = add_md(child_md, fn, offset)
                else:
                    dt = links_and_nodes.data_type_map[ftype]
                    fields.append((real_fname, dt, count, offset))
                    offset += count * dt.get_sizeof()
            return offset
        if not self.data_only_ts:
            add_md(self.md, None, 8 + 8 + 4)

        values = []
        for field_name, field_type, count, offset in fields:
            if count == 1:
                # 1d array
                values.append(numpy.empty((self.n_samples, ), dtype=field_type.npformat))
            else:
                # 2d array
                values.append(numpy.empty((self.n_samples, count), dtype=field_type.npformat))

        for n in range(self.n_samples):
            goffset = n * p_step
            for (field_name, field_type, count, offset), value in zip(fields, values):
                format = "%s%s" % (count, field_type.pyformat)
                v = struct.unpack(
                    format,
                    self.data[goffset + offset:goffset + offset + count * field_type.get_sizeof()])
                if count == 1:
                    value[n] = v[0]
                else:
                    value[n] = v
        data = dict([(field_name, value) for (field_name, field_type, count, offset), value in zip(fields, values)])
        keys = list(data.keys())
        keys.sort()
        self.topic_info = []
        for k in keys:
            self.topic_info.append("  %-50.50s %-12.12s %s" % (k, data[k].shape, data[k].dtype))
        self.flattened_data = data
        return data

class Logger(tracked_object):
    def __init__(self, manager, name):
        self.manager = manager
        self.name = name
        self.topics = {}
        tracked_object.__init__(self)

    def get_instance_id(self):
        return self.name

    to_pickle = "name".split(",")
    def __getstate__(self):
        state = {}
        for attr in self.to_pickle:
            attr = attr.strip()
            state[attr] = getattr(self, attr, None)
        return state
    def __setstate__(self, state):
        self.__dict__.update(state)

    def set_topics(self, topics):
        not_mentioned = set(self.topics.keys())
        for item in topics:
            if len(item) == 2:
                name, log_size = item
                divisor = 1
            else:
                name, log_size, divisor = item[:3]

            topic = self.topics.get(name)
            if topic is None:
                self.topics[name] = LoggerTopic(self, name, log_size, divisor)
            else:
                topic.set_log_size(log_size)
                topic.divisor = divisor
                not_mentioned.remove(name)
        for name in not_mentioned:
            del self.topics[name]

    def set_all_only_ts(self, only_ts):
        for name, topic in self.topics.items():
            topic.only_ts = only_ts

    def enable(self, cb=None, max_time=-1):
        """
        send enable request to all topic daemons
        and optionally  wait for them to ack and call cb()
        """
        wait_for_response = set(self.topics.keys())
        def mark_as_finished(name):
            wait_for_response.remove(name)
            if cb and len(wait_for_response) == 0: cb()
        for name, topic in self.topics.items():
            topic.enable(cb=callback(mark_as_finished, name), max_time=max_time)

    def disable(self, cb=None):
        wait_for_response = set(self.topics.keys())
        def mark_as_finished(name):
            wait_for_response.remove(name)
            if cb and len(wait_for_response) == 0: cb()
        for name, topic in self.topics.items():
            try:
                topic.disable(cb=callback(mark_as_finished, name))
            except Exception:
                self.manager.error("failed to disable topic %r in logger %r:\n%s" % (name, self.name, traceback.format_exc()))
                mark_as_finished(name)

    def download(self, cb):
        to_download = set(self.topics.keys())
        errors = []

        def _on_answer(name=None, exception=None):
            to_download.remove(name)
            if exception is not None:
                error = "failed to download topic %r in logger %r:\n%s" % (name, self.name, exception)
                self.manager.error(error)
                errors.append(error)
            if len(to_download) == 0:
                if errors:
                    cb(exception="\n--\n".join(errors))
                else:
                    cb()

        for name, topic in self.topics.items():
            topic.download(callback(_on_answer, name=name))

    def manager_save(self, cb, filename, format="auto", verbose=False, return_with_cb=False):
        if return_with_cb:
            user_cb = cb
            tmpfile = tempfile.NamedTemporaryFile()
            bn = os.path.basename(filename)
            if "." in bn:
                ext = "." + bn.rsplit(".", 1)[1]
                filename = tmpfile.name.rstrip(".") + "." + ext
            else:
                filename = tmpfile.name
            tmpfile.close()
            def return_cb(exception=None):
                if exception is not None: return user_cb(exception=exception)
                try:
                    with open(filename, "rb") as fp:
                        data = fp.read()
                except Exception:
                    return user_cb(exception="could not read tempfile %r:\n%s" % (filename, traceback.format_exc()))
                os.unlink(filename)
                user_cb(data)
            cb = return_cb
        else:
            # test write to target file
            with open(filename, "ab+") as fp:
                pass

        def _save():
            self._manager_save_after_download(filename, format, verbose)

        def _save_then_notify():
            try:
                _save()
            except Exception:
                self.manager.error("could not save downloaded log:\n%s" % traceback.format_exc())
                return cb(exception=str(sys.exc_info()[1]))
            cb()
        def _save_in_thread_then_notify():
            thread = ThreadWorker.Thread(_save)
            def _threaded_save_finished():
                if thread.exception:
                    return cb(thread.exception)
                cb()
            thread.on_finish_call_in_glib_thread(_threaded_save_finished)
            thread.start()

        save_in_thread = False
        if not save_in_thread:
            download_cb = _save_then_notify
        else:
            # threaded safe is pretty slow. instead build a multiprocess solution...
            download_cb = _save_in_thread_then_notify

        self.download(download_cb) # will do nothing if data is already avaliable

    def _manager_save_after_download(self, filename, format, verbose):
        if format == "auto":
            # try to guess fileformat
            format = "pickle" # default to pickle
            if "." in filename:
                ext = filename.rsplit(".", 1)[1].lower()
                if ext == "pickle":
                    format = "pickle"
                elif ext == "pcap":
                    format = "pcap"
                elif ext == "raw":
                    format = "raw"
                elif ext in ("mat", "matlab"):
                    format = "matlab"
        if format == "mat":
            format = "matlab"
        if format == "mat4":
            format = "matlab4"
        if format == "raw":
            if len(self.topics) != 1:
                raise Exception("raw format only for single topic logs possible!")
            tname, topic = list(self.topics.items())[0]
            with open(filename, "wb") as fp:
                fp.write(topic.data)
            return

        if format == "pcap":
            return self.manager_save_pcap(filename)

        if format == "pickle":
            all_data = {}
            for tname, topic in self.topics.items():
                data = topic.last_flattened_data = topic.get_flattened_data()
                if data is None:
                    #topic.warning("there is no data logged for this topic...")
                    continue
                all_data[tname] = data
            with open(filename, "wb") as fp:
                pickle.dump(all_data, fp, 2)
            return

        if not format.startswith("matlab"):
            return self.error("unknown format %r!" % format)

        all_data = {}
        for tname, topic in self.topics.items():
            data = topic.last_flattened_data = topic.get_flattened_data()
            if data is None:
                #topic.warning("there is no data logged for this topic...")
                continue
            for name, value in data.items():
                if len(self.topics) == 1:
                    all_data[name] = value
                else:
                    if name[0] == "_":
                        name = name[1:]
                    all_data["%s_%s" % (tname, name)] = value

        version = 5
        if len(format) > len("matlab"):
            version = int(format[6:])
        import scipy.io
        to_rename = []
        mat_data = {}
        for key, value in all_data.items():
            if key.startswith("_"):
                key = key[1:] + "_"
            key = key.replace(".", "_").replace("/", "_").replace("[", "_").replace("]", "")
            mat_data[key] = value
        # https://rmc-github.robotic.dlr.de/schm-fl/links_and_nodes/issues/139
        # first save to pickle and then have a dedicated process doing the conversion,
        # with an io-callback here waiting for completion not to block manager for too
        # long because of lost output
        scipy.io.savemat(filename, mat_data, format=str(version))

    def manager_save_pcap(self, filename):
        import links_and_nodes.lnrecorder

        log = links_and_nodes.lnrecorder.lnrecorder_log.open_for_writing(filename)
        first_ts = None
        ts_idx = 1
        for topic_name, topic in self.topics.items():
            topic.pcap_packet_iter = topic.iter()
            topic.pcap_next = topic.pcap_packet_iter.next_sample() # packet_log_ts, packet_source_ts, packet_counter, data
            if topic.pcap_next is None:
                continue # topic was not published
            ts = topic.pcap_next[ts_idx]
            if first_ts is None or ts < first_ts:
                first_ts = ts
            publisher_name = topic.publisher_name or "<unknown>"
            topic.pcap_topic_id = log.write_new_topic(
                ts,
                topic.md.calculate_size(),
                topic_name, topic.md_name,
                repr(topic.md.dict()),
                publisher_name,
                topic.md.get_hash()
            )

        next_ts = first_ts
        while next_ts is not None:
            ts = next_ts
            # as long as any topic has data left...
            next_ts = None
            for topic in list(self.topics.values()):
                while topic.pcap_next is not None:
                    this_ts = topic.pcap_next[ts_idx]
                    if this_ts > ts:
                        if next_ts is None or ts < next_ts:
                            next_ts = this_ts
                        break
                    # equal! -> write!
                    log.write_new_topic_packet(this_ts, topic.pcap_topic_id, topic.pcap_next[-1], topic.pcap_next[1])
                    topic.pcap_next = topic.pcap_packet_iter.next_sample()
        log.close()

    def get_statistics(self):
        """
        return timing statistic for first topic of this logger
        """
        topic = list(self.topics.items())[0][1]
        data = topic.get_flattened_data()
        if not data:
            self.manager.debug("topic %s has no data!" % topic.name)
            return

        def format_time(seconds):
            # ms   us   ns
            # 1e-3 1e-6 1e-9
            if seconds < 1e-9:
                return "%.3fns" % (seconds * 1e9)
            if seconds < 1e-6:
                return "%.1fns" % (seconds * 1e9)
            if seconds < 1e-3:
                return "%.1fus" % (seconds * 1e6)
            if seconds < 10*1e-3:
                return "%.3fms" % (seconds * 1e3)
            if seconds < 1:
                return "%.1fms" % (seconds * 1e3)
            return "%.1fs" % seconds

        def format_freq(hz):
            if hz < 1e3:
                return "%.1fHz" % hz
            if hz < 1e6:
                return "%.3fkHz" % (hz / 1e3)
            return "%.3fMHz" % (hz / 1e6)

        def get_timing(field):
            ts = data[field]
            steps = numpy.diff(ts)
            mean = steps.mean()
            dev = steps - mean
            return "mean step/rate: %s, std dev: %s, max dev: %s" % (
                "%s (%s)" % (format_time(mean), format_freq(1/mean)),
                format_time(steps.std()),
                format_time(max(abs(dev)))
            )
        stats = dict(
            lost_packets=sum(numpy.diff(data["_packet_counter"]) - 1),
            src_ts=get_timing("_packet_source_ts"),
            log_ts=get_timing("_packet_log_ts")
        )
        return stats
