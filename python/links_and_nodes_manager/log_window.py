"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

from __future__ import print_function

import os
import traceback
import datetime

import pyutils

from . import config
from . import ui_tools

from .CommunicationUtils import CommunicationModule
from . import Logging

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Pango', '1.0')
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
from gi.repository import GObject, Gtk, Gdk, Pango, GLib # noqa: E402

class log_window(object):
    update_hook = pyutils.hooked_object()

    def __init__(self, parent, dont_show=False):
        self.parent = parent
        if type(parent) == log_window:
            self.gui = parent.gui
        else:
            self.gui = parent
        self.manager = self.gui.manager
        self.visible = True
        Logging.enable_logging_on_instance(self, "LW")
        self.last_log_id = -1
        self.bug_label = None
        self.log_item_visible = False

        # own local logging from gui
        Logging.trigger_log_update.add_hook("log_update", self.queue_log_update)
        self.update_hook.add_hook("update", self.queue_log_display)

        self.xml = Gtk.Builder()
        resources_dir = os.path.join(config.manager_base_dir, "resources")
        self.xml_fn = os.path.join(resources_dir, "ln_manager_log.ui")
        self.xml.add_from_file(self.xml_fn)
        self.xml.connect_signals(self)

        self.custom_filter = None

        self.fn = ui_tools.get_monospace_font_name(self.main_window)
        self.fd = Pango.FontDescription(self.fn)
        self.item_tv.modify_font(self.fd)
        self.custom_filter_tv.modify_font(self.fd)

        if type(parent) == log_window:
            m = self.log_model = self.parent.log_model
        else:
            m = self.log_model = Gtk.ListStore(
                GObject.TYPE_STRING,   # 0 time
                GObject.TYPE_STRING,   # 1 level
                GObject.TYPE_STRING,   # 2 source
                GObject.TYPE_STRING,   # 3 msgline
                GObject.TYPE_PYOBJECT, # 4 log entry
                GObject.TYPE_STRING,   # 5 background color
                GObject.TYPE_STRING,   # 6 foreground color
                )
        self._get_levels_to_show()
        self.log_filtered_model = None
        tv = self.log_tv
        tv.set_model(m) # cursor-changed-fine
        value_renderer = Gtk.CellRendererText()
        value_renderer.set_property("font-desc", self.fd)
        value_renderer.set_property("yalign", 0)
        tv.insert_column_with_attributes(-1, "time", value_renderer, text=0, cell_background=5, foreground=6)
        col = tv.get_columns()[-1]
        col.set_resizable(True)
        self.time_col = col
        tv.insert_column_with_attributes(-1, "level", value_renderer, text=1)
        self.level_col = tv.get_columns()[-1]
        tv.insert_column_with_attributes(-1, "source", value_renderer, text=2)
        col = tv.get_columns()[-1]
        col.set_sizing(Gtk.TreeViewColumnSizing.FIXED)
        col.set_fixed_width(100)
        col.set_resizable(True)
        self.source_col = col
        tv.insert_column_with_attributes(-1, "msg", value_renderer, text=3)
        col = tv.get_columns()[-1]
        col.set_resizable(True)

        sel = tv.get_selection()
        sel.set_mode(Gtk.SelectionMode.MULTIPLE)

        tv = self.skip_events_tv
        m = self.skip_events_model = Gtk.ListStore(
            GObject.TYPE_BOOLEAN, # active
            GObject.TYPE_STRING,  # description
            GObject.TYPE_INT,  # index
        )
        
        tv.set_model(m) # cursor-changed-fine
        tvc = Gtk.TreeViewColumn("event")
        tcr = Gtk.CellRendererToggle()
        tcr.connect("toggled", self.on_skip_event_toggled)
        tvc.pack_start(tcr, False)
        tvc.set_attributes(tcr, active=0)

        value_renderer = Gtk.CellRendererText()
        tvc.pack_start(value_renderer, True)
        tvc.set_attributes(value_renderer, text=1)
        tv.insert_column(tvc, -1)
        
        w = self.main_window
        self._last_pos = None
        w.set_title("ln log")
        if not dont_show:
            position = self.gui.get_window_size("log_window")
        else:
            position = None
        if position:
            window_size, pos = position
            w.resize(*window_size)
        if not dont_show:
            w.show()
            if position:
                w.move(*pos)
            w.connect("configure-event", self.on_resize)

        if not dont_show:
            self.connect()
        else:
            self.time_cb.set_active(False)
            self.level_cb.set_active(False)

        self._sb_adj = None
        self._log_update = None
        if Logging.log_buffer:
            self.queue_log_update(None, 0)
        self.lw = None
        self.jump_targets = [
            (4, "<separator>"),
            (1, "error")]
        self.queue_log_display(None, False)
        self.on_level_change(None)

        self.log_sw.connect("scroll-event", self.on_scroll)

    def on_main_window_destroy(self, win):
        self.parent.lw = None
        
    def on_scroll(self, sw, ev):
        if ev.direction == Gdk.ScrollDirection.UP:
            self.follow_cb.set_active(False)
        return False
        
    def connect(self):
        def notification_wrapper(callback):
            def inner_notification_wrapper(gui, msg):
                if "request" in msg: 
                    del msg["request"]
                try:
                    return callback(**msg)
                except Exception:
                    self.error("error in notification callback %r%s:\n%s" % (callback.__name__, msg, traceback.format_exc()))
                    raise
            return inner_notification_wrapper
        #self.gui.add_hook(("notification", "update_service"), notification_wrapper(self.on_change))
        #self.gui.add_hook(("notification", "update_client"), notification_wrapper(self.on_change))
        #self.gui.add_hook(("notification", "del_client"), notification_wrapper(self.on_change))
        self.fn = self.gui.sysconf.instance_config.vte_font
        self.fd = Pango.FontDescription(self.fn)
        self.item_tv.modify_font(self.fd)
        self._try_load_custom_filter()

    def present(self):
        self.main_window.present()
        self.visible = True
        return True

    def __getattr__(self, name):
        if hasattr(self, "xml"):
            widget = self.xml.get_object(name)
            if widget is not None:
                return widget
        raise AttributeError(name)

    def on_resize(self, widget, ev):
        window_size = widget.get_size()
        position = widget.get_position()
        new_pos = window_size, position
        if new_pos != self._last_pos:
            self._last_pos = new_pos
            self.gui.store_window_size("log_window", new_pos)

    def queue_log_update(self, obj, log_head):
        if self._log_update is not None:
            return True
        self._log_update = GLib.idle_add(self.log_update)
        return True

    def _do_adjustment(self):
        adj = self.log_sw.get_vadjustment()
        old = adj.get_value()
        adj.set_value(adj.get_upper() - adj.get_page_size())
        if old == adj.get_value():
            self._sb_adj = None
            return False
        return True
    
    def log_update(self):
        if type(self.parent) == log_window:
            return False
        m = self.log_model
        lb = Logging.get_log_buffer(self.last_log_id + 1)
        if not lb:
            self._log_update = None
            return False
        had_error = False
        #print "%r, have %d new log entries starting from log_id %d+1:\n%s" % (
        #    self, len(lb), self.last_log_id, "".join(traceback.format_stack()))
        for log_id, now, level, subsystem, msg, stack in lb:
            if level == "error":
                bgcolor = "#fee"
                fcolor = "red"
                had_error = True
            elif level == "warning":
                bgcolor = "#fffefe"
                fcolor = "#800"
            elif level == "debug":
                bgcolor = "white"
                fcolor = "#222"
            else:
                bgcolor = "white"
                fcolor = "black"
            m.append((str(now), level, subsystem, msg.rstrip(), (msg, stack), bgcolor, fcolor))
        self.last_log_id = log_id
        if hasattr(self.gui, "sysconf"):
            N = m.iter_n_children(None)
            while N > self.gui.sysconf.instance_config.max_log_items:
                m.remove(m.get_iter_first())
                N -= 1
        self.update_hook.call_hook("update", had_error)            
        self._log_update = None
        return False

    def queue_log_display(self, obj, had_error):
        if self.follow_cb.get_active():
            if self._sb_adj is None:
                self._sb_adj = GLib.idle_add(self._do_adjustment)
        if had_error and self.bug_label is not None:
            self.enable_bug_label()
        return True

    def _get_separator_row(self):
        return str(datetime.datetime.now()), "", "", "-- separator --", "<separator>", "yellow", "darkgreen"
    def on_append_separator(self, item):
        m = self.log_model
        m.append(self._get_separator_row())
        if self.follow_cb.get_active():
            if self._sb_adj is None:
                self._sb_adj = GLib.idle_add(self._do_adjustment)
        return True

    def on_insert_separator(self, item):
        m = self.log_model
        x, y = self.popup_pos
        ret = self.log_tv.get_path_at_pos(int(x), int(y))
        if ret is None:
            print("not path!")
            return
        path, col, xrel, yrel = ret
        if self.log_tv.get_model() != m:
            path = self.log_tv.get_model().convert_path_to_child_path(path)
        iter = m.get_iter(path)
        m.insert_before(iter, self._get_separator_row())

    def on_follow_cb_toggled(self, btn):
        if btn.get_active():
            self._do_adjustment()
        return True

    def hide(self):
        self.visible = False
        self.main_window.hide()

    def on_detach_btn_clicked(self, btn):
        if self.lw is None:
            # open log window
            self.lw = log_window(self)
        elif not self.lw.visible:
             self.lw.present()
        else:
            self.lw.hide()
    
    def on_log_tv_button_press_event(self, tv, ev):
        if ev.button == 3:
            self.popup_pos = ev.x, ev.y
            self.log_tv_popup.popup(None, None, None, None, ev.button, ev.time)
            return True
        return False
    
    def on_up_btn_clicked(self, btn):
        m = self.log_model
        path, col = self.log_tv.get_cursor() # path-none-checked
        if path is None:
            path, end_path = self.log_tv.get_visible_range()
        path = list(path)
        while path[0] > 0:
            path[0] -= 1
            row = m[m.get_iter(tuple(path))]
            for i, text in self.jump_targets:
                if row[i] == text:
                    # match!
                    break
            else:
                continue
            break
        self.log_tv.set_cursor(tuple(path))
    def on_down_btn_clicked(self, btn):
        path, col = self.log_tv.get_cursor() # path-none-checked
        if path is None:
            path, end_path = self.log_tv.get_visible_range()
        m = self.log_model
        path = list(path)
        N = m.iter_n_children(None)
        while path[0] < N - 1:
            path[0] += 1
            row = m[m.get_iter(tuple(path))]
            for i, text in self.jump_targets:
                if row[i] == text:
                    # match!
                    break
            else:
                continue
            break
        self.log_tv.set_cursor(tuple(path))

    def on_source_cb_toggled(self, btn):
        self.source_col.set_visible(btn.get_active())
    def on_level_cb_toggled(self, btn):
        self.level_col.set_visible(btn.get_active())
    def on_time_cb_toggled(self, btn):
        self.time_col.set_visible(btn.get_active())
        
    def _get_levels_to_show(self):
        lts = self.levels_to_show = set()
        if self.debug_cb.get_active():
            lts.add("debug")
        if self.info_cb.get_active():
            lts.add("info")
        if self.warning_cb.get_active():
            lts.add("warning")
        if self.error_cb.get_active():
            lts.add("error")
        if self.sep_cb.get_active():
            lts.add("")
    def _filter_visible_func(self, model, iter, data):
        if model[iter][1] not in self.levels_to_show:
            return False
        if self.custom_filter is None:
            return True
        row = model[iter]
        if row is None or row[3] is None:
            return True
        try:
            return eval(self.custom_filter_wrap)
        except Exception:
            self.error("invalid filter: %r\nfor row: %r" % (self.custom_filter, tuple(row)))
            return False
    
    def on_level_change(self, btn):
        self._get_levels_to_show()
        had_path = False
        if len(self.levels_to_show) == 5 and self.custom_filter is None:
            if self.log_tv.get_model() != self.log_model:
                path, col = self.log_tv.get_cursor() # path-none-checked
                if path:
                    path = self.log_tv.get_model().convert_path_to_child_path(path)
                self.log_tv.set_model(self.log_model) # cursor-changed-fine
                del self.log_filtered_model
                self.log_filtered_model = None
                if path:
                    self.log_tv.set_cursor(path)
                    had_path = True

        else:
            if self.log_filtered_model is None:
                self.log_filtered_model = self.log_model.filter_new()
                self.log_filtered_model.set_visible_func(self._filter_visible_func)
            if self.log_tv.get_model() != self.log_filtered_model:
                path, col = self.log_tv.get_cursor() # path-none-checked
                if path:
                    iter = self.log_model.get_iter(path)
                self.log_tv.set_model(self.log_filtered_model) # cursor-changed-fine
                if path:
                    try:
                        new_iter = self.log_filtered_model.convert_child_iter_to_iter(iter)
                        self.log_tv.set_cursor(self.log_filtered_model.get_path(new_iter))
                        had_path = True
                    except Exception:
                        pass
            self.log_filtered_model.refilter()
        if self.follow_cb.get_active() and not had_path:
            if self._sb_adj is None:
                self._sb_adj = GLib.idle_add(self._do_adjustment)
    
    def on_save_btn_clicked(self, btn):
        m = self.log_tv.get_model()
        sel = self.log_tv.get_selection()
        m, iters = sel.get_selected_rows()
        
        save_all = not iters or len(iters) == 1

        #for iter in iters:
        #    #rows.append(tuple(m[iter]))
        dlg = Gtk.FileChooserDialog("save log entries", self.gui.dialog, action=Gtk.FileChooserAction.SAVE,
                                    buttons=(
                                        Gtk.STOCK_CANCEL, Gtk.ResponseType.REJECT,
                                        Gtk.STOCK_OK, Gtk.ResponseType.ACCEPT)
                                    )
        ret = dlg.run()
        dlg.hide()
        if ret != Gtk.ResponseType.ACCEPT:
            return True
        fn = dlg.get_filename()
        fp = open(fn, "w")
        self.n_rows = 0
        def save_row(row):
            ts, level, subsys, msg = tuple(row)[:4]
            head = "%-23.23s %-8.8s %-25.25s " % (
                ts, level, subsys)
            if self.indent is None:
                self.indent = " " * len(head)
            fp.write("%s%s\n" % (head, msg.strip().replace("\n", "\n%s" % self.indent)))
            self.n_rows += 1
        
        self.indent = None
        if save_all:
            m = self.log_model
            iter = m.get_iter_first()
            while iter:
                row = m[iter]
                save_row(row)
                iter = m.iter_next(iter)
        else:
            for iter in iters:
                row = m[iter]
                save_row(row)
        fp.close()

    def show_log_item(self, present=True):
        tv = self.log_tv
        path, col = tv.get_cursor() # path-none-checked
        if not path:
            return
        row = tuple(tv.get_model()[path])
        ts, level, subsys, msg, msg_stack = tuple(row)[:5]
        msg, stack = msg_stack
        self.ts_label.set_text(ts)
        self.level_label.set_text(level)
        self.source_label.set_text(subsys)
        tb = self.item_tv.get_buffer()
        if stack:
            output = "%s\n\nlog-stack:\n%s" % (msg, stack)
        else:
            output = msg
        tb.set_text(output)
        if present: 
            self.log_item_window.present()
        self.log_item_visible = True

    def on_log_tv_row_activated(self, tv, *args):
        self.show_log_item()

    def on_log_item_window_delete_event(self, window, ev):
        self.log_item_window.hide()
        self.log_item_visible = False
        return True

    def on_prev_item_btn_clicked(self, btn):
        tv = self.log_tv
        path, col = tv.get_cursor() # path-none-checked
        if not path:
            return
        path = (path[0] - 1, )
        if path[0] < 0:
            return
        tv.set_cursor(path)
        self.show_log_item()

    def on_next_item_btn_clicked(self, btn):
        tv = self.log_tv
        path, col = tv.get_cursor() # path-none-checked
        if not path:
            return
        path = (path[0] + 1, )
        if path[0] >= tv.get_model().iter_n_children(None):
            return
        tv.set_cursor(path)
        self.show_log_item()

    def on_log_tv_cursor_changed(self, *args):
        if not self.log_item_visible:
            return False
        self.show_log_item(present=False)
        return False


    def on_custom_filter_btn_toggled(self, btn):
        if btn.get_active():
            self.custom_filter_window.present()
            self.custom_filter_tv.grab_focus()
        else:
            self.custom_filter_window.hide()
        
    def on_custom_filter_window_delete_event(self, window, ev):
        if self.custom_filter_btn.get_active():
            self.custom_filter_btn.set_active(False)
        else:
            window.hide()
        return True

    def _get_custom_file_save_name(self):
        return os.path.join(
            os.path.expanduser("~"), ".ln_manager_cust_filt_%s" % self.gui.sysconf.instance_config.name)
        
    def _try_save_custom_filter(self, text):
        fn = self._get_custom_file_save_name()
        try:
            with open(fn, "w") as fp:
                fp.write(text)
        except Exception:
            pass
    def _try_load_custom_filter(self):
        fn = self._get_custom_file_save_name()
        try:
            with open(fn, "r") as fp:
                text = fp.read()
        except Exception:
            return
        self.custom_filter_tv.get_buffer().set_text(text)
        return text
    
    def on_enable_custom_filter_btn_toggled(self, btn):
        if btn.get_active():
            b = self.custom_filter_tv.get_buffer()
            self.custom_filter = b.get_text(b.get_start_iter(), b.get_end_iter(), True).strip()
            row = self.log_model[self.log_model.get_iter_first()]
            self._try_save_custom_filter(self.custom_filter)

            cf = []
            for line in self.custom_filter.split("\n"):
                cline = line.lstrip()
                if cline[:1] == "#":
                    continue
                cf.append(line)
            cf = "( %s )" % ("\n".join(cf), )
            try:
                ret = eval(cf)
                self.custom_filter_wrap = cf
            except Exception:
                self.error("invalid custom filter:\n%s\n%s" % (self.custom_filter, traceback.format_exc()))
                self.custom_filter = None
                btn.set_active(False)
        else:
            self.custom_filter = None
        self.on_level_change(None)

    def on_custom_filter_tv_key_press_event(self, tv, ev):
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.string == "\r":
            was_enabled = self.enable_custom_filter_btn.get_active()
            if not was_enabled:
                self.enable_custom_filter_btn.set_active(True)
            else:
                self.on_enable_custom_filter_btn_toggled(self.enable_custom_filter_btn)
            return True
        return False

    def enable_bug_label(self):
        if self.bug_label_enabled:
            return
        self.bug_label_enabled = True
        self.bug_label_state = False
        self._toggle_bug()
        self.bug_label_timer = GLib.timeout_add(500, self._flash_bug)

    def disable_bug_label(self):
        self.bug_label_enabled = False
        self.bug_label_state = True
        self._toggle_bug()
        GLib.source_remove(self.bug_label_timer)

    def _toggle_bug(self):
        if self.bug_label_state:
            self.bug_label.set_markup(self.bug_label_text)
        else:
            color = self.gui.color_scheme["stopped_or_error"]
            self.bug_label.set_markup('<span color="%s">%s</span>' % (color, self.bug_label_text))
        self.bug_label_state = not self.bug_label_state

    def _flash_bug(self):
        self._toggle_bug()
        if self.gui.notebook.get_current_page() == 5:
            self.disable_bug_label()
        return True

    def register_bug_label(self, label):
        self.bug_label = label
        self.bug_label_text = label.get_text()
        self.bug_label_enabled = False

    event_sources = [
        ("com_ignore_requests", "get_last_packet", "get_last_packet status update"),
        ("com_ignore_requests", "get_state", "get_state status update"),
        ("com_ignore_requests", "process_output", "process output"),
        ("com_ignore_requests", "send_stdin", "send_stdin events"),
        ("com_ignore_requests", "call_service", "internal call service events"),
        ("com_ignore_requests", "retrieve_process_list", "retrieve process list"),
        ("log_flags", "store_stack", "store backtrace with each log-entry"),
        ]

    def _get_event_sources(self):
        for i, (evtype, evid, evtext) in enumerate(self.event_sources):
            if evtype == "com_ignore_requests":
                active = evid in CommunicationModule.ignore_requests
            elif evtype == "log_flags":
                active = evid not in Logging.log_flags
            yield i, (active, evtext)

    def on_skip_events_btn_toggled(self, btn):
        if btn.get_active():
            # todo: fill model
            m = self.skip_events_model
            m.clear()
            for i, (active, text) in self._get_event_sources():
                m.append((active, text, i))
            self.skip_events_window.show()
        else:
            self.skip_events_window.hide()
            
    def on_skip_events_ok_btn_clicked(self, btn):
        m = self.skip_events_model
        self.skip_events_btn.set_active(False)

    def on_skip_event_toggled(self, cr, path):
        m = self.skip_events_model        
        row = m[path]
        row[0] = not row[0]
        active, text, i = row[0], row[1], row[2]
        evtype, evid, evtext = self.event_sources[i]

        if evtype == "com_ignore_requests":
            if active and evid not in CommunicationModule.ignore_requests:
                CommunicationModule.ignore_requests.add(evid)
            elif not active and evid in CommunicationModule.ignore_requests:
                CommunicationModule.ignore_requests.remove(evid)
        elif evtype == "log_flags":
            if active and evid in Logging.log_flags:
                Logging.log_flags.remove(evid)
            elif not active and evid not in Logging.log_flags:
                Logging.log_flags.add(evid)
                
        return True

    def on_clear_log(self, item):
        m = self.log_model
        m.clear()
        return True

    def on_copy_log_item(self, item):
        x, y = self.popup_pos
        tv = self.log_tv
        m = tv.get_model()
        ret = tv.get_path_at_pos(int(x), int(y))
        if ret is None:
            path, col = tv.get_cursor()
            if not path:
                return
        else:
            path, col, xrel, yrel = ret
        iter = m.get_iter(path)
        item = m[iter]
        #print("item: %r" % (list(item), ))
        ts, level, subsystem, msg_line, (msg, stack), bgcolor, fcolor = item
        text = "%s %s %r: %s" % (ts, level, subsystem, msg)
        if stack:
            text += "\n" + stack
        cb = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
        cb.set_text(text, len(text))
