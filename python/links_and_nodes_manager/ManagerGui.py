# This file is part of links and nodes.
#
# links and nodes is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# links and nodes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon

import os
import sys
import pprint
import glob
import copy
import traceback

from links_and_nodes_base.glib_mainloop import GLibMainloop

from . import config

from . import Logging
from .CommunicationUtils import check_for_display_unix_socket, get_xauthority, get_xcookie, import_plugin_package
from .Logging import enable_logging_on_instance
from .ProcessesGui import ProcessesGui
from .TopicsGui import TopicsGui
from .ClientsGui import ClientsGui
from .ServicesGui import ServicesGui
from.tools import get_default_glib_mainloop
from . import parameters_window
from . import log_window
from . import ManagerConnection

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import Gtk, GLib, GdkPixbuf # noqa: E402

manager_base_dir = os.path.dirname(os.path.realpath(__file__))

class ManagerGui(ManagerConnection.ManagerConnection):
    def __init__(self, manager=None, connect=None):
        super(ManagerGui, self).__init__(manager, connect,
                                         mainloop=GLibMainloop(loop=get_default_glib_mainloop()))
        enable_logging_on_instance(self, "ManagerGui")
        if manager:
            self.manager.gui = self
        
        self.window_sizes = {}
        self._last_pos = None
        self._save_last_pos_source = None

        self.is_alt_color_scheme = config.start_with_alternate_color_scheme
        if self.is_alt_color_scheme:
            self.color_scheme = config.alternate_color_scheme
        else:
            self.color_scheme = config.default_color_scheme

        resources_dir = os.path.join(config.manager_base_dir, "resources")
        icons = []
        fns = glob.glob(os.path.join(resources_dir, "lnm_*.png"))
        fns.sort()
        for fn in fns:
            icons.append(GdkPixbuf.Pixbuf.new_from_file(fn))
        Gtk.Window.set_default_icon_list(icons)
        
        self.dialog = Gtk.Window()
        self.dialog.resize(400, 700)
                
        self.notebook = Gtk.Notebook()
        self._pages = {}
        self.dialog.add(self.notebook)

        self.prefered_host_color = {}         
        
        self.pg = ProcessesGui(self)
        self.cg = ClientsGui(self)
        self.tg = TopicsGui(self)
        self.sg = ServicesGui(self)

        self.pw = parameters_window.parameters_window(self.tg, start_pattern="*", dont_show=True)
        self.pw.vbox1.unparent()
        self.add_notebook_page("parameters", self.pw.vbox1, show=False)

        self.lw = log_window.log_window(self, dont_show=True)
        self.lw.vpaned1.unparent()
        bug_label = Gtk.Label("log")
        self.lw.register_bug_label(bug_label)
        self.notebook.append_page(self.lw.vpaned1, bug_label)
        bug_label.show()

        self.notebook.connect("switch-page", self.on_switch_page)

        # window-wide shortcuts
        accel_group = Gtk.AccelGroup()
        self.dialog.add_accel_group(accel_group)

        keyval, modifiers = Gtk.accelerator_parse(config.switch_to_alt_color_combination)
        accel_group.connect(keyval, modifiers, 0, self.on_alt_color)

        self.g = None
        
    def main(self):
        self.dialog.connect("delete-event", self.on_close)
        self.dialog.connect("configure-event", self.on_resize)

        self.connect_to_manager()
        if self.connect:
            enable_logging_on_instance(self, "ManagerGui")
            self.request(("connection", "enable_log_update"))
            self.DISPLAY = os.getenv("DISPLAY")
            self.request(("connection", "set_member"), "DISPLAY", self.DISPLAY)
            have_display_unix_socket = check_for_display_unix_socket(self.DISPLAY)
            self.request(("connection", "set_member"), "DISPLAY_UNIX_SOCKET", have_display_unix_socket)
            self.XAUTHORITY = get_xauthority()
            if not os.path.isfile(self.XAUTHORITY):
                self.warning("XAUTHORITY file %r does not exist!?" % self.XAUTHORITY)
            self.request(("connection", "set_member"), "XAUTHORITY", self.XAUTHORITY)
            self.xcookie = get_xcookie()
            self.request(("connection", "set_member"), "xcookie", self.xcookie)
            
        self.sysconf = self.manager_proxy(request="get_system_configuration")

        self.pg.connect()
        self.cg.connect()
        self.tg.connect()
        self.sg.connect()
        self.pw.connect()
        self.lw.connect()

        self.restore_window_pos()

        if config.version is None:
            version = ""
        else:
            version = "v%s " % config.version
        self.dialog.set_title("ln_manager %s%s on %s" % (version, self.sysconf.instance_config.name, self.sysconf.instance_config.manager))
        self.dialog.show_all()

        self.pg.hide()

        self.notebook.set_current_page(0) # for debugging

        self.init_gui_plugins()

        if self.manager and self.manager.do_present:
            self.dialog.present()

    def select_page(self, page_name):
        for page_idx, pn in enumerate("processes,clients,topics,services,parameters,log".split(",")):
            if pn == page_name:
                self.notebook.set_current_page(page_idx)
                return
        raise Exception("unknown page name %r" % page_name)
            
    def init_gui_plugins(self):
        self.plugin_packages = {}
        self.plugins = {}
        for pname, plugin in self.sysconf.gui_plugins.items():
            try:
                mod = self.plugin_packages[pname] = import_plugin_package(plugin.plugin_package)
                self.debug("successfully imported module file %r" % plugin.plugin_package)
            except Exception:
                self.error("failed to import gui_plugin %r:\n%s" % (pname, traceback.format_exc()))
                continue
            try:
                self.plugins[pname] = mod.gui_plugin(self, plugin)
            except Exception:
                self.error("failed to initialize gui_plugin %r:\n%s" % (pname, traceback.format_exc()))
                continue

    def add_notebook_page(self, name, widget, show=True):
        ret = self.notebook.append_page(widget, Gtk.Label(name))
        self._pages[name] = ret
        if show:
            widget.show_all()
        return ret

    def switch_to_page(self, name):
        self.notebook.set_current_page(self._pages[name])
        
    def work_on_notifications(self):
        kwargs = self.notifications[0]
        self.notifications.pop(0)

        try:
            self._gui_notification(self, kwargs)
        except Exception:
            self.error("error while processing manager notification:\n%s\n%s" % (
                pprint.pformat(kwargs),
                traceback.format_exc()))
        if self.notifications:
            return True
        self.notification_id = None
        return False

    def on_notification(self, kwargs):
        self.notifications.append(kwargs)
        if self.notification_id is None:
            self.notification_id = GLib.idle_add(self.work_on_notifications)
        return True

    def on_close(self, widget, ev):
        if self.connect is None and os.getenv("LN_MANAGER_ASK_SHUTDOWN", "yes").lower()[:1] in ("y", "1", "t") :
            n_clients = self.cg.clients_model.iter_n_children(None)
            if n_clients > 0:
                dlg = Gtk.MessageDialog(
                    widget, 
                    Gtk.DialogFlags.DESTROY_WITH_PARENT, 
                    Gtk.MessageType.QUESTION,
                    message_format=(
                        "there are still clients connected, "
                        "do you only want to close the gui window or do you want to shutdown "
                        "the manager aswell?\n\n"
                        "shutdown ln_manager?")
                    )
                dlg.add_button("stop manager AND gui", Gtk.ResponseType.YES)
                dlg.add_button("stop only gui", Gtk.ResponseType.NO)
                dlg.add_button("cancel", Gtk.ResponseType.CLOSE)
                dlg.show()
                resp = dlg.run()
                dlg.hide()
                
                if resp == Gtk.ResponseType.YES: # stop manager AND gui
                    pass
                elif resp in (Gtk.ResponseType.CLOSE, Gtk.ResponseType.DELETE_EVENT): # cancel or pressing escape
                    return True
                else: # stop only gui
                    widget.hide()
                    self.manager.gui = None
                    del widget
                    return True
        
        if self.connect is None:
            self.manager.exit()
        self.info("Exiting application")
        sys.exit(0)

    def _gui_notification(self, manager_or_gui, msg):
        r = msg.get("request")
        
        if False:
            mkeys = list(msg.keys())
            mkeys.sort()        
            mN = 160
            tmsg = []
            for k in mkeys:
                if k == "request":
                    continue
                line = "  %s: %r" % (k, msg[k])
                if len(line) > mN:
                    line = line[:mN] + "..."
                tmsg.append(line)
            self.debug("gui_notification: %r\n%s" % (r, "\n".join(tmsg)))
        
        if r == "new_config":
            self.sysconf = msg["new_config"]
            if config.version is None:
                version = ""
            else:
                version = "v%s " % config.version
            self.dialog.set_title("ln_manager %s%s on %s" % (version, self.sysconf.instance_config.name, self.sysconf.instance_config.manager))
        elif r == "log_msg":
            for msg_item in msg["msgs"]:
                #print "received", msg
                Logging.append_log(*msg_item[1:])
            Logging.trigger_log_update.notify(Logging.log_head)
            return True
        self.call_hook(("notification", r), msg)
        return True

    def _get_window_pos_fn(self):
        display = os.getenv("DISPLAY", "").replace(":", "_")
        fn = "~/.ln_manager_%s_%s" % (self.sysconf.instance_config.name, display)
        if self.connect:
            fn += "_connected"
        fn = os.path.expanduser(fn)
        return fn

    def save_window_size(self):
        self._save_last_pos_source = None
        if self._old_last_pos != self._last_pos:
            fn = self._get_window_pos_fn()
            self._old_last_pos = copy.copy(self._last_pos)
            with open(fn, "w") as fp:
                fp.write(repr(self._last_pos))
        return False

    def on_resize(self, widget, ev=None):
        window_size = self.dialog.get_size()
        position = self.dialog.get_position()
        pane_pos = self.pg.vpaned.get_position()
        hpane_pos = self.pg.hpaned.get_position()
        new_pos = (
            (window_size.width, window_size.height),
            (position.root_x, position.root_y),
            pane_pos,
            hpane_pos,
            tuple(self.window_sizes.items())
        )
        if new_pos != self._last_pos:
            self._last_pos = new_pos
            if self._save_last_pos_source:
                GLib.source_remove(self._save_last_pos_source)
            self._save_last_pos_source = GLib.timeout_add(500, self.save_window_size)
        return False

    def restore_window_pos(self):
        fn = self._get_window_pos_fn()
        try:
            with open(fn, "r") as fp:
                self._last_pos = eval(fp.read())
        except Exception:
            self._old_last_pos = None
            self.pg.scrolledwindow1.connect("size-allocate", self.on_resize)
            return
        self._old_last_pos = copy.copy(self._last_pos)
        hpane_pos = None
        if len(self._last_pos) == 3:
            window_size, position, pane_pos = self._last_pos
        elif len(self._last_pos) == 4:
            window_size, position, pane_pos, hpane_pos = self._last_pos
        elif len(self._last_pos) == 5:
            window_size, position, pane_pos, hpane_pos, self.window_sizes = self._last_pos
            self.window_sizes = dict(self.window_sizes)
        self.dialog.resize(*window_size)
        self.dialog.move(*position)
        if hpane_pos is not None:
            self._relative_hpane_pos = window_size[0] - hpane_pos
        else:
            self._relative_hpane_pos = None
        self._relative_vpane_pos = window_size[1] - pane_pos
        GLib.idle_add(self._restore_pane_pos)

    def _restore_pane_pos(self):
        # (minimum value for the horizontal pane split.  This matters
        # because for a value of 0, no processes are visible, and
        # seeing no processes has turned out to be a bit confusing for
        # some users, which might erroneously think that something
        # else is misconfigured or not working.)
        MIN_HPANE_POS = 80 # Unit: pixels

        vpane_pos = self.dialog.get_allocated_height() - self._relative_vpane_pos
        self.pg.vpaned.set_position(vpane_pos)

        if self._relative_hpane_pos is not None:
            hpane_pos = self.dialog.get_allocated_width() - self._relative_hpane_pos
            self.pg.hpaned.set_position(max(hpane_pos, MIN_HPANE_POS))

        self.pg.scrolledwindow1.connect("size-allocate", self.on_resize)
        return False

    def get_window_size(self, window_id):
        return self.window_sizes.get(window_id)
    
    def store_window_size(self, window_id, size):
        self.window_sizes[window_id] = size
        self.on_resize(None)

    def on_switch_page(self, nb, ptr, page_no):
        if page_no == 5:
            self.lw.queue_log_display(None, False)
        return True

    def on_alt_color(self, ag, window, akey, amods):
        if self.is_alt_color_scheme:
            self.color_scheme = config.default_color_scheme
        else:
            self.color_scheme = config.alternate_color_scheme
        self.is_alt_color_scheme = not self.is_alt_color_scheme
        self.call_hook("color_scheme_changed", self.color_scheme)
        return True
