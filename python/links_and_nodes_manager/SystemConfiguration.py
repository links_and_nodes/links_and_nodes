"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

from __future__ import print_function

import platform
import pprint
import fnmatch
import socket
import os
import sys
import re
import signal
import copy
import time
import itertools
import shlex
import traceback
import functools
import subprocess
import datetime

try:
    from pyparsing import dblQuotedString, Forward, Optional, alphas, nums, White, OneOrMore, Word, Literal, Or, CharsNotIn, ParserElement, Regex
    import pyparsing
except ImportError:
    # use private pyparsing copy
    from links_and_nodes_manager.private_pyparsing.pyparsing import dblQuotedString, Forward, Optional, alphas, nums, White, OneOrMore, Word, Literal, Or, CharsNotIn, ParserElement, Regex
    import links_and_nodes_manager.private_pyparsing.pyparsing as pyparsing

try:
    import psyco
    psyco.full()
except Exception:
    pass

from . import Logging
from . import config
from . import CommunicationUtils
from . import tools
from . import io_scripts

from .py2compat import unicode, long, cmp

import links_and_nodes as ln
import links_and_nodes_base as ln_base

import pyutils

warned_about_missing_defines = set()
debug_inheritance = False
debug_sections = False
"""
rmpm versions (wihtout beta crap):
major.minor.patch
"""

ln_debug = ln_base.ln_debug
debug_getstate = os.getenv("DEBUG_GETSTATE", "").lower() in ("1", "y", "true")

if not hasattr(signal, "Signals"):
    signal.Signals = int

def is_true(v):
    return v is True or (isinstance(v, (str, unicode)) and v.lower() in ("true", "on", "1", "yes"))

def get_rmpm_version_tuple(a):
    return tuple(map(int, a.split(".")))

def compare_rmpm_version(a, b):
    if a is None or b is None:
        return 0
    at = get_rmpm_version_tuple(a)
    bt = get_rmpm_version_tuple(b)
    if at < bt:
        return -1
    if at == bt:
        return 0
    return 1
    

limit_alias = """
core file size: RLIMIT_CORE
data seg size: RLIMIT_DATA
scheduling priority: RLIMIT_NICE
file size: RLIMIT_FSIZE
pending signals: RLIMIT_SIGPENDING
max locked memory: RLIMIT_MEMLOCK
max memory size: RLIMIT_RSS
open files: RLIMIT_NOFILE
POSIX message queues: RLIMIT_MSGQUEUE
real-time priority: RLIMIT_RTPRIO
stack size: RLIMIT_STACK
cpu time: RLIMIT_CPU
max user processes: RLIMIT_NPROC
virtual memory: RLIMIT_AS
file locks: RLIMIT_LOCKS
"""
valid = set()
limit_alias = dict([line.strip().split(": ", 1) for line in limit_alias.strip().split("\n")])
all_alias = {}
for alias, target in list(limit_alias.items()):
    valid.add(target)
    if target.startswith("RLIMIT_"):
        short = target.split("_", 1)[1]
        all_alias[short.upper()] = target
    all_alias[alias.upper()] = target
limit_alias = all_alias
expect_new_section_error_text = "a new section (like `defines`, `process XY`, `template XY`, ...)"


def read_resource_limits_spec(output_list, input_list):
    existing = set([name for name, value in output_list])
    for name_value in input_list:
        name, value = name_value.split("=", 1)
        if value.strip("+-").isdigit():
            value = int(value)
        elif value.lower() in ("inf", "infinity", "rlim_infinity"):
            value = "inf"
        valid_name = limit_alias.get(name.upper(), name.upper())
        if valid_name not in valid:
            raise Exception("invalid resource limit name: %r. choose one of: %s" % (name, ", ".join(valid)))
        if valid_name not in existing:
            output_list.append((valid_name, value))
            existing.add(valid_name)
        else:
            for i, (name, old_value) in enumerate(output_list):
                if name == valid_name:
                    output_list[i][1] = value

def interpret_filesize(size_limit):
    if size_limit is None or type(size_limit) in (int, long):
        return size_limit
    sl = size_limit.lower()
    e = None
    strip = 2
    if sl.endswith("gb"):
        e = 3
    elif sl.endswith("mb"):
        e = 2
    elif sl.endswith("kb"):
        e = 1
    elif sl.endswith("b"):
        e = 0
        strip = 1
    if e is None:
        sl = int(sl)
    else:
        sl = int(sl[:-strip]) * 1024**e
    return sl

class string_arg_parser(object):
    def __init__(self):
        ParserElement.enablePackrat()
        ParserElement.setDefaultWhitespaceChars(" ")
        strings = Forward()
        strings << dblQuotedString + Optional(Literal(",").suppress() + strings)
        self.parser = strings

    def parse(self, input):
        if input.strip()[:1] != '"':
            return [input]
        ret = self.parser.parseString(input, parseAll=True)
        l = []
        for token in ret:
            if token[0] == '"':
                l.append(token[1:-1])
        return l

class ln_string_parser(object):
    def mark_as(self, what):
        return lambda toks: (what, toks)
    def __init__(self):
        ParserElement.enablePackrat()
        ParserElement.setDefaultWhitespaceChars("")
        mark_as = self.mark_as

        lnc_string_expr = Forward()
        lnc_string_expr_wo_dbl_quotes = Forward()

        quoted_lnc_string_expr = Literal('"').suppress() + lnc_string_expr_wo_dbl_quotes + Literal('"').suppress()
        quoted_lnc_string_expr_no_suppression = Literal('"') + lnc_string_expr_wo_dbl_quotes + Literal('"')

        other_token = CharsNotIn(" \t()%\\") # with dbl quotes! and without parens
        other_token_wo_dbl_quotes = CharsNotIn(" \t%\\\"") # wo dbl quotes with parens!

        other_token_with_paren = ~Literal("%(") + CharsNotIn(" \t%\\")

        escaped_char = Literal("\\").suppress() + Or([Literal(")"), Literal("(")])
        real_escaped_char = Literal("\\").suppress() + Regex(".")
        token = Or([escaped_char, other_token])
        token_wo_dbl_quotes = Or([real_escaped_char, other_token_wo_dbl_quotes])
        define_or_function_name = Word(alphas + "_." + nums, alphas + nums + "_.")
        word_or_define_or_function_call = Forward()
        define = Literal("%(").suppress() + OneOrMore(word_or_define_or_function_call) + Literal(")").suppress()
        define.setParseAction(mark_as("define"))
        list_of_quoted_lnc_string_expr_no_suppression = Forward()
        list_of_quoted_lnc_string_expr_no_suppression << (
            quoted_lnc_string_expr_no_suppression + 
            Optional(
                Literal(",") + 
                Optional(White()).suppress() + 
                list_of_quoted_lnc_string_expr_no_suppression
            )
        )
        function_call = (
            Literal("%(").suppress() + 
            word_or_define_or_function_call + 
            White() + 
            pyparsing.Group(Or([
                list_of_quoted_lnc_string_expr_no_suppression, lnc_string_expr
            ])) + 
            Literal(")").suppress()
        )
        function_call.setParseAction(mark_as("call"))
        word_or_define_or_function_call << Or([token, function_call, define, Literal("%")])
        word_or_define_or_function_call_wo_dbl_quotes = Or([token_wo_dbl_quotes, function_call, define, White()])
        lnc_string_expr << OneOrMore(Or([word_or_define_or_function_call, White()]))

        lnc_string_expr_wo_dbl_quotes << OneOrMore(word_or_define_or_function_call_wo_dbl_quotes)
        outer_lnc_string_expr = Or([
            quoted_lnc_string_expr, 
            OneOrMore(Or([
                word_or_define_or_function_call, 
                White(), 
                other_token_with_paren, 
                Regex(".")
            ]))
        ])
        self.outer_lnc_string_expr = outer_lnc_string_expr

        self.keep_defines = []
        self.defines = None
        self.functions = None

    def get_define(self, name):
        if name in self.keep_defines:
            return "%%(%s)" % name
        if self.defines is None:
            return "<define %r>" % name
        if name not in self.defines:
            self.warning("there is no define1 with name %r as used in %r\nreturning empty string." % (name, self.current_input))
            return ""
        return self.defines[name]
        
    def call_function(self, method, args):
        if self.functions is None:
            return "<func %r args %r>" % (method, args)
        if method not in self.functions:
            self.warning("there is no function with name %r as used in %r\nreturning verbatim!" % (method, self.current_input))
            return '%%(%s "%s")' % (method, args)
        method = self.functions[method]
        return method(args)
        
    def eval_tree(self, tree, skip_list=None, return_whether_includes_defines_to_skip=False):
        if skip_list is None:
            skip_list = []
        output = []
        for token in tree:
            if type(token) == tuple:
                if token[0] == "define":
                    define_name = self.eval_tree(token[1], skip_list)
                    if define_name in skip_list:
                        output.append("%%(%s)" % define_name)
                    else:
                        output.append(self.get_define(define_name))
                elif token[0] == "call":
                    method = self.eval_tree(token[1][:1], skip_list)
                    args = self.eval_tree(token[1][2], skip_list)
                    output.append(self.call_function(method, args))
            else:
                output.append(token)
        return "".join(output)
        
    def parse(self, input):
        return self.outer_lnc_string_expr.parseString(input, parseAll=True)

    def eval(self, input, skip_list=None):
        if skip_list is None:
            skip_list = []
        self.current_input = input
        #print " input: %r" % input
        tree = self.outer_lnc_string_expr.parseString(input, parseAll=True)
        #pprint.pprint(tree)
        out = self.eval_tree(tree, skip_list=skip_list)
        #print "output: %r" % out
        return out

def test_string_expr_parser():
    import sys
    tests = [
        "this  is  a %(variable) in normal text with /a/path/filename.txt\n",
        "  Hoho!  ",
        "simple () \"word\"",
        "%(shell ls /home)",
        "%(shell ls %(home))",
        "%(shell ls $\(home\))",
        "%(shell \"ls h(o)m%(variable)e)\")",
        "%(shell \"ls %(home)\")",
        "%(shell \"ls (home)\")",
        "%(shell id -u; echo ls \(variable\))",
        "%(shell id -u; echo ls %(variable))",
        "(id -u)",
        "%(shell echo \"hallo\")",
        "%(shell ls %(shell echo $HOME))",
        "indirect variable: %(%(varname))",
        "indirect function call : %(%(getfunction %(function_name)) arguments)",
        "%(%(verinner))",
        "%(varbase%(verinner))"
        ]
    #p = ln_string_parser()
    #for test in tests:
    #    print(repr(test))
    #    print(repr(p.eval(test)))
    #    print()
    #sys.exit(0)

    mode = "input"
    tests = []
    for line in open(os.path.join(os.path.dirname(__file__), "parser_tests")):
        if not line.strip():
            mode = "input"
            continue
        print(repr(line))
        value = eval(line.rstrip("\n"))
        if mode == "input":
            input_string = value
            mode = "output"
            continue
        if mode == "output":
            output_string = value
            tests.append((input_string, output_string))
            mode = "input"
            continue
            
    p = ln_string_parser()
    for test, expected in tests:
        print("input:")
        print("12345678901234567890123456789")
        print(test)
        #result = p.parse(test)
        #print "output:"
        #print result
        result = p.eval(test)
        print(result)
        if result != expected:
            print("test failed!")
            print("expected: %r" % expected)
            print("     got: %r" % result)
            sys.exit(0)
        print()

if __name__ == "__main__":
    test_string_expr_parser()
    
    sys.exit(0)

class configuration_object(object):
    _last_config_id = 0
    def __init__(self, start_line=None, cfg_stack=None):
        self._sysconf_start_line = start_line
        self._last_modified_list = None
        self._keys_seen = []
        configuration_object._last_config_id += 1
        self._config_id = configuration_object._last_config_id
        if not hasattr(self, "display_name"):
            self.display_name = self.name
        if self._sysconf.instance_config and is_true(self._sysconf.instance_config.enable_auto_groups) and "/" in self.display_name:
            self.display_name = self.display_name.rsplit("/", 1)[1]
        self.was_derived_from = []
        if cfg_stack is None:
            self._sysconf_stack = self._sysconf.get_include_stack(start_line)
        else:
            self._sysconf_stack = list(cfg_stack)

    def __str__(self):
        d = []
        dk = list(self.__dict__.keys())
        dk.sort()
        for dkk in dk:
            if dkk.startswith("_"):
                continue
            d.append("%s: %r" % (dkk, getattr(self, dkk)))
        return "<%s %s>" % (self.__class__.__name__, ", ".join(d))
    
    def __repr__(self):
        return str(self)

    def _eval_string(self, value):
        this_name = self.name
        if self.display_name is not None:
            this_name = self.display_name
        self.had_eval_string_error = False
        ret = self._sysconf.eval_string(value, add=dict(
            this_class=self.__class__.__name__.lower(),
            this_name=this_name, 
            this_real_name=self.name))
        self.had_eval_string_error = self._sysconf.had_eval_string_error
        return ret

    def _use_template(self, value, prefixes=None):
        derive_from = self._eval_string(value)
        name, args = ConfigurationTemplate._real_parse_template_call(derive_from)
        p = self._sysconf.find_configuration_template(self.name_prefixes, name)
        if p is None and prefixes:
            joined_prefixes = list(self.name_prefixes)
            joined_prefixes.extend(prefixes)
            p = self._sysconf.find_configuration_template(joined_prefixes, name)
            if p is None:
                p = self._sysconf.find_configuration_template(prefixes, name)
        if p is None:
            if name in self._sysconf.processes:
                raise Exception("can only derive from configuration_templates and not process %r! !" % derive_from)
            raise Exception("can not derive from unknown configuration template %r!" % derive_from)

        args = p.bind_arguments(args) # no side effects
        for key, value in p.configs:
            value = p._eval_parameters_all(value, args) # no side effects
            self._read_config(key, value)
        self.was_derived_from.append(p.name)
        
    def _read_config(self, key, value):            
        self._last_modified_list = None
        if key == "use_template":
            self._use_template(value)
            return
        if key.startswith("append ") or key.startswith("prepend ") or key.startswith("remove "):
            # to be executed later in finalize config!!
            is_prepend = key.startswith("prepend ")
            if is_prepend:
                what = "prepend"
            elif key.startswith("append "):
                what = "append"
            else:
                what = "remove"
            append_args = key.split(" ", 1)[1]
            key, item_key = [ part.strip() for part in append_args.split(" ", 1) ]
            if key not in self._valid_config_list_keys:
                raise Exception("invalid %s config key %r: can only %s to items of list-config-keys!" % (what, what, key))
            l = getattr(self, key, [])
            kv_delim = "="
            value = self._eval_string(value)
            # search item key
            for i, item in enumerate(l):
                if item.startswith(item_key + kv_delim):
                    if what == "prepend":
                        l[i] = "%s%s%s%s" % (item_key, kv_delim, value, item[len(item_key) + len(kv_delim):])
                    elif what == "append":
                        l[i] = "%s%s" % (item, value)
                    elif what == "remove":
                        l[i] = item.replace(value, "")
                    break
            else:
                # new create!
                l.append("%s%s%s" % (item_key, kv_delim, value))
            setattr(self, key, l)
            self._last_modified_list = key
            if key not in self._keys_seen:
                self._keys_seen.append(key)
            return
        if key.startswith("add "):
            # to be executed later in finalize config!!
            key = key.split(" ", 1)[1]
            if key not in self._valid_config_list_keys:
                if key + "s" in self._valid_config_list_keys:
                    key = key + "s"
                else:
                    raise Exception("invalid add config key %r: can only add items to list-config-keys!" % key)
            l = getattr(self, key, [])
            value = self._eval_string(value)
            if key == "flags" and "," in value:
                l.extend(re.split(",[ \t]*", value))
            else:
                l.append(value)
            setattr(self, key, l)
            self._last_modified_list = key
            if key not in self._keys_seen:
                self._keys_seen.append(key)
            return
        if key.startswith("del "):
            # remove an item from a config_list!
            key = key.split(" ", 1)[1]
            if key not in self._valid_config_list_keys:
                if key + "s" in self._valid_config_list_keys:
                    key = key + "s"
                else:
                    raise Exception("invalid config %r: can only renmove items from list-config-keys!" % ("remove " + key))
            l = getattr(self, key, [])
            value = self._eval_string(value)
            if key == "flags" and "," in value:
                to_remove = re.split(",[ \t]*", value)
            else:
                to_remove = [ value ]
            l = [i for i in l if i not in to_remove]
            setattr(self, key, l)
            self._last_modified_list = key
            if key not in self._keys_seen:
                self._keys_seen.append(key)
            return
        if key.startswith("reset "): # used to be called "remove "
            # to be executed later in finalize config!!
            key = key.split(" ", 1)[1]
            if key in self._valid_config_list_keys:
                setattr(self, key, [])
            else:
                setattr(self, key, None)
            # todo: this is not really resetting it to its default, but to empty/None!
            self._last_modified_list = key
            if key not in self._keys_seen:
                self._keys_seen.append(key)
            return            
        value = self._eval_string(value)
        if self.had_eval_string_error:
            self._sysconf.error("failed to eval value %r for key %r" % (value, key))

        if key.endswith("+"):
            key = key[:-1]
            do_append = True
        else:
            do_append = False
        if key in self._valid_config_keys:
            is_list = False
        elif key in self._valid_config_list_keys:
            is_list = True
            if "," in value:
                value = [ part.strip() for part in value.strip("\ ,").split(",") ]
            else:
                value = [value]
            self._last_modified_list = key
        else:
            raise Exception("invalid config-key: %r" % key)
        self._keys_seen.append(key)
        if not do_append:
            setattr(self, key, value)
            return
        if is_list:
            getattr(self, key).extend(value)
        else:
            setattr(self, key, getattr(self, key, "") + value)

    def _find_object(self, name, check_groups=False, check_providers=False, raise_when_missing=True):
        possibilities = []
        if hasattr(self, "name_prefixes"):
            if name.startswith("!"):
                format = "!%s/%s"
                pname = name[1:]
            else:
                format = "%s/%s"
                pname = name
            for i in range(len(self.name_prefixes), 0, -1):
                add = format % ("/".join(self.name_prefixes[:i]), pname)
                possibilities.append(add)
        possibilities.append(name)
        
        for m in possibilities:
            if check_groups and m in self._sysconf.groups:
                # exact group match
                yield m, self._sysconf.groups[m]
                return
            if m in self._sysconf.processes:
                # exact process match
                yield m, self._sysconf.processes[m]
                return
            if m in self._sysconf.states:
                # exact state match
                yield  m, self._sysconf.states[m]
                return
            if m[0] == "!" and m[1:] in self._sysconf.states:
                # exact state match NEG!
                yield m, self._sysconf.states[m[1:]]
                return
            
            had_match = False
            for pn, p in self._sysconf.processes.items():
                if fnmatch.fnmatch(pn, m):
                    yield pn, p
                    had_match = True
            for pn, p in self._sysconf.states.items():
                if fnmatch.fnmatch(pn, m) or (m[0] == "!" and fnmatch.fnmatch(pn, m[1:])):
                    yield pn, p
                    had_match = True
            if check_groups:
                for pn, p in self._sysconf.groups.items():
                    if fnmatch.fnmatch(pn, m):
                        yield pn, p
                        had_match = True
            if had_match:
                return

            # check whether other procs/states "provide" this name!
            providers = []
            for obj in itertools.chain(list(self._sysconf.processes.values()), list(self._sysconf.states.values())):
                obj._finalize_provides()
                for what, prio in obj.provides.items():
                    if fnmatch.fnmatch(what, m) or (m[0] == "!" and fnmatch.fnmatch(pn, m[1:])):
                        providers.append(obj.name)
                        #obj._needed_by.append(self)
            if providers:
                providers.sort()
                yield [None, providers], providers
                return
        if raise_when_missing:
            raise Exception("unknown dependancy %r specified!" % name)
        return
            
    def _finish_depends(self, dep):
        dep_out = []
        for dependency in dep:    
            for object_name, obj in self._find_object(dependency, check_providers=True):
                dep_out.append(object_name)
                
                if type(object_name) == list:
                    for o in obj:
                        o._needed_by.append(self)
                else:
                    obj._needed_by.append(self)
        
        return dep_out

    def _remove_depends_on(self, depends_to):
        """
        depends_to is some dependency of us! (depends_on, depends_on_restart) maybe inverse !
        remove this dependency and return dep-type
        """
        n = depends_to.name
        nn = "!" + depends_to.name
        if n in self.depends_on:
            dep_type = "depends_on"
            self.depends_on.remove(n)
        elif nn in self.depends_on:
            dep_type = "!depends_on"
            self.depends_on.remove(nn)
        elif n in self.depends_on_restart:
            dep_type = "depends_on_restart"
            self.depends_on_restart.remove(n)
        elif nn in self.depends_on_restart:
            dep_type = "!depends_on_restart"
            self.depends_on_restart.remove(nn)
        else:
            raise Exception("%r is not a dependency of %r!" % (depends_to.name, self.name))
        return dep_type
    
    def _add_depends_on(self, dep_type, dependency_name):
        if dep_type.endswith("depends_on"):
            self.depends_on.append(dependency_name)
        elif dep_type.endswith("depends_on_restart"):
            self.depends_on_restart.append(dependency_name)
        else:
            raise Exception("unknown dependency type %r for %r" % (dep_type, dependency_name))
    
    def _finalize_provides(self):
        if type(self.provides) == dict:
            return
        provides_dict = {}
        for what in self.provides:
            if ":" in what:
                what, prio = what.rsplit(":", 1)
                prio = float(prio)
            else:
                # are there other providers?
                other_providers = self._sysconf.providers.get(what, [])
                # find prio of conf object before this one and increment by one!
                oobj_before = None
                oprio_before = None
                for oprio, oobj in other_providers:
                    if oobj._config_id < self._config_id and (oobj_before is None or oobj._config_id > oobj_before._config_id): 
                        oobj_before = oobj
                        oprio_before = oprio
                if oobj_before:
                    prio = oprio_before + 1
                else:
                    prio = 0.0
            provides_dict[what] = prio            
            self._sysconf.add_provider(what, prio, self)
        self.provides = provides_dict

class Plugin(configuration_object):
    def __init__(self, sysconf, name, start_line=None):
        self._sysconf = sysconf
        self.name = name
        self.key_values = []
        self.plugin_package = None
        configuration_object.__init__(self, start_line=start_line)

    def _read_config(self, key, value):
        value = self._eval_string(value)
        if key == "plugin_package":
            self.plugin_package = value
            return
        self.key_values.append((key, value))

    def _finalize_config(self):
        if self.plugin_package is None:
            raise Exception("%s %r defined without plugin_package: key!" % (self.__class__.name, self.name))

class Group(configuration_object):
    def __init__(self, sysconf, name, start_line=None, name_prefixes=None, cfg_stack=None):
        if name_prefixes is None:
            name_prefixes = []
        self._sysconf = sysconf
        self.name = name
        self.name_prefixes = list(name_prefixes)
        if self.name_prefixes:
            self.display_name = name[len("/".join(self.name_prefixes))+1:]
        self._warning_is_active = False
        self._valid_config_keys = []
        self.flags = []
        self.show_opened = False
        self.sort_members = True
        self.members = []
        self.auto_members = []
        self._valid_config_list_keys = "members, flags".split(", ")
        configuration_object.__init__(self, start_line=start_line, cfg_stack=cfg_stack)

    def has_member(self, og, indirect=False):
        if not hasattr(self, "group_members"):
            return False
        if og in self.group_members:
            return True
        if indirect is False:
            return False
        for g in self.group_members:
            if g.__class__.__name__ != "Group":
                continue
            if g.has_member(og, indirect=True):
                return True
        return False

    def _finalize_config(self):
        self.allow_empty = "allow_empty" in self.flags
        self.group_members = []
        excludes = []
        def add_group(m):
            if m == self.name:
                return False
            if not self._sysconf.groups[m].has_member(self, indirect=True):
                self.group_members.append(self._sysconf.groups[m])
                return True
            Logging.warning("Group %s" % self.name, "skipping recursive group-linkage. will not add group %r to %r because %r is already a (possibly indirect) member of group %r" % (
                    m, self.name, self.name, m))
            return False
        process_names = list(self._sysconf.processes.keys())
        process_names.sort()
        state_names = list(self._sysconf.states.keys())
        state_names.sort()
        group_names = list(self._sysconf.groups.keys())
        group_names.sort()
        for m in self.members:
            if m.startswith("!"):
                excludes.append(m)
                continue
            had_match = False
            for name, obj in self._find_object(m, check_groups=True, raise_when_missing=False):
                had_match = True
                if type(obj) == Group:
                    add_group(name)
                else:
                    self.group_members.append(obj)                
            if not had_match and not self.allow_empty:
                raise Exception("unknown member group/process/state name-pattern %r used in group %r\n(add flag 'allow_empty' to disable this error)" % (m, self.name))
        
        for m in excludes:
            to_del = []
            for i, obj in enumerate(self.group_members):
                if fnmatch.fnmatch(obj.name, m[1:]):
                    to_del.append(i)
            to_del.reverse()
            for i in to_del:
                del self.group_members[i]
        self._excludes = excludes

        self.show_opened = self._sysconf.instance_config.groups_opened_default
        if "show_opened" in self.flags:
            self.show_opened = True
        elif "show_closed" in self.flags:
            self.show_opened = False

        self.sort_members = self._sysconf.instance_config.groups_sorted_default
        if "unsorted" in self.flags:
            self.sort_members = False
        elif "sorted" in self.flags:
            self.sort_members = True

        self.auto_members.sort()
        for cid, auto_member, obj in self.auto_members:
            if auto_member in self.group_members:
                continue
            m = auto_member
            # search processes/states
            if m in self._sysconf.processes:
                # exact process match
                obj = self._sysconf.processes[m]
                if "/" in obj.name:
                    obj.display_name = obj.name.rsplit("/", 1)[1]
                self.group_members.append(obj)
                continue
            if m in self._sysconf.states:
                # exact state match
                obj = self._sysconf.states[m]
                if "/" in obj.name:
                    obj.display_name = obj.name.rsplit("/", 1)[1]
                self.group_members.append(obj)
                continue
            # search other groups
            if m == self.name:
                continue # stupid, you!
            if m in self._sysconf.groups:
                # exact match
                add_group(m)
                continue
        if self.sort_members:
            self.group_members.sort(key=lambda m: m.name.lower())

    def _possibly_add_process(self, p):
        match = None
        for m in self.members:
            if m.startswith("!"):
                continue
            # search processes/states
            if m == p.name:
                match = "exact"
                break
            # search for patterns
            if fnmatch.fnmatch(p.name, m):
                match = "pattern"
                break
        if match is None:
            return False
        for m in self._excludes:
            if fnmatch.fnmatch(p.name, m[1:]):
                return False
        # add!
        self.group_members.append(p)
        return True

class OutputGenerating(object):
    def __init__(self):
        self.last_output = []
        self.last_output_lines = 0
        self.max_last_output_lines = 500 # lines kept in ln_daemon
        self.max_output_lines = 1000 # lines kept in vte scrollback
        self.output_logfile = None
        self.output_logfile_size_limit = "10MB"
        self.output_logfile_keep_count = 2
        self.output_buffer_size = 1024*100
        self.output_encoding = "utf-8"
        self._valid_config_keys = "max_last_output_lines, max_output_lines, output_logfile_size_limit, output_logfile_keep_count, output_logfile, output_buffer_size, output_encoding".split(", ")
        self.output_to_display = None

    def has_ready_state(self):
        return False # overload if you have...

    def _finalize_output_config(self):
        self.output_logfile_size_limit = interpret_filesize(self.output_logfile_size_limit)
        self.output_logfile_keep_count = int(self.output_logfile_keep_count)
        self.max_output_lines = int(self.max_output_lines)
        self.output_buffer_size = interpret_filesize(self.output_buffer_size)

    def register_output(self, output):
        """
        `output` is expected to be encoded bytes-string
        """
        io_scripts = getattr(self, "io_scripts", None)
        if io_scripts:
            io_scripts.check_output(output)

        if self.max_last_output_lines == 0:
            return
        self.last_output.append(output)
        self.last_output_lines += output.count(b"\n")
        while self.last_output_lines > self.max_last_output_lines:
            old = self.last_output.pop(0)
            self.last_output_lines -= old.count(b"\n")

    def get_output_lines(self, N=10):
        ret = []
        n = 0
        bl = len(self.last_output) - 1
        while bl >= 0 and n < N:
            add = self.last_output[bl]
            ret.insert(0, add)
            bl -= 1
            n += add.count(b"\n")
        return b"\n".join((b"".join(ret)).split(b"\n", N)[:N]) + b"\n"

class Process(configuration_object, OutputGenerating):
    # pickling:
    not_to_pickle = set("process_output_window,vte,update_all_like_cache,requested_state_ctx,output_logfile_fp".split(","))
    def __getstate__(self):
        state = {}
        if debug_getstate: print("debug_getstate: %s: %r" % (self.__class__.__name__, self.name))
        for attr, value in self.__dict__.items():
            if attr in self.not_to_pickle:
                continue
            state[attr] = getattr(self, attr)
            if debug_getstate: print("  %s <- %s" % (attr, type(state[attr])))
        return state
    def __setstate__(self, state):
        self.__dict__.update(state)

    def __deepcopy__(self, memo):
        if ln_debug: Logging.debug("Process %s" % self.name, "deepcopy")
        new_copy = copy.copy(self)
        for name, value in new_copy.__dict__.items():
            if name == "_sysconf":
                continue
            if type(value).__name__ == "SRE_Pattern" or name == "line_highlight_regexes": # immutable objects
                setattr(new_copy, name, value)
                continue
            setattr(new_copy, name, copy.deepcopy(value, memo))
        return new_copy

    def __init__(self, sysconf, name, start_line=None, name_prefixes=None):
        if name_prefixes is None:
            name_prefixes = []
        OutputGenerating.__init__(self)
        self._sysconf = sysconf
        # self.client = None # deprecated: use self.clients
        self.name = name
        self.name_prefixes = list(name_prefixes)
        if self.name_prefixes:
            self.display_name = name[len("/".join(self.name_prefixes))+1:]
        self.requested_state_ctx = None
        self.pid = None
        self.start_time = None
        self.stop_time = None
        self.state = None
        self._was_ready = False
        self.last_requested_state = None
        self.requested_state = None
        self.requested_state_time = None
        self.output = b""
        self.output_lines = 0
        self.total_output_lines = 0
        self.total_output_bytes = 0
        self.outputs_seen = 0
        self.term_timeout = 2
        self.term_signal = signal.SIGTERM
        self.max_queued_output_bytes = 10240
        self.auto_restart_interval = None
        self.start_in_shell = False
        self.use_execvpe = False
        self.no_pty = False
        self.ignore_dep = False
        self.no_error_on_stop = False
        self.no_error_on_successful_stop = False
        self.dont_translate_backspace_to_delete = False
        self.forward_x11 = False
        self.environment = []
        self.change_directory = None
        self.ready_regex = None
        self.ready_time = None
        #self.start_on_ready = None
        self.pass_environment = []
        self.policy = -1
        self.priority = 0
        self.affinity = -1
        self.stack_size = None # currently only used for vxworks
        self.change_user_to = None
        self.documentation = None
        self.description = None
        self.bugtracker_url = None
        self.mantis_project_id = None
        self.warning_regex = None
        self.warning_msg = None
        self.remove_color_codes = False
        self.max_output_frequency = 10
        self.provides = []
        self.map_topics = []
        self.map_services = []
        self.set_resource_limits = []
        self.node = None # logical node name
        self.host = None # pointer to host
        self.notebook = None
        self.ln_debug = False # start with LN_DEBUG set
        self.start_tool = None
        self.disable_gdb_run = False
        self.disable_gdb_rerun = False
        self.restart_requested = False
        self.frozen = False
        self.follow = True
        self.flags = []
        self.start_before = []
        self.line_highlight_regexes = []
        self.line_highlight_fg_color = 1
        self.line_highlight_bg_color = None
        self.blocking_output = False
        self.no_skip_display = False
        self.dont_stop_dependent_procs = False
        self.vte_color_style = None
        self._warning_is_active = False
        self._last_terminate = None
        self._last_stop_time = None
        self._terminate_allowed = False
        self._last_error = None
        self._warnings_log = []
        self.no_default_templates = False
        self.no_host_default_templates = False
        self.arch = None
        self._valid_config_keys += "description, arch, start_tool, max_queued_output_bytes, vte_color_style, line_highlight_fg_color, line_highlight_bg_color, bugtracker_url, mantis_project_id, map_service, stack_size, map_topic, notebook, change_user_to, max_output_frequency, policy, priority, affinity, node, warning_regex, warning_msg, documentation, term_timeout, term_signal, ready_time, ready_regex, change_directory, auto_restart_interval, command".split(", ")
        self._valid_config_list_keys = "start_on_successful_stop, start_before, line_highlight_regexes, set_resource_limits, pass_environment, flags, environment, start_on_ready, depends_on, depends_on_restart, provides".split(", ")
        self._valid_flags = "io_scripts_dont_enforce_blocking_output, no_shmv3, dont_stop_dependent_procs, no_skip_display, blocking_output, no_host_default_templates, no_default_templates, disable_gdb_run, disable_gdb_rerun, no_vte, dont_translate_backspace_to_delete, start_in_shell, use_execvpe, disable_stop, remove_color_codes, no_pty, no_state_led, no_warning_window, no_error_on_stop, no_error_on_successful_stop, use_vte, forward_x11, no_ln_ld_library_path, set_user_and_home, forward_x11_to_gui".split(", ")
        self._finalized = False
        self._has_problem = False
        self._needs_restart = False
        self._errors = []
        self._indirect_errors = []
        self._needed_by = []
        self.appends = {}
        self.item_appends = {}
        self.started_before = []
        self.wait_for_started_before = []
        self._last_stop_successful = True
        self.command_override = None
        self.io_scripts = io_scripts.Container(self)
        
        self.clients = [] # no known connected ln-clients in this process

        configuration_object.__init__(self, start_line=start_line)

    def _read_config(self, key, value):
        for what in "topic", "service":
            if key == "map_" + what:
                if "=>" not in value:
                    raise Exception("invalid map_%s line! expected syntax:\nmap_topic SEARCH_PREFIX => REPLACE\nyou gave %r" % (what, value))
                src, dst = [self._eval_string(part.strip()) for part in value.split("=>", 1)]
                if src != dst:
                    getattr(self, "map_%ss" % what).append((src, dst))
                return
        if key == "derive_from":
            self._sysconf.error("%s: 'derive_from had broken semantics and is no longer supported. please use 'use_template' instead! (will do this now)" % self.name)
            key = "use_template"
        if key.startswith("add_io_script"):
            if " " in key:
                _, name_override = key.split(" ", 1)
            else:
                name_override = None
            if key.startswith("add_io_script_file"):
                self.io_scripts.add_script_file(self._eval_string(value), name_override=name_override)
            else:
                self.io_scripts.add_script(value, name_override=name_override)
            return
        configuration_object._read_config(self, key, value)

    def _resolve_inheritance(self):
        # remove duplicates in environment, by using the last entry
        unique_env = dict()
        kv_delim = "="
        for entry in self.environment:
            if kv_delim not in entry:
                raise Exception("%s: no = in env-var assignment: %r" % (self.name, entry, ))
            key, value = entry.split(kv_delim, 1)
            unique_env[key] = value
        if debug_inheritance: self._sysconf.debug("proc %s: unique_env: %r" % (self.name, unique_env))
        self.environment = []
        for key, value in unique_env.items():
            self.environment.append(kv_delim.join([key, value]))

    def _finalize_config(self):
        if self._finalized:
            return
        if debug_inheritance: self._sysconf.debug("finalize process %r" % self.name)

        self.no_default_templates = "no_default_templates" in self.flags
        if not self.no_default_templates:
            # apply global default templates
            for prefixes, use_template in self._sysconf.process_default_templates:
                self._use_template(use_template, prefixes=prefixes)

        # resovle node name
        if (not hasattr(self, "node") or self.node is None) and hasattr(self, "command") and self.command:
            raise Exception("process %r needs to be assigned to a node!" % self.name)
        if hasattr(self, "command") and self.command:
            if self.node.strip() == "":
                self._sysconf.error("specified empty node-name for process %s! assuming localhost!" % self.name)
                self.node = "localhost"
            if self.node == "<gui_ip>":
                self.dynamic_host = True
                self.host = None
            else:
                self.dynamic_host = False
                self.host = self._sysconf.get_host_for_node(self.node, create=True)
        else:
            self.command = None
            self.node = None

        if len(self.io_scripts) and "io_scripts_dont_enforce_blocking_output" not in self.flags:
            self.blocking_output = True
            self.no_skip_display = True
        else:
            self.blocking_output = "blocking_output" in self.flags
            self.no_skip_display = "no_skip_display" in self.flags

        self.no_default_templates = "no_default_templates" in self.flags
        self.no_host_default_templates = "no_host_default_templates" in self.flags
        self.dont_stop_dependent_procs = "dont_stop_dependent_procs" in self.flags
        if not self.no_default_templates and not self.no_host_default_templates and self.host is not None:
            # apply per host default templates
            for use_template in self.host.process_default_templates:
                self._use_template(use_template)

        self._finalize_output_config()

        if self.mantis_project_id is not None:
            if self.bugtracker_url is not None:
                self._sysconf.warning("mantis_project_id %r overwrites specified bugtracker_url %r" % (self.mantis_project_id, self.bugtracker_url))
            self.bugtracker_url = config.mantis_report_url % self.mantis_project_id
        
        for flag in self.flags:
            if flag.strip() not in self._valid_flags:
                self._sysconf.warning("in %s:%s process %s: unknown flag: %r" % (
                        self._sysconf_start_line[0], self._sysconf_start_line[1],
                        self.name, flag))
        
        evaled = set()

        self._resolve_inheritance()

        self.max_output_frequency = float(self.max_output_frequency)
        if self.auto_restart_interval is not None:
            self.auto_restart_interval = int(self.auto_restart_interval)

        for l in self._valid_config_list_keys:
            if not hasattr(self, l):
                setattr(self, l, [])

        if "environment" not in evaled:
            for i, env in enumerate(self.environment):
                if "%(guess_display)" not in env:
                    continue
                display = os.getenv("DISPLAY")
                if display is None:
                    raise Exception("the current manager DISPLAY is not defined! - can not guess remote DISPLAY for host %s" % (
                        self.host))
                if self.host == self._sysconf.manager_host:
                    self._sysconf.debug("guess_display: process %r is on same host as manager -> simply pass managers display: %r" % (self.name, display))
                    guessed_display = display
                else:
                    if "localhost:" in display:
                        raise Exception("the current manager DISPLAY is %r - can not guess remote DISPLAY for host %s\nplease use the process forward_x11 flag instead!" % (
                            display, self.host))
                    self._sysconf.debug("guess_display: process %r is on a remote host!" % self.name)
                    if display.startswith(":"):
                        guessed_display = "%s%s" % (self.host.get_ip_from(self._sysconf.manager_host), display)
                        self._sysconf.debug("guess_display: assume a local x-connection at %r and hope x-server listens via tcp at %r" % (display, guessed_display))
                        self._sysconf.xhost(self._sysconf.manager_host.get_ip_from(self.host))
                    else:
                        self._sysconf.debug("guess_display: assume a remote x-connection %r and hope remote x-server listens via tcp" % display)
                        guessed_display = display
                    self._sysconf.warning("please consider switching to the process forward_x11-flag instead of using %(guess_display)!")
                self.environment[i] = env.replace("%(guess_display)", guessed_display)
            try:
                real_env = []
                had_ld_library_path = False
                no_ln_ld_library_path = "no_ln_ld_library_path" in self.flags
                for entry in self.environment:
                    if "=" not in entry:
                        raise Exception("no = in env-var assignment: %r" % (entry, ))
                    key, value = entry.split("=", 1)
                    if not no_ln_ld_library_path and key == "LD_LIBRARY_PATH":
                        had_ld_library_path = True
                        values = value.split(os.path.pathsep)
                        to_add = set(ln.ld_library_path).difference(values)
                        if to_add:
                            values += list(to_add)
                            value = os.path.pathsep.join(values).strip(os.path.pathsep)
                    real_env.append((key, value))
                if not no_ln_ld_library_path and not had_ld_library_path:
                    real_env.append(("LD_LIBRARY_PATH", os.path.pathsep.join(ln.ld_library_path)))
            except Exception:
                raise Exception("error in environment for process %r:\n%s\n%s" % (
                        self.name,
                        pprint.pformat(self.environment),
                        traceback.format_exc()))
            self.environment = real_env
        
        self.exec_command = self.command
        #self.depends_on.extend(self.depends_on_restart)

        self._finalize_provides()
        try:
            self.depends_on = self._finish_depends(self.depends_on)
            self.depends_on_restart = self._finish_depends(self.depends_on_restart)
            self.start_on_ready = self._finish_depends(self.start_on_ready)
            self.start_before = self._finish_depends(self.start_before)
            self.start_on_successful_stop = self._finish_depends(self.start_on_successful_stop)
        except Exception:
            raise Exception("Process %r defined at %s:%s: %s" % (self.name, self._sysconf_start_line[0], self._sysconf_start_line[1], sys.exc_info()[1]))

        for e in self.pass_environment:
            for en, ev in os.environ.items():
                if fnmatch.fnmatch(en, e):
                    self.environment.append((en, ev))

        self.dont_translate_backspace_to_delete = "dont_translate_backspace_to_delete" in self.flags
        self.start_in_shell = "start_in_shell" in self.flags
        self.use_execvpe = "use_execvpe" in self.flags
        self.disable_stop = "disable_stop" in self.flags
        self.remove_color_codes = "remove_color_codes" in self.flags
        self.no_pty = "no_pty" in self.flags
        self.no_state_led = "no_state_led" in self.flags
        self.no_warning_window = "no_warning_window" in self.flags
        self.no_error_on_stop = "no_error_on_stop" in self.flags
        self.no_error_on_successful_stop = "no_error_on_successful_stop" in self.flags

        if self.start_on_successful_stop and not (self.no_error_on_stop or self.no_error_on_successful_stop):
            self._sysconf.warning(("process %r: specifying 'start_on_successful_stop' only make sense in combination with one of "
                                   "these flags: 'no_error_on_stop' or 'no_error_on_successful_stop'") % self.name)

        if "use_vte" in self.flags:
            self.use_vte = True
        elif self._sysconf.instance_config.disable_all_vte:
            self.use_vte = False
        else:
            self.use_vte = "no_vte" not in self.flags
        
        self.forward_x11 = "forward_x11" in self.flags
        self.set_user_and_home = "set_user_and_home" in self.flags
        self.forward_x11_to_gui = "forward_x11_to_gui" in self.flags
        self.disable_gdb_rerun = "disable_gdb_rerun" in self.flags
        self.disable_gdb_run = "disable_gdb_run" in self.flags

        if self.ready_time is not None:
            self.ready_time = float(self.ready_time)
        self.term_timeout = float(self.term_timeout)

        if isinstance(self.term_signal, signal.Signals):
            pass
        elif self.term_signal.isdigit():
            self.term_signal = int(self.term_signal)
        else:
            s = self.term_signal.strip().upper()
            if not s.startswith("SIG"):
                s = "SIG" + s
            if not hasattr(signal, s):
                raise Exception("invalid signal specification - unknown signal %r" % (self.term_signal, ))
            self.term_signal = getattr(signal, s)

        if type(self.policy) != int:
            if self.policy[0].isdigit():
                self.policy = int(self.policy)
        if type(self.priority) != int:
            if self.priority is None:
                self.priority = 0
            else:
                self.priority = int(self.priority)
        if self.affinity != -1:
            self.affinity = CommunicationUtils.get_cpu_affinity_mask(self.affinity)

        if self.vte_color_style is None:
            self.vte_color_style = self._sysconf.instance_config.vte_color_style

        self.resource_limits = list(self._sysconf.instance_config.default_resource_limits)
        read_resource_limits_spec(self.resource_limits, self.set_resource_limits)

        # compile line highlight regexes
        for i, regex in enumerate(self.line_highlight_regexes):
            try:
                self.line_highlight_regexes[i] = re.compile(regex)
            except Exception:
                raise Exception("invalid line_highlight_regex: %r\n%s" % (regex, traceback.format_exc()))

    def has_ready_state(self):
        return bool(self.ready_time) or self.ready_regex is not None

    def _mark_as_stopped_if_daemon_connected(self):
        if not self.host:
            return
        daemon = self._sysconf.manager.get_daemon(self.host, create=False)
        if not daemon:
            return
        if daemon.when_register_finished.get_reached_or_fail() is True:
            # daemon is already connected, assume process is stopped!
            self.state = "stopped"
    
    def reload_update(self, op):
        # transer state from old process op
        """
        self._sysconf.debug("need to reload transfer state from\n%s to %s\n%s\nto\n%s\n" % (
            id(op._sysconf),
            id(self._sysconf),
            pprint.pformat(op.__dict__),
            pprint.pformat(self.__dict__)))
        """
        if op is None: # this is a new process!
            self._mark_as_stopped_if_daemon_connected()
            return
        for attr in "process_output_window,output,output_lines,outputs_seen,pid,_warning_is_active,client,clients,daemon,frozen,ignore_dep,last_requested_state,ln_debug,start_tool,start_time,state,stop_time".split(","):
            if not hasattr(op, attr):
                continue
            setattr(self, attr, getattr(op, attr))
            if attr == "process_output_window":
                self.process_output_window.p = self
        if self.host.name != op.host.name:
            self.state = None # reset state on host change!
        if hasattr(self, "daemon"):
            if self.pid in self.daemon._started_processes:
                self.daemon._started_processes[self.pid] = self
        for clnt in self.clients:
            clnt.process = self

    def has_window(self):
        return hasattr(self, "process_output_window") and self.process_output_window
        # and not self.current_p.process_output_window.hidden

class ConfigurationTemplate(configuration_object):

    def __deepcopy__(self, memo):
        if ln_debug: Logging.debug("ConfigurationTemplate %s" % self.name, "deepcopy")
        new_copy = copy.copy(self)
        for name, value in new_copy.__dict__.items():
            if name == "_sysconf":
                continue
            #if ln_debug: debug("ConfigurationTemplate %s" % self.name, "deepcopy field %r: %r" % (name, value))
            setattr(new_copy, name, copy.deepcopy(value, memo))
        return new_copy

    def __init__(self, sysconf, name, name_prefixes=None, start_line=None):
        if name_prefixes is None:
            name_prefixes = []
        self._sysconf = sysconf
        self._template_arguments = []
        name = self._parse_template_name(name)
        self._template_arguments_set = None
        self.name = name
        self.name_prefixes = list(name_prefixes)
        configuration_object.__init__(self, start_line=start_line)
        self.configs = []

    def _parse_template_name(self, tname):
        #print "parse_template_name"
        name, args = self._parse_template_call(tname)
        #print "name %r, arg %r" % (name, args)
        for arg in args:
            if "=" in arg:
                k, v = arg.split("=", 1)
                self._template_arguments.append((k, self._sysconf.eval_string(v)))
            else:
                self._template_arguments.append((arg, ""))
        return name

    @staticmethod
    def _parse_template_call(call):
        #print "parse tempalte call: %r" % (call, )
        m = re.search("\((.*)\)", call)
        if m is None:
            return call, []
        name = call[:m.start(1)-1].strip()
        args = [s.strip() for s in m.group(1).split(",")]
        #print "name %r, arg %r" % (name, args)
        return name, args

    @staticmethod
    def _real_parse_template_call(call):
        #print "real parse tempalte call: %r" % (call, )
        if not ("(" in call and ")" in call):
            return call, []
        name, args = call.split("(", 1)
        try:
            args = eval("(" + args.rstrip(")") + ", )")
        except Exception:
            raise Exception("error evaluating derive_from-/template arguments as python expression: %r" % (args.rstrip(")"), ))
        #print "name %r, arg %r" % (name, args)
        return name, args
        
    def bind_arguments(self, args):
        """ has no side effects! """
        if not args:
            return dict(self._template_arguments)
        values = {}
        for (name, default_value), vv in zip(self._template_arguments, args):
            values[name] = vv
        for name, default_value in self._template_arguments[len(args):]:
            values[name] = default_value
        return values

    def _eval_parameters(self, value, argument_values):
        """
        argument_values is already expected to include ALL of self._template_arguments!
        this function has no side effects!
        """
        if type(value) != str:
            #print "not a string..."
            return value
        for dn, replacement in list(argument_values.items()):
            ddn = "%%(%s)" % dn
            if ddn in value:
                value = value.replace(ddn, replacement)
        return value

    def _eval_parameters_all(self, value, argument_values):
        """ 
        argument_values is already expected to include ALL of self._template_arguments! 
        this function has no side effects!
        """
        if type(value) == str:
            return self._eval_parameters(value, argument_values)
        if type(value) == list:
            nl = []
            for item in value:
                nl.append(self._eval_parameters_all(item, argument_values))
            return nl
        raise Exception("dont know hwo to eval template parameters in this value: %r" % (value, ))


    def _read_config(self, key, value):
        self.configs.append((key, value))


class InstanceConfig(configuration_object):
    def __init__(self, sysconf):
        self._sysconf = sysconf
        self.documentation = None
        self._valid_config_keys = (
            "groups_opened_default, groups_sorted_default, enable_auto_groups, default_notebook_path, daemon_private_key_file, daemon_public_key_file, max_log_items, additional_ssh_arguments, " + 
            "scope_process_template, notebook_process_template, name, manager, ssh_binary, daemon_start_script, vte_font, vte_color_style, log_file, log_file_size_limit, log_file_keep_count, gen_message_definition_path, " + 
            "documentation, default_change_user_to, default_expected_user, allow_x11_tcp_connect, open_scope_on_parameter_double_click, do_not_try_uids_of_clients, do_lock_daemons_with_unexpected_uids, " +
            "check_all_states_after_daemon_connect, pid_file"
        ).split(", ")
        self._valid_config_list_keys = ["flags", "default_resource_limits"]
        self.ssh_binary = "/usr/bin/ssh"
        self.name = "unnamed testing instance"
        self.daemon_start_script = config.daemon_start_script
        configuration_object.__init__(self)
        # default config:
        self.max_log_items = 5000
        self.flags = []
        self.manager = None
        self.internal_scope_template_name = "internal scope template"
        self.scope_process_template = self.internal_scope_template_name
        self.internal_notebook_template_name = "internal notebook template"
        self.notebook_process_template = self.internal_notebook_template_name
        self.gen_message_definition_path = config.default_gen_msg_defs
        self.additional_ssh_arguments = config.additional_ssh_arguments
        self.daemon_private_key_file = config.daemon_private_key_file
        self.daemon_public_key_file = config.daemon_public_key_file
        self.enable_auto_groups = False
        self.allow_x11_tcp_connect = False
        self.open_scope_on_parameter_double_click = is_true(os.getenv("LN_OPEN_SCOPE_ON_PARAMETER_DOUBLE_CLICK", True))
        self.do_not_try_uids_of_clients = False
        self.do_lock_daemons_with_unexpected_uids = False
        self.check_all_states_after_daemon_connect = False
        self.log_file = None
        self.log_file_size_limit = None
        self.log_file_keep_count = 0
        self.vte_font = "Monospace"
        self.vte_color_style = "holo_flo"
        self.pid_file = None
        self.groups_sorted_default = None
        self.groups_opened_default = None
        self.default_change_user_to = None
        self.default_expected_user = None
        self.default_resource_limits = []

    def _read_config(self, key, value):
        configuration_object._read_config(self, key, value)
        if self._last_modified_list == "flags":
            if "log_store_stack" in self.flags:
                Logging.log_flags.add("store_stack")
            elif "store_stack" in Logging.log_flags:
                Logging.log_flags.remove("store_stack")
        
    def _finalize_config(self):
        self.log_file_size_limit = interpret_filesize(self.log_file_size_limit)
        self.log_file_keep_count = int(self.log_file_keep_count)
        
        resource_limits = []
        read_resource_limits_spec(resource_limits, self.default_resource_limits)
        self.default_resource_limits = resource_limits

        self.do_lock_daemons_with_unexpected_uids = is_true(self.do_lock_daemons_with_unexpected_uids)
        self.do_not_try_uids_of_clients = is_true(self.do_not_try_uids_of_clients)
        self.open_scope_on_parameter_double_click = is_true(self.open_scope_on_parameter_double_click)
        self.allow_x11_tcp_connect = is_true(self.allow_x11_tcp_connect)
        self.enable_auto_groups = is_true(self.enable_auto_groups)
        self.check_all_states_after_daemon_connect = is_true(self.check_all_states_after_daemon_connect)
        
        if self.groups_sorted_default is None:
            if self.enable_auto_groups:
                self.groups_sorted_default = False
            else:
                self.groups_sorted_default = True
        else:
            self.groups_sorted_default = is_true(self.groups_sorted_default)

        if self.groups_opened_default is None:
            if self.enable_auto_groups:
                self.groups_opened_default = True
            else:
                self.groups_opened_default = False
        else:
            self.groups_opened_default = is_true(self.groups_opened_default)
        
        self.default_notebook_path = os.path.join(
            os.path.dirname(
                os.path.realpath(self._sysconf.configuration_file)),
            ".notebooks")
        
        self.disable_all_vte = "disable_all_vte" in self.flags
        self.no_shmv3 = "no_shmv3" in self.flags
        
        if "raise_rlimit_nofile" in self.flags:
            import resource
            soft, hard = resource.getrlimit(resource.RLIMIT_NOFILE)
            if soft == hard:
                self._sysconf.debug("number of open files already at hard limit %r" % hard)
            else:
                self._sysconf.debug("raising number of open files to hard limit %r" % hard)
                soft = hard
                resource.setrlimit(resource.RLIMIT_NOFILE, (soft, hard))

        if self.manager is None and not self._sysconf.only_export_hosts:
            raise Exception("you did not specify a manager hostname and port in the config file's instance section!\nplease add something like\n\tinstance\n\tmanager: :54414\nto your config file!")

        if self.manager == "None" or self._sysconf.only_export_hosts:
            self.manager_host = "localhost"
            self.manager_port = None
            self.manager = None
        else:
            self.manager_host, self.manager_port = self.manager.split(":", 1)
            if not self.manager_host.strip():
                self.manager_host = platform.uname()[1]
                if self.manager_host == "localhost":
                    self._sysconf.warning("your systems hostname is reported as 'localhost' which is by far not unique. please choose a better hostname in your instance's config manager: variable!")
            if self.manager_host == "localhost":
                self._sysconf.warning("in the instance config section you specified the manager: host-name to be 'localhost' which is a misleading choice. please try to choose a better unique host-name!")
            if self._sysconf.manager.overwrite_port is None:
                self.manager_port = int(self.manager_port)
            else:
                self.manager_port = self._sysconf.manager.overwrite_port
            self.manager = "%s:%s" % (self.manager_host, self.manager_port)
        
        # check self.gen_message_definition_path
        real_gen_md_path = os.path.realpath(self.gen_message_definition_path)
        if not hasattr(ln_base, "initial_message_definition_dirs"):
            ln_base.initial_message_definition_dirs = list(ln_base.message_definition_dirs)
        else:
            # restore!
            ln_base.message_definition_dirs = list(ln_base.initial_message_definition_dirs)
            
        if not os.path.isdir(real_gen_md_path):
            os.makedirs(real_gen_md_path)
            ln_base.rescan_message_definition_dirs()
        if real_gen_md_path not in self._sysconf.additional_message_definition_dirs:
            self._sysconf.additional_message_definition_dirs.append(real_gen_md_path)

        for md_path in ln_base.message_definition_dirs:
            md_path = os.path.realpath(md_path)
            if not real_gen_md_path.startswith(md_path):
                continue
            if md_path.endswith(os.sep):
                l = len(md_path) - 1
            else:
                l = len(md_path)
            if len(real_gen_md_path) > l and real_gen_md_path[l] != os.sep:
                continue
            self.gen_message_definition_prefix = real_gen_md_path[l + 1:]
            # assume real_gen_md_path is below this md_path
            break
        else:
            raise Exception("instance_config.gen_message_definition_path %r is not within these specified message_definition_dirs:\n%s" % (
                    self.gen_message_definition_path,
                    "\n".join(ln_base.message_definition_dirs)))

        if (self.daemon_private_key_file and self.daemon_private_key_file.lower() == "none") or (self.daemon_public_key_file and self.daemon_public_key_file.lower() == "none"):
            self.daemon_private_key_file = None
            self.daemon_public_key_file = None
        else:
            if self.daemon_public_key_file and self.daemon_public_key_file != "never" and not self.daemon_private_key_file:
                raise Exception("daemon_public_key_file is specified in instance section. but there is no daemon_private_key_file specified!")
            if self.daemon_private_key_file and (not self.daemon_public_key_file or self.daemon_public_key_file == "never"):
                raise Exception("daemon_private_key_file is specified in instance section. but there is no daemon_public_key_file specified!")

            cfg_dir = os.path.dirname(os.path.realpath(self._sysconf.current_fn))
            preferred_private_key_fn = os.path.expanduser("~/.ssh/id_dsa_ln_daemons")
            did_try_to_gen = False
            if not self.daemon_private_key_file and self.daemon_public_key_file != "never":
                self._sysconf.debug("no daemon_private_key_file (type dsa) specified in instance section. will try to find an existing key...")
                while True:
                    # try to find existing
                    guesses = [preferred_private_key_fn, "~/.ssh/id_dsa", os.path.join(cfg_dir, ".id_dsa"), os.path.join(cfg_dir, "id_dsa")]
                    for g in guesses:
                        pn = os.path.expanduser(g)
                        if os.path.isfile(pn) and os.path.isfile(pn + ".pub"):
                            self._sysconf.debug("does %s{,.pub} exist? -> yes!" % (pn))
                            self.daemon_private_key_file = pn
                            self.daemon_public_key_file = pn + ".pub"
                            break
                        self._sysconf.debug("does %s{,.pub} exist? -> no" % (pn))
                    else:
                        if did_try_to_gen:
                            self._sysconf.error("there was no daemon_private_key_file (type dsa) specified in config's instance section and we failed to automatically create one!")
                        # try to generate
                        did_try_to_gen = True
                        cmd = "mkdir -p '%s'; ssh-keygen -N '' -t dsa -m PEM -f '%s'" % (os.path.dirname(preferred_private_key_fn), preferred_private_key_fn)
                        self._sysconf.warning("no daemon_private_key_file specified in instance section (type dsa). will try to generate one by executing:\n%s" % cmd)
                        ret = pyutils.system(cmd)
                        if ret:
                            self._sysconf.warning("ssh-keygen command %r returned: %d" % (cmd, ret))
                        continue
                    break

            if self.daemon_private_key_file:
                if not os.path.isfile(self.daemon_private_key_file):
                    self._sysconf.error("private key file %r does not exist!", self.daemon_private_key_file)

                # check private key perms
                sbuf = os.stat(self.daemon_private_key_file)
                mode = "%o" % sbuf.st_mode
                if mode[-2:] != "00":
                    self._sysconf.error("daemon private key %r has too open permissions: %s! remove group/other's rights!" % (
                        self.daemon_private_key_file, mode))
        if self.scope_process_template != self.internal_scope_template_name:
            if self.scope_process_template not in self._sysconf.configuration_templates:
                self._sysconf.error("configured scope_process_template %r does not exist!" % self.scope_process_template)

class State(configuration_object, OutputGenerating):
    not_to_pickle = set("process_output_window,vte,update_all_like_cache,requested_state_ctx".split(","))
    def __getstate__(self):
        state = {}
        if debug_getstate: print("debug_getstate: %s: %r" % (self.__class__.__name__, self.name))
        for attr, value in self.__dict__.items():
            if attr in self.not_to_pickle:
                continue
            state[attr] = getattr(self, attr)
            if debug_getstate: print("  %s <- %s" % (attr, type(state[attr])))
        return state
    def __setstate__(self, state):
        self.__dict__.update(state)

    def __deepcopy__(self, memo):
        if ln_debug:
            ident = "State %s" % self.name
            Logging.debug(ident, "deepcopy")
        new_copy = copy.copy(self)
        for name, value in new_copy.__dict__.items():
            if name == "_sysconf":
                continue
            if type(value).__name__ == "SRE_Pattern": # immutable objects
                setattr(new_copy, name, value)
                continue
            if name in ("_needed_by", "nodes", "host", "tcp_tunnel_exit_host"): # speed hacks?
                setattr(new_copy, name, value)
                continue
            if ln_debug: Logging.debug(ident, "  deepcopy state member %r" % name)
            a = time.time()
            setattr(new_copy, name, copy.deepcopy(value, memo))
            b = time.time() - a
            if ln_debug and b > 0.01:
                Logging.debug(ident, "copy state %r attribute %r took %.0fms" % (self.name, name, b*1e3))
        if ln_debug: Logging.debug(ident, "deepcopy done")
        return new_copy

    def __init__(self, sysconf, name, start_line=None, name_prefixes=None):
        if name_prefixes is None:
            name_prefixes = []
        OutputGenerating.__init__(self)
        self._sysconf = sysconf
        self.name = name
        
        self.name_prefixes = list(name_prefixes)
        if self.name_prefixes:
            self.display_name = name[len("/".join(self.name_prefixes))+1:]

        self.requested_state_ctx = None
        self.state = "<unknown>" # changed from current_state
        self.requested_state = None
        self.last_check = 0
        self.needed_forwards = {} # forwards in use. hostname -> forward_name
        self.request_active = False

        # cmd's output
        self.output = b""
        self.output_lines = 0
        self.outputs_seen = 0

        # config fields
        self.check = None
        self.up = None
        self.down = None
        self.command_overrides = dict() # check|up|down -> command|None
        self.provides = []

        self.min_check_interval = 30 #s do not call check-cmd more often that this! (assume that last state is still valid!)

        self.tcp_tunnel_exit_node = None
        self.tcp_tunnel_exit_target = None
        self.tcp_tunnel_entry_port = None

        # config flags
        #self.start_in_shell = False
        self.os_state = False
        self.autostart = False
        #self.no_pty = False
        #self.ignore_dep = False        

        # output
        self.output = b""
        self.output_lines = 0
        self.outputs_seen = 0
        self.max_queued_output_bytes = 10240
        self.no_skip_display = False
        # to dist. it from a process:
        self.no_pty = True
        self.use_vte = False
        self._errors = []
        self._indirect_errors = []
        self._warnings_log = []
        self._warning_is_active = False
        self.arch = None
        self._valid_config_keys += "arch, max_queued_output_bytes, check, up, down, min_check_interval, auto_check_interval, tcp_tunnel_exit_node, tcp_tunnel_exit_target, tcp_tunnel_entry_port".split(", ")
        self._valid_config_list_keys = "flags, node, depends_on, depends_on_restart, provides".split(", ")
        self._finalized = False
        self._needed_by = []
        configuration_object.__init__(self, start_line=start_line)

    def is_recheck_allowed(self, daemon_start_time=0):
        if daemon_start_time > self.last_check:
            return True
        age = time.time() - self.last_check
        return age > self.min_check_interval

    def _finalize_config(self):
        if self._finalized:
            return
        #print "finalize %r" % self.name
        self._finalize_output_config()

        # check, up, down cmd strings
        self.min_check_interval = float(self.min_check_interval)
        if hasattr(self, "auto_check_interval"):
            self._sysconf.error("%s: states no longer support 'auto_check_interval'!" % self.name)
        
        for l in self._valid_config_list_keys:
            if not hasattr(self, l):
                setattr(self, l, [])

        self.nodes = set()
        #if len(self.node) > 1:
        #    raise Exception("a state can have at most one assigned node!")
        for node in self.node:
            self.nodes.add((node, self._sysconf.get_host_for_node(node, create=True)))
            self._have_own_nodes = True

        if not self.nodes:
            self._have_own_nodes = False
            for p in self._needed_by:
                if not hasattr(p, "host") and not p._finalized:
                    if hasattr(p, "deviced_states"):
                        #print "have deviced_states!", len(p.deviced_states)
                        for ds in p.deviced_states:
                            #print "take host from derived state %r" % ds.name
                            self.nodes.add((ds.node, ds.host))
                        continue
                    #print "recurse finalize %r first!" % p.name
                    p._finalize_config()
                
                if not hasattr(p, "host"):
                    raise Exception("state %r is needed by %r, but this one has no host attribute!" % (
                        self.name, p.name))
                self.nodes.add((p.node, p.host))
        if not self.nodes:
            raise Exception("error: state %r needs to assigned to at least one node!" % self.name)
        self.nodes = list(self.nodes)
        self._finalize_provides()
        self.depends_on = self._finish_depends(self.depends_on)
        self.depends_on_restart = self._finish_depends(self.depends_on_restart)
        #self.start_in_shell = "start_in_shell" in self.flags
        self.os_state = "os_state" in self.flags
        self.autostart = "autostart" in self.flags
        self.need_check_after_up = "need_check_after_up" in self.flags
        self.last_command = None

        self.is_tunnel = self.tcp_tunnel_exit_target is not None and self.tcp_tunnel_exit_node is not None

        if (self.tcp_tunnel_exit_target is not None or self.tcp_tunnel_exit_node is not None) and not self.is_tunnel:
            raise Exception("a tunnel state has to have both keys: tcp_tunnel_exit_target AND tcp_tunnel_exit_node!")
        
        if (self.up or self.check or self.down) and self.is_tunnel:
            raise Exception("a tunnel state can not have up, down or a check-command!")

        if self.is_tunnel:
            self.min_check_interval = 1
            self.entry_port = self.tcp_tunnel_entry_port
            if self.entry_port is not None:
                self.entry_port = int(self.entry_port)
            try:
                self.target_ip, target_port = self.tcp_tunnel_exit_target.rsplit(":", 1)
            except Exception:
                raise Exception("invalid tunnel_exit_target format. needs to be ip:tcp-port not %r" % self.tcp_tunnel_exit_target)
            try:
                self.target_port = int(target_port)
            except Exception:
                raise Exception("invalid tunnel_target port: %r: %s" % (target_port, sys.exc_info()[1]))
            if self.target_ip.lower() == "localhost":
                self.target_ip = "127.0.0.1"
            if not CommunicationUtils.is_ip(self.target_ip):
                raise Exception("tunnel exit target needs to be IP:PORT - not an ip: %r" % self.target_ip)
            self.tcp_tunnel_exit_host = self._sysconf.get_host_for_node(self.tcp_tunnel_exit_node, create=True)

        if not self._have_own_nodes:
            if ln_debug:
                print("finalize state %s by these nodes: %s" % (self.name, self.nodes))
                import traceback
                print("".join(traceback.format_stack()))
                print("we are needed by %d:" % len(self._needed_by))
                for dp in self._needed_by:
                    print("  %r which has these %d dependencies:" % (dp.name, len(dp.depends_on)))
                    for dpdep in dp.depends_on:
                        if dpdep == self.name:
                            match = "*"
                        else:
                            match = ""
                        print("    %r%s" % (dpdep, match))
            # remove links to old name
            dep_type = {}
            for i, dp in enumerate(self._needed_by):
                dep_type[dp] = dp._remove_depends_on(self)
            deviced_states = []
            for node, host in self.nodes:
                state_name = "%s(%s)" % (self.name, host.name)
                if state_name in self._sysconf.assigned_states:
                    continue
                if ln_debug: print("  create state %s" % state_name)

                # do not copy needed by...
                saved_needed_by = self._needed_by
                self._needed_by = []
                saved_nodes = self.nodes
                self.nodes = []
                state = copy.deepcopy(self)
                self._needed_by = saved_needed_by
                self.nodes = saved_nodes
                
                deviced_states.append(state)
                state.name = state_name
                state.node = node
                state.host = host
                self._sysconf.assigned_states[state.name] = state
                # recalc needed by
                for i, dp in enumerate(self._needed_by):
                    #node = p.selected_node
                    #host = p.selected_host
                    if hasattr(dp, "host") and dp.host == state.host:
                        state._needed_by.append(dp)
                        dp._add_depends_on(dep_type[dp], state_name)
            self.deviced_states = deviced_states
        else:
            self._sysconf.assigned_states[self.name] = self
            self.node, self.host = self.nodes[0]
            
    def get_commands(self):
        if self.is_tunnel:
            return dict(
                tunnel_exit_target=self.tcp_tunnel_exit_target,
                tunnel_exit_node=self.tcp_tunnel_exit_node,
                # resolved info:
                tunnel_entry_port=self.entry_port,
                tunnel_exit_target_ip=self.target_ip,
                tunnel_exit_target_port=self.target_port,
                tunnel_exit_host=self.tcp_tunnel_exit_host
            )
        cmds = dict()
        for which in "check,up,down".split(","):
            ov = self.command_overrides.get(which)
            if ov is None:
                ov = getattr(self, which)
            cmds[which] = ov
        return cmds

class HostInterface(object):
    def __deepcopy__(self, memo):
        if ln_debug: print("deepcopy host-interface %r" % self.name)
        new_copy = copy.copy(self)
        for name, value in new_copy.__dict__.items():
            if name == "_sysconf":
                new_copy._sysconf = value
                continue
            if ln_debug: print("  deepcopy host member %r" % name)
            setattr(new_copy, name, copy.deepcopy(value, memo))
        if ln_debug: print("deepcopy host %r done" % self.name)
        return new_copy
    
    def __init__(self, host, spec, is_loopback=False):
        self.host = host
        self.netmask = None
        if ":" in spec:
            self.name, self.ip_address = spec.rsplit(":", 1)
        else:
            self.ip_address = spec
            self.name = "intf%d" % len(self.host.interfaces)
        self.is_loopback = is_loopback
        self.network = None

        if "@" in self.ip_address:
            self.ip_address, self.alternate_socket = self.ip_address.split("@")
            if "=" in self.alternate_socket:
                self.alternate_socket = self.alternate_socket.split("=", 1)[1]
        else:
            self.alternate_socket = None
        if not CommunicationUtils.is_ip(self.ip_address):
            raise Exception("ip_address-part %r of %r for host %s is not a valid ip-address!" % (
                self.ip_address, spec, host))
        
    def get_full_name(self):
        return "%s/%s" % (self.host.name, self.name)

    def matches(self, ip):
        if ip == self.ip_address:
            return True
        if ip == self.get_full_name():
            return True
        if ip == self.name:
            return True
        return False

    @staticmethod
    def find_interface(sysconf, spec, create=False):
        if "/" in spec:
            host, interface_spec = spec.split("/", 1)
            try:
                host = sysconf.get_host_for_node(host)
            except Exception:
                raise Exception("unknown host %r in interface spec %r!" % (host, spec))
            return host.get_interface(interface_spec)
        # search all hosts
        for host_name, host in sysconf.hosts.items():
            try:
                return host.get_interface(spec)
            except Exception:
                pass
        # is it a hostname of just one interface?
        try:
            host = sysconf.get_host_for_node(spec, create=create)
        except Exception:
            host = None
        if host is not None:
            if len(host.interfaces) > 1:
                raise Exception("interface spec %r matches host %s but this host has multiple interfaces: %s" % (
                    spec, host, ", ".join(["%s:%s" % (intf.name, intf.ip_address) for intf in host.interfaces])))
            return host.interfaces[0]
        raise Exception("interface %r not found!" % spec)

    def __str__(self):
        return repr(self)
    def __repr__(self):
        return "<interface %r on %r ip %r network %s>" % (self.name, self.host, self.ip_address, self.network)

def split_names(names):
    return [part.strip() for part in names.split(",")]

class Host(configuration_object):    
    def __init__(self, sysconf, name, start_line=None):
        self._sysconf = sysconf
        self.name = name # good, possibly user defined unique hostname

        self.aliases = set()
        
        self.daemon_start_script = None
        self.additional_ssh_arguments = None
        self.ssh_binary = None
        self.preferred_network = None
        self.arch = None
        self.arbiter_port = None
        self.requires_tunnel = None
        self.process_start_rate_limit = None
        self.lookup_aliases = True
        self.skip_fw_test = False
        
        self.interfaces = [] # list of HostInterface
        self.process_default_templates = []
        self.filesystems = [] # known filesystems on that host (manager is complete same filesystem that manager has)
        # a filesystem is considered some toplevel mountpoint. everything below that mountpoint is avaliable on all
        # hosts that have this filesystem.

        self._attrs = split_names("""
          arch,daemon_private_key_file,daemon_public_key_file,preferred_network,daemon_start_script,
          additional_ssh_arguments,expected_user,expected_user_id,arbiter_port,default_change_user_to,
          ssh_binary, filesystems, requires_tunnel, process_start_rate_limit, lookup_aliases,
          skip_fw_test
        """)
        
        # fill some aliases
        self.aliases.add(self.name)
        if self.name in ("localhost", "127.0.0.1"):
            try:
                hostname = socket.gethostname()
                self.aliases.add(hostname)
            except Exception:
                pass
        self.aliases.difference_update(("localhost", "127.0.0.1"))
        if platform.uname()[0] != "Windows":
            self.uid = os.getuid()
        else:
            self.uid = 0
        self.change_user_to = self._sysconf.instance_config.default_change_user_to
        if self._sysconf.instance_config.default_expected_user is not None:
            self.uid = self._sysconf.instance_config.default_expected_user
            if self.uid.isdigit():
                self.uid = int(self.uid)
        self.fqdns = {}
        self.daemon_private_key_file = None
        self.daemon_public_key_file = None
        self.process_start_rate_limit_last = None

    def add_aliases(self):
        if not self.lookup_aliases:
            return
        # fill some aliases
        self.aliases.add(self.name)
        if self.name in ("localhost", "127.0.0.1"):
            self.aliases.add(CommunicationUtils.try_to_get_fqdn(socket.gethostname()))
        self.aliases.add(CommunicationUtils.try_to_get_fqdn(self.name))

    def try_process_start(self):
        """
        returns True if process start is allowed,
        if rate_limit is exhaustet it will return the number of seconds to wait
        """
        if self.process_start_rate_limit is None:
            return True
        now = time.time()
        if self.process_start_rate_limit_last is None:
            self.process_start_rate_limit_last = now
            return True
        since_last = now - self.process_start_rate_limit_last
        min_needed = 1. / self.process_start_rate_limit
        if since_last < min_needed:
            return min_needed - since_last
        self.process_start_rate_limit_last = now
        return True

    def add_interface(self, interface_spec, is_loopback=False):
        interface_spec = interface_spec.strip()
        for attr in self._attrs:
            if interface_spec.startswith("%s:" % attr):
                value = interface_spec.split(":", 1)[1].strip()                
                if attr in ("expected_user_id", "expected_user"):
                    self.uid = value
                    if self.uid.isdigit():
                        self.uid = int(self.uid)
                    return
                if attr == "filesystems":
                    self.filesystems.extend([ part.strip() for part in value.split(",") ])
                    return
                if attr == "default_change_user_to":
                    self.change_user_to = value
                    return
                if attr == "arbiter_port":
                    value = int(value)
                if attr == "process_start_rate_limit":
                    value = float(value)
                if attr in ("lookup_aliases", "skip_fw_test"):
                    value = value.lower()[:1] in ("y", "1", "t")
                
                setattr(self, attr, value)
                return
            
        if interface_spec.startswith("add_process_default_template:"):
            self.process_default_templates.append(interface_spec.split(":", 1)[1].strip())
            return
        
        if self.has_interface(interface_spec):
            raise Exception("host %r already has an interface %s. can not add %r" % (self, self.get_interface(interface_spec), interface_spec))
        
        intf = HostInterface(self, interface_spec, is_loopback=is_loopback)
        self.interfaces.append(intf)
        return intf

    def get_interface(self, interface_spec):
        for interface in self.interfaces:
            if interface.matches(interface_spec):
                return interface
        raise Exception("host %s has no interface %r!" % (self, interface_spec))

    def has_interface(self, interface_spec):
        try:
            self.get_interface(interface_spec)
        except Exception:
            return False
        return True

    def has_alias(self, alias, check_interfaces=True):
        if self.name == alias:
            return True
        if alias in self.aliases:
            return True
        if check_interfaces and self.has_interface(alias):
            return True
        return False

    def __str__(self):
        return self.name

    def get_networks(self):
        networks = set()
        for interface in self.interfaces:
            if interface.network:
                networks.add(interface.network)
        return networks

    def get_common_networks(self, other_host):
        common_networks = []
        my_networks = self.get_networks()
        other_networks = other_host.get_networks()
        return my_networks.intersection(other_networks)

    def is_direct_reachable(self, other_host):
        if self == other_host:
            return True
        common_networks = self.get_common_networks(other_host)
        if not common_networks:
            return False
        return True

    def try_to_get_fqdn_from(self, other_host):
        """
        first get IP of other host
        if self == manager.host this method is called from localhost -> try to resolve a name for this ip, otherwise give ip
        """
        ip_address = self.get_ip_from(other_host)
        if self != self._sysconf.manager.host:
            # method not called for manager-host - no name resolving from remote POV possible... return ip
            return ip_address
        fqdn = other_host.fqdns.get(self)
        if fqdn is not None:
            return fqdn
        fqdn = CommunicationUtils.try_to_get_fqdn(ip_address)
        other_host.fqdns[self] = fqdn
        return fqdn

    def get_ip_from(self, other_host, network=None, avoid_alternate_socket=False):
        """
        return ip of other_host which is in one of our networks
        """
        intf = self.get_interface_from(other_host, network=network, avoid_alternate_socket=avoid_alternate_socket)
        return intf.ip_address

    def get_interface_from(self, other_host, network=None, avoid_alternate_socket=False):
        """
        return networ-interface of other_host which is in one of our networks
        """
        if self == other_host:
            # prefer loopbacks!
            for intf in self.interfaces:
                if intf.is_loopback:
                    return intf
            return self.interfaces[0]
        common_networks = self.get_common_networks(other_host)
        if network and network not in common_networks:
            raise Exception("%s: requested to get ip of %s in network %s which is not in the list of common networks!" % (
                self, other_host, network))
        elif network:
            common_networks = [network]
        if not common_networks:
            text = []
            def add(host):
                text.append("%s networks:\n  %s\n" % (
                    host, 
                    "\n  ".join(map(str, host.get_networks()))))
            add(self)
            add(other_host)
            raise Exception("host %s and host %s have no common networks!\n%s" % (self, other_host, "\n".join(text)))
        if avoid_alternate_socket:
            for net in common_networks:
                if not net.get_interface_for_host(self).alternate_socket:
                    return net.get_interface_for_host(other_host)
            raise Exception("host %s and host %s have only common networks via alternate_sockets! but avoid_alternate_socket is set to True!" % (
                self, other_host))
        network = list(common_networks)[0] # todo: lowest cost?
        return network.get_interface_for_host(other_host)

    def whois(self, ip_address):
        for network in self.get_networks():
            for interface in network.interfaces:
                if interface.ip_address == ip_address:
                    return interface
        raise Exception("unknown ip_address %r" % ip_address)

    def search_route_to(self, other_host, for_topic=None):
        debug = self._sysconf.debug
        if not len(other_host.get_networks()):
            raise Exception(("host %s has no connected networks!\n"
                             "check your config file whether you need to add one of this hosts\n"
                             "interfaces to the list of 'member_interfaces:' of some network-section!") % other_host)
        networks = [] # reached_via, network
        net_map = {} # network -> networks_i

        hosts_seen = set()
        checked_networks = set()

        def cheaper(old_rv, new_rv):
            # is new_rv cheaper than old_rv?
            return len(new_rv) < len(old_rv)

        network = None
        local_networks_to_check = set()
        
        for intf in self.interfaces:
            if network is None:
                # first network
                current_reached_via = [intf]
                network = current_reached_via[0].network
            if intf.network is not None:
                local_networks_to_check.add(intf.network)
        if network is None:
            raise Exception("host %s has no networks!" % self)

        def get_nis(nis):
            return [(ni.host.name, ni.name) for ni in nis]
        
        def add_networks_for_host(host):
            hosts_seen.add(host)
            def compare_interfaces(ia, ib):
                """
                compare two interfaces by their network
                """
                na, nb = ia.network, ib.network
                if na == nb:
                    return cmp(ia.name, ib.name)
                if na is None:
                    return 1
                if nb is None:
                    return -1
                return cmp(na.name, nb.name)
            host_interfaces = sorted(host.interfaces, key=functools.cmp_to_key(compare_interfaces))
            for interface in host_interfaces:
                if interface.network is None:
                    continue
                reached_via = list(current_reached_via)
                if reached_via[-1].host == interface.host:
                    del reached_via[-1]
                reached_via.append(interface)
                if interface.network not in net_map:
                    net_map[interface.network] = len(networks)
                    #debug("  append new network %s with reached_via: %r" % (interface.network.name, get_nis(reached_via)))
                    networks.append([reached_via, interface.network])
                else:
                    i = net_map[interface.network]
                    other_reached_via = networks[i][0]
                    if cheaper(other_reached_via, reached_via):
                        #debug("  found cheaper way to network %s with reached_via: %r" % (interface.network.name, get_nis(reached_via)))
                        networks[i][0] = reached_via

        msgs = []
        def msg_debug(msg):
            #debug(msg)
            msgs.append(msg)
            
        routes = []
        network_i = 0
        while True:
            # iterate hosts in this network
            hosts = []
            msg_debug("  via %s: network %s" % (
                ", ".join(["%s.%s" % (intf.host, intf.name) for intf in current_reached_via]),
                network.name)
            )
            if network is not None:
                if network in local_networks_to_check:
                    local_networks_to_check.remove(network)
                    msg_debug("   checked local network %s, %d left to check" % (network.name, len(local_networks_to_check)))
                
                for interface in network.interfaces:
                    host = interface.host
                    if host == other_host:
                        msg_debug("  -> found other host %s!" % other_host)
                        route = list(current_reached_via)
                        if len(route) > 1 and route[0].host == route[1].host: # in case we need to take another interface at start-host!
                            del route[0]
                        if len(route) and route[-1].host == interface.host:
                            del route[-1]
                        route.append(interface)
                        routes.append(route)
                    if host in hosts_seen:
                        continue
                    if host not in hosts:
                        #debug("add host %s" % host)
                        hosts.append(host)
            if routes and len(local_networks_to_check) == 0:
                break

            if len(local_networks_to_check) and self not in hosts_seen:
                add_networks_for_host(self)
            for host in hosts:
                add_networks_for_host(host)
            checked_networks.add(network)
            # next network!
            while True:
                current_reached_via, network = networks[network_i]
                #debug("new current_reached_via: %s" % get_nis(current_reached_via))
                if network not in checked_networks:
                    break
                network_i += 1
                if network_i == len(networks):
                    break
            if network_i == len(networks):
                debug("\n".join(msgs))
                raise Exception("no route from %s to %s!" % (self, other_host))
                #break
        debug("\n".join(msgs))
        shortest = None
        msg = []
        for i, route in enumerate(routes):
            if not shortest or len(shortest[0]) > len(route):
                shortest = [route]
            elif len(shortest[0]) == len(route):
                shortest.append(route)
            msg.append("  route %d: len %d" % (i, len(shortest[0])))
            for hop, intf in enumerate(route):
                msg.append("   %2d. via %s/%s to network %s" % (
                    hop, intf.host, intf.name, intf.network.name))
        debug("found %d routes:\n%s" % (len(routes), "\n".join(msg)))
        if len(routes) > 1:
            debug("found %d equally long shortest routes:" % len(shortest))
        if for_topic is None or len(routes) == 1:
            debug("returning first route")
            return shortest[0]
        def get_score(route):
            s = 0
            for intf0, intf1 in zip(route[:-1], route[1:]):
                preferred_network = for_topic.get_preferred_network_between(intf0.host, intf1.host)
                if intf0.network == preferred_network:
                    s += 1
            return s
        shortest.sort(key=get_score)
        # return route with highest score!
        best = shortest[-1]
        msg = []
        for hop, intf in enumerate(best):
            msg.append("   %2d. via %s/%s to network %s" % (
                hop, intf.host, intf.name, intf.network.name))
        debug("for topic %s best scored route is:\n%s" % (for_topic, "\n".join(msg)))
        return best

    def add_interfaces_to_matching_networks(self, networks):
        for intf in self.interfaces:
            if intf.network is not None:
                continue
            candidate_networks = []
            for netname, network in networks.items():
                if self.name in network.excludes:
                    continue
                if intf in network.interfaces:
                    continue
                if intf in network.excludes:
                    continue
                # for interfaces not already in a network: check masks
                netmask = network.has_matching_netmask(intf)
                if netmask is None:
                    continue
                mask, bits = netmask
                candidate_networks.append((bits, mask, network))
            if not candidate_networks:
                continue
            candidate_networks.sort()
            bits, mask, network = candidate_networks[-1] # select highest matching number of bits
            self._sysconf.info("network %r with netmask %s/%d matches host-interface %r" % (network.name, mask, bits, intf.get_full_name()))
            network.add_host_interface(intf)
            if len(candidate_networks) > 1:
                self._sysconf.debug("  other candidate networks:")
                for bits, mask, network in candidate_networks[:-1]:
                    self._sysconf.debug("    network %r with netmask %s/%d also matches" % (self.name, mask, bits))

    def get_ssh_args_string(self):
        if not self.uid or type(self.uid) in (int, int) or self.uid.isdigit():
            # numery user id specified for host. not possible to pass to ssh
            login_name = ""
        else:
            login_name = "-l '%s' " % self.uid
        
        additional_ssh_arguments = self._sysconf.instance_config.additional_ssh_arguments
        if self.additional_ssh_arguments:
            additional_ssh_arguments += " %s" % self.additional_ssh_arguments

        ssh_args = "-T -v -a -x -o SetEnv=LANG=C.UTF-8 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o PasswordAuthentication=no -o PreferredAuthentications=gssapi-with-mic,publickey -K"
        return login_name + ssh_args + additional_ssh_arguments
    
    def get_ssh_args(self):
        s = self.get_ssh_args_string()
        ssh_args = []
        import shlex
        args = shlex.split(s)
        skip = 0
        for i, arg in enumerate(args):
            if skip:
                skip -= 1
                continue
            if arg == "-l":
                user = args[i + 1].strip()
                if user[0] == "'":
                    user = user[1:-1]
                ssh_args.append(("User", user))
                skip = 1
            elif arg == "-o":
                skip = 1
                name, value = args[i + 1].split("=", 1)
                ssh_args.append((name, value))
                skip = 1
            elif arg == "-p":
                port = args[i + 1].strip()
                ssh_args.append(("Port", port))
                skip = 1
            elif arg == "-i":
                identity_file = args[i + 1].strip()
                ssh_args.append(("IdentityFile", identity_file))
                skip = 1
            elif arg == "-T": ssh_args.append(("RequestTTY", "no"))
            elif arg == "-a": ssh_args.append(("ForwardAgent", "no")) # todo: remove!
            elif arg == "-x": ssh_args.append(("ForwardX11", "no"))
            elif arg == "-X": ssh_args.append(("ForwardX11", "yes"))
            elif arg == "-v": ssh_args.append(("LogLevel", "VERBOSE"))
            elif arg == "-K": ssh_args.append(("GSSAPIAuthentication", "yes"))
            elif arg == "-k": ssh_args.append(("GSSAPIAuthentication", "no"))
            else: ssh_args.append(arg)
        return ssh_args
    
                
class Network(configuration_object):
    def __init__(self, sysconf, name, start_line=None):
        self._sysconf = sysconf
        self.name = name

        # config fields
        self.member_interfaces = []
        self.tcp_tunnel_host = None
        self.tcp_tunnel_entry = None
        self.tcp_tunnel_depends_on = None
        self.preferred_gateways = []
        # config flags
        self.interfaces = []
        self.netmasks = []
        self.excludes = set()

        self._valid_config_keys = "node, tcp_tunnel_host, tcp_tunnel_entry, tcp_tunnel_depends_on".split(", ")
        self._valid_config_list_keys = "member_interfaces, preferred_gateways".split(", ")
        self._finalized = False
        configuration_object.__init__(self, start_line=start_line)

    def __lt__(self, other):
        return self.name < other.name

    def _mask_matches(self, mask, bits, ip):
        def get_num_ip(ip, down_shift=0):
            num_ip = 0
            for word in ip.split("."):
                num_ip = num_ip << 8
                num_ip |= int(word)
            #print "ip %r has num_up 0x%08x" % (ip, num_ip)
            if down_shift:
                num_ip = num_ip >> down_shift
                #print "downshift %d results in 0x%08x" % (down_shift, num_ip)
            return num_ip
        down_shift = 4 * 8 - bits
        num_mask = get_num_ip(mask, down_shift)
        num_ip = get_num_ip(ip, down_shift)
        return num_mask == num_ip

    def has_matching_netmask(self, intf_or_ip):
        if type(intf_or_ip) == HostInterface:
            intf = intf_or_ip
            ip = intf.ip_address
        else:
            intf = None
            ip = intf_or_ip
        for mask, bits in self.netmasks:
            if self._mask_matches(mask, bits, ip):
                #print "netmask", mask, bits, "matches ip", ip
                return mask, bits

    def add_host_interface(self, interface):
        if interface.network is not None:
            raise Exception("interface %s is already in network %s. can not be added to network %s" % (
                    interface, interface.network, self))
        interface.network = self
        self.interfaces.append(interface)

    def _finalize_config(self):
        if self._finalized:
            return

        for l in self._valid_config_list_keys:
            if not hasattr(self, l):
                setattr(self, l, [])

        if self.tcp_tunnel_entry and not self.tcp_tunnel_host:
            self.tcp_tunnel_host = "127.0.0.1" # assume tunnel host is manager host

        if self.tcp_tunnel_entry and self.tcp_tunnel_depends_on:
            self.tcp_tunnel_depends_on = self._finish_depends(self.tcp_tunnel_depends_on)

        for spec in self.member_interfaces:
            if "/" in spec:
                ip, bits = spec.split("/", 1)
                if "." in ip and ip.replace(".", "").isdigit() and bits.isdigit():
                    # assume netmask!
                    netmask = ip, int(bits)
                    if netmask not in self.netmasks:
                        self.netmasks.append(netmask)
                    continue
            is_exclude = spec.startswith("!")
            if is_exclude:
                spec = spec[1:]
            # search host with matching interface spec
            try:
                interface = HostInterface.find_interface(self._sysconf, spec, create=True)
            except Exception:
                if not is_exclude:
                    raise
                self.excludes.add(spec)
                continue
            if not is_exclude:
                self.add_host_interface(interface)
            else:
                self.excludes.add(interface)

        if not self.preferred_gateways:
            self.interfaces.sort(key=lambda ni: (ni.host.name, ni.name))
        else:
            network_interfaces = []
            no_prio = []
            for interface in self.interfaces:
                try:
                    prio = self.preferred_gateways.index(interface.host.name)
                except Exception:
                    no_prio.append(interface)
                    continue
                network_interfaces.append((prio, interface))
            network_interfaces.sort(key=lambda ni: ni[0]) # sort by prio
            network_interfaces = [i for p, i in network_interfaces] # remove prio
            no_prio.sort(key=lambda ni: (ni.host.name, ni.name))
            network_interfaces.extend(no_prio)
            self.interfaces = network_interfaces

    def has_host(self, host):
        for interface in self.interfaces:
            if interface.host == host:
                return True
        return False

    def get_interface_for_host(self, host):
        for interface in self.interfaces:
            if interface.host == host:
                return interface
        raise Exception("host %s has no interface in network %s!" % (host, self))

    def get_ip_for_host(self, host):
        interface = self.get_interface_for_host(host)
        return interface.ip_address

    def __str__(self):
        if self.netmasks:
            netmasks = " netmasks: %s" % (", ".join([("%s/%d" % (ip, bits)) for ip, bits in self.netmasks]), )
        else:
            netmasks = ""
        if self.excludes:
            excludes = " excludes: %s" % (", ".join(map(str, self.excludes)))
        else:
            excludes = ""
        return "<network %r@%x interfaces: %s%s%s>" % (
            self.name,
            id(self),
            ", ".join([intf.get_full_name() for intf in self.interfaces]), netmasks, excludes)


class TopicSetting(configuration_object):
    def __init__(self, sysconf, name, start_line=None):
        self._finalized = False
        self._sysconf = sysconf
        self.name = name
        
        self._scheduling_on = {}
        self._valid_config_keys = "scheduling_on".split(", ")
        self._valid_config_list_keys = []
        configuration_object.__init__(self, start_line=start_line)

    def _read_config(self, key, value):
        self._scheduling_on[self._sysconf.eval_string(key)] = self._sysconf.eval_string(value)

    def _finalize_config(self):
        if self._finalized:
            return
        for sched_name in "FIFO,RR,NORMAL,IDLE,SPORADIC,OTHER,BATCH".split(","):
            locals()[sched_name] = sched_name
        flags = "offer_endianess_swap", "deny_endianess_swap"
        settings = {}
        for selector, kwargs in self._scheduling_on.items():
            kwargs = [ part.strip() for part in kwargs.split(",") ]
            args = {}
            for kw in kwargs:
                if "=" in kw and ":" in kw:
                    if kw.index("=") < kw.index(":"):
                        k, v = kw.split("=", 1)
                    else:
                        k, v = kw.split(":", 1)
                elif "=" in kw:
                    k, v = kw.split("=", 1)
                elif ":" in kw:
                    k, v = kw.split(":", 1)
                elif kw.lower() in flags:
                    k, v = kw.lower(), True
                else:
                    raise Exception("invalid topic setting %r!" % kw)
                if k == "priority":
                    k = "prio"
                args[k] = v
            kwargs = args
            if "affinity" in kwargs:
                kwargs["cpu_mask"] = CommunicationUtils.get_cpu_affinity_mask(kwargs["affinity"])
                del kwargs["affinity"]
            
            host = self._sysconf.get_host_for_node(selector, create=True) # assume selector is node-name
            if "preferred_network" in kwargs:
                if kwargs["preferred_network"] not in self._sysconf.networks:
                    raise Exception("topic %r onr host %r prefers unknown network %r" % (
                        self.name, host, kwargs["preferred_network"]))
                net = self._sysconf.networks[kwargs["preferred_network"]]
                if net not in host.get_networks():
                    raise Exception("topic %r on host %r prefers network %r which is not reachable by that host!" % (
                        self.name, host, net))
                kwargs["preferred_network"] = net
            if kwargs.get("offer_endianess_swap") and kwargs.get("deny_endianess_swap"):
                raise Exception("topic %r on host %r can not at the same time offer and deny endianess swap!" % (
                        self.name, host))
            settings[host] = kwargs
        self._finalized = True
        self.scheduling_on = settings

class SystemConfiguration(object):
    # pickling:
    to_pickle = "processes, configuration_templates, states, groups, defines, gui_plugins, topic_settings, networks, node_map, hosts, notebook_processes, scope_processes, configuration_file, instance_config, start_tools"
    def __getstate__(self):
        state = {}
        if debug_getstate: print("debug_getstate: %s" % (self.__class__.__name__))
        for attr in self.to_pickle.split(","):
            attr = attr.strip()
            state[attr] = getattr(self, attr)
            if debug_getstate: print("  %s <- %s" % (attr, type(state[attr])))
        return state
    def __setstate__(self, state):
        self._init()
        self.__dict__.update(state)
    def _init(self):
        Logging.enable_logging_on_instance(self, "SystemConfiguration")
        
    def __init__(self, manager, only_export_hosts=False):
        self._init()
        self.only_export_hosts = only_export_hosts
        self.manager = manager

        self.instance_config = None
        self.scope_processes = {}
        self.notebook_processes = {}
        self.hosts = {} # map from hostname to Host
        self.node_map = {}
        self.networks = {}
        self.additional_message_definition_dirs = []

        self.processes = {}
        self.configuration_templates = {}
        self.states = {}
        self.groups = {}
        self.defines = dict(empty="")
        self.topic_settings = {}
        self.gui_plugins = {}
        self.plugins = {}
        self.providers = {} # service -> [(prio, provider_obj), ...]

        self.defines["hostname"] = socket.gethostname()
        warned_about_missing_defines.clear()

        self.start_tools = [
            ("without any tool", None),
            ("gdb", "/usr/bin/gdb --args"),
            ("valgrind leak check", "/usr/bin/valgrind --leak-check=full"),
            ("strace", "/usr/bin/strace -tt -f"),
            ("strace /tmp/out", "/usr/bin/strace -o /tmp/out -tt -f"),
            ("strace /tmp/out -eopen", "/usr/bin/strace -o /tmp/out -eopen -tt -f"),
            ("ldd", "/usr/bin/ldd"),
            ("time", "/usr/bin/time"),
            ("gdbserver on 2345", "/usr/bin/gdbserver :2345"),
        ]
        self.xhosts_done = set()
        self.checked_nolisten_tcp = False
        self.ignore_hosts_from_old_config = False
        
        # setup string parser for eval_string()
        self.string_parser = ln_string_parser()
        self.string_parser.warning = self.warning
        self.string_parser.keep_defines.append("guess_display")
        def get_define(name):
            if name in self.string_parser.keep_defines:
                return "%%(%s)" % name
            define_value = self.defines.get(name)
            if define_value is not None:
                return define_value
            node_value = self.node_map.get(name)
            if node_value is not None:
                return node_value
            if name not in warned_about_missing_defines:
                warned_about_missing_defines.add(name)
                self.warning("there is no define with name %r as used in %r\n%s\n" % (name, self.string_parser.current_input, self.format_include_stack()))
            #print "keep defines: %s" % (self.string_parser.keep_defines, )
            return ""
        def has_define(name):
            if name in self.string_parser.keep_defines:
                return True
            if name in self.defines:
                return True
            if name in self.node_map:
                return True
            return False
        self.string_parser.get_define = get_define
        self.string_parser.has_define = has_define
        self.string_arg_parser = string_arg_parser()

        def getenv(args):
            args = self.string_arg_parser.parse(args)
            value = os.getenv(*args)
            if value is None:
                raise Exception("env variable %r is not defined!" % args[0])
            return value
        def get_ifip(args):
            iface = self.string_arg_parser.parse(args)[0]
            import socket
            import struct
            import fcntl
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sockfd = sock.fileno()
            SIOCGIFADDR = 0x8915
            ifreq = struct.pack('16sH14s', iface, socket.AF_INET, '\x00'*14)
            res = fcntl.ioctl(sockfd, SIOCGIFADDR, ifreq)
            ip = struct.unpack('16sH2x4s8x', res)[2]
            sock.close()
            return socket.inet_ntoa(ip)
        def exec_shell(args):
            args = self.string_arg_parser.parse(args)
            self.debug("execute shell command: %s" % args[0])
            out = CommunicationUtils.exec_cmd(args[0])
            print("\n\nshell command output: %r\n\n" % out)
            return out.rstrip("\n") # strip trailing newlines
        def exec_realpath(args):
            return os.path.realpath(self.string_arg_parser.parse(args)[0])
        def exec_dirname(args):
            return os.path.dirname(self.string_arg_parser.parse(args)[0])
        def exec_basename(args):
            return os.path.basename(self.string_arg_parser.parse(args)[0])
        def exec_subst(args):
            args = self.string_arg_parser.parse(args)
            search, replace, text = args
            return text.replace(search, replace)
        def exec_substring(args):
            args = self.string_arg_parser.parse(args)
            if len(args) == 3:
                this_str, start, n = args
                start = int(start)
                n = int(n)
            else:
                this_str, start = args
                start = int(start)
                n = len(this_str) - start
            return this_str[start:start + n]
        def exec_length(args):
            args = self.string_arg_parser.parse(args)
            return str(len(args[0]))
        def exec_ifeq(args):
            args = self.string_arg_parser.parse(args)
            text, compare, if_true, if_false = args
            if text == compare:
                return if_true
            return if_false
        def exec_findstring(args):
            args = self.string_arg_parser.parse(args)
            search, text = args
            if search in text:
                return search
            return ""
        def exec_ifndef(args):
            args = self.string_arg_parser.parse(args)
            defname, if_not_defined = args
            if has_define(defname):
                return get_define(defname)
            return if_not_defined
        def exec_ifexists(args):
            args = self.string_arg_parser.parse(args)
            pathname, if_true, if_false = args
            if os.path.exists(pathname):
                return if_true
            return if_false
        def exec_get_host_ifip(args):
            # todo: this can not really work until after hosts are assigned to networks?
            args = self.string_arg_parser.parse(args)
            node_name, ifname = args
            host = self.get_host_for_node(node_name, create=False)
            interface = host.get_interface(ifname)
            return interface.ip_address
        def exec_get_port_from_string(args):
            args = " ".join(self.string_arg_parser.parse(args))
            offset = 1024
            max_port = 65535
            max_range = max_port - offset
            p = 0
            for char in args:
                p += ord(char)
            return str(offset + p % max_range)
        def exec_get_host_ip_reachable_from_host(args):
            # todo: does not work, because host<->network assignments are done after reading config file...
            args = self.string_arg_parser.parse(args)
            pov_node, node = args
            pov_host = self.get_host_for_node(pov_node, create=False)
            host = self.get_host_for_node(node, create=False)
            if not host.is_direct_reachable(pov_host):
                raise Exception("%r is not directly rechable from %r!" % (pov_node, node))
            return host.get_ip_from(pov_host)
            
        self.string_parser.functions = dict(
            env=getenv,
            ifip=get_ifip,
            shell=exec_shell,
            realpath=exec_realpath,
            dirname=exec_dirname,
            basename=exec_basename,
            subst=exec_subst,
            substring=exec_substring,
            length=exec_length,
            strip=str.strip,
            findstring=exec_findstring,
            ifndef=exec_ifndef,
            ifexists=exec_ifexists,
            ifeq=exec_ifeq,
            get_host_ifip=exec_get_host_ifip,
            get_port_from_string=exec_get_port_from_string,
            get_host_ip_reachable_from_host=exec_get_host_ip_reachable_from_host,
        )
        self.include_stack = []
        try:
            self._read_configuration()
        except SystemExit:
            raise
        except Exception:
            raise Exception("error reading configuration:\n%s\n%s" % (traceback.format_exc(), self.format_include_stack())) # todo: LN3: from None
        CommunicationUtils.sysconfig = self

    def add_provider(self, what, prio, obj):
        if what not in self.providers:
            self.providers[what] = []
        self.providers[what].append((prio, obj))
        
    def xhost(self, remote_ip):
        if remote_ip in self.xhosts_done:
            return
        self.info("try to xhost + for remote_ip %r" % remote_ip)
        self.xhosts_done.add(remote_ip)
        pyutils.system("xhost +inet:%s" % remote_ip)
        if not self.checked_nolisten_tcp:
            self.checked_nolisten_tcp = True
            cmd = "LANG=C ps -Afl | grep X | grep -v grep"
            out = CommunicationUtils.exec_cmd(cmd)
            search = "-nolisten tcp"
            lines = []
            for line in out.split("\n"):
                if search in line:
                    lines.append(line.strip())
            if lines:
                self.warning("your x-server seems to be running with %r:\n"
                             "%s\n"
                             "but there is atleast one process on %s who wants to use this x-server!\n"
                             "if you still want to do this:\n"
                             "  you should add 'DisallowTCP=false' to the '[security]' section of your /etc/gdm3/daemon.conf\n"
                             "  or remove '-nolisten tcp' from your /etc/X11/xinit/xserverrc\n" % (
                    search,
                    "\n".join(lines),
                    remote_ip))
        
    def print_out(self):
        self.debug("hosts:\n%s" % pprint.pformat(self.hosts))
        self.debug("networks:\n%s" % pprint.pformat(self.networks))
        self.debug("node_map:\n%s" % pprint.pformat(self.node_map))
        self.debug("processes:\n%s" % pprint.pformat(self.processes))
        self.debug("groups:\n%s" % pprint.pformat(self.groups))

    def eval_deffun(self, name, line):
        args = line.split(" ", 1)[1].strip()
        if args[-1] != ")":
            raise Exception("deffun is missing closing ): %r" % line)
        args = args[:-1]
        arg_names, body = self.string_arg_parser.parse(args)
        arg_names = arg_names.split(",")
        def fun(args):
            args = self.string_arg_parser.parse(args)
            args = list(zip(arg_names, args))
            self.debug("call function %s(%s)" % (name, ", ".join(["%s=%s" % kv for kv in args])))
            return self.eval_string(body, add=args)
        self.string_parser.functions[name] = fun
        
    def eval_string(self, str, skip_list=None, add=None):
        if not str:
            return str
        if skip_list is None:
            skip_list = []
        self.defines["manager"] = self.instance_config.manager
        self.defines["instance_name"] = self.instance_config.name
        if not hasattr(self, "last_current_fn") or self.last_current_fn != self.current_fn:
            self.last_current_fn = self.current_fn
            self.defines["LN_LD_LIBRARY_PATH"] = os.path.pathsep.join(ln.ld_library_path)
            self.defines["LN_PYTHONPATH"] = os.path.pathsep.join(ln.pythonpath)
            self.defines["LN_PYTHON"] = sys.executable
            cfg_fn = os.path.realpath(self.current_fn)
            self.defines["CURRENT"] = cfg_fn
            self.defines["CURDIR"] = os.path.dirname(cfg_fn)
            self.defines["LN_PREFIX"] = config.prefix
            self.defines["LN_SHARE_DIR"] = ln_base.ln_share_dir
            self.defines["LN_BASE_MANAGER"] = config.manager_base_dir
        if add:
            self.defines.update(add)
        try:
            self.had_eval_string_error = False
            return self.string_parser.eval(str, skip_list=skip_list)
        except Exception:
            self.had_eval_string_error = True
            self.warning("failed to eval string, using verbatim string instead! fix string or notify flo!\ninput:\n%r\nerror:\n%s\n%s" % (
                str,
                traceback.format_exc(),
                self.format_include_stack()
            ))
            return str

    def _read_configuration(self):
        self.instance_config = InstanceConfig(self)
        if config.configuration_file is None:
            self.manager._usage()
            sys.exit(0)
        self.configuration_file = os.path.realpath(config.configuration_file)
        self.included_files = []
        self.process_default_templates = []
        #a = time.time()
        self._read_file(config.configuration_file)
        #print "finished reading file after %.1fs" % (time.time() - a)
        #sys.exit(0)

        self.instance_config._finalize_config()

        #self.manager.host = self.get_host(self.instance_config.manager_host, create=True)
        try:
            self.manager_host = self.get_host(self.instance_config.manager_host, create=False)
        except Exception:
            self.manager_host = None

        manager_host_without_specified_interfaces = self.manager_host and len(self.manager_host.interfaces) == 1 and self.manager_host.interfaces[0].name == "intf0" and self.manager_host.interfaces[0].netmask is None and self.manager_host.interfaces[0].network is None
        if manager_host_without_specified_interfaces:
            # remove that interface
            self.debug("manager host %r has no interfaces mentioned in hosts section. adding all found local interfaces" % self.instance_config.manager_host)
            del self.manager_host.interfaces[0]
            mh = self.manager_host

        if self.manager_host is None:
            self.debug("manager host %r is not mentioned in hosts section. creating with all found local interfaces" % self.instance_config.manager_host)
            if self.instance_config.manager_host == "localhost":
                # try to find better name than localhost
                for method in (socket.gethostname, lambda: os.uname()[1], platform.node, lambda: os.getenv("HOSTNAME", "localhost")):
                    value = method()
                    if value != "localhost":
                        self.instance_config.manager_host = value
                        self.info("using manager hostname %r instead of localhost!" % value)
                        break
            mh = self.hosts[self.instance_config.manager_host] = Host(self, self.instance_config.manager_host)

        if self.manager_host is None or manager_host_without_specified_interfaces:
            self.manager_host = mh
            # now try it iterate local interfaces
            cmd = "/sbin/ip addr list"
            out = CommunicationUtils.exec_cmd(cmd)
            interface_name = None
            for line in out.split("\n"):
                if line and line[0].isdigit():
                    interface_name = None
                if interface_name is None:
                    if not (line and line[0].isdigit()):
                        continue
                    if "POINTOPOINT" in line:
                        # ignore point to point interfaces for now
                        continue
                    interface_name = line.split(": ", 2)[1]
                    updown = "state DOWN" not in line
                    is_loopback = "LOOPBACK" in line
                    continue
                line = line.strip()
                #print "iface %r: %r" % (interface_name, line)
                if line.startswith("inet "):
                    interface_address = line.split("inet ", 1)[1].split(" ", 1)[0]
                    if " peer " in line:
                        interface_address += "/32" # netmasks won't work with peer-to-peer interfaces
                    try:
                        interface_address, interface_mask = interface_address.split("/", 1)
                    except Exception:
                        self.warning("ignore interface %s because of unparsable ip/netmask: %r" % (
                            interface_name, interface_address))
                        continue
                    if updown:
                        self.debug("  found manager interface %r up at %r" % (interface_name, interface_address))
                        interface = mh.add_interface("%s:%s" % (interface_name, interface_address), is_loopback=is_loopback)
                        interface.netmask = int(interface_mask)
        self.manager_host.aliases.add("localhost")
        try:
            self.manager_host.aliases.add(socket.gethostname())
        except Exception:
            pass
        if "manager" not in self.manager_host.filesystems:
            self.manager_host.filesystems.insert(0, "manager")
        self.info("manager host: %s" % self.manager_host)

        self.auto_networks = False
        if not self.networks:
            self.debug("no networks defined. adding one network per manager interface!")
            self.auto_networks = True
            if self.instance_config.manager_host == "localhost":
                mhn = "manager"
            else:
                mhn = self.instance_config.manager_host
            network_ips = {}
            for interface in self.manager_host.interfaces:
                if interface.netmask is None:
                    self.debug("  interface %r has no netmask, skipping...." % interface.name)
                    continue
                net_name = "%s_%s" % (mhn, interface.name)
                network_ip = CommunicationUtils.get_network_ip(interface.ip_address, interface.netmask)
                if network_ip in network_ips:
                    net = network_ips[network_ip] 
                    net.add_host_interface(interface)
                    continue
                net = self.networks[net_name] = network_ips[network_ip] = Network(self, net_name, start_line=("source", 0))                
                netmask = network_ip, interface.netmask
                net.netmasks.append(netmask)
                self.debug("  created network %r at %s/%d" % (net_name, network_ip, interface.netmask))
                net.add_host_interface(interface)
                # add additional routes as well
                cmd = "/bin/ip route list dev %s" % interface.name
                out, err = CommunicationUtils.exec_cmd(cmd, shell=True, exc_on_error=False)
                for line in out.strip().split("\n"):
                    if not line.strip():
                        continue
                    line = re.split("[ \t]+", line.strip())
                    if line[0] == "default":
                        ip, bits = "0.0.0.0", 0
                    elif "/" in line[0]:
                        ip, bits = line[0].split("/")
                        bits = int(bits)
                    else: # assume host route
                        ip = line[0].strip()
                        bits = 32
                    netmask = ip, bits
                    if netmask in net.netmasks:
                        continue
                    self.debug("    adding netmask %s/%d from routing table for this interface" % netmask)
                    net.netmasks.append(netmask)

        for pn, p in self.processes.items():
            try:
                p._finalize_config()
            except Exception:
                raise Exception("error finalizing config of process %r:\n%s" % (pn, traceback.format_exc()))
        self.assigned_states = {}
        for pn, p in self.states.items():
            try:
                p._finalize_config() # will write to self.assigned_state
            except Exception:
                raise Exception("error finalizing config of state %r:\n%s" % (pn, traceback.format_exc()))
        self.states = self.assigned_states

        if self.instance_config.enable_auto_groups:
            # collect groups from process & state names
            n_list = []
            for pn, p in self.processes.items():
                n_list.append((p._config_id, pn, p))
            for pn, p in self.states.items():
                n_list.append((p._config_id, pn, p))
            for pn, p in self.groups.items():
                n_list.append((p._config_id, pn + "/", p))
            n_list.sort()
            auto_groups = {}
            for config_id, name, obj in n_list:
                if "/" not in name:
                    continue
                parts = name.split("/")
                parent = None
                for i, part in enumerate(parts[:-1]):
                    gn = "/".join(parts[:i+1])
                    if gn not in auto_groups:
                        auto_groups[gn] = (config_id, [], obj)
                    if parent:
                        for k, (cid, egn, pobj) in enumerate(auto_groups[parent][1]):
                            if egn == gn:
                                if cid > config_id:
                                    auto_groups[parent][1][k][0] = config_id
                                    auto_groups[parent][1][k][2] = obj
                                break
                        else:
                            auto_groups[parent][1].append((config_id, gn, obj))
                    parent = gn
                auto_groups[gn][1].append((config_id, name, obj))
            #pprint.pprint(auto_groups)
            groups = list(auto_groups.items())
            groups.sort(key=lambda item: item[1][0])
            for group_name, (config_id, childs, first_obj) in groups:
                #print group_name, childs
                #childs.sort(key=lambda item: item[1][0])
                grp = self.groups.get(group_name)
                if grp is None:
                    grp = self.groups[group_name] = Group(self, group_name, start_line=first_obj._sysconf_start_line, cfg_stack=first_obj._sysconf_stack)
                if "/" in group_name:
                    grp.display_name = group_name.rsplit("/", 1)[1]
                grp.auto_members.extend(childs)

        for pn, p in self.groups.items():
            try:
                p._finalize_config()
            except Exception:
                raise Exception("error finalizing config of group %r:\n%s" % (pn, traceback.format_exc()))

        for nn, n in self.networks.items():
            try:
                n._finalize_config()
            except Exception:
                raise Exception("error finalizing config of network %r:\n%s" % (nn, traceback.format_exc()))
        
        if not self.networks:
            self.debug("no networks defined. adding default network with all hosts included")
            net = self.networks["default"] = Network(self, "default", start_line=("source", 0))
            net.member_interfaces = []
            for hname, host in self.hosts.items():
                self.debug("add interface %s to network %s\n" % (host.interfaces[0].get_full_name(), net.name))
                net.member_interfaces.append(host.interfaces[0].get_full_name())
            try:
                net._finalize_config()
            except Exception:
                raise Exception("error finalizing config of default-network %r:\n%s" % (
                        net, traceback.format_exc()))

        # let defined networks search for hosts-interfaces matching their netmasks
        for host in list(self.hosts.values()):
            host.add_interfaces_to_matching_networks(self.networks)
        # search for unreachable hosts
        had_hosts_without_networks = False
        for hostname, host in self.hosts.items():
            networks = set()
            for intf in host.interfaces:
                if intf.network is not None:
                    networks.add(intf.network)
            if not networks:
                self.warning("host %s (interfaces: %s) is not reachable from any network!" % (
                    host, 
                    ", ".join(["%s/%s" % (intf.name, intf.ip_address) for intf in host.interfaces])))
                had_hosts_without_networks = True
        if had_hosts_without_networks:
            self.warning("defined networks:")
            for netname, net in self.networks.items():
                self.warning(" %s" % net)
        # search for hosts with same ip's in same network!
        for netname, net in self.networks.items():
            seen_ips = {}
            for intf in net.interfaces:
                if intf.ip_address in seen_ips:
                    self.error("host %r has interface %s with ip %s in network %s which is also used by host %s interface %s!\n"
                               "this is an error -- they can not have the same ip within the same network!\n"
                               "either define their interfaces to be member of different networks\n"
                               "or if its the same host use only one host name to refer to it!" % (
                        intf.host.name, intf.name, intf.ip_address, net.name, seen_ips[intf.ip_address].host.name, seen_ips[intf.ip_address].name))
                    continue
                seen_ips[intf.ip_address] = intf

        for pn, p in self.topic_settings.items():
            try:
                p._finalize_config()
            except Exception:
                raise Exception("error finalizing config of topic_setting %r:\n%s" % (pn, traceback.format_exc()))

        for pn, p in self.plugins.items():
            try:
                p._finalize_config()
            except Exception:
                raise Exception("error finalizing config of plugin %r:\n%s" % (pn, traceback.format_exc()))
        
        for pn, p in self.gui_plugins.items():
            try:
                p._finalize_config()
            except Exception:
                raise Exception("error finalizing config of gui_plugin %r:\n%s" % (pn, traceback.format_exc()))

        for hname, host in self.hosts.items():
            if not host.preferred_network:
                continue
            if host.preferred_network not in self.networks:
                raise Exception("host %s prefers unknown network %r!" % (host, host.preferred_network))
            host.preferred_network = self.networks[host.preferred_network]

        ln_base.additional_message_definition_dirs = self.additional_message_definition_dirs
        ln_base.rescan_message_definition_dirs()

    def resolve_inc_fn(self, inc_fn, current_dir):
        if inc_fn.startswith(os.sep):
            # absolute path, leave
            return inc_fn
        if inc_fn.startswith("."):
            # relative path, join relative to current dir
            inc_fn = os.path.join(current_dir, inc_fn)
            return inc_fn
        # local path or system include
        local_inc_fn = os.path.join(current_dir, inc_fn)
        if os.path.isfile(local_inc_fn):
            # prefer local file
            return local_inc_fn
        # search system config include dirs
        for dn in config.ln_config_include_dirs:
            test_inc_fn = os.path.join(dn, inc_fn)
            if os.path.isfile(test_inc_fn):
                return test_inc_fn
        return local_inc_fn

    def format_include_stack(self, at=None):
        return "\n".join(["%s:%s" % item for item in self.get_include_stack(at=at)]) # checked
    
    def get_include_stack(self, at=None):
        if at is None:
            try:
                at = self.current_fn, self.current_line
            except Exception:
                return []
        stack = list(self.include_stack)
        if at: stack.append(at)
        stack.reverse()
        return stack
    
    def _read_file(self, fn, fn_contents=None, name_prefix=None, included_from=None):
        if included_from:
            self.include_stack.append(included_from)
            
        self.current_fn = fn
        section_type = current_section = None
        if fn_contents is None:
            ffn = os.path.realpath(fn)
            if ffn not in self.included_files:
                self.included_files.append(ffn)
            with open(fn) as fp:
                lines = list(fp.readlines())
        else:
            lines = fn_contents.split("\n")

        skip = 0
        def check_name(wanted_type, wanted_name, fn, line):
            if wanted_name in self.processes:
                p = self.processes[wanted_name]
            elif wanted_name in self.states:
                p = self.states[wanted_name]
            elif wanted_name in self.groups:
                p = self.groups[wanted_name]
            elif wanted_name in self.configuration_templates:
                p = self.configuration_templates[wanted_name]
            else:
                return
            raise Exception("duplicate name: %s:%d %s %r - already defined as %s at line %r" % (
                    fn, line, wanted_type, current_section, p.__class__.__name__, p._sysconf_start_line))

        if name_prefix:
            local_name_prefixes = name_prefix.split("/")
        else:
            local_name_prefixes = []

        for i, line in enumerate(lines):
            if skip:
                skip -= 1
                continue
            # strip leading/trailing whitespace
            line = line.strip()
            is_pipe_include = False
            # skip comments
            if line.startswith("#") or not line:
                continue
            self.current_line = i + 1
            #print "%s:%d" % (self.current_fn, self.current_line)

            if debug_sections: print("%s:%3d: %s %r" % (self.current_fn, self.current_line, current_section, line))
            
            if line.startswith("push_name_prefix "):
                new_name_prefix = self.eval_string(line.split(" ", 1)[1].strip()).strip("/")
                local_name_prefixes.append(new_name_prefix)
                continue
            if line.startswith("push_name_prefix:"):
                new_name_prefix = self.eval_string(line.split(":", 1)[1].strip()).strip("/")
                local_name_prefixes.append(new_name_prefix)
                continue
            if line.startswith("pop_name_prefix"):
                if local_name_prefixes:
                    local_name_prefixes.pop(-1)
                else:
                    self.warning("%s:%d: pop_name_prefix: there are no name prefixes to pop!" % (self.current_fn, self.current_line))
                continue
            if line.startswith("requires_version"):
                required_version = self.eval_string(line.split(" ", 1)[1].strip())
                if compare_rmpm_version(config.version, required_version) < 0:
                    raise Exception("this ln-config file requires a ln_manager version of atleast %s! (ours: %s)\n%s" % (
                        required_version, config.version, os.path.realpath(self.current_fn)))
                continue
            if line.startswith("warning:"):
                args = self.eval_string(line.split(":", 1)[1].strip())
                self.warning("%s:%d: %s" % (self.current_fn, self.current_line, args))
                continue
            if line.startswith("include_glob") or line.startswith("optional_include_glob"):
                params = shlex.split(self.eval_string(line))
                cmd = params[0].strip()
                patterns = params[1:]
                optional = "optional" in cmd
                self.debug("Searching for includes: %s" % patterns)
                inc_fn_list = tools.glob(os.path.dirname(fn), patterns)
                if not inc_fn_list:
                    error = "no include files found for: %r" % patterns
                    if not optional:
                        raise Exception(error)
                    self.warning(error)
                for inc_fn in inc_fn_list:
                    try:
                        self._read_file(inc_fn, name_prefix="/".join(local_name_prefixes), included_from=(fn, i+1))
                    except Exception:
                        raise Exception("error reading file %r included from %s:%d:\n%s" % (
                            inc_fn, fn, i + 1, traceback.format_exc()))
                    self.current_fn = fn
                continue
            if line.startswith("optional_include"):
                inc_fn = self.eval_string(line).split(" ", 1)[1].strip()
                inc_fn = self.resolve_inc_fn(inc_fn, os.path.dirname(fn))
                if os.path.isfile(inc_fn):
                    try:
                        self._read_file(inc_fn, name_prefix="/".join(local_name_prefixes), included_from=(fn, i+1))
                    except Exception:
                        print("included from %s:%d" % (fn, i + 1))
                        raise
                    self.current_fn = fn
                else:
                    self.debug("optional include file %r does not exist." % inc_fn)
                continue
            if line.startswith("include"):
                inc_fn = self.eval_string(line).split(" ", 1)[1].strip()
                inc_fn = self.resolve_inc_fn(inc_fn, os.path.dirname(fn))
                try:
                    self._read_file(inc_fn, name_prefix="/".join(local_name_prefixes), included_from=(fn, i+1))
                except Exception:
                    raise Exception("error reading file %r included from %s:%d:\n%s" % (
                        inc_fn, fn, i + 1, traceback.format_exc()))
                self.current_fn = fn
                continue
            if line.startswith("pipe_include"):
                is_pipe_include = True
            if line in ("hosts", "node_map", "defines", "custom_start_tools"):
                section_type = current_section = line
                continue
            if line.startswith("instance"):
                section_type = current_section = line
                current = self.instance_config
                continue
            if line.startswith("process "):
                section_type, current_section = line.split(" ", 1)
                current_section = self.eval_string(current_section.strip())
                self.defines["_PROCESS"] = current_section
                if local_name_prefixes:
                    current_section = "%s/%s" % ("/".join(local_name_prefixes), current_section)
                self.defines["_FULL_PROCESS"] = current_section
                check_name("process", current_section, fn, i + 1)
                current = Process(self, current_section, start_line=(fn, i+1), name_prefixes=local_name_prefixes)
                self.processes[current_section] = current
                continue
            if line.startswith("network "):
                section_type, current_section = line.split(" ", 1)
                current_section = self.eval_string(current_section.strip())
                check_name("network", current_section, fn, i + 1)
                current = Network(self, current_section, start_line=(fn, i+1))
                self.networks[current_section] = current
                continue
            if line.startswith("state "):
                section_type, current_section = line.split(" ", 1)
                current_section = self.eval_string(current_section.strip())
                if local_name_prefixes:
                    current_section = "%s/%s" % ("/".join(local_name_prefixes), current_section)
                check_name("state", current_section, fn, i + 1)
                current = State(self, current_section, start_line=(fn, i+1), name_prefixes=local_name_prefixes)
                self.states[current_section] = current
                continue
            if line.startswith("template ") or line.startswith("process_template "):
                section_type, current_section = line.split(" ", 1)
                current_section = self.eval_string(current_section.strip())
                if local_name_prefixes:
                    current_section = "%s/%s" % ("/".join(local_name_prefixes), current_section)
                check_name("template", current_section, fn, i + 1)
                current = ConfigurationTemplate(self, current_section, name_prefixes=local_name_prefixes, start_line=(fn, i+1))
                self.configuration_templates[current.name] = current
                continue
            if line.startswith("group "):
                section_type, current_section = line.split(" ", 1)
                current_section = self.eval_string(current_section.strip())
                if local_name_prefixes:
                    current_section = "%s/%s" % ("/".join(local_name_prefixes), current_section)
                check_name("group", current_section, fn, i + 1)
                current = Group(self, current_section, start_line=(fn, i+1), name_prefixes=local_name_prefixes)
                self.groups[current_section] = current
                continue
            if line.startswith("gui_plugin "):
                section_type, current_section = line.split(" ", 1)
                current_section = self.eval_string(current_section.strip())
                if local_name_prefixes:
                    current_section = "%s/%s" % ("/".join(local_name_prefixes), current_section)
                check_name("gui_plugin", current_section, fn, i + 1)
                current = Plugin(self, current_section, start_line=(fn, i+1))
                self.gui_plugins[current_section] = current
                continue
            if line.startswith("plugin "):
                section_type, current_section = line.split(" ", 1)
                current_section = self.eval_string(current_section.strip())
                if local_name_prefixes:
                    current_section = "%s/%s" % ("/".join(local_name_prefixes), current_section)
                check_name("plugin", current_section, fn, i + 1)
                current = Plugin(self, current_section, start_line=(fn, i+1))
                self.plugins[current_section] = current
                continue
            if line.startswith("topic "):
                section_type, current_section = line.split(" ", 1)
                current_section = self.eval_string(current_section.strip())
                check_name("topic_setting", current_section, fn, i + 1)
                current = self.topic_settings.get(current_section)
                if current is None:
                    current = self.topic_settings[current_section] = TopicSetting(self, current_section, start_line=(fn, i+1))
                continue

            is_one_line_directive = any(line.startswith(prefix + ":") for prefix in ("add_message_definition_dir", "add_process_default_template"))
            if current_section is None and not is_one_line_directive and not is_pipe_include:
                raise Exception("syntax error: %s:%d invalid line %r - expected %s" % (
                        fn, i + 1, line, expect_new_section_error_text))

            if current_section == "hosts" and not is_one_line_directive:
                try:
                    key, value = line.split(": ", 1)
                except Exception:
                    raise Exception("syntax error: %s:%d invalid line %r - expected 'hosts'-line of form hostname: hostip" % (
                            fn, i + 1, line))
                key = self.eval_string(key.strip())
                org_value = value
                value = self.eval_string(value.strip())
                if key not in self.hosts:
                    h = self.hosts[key] = Host(self, key)
                else:
                    h = self.hosts[key]
                for interface in value.split(","):
                    h.add_interface(interface)
                if not h.interfaces:
                    # user did not specify interfaces. try to resolve atleast one ipaddress!
                    if CommunicationUtils.is_ip(key):
                        ip_address = key
                    else:
                        try:
                            ip_address = socket.gethostbyname(key)
                        except Exception:
                            self.error("can not find ip_address for hostname %r!" % (key, ))
                            raise
                    h.add_interface(ip_address)
                h.add_aliases()
                continue
            if current_section == "node_map" and not is_one_line_directive:
                try:
                    key, value = line.split(": ", 1)
                except Exception:
                    raise Exception("syntax error: %s:%d invalid line %r - expected 'node_map'-line of form node_name: physical_hostname[,physical_hostname2]" % (
                            fn, i + 1, line))
                key = key.strip()
                value = self.eval_string(value.strip())
                if "," in value:
                    raise Exception("syntax error: %s:%d node map entry needs to map a node-name to a single host-name! not a list of hostnames! (%r -> %r)" % (
                        fn, i+1, line, value))
                #print "add to node map: %r %r" % (key, value)
                self.node_map[key] = value
                continue
            if current_section == "custom_start_tools" and not is_one_line_directive:
                try:
                    key, value = line.split(": ", 1)
                except Exception:
                    raise Exception("syntax error: %s:%d invalid line %r - expected 'custom_start_tools'-line of form tool-name: tool command" % (
                            fn, i + 1, line))
                key = key.strip()
                value = self.eval_string(value.strip())
                for i, (k, v) in enumerate(self.start_tools):
                    if k == key:
                        self.start_tools[i] = key, value
                        break
                else:
                    self.start_tools.append((key, value))
                continue
            
            if is_pipe_include:
                key = "pipe_include"
                value = line.split(" ", 1)[1]
            else:
                try:
                    key, value = line.split(":", 1)
                except Exception:
                    if current_section is None:
                        raise Exception("syntax error: %s:%d invalid line: %r - expect %s" % (
                            fn, i + 1, line, expect_new_section_error_text)) # todo: LN3: from None
                    else:
                        raise Exception("syntax error: %s:%d invalid line: %r - expect either a new section (like `defines`, `process XY`, `template XY`, ...) or a line in the form of `key: value` for the currently active `%s`-section" % (
                            fn, i + 1, line,
                            ("%s %s" % (section_type, current.name)) if section_type != current_section else section_type)) # todo: LN3: from None

            key = key.strip()
            if key.startswith("ifndef "):
                if_line = value.strip()
                defname = key.split(" ", 1)[1].strip()
                skip_value = len(self.defines.get(defname, "")) != 0
                try:
                    key, value = if_line.split(":", 1)
                except Exception:
                    raise Exception("syntax error: %s:%d invalid line %r - key: value" % (
                            fn, i + 1, line))
                key = key.strip()
            elif key == "undef":
                defname = value.strip()
                if defname in self.defines:
                    del self.defines[defname]
                continue
            elif key.startswith("ifdef "):
                if_line = value.strip()
                defname = key.split(" ", 1)[1].strip()
                skip_value = len(self.defines.get(defname, "")) == 0
                try:
                    key, value = if_line.split(":", 1)
                except Exception:
                    raise Exception("syntax error: %s:%d invalid line %r - key: value" % (
                            fn, i + 1, line))
                key = key.strip()
            else:
                skip_value = False
            value = value.strip()
            if value.startswith("["):
                if "]" in value:
                    warn_lines = True
                    self.error((
                        "syntax warning: %s:%d value starts with '[' and has a ']' in the same line:\n" +
                        "%s\n" +
                        "the '[' char at the beginning of a value is to indicate a multi-line value!\n" +
                        "please enclose your value in double-quotes '\"' if you just need your value to\n" + 
                        "start with '[' and don't want to have a multi-line comment.\n" + 
                        "will handle this as a multi-line value now!\n") % (
                            fn, i + 1, value))
                else:
                    warn_lines = False
                s = 0
                value_lines = [ value[1:] ]
                while True:
                    if s > 0:
                        if key.startswith("add_io_script"):
                            value_lines.append(lines[i + s])
                        else:
                            l = lines[i + s].strip()
                            if not l.startswith("#"):
                                value_lines.append(l)
                    if lines[i + s].rstrip().endswith("]"):
                        break
                    s += 1
                skip = s
                value_lines[-1] = value_lines[-1].rstrip()[:-1]
                if key.startswith("add_io_script"):
                    value = self.eval_string("".join(value_lines))
                elif key == "description":
                    value = "\n".join(value_lines).strip(" ")
                else:
                    value = " ".join(value_lines).strip(" ")
                if warn_lines:
                    self.error("interpreted multi-line value:\n%s\n" % value)

            if skip_value:
                continue
            if key == "pipe_include":
                inc_cmd = self.eval_string(value)
                self.debug("call pipe_include command: %r" % inc_cmd)
                p = subprocess.Popen(inc_cmd, shell=True, stdout=subprocess.PIPE)
                out, err = p.communicate()
                out = out.decode("utf-8")
                self.debug("pipe_include return value %d, output:\n%s" % (p.returncode, out))
                if p.returncode != 0:
                    raise Exception("%s:%d pipe_include command\n%s\nreturned %d!" % (
                        fn, i + 1, inc_cmd, p.returncode))
                try:
                    self._read_file(inc_cmd, fn_contents=out, name_prefix="/".join(local_name_prefixes), included_from=(fn, i+1))
                except Exception:
                    print("pipe_included from %s:%d" % (fn, i + 1))
                    raise
                self.current_fn = fn
                continue

            if is_one_line_directive:
                if key == "add_message_definition_dir":
                    value = self.eval_string(value)
                    if not os.path.isdir(value):
                        self.warning("message_definition_dir %r does not exist!\n%s" % (value, self.format_include_stack()))
                    if value not in self.additional_message_definition_dirs:
                        self.additional_message_definition_dirs.append(value)
                if key == "add_process_default_template":
                    self.process_default_templates.append(("/".join(local_name_prefixes), value))
                continue
            if current_section == "defines":
                key = self.eval_string(key)
                if value.startswith("%(deffun "):
                    self.eval_deffun(key, value)
                else:
                    value = self.eval_string(value)
                    self.defines[key] = value
                continue
            if key.startswith("ignore-"):
                continue
            try:
                current._read_config(key, value)
            except Exception:
               raise Exception("%s\n\nsyntax error: %s\n%s" % (traceback.format_exc(), sys.exc_info()[1], self.format_include_stack()))
        
        if included_from:
            del self.include_stack[-1]

    def get_host_for_node(self, node_name, create=False):
        if node_name.strip() == "":
            self.warning("specified empty node-name! assuming localhost!")
            node_name = "localhost"
        hostname = self.node_map.get(node_name)
        if hostname is None:
            # no node with node_name
            hostname = node_name
        try:
            return self.get_host(hostname, create=create)
        except Exception:
            raise Exception("unknown node_name %r:\n%s" % (node_name, traceback.format_exc()))

    def get_host(self, hostname, create=False, add_to_default_network=False, reachable_from=None):
        if hostname is None:
            raise Exception("invalid hostname: None!")
        if type(hostname) == Host:
            return hostname
        if hostname in ("localhost", "127.0.0.1"):
            hostname = self.manager_host.name
        try:
            host = self.hosts.get(hostname)
        except Exception:
            raise Exception("invalid host_name: %r" % (hostname, ))
        if host is not None:
            return host
        # maybe this is an alias/ip - search interfaces of all hosts
        for host_name, host in self.hosts.items():
            if host.has_alias(hostname, check_interfaces=reachable_from is None):
                self.debug("host %s has an alias %s" % (host, hostname))
                return host
            if reachable_from is None:
                continue
            try:
                intf = reachable_from.get_interface_from(host)
            except Exception:
                continue
            if intf.ip_address == hostname:
                self.debug("host %s has interface %s:%s in network %s" % (
                    host_name, intf.name, hostname, intf.network.name))
                return host
                
        if create:
            #self.info("need to create host entry for %r" % hostname)
            if CommunicationUtils.is_ip(hostname):
                ip_address = hostname
                try:
                    real_hostname = CommunicationUtils.try_to_get_fqdn(hostname)
                    if not CommunicationUtils.is_ip(real_hostname) and "." in real_hostname:
                        real_hostname = real_hostname.split(".", 1)[0]
                except Exception:
                    real_hostname = ip_address
                self.debug("using name %s as hostname for %s" % (real_hostname, hostname))
                hostname = real_hostname
            else:
                try:
                    ip_address = CommunicationUtils.try_to_get_ip(hostname)
                except Exception:
                    self.error("can not find ip_address for hostname %r!" % (hostname, ))
                    raise
            # is this ip_address already used?
            for host_name, host in self.hosts.items():
                if host.has_alias(ip_address):
                    self.info("existing host %s has alias for ip %s -- add %s as additional alias!" % (host, ip_address, hostname))
                    host.aliases.add(hostname)
                    return host
            host = self.hosts[hostname] = Host(self, hostname)
            iface = host.add_interface(ip_address)
            self.debug("created hostmap entry for name %r ip %r" % (hostname, ip_address,
                                                                        #"".join(traceback.format_stack())
                                                                        ))

            host.add_interfaces_to_matching_networks(self.networks)
            #if add_to_default_network or "default" in self.networks:
            #    if "default" in self.networks:
            #        self.networks["default"].add_host_interface(iface)
            #        self.debug("added interface %s to default network" % (iface))
            #    else:
            #        raise Exception("no default network - don't know how to reach this host....");
            return host
            
        raise Exception("unknown host_name/ip %r" % hostname)

    def _get_internal_scope_process_template(self):
        name = "%s(topic_name, value_name)" % self.instance_config.internal_scope_template_name
        template = ConfigurationTemplate(self, name)

        pythonpath = os.path.pathsep.join(ln.pythonpath)
        fn = os.path.join(config.manager_base_dir, "ln_topic_scope")

        template.org_fields = dict(
            # todo org field then in reamplte red config below!
            node="<gui_ip>",
            environment="USER=%%(env USER), HOME=%%(env HOME), DISPLAY=$(gui_DISPLAY), XAUTHORITY=$(gui_XAUTHORITY), PYTHONPATH=%s:%s" % (pythonpath, os.environ.get("PYTHONPATH", "")),
            pass_environment="LD_LIBRARY_PATH, XDG_RUNTIME_DIR",
            command='%s "%s" "%%(topic_name)" "%%(value_name)"' % (sys.executable, fn),
            ready_regex="scope ready",
            flags="no_state_led, no_error_on_stop, set_user_and_home",
        )
        for name, value in template.org_fields.items():
            template._read_config(name, value)
        return template

    def _get_scope_template(self, template_name):
        if self.instance_config.internal_scope_template_name not in self.configuration_templates:
            self.configuration_templates[self.instance_config.internal_scope_template_name] = self._get_internal_scope_process_template()
        return self.configuration_templates[template_name]

    def get_scope_process(self, topic, value):
        scope_id = topic, value
        process = self.scope_processes.get(scope_id)
        if process:
            return process

        name = "scope for %s %s" % (topic, value)
        process = self.processes.get(name)
        if process:
            return process
        
        # instanciate a new process
        template = self._get_scope_template(self.instance_config.scope_process_template)
        process = Process(self, name)
        process._read_config("use_template", "%s(%r, %r)" % (template.name, topic, value))
        process._finalize_config()
        self.processes[process.name] = process
        self.scope_processes[scope_id] = process
        # possibly add to groups
        for gn, g in self.groups.items():
            if g._possibly_add_process(process):
                self.debug("notify gui for process add %r %r" % (gn, process.name))
                self.manager._notify_gui(request="add_process", process=process, group_name=gn)
        self.manager._notify_gui(request="add_process", process=process, group_name=None)
        return process

    def _get_internal_notebook_process_template(self):
        name = "%s(fn, related_object_name)" % self.instance_config.internal_notebook_template_name
        template = ConfigurationTemplate(self, name)

        pythonpath = os.path.pathsep.join(ln.pythonpath)
        fn = os.path.join(config.manager_base_dir, "ln_notebook")

        change_dir = os.path.realpath(os.path.dirname(self.configuration_file))
        if not change_dir:
            change_dir = os.getcwd()

        template.org_fields = dict(
            # todo org field then in reamplte red config below!
            node="<gui_ip>",
            environment="USER=%%(env USER), HOME=%%(env HOME), DISPLAY=$(gui_DISPLAY), XAUTHORITY=$(gui_XAUTHORITY), PYTHONPATH=%s:%s" % (pythonpath, os.environ.get("PYTHONPATH", "")),
            pass_environment="LD_LIBRARY_PATH, XDG_RUNTIME_DIR",
            command='%s "%s" "%%(fn)" "%%(related_object_name)"' % (sys.executable, fn),
            ready_regex="notebook ready",
            flags="no_state_led, no_error_on_stop",
            term_timeout="10",
            change_directory=change_dir
        )
        for name, value in template.org_fields.items():
            template._read_config(name, value)
        return template

    def _get_internal_notebook_template(self):
        if self.instance_config.internal_notebook_template_name not in self.configuration_templates:
            self.configuration_templates[self.instance_config.internal_notebook_template_name] = self._get_internal_notebook_process_template()
        return self.configuration_templates[self.instance_config.internal_notebook_template_name]

    def _get_notebook_template(self, template_name):
        if template_name == self.instance_config.internal_notebook_template_name:
            return self._get_internal_notebook_template()
        return self.configuration_templates[template_name]

    def _prefix_client_name(self, prefix, related_object_name):
        if "/" in related_object_name:
            name = related_object_name.split("/")
            name[-1] = prefix + name[-1]
            return "/".join(name)
        return prefix + related_object_name
        
    def get_notebook_process(self, fn, related_object_name):
        notebook_id = fn, related_object_name
        process = self.notebook_processes.get(notebook_id)
        if process:
            return process
        
        name = self._prefix_client_name("nb:", related_object_name)
        process = self.processes.get(name)
        if process:
            # there already is a process with such a strange name... assume its a good notebook!
            return process
        
        # instanciate a new process
        template = self._get_notebook_template(self.instance_config.notebook_process_template)
        process = Process(self, name)
        process._read_config("use_template", "%s(%r, %r)" % (template.name, fn, related_object_name))
        process._finalize_config()
        self.processes[process.name] = process
        self.notebook_processes[notebook_id] = process
        # possibly add to groups
        for gn, g in self.groups.items():
            if g._possibly_add_process(process):
                #self.debug("notify gui for process add %r %r" % (gn, process.name))
                self.manager._notify_gui(request="add_process", process=process, group_name=gn)
        self.manager._notify_gui(request="add_process", process=process, group_name=None)
        return process

    def find_configuration_template(self, name_prefixes, name):
        if name == self.instance_config.internal_notebook_template_name:
            return self._get_internal_notebook_template()
        if name == self.instance_config.internal_scope_template_name:
            return self._get_internal_scope_process_template()
        for i in range(len(name_prefixes)):
            if i == 0:
                np = name_prefixes
            else:
                np = name_prefixes[:-i]
            prefixed_name = "%s/%s" % ("/".join(np), name)
            p = self.configuration_templates.get(prefixed_name)
            if p is not None:
                return p
        return self.configuration_templates.get(name)

    def export_hosts(self, ssh_config_fn_in):
        """
        generate a ssh-config section for each known host.
        also provide node-names as aliases.
        decide a route to ONE interface of each host.
        """
        ssh_config_fn = os.path.realpath(ssh_config_fn_in)
        
        # iterate over node-map and generate a host for each node
        for node_name, hostname in self.node_map.items():
            self.get_host(hostname, create=True)
        
        host_items = list(self.hosts.items())
        host_items.sort()
        this_host = self.manager_host

        def find_port(args):
            for arg in args:
                if isinstance(arg, tuple) and arg[0] == "Port":
                    return arg[1]
            return "22"

        longest_route = None # host, len
        entries = []
        for hname, host in host_items:
            # determine possible names
            aliases = set(host.aliases)
            aliases.add(hname)
            for nname, nhname in list(self.node_map.items()):
                if nhname in aliases:
                    aliases.add(nname)
            aliases = list(aliases.difference([hname]))
            aliases.sort()
            aliases = " ".join(aliases)
            if aliases: aliases = " " + aliases

            entry = [
                "Host %s%s" % (hname, aliases),
                "  HostKeyAlias %s" % hname.lower(),
            ]
            for arg in host.get_ssh_args():
                if isinstance(arg, tuple):
                    entry.append("  %s %s" % arg) # checked
                else:
                    entry.append("  # don't know how to add option %s" % arg)
            if this_host == host:
                entry.append("  HostName %s" % socket.gethostname())
                if longest_route is None:
                    longest_route = host, 1
            else:
                # decide route from THIS host
                route = this_host.search_route_to(host)
                if longest_route is None or len(route) > longest_route[1]:
                    longest_route = host, len(route)
                for ifout, ifin in zip(route[:-1], route[1:]):
                    entry.append("  # via network %s to %s" % (ifout.network.name, ifin.host.name))
                if len(route) > 2:
                    ssh_target = route[-2]
                    tunnel_target = route[-1]
                    entry.append("  ProxyCommand ssh %s -q -W %s:%s -F '%s'" % (
                        ssh_target.host.name,
                        tunnel_target.ip_address, find_port(tunnel_target.host.get_ssh_args()),
                        ssh_config_fn))
                ifin = route[-1]
                entry.append("  HostName %s" % ifin.ip_address)
            entries.append("\n".join(entry))
        entries = "\n\n".join(entries)

        # file will be generated on manager filesystem
        with open(ssh_config_fn, "w") as fp:
            fp.write("# generated from %s on %s\n" % (
                os.path.realpath(config.configuration_file),
                datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
            exhost = longest_route[0]
            fp.write("# to login to %s enter:\n"
                     "#  $ ssh %s -F %s -t\n"
                     "# to execute a remote command:\n"
                     "#  $ ssh %s -F %s -q /usr/bin/ls -l /\n"
                     "# or for rsync:\n"
                     "#  $ rsync -e \"ssh -F %s\" -r %s:/tmp/ /tmp/%s\n" % (
                         exhost.name,
                         exhost.name, ssh_config_fn_in,
                         exhost.name, ssh_config_fn_in,
                         ssh_config_fn_in, exhost.name, exhost.name))
            fp.write("\n" + entries + "\n")
        
