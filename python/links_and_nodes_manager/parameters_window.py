"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

from __future__ import print_function

import pprint
import os
import traceback
import numpy as np
from numpy import array, inf, nan, double, int8, uint8, int16, uint16, int32, uint32, int64, uint64 # noqa: F401 - needed for parameters

from . import config
from . import ui_tools

from .CommunicationUtils import callback
from .Logging import enable_logging_on_instance

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Pango', '1.0')
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
from gi.repository import GObject, Gtk, Pango, GLib # noqa: E402

class parameters_window(object):
    def __init__(self, parent, position=None, start_pattern=None, client_name=None, dont_show=False):
        self.parent = parent
        if parent.__class__.__name__ != "TopicsGui":
            print("different parameters window gui: %r" % (parent.__class__.__name__))
        self.gui = parent
        self.manager_proxy = self.gui.manager_proxy
        enable_logging_on_instance(self, "PW")

        self.xml = Gtk.Builder()
        resources_dir = os.path.join(config.manager_base_dir, "resources")
        self.xml_fn = os.path.join(resources_dir, "parameters_window.ui")
        self.xml.add_from_file(self.xml_fn)
        self.xml.connect_signals(self)

        self.refresh_timer = None

        self.fd = Pango.FontDescription(ui_tools.get_monospace_font_name(self.parameters_window))

        m = self.parameters_model = Gtk.TreeStore(
            GObject.TYPE_STRING,
            GObject.TYPE_STRING,
            GObject.TYPE_STRING,
            GObject.TYPE_STRING,
            GObject.TYPE_PYOBJECT)
        tv = self.parameters_tv
        tv.set_model(m) # cursor-changed-fine
        tv.insert_column_with_attributes(-1, "parameter", Gtk.CellRendererText(), text=0)
        value_renderer = Gtk.CellRendererText()
        value_renderer.set_property("font-desc", self.fd)
        tv.insert_column_with_attributes(-1, "input", value_renderer, text=1)
        col = tv.get_columns()[-1]
        col.set_resizable(True)

        tvc = Gtk.TreeViewColumn("override / output")
        tcr = Gtk.CellRendererToggle()
        tcr.connect("toggled", self.override_active_toggled)
        tvc.pack_start(tcr, False)
        tvc.set_cell_data_func(tcr, self.override_check_data_func)

        value_renderer = Gtk.CellRendererText()
        value_renderer.set_property("font-desc", self.fd)
        value_renderer.set_property("editable", True)
        value_renderer.connect("edited", self.on_override_edited)
        self.editing = {}
        value_renderer.connect("editing-started", self.on_editing_started)
        value_renderer.connect("editing-canceled", self.on_editing_canceled)
        tvc.pack_start(value_renderer, True)
        tvc.set_attributes(value_renderer, text=2)
        tv.insert_column(tvc, -1)

        description_renderer = Gtk.CellRendererText()
        description_renderer.set_property("font-desc", self.fd)
        tv.insert_column_with_attributes(-1, "description", description_renderer, text=3)
        col = tv.get_columns()[-1]
        col.set_resizable(True)

        w = self.parameters_window
        self._last_pos = None
        w.set_title("ln parameters")
        if position:
            window_size, pos = position
            w.resize(*window_size)
        if not dont_show:
            w.show()
            if position:
                w.move(*pos)
            w.connect("configure-event", self.on_resize)
        
        self.parameters = {}

        m = self.pattern_model = Gtk.ListStore(GObject.TYPE_STRING)
        patterns = self.load_patterns()
        if client_name is not None:
            self.pattern = "client:%s" % client_name
            if self.pattern not in patterns:
                patterns.insert(0, self.pattern)
        elif start_pattern is None:
            self.pattern = patterns[0]
        else:
            self.pattern = start_pattern
        
        self.pattern_cb.set_model(m) # cursor-changed-fine
        #self.pattern_cb.set_text_column(0)
        for i, pattern in enumerate(patterns):
            m.append((pattern, ))        
            if pattern == self.pattern:
                self.pattern_cb.set_active(i)
        if not dont_show:
            self.search_parameters()
            self.connect()
        
    def connect(self):
        self.sysconf = self.gui.gui.sysconf
        def notification_wrapper(callback):
            def inner_notification_wrapper(gui, msg):
                if "request" in msg: 
                    del msg["request"]
                try:
                    return callback(**msg)
                except Exception:
                    self.error("error in notification callback %r%s:\n%s" % (callback.__name__, msg, traceback.format_exc()))
                    raise
            return inner_notification_wrapper
        self.gui.add_hook(("notification", "update_service"), notification_wrapper(self.on_change))
        self.gui.add_hook(("notification", "update_client"), notification_wrapper(self.on_change))
        self.gui.add_hook(("notification", "del_client"), notification_wrapper(self.on_change))
        self.gui.add_hook(("notification", "new_config"), notification_wrapper(self.on_new_config))

    def on_new_config(self, new_config):
        self.sysconf = new_config
        return True
    
    def on_change(self, *args, **kwargs):
        self.refresh_needed = True
        self.enable_refresh(t=0.5)
        return True

    def present(self):
        self.parameters_window.present()
        return True

    def __getattr__(self, name):
        if hasattr(self, "xml"):
            widget = self.xml.get_object(name)
            if widget is not None:
                return widget
        raise AttributeError(name)

    def on_resize(self, widget, ev):
        window_size = widget.get_size()
        position = widget.get_position()
        new_pos = window_size, position
        if new_pos != self._last_pos:
            self._last_pos = new_pos
            #todo self.parent.store_parameters_position(new_pos)


    def on_refresh_btn_clicked(self, btn):
        self.update_pattern_model()
        self.search_parameters()
        return True

    def _split_pname(self, pname):
        return pname.split(".")

    def _add_parameter(self, pname):
        ppath = self._split_pname(pname)
        m = self.parameters_model
        # search / create parent for this parameter
        parent = None
        def add_find(path, parent):
            if len(path) == 0:
                return parent
            item = path.pop(0)
            child_iter = m.iter_children(parent)
            while child_iter:
                if m[child_iter][0] == item:
                    parent = child_iter
                    break
                if m[child_iter][0] > item:
                    # insert!
                    parent = m.insert_before(parent, child_iter, (item, "", "", "", None))
                    break
                child_iter = m.iter_next(child_iter)
            else:
                parent = m.append(parent, (item, "", "", "", None))
            return add_find(path, parent)
        return add_find(ppath, parent)

    def _format_data(self, data):
        if type(data) == np.ndarray:
            return pprint.pformat(data)
        return str(data)

    def _add_parameters_from(self, resp, s):
        if s.provider not in self.parameters:
            self.parameters[s.provider] = {}
        new_params = eval(resp.data)
        if s.name not in self.parameters[s.provider]:
            params = self.parameters[s.provider][s.name] = {}
        else:
            params = self.parameters[s.provider][s.name]
        
        m = self.parameters_model
        tv = self.parameters_tv
        unseen_params = list(params.keys())
        for pname, new_value in new_params.items():
            is_new = False
            if pname in unseen_params:
                unseen_params.remove(pname)
                value = params[pname]
                value.update(new_value)
            else:
                is_new = True
                value = new_value
                params[pname] = value
                value["service"] = s
                value["parameter"] = pname
            iter = self._add_parameter(pname)
            value["iter"] = iter
            if value.get("editing"):
                pass
            else:
                m[iter][1] = self._format_data(value["input"])
                if value["override_enabled"]:
                    m[iter][2] = self._format_data(value["output"])
                else:
                    m[iter][2] = ""

                m[iter][4] = value
                if "description" in value.keys():
                    m[iter][3] = value["description"]
            #if is_new:
            #    tv.expand_to_path(m.get_path(iter))
        # remove unseen
        def remove(iter):
            parent = m.iter_parent(iter)
            del m[iter]
            if parent is not None and not m.iter_has_child(parent):
                remove(parent)
            
        for pname in unseen_params:
            value = params[pname]
            remove(value["iter"])
            del params[pname]
        
        self.open_responses -= 1
        if self.open_responses == 0:
            # last response
            self.enable_refresh()

    def update_pattern_model(self):
        self.pattern = self.pattern_cb.get_active_text()
        m = self.pattern_model
        # search text
        iter = m.get_iter_first()
        if m[iter][0] == self.pattern:
            # nothing todo
            return
        # search further, remove if exists, prepent at index 0
        while iter:
            if m[iter][0] == self.pattern:
                # found! -> remove!
                m.remove(iter)
                break
            iter = m.iter_next(iter)
        # insert at top!
        m.insert(0, (self.pattern, ))
        
        patterns = []
        iter = m.get_iter_first()
        while iter:
            patterns.append(m[iter][0])
            iter = m.iter_next(iter)
        self.save_patterns(patterns)
    
    def save_patterns(self, patterns):
        if patterns == self._last_patterns:
            return
        with open(os.path.expanduser("~/.ln_parameters.%s" % self.sysconf.instance_config.name), "w") as fp:
            fp.write(repr(patterns))
        self._last_patterns = patterns

    def load_patterns(self):
        try:
            with open(os.path.expanduser("~/.ln_parameters.%s" % self.sysconf.instance_config.name), "r") as fp:
                self._last_patterns = eval(fp.read())
        except Exception:
            self._last_patterns = ["*"]
        return self._last_patterns

    def search_parameters(self): # update parameters
        # query all services
        seen_clients = set()
        self.open_responses = 0
        cn = None
        if self.pattern.startswith("client:"):
            cn = self.pattern.split(":", 1)[1].strip()
            pattern = "*"
        else:
            pattern = self.pattern

        m = self.parameters_model
        clients = self.gui.manager_proxy("get_clients")
        for client in clients:
            if cn is not None:
                if client.program_name != cn:
                    continue
            for service in list(client.provided_services.values()):
                if service.interface == "ln/parameters/query_dict":
                    seen_clients.add(client)
                    self.open_responses += 1
                    self.manager_proxy(
                        "send_request", 
                        service, "call",
                        callback(self._add_parameters_from, service),
                        pattern=pattern)
            if client in self.parameters:
                # remove disappeared services
                ps = set(client.provided_services.keys())
                ks = set(self.parameters[client].keys())
                us = ks.difference(ps)
                for s in us:
                    for pname, value in self.parameters[client][s].items():
                        m.remove(value["iter"])
                    del self.parameters[client][s]
        known_clients = set(self.parameters.keys())
        unseen_clients = known_clients.difference(seen_clients)
        def remove(iter):
            parent = m.iter_parent(iter)
            del m[iter]
            if parent is not None and not m.iter_has_child(parent):
                remove(parent)
        for c in unseen_clients:
            for service_name, parameters in self.parameters[c].items():
                for pname, value in parameters.items():
                    remove(value["iter"])
            del self.parameters[c]
        if self.open_responses == 0:
            # immediate refresh
            self.enable_refresh()

    def on_expand_all_btn_clicked(self, btn):
        self.parameters_tv.expand_all()

    def on_collapse_all_btn_clicked(self, btn):
        self.parameters_tv.collapse_all()

    def enable_refresh(self, t=None):
        self.refresh_time = float(self.refresh_sbtn.get_value())
        # disable refresh
        if self.refresh_timer is not None:
            GLib.source_remove(self.refresh_timer)
            self.refresh_timer = None
        if not self.auto_refresh_cb.get_active() and t is None:
            return True
        if self.refresh_time <= 0 and t is None:
            return True
        # enable refresh!
        if t is None:
            t = int(self.refresh_time * 1000)
        else:
            t = int(t * 1000)
        self.refresh_timer = GLib.timeout_add(t, self.on_refresh)
        return True

    def on_refresh_sbtn_value_changed(self, btn):
        return self.enable_refresh()

    def on_refresh(self):
        self.refresh_timer = None
        self.pattern = self.pattern_cb.get_active_text()
        self.search_parameters()
        return False

    def on_parameters_window_delete_event(self, win, ev):
        # disable refresh
        if self.refresh_timer is not None:
            GLib.source_remove(self.refresh_timer)
            self.refresh_timer = None
        self.parameters_window.hide()
        return True

    def override_active_toggled(self, cr, path):
        m = self.parameters_model
        row = m[path]
        value = row[4]
        if not cr.get_active():
            # override!
            if row[2] == "":
                # get input
                row[2] = row[1]
            self.set_override(value, True)
        else:
            # disable override!
            self.set_override(value, False)

    def set_override(self, value, enable):
        service, parameter = value["service"], value["parameter"]
        m = self.parameters_model
        row = m[value["iter"]]
        data = row[2]

        for sname, sval in service.provider.provided_services.items():
            if sval.interface == "ln/parameters/override_dict_single":
                break
        else:
            raise Exception("client %s provide's no override_dict_single service!" % service.provider)
        
        svc = sval.get_service()
        svc.req.parameter_name = parameter
        if enable:
            data = data.strip()
            if data.startswith("["):
                data = "array(%s)" % data
            svc.req.override_data = data
        else:
            svc.req.override_data = ""

        def on_override_answer(r, sval):
            if r.error_len:
                dlg = Gtk.MessageDialog(
                    self.parameters_window, 
                    Gtk.DialogFlags.MODAL | Gtk.DialogFlags.DESTROY_WITH_PARENT,
                    Gtk.MessageType.ERROR,
                    Gtk.ButtonsType.CLOSE,
                    "can not set override value!")
                dlg.format_secondary_text(r.error)
                #raise Exception(r.error)
                dlg.set_resizable(True)
                dlg.run()
                dlg.hide()
            self.search_parameters()
        svc.call_async(cb=callback(on_override_answer, sval))

    def override_check_data_func(self, tvc, cr, m, iter, data):
        value = m[iter][4]
        if value is None:
            cr.set_property("visible", False)
            return
        cr.set_property("visible", True)
        cr.set_property("active", value["override_enabled"])
    
    def on_override_edited(self, cr, path, new_text):
        m = self.parameters_model
        row = m[path]
        value = row[4]
        value["editing"] = False
        row[2] = new_text
        if value["override_enabled"]:
            self.set_override(value, True)

    def on_editing_started(self, cr, edt, path):
        edt.modify_font(self.fd)
        m = self.parameters_model
        iter = m.get_iter(path)
        row = m[iter]
        row[4]["editing"] = True
        self.editing[cr] = iter
        return True

    def on_editing_canceled(self, cr):
        if cr in self.editing:
            iter = self.editing[cr]
            m = self.parameters_model
            row = m[iter]
            row[4]["editing"] = False
            del self.editing[cr]
        return True

    def on_auto_refresh_cb_toggled(self, btn):
        self.enable_refresh()
            
    def on_pattern_cb_entry_activate(self, btn):
        self.update_pattern_model()
        self.search_parameters()
        return True

    def on_pattern_cb_changed(self, cb):
        self.enable_refresh(0.5)
        return True

    def on_parameters_tv_row_expanded(self, tv, iter, path):
        m = self.parameters_model
        if m.iter_n_children(iter) == 1:
            child = m.iter_children(iter)
            tv.expand_row(m.get_path(child), False)
        return True

    def on_parameters_tv_cursor_changed(self, tv):
        path, col = tv.get_cursor() # path-none-checked
        if not path:
            return True
        m = self.parameters_model
        iter = m.get_iter(path)
        def get_name(iter, names=None):
            if names is None:
                names = []
            names.insert(0, m[iter][0])
            parent = m.iter_parent(iter)
            if parent is not None:
                return get_name(parent, names)
            return names
        name = get_name(iter)
        self.param_label.set_text(".".join(name))
        return True

    def request_topic_publishing(self, value, cb):
        parameter_name = value["parameter"]
        # now ask parameter provider to publish, get topic_name
        service = value["service"]
        service_name = service.name.replace("query_dict", "request_topic")
        service = service.provider.provided_services[service_name]
        svc = service.get_service()
        svc.req.parameters = parameter_name
        def on_got_topic(r, service, parameter_name, cb):
            if svc.resp.error_len:
                raise Exception(svc.resp.error)
            topic_name, topic_md = eval(r.parameter_topics)[parameter_name]
            cb(topic_name, topic_md, parameter_name)
        svc.call_async(cb=callback(on_got_topic, service, parameter_name, cb))
        return True

    def _open_scope_cb(self, topic_name, topic_md, parameter_name):
        self.gui.manager_proxy("open_scope", topic_name, parameter_name + ".output")
    def _open_topic_cb(self, topic_name, topic_md, parameter_name):
        topic = self.gui.topics[topic_name]
        self.gui.open_inspector_for_port(topic.source_port, topic, subscriber=None)

    def on_parameters_tv_row_activated(self, tv, path, col):
        if self.sysconf.instance_config.open_scope_on_parameter_double_click:
            m = tv.get_model()
            row = m[path]
            value = row[4]
            self.request_topic_publishing(value, self._open_scope_cb)

    def on_parameters_tv_button_press_event(self, tv, ev):
        if not ev.button == 3: 
            return False
        res = tv.get_path_at_pos(int(ev.x), int(ev.y))
        if not res: 
            return False
        path, col, xp, yp = res
        m = tv.get_model()
        row = m[path]
        value = row[4]
        self.selected = value
        self.parameter_popup.popup(None, None, None, None, ev.button, ev.time)
        return True

    def on_parameter_open_scope_activate(self, item):
        self.request_topic_publishing(self.selected, self._open_scope_cb)
        return True

    def on_parameter_open_port_activate(self, item):
        self.request_topic_publishing(self.selected, self._open_topic_cb)
        return True
