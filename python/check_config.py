import sys
import sysconfig
import pprint

cfg = dict()

try:
    import numpy
    cfg["numpy_include"] = numpy.get_include()
except Exception as e:
    sys.stderr.write("no numpy: %r\n" % e)
    cfg["numpy_include"] = ""

try:
    import distutils.sysconfig
    cfg["python_lib"] = distutils.sysconfig.get_python_lib(plat_specific=False, standard_lib=False, prefix="")
except Exception:
    cfg["python_lib"] = "lib/python%s.%s/dist-packages" % (sys.version_info[0], sys.version_info[1])

cfg["python_major_minor"] = sys.version_info[0], sys.version_info[1]
cfg["ext_dir"] = "%s-%s.%s" % (sysconfig.get_platform(), sys.version_info[0], sys.version_info[1])

pprint.pprint(cfg)
