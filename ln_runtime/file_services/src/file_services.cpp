#ifdef __WIN32__
#include <windows.h>
#endif

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string>
#include <signal.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <dirent.h>
#include <fcntl.h>
#include <time.h>
#include <sys/types.h>
#include <locale.h>

#include <ln/ln.h>
#include <string_util/string_util.h>

#include "ln_messages.h"

using namespace std;
using namespace string_util;

bool keep_running = true;

#if !defined(__QNX__) && !defined(__WIN32__) && !defined(__VXWORKS__) && !defined(__ANDROID__)
#define HAVE_UTIMENS
#define HAVE_GETPWNAM
#endif

#if !defined(__WIN32__) && !defined(__VXWORKS__)
#define HAVE_UTIMES
#endif

#if !defined(__WIN32__)
#define HAVE_CLOCK_GETTIME
#endif

#ifndef HAVE_UTIMENS
#include <sys/types.h>
#include <utime.h>
#include <sys/time.h>
#endif

#ifdef HAVE_GETPWNAM
#include <pwd.h>
#endif

using namespace ln::file_services;
using namespace ln::file_services2;

class file_services :
	public read_from_file_base,
	public write_file_base,
	public put_symlink_base,
	public chown_base,
	public get_tree_info_base,
	public get_tree_info2_base,
	public clock_gettime_base,
	public clock_settime_base,
	public unlink_base,
	public rmdir_base
{
	ln::client* clnt;

public:
	file_services(ln::client* clnt) : clnt(clnt) {
		vector<string> args = clnt->get_remaining_args();
		string prepend_node_name;
		unsigned int skip = 0;
		for(unsigned int i = 0; i < args.size(); i++) {
			if(skip) {
				skip --;
				continue;
			}
			if(args[i] == "-prepend-node-name" && args.size() > i + 1) {
				prepend_node_name = args[i + 1];
				skip = 1;
				continue;
			}
		}
		if(prepend_node_name.size())
			prepend_node_name += ".";
		register_read_from_file(clnt, prepend_node_name + "read_from_file");
		register_write_file(clnt, prepend_node_name + "write_file");
		register_put_symlink(clnt, prepend_node_name + "put_symlink");
		register_get_tree_info(clnt, prepend_node_name + "get_tree_info");
		register_get_tree_info2(clnt, prepend_node_name + "get_tree_info2");
#ifdef HAVE_CLOCK_GETTIME
		register_clock_gettime(clnt, prepend_node_name + "clock_gettime");
		register_clock_settime(clnt, prepend_node_name + "clock_settime");
#endif
		register_unlink(clnt, prepend_node_name + "unlink");
		register_rmdir(clnt, prepend_node_name + "rmdir");
	}

	int run() {
		clnt->handle_service_group_in_thread_pool(NULL, "main_pool");
		clnt->set_max_threads("main_pool", 8);
	
		printf("waiting for service requests\n");
		fflush(stdout);
		while(keep_running) {
#ifdef __WIN32__
			Sleep(1000);
#else
			sleep(1);
#endif
		}
		return 0;
	}

	bool is_dir(string path) {
		struct stat sbuf;
		int ret = stat(path.c_str(), &sbuf);
		if(ret == -1)
			return false;

		return S_ISDIR(sbuf.st_mode);
	}
	bool exists(string path) {
		struct stat sbuf;
		int ret = stat(path.c_str(), &sbuf);
		if(ret == -1)
			return false;
		return true;
	}

	template<typename tree_info_type>
	void fill_tree_info_symlink_target(const std::string& pathname, tree_info_type* info);

	template<typename tree_info_type>
	void fill_tree_info(string pathname, tree_info_type* info, unsigned int max_depth) {
		memset(info, 0, sizeof(*info));
		try {
			// printf("fill_tree_info (max_depth %d): %s\n", max_depth, repr(pathname).c_str());
			pathname = strip(pathname);
			bool had_trailing_slash = pathname[pathname.size() - 1] == '/';
			if(had_trailing_slash)
				pathname = pathname.substr(0, pathname.size() - 1);
			string::size_type pos = pathname.rfind("/");
			string basename;
			if(pos != string::npos)
				basename = pathname.substr(pos + 1);
			else
				basename = pathname;
			info->entry = strdup(basename.c_str());
			info->entry_len = strlen(info->entry);
			struct stat sbuf;
#if defined(__WIN32__)
			int ret = stat(pathname.c_str(), &sbuf);
#else
			int ret = lstat(pathname.c_str(), &sbuf);
#endif
			if(ret == -1)
				throw errno_exception("stat(%s)", repr(pathname).c_str());

			info->size = sbuf.st_size;
			info->mode = sbuf.st_mode;
			info->uid = sbuf.st_uid;
			info->gid = sbuf.st_gid;
			info->mtime = sbuf.st_mtime;

			info->type = '.';
#if !defined(__WIN32__)
			if(S_ISLNK(sbuf.st_mode)) {
				info->type = 'l';
				info->size = 0; // protocol for symlinks
				info->mode = 0;
				info->mtime = 0;
				fill_tree_info_symlink_target(pathname, info);
			} else
#endif
			if(S_ISDIR(sbuf.st_mode))
				info->type = 'd';
			else if(S_ISREG(sbuf.st_mode))
				info->type = 'f';

			if(S_ISDIR(sbuf.st_mode) && (max_depth == 0 || max_depth > 1)) { // todo: max_depth is nonsense
				printf("it is a dir!\n");
				// fill childs!
				DIR* dp = opendir(pathname.c_str());
				if(!dp)
					throw errno_exception("opendir(%s)", repr(pathname).c_str());
				struct dirent* entry;
				string base = pathname + "/";
				list<string> entries;
				while((entry = readdir(dp))) {
					string d_name = entry->d_name;
					if(d_name == "." || d_name == "..")
						continue;
					entries.push_back(base + d_name);
				}
				closedir(dp);
				info->childs = (tree_info_type*)calloc(entries.size(), sizeof(*info));
				info->childs_len = entries.size();
				int i = 0;
				for(list<string>::iterator it = entries.begin(); it != entries.end(); it++)
					fill_tree_info<tree_info_type>(*it, &info->childs[i++], max_depth - 1);
			}
		}
		catch(const exception& e) {
			printf("had error file filling tree_info for %s:\n%s\n", repr(pathname).c_str(), e.what());
			info->error_message = strdup(e.what());
			info->error_message_len = strlen(info->error_message);
		}
	}

	template<typename tree_info_type>
	void free_tree_info(tree_info_type* info);

	virtual int on_get_tree_info(ln::service_request& req, get_tree_info_t& svc);
	virtual int on_get_tree_info2(ln::service_request& req, get_tree_info2_t& svc);

	virtual int on_read_from_file(ln::service_request& req, read_from_file_t& svc) {
		string filename(svc.req.filename, svc.req.filename_len);		
		
		printf("read %d bytes from %d of %s\n", (int)svc.req.len, (int)svc.req.offset, filename.c_str());

		struct stat sbuf;
		int ret = stat(filename.c_str(), &sbuf);
		if(ret == -1)
			return 2;

		unsigned int max_block_size = sbuf.st_size - svc.req.offset;
		if(svc.req.len > max_block_size)
			svc.req.len = max_block_size;
		
		ret = 1; // error

		svc.resp.data = new uint8_t[svc.req.len];
		if(svc.resp.data) {
			FILE* fp = fopen(filename.c_str(), "rb");
			if(fp) {
				fseek(fp, svc.req.offset, SEEK_SET);
				svc.resp.data_len = fread(svc.resp.data, 1, svc.req.len, fp);
				fclose(fp);

				req.respond();
				ret = 0; // success
			} else
				ret = 2;

			delete[] svc.resp.data;
		} else 
			ret = 3;
		return ret;
	}

	virtual int on_write_file(ln::service_request& req, write_file_t& svc) {
		svc.resp.error_message_len = 0;

		string filename(svc.req.filename, svc.req.filename_len);
		printf("write %d bytes to %s, set_mtime: %.3f\n", (int)svc.req.data_len, filename.c_str(), svc.req.set_mtime);

		string::size_type pos = filename.rfind("/");
		if(pos != string::npos) {
			string dirname = filename.substr(0, pos);
			if(!is_dir(dirname)) {
				printf("create directory %s", dirname.c_str());
				system(format_string("mkdir -p '%s'", dirname.c_str()).c_str());
			}
		}
		
		FILE* fp = fopen(filename.c_str(), "wb");
		if(fp) {
			int written = fwrite(svc.req.data, 1, svc.req.data_len, fp);
			printf("wrote %d bytes\n", written);
			/*
			if(svc.req.set_mtime != 0) {
				// try to set mtime
				struct timespec ts[2];
				ts[0].tv_sec = (time_t)svc.req.set_mtime;
				ts[0].tv_nsec = (long)((svc.req.set_mtime - (double)ts[0].tv_sec) * 1e9);
				ts[1] = ts[0];
				if(futimens(fileno(fp), ts) == -1)
					printf("failed to set modification time to %.3fs: %s\n", svc.req.set_mtime, strerror(errno));
			}
			*/
			fclose(fp);
			if(svc.req.set_mtime != 0) {
				// try to set mtime
#ifdef HAVE_UTIMENS
				struct timespec ts[2];
				ts[0].tv_sec = (time_t)svc.req.set_mtime;
				ts[0].tv_nsec = (long)((svc.req.set_mtime - (double)ts[0].tv_sec) * 1e9);
				ts[1] = ts[0];
				int fd = open(filename.c_str(), O_WRONLY);
				if(fd == -1)
					printf("failed to open filename a second time!\n");
				else {
					printf("set ts!\n");
					if(futimens(fd, ts) == -1)
						printf("failed to set modification time to %.3fs: %s\n", svc.req.set_mtime, strerror(errno));
					close(fd);
				}
#else
#ifdef HAVE_UTIMES
				struct timeval tv[2];
				tv[0].tv_sec = (time_t)svc.req.set_mtime;
				tv[0].tv_usec = (long)((svc.req.set_mtime - (double)tv[0].tv_sec) * 1e6);
				tv[1] = tv[0];
				if(utimes(filename.c_str(), tv) == -1)
					printf("failed to set modification time to %.3fs: %s\n", svc.req.set_mtime, strerror(errno));				
#endif
#endif
				
			}
			if(svc.req.set_mode != 0) {
				int ret = chmod(filename.c_str(), svc.req.set_mode);
				if(ret == -1)
					printf("failed to set protection mode to %04o: %s\n", svc.req.set_mode, strerror(errno));
			}
		} else {
			char errmsg[1024];
			int len = snprintf(errmsg, 1024, "could not open file: %s\n", strerror(errno));
			svc.resp.error_message = errmsg;
			svc.resp.error_message_len = len;
			printf("%s", errmsg);
		}
		req.respond();
		return 0;
	}
	virtual int on_put_symlink(ln::service_request& req, put_symlink_t& svc) {
		svc.resp.error_message_len = 0;

		string filename(svc.req.filename, svc.req.filename_len);
		string target(svc.req.target, svc.req.target_len);
		printf("put_symlink '%s' targetting '%s'\n",
		       filename.c_str(), target.c_str());

		string::size_type pos = filename.rfind("/");
		if(pos != string::npos) {
			string dirname = filename.substr(0, pos);
			if(!is_dir(dirname)) {
				printf("create directory %s", dirname.c_str());
				system(format_string("mkdir -p '%s'", dirname.c_str()).c_str());
			}
		}
		if(exists(filename.c_str()))
			if(unlink(filename.c_str()) != 0) {
				std::string errmsg = "entry " + filename + " already exists. can not unlink: " + std::string(strerror(errno));
				svc.resp.error_message = (char*)errmsg.c_str();
				svc.resp.error_message_len = errmsg.size();
				printf("%s", errmsg.c_str());
				req.respond();
				return 0;
			}
		symlink(target.c_str(), filename.c_str());

		req.respond();
		return 0;
	}
	virtual int on_chown(ln::service_request& req, chown_t& svc) {
		svc.resp.error_message_len = 0;

		string filename(svc.req.filename, svc.req.filename_len);
		string chownstr(svc.req.chown, svc.req.chown_len);

#ifdef HAVE_GETPWNAM				
		printf("chown to %s: %s\n", chownstr.c_str(), filename.c_str());
		// getpwnam
		struct passwd* pw = getpwnam(chownstr.c_str());
		if(pw == NULL)
			printf("failed to getpwnam for user %s: %s\n",
			       chownstr.c_str(), strerror(errno));
		else {
			uid_t owner = pw->pw_uid;
			gid_t group = pw->pw_gid;
			int ret = chown(filename.c_str(), owner, group);
			if(ret == -1)
				printf("failed to chown to %s (uid %d, gid %d): %s\n",
				       chownstr.c_str(),
				       owner, group, strerror(errno));
		}
#else
		printf("this arch does not support getpwnam()\n");
#endif
		req.respond();
		return 0;
	}
#ifdef HAVE_CLOCK_GETTIME
	virtual int on_clock_gettime(ln::service_request& req, clock_gettime_t& svc) {
		svc.resp.error_message_len = 0;
		try {
			struct timespec ts;
			clockid_t clk_id;
			
			if(svc.req.clock_id == 0)
				clk_id = CLOCK_REALTIME;
			else if(svc.req.clock_id == 1)
				clk_id = CLOCK_MONOTONIC;
			else
				throw str_exception_tb("invalid clock_id %d: choose 0 for CLOCK_REALTIME or 1 for CLOCK_MONOTONIC", svc.req.clock_id);

			int ret = clock_gettime(clk_id, &ts);
			if(ret != 0)
				throw errno_exception_tb("clock_gettime");
			svc.resp.tv_sec = (uint32_t)ts.tv_sec;
			svc.resp.tv_nsec = (uint32_t)ts.tv_nsec;
			
			req.respond();
		}
		catch(const exception& e) {
			ln::string_buffer err(&svc.resp.error_message, e.what());
			req.respond();
		}
		return 0;
	}
	virtual int on_clock_settime(ln::service_request& req, clock_settime_t& svc) {
		svc.resp.error_message_len = 0;
		try {
			struct timespec ts;
			clockid_t clk_id;
			
			if(svc.req.clock_id == 0)
				clk_id = CLOCK_REALTIME;
			else if(svc.req.clock_id == 1)
				clk_id = CLOCK_MONOTONIC;
			else
				throw str_exception_tb("invalid clock_id %d: choose 0 for CLOCK_REALTIME or 1 for CLOCK_MONOTONIC", svc.req.clock_id);

			ts.tv_sec = (time_t)svc.req.tv_sec;
			ts.tv_nsec = (long)svc.req.tv_nsec;
			
			int ret = clock_settime(clk_id, &ts);
			if(ret != 0)
				throw errno_exception_tb("clock_settime");

			if(svc.req.systohc == 1)
				system("/sbin/hwclock --systohc");
			
			req.respond();
		}
		catch(const exception& e) {
			ln::string_buffer err(&svc.resp.error_message, e.what());
			req.respond();
		}
		return 0;
	}
#endif
	virtual int on_unlink(ln::service_request& req, unlink_t& svc) {
		svc.resp.error_message_len = 0;

		string filename(svc.req.filename, svc.req.filename_len);
		printf("unlink %s\n", filename.c_str());

		if (access(filename.c_str(), F_OK) == 0 && unlink(filename.c_str())) { // existed, but delete failed
			char errmsg[1024];
			int len = snprintf(errmsg, 1024, "could not unlink file %s: errno %d, %s\n", repr(filename).c_str(), errno, strerror(errno));
			svc.resp.error_message = errmsg;
			svc.resp.error_message_len = len;
			printf("%s", errmsg);
		}

		req.respond();

		return 0;
	}

	void do_rmdir(const std::string& dirname, bool recursive) {
		if (access(dirname.c_str(), F_OK)) // does not exist
			return;

		if (recursive) {
			// delete contents, but only if this is not a symlink!
			struct stat sbuf;
			if (stat(dirname.c_str(), &sbuf))
				throw errno_exception("stat(%s)", repr(dirname).c_str());
			if ((sbuf.st_mode & S_IFMT) != S_IFLNK) {
				DIR* dp = opendir(dirname.c_str());
				if (!dp)
					throw errno_exception("opendir(%s)", repr(dirname).c_str());

				std::string base = dirname + "/";
				struct dirent* entry;
				while ((entry = readdir(dp))) {
					std::string d_name = entry->d_name;
					if (d_name == "." || d_name == "..")
						continue;
					std::string p = base + d_name;

					do {
						if (stat(p.c_str(), &sbuf))
							throw errno_exception("stat(%s)", repr(p).c_str());
						if ((sbuf.st_mode & S_IFMT) != S_IFLNK)
							break;
						char link_target[1024 * 2];
						ssize_t link_target_len = readlink(p.c_str(), link_target, sizeof(link_target));
						if (link_target_len == -1)
							throw errno_exception("readlink(%s)", repr(p).c_str());
						std::string lt(link_target, link_target_len);
						if(lt == p)
							throw str_exception_tb("symlink points to itself: %r", repr(p).c_str());
						p = lt;
					} while(true);
					if ((sbuf.st_mode & S_IFMT) == S_IFDIR)
						do_rmdir(p, true);
					else if(unlink(p.c_str()))
						throw errno_exception("could not unlink file %s", repr(p).c_str());
				}
				closedir(dp);
			}
		}
		if (rmdir(dirname.c_str()))
			throw errno_exception("could not rmdir %s", repr(dirname.c_str()).c_str());
	}

	virtual int on_rmdir(ln::service_request& req, rmdir_t& svc) {
		svc.resp.error_message_len = 0;

		string dirname(svc.req.dirname, svc.req.dirname_len);
		printf("rmdir (recursive: %d) %s\n", svc.req.recursive, dirname.c_str());

		try {
			do_rmdir(dirname, svc.req.recursive);
			req.respond();
		}
		catch(const std::exception& e) {
			ln::string_buffer err(&svc.resp.error_message, e.what());
			printf("%s\n", e.what());
			req.respond();
		}
		return 0;
	}
};

	template<>
	void file_services::fill_tree_info_symlink_target<ln_file_services_tree_info_t>(const std::string& pathname, ln_file_services_tree_info_t* info) {
		// empty
	}

	template<>
	void file_services::fill_tree_info_symlink_target<ln_file_services2_tree_info2_t>(const std::string& pathname, ln_file_services2_tree_info2_t* info) {
		char target_buf[512];
		ssize_t len = readlink(pathname.c_str(), target_buf, sizeof(target_buf));
		info->symlink_target = strndup(target_buf, len);
		info->symlink_target_len = len;
	}

	template<>
	void file_services::free_tree_info<ln_file_services_tree_info_t>(ln_file_services_tree_info_t* info) {
		for(unsigned int i = 0; i < info->childs_len; i++)
			free_tree_info(&info->childs[i]);
		free(info->childs);
		free(info->entry);
		free(info->error_message);
	}
	template<>
	void file_services::free_tree_info<ln_file_services2_tree_info2_t>(ln_file_services2_tree_info2_t* info) {
		for(unsigned int i = 0; i < info->childs_len; i++)
			free_tree_info(&info->childs[i]);
		free(info->childs);
		free(info->entry);
		free(info->symlink_target);
		free(info->error_message);
	}

	int file_services::on_get_tree_info(ln::service_request& req, get_tree_info_t& svc) {
		svc.resp.error_message_len = 0;
		vector<ln_file_services_tree_info_t> tree_infos;
		try {
			string pathnames_string(svc.req.pathnames, svc.req.pathnames_len);
			vector<string> pathnames = split_string(pathnames_string, ",");

			tree_infos.resize(pathnames.size());
			svc.resp.infos = &tree_infos[0];
			svc.resp.infos_len = tree_infos.size();
			memset(svc.resp.infos, 0, sizeof(svc.resp.infos[0]) * svc.resp.infos_len);
			for(unsigned int i = 0; i < pathnames.size(); i++)
				fill_tree_info(pathnames[i], &tree_infos[i], svc.req.max_depth);

			printf("first returned entry name: %*.*s with %d childs\n",
			       svc.resp.infos[0].entry_len, svc.resp.infos[0].entry_len, svc.resp.infos[0].entry,
			       svc.resp.infos[0].childs_len);
			req.respond();
		}
		catch(const exception& e) {
			printf("error in %s:\n%s\n", __func__, e.what());
			ln::string_buffer err(&svc.resp.error_message, e.what());
			req.respond();
		}

		for(unsigned int i = 0; i < tree_infos.size(); i++)
			free_tree_info(&tree_infos[i]);

		return 0;
	}

	int file_services::on_get_tree_info2(ln::service_request& req, get_tree_info2_t& svc) {
		svc.resp.error_message_len = 0;
		vector<ln_file_services2_tree_info2_t> tree_infos;
		try {
			string pathnames_string(svc.req.pathnames, svc.req.pathnames_len);
			vector<string> pathnames = split_string(pathnames_string, ",");

			tree_infos.resize(pathnames.size());
			svc.resp.infos = &tree_infos[0];
			svc.resp.infos_len = tree_infos.size();
			memset(svc.resp.infos, 0, sizeof(svc.resp.infos[0]) * svc.resp.infos_len);
			for(unsigned int i = 0; i < pathnames.size(); i++)
				fill_tree_info(pathnames[i], &tree_infos[i], svc.req.max_depth);
			req.respond();
		}
		catch(const exception& e) {
			printf("error in %s:\n%s\n", __func__, e.what());
			ln::string_buffer err(&svc.resp.error_message, e.what());
			req.respond();
		}

		for(unsigned int i = 0; i < tree_infos.size(); i++)
			free_tree_info(&tree_infos[i]);

		return 0;
	}


void on_signal(int signo) {
	if(signo == SIGTERM) {
		printf("\ngot SIGTERM, terminating...\n");
		keep_running = false;
	} else if(signo == SIGINT) {
		printf("\ngot SIGINT, terminating...\n");
		keep_running = false;
	}
}

int main(int argc, char* argv[]) {
	setlocale(LC_ALL, "C"); // we want consistent error messages
	ln::client clnt("file services", argc, argv);
	file_services fs(&clnt);
	
	// register SIGTERM handler for clean shutdown
	signal(SIGTERM, on_signal);
	signal(SIGINT, on_signal);
	
	return fs.run();
}
