// -*- mode: c++ -*-
#ifndef LN_PORTS
#define LN_PORTS

#include <deque>

#include <string_util/string_util.h>
#include "os.h"
#include "logger.h"

class ln_port;
#include "ln_instance.h"

#include <ln/ln.h>

#include "element_ring_buffer.h"

#include <assert.h>

using namespace std;
using namespace string_util;

typedef enum {
	SINK_PORT,
	SOURCE_PORT
} port_direction;

class thread_settings {
public:
	int prio;
	int policy;
	int affinity_mask;
	
	thread_settings(int prio=-1, int policy=-1, int affinity_mask=-1) :
		prio(prio),
		policy(policy),
		affinity_mask(affinity_mask) {
	}
	void apply();
};

class ln_port_interface {
public:
	virtual ~ln_port_interface() { }

	virtual string get_id() {
		return "<ln_port_interface>";
	}
	virtual py_tuple* get_tuple(py_tuple* t=NULL, int with_last_packet=0) = 0;
	virtual py_tuple* get_tuple_(py_tuple* t=NULL, int with_last_packet=0) = 0;
	/*
	virtual void get_state(request_t& answer) = 0;
	virtual void set_rate(float rate) = 0;
	virtual void get_last_packet(request_t& answer) = 0;
	*/

	virtual double& get_last_packet_source_time() = 0;
	virtual double& get_last_packet_time() = 0;
	virtual uint64_t& get_packet_count() = 0;
	virtual void*& get_last_packet() = 0;
	virtual unsigned int& get_size() = 0;
	virtual void _append_to_log(void* data, double* ts, uint32_t* counter) = 0;

	virtual void _need_thread_for_logging(bool needed) {

	}

};

class ln_port : public ln_port_interface {
public:
	uint64_t packet_count;
	void* last_packet;
	double last_packet_source_time;
	double last_packet_time;

	string port_type;
	ln_instance* instance;
	port_direction direction;
	unsigned int size;

	bool logging_enabled;
	double logging_end_time;
	element_ring_buffer log_buffer;
	bool had_buffer_filled;
	bool logging_only_ts;
	unsigned int logging_divisor;
	unsigned int logging_divisor_pos;

	ln_port(ln_instance* instance, string port_type, port_direction dir, unsigned int size);
	virtual ~ln_port() = 0;

	virtual string get_id() {
		//assert(0);
		// throw str_exception_tb("port of unknown type");
		return format_string("('unknown port', %p)", this);
	}
	virtual void get_state(request_t& answer) {
		throw str_exception_tb("not implemented on port %s", get_id().c_str());
	}
	virtual void set_rate(float rate) {
		throw str_exception_tb("not implemented on port %s", get_id().c_str());
	}
	virtual void get_last_packet_req(request_t& answer) {
		answer["success"] = repr("success");
		answer["last_packet"] = binary_data_repr((char*)last_packet, size);
		answer["last_packet_time"] = format_string("%f", last_packet_time);
		answer["packet_count"] = format_string("%llu", packet_count);
		answer["logging_enabled"] = format_string("%d", logging_enabled);
		answer["logging_size"] = format_string("%u", log_buffer.get_size());
		answer["logging_count"] = format_string("%llu", log_buffer.get_count());
		answer["logging_only_ts"] = format_string("%d", logging_only_ts);
		answer["logging_divisor"] = format_string("%d", logging_divisor);
	}
	virtual py_tuple* get_tuple(py_tuple* t=NULL, int with_last_packet=0);

	void start_logging(int max_samples, float max_time, bool only_ts, unsigned int divisor);
	void stop_logging();
	void retrieve_log(request_t& answer);
	void retrieve_log_data(request_t& answer, std::string& data);
	void _append_to_log(void* data, double* ts, uint32_t* counter);
};


class ln_sink_port;
/*
  source ports have their own thread, who's sole work is the distribution of new packets to all connected sink-ports
  source ports have no rate limiters! they process in the rate of the writing-client.
 */
class ln_shm_source;
class ln_source_port : public ln_port_interface {
	friend class ln_shm_source;
	friend class ln_sink_port;
	os::thread_handle_t thread_handle;
#if defined(__LINUX__) || defined(__VXWORKS__)
	pid_t tid;
#endif	
	os::mutex_handle_t sinks_mutex;
	bool started;
	typedef list<ln_sink_port*> sinks_t;
	sinks_t sinks;
	unsigned int _n_logs_sink_usage;

#ifdef __WIN32__
	static DWORD __stdcall _static_thread(void* data);
#else
	static thread_return_t _static_thread(void* data);
#endif
	void* _thread();

	sinks_t::iterator _find_sink(ln_sink_port* s);

	// rate estimation info
	deque<pair<double, uint64_t> > rate_est_info;
	unsigned int rate_est_info_size;
	double last_rate_est_time;

protected:
	uint32_t packet_counter;
	double packet_timestamp;
	thread_settings ts;

	void start();
	void stop();
	virtual int generate_packet(void* data, double timeout=-1) = 0; // fills packet_counter and packet_timestamp
	
public:
	ln_source_port(thread_settings* ts=NULL);
	virtual ~ln_source_port();

	void add_sink(ln_sink_port* s);
	bool remove_sink(ln_sink_port* s);
	void _list_sinks();

	void get_state(request_t& answer);
	double _estimate_rate();
	py_tuple* get_tuple(py_tuple* t, int with_last_packet=0);

	virtual void sink_port_get_state(request_t& answer) {};	
};


class ln_sink_port : public ln_port_interface {
protected:
	ln_source_port* source;
	double allowed_wave_length;

	bool can_consume;
	unsigned int could_not_consume;

 public:
	ln_sink_port(float rate);
	virtual ~ln_sink_port();

	void set_source(ln_source_port* source) {
		this->source = source;
	}
	ln_source_port* get_source() {
		return this->source;
	}

	virtual void set_rate(double rate) {
		log("sink port with rate %f\n", rate);

		if(rate <= 0)
			allowed_wave_length = -1;
		else
			allowed_wave_length = 1. / rate;
		log("sink allowed wave length: %f\n", allowed_wave_length);
	}

	void _consume_packet(double ts, uint32_t counter, void* data);
	virtual void consume_packet(double ts, uint32_t counter, void* data) = 0;
	void get_state(request_t& answer);
	py_tuple* get_tuple(py_tuple* t, int with_last_packet=0);

};

#include "ln_udp_ports.h"
#include "ln_tcp_ports.h"
#include "ln_shm_ports.h"

#endif // LN_PORTS
