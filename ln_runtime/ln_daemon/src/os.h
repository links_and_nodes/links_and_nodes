#ifndef OS_H
#define OS_H

#include "config.h"

#ifdef __LINUX__
#include "os_linux.h"
#endif

#ifdef __QNX__
#include "os_qnx.h"
#endif

#ifdef __VXWORKS__
#include "os_vxworks.h"
#endif

#ifdef __WIN32__
#include "os_win32.h"
#endif

#ifdef HAVE_GETPWUID
#include <pwd.h>
#endif

#define ARRAY_LENGTH(arr) (sizeof((arr)) / sizeof((arr)[0]))

namespace os {

//! kill process with given pid
/*!
  \param pid process pid to kill
  \return success or error code
 */
int kill(pid_t pid);

//! send signal to process with given pid
/*!
  \param pid process pid to signal
  \parma signo signal number to send
  \return success or error code
 */
int send_signal(pid_t pid, int signo);

}

// ln-c-api
extern "C" {

	double ln_get_time();
	
	double ln_get_monotonic_time();

	// unnamed semaphore
	int ln_semaphore_init(ln_semaphore_handle_t* handle, unsigned int value);
	int ln_semaphore_destroy(ln_semaphore_handle_t handle);
	// named semaphore
	int ln_semaphore_create(ln_semaphore_handle_t* handle, const char* name, int create);
	int ln_semaphore_close(ln_semaphore_handle_t);
	int ln_semaphore_unlink(const char* name);
	// common semaphores
	int ln_semaphore_wait(ln_semaphore_handle_t);
	int ln_semaphore_trywait(ln_semaphore_handle_t);
	int ln_semaphore_timedwait(ln_semaphore_handle_t, double seconds);
	int ln_semaphore_post(ln_semaphore_handle_t handle);
	int ln_semaphore_getvalue(ln_semaphore_handle_t handle);

	void _ln_close_socket(int fd);

	std::string get_windows_error();
}

#endif // OS_H
