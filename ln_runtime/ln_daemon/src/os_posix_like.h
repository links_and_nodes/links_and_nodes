#ifndef __OS_POSIX_LIKE__
#define __OS_POSIX_LIKE__

#include <pthread.h>
#include <sys/wait.h> 
#include <sys/utsname.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <semaphore.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <iostream>

#define THREAD_CALL
#define thread_cleanup_push pthread_cleanup_push
#define thread_cleanup_pop pthread_cleanup_pop

typedef struct ln_pipe {
	int input_fd;
	int output_fd;
} ln_pipe_t;
#define ln_pipe_invalid -1

typedef sem_t* ln_semaphore_handle_t;

namespace os {

//! set current thread priority and scheduling policy
/*!
  \param policy new thread scheduling policy
  \param priority new thread priority
  \return N/A
*/
void set_own_prio(int policy, int priority, int affinity);
unsigned int get_max_number_of_fds();

typedef pthread_mutex_t mutex_handle_t;
int create_mutex(mutex_handle_t* mutex_handle);
int destroy_mutex(mutex_handle_t* mutex_handle);
int lock_mutex(mutex_handle_t* mutex_handle);
int trylock_mutex(mutex_handle_t* mutex_handle);
int unlock_mutex(mutex_handle_t* mutex_handle);

typedef pthread_cond_t condition_handle_t;
void condition_create(condition_handle_t* handle);
void condition_destroy(condition_handle_t* handle);
void condition_wait(condition_handle_t* cond, mutex_handle_t* mutex);
void condition_signal(condition_handle_t* handle);
void condition_broadcast(condition_handle_t* handle);

}

#define closesocket close

#endif // __OS_POSIX_LIKE__

