#ifndef LINUX_PROCESS_LISTING_H
#define LINUX_PROCESS_LISTING_H

#include <string>

std::string linux_process_listing(bool with_env, bool with_threads, std::string filter);

#endif // LINUX_PROCESS_LISTING_H
