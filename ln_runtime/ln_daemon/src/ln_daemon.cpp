/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include "os.h"

#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifdef __VXWORKS__
#include <string.h>
#include <selectLib.h>
#include <rtpLib.h>
// needs to be commented-out for vxworks6.9.3-x86-gcc4.x: extern "C" void bzero(char *, int);
#endif

#include <string_util/string_util.h>
#include "logger.h"
#include "util.h"
#include "ln_daemon.h"

#ifdef __VXWORKS__	
#include "ln_helper.h"
#endif

#include "version.h"

using namespace string_util;

static class ln_daemon* self = NULL; // ln_daemon is a signleton!

config_class* config = NULL;

ln_daemon::ln_daemon() {
	_last_sigchild_check = 0;
	_sigchild_check_interval = 1;
	timeout_counter = 0;
	self = this; // ln_daemon is a sigleton!
	fd = -1;
	{
		const char* no_hide_str = getenv("LN_DAEMON_NO_HIDE");
		if(no_hide_str && tolower(no_hide_str[0]) == 'y')
			no_hide = true;
	}
	config = new config_class();

	raise_resource_limits();
}

ln_daemon::~ln_daemon() {
#ifdef __VXWORKS__
	vxworks_free_ln_helper();
#endif
}

#if defined(__WIN32__) || defined(__VXWORKS__)
void ln_daemon::raise_resource_limits() {
}
#else
#include <sys/resource.h>

void ln_daemon::raise_resource_limits() {
	// raise soft resource limit to their hard limits!
	int resources[] = {
		RLIMIT_MEMLOCK,
		RLIMIT_STACK,
		RLIMIT_CORE,
#ifdef __LINUX__		
		RLIMIT_NICE,
		RLIMIT_RTPRIO,
		15, // RLIMIT_RTTIME,
#endif		
		-1
	};
	for(unsigned int i = 0; i < sizeof(resources) / sizeof(resources[0]); ++i) {
		int resource = resources[i];
		if(resource == -1)
			break;
		struct rlimit limit;
		int ret = getrlimit(resource, &limit);
		if(ret)
			continue;
		limit.rlim_cur = limit.rlim_max;
		ret = setrlimit(resource, &limit);
		if(ret)
			log("error setting resource limit for resource %d: %d %s\n",
			    resource, errno, strerror(errno));
	}
}
#endif

void ln_daemon::reset_last_sigchild_check() {
	_last_sigchild_check = 0;
}

void ln_daemon::stop() {
	running = false;
}

#ifdef __WIN32__
thread_return_t THREAD_CALL ln_daemon::_select_thread(void* data) {
	ln_daemon* self = (ln_daemon*)data;
	self->select_thread();
	return NULL;
}

void ln_daemon::select_thread() {
	log("select thread running!\n");
	while(running) {
		int max_fd = 0;

		lock_mutex(&select_mutex);		
		FD_ZERO(&read_fds);
		FD_ZERO(&write_fds);
		FD_ZERO(&except_fds);
		
		for(io_watches_t::iterator i = io_watches.begin(); i != io_watches.end(); i++) {
			int fd = i->first;
			struct io_watch& w = i->second;
			if(w.flags & IO_WATCH_READ)
				FD_SET(fd, &read_fds);
			if(w.flags & IO_WATCH_WRITE)
				FD_SET(fd, &write_fds);
			if(w.flags & IO_WATCH_EXCEPT)
				FD_SET(fd, &except_fds);
			if(fd > max_fd)
				max_fd = fd;
			// log("select thread have fd %d\n", fd);
		}
		struct timeval tv;
		struct timeval* tvp = &tv;
		_get_until_next_timeout_expiration(&tvp);
		// log("select thread will block with tvp %p, max_fd %d...\n", tvp, max_fd);
		int n = select(max_fd + 1, &read_fds, &write_fds, &except_fds, tvp);
		// log("select thread unblocks: %d\n", n);
		if(n == -1)
			throw errno_exception_tb("select() error!");

		if(n)
			_have_socket_event = true;
		unlock_mutex(&select_mutex);

		// trigger main thread
		// log("select thread trigger main thread!\n", n);
		_need_finished_post = true;
		ln_semaphore_post(main_semaphore);
		// log("select thread waiting for main thread to finish!\n", n);
		ln_semaphore_wait(main_thread_finished);
		// log("select thread main thread is finished!\n", n);
	}
	return;
}
#endif

static void usage_exit(const char* argv0, int retval=0)
{
	printf("\n"
	       "usage: %s [-d] [-f] [-l FILE] [-a] [-qa] [-slow] [-fake_uid UID] [-pidfile FILE] [-public_key_file FILE] [-arch ARCH]\n"
	       " -f                     stay in foreground and log to stdout\n"
	       " -l FILE                log also into specified file\n"
	       " -a                     only start arbiter, list daemons\n"
	       " -qa                    quit arbiter, iff running\n"
	       " -fake_uid UID          pretend to run with a different uid\n"
	       " -pidfile FILE          write own pid into given pidfile\n"
	       " -public_key_file FILE  only accept manager connections with private key matching this public key\n"
	       "                        or set FILE to `never` to never allow this daemon to be locked!\n"
	       " -debug                 enable additional debug output\n"
	       " -arch ARCH             overwrite compiled-in reported architecure of this daemon\n"
	       " -p PORT                listen on specified port for manager connection (instead of random one)\n"
	       " -ap PORT               connect/start daemon arbiter on at specified port number (instead of %d)\n"
	       " --isolated-test        start new, private ln_daemon process with random free port number\n"
	       " -d                     does nothing. only for backwards compatibility\n"
	       " -slow                  only for development: sleeps 2 seconds after each received request\n"
	       "\n", argv0, LN_ARBITER_PORT);
	exit(retval);
}

int ln_daemon::run(int argc, char* argv[]) {
	log("ln_daemon %s", LN_RUNTIME_VERSION);
	int skip = 0;
	bool qnx_close_stdout = false;

	arch = DLRRM_ARCH; // compiled arch

	char* arch_env = getenv("DLRRM_HOST_PLATFORM"); // dlr arch from env
	if(arch_env)
		arch = arch_env;

	config->isolated_test = false;
	isolated_test_had_mgr_connection = false;
	config->background = false;
	config->log_stdout = false;
	config->port = 0;

#ifndef __WIN32__
	signal(SIGPIPE, SIG_IGN);
#endif
	
	if(argc > 1 && !strcmp(argv[1], "child")) {
		// for qnx
		fd = atoi(argv[2]);
		arbiter_client.fd = atoi(argv[3]);
		if(argc > 4) {
			config->public_key_file = argv[4];
			locked_by = "command line";
		}
		
		config->log_stdout = false;
		config->background = false;
		config->log_file = "/tmp/ln_daemon.log";
		qnx_close_stdout = true;

	} else if(argc > 1 && !strcmp(argv[1], "arbiter_child")) {
		// for arbiter child for vxworks & qnx
#ifdef __WIN32__
		printf("i am the arbiter child!\n");
		WSADATA wsaData = {0};
		DWORD iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0)
			throw str_exception_tb("WSAStartup fauled: %d", iResult);
#endif

		ln_arbiter arbiter;
		if(argc > 2)
			arbiter.run(atoi(argv[2]));
		else
			arbiter.run();
		return 0;
	} else {
		bool start_arbiter_only = false;
		bool quit_arbiter_only = false;
		
		config->background = true;
		config->log_stdout = false;
		
		for(int i = 1; i < argc; i++) {
			if(skip) {
				skip --;
				continue;
			}
			string arg = argv[i];
			if(arg == "--isolated-test") {
				config->isolated_test = true;
				log("starting in isolated mode");
			} else if(arg == "-debug")
				debug_enabled = true;
			else if(arg == "-public_key_file" && (i + 1) < argc) {
				config->public_key_file = argv[i + 1];
				locked_by = "command line";
				skip = 1;
			} else if(arg == "-p" && (i + 1) < argc) {
				config->port = atoi(argv[i + 1]);
				skip = 1;
			} else if(arg == "-ap" && (i + 1) < argc) {
#ifdef __WIN32__
				if(atoi(argv[i + 1]) != LN_ARBITER_PORT) {
					printf("error, win32 does not support setenv(). please set env var LN_ARBITER_PORT=PORT instead of using -ap PORT\n");
					exit(1);
				}
#else
				setenv("LN_ARBITER_PORT", argv[i + 1], 1);
#endif
				skip = 1;
			} else if(arg == "-a")
				start_arbiter_only = true;
			else if(arg == "-qa")
				quit_arbiter_only = true;
			else if(arg == "-f") {
				config->log_stdout = true;
				config->background = false;
			} else if(arg == "-d") {
				// nothing, only for backward compatibility
			} else if(arg == "-l" && (i + 1) < argc) {
				config->log_file = argv[i + 1];
				skip = 1;
			} else if(arg == "-pidfile" && (i + 1) < argc) {
				config->pid_file = argv[i + 1];
				skip = 1;
			} else if(arg == "-slow")
				config->slow_mode = true;
			else if(arg == "-fake_uid") {
				config->fake_uid = atoi(argv[i + 1]);
				skip = 1;
			} else if(arg == "-arch") {
				arch = argv[i + 1];
				skip = 1;
			} else if(arg == "--help")
				usage_exit(argv[0]);
			else  {
				printf("error: unknown argument '%s'\n", arg.c_str());
				usage_exit(argv[0], 1);
			}
		}

#ifdef __WIN32__
		WSADATA wsaData = {0};
		DWORD iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0)
			throw str_exception_tb("WSAStartup fauled: %d", iResult);
#endif

		arbiter_client.program_name = argv[0];
		arbiter_client.fake_uid = config->fake_uid;
		
		if(quit_arbiter_only) {
			log("requesting arbiter to quit!\n");
			arbiter_client.quit_arbiter();
 			return 0;
		}

		if(start_arbiter_only) {
			log("requesting list of running daemons from arbiter...\n");
			list<string> daemons = arbiter_client.list_daemons();
			log("there are %d running daemons.", daemons.size());
			unsigned int k = 0;
			for(list<string>::iterator i = daemons.begin(); i != daemons.end(); i++) {
				log("  %d. %s\n", ++k, i->c_str());
			}
			return 0;
		}

		fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
		if(fd == -1)
			throw errno_exception("socket");
		
#ifdef __VXWORKS__
		/*
		  did not work on sigyn:
		  setsockopt(SO_REUSEADDR): invalid argument (errno 22)
		  char on = 1;
		  if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0)
		  throw errno_exception("setsockopt(SO_REUSEADDR)");
		*/
#elif __WIN32__
		char on = 1;
		if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0)
			throw errno_exception("setsockopt(SO_REUSEADDR)");
#else
		int on = 1;
		if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0)
			throw errno_exception("setsockopt(SO_REUSEADDR)");
#endif

#if !defined(__WIN32__)
		if(config->port)
#endif
		{
			// win32 always needs a bind"!
			struct sockaddr_in addr;
			addr.sin_family = AF_INET;
			addr.sin_port = htons(config->port);
			addr.sin_addr.s_addr = INADDR_ANY;
			if(bind(fd, (struct sockaddr*) &addr, sizeof(addr)))
				throw errno_exception_tb("bind");
		}

		if(listen(fd, 5))
			throw errno_exception_tb("listen");

		// create listening socket
		struct sockaddr_in server_address;
		os::socklen_t_type server_address_len = sizeof(server_address);
		if(getsockname(fd, (struct sockaddr*)&server_address, &server_address_len) == -1)
			throw errno_exception("getsockname");
		unsigned int daemon_port = ntohs(server_address.sin_port);
		log("daemon listening on port %d fd %d!", daemon_port, fd);


		if(config->isolated_test == false && !arbiter_client.register_daemon(daemon_port)) {
			// register failed!
			// probably there already is another ln_daemon for this user!
			return -1;
		}

	}

	add_io_watch(fd, IO_WATCH_READ, on_new_manager_connection, this, "incoming manager connection");

#ifdef __ANDROID__
	log("creating unix domain socket for shm-fd-communication on android!. incoming manager connection fd: %d\n", fd);
	int old_umask = umask(0);
	int unix_fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if(unix_fd == -1)
		throw errno_exception_tb("socket(AF_UNIX, SOCK_STREAM, 0)");
	unlink(ANDROID_FD_UNIX_SOCKET);
	struct sockaddr_un unix_addr;
	memset(&unix_addr, 0, sizeof(unix_addr));
	unix_addr.sun_family = AF_UNIX;
	strcpy(unix_addr.sun_path, ANDROID_FD_UNIX_SOCKET);
	if(bind(unix_fd, (struct sockaddr*)&unix_addr, sizeof(unix_addr)) == -1)
		throw errno_exception_tb("bind(unix_fd, %s)", ANDROID_FD_UNIX_SOCKET);
	if(listen(unix_fd, 10) == -1)
		throw errno_exception_tb("listen(unix_fd, 10)");
	umask(old_umask);

	add_io_watch(unix_fd, IO_WATCH_READ, on_new_unix_connection, this, "unix socket for android fd communication");
#endif


	if(config->background) {
#if defined(__VXWORKS__) || defined(__WIN32__)
		log("background is not yet implemented on vxworks/win32!");
#else
#if defined(__QNX__)
		// fork does not work on QNX - btw. you can not gdb-attach a forked-child!

		// redirect input from /dev/null
		// int null_input_fd = open("/dev/null", O_RDONLY);


		struct inheritance inh;
		// SPAWN_EXPLICIT_SCHED 
		inh.flags = SPAWN_ALIGN_DEFAULT | SPAWN_SETSIGDEF | SPAWN_TCSETPGROUP | SPAWN_SETGROUP;
		if(argv[0][0] != '/' && argv[0][0] != '.') /*
							     it's neither an absolute nor a relative path -- need to search PATH!
							     https://rmc-github.robotic.dlr.de/common/links_and_nodes/issues/331
							   */
			inh.flags |= SPAWN_SEARCH_PATH;
		inh.pgroup = SPAWN_NEWPGROUP;
		char fd_str[16];
		snprintf(fd_str, 16, "%d", fd);
		char fd2_str[16];
		snprintf(fd2_str, 16, "%d", arbiter_client.fd);
		char key_file[1024];
		strncpy(key_file, config->public_key_file.c_str(), 1024);
		char* const spawn_argv[] = { argv[0], "child", fd_str, fd2_str, key_file, NULL };
		pid_t pid = spawn(spawn_argv[0], 0, NULL, &inh, spawn_argv, NULL);
		if(pid == -1) {
			printf("trying to start arbiter from '%s': spawn failed: errno %d: %s\n", argv[0], errno, strerror(errno));
			return -1;
		}
		if(config->pid_file.size()) {
			FILE* fp = fopen(config->pid_file.c_str(), "wb");
			if(fp) {
				fprintf(fp, "%d", pid);
				fclose(fp);
			}
		}
		// printf("parent exiting\n");
		return 0;
#else
		// daemonize
		pid_t pid = fork();
		if(pid != 0) {
			if(config->pid_file.size()) {
				FILE* fp = fopen(config->pid_file.c_str(), "wb");
				if(fp) {
					fprintf(fp, "%d", pid);
					fclose(fp);
				}
			}
			return 0;
		}
		if(config->isolated_test == false)
			arbiter_client.update_pid();
		signal(SIGPIPE, SIG_IGN);

		// close all file deskriptors
		int table_size = os::get_max_number_of_fds();
		for(int i = 0; i < table_size; i++) {
			if(i == arbiter_client.fd)
				continue;
			if(io_watches.find(i) != io_watches.end())
				continue;
		  	close(i);
		}
		// redirect input from /dev/null
		int fd = open("/dev/null", O_RDONLY);
		if(fd == -1)
			throw errno_exception("open /dev/null");
		dup2(fd, 0); // stdin
		// redirect output to
#ifdef __ANDROID__
		char* redirect_output = "/dev/background_ln_daemon.log";
#else
		char* redirect_output = (char*)"/tmp/background_ln_daemon.log";
#endif
		if(!access(redirect_output, W_OK)) {
			fd = open(redirect_output, O_WRONLY | O_APPEND);
			if(fd == -1)
				throw errno_exception("open redirect_output O_APPEND '%s'", redirect_output);
		} else {
			int old_mask = umask(0);
			fd = open(redirect_output, O_WRONLY | O_CREAT, 0666);
			if(fd == -1)
				throw errno_exception("open redirect_output O_CREAT '%s'", redirect_output);
			umask(old_mask);
		}
		dup2(fd, 1); // stdout
		dup2(fd, 2); // stderr
		if(setsid() == -1)
			throw errno_exception("setsid");
#endif
#endif
	} else {
		arbiter_client.update_pid();
	}

	root_logger->write_to_stdout = config->log_stdout;
	if(config->log_file != "")
		root_logger->open_logfile(config->log_file);

	load_public_key();
#if defined(__LINUX__) || defined(__QNX__) || defined(__VXWORKS__)
	raise_resource_limits();
#endif

	if(qnx_close_stdout) {
		// such that ssh can exit
		close(0);
		close(1);
		close(2);
	}
	
#ifndef WIN32
	// do not pass this socket fd to our exec/childs!
	if(fcntl(fd, F_SETFD, fcntl(fd, F_GETFD, 0) | FD_CLOEXEC) == -1)
		log("could not F_SETFD FD_CLOEXEC for our listening socket fd %d: %s", errno, strerror(errno));
#endif

	register_sigchild();
#ifndef WIN32
	signal(SIGPIPE, SIG_IGN);
	signal(SIGHUP, &on_sigint);
#endif

	signal(SIGINT, &on_sigint);
	signal(SIGTERM, &on_sigint);
	running = true;
	_sigchild_pending = false;

#ifdef __WIN32__
	// win32 can not block on select because we need to react on socket events and on pipe-handle-events!
	int ret;

	ret = ln_semaphore_create(&main_semaphore, NULL, 1);
	if(ret)
		throw errno_exception("could not create mainloop-semaphore!");

	ret = ln_semaphore_create(&main_thread_finished, NULL, 1);
	if(ret)
		throw errno_exception("could not create mainloop-finished-semaphore!");

	// create one thread which call select() to block on sockets
	// each process has its own output-thread
	// these threads trigger this mainloop by a semaphore!

	if(create_mutex(&select_mutex))
		throw errno_exception("could not create select-mutex!");

	_have_process_output = false;
	_have_socket_event = false;
	thread_handle_t select_thread_tid;
	if((ret = create_thread(&select_thread_tid, _select_thread, this)))
		throw retval_exception_tb(ret, "could not create select thread pthread_create()!");
	
	// log("main thread entering main loop");
	while(running) {
		// log("main thread blocking");
		WaitForSingleObject(main_semaphore, INFINITE);
		// log("main thread unblocked!");

		_check_timeouts();
		/*
		if(ret != WAIT_OBJECT_0 && !_sigchild_pending && !_have_socket_event) { // timeout
			log("main thread: was just a timeout!");
			continue;
		}
		*/
		// log("main thread checking for event source!");

		// handle socket events
		if(_have_socket_event) {
			_have_socket_event = false; // no race!!
			// log("main thread it was a socket event!");

			lock_mutex(&select_mutex);
			restart_io_watch_iterator = false;
			for(io_watches_t::iterator i = io_watches.begin(); i != io_watches.end(); i++) {
				int fd = i->first;
				struct io_watch& w = i->second;
				int why = 0;
				if(w.flags & IO_WATCH_READ && FD_ISSET(fd, &read_fds))
					why |= IO_WATCH_READ;
				if(w.flags & IO_WATCH_WRITE && FD_ISSET(fd, &write_fds))
					why |= IO_WATCH_WRITE;
				if(w.flags & IO_WATCH_EXCEPT && FD_ISSET(fd, &except_fds))
					why |= IO_WATCH_EXCEPT;
				if(!why)
					continue;
				try {
					w.callback(fd, why, w.data);
					if(restart_io_watch_iterator) {
						restart_io_watch_iterator = false;
						i = io_watches.begin();
						if(i == io_watches.end())
							break;
					}
				}
				catch(exception& e) {
					log("uncaught exception in io_watch on fd %d (%s): %s", fd, w.hint.c_str(), e.what());
				}
			}
			unlock_mutex(&select_mutex);
			// log("main thread socket event finished!");
		}

		// handle process output events!
		if(_have_process_output) {
			_have_process_output = false; // race condition?
			// log("main thread it was a process output event!");
			for(process_watches_t::iterator i = process_watches.begin(); i != process_watches.end(); i++) {
				process_watch& w = i->second;
				ln_process* p = (ln_process*)w.data;
				if(p->output_buffer->get_size())
					p->send_output_buffer();
			}
		}

		// handle child process termination events
		{
			self->_sigchild_pending = false;
			_last_sigchild_check = ln_get_monotonic_time();
			// log("main thread checking for terminated process!");
			for(process_watches_t::iterator i = process_watches.begin(); i != process_watches.end(); i++) {
				process_watch& w = i->second;
				ln_process* p = (ln_process*)w.data;
				DWORD exit_code;
				BOOL ret = GetExitCodeProcess(p->win32_process_handle, &exit_code);
				if(!ret) {					
					log("error from GetExitCodeProcess(): %s!", get_windows_error().c_str());
				} else {
					if(exit_code != STILL_ACTIVE) {
						try {
							w.callback(p->pid, exit_code, w.data);
						}
						catch(exception& e) {
							log("uncaught exception in process_watch on pid %d: %s", p->pid, e.what());
						}
						i = process_watches.begin();
						if(i == process_watches.end())
							break;
					}
				}
			}
		}
		if(_need_finished_post) {
			_need_finished_post = false;
			ln_semaphore_post(main_thread_finished);
		}

	}

#else // LINUX, QNX, VxWORKS, ANDROID

	while(running) {
		fd_set read_fds, write_fds, except_fds;
		int max_fd = update_iowatch_fd_sets(read_fds, write_fds, except_fds);

		struct timeval tv;
		struct timeval* tvp = &tv;
		_get_until_next_timeout_expiration(&tvp);

		/*
		struct timeval tv2;
		memcpy(&tv2, tvp, sizeof(struct timeval));
		double select_start = ln_get_monotonic_time();
		*/
		
		int n = select(max_fd + 1, &read_fds, &write_fds, &except_fds, tvp);

		/*
		double select_d = ln_get_monotonic_time() - select_start;
		log("select unblocked after %6.3fs with commanded timeout of %.3fs and max_fd of %d: %d\n",
			select_d, 1e-6*tv2.tv_usec + tv2.tv_sec, max_fd, n);
		*/

		if(n == -1 && errno != EINTR && !(_sigchild_pending && errno == 3)) {
			log("select error %s: currently registered file descriptors (max %d):", strerror(errno), max_fd);
			for(io_watches_t::iterator i = io_watches.begin(); i != io_watches.end(); ++i) {
				int fd = i->first;
				struct io_watch& w = i->second;
				log("| %3d: %s%s%s: %s",
					fd,
					w.flags & IO_WATCH_READ ? "read " : "",
					w.flags & IO_WATCH_WRITE ? "write " : "",
					w.flags & IO_WATCH_EXCEPT ? "except " : "",
					w.hint.c_str());
			}
			if(errno == 9) {
				log("try to sleep a little and then try again...\n");
				os::sleep(1);
				continue;
			}
			throw errno_exception("select");
		}
		
		if(_sigchild_pending || 
				((ln_get_monotonic_time() - _last_sigchild_check) >= _sigchild_check_interval)) {
				
			sigchild_check();
			continue;
		}

		_check_timeouts();

		if(n < 1)
			continue;
			
		restart_io_watch_iterator = false;
		for(io_watches_t::iterator i = io_watches.begin(); i != io_watches.end(); i++) {
			struct io_watch& w = i->second;
			int why = 0;
			if(w.fd > max_fd) 
				continue;
			if(w.flags & IO_WATCH_READ && FD_ISSET(w.fd, &read_fds))
				why |= IO_WATCH_READ;
			if(w.flags & IO_WATCH_WRITE && FD_ISSET(w.fd, &write_fds))
				why |= IO_WATCH_WRITE;
			if(w.flags & IO_WATCH_EXCEPT && FD_ISSET(w.fd, &except_fds))
				why |= IO_WATCH_EXCEPT;
			if(!why)
				continue;

			/* clear bits in fd_sets */
			FD_CLR(w.fd, &read_fds); 
			FD_CLR(w.fd, &write_fds); 
			FD_CLR(w.fd, &except_fds); 

			try {
				w.callback(w.fd, why, w.data);
			} catch(exception& e) {
				log("uncaught exception in local_io_watch on fd %d (%s): %s", 
						w.fd, w.hint.c_str(), e.what());
				remove_io_watch(w.fd);
			}

			if (restart_io_watch_iterator) {
				i = io_watches.begin();
				if(i == io_watches.end())
					break;
			}
		}
	}
#endif

	log("ln_daemon exiting!");
	arbiter_client.unregister_from_arbiter();
	signal(SIGINT, SIG_DFL);
	signal(SIGTERM, SIG_DFL);

	for(manager_connections_t::iterator i = manager_connections.begin(); i != manager_connections.end(); i++) {
		delete *i;
	}
	manager_connections.clear();

	for(instances_t::iterator i = instances.begin(); i != instances.end(); i++) {
		ln_instance* inst = i->second;
		delete inst;
	}
	instances.clear();
	
	// delete manager_connection;
	return 0;
}

//! updates iowatch fd_sets
/*!
 \param read_fds   fd set with read file descriptors
 \param write_fds  fd set with write file descriptors
 \param except_fds fd set with except file descriptors
 \return           maximum fd number
 */
int ln_daemon::update_iowatch_fd_sets(fd_set& read, fd_set& write, fd_set& except) {
	int max_fd = 0;
	FD_ZERO(&read);
	FD_ZERO(&write);
	FD_ZERO(&except);

	for(io_watches_t::iterator i = io_watches.begin(); i != io_watches.end(); i++) {
		struct io_watch& w = i->second;
		if (fd >= FD_SETSIZE)
			continue;
		if (w.flags & IO_WATCH_READ)
			FD_SET(w.fd, &read);
		if (w.flags & IO_WATCH_WRITE)
			FD_SET(w.fd, &write);
		if(w.flags & IO_WATCH_EXCEPT)
			FD_SET(w.fd, &except);
		max_fd = max(w.fd, max_fd);
	}

	return max_fd;
}

#ifndef WIN32

//! check for child termination
/*!
 \return N/A
 */
void ln_daemon::sigchild_check() {
	_sigchild_pending = false;
	_last_sigchild_check = ln_get_monotonic_time();

	while(true) {
		int status = 0;
		pid_t pid = waitpid(-1, &status, WNOHANG);
		if (pid == 0)
			break; // nothing to do
		if (pid == -1) { 
			if (errno != EINTR && errno != ECHILD)
				log("ln_daemon waitpid returned -1: %s\n", strerror(errno));
			break; // nothing to do
		}

		process_watches_t::iterator i = process_watches.find(pid);
		if(i == process_watches.end()) {
			log("ln_daemon received unknown terminated process with pid %d and status %d", pid, status);
		} else {
			process_watch& w = i->second;
			log("ln_daemon calling sigchild handler\n");
			try {
				w.callback(pid, status, w.data);
			}
			catch(exception& e) {
				log("uncaught exception in process_watch on pid %d: %s", pid, e.what());
			}
		}
	}
}

#endif

void ln_daemon::on_new_manager_connection(int fd, int why, void* data) {
	ln_daemon* self = (ln_daemon*) data;

	if(why & IO_WATCH_EXCEPT) {
		log("exception on listening socket fd%d. closing: %d, %s", fd, errno, strerror(errno));
		// self->running = false;
		return;
	}
	if(!(why & IO_WATCH_READ))
		return;

	struct sockaddr_in client_addr;
	os::socklen_t_type client_addr_len = sizeof(client_addr);
	int manager_fd = accept(fd, (struct sockaddr*)&client_addr, &client_addr_len);
	if(manager_fd == 0) {
		log("error, accept() returned 0-manager filedescriptor!\n");
	}
	if(manager_fd == -1)
		log("error, accept() returned -1-manager filedescriptor: listen fd: %d, errno %d %s\n", fd, errno, strerror(errno));
	const char* manager_host = inet_ntoa(client_addr.sin_addr);
	if(config->isolated_test && self->isolated_test_had_mgr_connection) {
		log("denying new manager connection from %s:%d\n", manager_host, ntohs(client_addr.sin_port));
		close(manager_fd);
		return;
	}
	ln_manager_connection* conn = new ln_manager_connection(self, manager_fd, manager_host);
	self->manager_connections.push_back(conn);
	log("new connection from manager on %s:%d at fd%d\n", manager_host, ntohs(client_addr.sin_port), manager_fd);

	if(config->isolated_test) {
		self->isolated_test_had_mgr_connection = true;
		log("isolated-test: will not allow any further ln_manager connections\n");
	}
}

#ifdef __ANDROID__
void ln_daemon::on_new_unix_connection(int fd, int why, void* data) {
	ln_daemon* self = (ln_daemon*) data;

	if(why & IO_WATCH_EXCEPT) {
		log("exception on listening socket unix fd %d. closing: %d, %s", fd, errno, strerror(errno));
		// self->running = false;
		return;
	}
	if(!(why & IO_WATCH_READ))
		return;

	struct sockaddr_un client_addr;
	os::socklen_t_type client_addr_len = sizeof(client_addr);
	int client_fd = accept(fd, (struct sockaddr*)&client_addr, &client_addr_len);
	if(client_fd == 0)
		log("error, accept() returned 0 - unix filedeskriptor!\n");
	log("new unix connection at fd %d from %s\n", client_fd, repr(client_addr.sun_path).c_str());
	self->add_io_watch(client_fd, IO_WATCH_READ, on_unix_request, self, 
			   format_string("unix socket for client %s", repr(client_addr.sun_path).c_str()).c_str());
}
void ln_daemon::on_unix_request(int unix_fd, int why, void* data) {
	ln_daemon* self = (ln_daemon*) data;

	if(why & IO_WATCH_EXCEPT) {
		log("exception on listening socket unix-client fd %d. closing: %d, %s", unix_fd, errno, strerror(errno));
		// self->running = false;
		return;
	}
	if(!(why & IO_WATCH_READ))
		return;

	// there is data to read!
	char command[1024];
	ssize_t n = recv(unix_fd, &command, sizeof(command), 0);

	if(n < 1) {
		if(n == -1)
			log("error on unix client fd %d: %d %s - closing socket!", unix_fd, errno, strerror(errno));
		if(n == 0)
			log("EOF on unix client fd %d: closing socket!", unix_fd);
		close(unix_fd);
		self->remove_io_watch(unix_fd);
		return;
	}
	log("request on unix client fd %d: %d bytes: %d\n", unix_fd, n, command[0]);
	
	// assume shm fd request!
	// command[1..] is \0 terminated shm name
	string shm_name(command + 1);
	log("fd for shm name %s requested!\n", repr(shm_name).c_str());
	// search all instances, all ports for this shm name	
	int fd = -1;
	unsigned int shm_size;
	for(instances_t::iterator i = self->instances.begin(); i != self->instances.end(); ++i) {
		ln_instance* inst = i->second;
		if(inst->get_shm_fd(shm_name, &fd, &shm_size))
			break;
	}
	if(fd == -1) 
		log("error, requested shm %s not found in any instance!\n", repr(shm_name).c_str());

	// now send fd to client!
	uint8_t response;
	struct iovec iov[2];
	iov[0].iov_base = &response;
	iov[0].iov_len = sizeof(response);
	iov[1].iov_base = &shm_size;
	iov[1].iov_len = sizeof(shm_size);
	struct msghdr msg;
	memset(&msg, 0, sizeof(msg));
	msg.msg_iov = iov;
	msg.msg_iovlen = 2;

	char control_buffer[CMSG_SPACE(sizeof(fd))];

	if(fd == -1) {
		response = 0; // error

		// no ancilliary data
		msg.msg_control = NULL;
		msg.msg_controllen = 0;
	} else {
		response = 1; // success, fd attached

		// fill ancilliary data
		msg.msg_control = control_buffer;
		msg.msg_controllen = sizeof(control_buffer);
		struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
		cmsg->cmsg_len = CMSG_LEN(sizeof(fd));
		cmsg->cmsg_level = SOL_SOCKET;
		cmsg->cmsg_type = SCM_RIGHTS;
		*(int*)CMSG_DATA(cmsg) = fd;
		msg.msg_controllen = cmsg->cmsg_len;
	}

	while(true) {
		n = sendmsg(unix_fd, &msg, 0);
		if(n < 1) {
			if(n == -1) {
				if(errno == EINTR)
					continue;
				log("error sending to unix client fd %d - for shm %s, fd %d: %d %s - closing socket!", 
				    unix_fd, 
				    repr(shm_name).c_str(),
				    fd,
				    errno, strerror(errno));
			}
			if(n == 0)
				log("EOF while sending to unix client fd %d - for shm %s, fd %d - closing socket!", 
				    unix_fd, 
				    repr(shm_name).c_str(),
				    fd);
			close(unix_fd);
			self->remove_io_watch(unix_fd);
			return;
		}
		if(n != 1)
			log("sending to unix client fd %d - for shm %s, fd %d - sent %d bytes!?",
			    unix_fd, 
			    repr(shm_name).c_str(),
			    fd,
			    n);
		break;
	}
		
}
#endif

void ln_daemon::remove_manager_connection(ln_manager_connection* conn) {
	manager_connections.remove(conn);
	delete conn;

	if(config->isolated_test && isolated_test_had_mgr_connection) {
		log("isolated-test mode: stopping after manager closed connection!");
		stop();
	}
}


void ln_daemon::add_io_watch(int fd, int flags, callback_t callback, void* data, string hint) {
	if(io_watches.find(fd) != io_watches.end()) {
		log("warning: tried to register duplicate io_watch with fd%d.", fd);
		return;
	}

	struct io_watch& w = io_watches[fd];
	w.fd = fd;
	w.flags = flags;
	w.callback = callback;
	w.data = data;
	w.hint = hint;
	log("adding io watch fd%d, flags %d, cb %p, data %p, hint %s\n",
			w.fd, w.flags, w.callback, w.data, repr(w.hint).c_str());

#if !defined(__WIN32__)
	if(fd >= FD_SETSIZE) {
		log("error: tried to register io_watch %s on fd %d with FD_SETSIZE of %d.", repr(hint).c_str(), fd, FD_SETSIZE);
		os::sleep(0.5);
	}
#endif
}

bool ln_daemon::remove_io_watch(int fd) {
	if(io_watches.find(fd) == io_watches.end()) {
		log("warning: tried to unregister unknown io_watch with fd %d.", fd);
		return false;
	}
	struct io_watch& w = io_watches[fd];
	log("removing io watch fd%d, hint %s\n", fd, w.hint.c_str());
	io_watches.erase(fd);
	restart_io_watch_iterator = true;

	return true;
}

double ln_daemon::_get_until_next_timeout_expiration(struct timeval** tv) {
	struct timeval* org_tv = *tv;
	*tv = NULL;
	double min_time = 0;

	if(self->_sigchild_pending) {
		*tv = org_tv;
		min_time = 0;
		log("sigchild pending...\n");
	} else {
		double now = ln_get_monotonic_time();
		for(timeouts_t::iterator i = timeouts.begin(); i != timeouts.end(); i++) {
			timeout_t& t = i->second;
			double until_expiration = t.expiration - now;
			if(until_expiration < 0)
				until_expiration = 0;
			if(until_expiration < min_time || *tv == NULL) {
				// log("have timeout with until_expiration: %f\n", until_expiration);
				*tv = org_tv;
				min_time = until_expiration;
				// log("min_time: %.3f\n", min_time);
			}		
		}

		// always have a sigchild timeout!
		double next_sigchild_in = _last_sigchild_check + _sigchild_check_interval - now;
		if(next_sigchild_in < 0)
			next_sigchild_in = 0;
		if(*tv == NULL || next_sigchild_in < min_time) {
			// log("next_sigchild sets timeout: %.3f\n", next_sigchild_in);
			*tv = org_tv;
			min_time = next_sigchild_in;
		}

/*
//#if defined(__VXWORKS__) || defined(__WIN32__)
if(process_watches.size()) {
*tv = org_tv;
min_time = 1; // every second for vxworks
// self->_sigchild_pending = true; // always check for childs!
}
//#endif
*/
	}

	if(org_tv) {
		org_tv->tv_sec = (long)min_time;
		org_tv->tv_usec = (int)((min_time - org_tv->tv_sec) * 1e6);
	}
	return min_time;
}
void ln_daemon::_check_timeouts() {
	double now = ln_get_monotonic_time();
	
	for(timeouts_t::iterator i = timeouts.begin(); i != timeouts.end();) {
		bool increment = true;
		timeout_t& t = i->second;
		double until_expiration = t.expiration - now;
		if(until_expiration <= 0) {
			bool remove;
			try {
				t.expiration = now + t.interval;
				remove = !t.callback(t.data);
			}
			catch(exception& e) {
				log("timeout %s (interval %.3fs) produced exception: %s -- removing.", 
					t.hint.c_str(),
					t.interval,
					e.what());
				remove = true;
			}
			if(remove) {
				timeouts.erase(i->first);
				i = timeouts.begin();
				increment = false;
			}
		}
		if(increment)
			i++;
	}
}

int ln_daemon::add_timeout(double interval, timeout_callback_t callback, void* data, string hint) {
	timeout_counter ++;
	timeout_t t;
	t.interval = interval;
	t.callback = callback;
	t.data = data;
	t.toid = timeout_counter;
	t.hint = hint;
	t.expiration = ln_get_monotonic_time() + t.interval;
	timeouts[timeout_counter] = t;
	return timeout_counter;
}

bool ln_daemon::remove_timeout(int toid) {
	if(timeouts.find(toid) == timeouts.end())
		return false;
	timeouts.erase(toid);
	return true;
}

void ln_daemon::add_process_watch(pid_t pid, callback_t callback, void* data) {
	log("added process watch for pid %d\n", pid);
	struct process_watch& w = process_watches[pid];
	w.pid = pid;
	w.callback = callback;
	w.data = data;
}

bool ln_daemon::remove_process_watch(pid_t pid) {
	if(process_watches.find(pid) == process_watches.end()) {
		log("warning: tried to unregister unknown process_watch with pid %d.", pid);
		return false;
	}

	log("removed process watch for pid %d\n", pid);
	process_watches.erase(pid);
	return true;
}


void ln_daemon::on_sigchild(int signo) {
	self->_sigchild_pending = true;
}

void ln_daemon::on_sigint(int signo) {
#ifdef __VXWORKS__
	printf("sigin received\n");
	log("sigint!");
#endif
	self->stop();
}


ln_instance* ln_daemon::get_instance(string instance_name) {
	ln_instance* instance = NULL;
	instances_t::iterator i = instances.find(instance_name);
	if(i == instances.end()) {
		// new instance !
		instance = new ln_instance(this, instance_name);
		instances[instance_name] = instance;
		return instance;
	}
	return i->second;
}
void ln_daemon::remove_instance(string instance_name) {
	ln_instance* instance = instances[instance_name];
	delete instance;
	instances.erase(instance_name);
}


void ln_daemon::deregister_sigchild() {
#ifndef __WIN32__
	signal(SIGCHLD, SIG_DFL);
#endif
}

void ln_daemon::register_sigchild() {
#ifndef __WIN32__
	signal(SIGCHLD, &on_sigchild);
#endif
}

void ln_daemon::load_public_key() {
	if(!config->public_key_file.size()) {
		locked_by = "";
		return;
	}
	if (config->public_key_file == "never")
		public_key = "never";
	else
		public_key = read_file(config->public_key_file);
}

void ln_daemon::set_public_key(std::string key_contents, std::string locked_by)
{
	public_key = key_contents.c_str();
	this->locked_by = locked_by;
}
