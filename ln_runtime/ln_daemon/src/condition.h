#ifndef CONDITION_H
#define CONDITION_H

#include "os.h"

namespace os {
class condition;
}

#include "lock.h"

namespace os {

class condition {
	condition_handle_t handle;
public:
	condition();
	~condition();
	
	void wait(flolock& l);

	void signal();
	void broadcast();
};

}

#endif /* CONDITION_H */

