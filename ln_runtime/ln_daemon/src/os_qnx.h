#ifndef OS_QNX_H
#define OS_QNX_H

#define FD_SETSIZE 1024 // todo: switch to poll!

#include "os_posix_like.h"
#include <string_util/exceptions.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>

#include <spawn.h>

#define HAVE_GETPWUID
#define OS_SHM_DIR "/dev/shmem"

using namespace string_util;
namespace os {

typedef socklen_t socklen_t_type;

typedef pthread_t thread_handle_t;
typedef void* thread_return_t;
typedef thread_return_t (thread_func_type)(void*);

int create_thread(thread_handle_t* thread_handle, thread_func_type* thread, void* data);
int cancel_thread(thread_handle_t thread_handle);
int join_thread(thread_handle_t thread_handle);
void test_cancel(thread_handle_t thread_handle);

void sleep(double s);

//! kill process with given pid
/*!
  \param pid process pid to kill
  \return success or error code
  */
int kill(pid_t pid);

//! send signal to process with given pid
/*!
  \param pid process pid to signal
  \parma signo signal number to send
  \return success or error code
  */
int send_signal(pid_t pid, int signo);

//! initialize os dependant pipe device
/*!
  \param reference to pipe device structure
  \return success or error code
 */
int pipe_init(ln_pipe_t& pipe);

//! closes os dependant pipe device
/*!
 \param reference to pipe device structure
 \return success or error code
 */
int pipe_close(ln_pipe_t& pipe);

//! return temp dir as std::string
/*
  \return temp dir
 */
std::string get_temp_dir();

int set_prio(pid_t pid, int tid, int prio, int policy);
int set_affinity(pid_t pid, int tid, int affinity);
int set_affinity_of_current_thread(int affinity);

pid_t gettid();

}

#endif // OS_QNX_H
