/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include "os.h"

#include <time.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <stdexcept>
#include <sstream>

#include <string_util/string_util.h>
#include "util.h"

using namespace string_util;

int resolve_hostname(string hostname_str, struct sockaddr_in* sa) {
	const char* hostname = hostname_str.c_str();

	if(isdigit(hostname[0])) {
		// printf("hostname[0] is digit: '%s'\n", hostname);
		// assume dotted decimal notation
		int ret = inet_aton(hostname, &sa->sin_addr);
#ifndef __VXWORKS__
		if(ret) {
#else
		if(ret == 0) {
#endif
			// printf("it seems to be a dotted decimal ip: %s, ret: %d\n", hostname, ret);
			sa->sin_family = AF_INET;
			return 0; // otherwise try to resolve...
		}
		printf("inet_aton failed: %d, %s\n", ret, strerror(errno));
	}/* else {
		printf("hostname[0] is not digit: '%s'\n", hostname);
	}
	 */

	// resolve hostname
	if(!hostname || hostname[0] == 0)
		throw str_exception_tb("empty hostname specified!");
	struct hostent* he = gethostbyname(hostname);
	if(!he)
		throw str_exception_tb("unknown hostname %s", repr(hostname_str).c_str());
	if(he->h_addrtype != AF_INET)
		throw str_exception_tb("invalid address family reported on hostname %s", repr(hostname_str).c_str());
	memcpy(&sa->sin_addr, he->h_addr_list[0], sizeof(sa->sin_addr));
	sa->sin_family = AF_INET;
	return 0;
}

#if !defined(__WIN32__) && !defined(__VXWORKS__)
#include <pwd.h>
#endif
int getuid_by_name(const char *name) {
	if(!name)
		return -1;
#if !defined(__WIN32__) && !defined(__VXWORKS__)
        struct passwd *pwd = getpwnam(name); /* don't free, see getpwnam() for details */
        if(pwd)
		return pwd->pw_uid;
#endif
	return -1;
}

#if !defined(__WIN32__) && !defined(__VXWORKS__)
#include <grp.h>
#endif
int getgid_by_name(const char *name) {
	if(!name)
		return -1;
#if !defined(__WIN32__) && !defined(__VXWORKS__)
        struct group* grp = getgrnam(name);
        if(grp)
		return grp->gr_gid;
#endif
	return -1;
}

std::string read_file(const std::string& fn) {

	FILE* fp = fopen(fn.c_str(), "rb");
	if(!fp)
		throw std::runtime_error((std::string("open ") + fn).c_str());
	struct stat sbuf;
	int ret = fstat(fileno(fp), &sbuf);
	if(ret == -1)
		throw std::runtime_error((std::string("stat fd ") + fn).c_str());
	std::vector<uint8_t> data(sbuf.st_size);
	ret = fread(&data[0], sbuf.st_size, 1, fp);
	if(ret != 1) {
		std::ostringstream ss;
		ss << "read '" << fn << "': " << ret;
		throw std::runtime_error(ss.str().c_str());
	}
	fclose(fp);
	return std::string((const char*)&data[0], data.size());
}
