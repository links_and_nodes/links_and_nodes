#ifndef FILTER_H
#define FILTER_H

#include <list>
#include <string>

class filter_item_t {
public:
	std::string key;
	std::string op;
	std::string value;
	
	filter_item_t(std::string key, std::string op, std::string value) : key(key), op(op), value(value) {}
};

typedef std::list<filter_item_t> filter_t;

filter_t parse_filter(std::string filter);

#endif
