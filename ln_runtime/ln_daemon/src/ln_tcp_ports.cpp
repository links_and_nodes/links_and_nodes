/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include "os.h"

#include <stdio.h>
#include <stdio.h>
#include <errno.h>

#include "ln_ports.h"

// tcp sender sink port
ln_tcp_sender::ln_tcp_sender(ln_instance* instance, string target_host, int target_port, int size, float rate, string swap_endianess) : 
	ln_port(instance, "tcp", SINK_PORT, size),
	ln_sink_port(rate), 
	target_host(target_host), target_port(target_port) {
	thread_handle = 0;
	log(__func__);
	if(swap_endianess.size())
		this->swap_endianess.set_format(format_string("48%s4", swap_endianess.c_str()));
	sfd = -1;
	log("rate setting: %f\n", rate);

	if(!connect())
		throw errno_exception_tb("could not connect tcp_sender socket to target %s port %d!", target_host.c_str(), target_port);

	instance->register_port(this);

	memset(&message, 0, sizeof(struct msghdr));
	message.msg_iovlen = 4;
	buffers.resize(message.msg_iovlen);
	message.msg_iov = &buffers[0];
	send_registered = false;
	reconnect_registered = false;

	keep_running = true;
	int ret = ln_semaphore_init(&trigger_send, 0);
	if(ret)
		throw errno_exception("could not create mainloop-semaphore!");
	os::create_thread(&thread_handle, ln_tcp_sender::_sender_thread, this);
}

#ifdef __WIN32__
#ifdef MSVC
thread_return_t ln_tcp_sender::_sender_thread(void* data) {
#else
WINAPI thread_return_t ln_tcp_sender::_sender_thread(void* data) {
#endif
#else
thread_return_t ln_tcp_sender::_sender_thread(void* data) {
	signal(SIGPIPE, SIG_IGN);
#endif
	ln_tcp_sender* port = (ln_tcp_sender*) data;
	try {
		return (thread_return_t)port->sender_thread();
	}
	catch(exception& e) {
		log("%s: error in tcp sender thread!\n%s\n", port->get_id().c_str(), e.what());
	}
	return (thread_return_t)0;
}

thread_return_t ln_tcp_sender::sender_thread() {
	log("%s: sender thread started.\n", get_id().c_str());
#if defined(__LINUX__) || defined(__VXWORKS__)
	// get linux thread id
	sender_tid = os::gettid();
#endif
	while(keep_running) {
		send_registered = false;
		ln_semaphore_wait(trigger_send);
		//log("%s: got semaphore!\n", get_id().c_str());
		if(!keep_running)
			break;
		// finish send job!
		while(keep_running) {
			// blocking wait until we can send more data!
			fd_set writefds;
			FD_ZERO(&writefds);
			FD_SET(sfd, &writefds);
			struct timeval tv = {0, 250000};
			int n = select(sfd + 1, NULL, &writefds, NULL, &tv);
			if(n == 0) // timeout!
				continue;
			if(n == -1) {
				if(errno == EINTR || errno == EAGAIN)
					continue;
				log("%s: select(): %s\n", get_id().c_str(), strerror(errno));
				fprintf(stderr, "%s: select(): %s\n", get_id().c_str(), strerror(errno));
				keep_running = false;
				break;
			}
			//log("%s: send!\n", get_id().c_str());
			try_send();
			if(can_consume)
				break;
		}
		reconnect_registered = false;
	}
	log("%s: sender thread exiting: %d...\n", get_id().c_str(), keep_running);
	return (thread_return_t)0;
}

	
ln_tcp_sender::~ln_tcp_sender() {
	keep_running = false;
	if(thread_handle) {
		log("%s: command thread stop!\n", get_id().c_str());
		ln_semaphore_post(trigger_send);
		// os::cancel_thread(thread_handle);
		os::join_thread(thread_handle);
	}
	ln_semaphore_destroy(trigger_send);
	if(source) {
		log("ln_tcp_sender was still connected. removing connection now!");
		source->remove_sink(this);
		source = NULL;
	}
	if(reconnect_registered)
		instance->daemon->remove_timeout(timeout_id);

	close();
}

bool ln_tcp_sender::connect() {
	if(sfd != -1)
		return true;
	
#if !defined(__WIN32__)
	signal(SIGPIPE, SIG_IGN);
#endif
	sfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sfd == -1)
		throw errno_exception_tb("could not create tcp_sender socket!");
	
	struct sockaddr_in target_addr;
	resolve_hostname(target_host, &target_addr);
	target_addr.sin_port = htons(target_port);

	if(::connect(sfd, (struct sockaddr*)&target_addr, sizeof(target_addr))) {
		close();
		return false;
	}

	// disable nagle
	int flag = 1;
	setsockopt(sfd, IPPROTO_TCP, TCP_NODELAY, (char*)&flag, sizeof(int));
	
#if defined(__WIN32__)
	// windows socket is blocking...
#else
	// only when there are more sinks connected to this source thread?	
	int flags = fcntl(sfd, F_GETFL, 0);
	fcntl(sfd, F_SETFL, flags | O_NONBLOCK);
#endif

	can_consume = true;

	return true;
}

void ln_tcp_sender::close() {
	if(sfd != -1) {
		log("%s: closing client connection\n", get_id().c_str());
		unregister_send();
		shutdown(sfd, SHUT_RDWR);
		::closesocket(sfd);
		sfd = -1;
	}
	can_consume = false;
}

void ln_tcp_sender::consume_packet(double ts, uint32_t counter, void* data) {
	if(!can_consume)
		return;

	/* test with sentinels in data...
	uint32_t first = *(uint32_t*)data;
	uint32_t last = *(uint32_t*)((char*)data + size - sizeof(uint32_t));
	if(first != last)
		log("%s: warning: first: %d, last: %d in consume_packet!\n", get_id().c_str(), first, last);
	first_at_start = first;
	last_at_start = last;
	data_at_start = data;
	*/
	
	// log("%s: accept new packet\n", get_id().c_str());
	// accept new packet
	bytes_to_send = sizeof(uint32_t) + sizeof(double) + size + sizeof(uint32_t);
	// set vectors
	if(swap_endianess) {
		// reset message
		message.msg_iovlen = 4;
		message.msg_iov = &buffers[0];
	}
	buffers[0].iov_base = (char*)&bytes_to_send;
	buffers[0].iov_len = sizeof(bytes_to_send);
	
	buffers[1].iov_base = (char*)&ts;
	buffers[1].iov_len = sizeof(double);
	
	buffers[2].iov_base = (char*)data;
	buffers[2].iov_len = size;
	
	buffers[3].iov_base = (char*)&counter;
	buffers[3].iov_len = sizeof(counter);

	// todo: endianess swap on sender side!
	if(swap_endianess)
		swap_endianess.swap_msg(&message);	

	bytes_sent = 0;
#if defined(__WIN32__)
	// there is no non-blocking tcp send on windows. so always send in background thread...
	can_consume = false;
	register_send();
#else
	try_send();
#endif
}

void ln_tcp_sender::send_callback(int fd, int why, void* data) {
	ln_tcp_sender* port = (ln_tcp_sender*)data;
	log("%s: send_callback!\n", port->get_id().c_str());
	port->try_send();
}

bool ln_tcp_sender::try_connect_timeout(void* data) {
	ln_tcp_sender* port = (ln_tcp_sender*) data;
	return port->try_connect();
}

bool ln_tcp_sender::try_connect() {
	if(connect()) {
		// success!
		instance->daemon->remove_timeout(timeout_id);
		timeout_id = -1;
		reconnect_registered = false;
		return false; // stop timeout
	}
	return true; // try again later...
}

void ln_tcp_sender::register_reconnect() {
	if(reconnect_registered)
		return; // do nothing
	timeout_id = instance->daemon->add_timeout(5, &try_connect_timeout, this, format_string("connect_timeout %s", get_id().c_str()));
	reconnect_registered = true;
}

void ln_tcp_sender::register_send() {
	if(send_registered)
		return; // do nothing
	// instance->daemon->add_io_watch(sfd, IO_WATCH_WRITE, &send_callback, this, format_string("tcp_sender %s", get_id().c_str()));
	// notify own sender thread to finish this send!
	send_registered = true;
	ln_semaphore_post(trigger_send);
	
}

void ln_tcp_sender::unregister_send() {
	if(!send_registered)
		return; // do nothing
	// instance->daemon->remove_io_watch(sfd);	
	send_registered = false;
}

void ln_tcp_sender::try_send() {
#if defined(__VXWORKS__)
	ssize_t sent = sendmsg(sfd, &message, 0);
#elif defined(__WIN32__)
	ssize_t sent = sendmsg(sfd, &message, 0); // __WIN32__DGRAM_SEND
#else
	ssize_t sent = sendmsg(sfd, &message, 0);
#endif

	if(sent == -1) {
		if(errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR) {
			can_consume = false;
			register_send();
			return;
		}
		log("error: port %s: sendmsg: %s - closing connection\n", get_id().c_str(), strerror(errno));
		close();
		register_reconnect();
		return;
	}
	bytes_sent += sent;
	if(bytes_sent != bytes_to_send) {
		// not yet finished. shift vector pointers...
		while(sent > 0) {
			for(unsigned int i = 0; i < buffers.size(); i++) {
				if(buffers[i].iov_len <= (unsigned int)sent) {
					sent -= buffers[i].iov_len;
					buffers[i].iov_len = 0;
				} else {
					// shift pointer aswell
					buffers[i].iov_base = ((char*)buffers[i].iov_base + sent);
					buffers[i].iov_len -= sent;
					sent = 0;
					break;
				}
			}
		}
		/*
		  log("%s: need to send %d bytes to %s:%d but up-to-now managed only %d bytes!\n",
		  get_id().c_str(), bytes_to_send, target_host.c_str(), target_port, bytes_sent);
		*/
		// os::sleep(1);
		can_consume = false;
		register_send();
		return;
	}
	// packet finished!
	/* data sentinals
	void* data = data_at_start;
	uint32_t first = *(uint32_t*)data;
	uint32_t last = *(uint32_t*)((char*)data + size - sizeof(uint32_t));
	if(first != last)
		log("%s: warning at end: first: %d, last: %d in consume_packet!\n", get_id().c_str(), first, last);
	if(first != first_at_start)
		log("%s: warning at end: first: %d, first_at_start: %d in try_send!\n", get_id().c_str(), first, first_at_start);
	if(last != last_at_start)
		log("%s: warning at end: last: %d, last_at_start: %d in try_send!\n", get_id().c_str(), last, last_at_start);
	*/
	// log("%s: packet finished!\n", get_id().c_str());
	can_consume = true;
	// unregister_send();
}


void ln_tcp_receiver::incoming_connection_cb(int fd, int why, void* data) {
	ln_tcp_receiver* port = (ln_tcp_receiver*) data;
	port->incoming_connection();
}

void ln_tcp_receiver::incoming_connection() {
	os::socklen_t_type client_address_len = sizeof(client_address);
	client_fd = accept(sfd, (struct sockaddr*)&client_address, &client_address_len);
	if(client_fd == -1) {
		log("%s: error accepting incoming connection! errno %d: %s\n", get_id().c_str(), errno, strerror(errno));
		return;
	}	
}

void ln_tcp_receiver::close() { // close client connection
	if(client_fd == -1)
		return;
	shutdown(client_fd, SHUT_RDWR);
	::closesocket(client_fd);
	client_fd = -1;
}


// tcp receiver source port
ln_tcp_receiver::ln_tcp_receiver(ln_instance* instance, unsigned int size, thread_settings* ts, string swap_endianess) : 
	ln_port(instance, "tcp", SOURCE_PORT, size),
	ln_source_port(ts) {

	log(__func__);
	if(swap_endianess.size())
		this->swap_endianess.set_format(format_string("48%s4", swap_endianess.c_str()));
	this->size = size;
	sfd = -1;
	packet_counter = 0;
	packet = malloc(sizeof(uint32_t) + sizeof(double) + size + sizeof(uint32_t));  // to be able to receive timestamp & counter via tcp!
	if(!packet)
		throw errno_exception_tb("could not allocate tcp_receiver packet of size %d bytes!", size);
	memset(packet, 0, sizeof(uint32_t) + sizeof(double) + size + sizeof(uint32_t));

	// create listening socket
	sfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sfd == -1)
		throw errno_exception_tb("could not create tcp_receiver socket!");

	struct sockaddr_in local_addr;
	os::socklen_t_type len = sizeof(local_addr);
	memset(&local_addr, 0, sizeof(local_addr));
	local_addr.sin_family = AF_INET;
	if(bind(sfd, (struct sockaddr*)&local_addr, sizeof(local_addr)))
		throw errno_exception_tb("could not bind tcp_receiver's server port!");
	
	if(getsockname(sfd, (struct sockaddr*)&local_addr, &len))
		throw errno_exception_tb("could not get tcp_receiver' server socket name!");
	
	tcp_port = ntohs(local_addr.sin_port);

	if(listen(sfd, 1))
		throw errno_exception_tb("could not set tcp_receiver' server socket to listen(1) mode!");

	// printf("port %s: ctor with %p %d bytes (size: %d)\n", get_id().c_str(), packet, size + sizeof(double) + sizeof(uint32_t), size);

	instance->daemon->add_io_watch(sfd, IO_WATCH_READ, &incoming_connection_cb, this, format_string("tcp_receiver server %s", get_id().c_str()));	
	client_fd = -1;
	
	instance->register_port(this);
	start();
	}
ln_tcp_receiver::~ln_tcp_receiver() {
	log("ln_tcp_receiver de-tor!");
	instance->daemon->remove_io_watch(sfd);	
	stop();
	close();
	if(sfd != -1) {
		shutdown(sfd, SHUT_RDWR);
		::closesocket(sfd);
	}
	if(packet)
		free(packet);
}

#if defined(__VXWORKS__)
#include "os.h"
#include <sys/select.h>
#endif
int ln_tcp_receiver::generate_packet(void* data, double timeout) {
	if(client_fd == -1) {
		// no connection yet.
		sleep(timeout);
		return 0;
	}

	/*
	unsigned int flags = 0;
#ifndef __WIN32__
	if(timeout == 0)
		flags = MSG_DONTWAIT;
	// todo: win32 non blocking receive flag?
#endif
	*/
	unsigned int s = sizeof(uint32_t) + sizeof(double) + size + sizeof(uint32_t);
	int n;

	const void* target_buffer = packet;
	if(swap_endianess)
		target_buffer = swap_endianess.get_buffer();

	// todo: maybe use recv iov to fill first timestamp and then directly data and get rid of this additional memcpy!
#if defined(__VXWORKS__)
	// let vxworks always only block in select call ... do not block with vxw in recv()!
	fd_set readfds;
	fd_set excfds;
	while(true) {
		os::test_cancel(0);

		FD_ZERO(&readfds);
		FD_ZERO(&excfds);
		FD_SET(client_fd, &readfds);
		FD_SET(client_fd, &excfds);
		struct timeval tv = {0, 250000};
		tv.tv_sec = (unsigned int)timeout;
		tv.tv_usec = (unsigned int)(1e6 * (timeout - tv.tv_sec));
		if(timeout == -1)
			tv.tv_usec = 250000; // blocking
		n = select(client_fd + 1, &readfds, NULL, &excfds, &tv);
		if(n == 0) {
			// timeout!
			if(timeout >= 0)
				return 0;
			continue;
		}
		if(n == -1) {
			fprintf(stderr, "ln_tcp_receiver::generate_packet() select(): %s\n", strerror(errno));
			return -1;
		}
		if(FD_ISSET(client_fd, &readfds))
			break;
		if(FD_ISSET(client_fd, &excfds)) {
			fprintf(stderr, "ln_tcp_receiver::generate_packet() select(): fd %d is in exception-fds!\n", client_fd);
			break;
		}
	}
#else
	if(timeout >= 0) {
		// with timeout
		fd_set readfds;
		fd_set excfds;
		while(true) {
			os::test_cancel(0);
			
			FD_ZERO(&readfds);
			FD_ZERO(&excfds);
			FD_SET(client_fd, &readfds);
			FD_SET(client_fd, &excfds);
			struct timeval tv = {0, 0};
			tv.tv_sec = (unsigned int)timeout;
			tv.tv_usec = (unsigned int)(1e6 * (timeout - tv.tv_sec));
			n = select(client_fd + 1, &readfds, NULL, &excfds, &tv);
			if(n == 0) // timeout!
				return 0;
			if(n == -1) {
				fprintf(stderr, "ln_tcp_receiver::generate_packet() select(): %s\n", strerror(errno));
				return -1;
			}
			if(FD_ISSET(client_fd, &readfds))
				break;
			if(FD_ISSET(client_fd, &excfds)) {
				fprintf(stderr, "ln_tcp_receiver::generate_packet() select(): fd %d is in exception-fds!\n", client_fd);
				break;
			}
		}
	}
#endif
	
	// do blocking full receive!
	unsigned int bytes_received = 0;
	unsigned int bytes_left = s;
	bool did_sender_size_check = false;
	while(bytes_left > 0) {
		n = recv(client_fd, (char*)target_buffer + bytes_received, bytes_left, 0);
		if(n == -1) {
			if(errno == EAGAIN || errno == EINTR)
				continue;
			throw errno_exception("tcp_receiver port %s got error on receive:", get_id().c_str());
		}
		if(n == 0) {
			// eof?
			log("%s: got eof (ret == 0) from tcp receiver socket! closing connection!\n", get_id().c_str());
			close();
			return 0;
		}
		bytes_received += n;
		bytes_left -= n;
		
		if(!did_sender_size_check && bytes_received >= sizeof(uint32_t)) {
			did_sender_size_check = true;
			uint32_t sender_size;
			// todo: endianess swap on receiver side!			
			if(swap_endianess)
				swap_endianess.swap(4, &sender_size, target_buffer);
			else
				sender_size = *(uint32_t*)target_buffer;
			if(sender_size != s) {
				log("%s: sender is sending invalid packet length %d! expect %d! closing connection!\n",
				    get_id().c_str(), sender_size, s);
				close();
				return 0;
			}
		}
	}
	
	// todo: endianess swap on receiver side!			
	if(swap_endianess)
		swap_endianess.swap_into(packet);
	
	void* inner_packet = ((uint32_t*)packet) + 1;	
	packet_timestamp = *(double*)inner_packet;
	memcpy(data, (unsigned char*)inner_packet + sizeof(double), size);
	packet_counter = *(uint32_t*)((unsigned char*)inner_packet + sizeof(double) + size);
	
	return 1; // generated a packet
}

