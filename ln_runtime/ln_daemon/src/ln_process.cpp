/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include "os.h"
#include <string_util/string_util.h>

#include "ln_process.h"

#include <stdio.h>
#include <sys/types.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifdef __LINUX__
#ifndef __ANDROID__
#include <pty.h>
#include <sys/time.h>
#include <sys/resource.h>
#endif
#include <sched.h>
#endif

#ifdef __QNX__
#include <unix.h>
#include <sys/resource.h>
#endif

#ifdef MSVC
#include <io.h>
#include <direct.h>
#define chdir _chdir
#endif


#if defined(__LINUX__) || defined(__QNX__)
#include <termios.h>
#endif

#include "filter.h"

using namespace string_util;

// __stdcall
thread_return_t THREAD_CALL _output_thread(void* data)
{
	ln_process* self = (ln_process*)data;
	self->output_thread();
	return (thread_return_t)0;
}

void output_buffer_notification(int fd, int why, void* instance)
{
	ln_process* self = (ln_process*)instance;
	char data[256];
#ifdef __VXWORKS__
	// needed for vxworks6.7!?
	read(fd, data, 1);
#else
	int n = 0;
	while((n = read(fd, data, 256)) > 0)
		// printf("reading buffer\n");
		// log("[%d] output_buffer_notification read() returned %d\n", self->pid, n);
		;
#endif
	// printf("sending buffer\n");
	self->send_output_buffer();
}

void _process_terminated(int pid, int status, void* instance)
{
	ln_process* self = (ln_process*)instance;
	self->terminated(status);
}

#if defined(__LINUX__) || defined(__QNX__)
void set_terminal(int fd) {
	struct termios term;
	tcgetattr(fd, &term);
	// term.c_oflag = 0; // clear ONLCR - Map NL to CR-NL on output. and clear OPOST
	term.c_oflag = ONLCR | OPOST; // set ONLCR - Map NL to CR-NL on output. and clear OPOST -> see newlines_in_process_output.txt

	if(tcsetattr(fd, TCSANOW, &term) == -1)
		fprintf(stderr, "tcsetattr: %s\n", strerror(errno));
}
#endif

ln_process::ln_process(ln_instance* instance, py_list* env_pylist,
                       config_t& config, bool autorun)
	: instance(instance), last_output_buffer(1024), exitcb(NULL), pid(-1), config(config),
	  no_output_rate_limit(false)
{
	stdin_pipe.input_fd = ln_pipe_invalid;
	stdin_pipe.output_fd = ln_pipe_invalid;
	props = new py_dict();
	start_time = ln_get_time(); /* not using monotonic time here because 
				       this goes to the python interface */
	triggered_output_buffer = false;
	is_ready = false;
	output_thread_running = false;
	output_thread_joined = false;
	output_buffer = NULL;

	if(create_mutex(&PATH_env_mutex))
		throw errno_exception("could not create PATH_env_mutex!");

	// pass complete env if state
	if(config.is_state) {
		char** ep = environ;
		while(*ep) {
			char* name = *ep;
			char* value = strchr(name, '=');
			env.push_back(pair<string, string>(string(name, value - name), string(value + 1)));
			ep++;
		}
	}
	bool have_ln_manager = false;
	if(env_pylist) {
		for(py_list_value_t::iterator i = env_pylist->value.begin(); i != env_pylist->value.end(); i++) {
			py_tuple* v = dynamic_cast<py_tuple*>(*i);
			py_string* key = dynamic_cast<py_string*>(*v->value.begin());
			py_string* value = dynamic_cast<py_string*>(*v->value.rbegin());
			if(key && value) {
				string key_str = key->value;
				env.push_back(pair<string, string>(key_str, value->value));
				have_ln_manager |= key_str == "LN_MANAGER";
			}
		}
	}
	if(!have_ln_manager && instance->manager.size()) {
		// add pointer to ln_manager!
		env.push_back(pair<string, string>("LN_MANAGER", instance->manager));
	}

	if (autorun)
		execute();
}

void apply_resource_limits(py_list* resource_limits)
{
	if(!resource_limits)
		return;
#if defined(__VXWORKS__) || defined(__WIN32__)
	log("warning: resource limits not supported on this platform!\n");
	return; // not supported
#else
	for(py_list_value_t::iterator i = resource_limits->value.begin(); i != resource_limits->value.end(); ++i) {
		py_value* item = *i;
		py_tuple* t = dynamic_cast<py_tuple*>(item);
		if(!t || t->value.size() != 2) {
			log("invalid resource item: %s\n", repr(item).c_str());
			continue;
		}
		py_value* pyname = *t->value.begin();
		py_value* pyvalue = *t->value.rbegin();
		string name(*pyname);
		int resource;
#define if_resource(res) if(name == #res) resource = res
		if_resource(RLIMIT_CORE);
		else if_resource(RLIMIT_DATA);

#if !defined(__QNX__)
		else if_resource(RLIMIT_NICE);
		else if_resource(RLIMIT_SIGPENDING);
		else if_resource(RLIMIT_MSGQUEUE);
		else if_resource(RLIMIT_RTPRIO);
		else if_resource(RLIMIT_LOCKS);
#endif
		else if_resource(RLIMIT_FSIZE);
		else if_resource(RLIMIT_MEMLOCK);
		else if_resource(RLIMIT_RSS);
		else if_resource(RLIMIT_NOFILE);
		else if_resource(RLIMIT_STACK);
		else if_resource(RLIMIT_CPU);
		else if_resource(RLIMIT_NPROC);
		else if_resource(RLIMIT_AS);
		else {
			log("invalid resource item for this platform: %s\n", repr(item).c_str());
			continue;
		}

		struct rlimit limit;
		int ret = getrlimit(resource, &limit);
		if(ret) {
			log("could not getrlimit() for item: %s\n", repr(item).c_str());
			continue;
		}
		// apply limit change
		string str_value = string(*pyvalue);
		if(str_value == "max")
			limit.rlim_cur = limit.rlim_max;
		else if(str_value == "inf")
			limit.rlim_cur = RLIM_INFINITY;
		else
			limit.rlim_cur = int(*pyvalue);

		// enforce hard limit!
		limit.rlim_max = limit.rlim_cur;
		debug("set limit %s (%d) to %d\n", name.c_str(), resource, (int)limit.rlim_cur);
		ret = setrlimit(resource, &limit);
		if(ret)
			log("failed to set limit %s (%d) to %d: %s\n", name.c_str(), resource, (int)limit.rlim_cur, strerror(errno));
	}
#endif
}

#ifdef __VXWORKS__
int ln_process::execute()
{
	// build environment
	envp = (char**) malloc(sizeof(char*) * (env.size() + 1));
	int k = 0;
	for(env_t::iterator i = env.begin(); i != env.end(); i++) {
		pair<string, string>& p = *i;
		envp[k++] = strdup(format_string("%s=%s", p.first.c_str(), p.second.c_str()).c_str());
	}
	envp[k] = NULL;

	// create default process pipe
	os::pipe_init(pipe);
	config.no_pty = true; // there is no pty handling in vxworks!

	if (config.start_in_shell)
		log("warning: there is no real non-interactive shell on vxworks!?");

	if (config.change_directory.size()) {
		if(chdir(config.change_directory.c_str()))
			log("chdir(%s) error: %s\n", repr(config.change_directory).c_str(), strerror(errno));
	} else
		chdir("/tmp");

	// build arguments
	list<string> args = split_command_line(config.cmdline);
	char** argv = (char**) malloc(sizeof(char*) * (args.size() + 1));
	k = 0;
	for(list<string>::iterator i = args.begin(); i != args.end(); i++)
		argv[k++] = strdup(i->c_str());
	argv[k] = NULL;

	if (config.priority == 0)
		config.priority = 200;
	int stacksize = config.stack_size;
	int options = RTP_CPU_AFFINITY_NONE;
	int task_options = VX_FP_TASK;

	// dup stdin/out/err
	int org_stdin = dup(0);
	int org_stdout = dup(1);
	int org_stderr = dup(2);

	// redirect input from /dev/null
	int fd = open("/dev/null", O_RDONLY);
	if(fd == -1) {
		free(argv);
		throw errno_exception("open /dev/null");
	}
	dup2(fd, 0); // stdin
	close (fd);

	fcntl(pipe.output_fd, F_SETFL, O_NONBLOCK);
	// redirect output
	dup2(pipe.output_fd, 1); // stdout
	dup2(pipe.output_fd, 2); // stderr
	fcntl(1, F_SETFL, O_NONBLOCK);
	fcntl(2, F_SETFL, O_NONBLOCK);

	// TODO: how to close all other file deskriptors!?
	/*
	  From: 	Resmerita, Cristian-Catalin (Cristian) <Cristian.Resmerita@windriver.com>
	  To: 	robert.burger@dlr.de <robert.burger@dlr.de>
	  Cc: 	florian.schmidt@dlr.de <florian.schmidt@dlr.de>
	  Subject: 	TSR#1183684 is there a way to avoid the fact that the spawned child RTP inherits all
	  Date: 	14.11.2012 14:54:20

	  I just received the confirmation form our product engineering team that this is not supported.
	  As published documentation (rtpLib's in particular) clearly states that an RTP launched by another RTP inherits all of its parent's file descriptor.
	  Here is the enhancement request created for this issue: WIND00388684.
	  Actual work on this will happen only when this is turned into a requirement and gets high-enough priority but I can give you a release date at this moment.
	 */
	pid = rtpSpawn((const char*)argv[0], (const char**)argv,
	               (const char**)envp, config.priority, stacksize, options, task_options);
	// restore stdin/out/err
	dup2(org_stdin, 0);
	dup2(org_stdout, 1);
	dup2(org_stderr, 2);
	close(org_stdin);
	close(org_stdout);
	close(org_stderr);
	close(pipe.output_fd);
	pipe.output_fd = -1;

	if(pid == ERROR)
		throw errno_exception_tb("could not rtpSpawn() new process!");

	instance->daemon->add_process_watch(pid, _process_terminated, this);
	log("added process watch for pid %d name %s at process %p!", pid, repr(config.name).c_str(), this);

	// create output buffer
	set_log_output(config.log_output, config.buffer_size);

	// create output buffer notification pipe
	os::pipe_init(buffer_pipe);
	// not needed? fcntl(buffer_pipe.output_fd, F_SETFL, O_NONBLOCK);
	fcntl(buffer_pipe.input_fd, F_SETFL, O_NONBLOCK);
	instance->daemon->add_io_watch(buffer_pipe.input_fd, IO_WATCH_READ,
	                               output_buffer_notification, this, format_string("output buffer notification for pid %d", pid));

	// start output thread
	int ret;
	output_thread_running = true;
	output_thread_joined = false;
	if ((ret = create_thread(&output_thread_tid, (thread_func_type)_output_thread, this)))
		throw retval_exception_tb(ret, "could not create output thread pthread_create()!");

	instance->register_process(this);

	after_execute();
	return 0;
}
#endif

void ln_process::_forked_child()
{
#if !defined(__VXWORKS__) && !defined(__WIN32__)
	int table_size = os::get_max_number_of_fds();

	bool with_pty = !config.no_pty;
	if(with_pty) {
		// close all file descriptors except 0, 1, 2
		for(int i = 3; i < table_size; i++)
			close(i);

		set_terminal(0);
	} else {
		for(int i = 0; i < table_size; i++) {
			if(i != pipe.output_fd && i != stdin_pipe.input_fd)
				close(i);
		}

		// redirect input
		dup2(stdin_pipe.input_fd, 0);
		// redirect output to
		fcntl(pipe.output_fd, F_SETFL, O_NONBLOCK);
		dup2(pipe.output_fd, 1); // stdout
		dup2(pipe.output_fd, 2); // stderr
		fcntl(1, F_SETFL, O_NONBLOCK);
		fcntl(2, F_SETFL, O_NONBLOCK);

		if(setsid() == -1)
			fprintf(stderr, "could not become session leader: %s\n", strerror(errno));
	}

	if(config.change_directory.size()) {
		if(chdir(config.change_directory.c_str()))
			fprintf(stderr, "chdir(%s) error: %s\n", repr(config.change_directory).c_str(), strerror(errno));
	} else
		chdir("/tmp");

	// execute shell with specified command line
	set_own_prio(config.policy, config.priority, config.affinity);

	apply_resource_limits(config.resource_limits);

	if(config.change_user_to.size()) {

		vector<string> cu_parts = split_string(config.change_user_to, ":", 1);
		int change_uid = getuid_by_name(cu_parts[0].c_str());
		int change_gid = -1;
		if(cu_parts.size() > 1) // get_group
			change_gid = getgid_by_name(cu_parts[1].c_str());

		if(change_gid != -1 && (unsigned)change_gid != (unsigned)getgid()) {
			if(setgid(change_gid))
				fprintf(stderr, "could not change group id to id %d (%s): %s\n",
					change_gid, config.change_user_to.c_str(), strerror(errno));
		}
		if(change_uid != -1 && (unsigned)change_uid != (unsigned)getuid()) {
			if(setuid(change_uid))
				fprintf(stderr, "could not change user to id %d (%s): %s\n",
					change_uid, config.change_user_to.c_str(), strerror(errno));
		}
	}

	if(config.start_in_shell) {
#ifndef __ANDROID__
		const char* bin_sh = "/bin/sh";
#else
		const char* bin_sh = "/system/bin/sh";
#endif
		execle(bin_sh, bin_sh, "-c", config.cmdline.c_str(), NULL, envp);
		fprintf(stderr, "could not execle()\n%s -c %s\nerrno %d: %s\n",
			bin_sh, config.cmdline.c_str(), errno, strerror(errno));
		exit(1);
	}

	// build arguments
	list<string> args = split_command_line(config.cmdline);

	if(args.size() == 0) {
		fprintf(stderr, "can not execute empty command-line '%s'\n", repr(config.cmdline).c_str());
		exit(3);
	}

	char** argv = (char**) malloc(sizeof(char*) * (args.size() + 1)); // will be free'd on exit()...
	unsigned int k = 0;
	for(list<string>::iterator i = args.begin(); i != args.end(); i++)
		argv[k++] = strdup(i->c_str()); // will be free'd on exit()...
	argv[k] = NULL;

#ifdef __ANDROID__
	if(config.use_execvpe) {
		fprintf(stderr, "warning: use_execvpe was requested but is not supported on android17!\n");
	}
#endif
	const char* method_name;
#ifndef __ANDROID__
	if(config.use_execvpe) {
		method_name = "execvpe";
		if(seen_PATH)
			setenv("PATH", seen_PATH, 1);
		else
			fprintf(stderr, "warning: use_execvpe is set, but no PATH provided!\n");
		execvpe(argv[0], argv, envp);
	} else
#endif
	{
		method_name = "execve";
		execve(argv[0], argv, envp);
	}
	fprintf(stderr, "could not %s()\n%s\nerrno %d: %s\n", method_name, config.cmdline.c_str(), errno, strerror(errno));
	exit(2);
#endif
}

void ln_process::_fork_child()
{
#if defined(__LINUX__)

#if defined(__ANDROID__)
	config.no_pty = true;// there is no library/header based pty handling in android?
#else
	if(config.no_pty)
#endif
	{
		// ..as for states...
		// create output pipe
		os::pipe_init(pipe);
		fcntl(pipe.input_fd, F_SETFL, O_NONBLOCK);
		os::pipe_init(stdin_pipe);

		// fork new process
		pid = fork();

		if(pid == 0)
			_forked_child();

		close(pipe.output_fd);
		pipe.output_fd = -1;
		close(stdin_pipe.input_fd);
		stdin_pipe.input_fd = -1;
	}
#if !defined(__ANDROID__)
	else {
		// start process within new pseudo terminal
		// in this case pipe is not a real pipe!! only the first int will be written! the other end is by definition/implicitly == 0 and not returned!!
		pipe.input_fd = -1;
		pipe.output_fd = -1;

		char pty_name[1024] = "";
		pid = forkpty(&pipe.input_fd, pty_name, NULL, NULL);

		if(pid == 0)
			_forked_child();
		else if(config.term_rows != 0 && config.term_cols != 0)
			send_winch(config.term_rows, config.term_cols);
	}
#endif

	if(pid == -1)
		throw errno_exception_tb("could not fork new process!");
	instance->daemon->add_process_watch(pid, _process_terminated, this);
	log("added process watch for pid %d name %s at process %p, is_state: %d!", pid, repr(config.name).c_str(), this, config.is_state);

#endif // __LINUX__
}

void ln_process::_qnx_spawn_child()
{
#if defined(__QNX__)
	if(config.change_directory.size()) {
		if(chdir(config.change_directory.c_str()))
			log("chdir(%s) error: %s\n", repr(config.change_directory).c_str(), strerror(errno));
	} else
		chdir("/tmp");

	// create output pipe
	int null_input_fd;

	if(config.no_pty) {
		if(os::pipe_init(pipe))
			throw errno_exception_tb("could not create pipe!");
		// log("created output pipe input_fd %d, output_fd %d\n", pipe.input_fd, pipe.output_fd);
		fcntl(pipe.input_fd, F_SETFL, O_NONBLOCK);

		os::pipe_init(stdin_pipe);
		null_input_fd = stdin_pipe.input_fd;
	} else {
		char pty_name[1024];
		/* int ret = */
		pipe.input_fd = 0;
		pipe.output_fd = 0;
		openpty(&pipe.input_fd, &pipe.output_fd, pty_name, NULL, NULL);
		set_terminal(pipe.output_fd);
		null_input_fd = pipe.output_fd;
	}

	struct inheritance inh;
	// SPAWN_EXPLICIT_SCHED
	inh.flags = SPAWN_ALIGN_DEFAULT | SPAWN_SETSIGDEF | SPAWN_TCSETPGROUP | SPAWN_SETGROUP;
	inh.pgroup = SPAWN_NEWPGROUP;

	if(config.policy != -1 || config.priority != 0) {
		inh.flags |= SPAWN_EXPLICIT_SCHED;
		if(config.policy != -1)
			inh.policy = config.policy;
		if(config.priority != 0)
			inh.param.sched_priority = config.priority;
	}
#ifdef SPAWN_EXPLICIT_CPU
	if(config.affinity != -1) {
		inh.flags |= SPAWN_EXPLICIT_CPU;
		inh.runmask = config.affinity;
	}
#endif
	const char* old_path = NULL;
	bool do_use_execvpe;
	if(config.start_in_shell)
		do_use_execvpe = false; // shell will be specified as absolute /bin/sh!
	else if(config.cmdline[0] == '/' || config.cmdline[0] == '.')
		do_use_execvpe = false; // user did send path components, ignore `use_execvpe` request
	else if(config.use_execvpe)
		do_use_execvpe = true; // user requested `use_execvpe`
	else
		do_use_execvpe = false; // user did not request `use_execvpe`

	if(do_use_execvpe) {
		lock_mutex(&PATH_env_mutex);
		inh.flags |= SPAWN_SEARCH_PATH;

		if(seen_PATH) {
			old_path = getenv("PATH");
			setenv("PATH", seen_PATH, 1);
		} else
			fprintf(stderr, "warning: use_execvpe is set, but no PATH provided!\n");
	}
	
	int fd_map[] = { null_input_fd, pipe.output_fd, pipe.output_fd };
	
	if(config.start_in_shell) {
		char* cmd_ptr = (char*)config.cmdline.c_str();
		char* const argv[] = { "/bin/sh", "-c", cmd_ptr, NULL };
		pid = spawn(argv[0], sizeof(fd_map) / sizeof(int), fd_map, &inh, argv, envp);
	} else {
		// build arguments
		list<string> args = split_command_line(config.cmdline);
		vector<char*> argv(args.size() + 1);
		unsigned int k = 0;
		for(list<string>::iterator i = args.begin(); i != args.end(); i++)
			argv[k++] = (char*)i->c_str();
		argv[k] = NULL;
		pid = spawn(argv[0], sizeof(fd_map) / sizeof(int), fd_map, &inh, &argv[0], envp);
	}

	if(do_use_execvpe && seen_PATH) {
		setenv("PATH", old_path, 1);
		unlock_mutex(&PATH_env_mutex);
	}

	if(pid == -1) {
		string info = "";
		if(config.policy != -1 || config.priority != 0)
			info += format_string("\nyou requested policy %d, priority %d. valid range for this prio: %d to %d",
			                      config.policy, config.priority, sched_get_priority_min(config.policy), sched_get_priority_max(config.policy));
		info += format_string("\ncmdline: %s", repr(config.cmdline).c_str());
		throw errno_exception_tb("could not spawn() process!%s", info.c_str());
	}
	// log("closing output pipe output_fd %d\n", pipe.output_fd);
	close(stdin_pipe.input_fd);
	stdin_pipe.input_fd = -1;
	close(pipe.output_fd);
	close(null_input_fd);
	pipe.output_fd = -1;

#ifndef SPAWN_EXPLICIT_CPU
	// hack for pre qnx 6.3.2
	set_affinity(pid, -1, config.affinity);
#endif
	instance->daemon->add_process_watch(pid, _process_terminated, this);
	log("added process watch for pid %d name %s at process %p!", pid, repr(config.name).c_str(), this);
#endif
}

void ln_process::_win32_create_process()
{
#if defined(__WIN32__)
	if (os::pipe_init(pipe))
		throw errno_exception_tb("could not create pipe!");

	// build environment
	stringstream env_block_ss;
	string add_systemroot = getenv("SystemRoot");

	for(env_t::iterator i = env.begin(); i != env.end(); i++) {
		pair<string, string>& p = *i;
		if(p.first == "SystemRoot")
			add_systemroot = "";
		string env_pair = format_string("%s=%s", p.first.c_str(), p.second.c_str());
		env_block_ss.write(env_pair.c_str(), env_pair.size() + 1); // write including zero!
		log("ln_process adding %s to env", repr(env_pair).c_str());
	}
	if(add_systemroot != "") {
		string env_pair = format_string("SystemRoot=%s", add_systemroot.c_str());
		env_block_ss.write(env_pair.c_str(), env_pair.size() + 1); // write including zero!
	}

	env_block_ss.write("\0", 1); // write trailing zero to terminate block!
	char* env_block = (char*)malloc(env_block_ss.str().size());
	memcpy(env_block, env_block_ss.str().c_str(), env_block_ss.str().size());

	// build arguments
	log("ln_process cmd line: %s", config.cmdline.c_str());
	list<string> args = split_command_line(config.cmdline);
	string joined_cmd;
	if(config.start_in_shell) {
		joined_cmd = "c:\\windows\\system32\\cmd.exe /c \"" + config.cmdline + "\"";
	} else {
		vector<string> argsv(args.begin(), args.end());
		joined_cmd = join_string(argsv, " ");
	}

	char* non_const_cmd = strdup(joined_cmd.c_str()); // TODO: free!!

	log("ln_process cmd after join line: %s", non_const_cmd);
	log("ln_process cmd after join line: %s", repr(non_const_cmd).c_str());

	// build startup info
	STARTUPINFO info;
	memset(&info, 0, sizeof(info));

	info.cb = sizeof(info);
	info.dwFlags    = STARTF_USESTDHANDLES | CREATE_NEW_PROCESS_GROUP ;
	info.hStdInput  = GetStdHandle(STD_INPUT_HANDLE); // todo: not nice!
	info.hStdOutput = pipe.output_fd; // pipe.input_fd;
	info.hStdError  = pipe.output_fd;

	if(!SetHandleInformation(pipe.input_fd, HANDLE_FLAG_INHERIT, 0)) {
		free(env_block);
		throw errno_exception_tb("could not set inherit to 0 for read side of pipe!");
	}

	const char* dir = NULL;
	if(config.change_directory.size())
		dir = config.change_directory.c_str();
	else
		chdir("c:\\");

	PROCESS_INFORMATION pinfo;
	// maybe use this: http://www.codeproject.com/Articles/16163/Real-Time-Console-Output-Redirection
	BOOL cp_ret = CreateProcess(
	                  (LPCSTR)args.begin()->c_str(),
	                  (LPSTR)non_const_cmd,
	                  NULL,       // LPSECURITY_ATTRIBUTES lpProcessAttributes,
	                  NULL,       // LPSECURITY_ATTRIBUTES lpThreadAttributes,
	                  TRUE,      // BOOL bInheritHandles,
	                  0,          // DWORD dwCreationFlags,
	                  //NULL, // env_block,  // LPVOID lpEnvironment, // TODO: windows needs systemroot and so on to find winsock dlls and so on!
	                  env_block,  // LPVOID lpEnvironment, // TODO: windows needs systemroot and so on to find winsock dlls and so on!
	                  (LPCSTR)dir,       // LPCTSTR lpCurrentDirectory,
	                  &info,      // LPSTARTUPINFO lpStartupInfo,
	                  &pinfo      // LPPROCESS_INFORMATION lpProcessInformation
	              );
	if(!cp_ret)
		throw errno_exception_tb("could not CreateProcess()!");
	pid = pinfo.dwProcessId;
	win32_process_handle = pinfo.hProcess;
	CloseHandle(pinfo.hThread);
	CloseHandle(pipe.output_fd);
	pipe.output_fd = NULL;

	instance->daemon->add_process_watch(pid, _process_terminated, this);
	log("added process watch for pid %d, name %s, term_timeout %.1f at process %p!",
	    pid, repr(config.name).c_str(), config.term_timeout, this);
#endif
}

#ifndef __VXWORKS__
int ln_process::execute()
{
#if defined(__WIN32__)
	envp = NULL;
	_win32_create_process();

#else // assume linux / unix-like
	// build environment
	envp = (char**) malloc(sizeof(char*) * (env.size() + 1));
	int k = 0;
	seen_PATH = NULL;
	for(env_t::iterator i = env.begin(); i != env.end(); i++) {
		pair<string, string>& p = *i;
		envp[k++] = strdup(format_string("%s=%s", p.first.c_str(), p.second.c_str()).c_str());
		if(p.first == "PATH")
			seen_PATH = p.second.c_str();
	}
	envp[k] = NULL;
#if defined(__LINUX__) // only known nearly complete POSIX implementation...
	_fork_child();
#elif defined(__QNX__)
	_qnx_spawn_child();
#endif

#endif

	// create output buffer
	set_log_output(config.log_output, config.buffer_size);

#ifndef __WIN32__ // windows imple will use main_semaphore for output triggering....
	// create output buffer notification pipe
	os::pipe_init(buffer_pipe);
	log("created buffer pipe input_fd %d, output_fd %d\n", buffer_pipe.input_fd, buffer_pipe.output_fd);

	fcntl(buffer_pipe.output_fd, F_SETFL, O_NONBLOCK);
	fcntl(buffer_pipe.input_fd, F_SETFL, O_NONBLOCK);
	instance->daemon->add_io_watch(buffer_pipe.input_fd, IO_WATCH_READ,
	                               output_buffer_notification, this, format_string("output buffer notification for pid %d", pid));
#endif

	// start output thread
	int ret;
	output_thread_running = true;
	output_thread_joined = false;
	if((ret = create_thread(&output_thread_tid, _output_thread, this)))
		// if((ret = pthread_create(&output_thread_tid, NULL, &_output_thread, this)))
		throw retval_exception_tb(ret, "could not create output thread pthread_create()!");

	instance->register_process(this);

	after_execute();
	return 0;
}
#endif

void ln_process::after_execute()
{
	if(config.is_state) {
		request_t r;
		r["request"] = repr("state_process_started");
		r["pid"] = format_string("%d", pid);
		r["name"] = repr(config.name);
		r["state_cmd"] = repr((*props)[string("state_cmd")]);

		if(instance->connection)
			instance->connection->send(r);
	}

}

ln_process::~ln_process()
{
	stop();
	if(pid != -1)
		instance->daemon->remove_process_watch(pid);
	output_thread_running = false;
	if(!output_thread_joined) {
		join_thread(output_thread_tid);
		output_thread_joined = true;
	}

	if(envp) {
		char** ecp = envp;
		while(*ecp) {
			free(*ecp);
			ecp++;
		}
		free(envp);
	}
	delete props;
	delete output_buffer;

	destroy_mutex(&PATH_env_mutex);
}


void ln_process::send_signal(pid_t pid, int signo)
{
	if (os::send_signal(pid, signo))
		throw errno_exception_tb("send_signal(%d, %d)", pid, signo);
}

void ln_process::set_log_output(bool log_output, unsigned int buffer_size)
{
	//log("setting output buffer to %s with size %u\n", log_output ? "enabled" : "disabled", buffer_size);

	if(!output_buffer)
		output_buffer = new char_ringbuffer(buffer_size);
	output_buffer->set_size(buffer_size);
	config.log_output = log_output;
}


void ln_process::set_ready()
{
	is_ready = true;
}

void ln_process::set_property(string name, py_value* value)
{
	props->value[name] = value->copy();

	if(name == "no_output_rate_limit") {
		no_output_rate_limit = ((string)*value).substr(0, 1) == "1";
		log("[%d] no_output_rate_limit: %d\n", pid, no_output_rate_limit);
	}
}


void ln_process::stop()
{
	if(pid != -1) {
#ifdef __WIN32__
		os::kill((pid_t)win32_process_handle);
#else
		int ret;
		ret = os::kill(-pid);
		if(ret == -1)
			output_line(format_string("\r\nerror stopping process %d: %s\r\n", -pid, strerror(errno)).c_str());
		ret = os::kill(pid);
		if(ret == -1)
			output_line(format_string("\r\nerror stopping process %d: %s\r\n", pid, strerror(errno)).c_str());
#endif
	}
}

void _output_line(void* instance, string line) {
	ln_process* self = (ln_process*)instance;
	self->output_line(line);
}

void ln_process::output_line(string line) {
	// printf("write line...\n");
	output_buffer->write(line);

#ifdef __WIN32__
	// do this in output thread so that max_output_frequency is used!
	//instance->daemon->_have_process_output = true;
	// ln_semaphore_post(instance->daemon->main_thread_finished);
#else
	// trigger sending of output buffer
	// printf("triggering output!\n");
	// -- trigger later, as soon as select() says there is nothing more to read // write(output_buffer_write_fd, ".", 1);
#endif
}

void ln_process::send_output_buffer()
{
	unsigned int bytes_lost;
	string data = output_buffer->get(false, &bytes_lost);
	if(!data.size() || !instance->connection) {
		// log("[%d] empty send_output_buffer!\n", pid);
		return;
	}
	if(bytes_lost > 0)
		data = format_string("\r\n((lost %d bytes in process output thread))\r\n%s", bytes_lost, data.c_str());

	request_t r;
	r["request"] = repr("process_output");
	r["pid"] = format_string("%d", pid);
	r["name"] = repr(config.name);
	r["output"] = format_string("data %d\n", data.size()) + data; // bytes-string repr(data);
	if(config.is_state) {
		r["state_cmd"] = repr((*props)[string("state_cmd")]);
	}
	try {
		instance->connection->send(r);
	}
	catch(const exception& e) {
		log("[%d] got exception while sending output to manager: %s\n", pid, e.what());
	}
}

void* ln_process::output_thread()
{
	// todo: for what this thread? why not just register an io_watch with the daemon?!
	log("[%d] process output thread %p starting at tid %d!\n", pid, output_thread_tid, os::gettid());
	// pthread_setname_np(pthread_self(), format_string("lnd:ot:%s", config.name.c_str()).c_str());

	line_assembler la(this, _output_line);
	la.set_return_newline(true);
	size_t buffersize = (unsigned int)(0.25 * output_buffer->get_size());
	char* buffer = new char[buffersize];
	double max_output_fusion_time = 1.0 / config.max_output_frequency;
	bool no_buffer_timeout_next_time = false;
	bool output_triggered = false;
	double last_send = ln_get_monotonic_time();
	bool did_atleast_single_run = false;
#ifdef __WIN32__
	while (output_thread_running) {
		DWORD n;
		// log("[%d] process output thread reading!\n", pid);
		// we need something like select to implement max_output_fusion time!

		/*
		  does not work on unnamed pipe:
		double timeout = 0.5;
		if(!no_buffer_timeout_next_time && output_buffer->has_data())
			timeout = 0.05;
		timeouts.ReadTotalTimeoutConstant = (DWORD)(1000 * timeout);
		if(SetCommTimeouts(pipe.input_fd, &timeouts) == 0) {
			fprintf(stderr, "SetCommTimeouts() failed!\n");
		}
		*/

		BOOL ret = ReadFile(pipe.input_fd, buffer, 64, &n, NULL); // small buffer to get more reactive output
		// log("[%d] process output thread ReadFile: %d %d\n", pid, ret, n);
		// log("[%d] process output thread ReadFile: %s\n", pid, repr(buffer, n).c_str());
		if (!ret) {
			// throw errno_exception_tb("ReadFile()");
			break;
		}
		if (config.log_output && output_buffer) {
			// log("[%d] process output thread writing to line assembler!\n", pid);
			// la.write(buffer, n);
			string line = string(buffer, n);
			last_output_buffer.write(line);

			if(!config.blocking_output)
				output_line(line);
			else {
				while(true) {
					unsigned int space_in_output_buffer_left = output_buffer->get_size() - output_buffer->get_data_len();
					if(line.size() <= space_in_output_buffer_left)
						break;

					if(space_in_output_buffer_left) {
						output_line(line.substr(0, space_in_output_buffer_left));
						line = line.substr(space_in_output_buffer_left);
					}
					// now wait until there is space in output buffer
					instance->daemon->_have_process_output = true;
					ln_semaphore_post(instance->daemon->main_thread_finished);
					output_buffer->wait_for_space();
				}
				if(line.size())
					output_line(line);
			}

			if(output_buffer->get_data_len() > 0.75 * output_buffer->get_size() || no_output_rate_limit || (ln_get_monotonic_time() - last_send) > max_output_fusion_time) {
				// send now!
				// to trigger immediately:
				instance->daemon->_have_process_output = true;
				ln_semaphore_post(instance->daemon->main_thread_finished);
				last_send = ln_get_monotonic_time();
			}
		}
	}
#else // LINUX, QNX, VxWORKS
#ifdef __VXWORKS__
	// log("[%d] process output thread on vxworks at pthread-self: 0x%x taskIdSelf(): 0x%0x\n", pid, pthread_self(), taskIdSelf());
	// log("        pthread-self: 0x%x\n", pthread_self());
	// log("        taskIdSelf(): 0x%x\n", taskIdSelf());
#endif
	while(output_thread_running || !did_atleast_single_run) { // check at the end for condition! do at least one read!!!
		// printf("process output thread looping\n");
		fd_set read_fds;
		FD_ZERO(&read_fds);
		FD_SET(pipe.input_fd, &read_fds);
		int n;
		// log("[%d] process output thread calls select()\n", pid);
		struct timeval tv = {0, 500 * 1000};
		if((!no_buffer_timeout_next_time && output_buffer->has_data()) || !did_atleast_single_run) { // there is data waiting to be sent -- set timeout to zero or small value!
			// log("[%d] process has output data waiting: %d bytes!\n", pid, output_buffer->get_data_len());
			// usleep(250*1000); // wait for new data to arrive
			tv.tv_usec = 50 * 1000; // 50ms
		}
		n = select(pipe.input_fd + 1, &read_fds, NULL, NULL, &tv);
		// log("[%d] output_thread select() returned - : %d\n\n", pid, n);

		if(n == -1 && (errno == EAGAIN || errno == EINTR)) {
			// log("[%d] process output thread got select errno %d: %s!\n", pid, errno, strerror(errno));
			continue;
		}
		if(n == -1) {
			log("[%d] process output thread error from select: %d, errno: %d, %s!\n", pid, n, errno, strerror(errno));
			break;
		}

		// log("[%d] process check for has_data\n", pid);
		if(!output_triggered && output_buffer->has_data() && (output_buffer->get_data_len() > 0.75 * output_buffer->get_size() || !did_atleast_single_run)) {
			// log("[%d] process trigger output buffer sending because more than 3/4 full! len: %d, size: %d, did_atleast_single_run: %d\n", pid, output_buffer->get_data_len(), output_buffer->get_size(), did_atleast_single_run);
			write(buffer_pipe.output_fd, ".", 1);
			output_triggered = true;
			no_buffer_timeout_next_time = true;
			did_atleast_single_run = true;
			last_send = ln_get_monotonic_time();
		} else if(!output_triggered && output_buffer->has_data() && (no_output_rate_limit || ln_get_monotonic_time() - last_send > max_output_fusion_time)) {
			// log("[%d] process trigger output buffer sending because buffer of age %.3fs is older than %.3fs!\n", pid, ln_get_monotonic_time() - last_send, max_output_fusion_time);
			write(buffer_pipe.output_fd, ".", 1);
			output_triggered = true;
			no_buffer_timeout_next_time = true;
			last_send = ln_get_monotonic_time();
		}
		if(n == 0) { // timeout
			// log("[%d] process timeout!\n", pid);
			did_atleast_single_run = true;
			if(!output_triggered && output_buffer->has_data()) { // trigger sending of output buffer
				// log("[%d] process trigger output sending because of data!\n", pid);
				write(buffer_pipe.output_fd, ".", 1);
				output_triggered = true;
				no_buffer_timeout_next_time = true;
				last_send = ln_get_monotonic_time();
			}

			continue;
		}

		//log("[%d] process output thread calls read()\n", pid);
		n = read(pipe.input_fd, buffer, buffersize);
		if(n == -1) {
			if (errno == EAGAIN || errno == EINTR) {
				log("[%d] process output thread got read errno %d: %s!\n",
				    pid, errno, strerror(errno));
				continue;
			}

			log("[%d] process output thread error from read: %d, errno: %d, %s!\n",
			    pid, n, errno, strerror(errno));
			break;
		}

		if(n == 0) {
			log("[%d] process output thread eof from read: %d!\n", pid, n);
			break;
		}

		// printf("[%d] process output: %.*s\n", pid, n, buffer);
		// log("[%d] process output: %.*s\n", pid, n, buffer);
		/*
		if(config.is_state) {
		  log("[%d] state output: %.*s\n", pid, n, buffer);
		}
		*/
		if(config.log_output && output_buffer) {
			// printf("output from process to la: %s\n", repr(buffer, n).c_str());
			no_buffer_timeout_next_time = false;
			output_triggered = false;
			// la.write(buffer, n); // disable line assembler

			string line = string(buffer, n);
			last_output_buffer.write(line);
			if(!config.blocking_output)
				output_line(line);
			else {
				while(true) {
					unsigned int space_in_output_buffer_left = output_buffer->get_size() - output_buffer->get_data_len();
					if(line.size() <= space_in_output_buffer_left)
						break;

					if(space_in_output_buffer_left) {
						output_line(line.substr(0, space_in_output_buffer_left));
						line = line.substr(space_in_output_buffer_left);
					}
					// now wait until there is space in output buffer
					write(buffer_pipe.output_fd, ".", 1);
					output_buffer->wait_for_space();
				}
				if(line.size())
					output_line(line);
			}
		}
	}
#endif
	delete[] buffer;
	log("[%d] process output thread %p exiting\n", pid, output_thread_tid);

	// log("[%d] process output thread exiting!\n", pid);
	return NULL;
}

void ln_process::terminated(int status)
{
#if !defined(__WIN32__)
	if(!WIFEXITED(status) && !WIFSIGNALED(status)) {
	  log("::terminated() for pid %d, process %p - unknown signal reason in status %d: not exited and not signaled! assuming its terminated.\n"
	      "WIFCONTINUED: %d, WIFSTOPPED: %d\n",
	      pid, this, status,
	      WIFCONTINUED(status),
	      WIFSTOPPED(status));
	  // return;
	}
#endif
	log("[%d] process %s cmd %s terminated with status %d\n", pid, repr(config.name).c_str(), repr(config.cmdline).c_str(), status);

	instance->daemon->remove_process_watch(pid);
	output_thread_running = false;
	if(!output_thread_joined) {
		join_thread(output_thread_tid);
		output_thread_joined = true;
	}

	if(output_buffer && config.log_output) {
		log("[%d] process send_output_buffer, is_state: %d\n", pid, config.is_state);
		send_output_buffer();
	}

	log("[%d] process send process_terminated\n", pid);
	request_t r;
	r["request"] = repr("process_terminated");
	r["pid"] = format_string("%d", pid);
	r["name"] = repr(config.name);
	r["status"] = format_string("%d", status);
	if(config.is_state) {
		r["state_cmd"] = repr((*props)[string("state_cmd")]);
	}
	r["retval"] = "None";
	r["terminated_by_signal"] = "None";
#if !defined(__WIN32__)
	if (WIFEXITED(status))
		r["retval"] = format_string("%d", WEXITSTATUS(status));
	else if (WIFSIGNALED(status))
		r["terminated_by_signal"] = format_string("%d", WTERMSIG(status));
#else
	r["retval"] = format_string("%d", status);
#endif
	if(instance->connection) {
		instance->connection->send(r);
		log("[%d] did send process_terminated message\n", pid);
	} else 
		log("[%d] did NOT send process_terminated message, as there is no lnm connection!\n", pid);

#if !defined(__WIN32__)
	// close buffer pipe
	instance->daemon->remove_io_watch(buffer_pipe.input_fd);
	os::pipe_close(buffer_pipe);
#endif

	// close pipe
	// log("[%d] process close_pipe()\n", pid);
	os::pipe_close(pipe);
	os::pipe_close(stdin_pipe);

	pid_t p = pid;
	pid = -1;
	this->status = status;

	// call exit callback if any
	if (exitcb) {
		log("[%d] process calling exit callback with status %d\n", p, status);
		(*exitcb)(p, status, exitcb_arg);
	}

	log("[%d] process removed!\n", p);
	instance->remove_process(p); // should be the last thing!
}

py_tuple* ln_process::get_tuple(py_tuple* t)
{
	if(!t)
		t = new py_tuple();

	t->value.push_front(props->copy());
	t->value.push_front(new py_bytes(last_output_buffer.get(true)));
	t->value.push_front(new py_float(start_time));
	t->value.push_front(new py_int(is_ready));
	t->value.push_front(new py_int(pid));
	t->value.push_front(new py_string(config.cmdline));

	t->value.push_front(new py_string(config.name));

	return t;
}


#ifdef __QNX__
#include <dirent.h>
#include <sys/iofunc.h>
#include <sys/dispatch.h>
#include <sys/neutrino.h>
#include <sys/procfs.h>
#include <sys/stat.h>

struct dinfo_s {
	procfs_debuginfo    info;
	char                pathbuffer[PATH_MAX]; /* 1st byte is info.path[0] */
};

int get_process_info(char* pidstr, py_list* l, py_list* h, bool with_threads, filter_t& filter)
{
	static struct dinfo_s dinfo;
	char buf[PATH_MAX + 1];
	pthread_t last_thread_id = 1;
	sprintf(buf, "/proc/%s/as", pidstr);

	int pidint = atoi(pidstr);
	bool ok = true;
	for(filter_t::iterator i = filter.begin(); i != filter.end(); i++) {
		filter_item_t& item = *i;
		if(item.op == "==" && item.key == "pid") {
			int value = atoi(item.value.c_str());
			if(pidint != value) {
				ok = false;
				break;
			}
		}
	}
	if(!ok)
		return -1;


	int fd;
	if((fd = open(buf, O_RDONLY|O_NONBLOCK)) == -1) {
		// log("open /proc/%s/as failed!", pidstr);
		return -1;
	}

	if(devctl(fd, DCMD_PROC_MAPDEBUG_BASE, &dinfo, sizeof(dinfo), NULL) != EOK) {
		// log("DCMD_PROC_MAPDEBUG_BASE failed for pid %s", pidstr);
		strcpy(dinfo.info.path, "-");
		//goto error;
	}

	debug_process_t proc;
	if(devctl(fd, DCMD_PROC_INFO, &proc, sizeof(proc), NULL) != EOK) {
		log("DCMD_PROC_INFO failed for pid %s", pidstr);
		goto error;
	}
#define fp(t, n) { l->value.push_back(new py_ ##t(proc.n)); if(h) { h->value.push_back(new py_string(string(#n))); } }
#define fprn(t, n, rn) { l->value.push_back(new py_ ##t(proc.n)); if(h) { h->value.push_back(new py_string(string(rn))); } }

	for(unsigned int i = 0; i < proc.num_threads; i++) {
		debug_thread_t thread;
		thread.tid = last_thread_id;
		if(devctl(fd, DCMD_PROC_TIDSTATUS, &thread, sizeof(thread), NULL) != EOK) {
			log("DCMD_PROC_STATUS failed for pid %s tid %x", pidstr, thread.tid);
			goto error;
		}
		last_thread_id = thread.tid + 1;
		if(last_thread_id == 1) {
			// there are no more threads
			break;
		}
#define ft(t, n) { l->value.push_back(new py_ ##t(thread.n)); if(h) { h->value.push_back(new py_string(string(#n))); } }

		bool ok = true;
		for(filter_t::iterator i = filter.begin(); i != filter.end(); i++) {
			filter_item_t& item = *i;
#define compare_int(n) if(item.op == "==" && item.key == #n) { int value = atoi(item.value.c_str()); if(proc.n != value) { ok = false; break; } }
#define compare_long(n) if(item.op == "==" && item.key == #n) { long value = atol(item.value.c_str()); if(proc.n != value) { ok = false; break; } }
#define compare_ulong(n) if(item.op == "==" && item.key == #n) { unsigned long value = strtoul(item.value.c_str(), NULL, 0); if(proc.n != value) { ok = false; break; } }

#define compare_ft_int(n) if(item.op == "==" && item.key == #n) { int value = atoi(item.value.c_str()); if(thread.n != value) { ok = false; break; } }
#define compare_ft_long(n) if(item.op == "==" && item.key == #n) { long value = atol(item.value.c_str()); if(thread.n != value) { ok = false; break; } }

			compare_ft_int(pid);
			compare_ft_long(tid);
			compare_int(parent);
			compare_ft_int(priority);
			compare_ft_int(real_priority);
			compare_ft_int(policy);
			compare_int(sid);
		}
		if(!ok)
			continue;

		// from process
		l->value.push_back(new py_string(dinfo.info.path));
		if(h)
			h->value.push_back(new py_string(string("path")));
		//fp(int, pid);
		fp(int, parent);
		fprn(int, flags, "proc_flags");
		fp(int, umask);
		// fp(int, child);
		// fp(int, sibling);
		fp(int, sid);
		fp(int, base_address);
		fp(int, initial_stack);
		fp(int, uid);
		fp(int, gid);
		fp(int, num_threads);
		fp(int, num_timers);
		fprn(long, start_time, "proc_start_time");
		fp(long, utime);
		fp(long, stime);
		fp(long, cutime);
		fp(long, cstime);
		fprn(int, priority, "proc_prio");

		// overwrite from thread
		ft(int, pid);
		ft(long, tid);
		ft(int, flags);
		ft(int, why);
		ft(int, what);
		ft(int, stksize);
		ft(int, tid_flags);
		ft(int, priority);
		ft(int, real_priority);
		ft(int, policy);
		ft(int, state);
		ft(int, syscall);
		ft(int, last_cpu);
		ft(int, last_chid);
		ft(long, start_time);
		ft(long, sutime);

		h = NULL;
		if(!with_threads)
			break;
	}

	close(fd);
	return 0;
error:
	close(fd);
	return -1;
}
#endif

py_list* ln_process::get_process_list(bool with_env, bool with_threads, string filter_string)
{
	py_list* ret = new py_list();

#ifdef __QNX__
	py_list* hptr = new py_list();
	ret->value.push_back(hptr);

	py_list* t = new py_list();
	ret->value.push_back(t);

	filter_t filter = parse_filter(filter_string);

	DIR* dir = opendir("/proc");
	if(!dir) {
		log("could not opendir(/proc)!");
		return NULL;
	}
	struct dirent* entry;
	while(true) {
		entry = readdir(dir);
		if(!entry)
			break;
		if(!isdigit(entry->d_name[0]))
			continue;
		if(get_process_info(entry->d_name, t, hptr, with_threads, filter) != -1)
			hptr = NULL;
	}
	closedir(dir);

#endif

	log("retrieve processes return %d", ret);

	return ret;
}

void ln_process::send_stdin(string text)
{
#ifndef __WIN32__
	int fd;
	if(config.no_pty)
		fd = stdin_pipe.output_fd;
	else
		fd = pipe.input_fd;
	write(fd, text.c_str(), text.size());
#else
	HANDLE fd;
	if(config.no_pty)
		fd = stdin_pipe.output_fd;
	else
		fd = pipe.input_fd;
	DWORD n = 0;
	WriteFile(fd, text.c_str(), text.size(), &n, NULL);
#endif
}

void ln_process::send_winch(int rows, int cols)
{
	if(config.no_pty)
		return;
#ifdef __LINUX__
	struct winsize size;
	memset(&size, 0, sizeof(size));
	size.ws_col = cols;
	size.ws_row = rows;
	int ret = ioctl(pipe.input_fd, TIOCSWINSZ, &size);
	if(ret)
		log("TIOCSWINSZ-ioctrl with %d rows and %d cols returned %d: %s\n", rows, cols, ret, strerror(errno));
#endif
}
