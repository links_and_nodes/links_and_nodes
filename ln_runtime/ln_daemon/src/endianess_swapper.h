#ifndef ENDIANESS_SWAPPER_H
#define ENDIANESS_SWAPPER_H

#include <string>
#include <vector>

class endianess_swapper {
	std::string format;
	typedef std::vector<uint8_t> format_counts_t;
	format_counts_t format_counts;
	std::vector<uint8_t> buffer;
	
public:

	void set_format(std::string format);
	void swap_into(void* target);
	void swap_msg(struct msghdr* message);
	
	const void* get_buffer() const {
		if(!format.size())
			return NULL;
		return &buffer[0];
	}
	inline operator bool() const {
		return format.size();
	}
	void swap(const uint8_t what, const void* dst, const void* src) const {
		if(what == 1) {
			*(uint8_t*)dst = *(uint8_t*)src;
/** those only seem to work on aligned data!!!
		} else if(what == 2) {
#ifdef __builtin_bswap16
			*(uint16_t*)dst = __builtin_bswap16(*(uint16_t*)src);
#else
			uint16_t& src_ = *(uint16_t*)src;
			*(uint16_t*)dst = ((src_ & 0xff) << 8) | (src_ >> 8);
#endif
#ifdef __builtin_bswap32
		} else if(what == 4) {
			*(uint32_t*)dst = __builtin_bswap32(*(uint32_t*)src);
#endif
#ifdef __builtin_bswap64
		} else if(what == 8) {
			*(uint64_t*)dst = __builtin_bswap64(*(uint64_t*)src);
#endif
*/
		} else {
			uint8_t* dst_ = (uint8_t*)dst;
			uint8_t* src_ = (uint8_t*)src;
			dst_ += what;
			for(unsigned int c = 0; c < what; c++)
				*(--dst_) = *(src_++);
		}
	}
};

#endif // ENDIANESS_SWAPPER_H
