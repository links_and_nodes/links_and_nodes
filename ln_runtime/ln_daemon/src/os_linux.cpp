/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#ifdef __LINUX__

#include "os_linux.h"

#include <time.h>
#include <sys/syscall.h>

namespace os {

int create_thread(thread_handle_t* thread_handle, thread_func_type thread, void* data) {
	int ret;
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	ret = pthread_create(thread_handle, &attr, thread, data);
	pthread_attr_destroy(&attr);
	return ret;
}

int cancel_thread(thread_handle_t thread_handle) {
#ifndef __ANDROID__
	return pthread_cancel(thread_handle);
#else
	return 0;
#endif
}

int join_thread(thread_handle_t thread_handle) {
	return pthread_join(thread_handle, NULL);
}

void test_cancel(thread_handle_t thread_handle) {
#ifndef __ANDROID__
	pthread_testcancel();
#endif
}

double ts2double(struct timespec* ts) {
	return (double)ts->tv_sec + (ts->tv_nsec / 1e9);
}

void double2ts(double s, struct timespec* ts) {
	ts->tv_sec = (time_t)s;
	ts->tv_nsec = (long)((s - ts->tv_sec) * 1e9);
}

void sleep(double s) {
	struct timespec ts;
	double2ts(s, &ts);
	nanosleep(&ts, NULL);
}

int set_prio(pid_t pid, int tid, int prio, int policy) {
	struct sched_param param;
	int current_policy = sched_getscheduler(tid);
	sched_getparam(tid, &param);

	if(prio != -1)
		param.sched_priority = prio;

	int ret = 0;
	if(policy != -1 && policy != current_policy)
		ret = sched_setscheduler(tid, policy, &param);
	else if(prio != -1)
		ret = sched_setparam(tid, &param);

	return ret;
}
int set_affinity(pid_t pid, int tid, int affinity) {
#ifndef __ANDROID__
	cpu_set_t cpuset;
        CPU_ZERO(&cpuset);
        for (unsigned int i = 0; i < (sizeof(affinity) * 8); ++i) 
		if(affinity & (1 << i))
			CPU_SET(i, &cpuset);
	return sched_setaffinity(tid, sizeof(cpu_set_t), &cpuset);
#else
	return 0;
#endif	
}


pid_t gettid() {
	return syscall( __NR_gettid );
}

int set_affinity_of_current_thread(int affinity) {
#ifndef __ANDROID__
	unsigned int mask = (unsigned) affinity;
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	for (unsigned int i = 0; i < (sizeof(mask)*8); ++i) 
		if (mask & (1 << i))
			CPU_SET(i, &cpuset);
	return pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);
#else
	return 0;
#endif
}


}
#endif
