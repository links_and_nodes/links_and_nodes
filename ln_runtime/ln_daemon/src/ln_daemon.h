#ifndef LN_DAEMON_H
#define LN_DAEMON_H

#include <map>
#include <string>

#include "os.h"
#include "util.h"

using namespace std;

class ln_daemon;
typedef map<string, string> request_t;
typedef void (*callback_t)(int, int, void*);
typedef bool (*timeout_callback_t)(void*);

#include "ln_manager_connection.h"
#include "ln_instance.h"

#include "ln_arbiter.h"

struct timeout_t {
	double interval;
	timeout_callback_t callback;
	void* data;
	int toid;
	string hint;
	double expiration;
};

struct io_watch {
	callback_t callback;
	void* data;
	int fd;
	int flags;
	string hint;
};

#define IO_WATCH_READ   1
#define IO_WATCH_WRITE  2
#define IO_WATCH_EXCEPT 4

struct process_watch {
	callback_t callback;
	void* data;
	pid_t pid;
};

class config_class {
public:
	bool isolated_test;
	bool log_stdout;
	bool background;
	string log_file;
	bool slow_mode;
	int fake_uid;
	string pid_file;
	string public_key_file;
	uint16_t port;
	
	config_class() {
		log_stdout = true;
		background = false;
		slow_mode = false;
		fake_uid = -1;
	}
};

extern config_class* config;

class ln_daemon {
	int fd; // listening socket for incoming ln_manager connections
	typedef map<int, io_watch> io_watches_t;
	io_watches_t io_watches;

	ln_arbiter_client arbiter_client;
	
	int timeout_counter;
	typedef map<int, timeout_t> timeouts_t;
	timeouts_t timeouts;
	double _get_until_next_timeout_expiration(struct timeval** tv);
	void _check_timeouts();

	typedef map<pid_t, process_watch> process_watches_t;
	process_watches_t process_watches;

	bool running;
	bool restart_io_watch_iterator;
	bool _sigchild_pending;
	double _last_sigchild_check;
	double _sigchild_check_interval;
	bool isolated_test_had_mgr_connection;

	typedef map<string, ln_instance*> instances_t;
	instances_t instances;

	typedef list<ln_manager_connection*> manager_connections_t;
	manager_connections_t manager_connections;

	
	static void on_new_manager_connection(int fd, int why, void* data);
#ifdef __ANDROID__
	typedef int ln_unix_connection;
	static void on_new_unix_connection(int fd, int why, void* data);
	static void on_unix_request(int fd, int why, void* data);
#endif

	friend class ln_state;
#ifdef __WIN32__
	friend class ln_process;
	ln_semaphore_handle_t main_semaphore;
	ln_semaphore_handle_t main_thread_finished;
	os::mutex_handle_t select_mutex;
	fd_set read_fds, write_fds, except_fds;
	bool _have_socket_event;
	bool _have_process_output;
	bool _need_finished_post;
	static thread_return_t THREAD_CALL _select_thread(void* data);
	void select_thread();
#endif

	//! check for child termination
	/*!
	  \return N/A
	*/
	void sigchild_check();
	
	//! updates iowatch fd_sets
	/*!
	  \param read_fds   fd set with read file descriptors
	  \param write_fds  fd set with write file descriptors
	  \param except_fds fd set with except file descriptors
	  \return           maximum fd number
	*/
	int update_iowatch_fd_sets(fd_set& read, fd_set& write, fd_set& except);

	void raise_resource_limits();
public:
	std::string public_key;
	std::string locked_by;
	string arch;
	// ln_manager_connection* manager_connection;
	bool no_hide;

	ln_daemon();
	~ln_daemon();

	int run(int argc, char* argv[]);

	void add_io_watch(int fd, int flags, callback_t callback, void* data, string hint);
	bool remove_io_watch(int fd);

	int add_timeout(double interval, timeout_callback_t callback, void* data, string hint);
	bool remove_timeout(int toid);

	void add_process_watch(pid_t pid, callback_t callback, void* data);
	bool remove_process_watch(pid_t pid);

	ln_instance* get_instance(string instance_name);
	void remove_instance(string instance_name);

	void stop();
  
	static void on_sigchild(int signo);
	static void on_sigint(int signo);

	void remove_manager_connection(ln_manager_connection* conn);

	void deregister_sigchild();
	void register_sigchild();

	void reset_last_sigchild_check();

	void load_public_key();
	void set_public_key(std::string contents, std::string locked_by);
};


#endif // LN_DAEMON_H
