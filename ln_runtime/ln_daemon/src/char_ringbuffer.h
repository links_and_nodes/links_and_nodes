// -*- mode: c++ -*-
#ifndef CHAR_RINGBUFFER_H
#define CHAR_RINGBUFFER_H

#include <sstream>

#include <string_util/string_util.h>
#include "lock.h"
#include "condition.h"

#ifdef DEBUG
#include <stdio.h>
#endif

using namespace std;
using namespace string_util;

void cleanup_unlock_mutex(void* mutex);

class char_ringbuffer {
	unsigned int size;
	char* data;

	unsigned int write_start;
	unsigned int read_start;
	bool buffer_full;
	unsigned int bytes_lost;
	
	flolock l;

	os::condition have_space_condition;
public:
	char_ringbuffer(unsigned int size) {
		data = (char*) malloc(size);
		if(!data)
			throw errno_exception_tb("could not get char_ringbuffer of size %d bytes\n", size);
		memset(data, 0, size);
		this->size = size;
		write_start = 0;
		read_start = 0;
		buffer_full = false;
		bytes_lost = 0;
		l.unlock();
	}
	
	~char_ringbuffer() {
		if(data)
			free(data);
		data = NULL;
	}

	unsigned int get_size() {
		return size;
	}
	
	void set_size(unsigned int new_size) {
		l();
		thread_cleanup_push(cleanup_unlock_mutex, &l);
		void* new_mem = realloc(data, new_size);
		if(new_mem) {
		  data = (char*)new_mem;
		  size = new_size;
		  memset(data, 0, size);
		  write_start = 0;
		  read_start = 0;
		  buffer_full = false;
		}
		l.unlock();
		thread_cleanup_pop(0);
	}
	
	void write(string data) {
		const char* new_data = data.c_str();
		int n = data.size();
		l();
		thread_cleanup_push(cleanup_unlock_mutex, &l);
		
		while((write_start + n) >= size) {
			buffer_full = true;
			
			unsigned int first_write = size - write_start;
			// printf("write at first only %d bytes: '%*.*s'\n", first_write, first_write, first_write, new_data);
			memcpy(this->data + write_start, new_data, first_write);
			new_data += first_write;
			n -= first_write;
			if(read_start > write_start) {
				// printf("set read_start to 0\n");
				bytes_lost += size - read_start;
				read_start = 0;
			}
			write_start = 0;
		}
		if(n) {
			//printf("write at last %d bytes: '%*.*s'\n", n, n, n, new_data);
			memcpy(this->data + write_start, new_data, n);
			if(buffer_full && write_start == read_start) {
				//printf("increment read_start by %d\n", n);
				read_start += n;
				bytes_lost += n;
			} else if(read_start > write_start && write_start + n > read_start) {
				//printf("set read start to new write_start!\n");
				bytes_lost += (write_start + n) - read_start;
				read_start = write_start + n;
			}
			write_start += n;
		}
		l.unlock();
		thread_cleanup_pop(0);
	}

#ifdef DEBUG
	void debug() {
		printf("write_start: %d\nread_start: %d\nbuffer_full: %d\n", write_start, read_start, buffer_full);
		printf("contents:\n");
		for(unsigned int i = 0; i < size; i++) {
			printf("%c", isprint(data[i]) ? data[i] : '.');
		}
		printf("\n");
		printf("get: %s\n\n", repr(get(true)).c_str());
	}
#endif
	string get(bool keep=false, unsigned int* bytes_lost_ret=NULL) {
		stringstream ss;
		unsigned int this_read_start;
		l();
		thread_cleanup_push(cleanup_unlock_mutex, &l);
		this_read_start = read_start;
		
		if(buffer_full) {
			ss << string(data + read_start, size - this_read_start);
			this_read_start = 0;
		}
		ss << string(data + this_read_start, write_start - this_read_start);
		if(!keep) {
			read_start = write_start;
			buffer_full = false;
		}
		if(bytes_lost_ret)
			*bytes_lost_ret = bytes_lost;
		bytes_lost = 0;
		l.unlock();
		have_space_condition.signal();
		thread_cleanup_pop(0);
		return ss.str();
	}

	void wait_for_space() {
		l();
		while((size - get_data_len()) == 0) {
			have_space_condition.wait(l);
		}
		l.unlock();
	}
	
	bool has_data() {
		return read_start != write_start || buffer_full;
	}
	unsigned int get_data_len() {
		unsigned int s = 0;
		unsigned int rs = read_start;
		if(buffer_full) {
			s = size - read_start;
			rs = 0;
		}
		s += write_start - rs;
		return s;
	}
};

#endif // CHAR_RINGBUFFER_H
