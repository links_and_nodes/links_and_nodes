#ifndef LN_INSTANCE
#define LN_INSTANCE

class ln_instance;
#include "ln_manager_connection.h"
#include "ln_ports.h"
#include "ln_process.h"
#include "ln_state.h"
#include "ln_tcp_forward.h"
#include "log_token.h"

#define SIGTERM_TIMEOUT 2.0

class ln_instance {
	friend class ln_manager_connection;

	string name;

	typedef map<string, ln_port*> ports_t;
	ports_t ports;

	typedef map<int, ln_process*> processes_t;
	processes_t processes;
	
	typedef struct {
		int timeout_id;
		double term_time;
		ln_process* process;
	} process_to_stop_t;
	typedef list<process_to_stop_t> processes_to_stop_t;
	processes_to_stop_t processes_to_stop;

	typedef map<string, ln_state*> states_t;
	states_t states;

	typedef list<ln_tcp_forward*> tcp_forwards_t;
	tcp_forwards_t tcp_forwards;

	typedef std::map<std::string, log_token*> log_tokens_t;
	log_tokens_t log_tokens;
	os::mutex_handle_t log_tokens_mutex;
public:
	struct FirewallTest {
		ln_instance* instance;
		unsigned int udp_port;
		unsigned int tcp_port;

		int udp_fd = -1;
		int tcp_fd = -1;
		std::list<int> tcp_client_fds;

		FirewallTest(ln_instance* instance) : instance(instance) {};
		~FirewallTest();

		void open();
		static void on_incoming_udp(int fd, int why, void* instance);
		static void on_incoming_tcp(int fd, int why, void* instance);
		static void on_incoming_tcp_connection(int fd, int why, void* instance);
		void ack_reception(const std::string& kind, uint64_t remote_id);
	} firewall_test;
	string manager;
	ln_daemon* daemon;
	ln_manager_connection* connection; // optional

	ln_instance(ln_daemon* daemon, string name, ln_manager_connection* connection=NULL);
	~ln_instance();

	// process management
	void register_process(ln_process* process);
	void remove_process(int pid);
	ln_process* get_process(int pid);
	void stop_process(ln_process* process, unsigned int term_signal);
	static bool _stop_process_timeout(void* instance);
	bool stop_process_timeout();
	
	// port management
	void delete_port(string port);
	void connect_ports(string source, string target);
	void disconnect_ports(string source, string target);
	ln_port* get_port(string port);
	void register_port(ln_port* port);
	void fill_ports_list(py_list& l, int with_last_packet=0);
	void fill_processes_list(py_list& l);
#ifdef __ANDROID__
	bool get_shm_fd(string shm_name, int* fd, unsigned int* shm_size);
#endif
	void register_log_token(request_t& answer, std::string data);
	void remove_log_token(std::string token);
	void remove_log_tokens(py_list* tokens_pylist);
	
	// states
	ln_state* get_state(string name, string check, string up, string down, bool os_state);
	ln_state* get_state(string name);

	// tcp forwards
	void register_tcp_forward(ln_tcp_forward* forward);
	void delete_tcp_forward(ln_tcp_forward* forward, bool do_delete=true);
	bool delete_tcp_forward(string name);
	string is_tcp_forward_source_port(int port);
	void fill_tcp_forwards_list(py_list& l);

	void open_fw_test_ports(unsigned int& udp_port, unsigned int& tcp_port);
	void test_for_fw(std::string ip, unsigned int udp_port, unsigned int tcp_port, uint64_t remote_id, double timeout);

	void release_all_resources();
};


#endif // LN_INSTANCE

