/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#ifdef __VXWORKS__


#include "os.h"
#include <sys/ioctl.h>

#include <string_util/string_util.h>

#include <stdint.h>
#include "ln_helper_api.h"

#include "telnet_connection.h"

#include "filter.h"

using namespace string_util;
class ln_helper;

static ln_helper* singleton = NULL;
static telnet_connection* conn = NULL;

class ln_helper {
	int fd;

	ln_helper() {
		fd = open("/dev/ln_helper", O_RDWR);
		if(fd != -1)
			return;

		if(0) { // vxworks seems to not allow local connections to telnet port
		// try to load module
		if(!conn)
			conn = new telnet_connection("localhost");
		try {
			conn->wait_prompt();		
			printf("output: %s\n", repr(conn->output.str()).c_str());
			
			conn->write("module load /home/schm_fl/workspace/ln_base/ln_helper/PENTIUM4gnu_SMP/ln_helper/Debug/ln_helper.out\n");
			conn->wait_prompt();		
			printf("output: %s\n", repr(conn->output.str()).c_str());
			
			conn->write("C init_ln_helper\n");
			conn->wait_prompt();		
			printf("output: %s\n", repr(conn->output.str()).c_str());

			// try again
			fd = open("/dev/ln_helper", O_RDWR);
			if(fd != -1)
				return;

		}
		catch(exception& e) {
			printf("got telnet exception: %s\n", e.what());
			delete conn;
			conn = NULL;
		}
		}
		throw errno_exception_tb("no ln_helper avaliable!");
	}
public:
	~ln_helper() {
		if(conn)
			delete conn;
	}
	static ln_helper* get_ln_helper() {
		if(singleton)
			return singleton;
		singleton = new ln_helper();
		return singleton;
	}
	static void free_ln_helper() {
		if(singleton)
			delete singleton;
	}

	string get_processlist(string filter_string) {
		struct ln_helper_get_proclist_t args;
		args.max_output = 1024 * 10;
		args.output = (char*)malloc(args.max_output);
		args.n_output = 0;

		// prepare filter
		filter_t filter = parse_filter(filter_string);
		args.n_items = filter.size();
		args.filter_items = (ln_helper_get_proclist_filter_item_t*)malloc(args.n_items * sizeof(ln_helper_get_proclist_filter_item_t));
		ln_helper_get_proclist_filter_item_t* current = args.filter_items;
		for(filter_t::iterator i = filter.begin(); i != filter.end(); i++) {
			filter_item_t& item = *i;
			current->key = (char*)item.key.c_str();
			current->op = (char*)item.op.c_str();
			current->value = (char*)item.value.c_str();
			current++;
		}
		int ret = ioctl(fd, LN_HELPER_GET_PROCLIST, &args);
		if(ret)
			throw str_exception_tb("ln_helper GET_PROCLIST returned %d!", ret);
		string retstr(args.output, args.n_output);
		free(args.output);

		return retstr;
	}
	void set_prio(int tid, int prio, int mask) {
		struct ln_helper_set_prio_t args;
		args.tid = tid;
		args.prio = prio;
		args.mask = mask;
		int ret = ioctl(fd, LN_HELPER_SET_PRIO, &args);
		if(ret)
			throw str_exception_tb("ln_helper SET_PRIO returned %d!", ret);
	}

	int gettid() {
		int tid;
		int ret = ioctl(fd, LN_HELPER_GET_TASKIDSELF, &tid);
		if(ret)
			throw str_exception_tb("ln_helper gettid returned %d!", ret);
		printf("tid from ln_helper: 0x%x\n", tid);
		return tid;
	}
};

string vxworks_process_listing(bool with_env, bool with_threads, string filter) {
	ln_helper* h = ln_helper::get_ln_helper();

	return repr(h->get_processlist(filter));
}

void vxworks_set_prio(int tid, int prio, int affinity) {
	ln_helper* h = ln_helper::get_ln_helper();
	h->set_prio(tid, prio, affinity);
}

void vxworks_free_ln_helper() {
	ln_helper::free_ln_helper();
}

int vxworks_gettid() {
	try {
		ln_helper* h = ln_helper::get_ln_helper();
		return h->gettid();
	} 
	catch(...) {

	}
	return 0;
}

#endif
