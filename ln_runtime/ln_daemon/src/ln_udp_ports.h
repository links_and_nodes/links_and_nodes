#ifndef LN_UDP_PORTS_H
#define LN_UDP_PORTS_H

#include "endianess_swapper.h"

class ln_udp_receiver : public ln_port, public ln_source_port {
	int sfd;
	int udp_port;
	unsigned int size;
	void* packet;
	endianess_swapper swap_endianess;
	
protected:
	int generate_packet(void* data, double timeout=-1);

public:
	ln_udp_receiver(ln_instance* instance, unsigned int size, thread_settings* ts=NULL, string swap_endianess="");
	~ln_udp_receiver();


#include "port_virtuals.h"
#include "src_port_virtuals.h"

	string get_id() {
		return format_string("(%s, %d)", repr(port_type).c_str(), udp_port);
	}
};

class ln_udp_sender : public ln_port, public ln_sink_port {
	int sfd;
	string target_host;
	int target_port;
	endianess_swapper swap_endianess;
public:
	ln_udp_sender(ln_instance* instance, string target_host, int target_port, int size, float rate, string swap_endianess="");
	~ln_udp_sender();

#include "port_virtuals.h"
#include "dst_port_virtuals.h"

	string get_id() {
		return format_string("(%s, %s, %d)", repr(port_type).c_str(), repr(target_host).c_str(), target_port);
	}
	void consume_packet(double ts, uint32_t counter, void* data);
};

#endif // LN_UDP_PORTS_H
