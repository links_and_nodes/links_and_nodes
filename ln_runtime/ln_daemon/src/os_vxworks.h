#ifndef OS_VXWORKS_H
#define OS_VXWORKS_H


#include "os_posix_like.h"
#include <vxWorks.h>
#include <version.h>
#include <sockLib.h>
#include <hostLib.h>

#if (_WRS_VXWORKS_MAJOR < 7)
#  include <ioLib.h>
#  include <net/uio.h> // struct iovec
#else
#  include <sys/uio.h> // struct iovec
#endif

#include <stdlib.h>
#include <rtpLib.h>
#include <taskLibCommon.h>
#include <selectLib.h>
#include <rtpLib.h>
#include <inetLib.h>

#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>

#define OS_SHM_DIR "/tmp"

#define bzero(buffer, nbytes) memset(buffer, 0, nbytes)
#define WIFCONTINUED(a) (0)

namespace os {

typedef int socklen_t_type;

typedef pthread_t thread_handle_t;
typedef void* thread_return_t;
typedef thread_return_t (*thread_func_type)(void*);

int create_thread(thread_handle_t* thread_handle, thread_func_type thread, void* data);
int cancel_thread(thread_handle_t thread_handle);
int join_thread(thread_handle_t thread_handle);
void test_cancel(thread_handle_t thread_handle);

void sleep(double s);


//! kill process with given pid
/*!
  \param pid process pid to kill
  \return success or error code
  */
inline int kill(pid_t pid) {
	return ::kill(abs(pid), 9); /* sending to process groups is not allowed */
}

//! send signal to process with given pid
/*!
  \param pid process pid to signal
  \parma signo signal number to send
  \return success or error code
  */
inline int send_signal(pid_t pid, int signo) {
	return ::kill(abs(pid), signo); /* sending to process groups is not allowed */
}

//! initialize os dependant pipe device
/*!
  \param reference to pipe device structure
  \return success or error code
  */
int pipe_init(ln_pipe_t& pipe);

//! closes os dependant pipe device
/*!
  \param reference to pipe device structure
  \return success or error code
  */
int pipe_close(ln_pipe_t& pipe);

//! return temp dir as std::string
/*
  \return temp dir 
 */
inline std::string get_temp_dir() {
	return std::string(OS_SHM_DIR);
}
int set_prio(pid_t pid, int tid, int prio, int policy);
int set_affinity(pid_t pid, int tid, int affinity);
int set_affinity_of_current_thread(int affinity);

STATUS vxworks_inet_aton(const char*, in_addr*);
#define inet_aton(a, b) vxworks_inet_aton(a, b)

int gettid();

#define random() rand()
#define srandom(a) srand(a)

}

#include "ln_helper.h"

#endif // OS_VXWORKS_H

