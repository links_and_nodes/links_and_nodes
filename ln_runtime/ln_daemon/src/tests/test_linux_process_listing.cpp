#include <stdio.h>
#include <string.h>

#include "linux_process_listing.h"

#ifdef OLD_LIBPROCPS
std::string _linux_process_listing(bool with_env, bool with_threads, std::string filter_string);
#endif

int main(int argc, char* argv[]) {
	bool with_env = false;
	bool with_threads = false;
	bool do_print = false;
	bool do_loop = false;
	bool do_fork = false;
	unsigned int n = 300000;
	std::string filter = "";

#ifndef OLD_LIBPROCPS
	do_fork = true;
#endif

	for(int i = 1; i < argc; i++) {
		if(!strcmp(argv[i], "-t"))
			with_threads = true;
		else if (!strcmp(argv[i], "-e"))
			with_env = true;
		else if (!strcmp(argv[i], "-p"))
			do_print = true;
		else if (!strcmp(argv[i], "-l"))
			do_loop = true;
#ifdef OLD_LIBPROCPS
		else if (!strcmp(argv[i], "-f"))
			do_fork = true;
#endif
		else if (!strcmp(argv[i], "--filter")) {
			filter = argv[i + 1];
			i++;
		}
	}

	unsigned int i = 0;
	while(n--) {
		i++;
		printf("run %d\n", i);

		std::string ret;
		if(do_fork)
			ret = linux_process_listing(with_env, with_threads, filter);
#ifdef OLD_LIBPROCPS
		else
			ret = _linux_process_listing(with_env, with_threads, filter);
#endif
		printf("process listing: %d bytes\n", (unsigned int)ret.size());
		if(do_print) {
			printf("%s\n", ret.c_str());
		}
		
		if(!do_loop)
			break;
	}
	return 0;
}
