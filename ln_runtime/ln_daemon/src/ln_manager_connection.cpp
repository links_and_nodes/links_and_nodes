/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include "os.h"

#include <stdio.h>
#include <time.h>

#include <sys/types.h>
#include <stdlib.h>

#include <ln/ln.h>

#ifdef __VXWORKS__
#include <sockLib.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>

#include <sstream>

#include <string_util/string_util.h>
#include "ln_manager_connection.h"
#include "logger.h"
#include "ln_ports.h"
#include "ln_state.h"

//#define PROTOCOL_VERSION 1
//#define PROTOCOL_VERSION 2
//#define PROTOCOL_VERSION 3 // needs priority and policy for process (policy can be -1, priority can be 0)
//#define PROTOCOL_VERSION 4 // max_output_frequency
//#define PROTOCOL_VERSION 5 // shorter port-list
// #define PROTOCOL_VERSION 6 // tcp_forwards
// #define PROTOCOL_VERSION 7 // udp send's with ts AND counter
// #define PROTOCOL_VERSION 8 // svn info
// #define PROTOCOL_VERSION 9 // request id's
// #define PROTOCOL_VERSION 10 // n_elements in shm-ports
// #define PROTOCOL_VERSION 11 // tcp_forwards listen_port -2 to search free xforwarding port
// #define PROTOCOL_VERSION 12 // change_user_to in start_process
// #define PROTOCOL_VERSION 13 // stack_size in start_process
// #define PROTOCOL_VERSION 14 // resource_limits in start_process
// #define PROTOCOL_VERSION 15 // tcp ports
// #define PROTOCOL_VERSION 16 // alternate_sockets
// #define PROTOCOL_VERSION 17 // set_state_property
// #define PROTOCOL_VERSION 18 // same as 17 but listen_port not in (-1, -2) works
// #define PROTOCOL_VERSION 19 // log divisor
// #define PROTOCOL_VERSION 20 // daemon is able to swap endianess
// #define PROTOCOL_VERSION 21 // daemon can be locked via register response from manager
// #define PROTOCOL_VERSION 22 // delete tcp forwards
// #define PROTOCOL_VERSION 23 // fixed endianess typo
// #define PROTOCOL_VERSION 24 // enable/disable debug output, shm v2 support, free all instance resources
// #define PROTOCOL_VERSION 25 // support blocking output
// #define PROTOCOL_VERSION 26 // support tcp forwardings to unix sockets
// #define PROTOCOL_VERSION 27 // use_execvpe
// #define PROTOCOL_VERSION 28 // retrieve_log_token
// #define PROTOCOL_VERSION 29 // tcp forward send-source magic
// #define PROTOCOL_VERSION 30 // removed state auto_check_interval and min_check_interval
// #define PROTOCOL_VERSION 31 // send process output via b"" byte strings, drop L-postfix for process listing long's
//#define PROTOCOL_VERSION 32 // test for firewall: open_fw_test_ports & test_for_fw
// #define PROTOCOL_VERSION 33 // term_rows, term_cols with start_process
#define PROTOCOL_VERSION 34 // disconnect_because_of_new_manager & replaced_manager, locked_by

using namespace std;
using namespace string_util;

string generate_random_message(unsigned int len, uint8_t start, uint8_t end) {
	stringstream ss;
	srandom((unsigned int)time(NULL));
	unsigned int range = (unsigned int)end - (unsigned int)start + 1;
	for(unsigned int i = 0; i < len; i++)
		ss << (char)(start + (uint8_t)(random() % range));
	return ss.str();
}

ln_manager_connection::ln_manager_connection(ln_daemon* daemon, int fd, string manager_host) :
	daemon(daemon), manager_host(manager_host), la(this, &received_line) {
	
	data_len = 0;
	skip_empty_lines = 0;
	this->fd = fd;
	instance = NULL; // unknown instance for this connection!
	connected = true;

#ifndef __WIN32__
	if(fcntl(fd, F_SETFD, FD_CLOEXEC) < 0)
		log("ln_manager_connection: F_SETFD, FD_CLOEXEC failed: %s", strerror(errno));
#endif
	daemon->add_io_watch(fd, IO_WATCH_READ | IO_WATCH_EXCEPT, on_server_data, this, "manager connection");

	char hostname[128];
	gethostname(hostname, 128);
	request_t r;
	r["request"] = repr("register_daemon");
	r["pid"] = format_string("%d", getpid());
	r["hostname"] = repr(hostname);
	r["architecture"] = repr(daemon->arch);
	r["protocol_version"] = format_string("%d", PROTOCOL_VERSION);
	r["library_version"] = format_string("%d", LN_LIBRARY_VERSION);
	r["svn_revision"] = format_string("%d", 0);
	r["is_modified"] = format_string("%d", 0);
	r["float_value"] = format_string("%f", 3.1415);

	unsigned int is_with_sys_futex = 0;
	unsigned int is_glibc_atleast_2_25 = 0;
#if LN_LIBRARY_VERSION >= 16
	ln_get_v16_features(&is_with_sys_futex, &is_glibc_atleast_2_25);
#endif
	r["have_sys_futex"] = format_string("%d", is_with_sys_futex);
	r["have_glibc_atleast_2_25"] = format_string("%d", is_glibc_atleast_2_25);

	char* cp = getenv("USER");
	if(cp)
		r["USER_env"] = repr(cp);
	else
		r["USER_env"] = "None";
	cp = getenv("HOME");
	if(cp)
		r["HOME_env"] = repr(cp);
	else
		r["HOME_env"] = "None";
	cp = getenv("HOME");
	if(cp)
		r["HOME_env"] = repr(cp);
	else
		r["HOME_env"] = "None";

#ifdef HAVE_GETPWUID
	uid_t uid = geteuid();
	r["uid"] = format_string("%d", uid);
	r["gid"] = format_string("%d", getegid());
	uid_t NO_UID = -1;
	struct passwd* pw = (uid != NO_UID) ? getpwuid(uid) : NULL;
	if(pw) {
		r["USER"] = repr(pw->pw_name);
#if defined(__ANDROID__) //on android pw_gecos is missing
		r["user_real_name"] = repr("android");
#else
		r["user_real_name"] = std::string("b") + repr(pw->pw_gecos);
#endif
		r["HOME"] = repr(pw->pw_dir);
		r["SHELL"] = repr(pw->pw_shell);
	} else {
		r["USER"] = "None";
		r["user_real_name"] = "None";
		r["HOME"] = "None";
		r["SHELL"] = "None";
	}
#else
	r["uid"] = format_string("%d", -1);
	r["gid"] = format_string("%d", -1);
	r["USER"] = "None";
	r["real_name"] = "None";
	r["HOME"] = "None";
	r["SHELL"] = "None";
#endif	
	
	uint16_t test = 0x1234;
	if(((uint8_t*)&test)[0] == 0x34)
		r["endianess"] = repr("little");
	else
		r["endianess"] = repr("big");

	// when locked via public key -> send random message to sign by private key!
#ifdef NO_DAEMON_AUTH
	challange_finished = true;
#else
	if(daemon->public_key.size() && daemon->public_key != "never") {
		key_challange = generate_random_message(256);
		r["key_challange"] = std::string("b") + repr(key_challange);
		r["locked_by"] = repr(daemon->locked_by);
		challange_finished = false;
	} else
		challange_finished = true;
#endif

#ifdef __LINUX__
	r["is_linux"] = "True";
#else
	r["is_linux"] = "False";
#endif
#ifdef __ANDROID__
	r["is_android"] = "True";
#else
	r["is_android"] = "False";
#endif
#ifdef __QNX__
	r["is_qnx"] = "True";
#else
	r["is_qnx"] = "False";
#endif
#ifdef __VXWORKS__
	r["is_vxworks"] = "True";
#else
	r["is_vxworks"] = "False";
#endif
#ifdef __WIN32__
	r["is_windows"] = "True";
#else
	r["is_windows"] = "False";
#endif
	
	send(r);
}


ln_manager_connection::~ln_manager_connection() {
	if (instance && instance->connection == this)
		instance->connection = NULL;

	// log("ln_manager detor!");
	if(fd != -1)
		_ln_close_socket(fd);
}

/*
int ln_manager_connection::connect(string ln_manager) {
	string::size_type p = ln_manager.find(":");
	if(p == string::npos)
		throw str_exception_tb("invalid ln_manager specification: '%s'\n", ln_manager.c_str());
	manager_host = ln_manager.substr(0, p);
	manager_port = atoi(ln_manager.substr(p+1).c_str());
	resolve_hostname(manager_host, &manager_address);
	manager_address.sin_port = htons(manager_port);

	if(::connect(fd, (struct sockaddr*)&manager_address, sizeof(manager_address)))
		throw errno_exception_tb("could not connect to ln_manager %s", repr(ln_manager).c_str());
	connected = true;

	daemon->add_io_watch(fd, IO_WATCH_READ | IO_WATCH_EXCEPT, on_server_data, this, "manager connection");

	char hostname[128];
	gethostname(hostname, 128);
	request_t r;
	r["request"] = repr("register_daemon");
	r["pid"] = format_string("%d", getpid());
	r["hostname"] = repr(hostname);
	send(r);
	return 0;
}
*/

void ln_manager_connection::disconnect() {
	if(fd != -1) {
		_ln_close_socket(fd);
		fd = -1;
	}
	connected = false;
}

void ln_manager_connection::disconnect_because_of_new_manager(ln_manager_connection* other_conn, std::string other)
{
	std::string old_manager = instance->manager;
	if (manager_protocol >= 34) {
		// notify manager of new manager connection
		request_t r;
		r["request"] = repr("disconnect_reason");
		r["reason"] = repr(
			format_string(
				"connection to this daemon will be closed as there is another manager connecting using the same instance name %s!\n"
				"other manager: %s",
				repr(instance_name).c_str(),
				other.c_str()));
		try {
			send(r);
		}
		catch(const std::exception& e) {
			log("got exception while sending disconnect_reason to manager: %s\n", e.what());
		}
	}
	daemon->remove_io_watch(fd);
	daemon->remove_manager_connection(this);
	disconnect();

	// notify new manager that this connection replaced an old one
	if (other_conn->manager_protocol >= 34) {
		// notify manager of new manager connection
		request_t r;
		r["request"] = repr("replaced_manager");
		r["other"] = repr(old_manager);
		try {
			other_conn->send(r);
		}
		catch(const std::exception& e) {
			log("got exception while sending replaced_manager to manager: %s\n", e.what());
		}
	}
}


void ln_manager_connection::on_server_data(int fd, int why, void* data) {
	ln_manager_connection* self = (ln_manager_connection*)data;

	if(why & IO_WATCH_EXCEPT) {
		log("exception on manager connection fd%d. closing...", fd);
		self->disconnect();
		self->daemon->remove_io_watch(fd);
		return;
	}
	if(!(why & IO_WATCH_READ))
		return;
	int ret = self->read();
	if(ret <= 0) {
		log("removing manager connection fd%d because read() returned %d", fd, ret);
		// self->disconnect();
		if(self->instance)
			self->instance->connection = NULL;
		self->daemon->remove_io_watch(fd);
		self->daemon->remove_manager_connection(self);
	}
}

int ln_manager_connection::read(int max_bytes) {
	int n;
	while(true) {
		if(max_bytes != -1)
			n = recv(fd, receive_buffer, max_bytes, 0);
		else
			n = recv(fd, receive_buffer, LN_MANAGER_RECEIVE_BUFFER, 0);
		if(n != -1)
			break;
		log("recv on ln_manager connected fd%d returned -1 with errno %d: %s",
		    fd, errno, strerror(errno));
		if(errno != EINTR)
			return -1;
		log("assume interrupted by signal and trying again!\n");
	}
	
	if(n == 0) {
		log("ln_manager on fd%d terminated connection.", fd);
		disconnect();
		return 0;
	} else if(n > 0) {
		// process data
		la.write(receive_buffer, n);
	}
	
	return n;
}

void ln_manager_connection::write(string data) {
	if(fd == 0)
		log("have 0 fd as manager connection!\n");

	const char* src = data.c_str();
	unsigned int already_sent = 0;
	int n;
	while(already_sent < data.size()) {
		n = ::send(fd, src + already_sent, data.size() - already_sent, 0);
		if(n == -1) {
			if(errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR)
				continue;
			throw errno_exception_tb(
				"error sending %d bytes to ln_manager connection at fd %d. send returned %d",
				data.size(), fd, n);
		}
		already_sent += n;
	}
}

void ln_manager_connection::received_line(void* instance, string line) {
	ln_manager_connection* self = (ln_manager_connection*) instance;
	// log("received line: '%s'", repr(line).c_str());
	self->feed_line(line);
}

void ln_manager_connection::feed_line(string line) {
	// log("received line: %s\n", line.c_str());
	if(data_len) {
		// read further data
		line.append("\n");
		if(data_len >= line.size()) {
			data_value.append(line);
			data_len -= line.size();
			line = "";
		} else {
			data_value.append(line.substr(0, data_len));
			line = line.substr(data_len + 1); // skip appended \n
			data_len = 0;
		}
		if(data_len == 0) {
			received_request[data_key] = data_value;
			if(data_value[data_value.size() - 1] == '\n')
				skip_empty_lines = 1;
			data_value = "";
		}
		if(line.size())
			log("warning: got remaining line %s", repr(line).c_str());
		return;
	}
	if(line == "") {
		if(skip_empty_lines) {
			skip_empty_lines--;
			return;
		}
		// process and finish this request
		try {	
			_process_request(received_request);
		}
		catch(exception& e) {
			log("uncaught exception while processing request: %s", e.what());
		}
		received_request.clear();
		return;
	}
	string::size_type p = line.find(':');
	if(p == string::npos) {
		log("invalid request line received: %s", repr(line).c_str());
		return;
	}

	string key;
	string value;
	key.assign(line, 0, p);
	p ++;
	while(p < line.size() && line[p] == ' ') p++;
	value.assign(line, p, line.size() - p);
	if(value.substr(0, 5) == "data ") {
		// data block
		data_key = key;
		data_len = atoi(value.substr(5).c_str());
		data_value = "";
		return;
	}
	// printf("input value: %s\n", value.c_str());
	// printf("evaluated value: %s\n", value.c_str());
	received_request[key] = value;
}

string ln_manager_connection::_format_request(request_t& req, bool trailing_newline, bool pretty) {
	stringstream ss;
	// log("formatting request:");
	for(request_t::iterator i = req.begin(); i != req.end(); i++) {
		// log(" %s: %s", i->first.c_str(), i->second.c_str());
		//if(i->second.size() <= 128)
		if(pretty && i->second.substr(0, 5) == "data ") {
			string::size_type p = i->second.find_first_of("\n", 5);
			ss << i->first << ": " << i->second.substr(0, p) << "\n";
		} else if(pretty && i->first == "process_list")
			ss << i->first << ": [...]\n";
		else
			ss << i->first << ": " << i->second << "\n";
			//else
			//ss << i->first << ": data " << i->second.size() << "\n" << i->second << "\n";
	}
	if(trailing_newline)
		ss << "\n";
	return ss.str();
}

py_value* ln_manager_connection::_get_from_request(py_request_t& py_request, string name, string func, int line) {
	if(py_request.find(name) == py_request.end())
		throw str_exception("missing request argument %s in %s:%d %s()\n",
							repr(name).c_str(), __FILE__, line, func.c_str());
	return py_request[name];
}

#define rp(n) _get_from_request(py_request, n, __func__, __LINE__)
#define r(n) *rp(n)
#define rd(n, d) ((py_request.find(n) != py_request.end()) ? r(n) : (d)) // has problems with auto-cast to floats?
	

void set_alternate_socket(string alternate_socket) {
#if defined(__QNX__)
	if(alternate_socket.size()) {
		log("setting SOCK=%s\n", alternate_socket.c_str());
		setenv("SOCK", alternate_socket.c_str(), 1);
	} else {
		log("resetting SOCK\n");
		unsetenv("SOCK");
	}
#endif
}

void ln_manager_connection::_process_request(request_t& req) {
	bool hide = false;
	if(req.find("request") != req.end()) {
		hide |= req["request"] == "'get_state'";
		hide |= req["request"] == "'get_last_packet'";
		hide |= req["request"] == "'retrieve_process_list'";
		hide |= req["request"] == "'send_stdin'";
	}
	if(daemon->no_hide)
		hide = false;

	if(!hide) log("received request:\n"); // , _format_request(req).c_str()
	py_request_t py_request;
	for(request_t::iterator i = req.begin(); i != req.end(); i++) {
		py_value* pv = eval_full(i->second);
		py_request[i->first] = pv;
		if(!hide) log("  %s: %s\n", i->first.c_str(), repr(*pv).c_str());
	}
	if(config->slow_mode) {
		log("slow mode...");
		os::sleep(2);
	}
	request_t answer;

	if(req.find("response") != req.end()) {
		string method = eval(req["response"]);
		if(method == "register_daemon") {
			if(string(r("success")) != "success")
				throw str_exception("daemon-registration at manager failed:\n%s", string(r("success")).c_str());							
			instance_name = string(r("instance_name"));
			instance = daemon->get_instance(instance_name);

			if(daemon->public_key.size() && daemon->public_key != "never") {
				// expect challange answer for key_challange
				string empty;
				string signature = string(rd("signature", empty));
				bool valid;
				try {
					valid = ln::dsa_dss1_verify_message_signature_memkey(daemon->public_key, key_challange, signature);
				}
				catch (std::exception& e) {
					log("dss1_verify_message failed: %s", e.what());
					valid = false;
				}
				if(valid) {
					challange_finished = true;
					log("manager successfully signed our challange!\n");
				} else {
					challange_finished = false;
					log("manager failed to correctly sign our challange! -- denying any further access!\n");
					return;
				}
			}
			manager_protocol = rd("manager_protocol", 23);

		        std::string this_manager = "";
			string manager_port = r("manager_port");
			if(manager_port != "None")
				this_manager = format_string("%s:%d", rd("manager_ip", manager_host).c_str(), (int)r("manager_port"));

			tried_to_lock_never_lock_daemon = false;
			string lock_with_pub_key = rd("lock_with_pub_key", string(""));
			if (lock_with_pub_key == "never" && daemon->public_key != "never") {
				log("manager requests to never lock this daemon!");
				daemon->set_public_key(lock_with_pub_key, rd("locked_by", string("manager ") + this_manager));
			} else if (lock_with_pub_key.size() && lock_with_pub_key != "never") {
				if (daemon->public_key == "never") {
					log("ERROR: manager requested to lock this daemon, but this daemon is set to never-lock!");
					tried_to_lock_never_lock_daemon = true;
					return;
				} else {
					log("manager requests to lock this daemon via provided public key!");
					daemon->set_public_key(lock_with_pub_key, rd("locked_by", string("manager ") + this_manager));
				}
			}

			if (instance->connection) {
				std::string old_manager = instance->manager;
				log("instance %s already had a connected manager %s!", repr(instance_name).c_str(), old_manager.c_str());
				instance->connection->disconnect_because_of_new_manager(this, this_manager);
			}
			instance->connection = this;
			instance->manager = this_manager;

			log("daemon successfully registered for instance %s on manager ip %s (using protocol %d)\n",
			    repr(instance_name).c_str(), manager_host.c_str(),
			    manager_protocol);
		}
		for(py_request_t::iterator i = py_request.begin(); i != py_request.end(); i++)
			delete i->second;
		return;
	}
	if(!instance) {
		log("error, no instance_name set for this manager_connection! no requests are allowed!\n");
		answer["success"] = repr("error: instance_name not specified!");
		send(answer);
		return;
	}

	if(req.find("request") == req.end()) {
		answer["success"] = repr("error: request not specified!");
		send(answer);
		return;
	}
	string method = eval(req["request"]);
	
	bool skip_answer = false;
	if(req.find("request_id") != req.end())
		answer["request_id"] = req["request_id"];
	answer["response"] = repr(method);
	answer["success"] = repr("success");

	if(!challange_finished) {
		log("error, key_challange not passed! this daemon instance is locked!\n");
		answer["success"] = repr(
			format_string("error: key_challange not passed! access to this daemon denied by %s!", daemon->locked_by.c_str()));
		send(answer);
		return;
	}

	if (daemon->public_key == "never" && tried_to_lock_never_lock_daemon) {
		// do not allow manager that wants to lock this daemon to continue
		answer["success"] = repr(
			format_string("error: you tried to lock this daemon, but it was set to never-lock by %s!", daemon->locked_by.c_str()));
		send(answer);
		return;
	}

	if(!instance->connection)
		instance->connection = this;

	try {
		// ports management
		if(method == "add_xauth") {
			stringstream cmd;
			cmd << "xauth -f '"
			    << string(r("XAUTHORITY")) << "' add " 
			    << string(r("DISPLAY")) << " '" 
			    << string(r("key_type")) << "' " 
			    << string(r("key_hex"));
			answer["retval"] = format_string("%d", system(cmd.str().c_str()));
			cmd.str("");
			cmd << "chmod go+r '" << string(r("XAUTHORITY")) << "'";
			printf("exec cmd: '%s'\n", cmd.str().c_str());
			system(cmd.str().c_str());
		} else if(method == "list_ports") {
			py_list port_list;
			instance->fill_ports_list(port_list, rd("with_last_packet", 0));
			answer["ports"] = repr(port_list);
		} else if(method == "list_processes") {
			py_list process_list;
			instance->fill_processes_list(process_list);
			answer["processes"] = repr(process_list);
		} else if(method == "get_svn_diff") {
			answer["architecture"] = repr(daemon->arch);
			answer["protocol_version"] = format_string("%d", PROTOCOL_VERSION);
			answer["library_version"] = format_string("%d", LN_LIBRARY_VERSION);
			answer["svn_revision"] = format_string("%d", 0);
			answer["is_modified"] = format_string("%d", 0);
			answer["svn_diff"] = repr("", 0, false);
			
		} else if(method == "new_tcp_forward") {
			py_string* target_string = dynamic_cast<py_string*>(rp("to_port"));
			ln_tcp_forward* forward;
			bool send_source = rd("send_source", false);
			if(target_string) { // unix/other target
				string other_target = *target_string;
				string listen_ip = r("listen_ip");
				forward = new ln_tcp_forward(
					instance, 
					other_target,
					listen_ip, r("listen_port"),
					r("keep_listening"),
					r("data"),
					send_source
					);
			} else { // tcp target port
				string target_ip = r("to_ip");
				unsigned int target_port = r("to_port");
				forward = new ln_tcp_forward(
					instance, 
					target_ip, target_port, 
					r("listen_ip"), r("listen_port"),
					r("keep_listening"),
					r("data"),
					send_source
					);
			}
			answer["listen_port"] = format_string("%d", forward->listen_port);			
		} else if(method == "delete_tcp_forward") {
			bool found = instance->delete_tcp_forward(r("data"));
			answer["found"] = format_string("%d", found ? 1 : 0);
		} else if(method == "list_tcp_forwards") {
			py_list forward_list;
			instance->fill_tcp_forwards_list(forward_list);
			answer["tcp_forwards"] = repr(forward_list);
		} else if(method == "is_tcp_forward_source_port") {
			answer["forwarded_from"] = instance->is_tcp_forward_source_port(r("port"));
			
		} else if(method == "new_shm_destination") {
			ln_port* port = new ln_shm_destination(
				instance, r("shm_name"), r("element_size"), r("n_elements"), r("rate"), rd("shm_version", 1));
			answer["port"] = port->get_id();
		} else if(method == "new_shm_source") {
			thread_settings ts(rd("prio", -1), rd("policy", -1), rd("cpu_mask", -1));
			ln_port* port = new ln_shm_source(
				instance, r("shm_name"), r("element_size"), r("n_elements"), &ts, rd("shm_version", 1));
			answer["port"] = port->get_id();
			
		} else if(method == "new_udp_receiver") {
			thread_settings ts(rd("prio", -1), rd("policy", -1), rd("cpu_mask", -1));
			set_alternate_socket(rd("alternate_socket", string("")));
			ln_port* port = new ln_udp_receiver(instance, r("size"), &ts, rd("swap_endianess", string("")));
			set_alternate_socket("");
			answer["port"] = port->get_id();
		} else if(method == "new_udp_sender") {
			set_alternate_socket(rd("alternate_socket", string("")));
			ln_port* port = new ln_udp_sender(
				instance, r("target_host"), r("target_port"), r("size"), (float)r("rate"), rd("swap_endianess", string("")));
			set_alternate_socket("");
			answer["port"] = port->get_id();
			
		} else if(method == "new_tcp_receiver") {
			thread_settings ts(rd("prio", -1), rd("policy", -1), rd("cpu_mask", -1));
			set_alternate_socket(rd("alternate_socket", string("")));
			ln_port* port = new ln_tcp_receiver(instance, r("size"), &ts, rd("swap_endianess", string("")));
			set_alternate_socket("");
			answer["port"] = port->get_id();
		} else if(method == "new_tcp_sender") {
			set_alternate_socket(rd("alternate_socket", string("")));
			ln_port* port = new ln_tcp_sender(
				instance, r("target_host"), r("target_port"), r("size"), (float)r("rate"), rd("swap_endianess", string("")));
			set_alternate_socket("");
			answer["port"] = port->get_id();
			
		} else if(method == "delete_port") {
			instance->delete_port(r("port"));
		} else if(method == "connect") {
			instance->connect_ports(r("source"), r("target"));
		} else if(method == "disconnect") {
			instance->disconnect_ports(r("source"), r("target"));
		} else if(method == "get_state") {
			ln_port* port = instance->get_port(r("port"));
			port->get_state(answer);
		} else if(method == "set_rate") {
			ln_port* port = instance->get_port(r("port"));
			port->set_rate(r("rate"));
		} else if(method == "get_last_packet") {
			ln_port* port = instance->get_port(r("port"));
			port->get_last_packet_req(answer);

			// logging
		} else if(method == "start_logging") {
			ln_port* port = instance->get_port(r("port"));
			port->start_logging(r("max_samples"), r("max_time"), r("only_ts"), rd("divisor", 1));
		} else if(method == "stop_logging") {
			ln_port* port = instance->get_port(r("port"));
			port->stop_logging();
		} else if(method == "retrieve_log") {
			ln_port* port = instance->get_port(r("port"));
			port->retrieve_log(answer);
		} else if(method == "retrieve_log_token") {
			ln_port* port = instance->get_port(r("port"));
			std::string data;
			port->retrieve_log_data(answer, data);
			instance->register_log_token(answer, data);
		} else if(method == "remove_log_tokens") {
			py_list* tokens = dynamic_cast<py_list*>(py_request["tokens"]);
			if(!tokens)
				throw str_exception("there is no tokens-list in remove_log_tokens-request!");
			instance->remove_log_tokens(tokens);
			
			// process management
		} else if(method == "request_state") {
			answer["name"] = repr(r("name")); // the c-tor could throw an exception...
				
			ln_state* state = instance->get_state(
				r("name"), 
				r("check"), 
				r("up"), 
				r("down"), 
				r("os_state")
				);
			// pass our pointer so that we get a notification if we already are in this state!
			answer["current_state"] = repr(state->request_state(this, r("up_down"))); 

		} else if(method == "start_process") {
			py_list empty_env;
			py_list* envlist = dynamic_cast<py_list*>(&(rd("environment", empty_env)));
			answer["name"] = repr(r("name")); // the c-tor could throw an exception...

			py_list* resource_limits = NULL;
			py_request_t::iterator rl = py_request.find("resource_limits");
			if(rl != py_request.end())
				resource_limits = dynamic_cast<py_list*>(rl->second);
			
			ln_process::config_t config = { 
				r("name"),
				r("cmdline"), 
				r("start_in_shell"), 
				r("no_pty"), 
				r("log_output"), 
				rd("buffer_size",(unsigned int)1024*100), 
				r("change_directory"),
				r("term_timeout"),
				r("priority"), 
				r("policy"),
				rd("affinity", -1),
				r("max_output_frequency"),
				false,
				rd("change_user_to", string("")),
				rd("stack_size", 0x4000),
				resource_limits,
				rd("blocking_output", false),
				rd("use_execvpe", false),
				rd("term_rows", (unsigned int)0),
				rd("term_cols", (unsigned int)0)
			};

			ln_process* process = new ln_process(
				instance, 
				envlist, 
				config
				);


			answer["pid"] = format_string("%d", process->get_pid());
			answer["start_time"] = format_string("%f", process->get_start_time());
		} else if(method == "set_process_ready") {
			ln_process* process = instance->get_process((int)r("pid"));
			process->set_ready();
			answer["pid"] = string(r("pid"));

		} else if(method == "set_process_property") {
			ln_process* process = instance->get_process((int)r("pid"));
			process->set_property(r("name"), rp("value"));
			answer["pid"] = string(r("pid"));

		} else if(method == "set_state_property") {
			ln_state* state = instance->get_state(r("name"));
			if(!state)
				throw str_exception("there is no state with name %s", repr(r("name")).c_str());
			state->set_property(r("name"), rp("value"));

		} else if(method == "set_log_output") {
			ln_process* process = instance->get_process((int)r("pid"));
			process->set_log_output(r("log_output"), rd("buffer_size", 0));
		} else if(method == "send_signal") {
			pid_t pid = (int)r("pid");
			if(pid != 0)
				os::send_signal(pid, r("signo"));
/*
		} else if(method == "setrlimit") {
			int resource = r("resource");
			int soft_limit = r("soft_limit");
			int hard_limit = r("hard_limit");
			struct rlimit limit = { soft_limit, hard_limit };
			int ret = setrlimit(resource, &limit);
			if(ret)
				answer["success"] = strerror(errno);
*/
		} else if(method == "set_prio") {
			pid_t pid = (int)r("pid");
			answer["pid"] = string(r("pid"));
			int tid = (int)r("tid");
			answer["tid"] = string(r("tid"));
			int ret = os::set_prio(pid, tid, r("prio"), r("policy"));
			if(ret)
				throw errno_exception("setting prio %d, policy %d", (int)r("prio"), (int)r("policy"));
		} else if(method == "send_stdin") {
			ln_process* process = instance->get_process((int)r("pid"));
			process->send_stdin(r("text"));
			skip_answer = true;
		} else if(method == "send_winch") {
			ln_process* process = instance->get_process((int)r("pid"));
			process->send_winch(r("rows"), r("cols"));
			skip_answer = true;
		} else if(method == "set_affinity") {
			pid_t pid = (int)r("pid");
			answer["pid"] = string(r("pid"));
			int tid = (int)r("tid");
			answer["tid"] = string(r("tid"));
			int ret = os::set_affinity(pid, tid, r("affinity"));
			if(ret)
				throw errno_exception("setting affinity 0x%02x", (int)r("affinity"));
		} else if(method == "stop_process") {
			// todo: get asynchronous!
			ln_process* process = NULL;
			try {
				process = instance->get_process((int)r("pid"));
			}
			catch(exception&) {
				answer["info"] = repr("unknown process - killing anyway");
			}
			if(process) {
				// process->stop();
				instance->stop_process(process, r("term_signal"));
			} else {
				os::kill(-1 * (int)r("pid"));
			}
			answer["pid"] = string(r("pid"));
			daemon->reset_last_sigchild_check();

			// general requests
		} else if(method == "quit") {
			daemon->remove_instance(instance_name);
			instance_name = "";
			instance = NULL;

		} else if(method == "quit_daemon") {
			daemon->stop();

		} else if(method == "retrieve_process_list") {
			bool with_env = r("show_env");
			bool with_threads = r("show_threads");
			string empty_filer = "";
			string filter = rd("filter_string", empty_filer);

#if defined(__LINUX__) && !defined(__ANDROID__)
			answer["process_list"] = linux_process_listing(with_env, with_threads, filter);
#else
#if defined(__VXWORKS__)
			answer["process_list"] = vxworks_process_listing(with_env, with_threads, filter);
#else			
			py_list* process_list = ln_process::get_process_list(with_env, with_threads, filter);
			answer["process_list"] = repr(process_list);
			delete process_list;
#endif
#endif
		} else if(method == "set_debug") {
			debug_enabled = r("debug_enabled");

		} else if(method == "release_all_instance_resources") {
			instance->release_all_resources();

		} else if(method == "open_fw_test_ports") {
			unsigned int udp_port;
			unsigned int tcp_port;
			instance->open_fw_test_ports(udp_port, tcp_port);
			answer["udp_port"] = std::to_string(udp_port);
			answer["tcp_port"] = std::to_string(tcp_port);

		} else if(method == "test_for_fw") {
			std::string ip = r("ip");
			unsigned int udp_port = r("udp_port");
			unsigned int tcp_port = r("tcp_port");
			uint64_t remote_id = (unsigned int)r("remote_id");
			double timeout = rd("timeout", 5);
			instance->test_for_fw(ip, udp_port, tcp_port, remote_id, timeout);

		} else {
			answer["success"] = repr("error: unknown request!");
		}
	}
	catch(exception& e) {
		stringstream response;
		log("have exception:\n%s", e.what());
		response << "error:\n";
		response << e.what();
		answer["success"] = repr(response.str());
	}

	// free request
	for(py_request_t::iterator i = py_request.begin(); i != py_request.end(); i++)
		delete i->second;
	if(!skip_answer) {
		// send answer
		send(answer);
	}
}

void ln_manager_connection::send(request_t& req) {
	bool show = true;
	bool have_method = false;
	bool have_response = false;
	bool have_request = false;
	
	request_t::iterator i = req.find("method");
	if(i != req.end()) {
		have_method = (i->second != "");
		if (i->second == "logs")
			show = false;
	}
	i = req.find("request");
	if(i != req.end()) {
		have_request = (i->second != "");
		if(i->second == "'process_output'")
			show = false;
	}
	
	i = req.find("response");
	if(i != req.end()) {
		have_response = (i->second != "");
		if(
		   i->second == "'process_output'"
		     || req["response"] == "'get_last_packet'" 
		     || req["response"] == "'get_state'" 
		     || req["response"] == "'retrieve_process_list'" 
		     || req["response"] == "'retrieve_log'"
			)
			show = false;
	}
		
	if(show || daemon->no_hide) {
		string req_str_p = _format_request(req, true, true);
		string indented = string_replace(req_str_p, "\n", "\n   ");
		log("sending:\n   %s", indented.c_str());
	}
	
	if(!have_method && !have_response && !have_request)
		log("WARNING: have to send request with empty method/response/request field!\n");
	string req_str = _format_request(req, true);
	write(req_str);
}

