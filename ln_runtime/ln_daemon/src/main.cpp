/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include "os.h"
#include <locale.h>

#include "telnet_connection.h"
#include "logger.h"
#include "ln_daemon.h"

#include "char_ringbuffer.h"

int main(int argc, char* argv[]) {
	setlocale(LC_ALL, "C");
	
	logger my_logger;
	root_logger = &my_logger;
	
	try {
		ln_daemon daemon;
		return daemon.run(argc, argv);
	}
	catch(exception& e) {
		log("ln_daemon threw uncaught exception:\n%s", e.what());
	}
	
	return -1;
}
