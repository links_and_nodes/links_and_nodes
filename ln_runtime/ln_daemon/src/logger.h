#ifndef LOGGER_H
#define LOGGER_H

#include <list>
#include <string>
#include <stdarg.h>

#include "lock.h"
#include <string_util/string_util.h>

#include <string>

#include <sys/types.h>
#ifndef MSVC
#include <unistd.h>
#endif

using namespace std;
using namespace string_util;

class log_entry {
public:
	double timestamp;
	std::string msg;

	log_entry(std::string msg);

	void format_time(char* ts_buffer, int ts_buffer_len);
};

typedef std::list<log_entry> log_history_t;

class logger {
	flolock l;
	unsigned int max_size;
	log_history_t history;
	FILE* fp;
	char* vlog_buffer;
	unsigned int vlog_buffer_size;
  
public:
	bool write_to_stdout;

	logger(unsigned int max_size=1024, bool to_stdout=true);
	virtual ~logger();

	virtual void log(std::string msg);
	virtual void log(const char* format, ...);
	void vlog(const char* format, va_list ap);
	void _log(std::string msg);

	bool has_history() { return history.size(); }
	void flush_history(std::list<log_entry>& target);
  
	void open_logfile(string log_file);

};

class logging {
public:
	logger _logger;
	string _logging_id;

	logging(string id, bool to_stdout=true, unsigned int max_size=1024) : 
		_logger(max_size, to_stdout),
		_logging_id(id) {
		log("new logging object with id %s", id.c_str());
	}

	void log(const char* format, ...);
	void log(std::string msg);

	void append_py_log(py_list& all_logs);
};

extern logger* root_logger;

void log(const char* format, ...);

extern bool debug_enabled;
#ifdef WITHOUT_DEBUG
#define debug(...)
#else
#define debug(format, ...) if(debug_enabled) log("%s:%d: %s() " format, __FILE__, __LINE__, __func__, ##__VA_ARGS__)
#endif

#endif // LOGGER_H
