#ifndef LN_ARBITER_H
#define LN_ARBITER_H

#include <string>
#include <vector>
#include <list>

#include "line_assembler.h"

using namespace std;

#define LN_ARBITER_PORT 54376

class ln_arbiter_client {
	// arbiter connection

	line_assembler la;
	bool _have_line;
	string _last_line;

	unsigned int daemon_port;

	bool _connect_or_start_arbiter(bool start=true);
	static void _received_line(void* instance, string line);
	vector<string> _send_request(string msg);
	string _wait_for_line();

public:
	int fd;
	string program_name;
	int fake_uid;
	ln_arbiter_client();

	bool register_daemon(unsigned int daemon_port);
	list<string> list_daemons();
	void quit_arbiter();
	void unregister_from_arbiter();
	bool update_pid();
};


class ln_arbiter_connection;
class ln_arbiter {
	int fd;

	typedef list<ln_arbiter_connection*> connections_t;
	connections_t connections;

public:
	bool keep_running;

	ln_arbiter();

	void run(int arbiter_port=LN_ARBITER_PORT);
	unsigned int get_daemon_port_for_uid(unsigned int uid);
	unsigned int get_daemon_pid_for_uid(unsigned int uid);
	list<string> get_daemons_list();
	unsigned int query_daemon_port(unsigned int uid);
};


#endif // LN_ARBITER_H
