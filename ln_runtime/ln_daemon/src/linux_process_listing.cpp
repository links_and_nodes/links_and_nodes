/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#if defined(__LINUX__) && !defined(__ANDROID__)

#include <string.h>

#include "os.h"
#include <string_util/string_util.h>

#include <stdio.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <strings.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "filter.h"

#ifdef NO_LIBPROCPS
using namespace std;
using namespace string_util;

#warning building without libprocps!

string linux_process_listing(bool with_env, bool with_threads, string filter_string) {
	py_list* ret = new py_list();
	string strret = repr(ret);
	delete ret;
	return strret;
}

#else // LIBPROC2 || LIBPROCPS
#ifdef LIBPROC2
// libproc2

#include <pty.h>
#include <sched.h>
#include <termios.h>

#include <libproc2/pids.h>

using namespace std;
using namespace string_util;

bool have_process = false;
ln_pipe_t cmd_pipe;
ln_pipe_t resp_pipe;
pid_t process_pid;

string linux_process_listing(bool with_env, bool with_threads, string filter_string) {
	static double uptime = -1; // time of boot in real!
	// get uptime
	if(uptime == -1) {
		FILE* fp = fopen("/proc/uptime", "rb");
		char line[1024];
		fgets(line, 1024, fp);
		fclose(fp);
		char* first = strtok(line, " ");
		uptime = time(NULL) - atol(first);

	}

	struct pids_info* info = NULL;
	enum pids_item items[] = {
		PIDS_ID_TID,
		PIDS_ID_PPID,
		PIDS_STATE,
		PIDS_TICS_USER,
		PIDS_TICS_SYSTEM,
		PIDS_TICS_USER_C,
		PIDS_TICS_SYSTEM_C,
		PIDS_TIME_START,
		PIDS_WCHAN_NAME,
		PIDS_PRIORITY,
		PIDS_NICE,
		PIDS_MEM_VIRT_PGS,
		PIDS_MEM_RES_PGS,
		PIDS_MEM_SHR_PGS,
		PIDS_VM_SIZE,
		PIDS_VM_RSS_LOCKED,
		PIDS_VM_RSS,
		PIDS_VM_DATA,
		PIDS_VM_STACK,
		PIDS_VM_EXE,
		PIDS_VM_LIB,
		PIDS_PRIORITY_RT,
		PIDS_SCHED_CLASS,
		PIDS_FLAGS,
		PIDS_FLT_MIN,
		PIDS_FLT_MAJ,
		PIDS_CMD,
		PIDS_CMDLINE_V,
		PIDS_ID_EUSER,
		PIDS_ID_EGROUP,
		PIDS_ID_EUID,
		PIDS_ID_EGID,
		PIDS_ID_PGRP,
		PIDS_ID_SESSION,
		PIDS_NLWP,
		PIDS_ID_TGID,
		PIDS_TTY,
		PIDS_ID_TPGID,
		PIDS_EXIT_SIGNAL,
		PIDS_PROCESSOR,
		PIDS_ENVIRON_V
	};
	enum rel_items {
		EU_ID_TID,
		EU_ID_PPID,
		EU_STATE,
		EU_TICS_USER,
		EU_TICS_SYSTEM,
		EU_TICS_USER_C,
		EU_TICS_SYSTEM_C,
		EU_TIME_START,
		EU_WCHAN_NAME,
		EU_PRIORITY,
		EU_NICE,
		EU_MEM_VIRT_PGS,
		EU_MEM_RES_PGS,
		EU_MEM_SHR_PGS,
		EU_VM_SIZE,
		EU_VM_RSS_LOCKED,
		EU_VM_RSS,
		EU_VM_DATA,
		EU_VM_STACK,
		EU_VM_EXE,
		EU_VM_LIB,
		EU_PRIORITY_RT,
		EU_SCHED_CLASS,
		EU_FLAGS,
		EU_FLT_MIN,
		EU_FLT_MAJ,
		EU_CMD,
		EU_CMDLINE_V,
		EU_ID_EUSER,
		EU_ID_EGROUP,
		EU_ID_EUID,
		EU_ID_EGID,
		EU_ID_PGRP,
		EU_ID_SESSION,
		EU_NLWP,
		EU_ID_TGID,
		EU_TTY,
		EU_ID_TPGID,
		EU_EXIT_SIGNAL,
		EU_PROCESSOR,
		EU_ENVIRON_V
	};

	int err;
	err = procps_pids_new(&info, items, ARRAY_LENGTH(items) - (with_env ? 0 : 1));
	if (err < 0)
		throw errno_exception_tb(-err, "procps_pids_new failed!");

	enum pids_fetch_type which = with_threads ? PIDS_FETCH_THREADS_TOO : PIDS_FETCH_TASKS_ONLY;

	py_list* ret = new py_list();

	py_list* hptr = new py_list();
	py_list& h = *hptr;
	ret->value.push_back(hptr);
	bool have_header = false;

	py_list* t = new py_list();
	py_list& d = *t;
	ret->value.push_back(t);

	filter_t filter = parse_filter(filter_string);

	while(true) {
		struct pids_stack* stack = procps_pids_get(info, which);
		if(!stack)
			break;

#define get(t, n, type, eu) {						\
			d.value.push_back(				\
				new py_ ## t(PIDS_VAL(EU_ ## eu, type, stack, info))); \
			if(!have_header) {				\
				h.value.push_back(new py_string(string(#n))); \
			}						\
		}
#define get_char(n, eu) {						\
			char tmp = PIDS_VAL(EU_ ## eu, s_ch, stack, info); \
			d.value.push_back(new py_string(&tmp, 1));	\
			if(!have_header) {				\
				h.value.push_back(new py_string(string(#n))); \
			}						\
		}

		if(filter.size()) {
			bool ok = true;
			for(filter_t::iterator i = filter.begin(); i != filter.end(); ++i) {
				filter_item_t& item = *i;

				int tgid =     PIDS_VAL(EU_ID_TGID, s_int, stack, info);
				int tid =      PIDS_VAL(EU_ID_TID, s_int, stack, info);
				int ppid =     PIDS_VAL(EU_ID_PPID, s_int, stack, info);
				int priority = PIDS_VAL(EU_PRIORITY, s_int, stack, info);
				int nice =     PIDS_VAL(EU_NICE, s_int, stack, info);
				int rtprio =   PIDS_VAL(EU_PRIORITY_RT, s_int, stack, info);
				int sched =    PIDS_VAL(EU_SCHED_CLASS, s_int, stack, info);
				int pgrp =     PIDS_VAL(EU_ID_PGRP, s_int, stack, info);
				int session =  PIDS_VAL(EU_ID_SESSION, s_int, stack, info);

#define compare_int(n) if(item.op == "==" && item.key == #n) { int value = atoi(item.value.c_str()); if(n != value) { ok = false; break; } }
				compare_int(tgid);
				compare_int(tid);
				compare_int(ppid);
				compare_int(priority);
				compare_int(nice);
				compare_int(rtprio);
				compare_int(sched);
				compare_int(pgrp);
				compare_int(session);
			}
			if(!ok)
				continue; // skip
		}


		get(int, tid, s_int, ID_TID);
		get(int, ppid, s_int, ID_PPID);
		get_char(state, STATE);
		get(long, utime, ull_int, TICS_USER);
		get(long, stime, ull_int, TICS_SYSTEM);
		get(long, cutime, ull_int, TICS_USER_C);
		get(long, cstime, ull_int, TICS_SYSTEM_C);

		d.value.push_back(new py_float(uptime + PIDS_VAL(EU_TIME_START, real, stack, info)));
		if(!have_header)
			h.value.push_back(new py_string(string("start_time")));

		get(string, wchan, str, WCHAN_NAME);
		get(int, priority, s_int, PRIORITY);

		get(int, nice, s_int, NICE);
		get(long, size, ul_int, MEM_VIRT_PGS); // statm: size | total number of pages
		get(long, resident, ul_int, MEM_RES_PGS); // statm: resident | number of resident set non-swapped pages (4k)
		get(long, share, ul_int, MEM_SHR_PGS); // statm: shared | number of mmap'd pages

		// missing? get(long, dt); // dirty pages

		get(long, vm_size, ul_int, VM_SIZE); // status: VmSize | virtual memory in kb
		get(long, vm_lock, ul_int, VM_RSS_LOCKED); // status: VmLck | locked memory in kb
		get(long, vm_rss, ul_int, VM_RSS); // status: VmRSS | resident set size in kb
		get(long, vm_data, ul_int, VM_DATA); // status: VmData | data in kb
		get(long, vm_stack, ul_int, VM_STACK); // status: VmStk | stack size in kb
		get(long, vm_exe, ul_int, VM_EXE); // status: VmExe | executable size in kb
		get(long, vm_lib, ul_int, VM_LIB); // status: VmLib | library size in kb

		get(long, rtprio, s_int, PRIORITY_RT); // stat: rt_priority
		get(long, sched, s_int, SCHED_CLASS); // stat: policy
		get(long, flags, ul_int, FLAGS); // stat: flags | kernel flags
		get(long, min_flt, ul_int, FLT_MIN); // stat: min_flt | minor page faults
		get(long, maj_flt, ul_int, FLT_MAJ); // stat: maj_flt | minor page faults

		if(with_env) {
			py_list* el = new py_list();
			char** ep = PIDS_VAL(EU_ENVIRON_V, strv, stack, info);
			while(ep && *ep) {
				el->value.push_back(new py_string(*ep));
				ep++;
			}
			d.value.push_back(el);
			if(!have_header) {
				h.value.push_back(new py_string(string("environ")));
			}
		}

		py_list* el = new py_list();
		char** ep = PIDS_VAL(EU_CMDLINE_V, strv, stack, info);
		while(ep && *ep) {
			el->value.push_back(new py_string(*ep));
			ep++;
		}
		d.value.push_back(el);
		if(!have_header) {
			h.value.push_back(new py_string(string("cmdline")));
		}

		get(string, cmd, str, CMD);

		get(string, euser, str, ID_EUSER);
		//get(string, ruser, ID_RUSER);
		//get(string, suser, ID_SUSER);
		//get(string, fuser, ID_FUSER);

		get(string, egroup, str, ID_EGROUP);
		//get(string, rgroup, ID_RGROUP);
		//get(string, sgroup, ID_SGROUP);
		//get(string, fgroup, ID_FGROUP);

		get(int, euid, u_int, ID_EUID); // status: Uid
		//get(int, ruid, ID_RUID); // status: Uid | real
		//get(int, suid, ID_SUID); // status: Uid | saved
		//get(int, fuid, ID_FUID); // status: Uid | for file access

		get(int, egid, u_int, ID_EGID); // status: Gid
		//get(int, rgid, ID_RGID); // status: Gid | real
		//get(int, sgid, ID_SGID); // status: Gid | saved
		//get(int, fgid, ID_FGID); // status: Gid | for file access

		get(int, pgrp, s_int, ID_PGRP); // stat: pgrp
		get(int, session, s_int, ID_SESSION); // stat: sid
		get(int, nlwp, s_int, NLWP); // number of threads
		get(int, tgid, s_int, ID_TGID); // status: Tgid | task group ID -> POSIX PID
		get(int, tty, s_int, TTY); // stat: tty_nr

		get(int, tpgid, s_int, ID_TPGID); // stat: tty_pgrp
		get(int, exit_signal, s_int, EXIT_SIGNAL); // stat: exit_signal
		get(int, processor, s_int, PROCESSOR); // stat: task_cpu

		have_header = true;
	}
	procps_pids_unref(&info);

	string strret = repr(ret);
	delete ret;
	return strret;
}


#else // LIBPROCPS

#include <pty.h>
#include <proc/readproc.h>
#include <proc/wchan.h>
#include <sched.h>

#include <termios.h>

using namespace std;
using namespace string_util;

#ifndef OLD_LIBPROCPS
// newer libprocps
bool have_process = false;
ln_pipe_t cmd_pipe;
ln_pipe_t resp_pipe;
pid_t process_pid;

string linux_process_listing(bool with_env, bool with_threads, string filter_string) {
	py_list* ret = new py_list();
	static double uptime = -1; // time of boot in real!
	
	// get uptime
	if(uptime == -1) {
		FILE* fp = fopen("/proc/uptime", "rb");
		char line[1024];
		fgets(line, 1024, fp);
		fclose(fp);
		char* first = strtok(line, " ");
		uptime = time(NULL) - atol(first);

	}

	int flags = PROC_FILLMEM | PROC_FILLCOM	| PROC_FILLUSR | PROC_FILLGRP | PROC_FILLSTATUS | PROC_FILLSTAT | PROC_FILLARG | PROC_LOOSE_TASKS;
	if(with_env)
		flags |= PROC_FILLENV;

	PROCTAB* pt = openproc(flags);

	py_list* hptr = new py_list();
	py_list& h = *hptr;
	ret->value.push_back(hptr);
	bool have_header = false;

	py_list* t = new py_list();
	py_list& d = *t;
	ret->value.push_back(t);

	filter_t filter = parse_filter(filter_string);
	proc_t* taskp = NULL;
	proc_t* threadp = NULL;
	
	while(true) {
		if(!(taskp = readproc(pt, taskp)))
			break;
		proc_t* curptr = taskp;

#define get(t, n) { d.value.push_back(new py_ ## t(cur.n)); if(!have_header) { h.value.push_back(new py_string(string(#n))); } }
#define get_char(n) { d.value.push_back(new py_string(&cur.n, 1)); if(!have_header) { h.value.push_back(new py_string(string(#n))); } }

		while(true) {

			if(with_threads) {
				if(!(threadp = readtask(pt, taskp, threadp)))
					break; // no more threads...
				curptr = threadp;
			}
			proc_t& cur = *curptr;

			if(filter.size()) {
				bool ok = true;
				for(filter_t::iterator i = filter.begin(); i != filter.end(); ++i) {
					filter_item_t& item = *i;
#define compare_int(n) if(item.op == "==" && item.key == #n) { int value = atoi(item.value.c_str()); if(cur.n != value) { ok = false; break; } }
#define compare_long(n) if(item.op == "==" && item.key == #n) { long value = atol(item.value.c_str()); if(cur.n != value) { ok = false; break; } }
#define compare_ulong(n) if(item.op == "==" && item.key == #n) { unsigned long value = strtoul(item.value.c_str(), NULL, 0); if(cur.n != value) { ok = false; break; } }

					compare_int(tgid);
					compare_int(tid);
					compare_int(ppid);
					compare_long(priority);
					compare_long(nice);
					compare_ulong(rtprio);
					compare_ulong(sched);
					compare_int(pgrp);
					compare_int(session);
				}
				if(!ok)
					continue; // skip
			}
			get(int, tid);
			get(int, ppid);
			get_char(state);
			get(long, utime);
			get(long, stime);
			get(long, cutime);
			get(long, cstime);
			// d.value["start_time"] = new py_float(uptime + (double)cur.start_time / 100.);
			d.value.push_back(new py_float(uptime + (double)cur.start_time / 100.));
			if(!have_header) {
				h.value.push_back(new py_string(string("start_time")));
			}
			// get(long, start_time);

			//get(long, wchan);
			d.value.push_back(new py_string(lookup_wchan(cur.tid)));
			if(!have_header) {
				h.value.push_back(new py_string(string("wchan")));
			}

			get(long, priority);

			get(long, nice);
			get(long, size); // total number of pages
			get(long, resident); // number of resident set non-swapped pages (4k)
			get(long, share); // number of mmap'd pages
			get(long, dt); // dirty pages

			get(long, vm_size); // virtual memory in kb
			get(long, vm_lock); // locked memory in kb
			get(long, vm_rss); // resident set size in kb
			get(long, vm_data); // data in kb
			get(long, vm_stack); // stack size in kb
			get(long, vm_exe); // executable size in kb
			get(long, vm_lib); // library size in kb

			get(long, rtprio);
			get(long, sched);
			get(long, flags); // # kernel flags
			get(long, min_flt); // # minor page faults
			get(long, maj_flt); // # minor page faults

			if(with_env) {
				py_list* el = new py_list();
				char** ep = cur.environ;
				while(ep && *ep) {
					el->value.push_back(new py_string(*ep));
					ep++;
				}
				// d.value["environ"] = el;
				d.value.push_back(el);
				if(!have_header) {
					h.value.push_back(new py_string(string("environ")));
				}
			}

			if((flags & PROC_FILLCOM) || (flags & PROC_FILLARG)) {
				py_list* el = new py_list();
				char** ep = cur.cmdline;
				while(ep && *ep) {
					el->value.push_back(new py_string(*ep));
					ep++;
				}
				// d.value["cmdline"] = el;
				d.value.push_back(el);
				if(!have_header) {
					h.value.push_back(new py_string(string("cmdline")));
				}
			}

			get(string, euser);
			get(string, ruser);
			get(string, suser);
			get(string, fuser);
			get(string, rgroup);
			get(string, egroup);
			get(string, sgroup);
			get(string, fgroup);

			get(string, cmd);

			get(int, pgrp);
			get(int, session);
			get(int, nlwp); // number of threads
			get(int, tgid); // task group ID -> POSIX PID
			get(int, tty);

			get(int, euid); // efff.
			get(int, ruid); // real
			get(int, suid); // saved
			get(int, fuid); // for file access

			get(int, egid); // efff.
			get(int, rgid); // real
			get(int, sgid); // saved
			get(int, fgid); // for file access

			get(int, tpgid);
			get(int, exit_signal);
			get(int, processor);

			have_header = true;

			if(threadp) {
				freeproc(threadp);
				threadp = NULL;
			}
			
			if(!with_threads || taskp->nlwp <= 1)
				break; // no threads...
			// do_threads = true;
		}
		
		if(taskp) {
			freeproc(taskp);
			taskp = NULL;
		}
	}
	closeproc(pt);

	string strret = repr(ret);
	delete ret;
	return strret;
}

#else // older libprocps that tends to cause segfaults... thats why we fork in here...

#warning compiling workaround for older libprocps
bool have_process = false;
ln_pipe_t cmd_pipe;
ln_pipe_t resp_pipe;
pid_t process_pid;

string _linux_process_listing(bool with_env, bool with_threads, string filter_string) {
	py_list* ret = new py_list();
	static double uptime = -1; // time of boot in real!
	
	// get uptime
	if(uptime == -1) {
		FILE* fp = fopen("/proc/uptime", "rb");
		char line[1024];
		fgets(line, 1024, fp);
		fclose(fp);
		char* first = strtok(line, " ");
		uptime = time(NULL) - atol(first);

		// open kernel symbol table
		open_psdb(NULL);
	}

	int flags = PROC_FILLMEM | PROC_FILLCOM	| PROC_FILLUSR | PROC_FILLGRP | PROC_FILLSTATUS | PROC_FILLSTAT | PROC_FILLWCHAN | PROC_FILLARG | PROC_LOOSE_TASKS;
	if(with_env)
		flags |= PROC_FILLENV;

	PROCTAB* pt = openproc(flags);
	if(!pt)
		throw str_exception_tb("openproc %#x failed!", flags);

	py_list* hptr = new py_list();
	py_list& h = *hptr;
	ret->value.push_back(hptr);
	bool have_header = false;

	py_list* t = new py_list();
	py_list& d = *t;
	ret->value.push_back(t);

	filter_t filter = parse_filter(filter_string);
	
	proc_t task;
	proc_t thread;
	memset(&task, 0, sizeof(task));
	memset(&thread, 0, sizeof(thread));
	while(true) {
		if(!(readproc(pt, &task)))
			break;
		proc_t* curptr = &task;

#define get(t, n) { d.value.push_back(new py_ ## t(cur.n)); if(!have_header) { h.value.push_back(new py_string(string(#n))); } }
#define get_char(n) { d.value.push_back(new py_string(&cur.n, 1)); if(!have_header) { h.value.push_back(new py_string(string(#n))); } }

		while(true) {
			if(with_threads) {
				if(!(readtask(pt, &task, &thread)))
					break; // no more threads...
				curptr = &thread;
			}
			proc_t& cur = *curptr;

			if(filter.size()) {
				bool ok = true;
				for(filter_t::iterator i = filter.begin(); i != filter.end(); ++i) {
					filter_item_t& item = *i;
#define compare_int(n) if(item.op == "==" && item.key == #n) { int value = atoi(item.value.c_str()); if(cur.n != value) { ok = false; break; } }
#define compare_long(n) if(item.op == "==" && item.key == #n) { long value = atol(item.value.c_str()); if(cur.n != value) { ok = false; break; } }
#define compare_ulong(n) if(item.op == "==" && item.key == #n) { unsigned long value = strtoul(item.value.c_str(), NULL, 0); if(cur.n != value) { ok = false; break; } }

					compare_int(tgid);
					compare_int(tid);
					compare_int(ppid);
					compare_long(priority);
					compare_long(nice);
					compare_ulong(rtprio);
					compare_ulong(sched);
					compare_int(pgrp);
					compare_int(session);
				}
				if(!ok)
					continue; // skip
			}
			get(int, tid);
			get(int, ppid);
			get_char(state);
			get(long, utime);
			get(long, stime);
			get(long, cutime);
			get(long, cstime);
			// d.value["start_time"] = new py_float(uptime + (double)cur.start_time / 100.);
			d.value.push_back(new py_float(uptime + (double)cur.start_time / 100.));
			if(!have_header) {
				h.value.push_back(new py_string(string("start_time")));
			}
			// get(long, start_time);

			//get(long, wchan);
			d.value.push_back(new py_string(lookup_wchan(cur.wchan, cur.tid)));
			if(!have_header) {
				h.value.push_back(new py_string(string("wchan")));
			}

			get(long, priority);

			get(long, nice);
			get(long, size); // total number of pages
			get(long, resident); // number of resident set non-swapped pages (4k)
			get(long, share); // number of mmap'd pages
			get(long, dt); // dirty pages

			get(long, vm_size); // virtual memory in kb
			get(long, vm_lock); // locked memory in kb
			get(long, vm_rss); // resident set size in kb
			get(long, vm_data); // data in kb
			get(long, vm_stack); // stack size in kb
			get(long, vm_exe); // executable size in kb
			get(long, vm_lib); // library size in kb

			get(long, rtprio);
			get(long, sched);
			get(long, flags); // # kernel flags
			get(long, min_flt); // # minor page faults
			get(long, maj_flt); // # minor page faults

			if(with_env) {
				py_list* el = new py_list();
				char** ep = cur.environ;
				while(ep && *ep) {
					el->value.push_back(new py_string(*ep));
					ep++;
				}
				// d.value["environ"] = el;
				d.value.push_back(el);
				if(!have_header) {
					h.value.push_back(new py_string(string("environ")));
				}
			}

			if((flags & PROC_FILLCOM) || (flags & PROC_FILLARG)) {
				py_list* el = new py_list();
				char** ep = cur.cmdline;
				while(ep && *ep) {
					el->value.push_back(new py_string(*ep));
					ep++;
				}
				// d.value["cmdline"] = el;
				d.value.push_back(el);
				if(!have_header) {
					h.value.push_back(new py_string(string("cmdline")));
				}
			}

			get(string, euser);
			get(string, ruser);
			get(string, suser);
			get(string, fuser);
			get(string, rgroup);
			get(string, egroup);
			get(string, sgroup);
			get(string, fgroup);

			get(string, cmd);

			get(int, pgrp);
			get(int, session);
			get(int, nlwp); // number of threads
			get(int, tgid); // task group ID -> POSIX PID
			get(int, tty);

			get(int, euid); // efff.
			get(int, ruid); // real
			get(int, suid); // saved
			get(int, fuid); // for file access

			get(int, egid); // efff.
			get(int, rgid); // real
			get(int, sgid); // saved
			get(int, fgid); // for file access

			get(int, tpgid);
			get(int, exit_signal);
			get(int, processor);

			have_header = true;

			if(!with_threads || cur.nlwp <= 1)
				break; // no threads...
			// do_threads = true;
		}
	}
	closeproc(pt);

	string strret = repr(ret);
	delete ret;
	return strret;
}

string linux_process_listing(bool with_env, bool with_threads, string filter);

string retry(bool with_env, bool with_threads, string filter) {
	os::kill(process_pid);
	process_pid = 0;
	have_process = false;
	os::pipe_close(cmd_pipe);
	os::pipe_close(resp_pipe);
	printf("waiting before retry...\n");
	sleep(1);
	return linux_process_listing(with_env, with_threads, filter);
}

string linux_process_listing(bool with_env, bool with_threads, string filter) {
	if(!have_process) {
		signal(SIGPIPE, SIG_IGN);
		os::pipe_init(cmd_pipe);
		os::pipe_init(resp_pipe);
		process_pid = fork();

		if(process_pid == 0) {
			signal(SIGPIPE, SIG_IGN);
			signal(SIGTERM, SIG_DFL);
			signal(SIGINT, SIG_DFL);
			// new child process
			// close all file descriptors
			int table_size = getdtablesize();
			for(int i = 3; i < table_size; i++) {
				if(i != resp_pipe.output_fd && i != cmd_pipe.input_fd)
					close(i);
			}

			while(true) {
				// wait for requests
				int ret;
				uint32_t cmd;
				// printf("process listing: waiting for command\n");
				ret = read(cmd_pipe.input_fd, &cmd, sizeof(cmd));
				if(ret != sizeof(cmd)) {
					if(errno == EINTR)
						continue;
					break;
				}
				// printf("process listing: got command: 0x%x\n", cmd);
				bool with_env = cmd & 1;
				bool with_threads = cmd & 2;
				bool with_filter = cmd & 4;
				string filter = "";
				if(with_filter) {
					uint32_t filter_len;
					ret = read(cmd_pipe.input_fd, &filter_len, sizeof(filter_len));
					if(ret != sizeof(filter_len)) {
						if(errno == EINTR)
							continue;
						break;
					}
					static char* filter_string = NULL;
					static uint32_t filter_string_len = 0;
					if(filter_string_len < filter_len) {
						filter_string_len = filter_len;
						filter_string = (char*)malloc(filter_len);
					}
					ret = read(cmd_pipe.input_fd, filter_string, filter_len);
					if((unsigned)ret != filter_len) {
						if(errno == EINTR)
							continue;
						break;
					}
					filter = filter_string;
				}
				string pl = _linux_process_listing(with_env, with_threads, filter);
				uint32_t retlen = pl.size();
				// printf("process listing: write response of %d bytes\n", retlen);
				write(resp_pipe.output_fd, &retlen, sizeof(retlen));
				write(resp_pipe.output_fd, pl.c_str(), retlen);
			}
			// printf("process listing: exiting...\n");
			exit(0);
		}
		close(cmd_pipe.input_fd);
		close(resp_pipe.output_fd);
		have_process = true;
	}
	// send command to process
	uint32_t cmd = 0;
	if(with_env)
		cmd |= 1;
	if(with_threads)
		cmd |= 2;
	if(filter.size())
		cmd |= 4;
	int ret;
	while(true) {
		ret = write(cmd_pipe.output_fd, &cmd, sizeof(cmd));
		if(ret == sizeof(cmd))
			break;
		if(errno == EINTR)
			continue;
		return retry(with_env, with_threads, filter);
	}
	if(filter.size()) {
		// send filter length and filter string
		uint32_t filter_len = filter.size();
		while(true) {
			ret = write(cmd_pipe.output_fd, &filter_len, sizeof(filter_len));
			if(ret == sizeof(filter_len))
				break;
			if(errno == EINTR)
				continue;
			return retry(with_env, with_threads, filter);
		}
		while(true) {
			ret = write(cmd_pipe.output_fd, filter.c_str(), filter_len);
			if((unsigned)ret == filter_len)
				break;
			if(errno == EINTR)
				continue;
			return retry(with_env, with_threads, filter);
		}
	}
	// receive response
	// printf("process listing: waiting for response!\n");
	uint32_t resp_len;
	while(true) {
		ret = read(resp_pipe.input_fd, &resp_len, sizeof(resp_len));
		if(ret == sizeof(resp_len))
			break;
		if(errno == EINTR)
			continue;
		// printf("process listing: retry!\n");
		return retry(with_env, with_threads, filter);
	}
	// printf("process listing: will read resp of %d bytes\n", resp_len);
	static char* resp = NULL;
	static uint32_t max_resp_len = 0;
	if(resp_len > max_resp_len) {
		void* new_mem = realloc(resp, resp_len);
		if(!new_mem) {
			printf("process listing: failed to realloc...\n");
			return "";
		}
		resp = (char*)new_mem;
		max_resp_len = resp_len;
	}
	while(true) {
		// printf("process listing: try to read remaining %d bytes...\n", resp_len);
		ret = read(resp_pipe.input_fd, resp, resp_len);
		if((unsigned)ret == resp_len)
			break;
		if(ret == -1 && errno == EINTR)
			continue;
		return retry(with_env, with_threads, filter);
	}
	// printf("process listing: done\n");
	return string(resp, resp_len);
}

#endif // OLD_LIBPROCPS
#endif // LIBPROCPS
#endif // LIBPROC2 || LIBPROCPS
#endif 
