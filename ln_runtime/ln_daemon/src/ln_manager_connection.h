#ifndef LN_MANAGER_CONNECTION_H
#define LN_MANAGER_CONNECTION_H

#include <sys/types.h>
#include <map>
#include <string>
#include <string_util/string_util.h>
#include "line_assembler.h"


class ln_manager_connection;
#include "ln_daemon.h"
#include "ln_instance.h"

using namespace std;
using namespace string_util;

#define LN_MANAGER_RECEIVE_BUFFER (1024*4)

typedef map<string, py_value*> py_request_t;

class ln_daemon;

std::string generate_random_message(unsigned int len, uint8_t start=0, uint8_t end=255);


class ln_manager_connection {
	ln_daemon* daemon;
	ln_instance* instance;
	string instance_name;
  
	int fd;
	string manager_host;
	bool connected;

	char receive_buffer[LN_MANAGER_RECEIVE_BUFFER];
	line_assembler la;
	string data_key;
	unsigned int data_len;
	string data_value;
	int skip_empty_lines;

	request_t received_request;
	string key_challange;
	bool challange_finished;

	unsigned int manager_protocol;

	bool tried_to_lock_never_lock_daemon;
	
	friend class ln_state;
	friend class ln_instance;

	bool use_alternate_socket(request_t& req, request_t& answer);
public:
	ln_manager_connection(ln_daemon* daemon, int fd, string manager);
	~ln_manager_connection();
  
	// int connect(string manager);
	void disconnect();
	void disconnect_because_of_new_manager(ln_manager_connection* other_conn, std::string other);
	bool is_connected() { return connected; }

	static void on_server_data(int fd, int why, void* data);
	int read(int max_bytes=-1);
	void write(string data);

	static void received_line(void* instance, string line);
	void feed_line(string line);
	void _process_request(request_t& req);
	void send(request_t& req);
	string _format_request(request_t& reg, bool trailing_newline=false, bool pretty=false);
	py_value* _get_from_request(py_request_t& py_request, string name, string func, int line);

	unsigned int get_protocol() { return manager_protocol; }
	
	friend class robot_node_server;
};

#endif // LN_MANAGER_CONNECTION_H

