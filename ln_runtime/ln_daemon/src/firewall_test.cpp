#include "os.h"
#include "ln_instance.h"

void ln_instance::open_fw_test_ports(unsigned int& udp_port, unsigned int& tcp_port)
{
	firewall_test.open();
	udp_port = firewall_test.udp_port;
	tcp_port = firewall_test.tcp_port;
}

ln_instance::FirewallTest::~FirewallTest()
{
	if(udp_fd != -1) {
		instance->daemon->remove_io_watch(udp_fd);
		::closesocket(udp_fd);
		udp_fd = -1;
	}
	if(tcp_fd != -1) {
		instance->daemon->remove_io_watch(tcp_fd);
		shutdown(tcp_fd, SHUT_RDWR);
		::closesocket(tcp_fd);
		tcp_fd = -1;
		for(auto cfd : tcp_client_fds) {
			shutdown(cfd, SHUT_RDWR);
			::closesocket(cfd);
			instance->daemon->remove_io_watch(cfd);
		}
	}
}

void ln_instance::FirewallTest::on_incoming_udp(int fd, int why, void* instance)
{
	auto self = (ln_instance::FirewallTest*)instance;
	uint64_t remote_id;
	int n = recv(fd, &remote_id, sizeof(remote_id), 0);
	if (n == sizeof(remote_id))
		self->ack_reception("udp", remote_id);
}

void ln_instance::FirewallTest::on_incoming_tcp(int fd, int why, void* instance)
{
	auto self = (ln_instance::FirewallTest*)instance;
	uint64_t remote_id;
	int n = recv(fd, &remote_id, sizeof(remote_id), 0);
	//log("fw-test got tcp connection data at fd %d: %d bytes\n", fd, n);
	if (n == sizeof(remote_id))
		self->ack_reception("tcp", remote_id);
	shutdown(fd, SHUT_RDWR);
	::closesocket(fd);
	self->tcp_client_fds.remove(fd);
	self->instance->daemon->remove_io_watch(fd);
}

void ln_instance::FirewallTest::on_incoming_tcp_connection(int fd, int why, void* instance)
{
	auto self = (ln_instance::FirewallTest*)instance;
	struct sockaddr_in client_address;
	os::socklen_t_type client_address_len = sizeof(client_address);
	int client_fd = accept(fd, (struct sockaddr*)&client_address, &client_address_len);
	if(client_fd == -1) {
		log("fw-test: error accepting incoming tcp connection! errno %d: %s\n", errno, strerror(errno));
		return;
	}
	std::string hint = "fw_test_tcp ";
	hint += std::string(inet_ntoa(client_address.sin_addr));
	hint += ":";
	hint += std::to_string(ntohs(client_address.sin_port));

#if defined(SO_RCVTIMEO)
	struct timeval timeout;
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;
	if (setsockopt(client_fd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0)
		log("fw-test: could not set SO_RCVTIMEOout! errno %d: %s\n", errno, strerror(errno));
#else
#  warning SO_RCVTIMEO is not defined. there will be no read-timeout for firewall-test incoming tcp connections!
#endif

	log("fw-test got incoming tcp connection %s at fd %d\n", hint.c_str(), client_fd);
	self->instance->daemon->add_io_watch(client_fd, IO_WATCH_READ, on_incoming_tcp, self, hint);
	self->tcp_client_fds.push_back(client_fd);
}

void ln_instance::FirewallTest::open()
{
	if(udp_fd == -1) {
		udp_fd = socket(AF_INET, SOCK_DGRAM, 0);
		if(udp_fd == -1)
			throw errno_exception_tb("could not create fw-test udp socket!");

		struct sockaddr_in local_addr;
		os::socklen_t_type len = sizeof(local_addr);
		memset(&local_addr, 0, sizeof(local_addr));
		local_addr.sin_family = AF_INET;
		if(bind(udp_fd, (struct sockaddr*)&local_addr, sizeof(local_addr)))
			throw errno_exception_tb("could not bind fw-test udp port!");

		if(getsockname(udp_fd, (struct sockaddr*)&local_addr, &len))
			throw errno_exception_tb("could not get fw-test udp socket name!");
		udp_port = ntohs(local_addr.sin_port);

		instance->daemon->add_io_watch(udp_fd, IO_WATCH_READ, on_incoming_udp, this, "fw_test_udp");
	}

	if(tcp_fd == -1) {
		// create listening socket
		tcp_fd = socket(AF_INET, SOCK_STREAM, 0);
		if(tcp_fd == -1)
			throw errno_exception_tb("could not create fw-test tcp socket!");

		struct sockaddr_in local_addr;
		os::socklen_t_type len = sizeof(local_addr);
		memset(&local_addr, 0, sizeof(local_addr));
		local_addr.sin_family = AF_INET;
		if(bind(tcp_fd, (struct sockaddr*)&local_addr, sizeof(local_addr)))
			throw errno_exception_tb("could not bind fw-test tcp port!");

		if(getsockname(tcp_fd, (struct sockaddr*)&local_addr, &len))
			throw errno_exception_tb("could not get fw-test tcp socket name!");

		if(listen(tcp_fd, 10))
			throw errno_exception_tb("could not set fw-test tcp socket to listen(10) mode!");

		tcp_port = ntohs(local_addr.sin_port);

		instance->daemon->add_io_watch(tcp_fd, IO_WATCH_READ, on_incoming_tcp_connection, this, "fw_test_tcp_listen");
	}
}

void ln_instance::FirewallTest::ack_reception(const std::string& kind, uint64_t remote_id)
{
	if(!instance->connection)
		return;
	request_t req;
	req["response"] = repr("fw_test_result");
	req["kind"] = repr(kind);
	req["remote_id"] = std::to_string(remote_id);
	instance->connection->send(req);
}

static int _connect_with_timeout(int fd, const sockaddr* addr, os::socklen_t_type addr_len, double timeout)
{
	int flags = fcntl(fd, F_GETFL, NULL); // ignore error
	if (flags != -1)
		fcntl(fd, F_SETFL, flags | O_NONBLOCK); // ignore error

	if (connect(fd, addr, addr_len) == -1)
		if (errno != EINPROGRESS)
			return -1;

	fd_set write_set;
	FD_ZERO(&write_set);
	FD_SET(fd, &write_set);

	struct timeval tv;
	tv.tv_sec = (unsigned long)timeout;
	tv.tv_usec = (unsigned long)((timeout - tv.tv_sec) * 1e6);

	int ret = select(fd + 1, NULL, &write_set, NULL, &tv);

	if (ret == -1)
		return -1;

	if (ret == 0)
		throw str_exception_tb("timeout connecting fw-test tcp server!");

	if (!FD_ISSET(fd, &write_set))
		return -1;

        socklen_t len = sizeof(ret);
        getsockopt(fd, SOL_SOCKET, SO_ERROR, &ret, &len);

	// restore flags
	if (flags != -1)
		fcntl(fd, F_SETFL, flags);

	if(ret) {
		errno = ret;
		return -1;
	}
	return 0;
}

void ln_instance::test_for_fw(std::string ip, unsigned int udp_port, unsigned int tcp_port, uint64_t remote_id, double timeout)
{
	struct sockaddr_in target_addr {};

	target_addr.sin_family = AF_INET;
#ifdef __WIN32__
	target_addr.sin_addr.s_addr = inet_addr(ip.c_str());
	if(target_addr.sin_addr.s_addr == INADDR_NONE)
#else
	if(!inet_aton(ip.c_str(), &target_addr.sin_addr))
#endif
	{
		throw errno_exception_tb("invalid ip %s!", repr(ip).c_str());
	}

	int fd = socket(AF_INET, SOCK_STREAM, 0);
	if(fd == -1)
		throw errno_exception_tb("could not create fw-test tcp socket!");

	target_addr.sin_port = htons(tcp_port);

	if(_connect_with_timeout(fd, (const sockaddr*)&target_addr, sizeof(target_addr), timeout))
		throw errno_exception_tb("could not connect to remote fw-test tcp port %s:%d", ip.c_str(), tcp_port);

	int n = send(fd, &remote_id, sizeof(remote_id), 0);
	if(n != sizeof(remote_id))
		throw errno_exception_tb("could not send TCP remote_id: %d", n);

	// wait for remote side to close connection
	uint8_t dummy;
#if defined(SO_RCVTIMEO)
	struct timeval tv;
	tv.tv_sec = (unsigned long)timeout;
	tv.tv_usec = (unsigned long)((timeout - tv.tv_sec) * 1e6);
	if (setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0)
		log("fw-test: could not set SO_RCVTIMEOout! errno %d: %s\n", errno, strerror(errno));
#else
#  warning SO_RCVTIMEO is not defined. there will be no read-timeout for firewall-test tcp connections!
#endif
	n = recv(fd, &dummy, sizeof(dummy), 0); // ignore return value
	::closesocket(fd);
	if(n != 0) // expect other side to close
		throw errno_exception_tb("could not send(recv wait shutdown) TCP remote_id");

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if(fd == -1)
		throw errno_exception_tb("could not create fw-test udp socket!");
	target_addr.sin_port = htons(udp_port);
	for(unsigned int i = 0; i < 3; i++) {
		int n = sendto(fd, &remote_id, sizeof(remote_id), 0, (sockaddr*)&target_addr, sizeof(target_addr));
		if(n != sizeof(remote_id))
			throw errno_exception_tb("could not send UDP remote_id: %d", n);
		sleep(0.03);
	}
	::closesocket(fd);
}
