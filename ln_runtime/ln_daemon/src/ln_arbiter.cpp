/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include "os.h"

#include <string_util/exceptions.h>
#include "logger.h"
#include "util.h"
#include "ln_arbiter.h"

#include <stdio.h>
#include <errno.h>
#if !defined(__WIN32__)
#include <netinet/ip.h>
#endif

#include <list>
#include <string>

using namespace std;
using namespace string_util;

ln_arbiter_client::ln_arbiter_client() : la(this, &_received_line) {
	fd = -1;
}

bool ln_arbiter_client::_connect_or_start_arbiter(bool start) {
	if(fd != -1) 
		return true; // already connected

	// try to connect to arbiter
	fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	int tries = 3;
	pid_t arbiter_pid = 0;

	unsigned int arbiter_port = LN_ARBITER_PORT;
	const char* arbiter_port_env = getenv("LN_ARBITER_PORT");
	if(arbiter_port_env)
		arbiter_port = atoi(arbiter_port_env);
	
	struct sockaddr_in arbiter_addr;
	arbiter_addr.sin_family = AF_INET;
	arbiter_addr.sin_port = htons(arbiter_port);
#if defined(__WIN32__)
	arbiter_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
#else
	arbiter_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
#endif

	while(true) {
		if(connect(fd, (struct sockaddr*)&arbiter_addr, sizeof(arbiter_addr)) == 0)
			break; // successfull connection
		// throw errno_exception_tb("failed to connect() to arbiter!");
		log("failed to connect to arbiter on port %d: %s", arbiter_port, strerror(errno));
#if defined(__QNX__) || defined(__VXWORKS__)
		// qnx can not reuse a socket that once had connection refused!?! - it complains about invalid argument on next try...
		// ...so just burn them all here...
		close(fd);
		fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
#endif
		if(!start)
			return false;
		tries --;
		if(tries == 0)
			throw str_exception_tb("failed to connect and start arbiter on port %d!", arbiter_port);
		if(arbiter_pid != 0) {
			os::sleep(1);
			continue;
		}
#if defined(__VXWORKS__)
		{
			// const char* argv[] = { program_name.c_str(), "arbiter_child", NULL };
			const char* argv[] = { "ln_arbiter", "arbiter_child", arbiter_port_env, NULL };
			int priority = 221; // lowest prio
			int stacksize = 0x4000;
			int options = RTP_CPU_AFFINITY_NONE;
			int task_options = VX_FP_TASK;
			arbiter_pid = rtpSpawn((const char*)program_name.c_str(), (const char**)argv, (const char**)NULL, priority, stacksize, options, task_options);
			if(arbiter_pid == ERROR)
				throw errno_exception_tb("failed to rtpSpawn() arbiter child!");
		}
#elif defined(__WIN32__)
		{
			STARTUPINFO info;
			memset(&info, 0, sizeof(info));
			info.cb = sizeof(info);
			info.dwFlags = STARTF_USESTDHANDLES | CREATE_NEW_PROCESS_GROUP ;
			info.hStdInput = GetStdHandle(STD_INPUT_HANDLE); // todo: not nice!
			info.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
			info.hStdError = GetStdHandle(STD_OUTPUT_HANDLE);

			string cmd_line = format_string("%s arbiter_child", program_name.c_str());
			if(arbiter_port_env) {
				cmd_line += " ";
				cmd_line += arbiter_port_env;
			}
			char* non_const_cmd = strdup(cmd_line.c_str()); // TODO: free!!

			PROCESS_INFORMATION pinfo;
			BOOL cp_ret = CreateProcess(
				program_name.c_str(),
				non_const_cmd,
				NULL,       // LPSECURITY_ATTRIBUTES lpProcessAttributes,
				NULL,       // LPSECURITY_ATTRIBUTES lpThreadAttributes,
				TRUE,      // BOOL bInheritHandles,
				0,          // DWORD dwCreationFlags,
				NULL, // env_block,  // LPVOID lpEnvironment, // TODO: windows needs systemroot and so on to find winsock dlls and so on!
				NULL,       // LPCTSTR lpCurrentDirectory,
				&info,      // LPSTARTUPINFO lpStartupInfo,
				&pinfo      // LPPROCESS_INFORMATION lpProcessInformation
				);
			if(!cp_ret)
				throw errno_exception_tb("could not arbiter CreateProcess('%s')!", program_name.c_str());
			arbiter_pid = pinfo.dwProcessId;
			free(non_const_cmd);
			CloseHandle(pinfo.hProcess);
			CloseHandle(pinfo.hThread);
		}

#else // POSIX
		// try to start arbiter, pass arbiter_port_env via env...
		arbiter_pid = fork();
		if(arbiter_pid == -1)
			throw errno_exception_tb("failed to fork() arbiter child!");

		if(arbiter_pid == 0) {
			close(fd);
			ln_arbiter arbiter;
			arbiter.run();
			exit(0);
		}
#endif

		log("started arbiter at pid %d\n", arbiter_pid);
		// wait a little
		os::sleep(1);
		// try again to connect!
	}
	return true;
}

bool ln_arbiter_client::register_daemon(unsigned int daemon_port) {
	_connect_or_start_arbiter();

#if defined(__VXWORKS__)
	unsigned int uid = 0;
#elif defined(__WIN32__)
	unsigned int uid = 0; // todo: assume only one user on win32!
#else // POSIX
	uid_t uid = getuid();
#endif

	if(fake_uid != -1)
		uid = fake_uid;

#if defined(__QNX__)
	string user_name(getlogin());
#elif defined(__VXWORKS__)
	string user_name = "root";
#elif defined(__WIN32__)
	string user_name = "current user";
#elif defined(__ANDROID__)
	string user_name = "current user";
#else // posix
	string user_name(cuserid(NULL));
#endif
	log("have connection to arbiter - requesting permission for user %s, uid %d and port number %d at pid %d!\n", repr(user_name).c_str(), uid, daemon_port, getpid());

	vector<string> msg = _send_request(format_string("register (%d, %s, %d, %d)\n", uid, repr(user_name).c_str(), daemon_port, getpid()));
	if(msg[0] == "error") {
		string v = *eval_full(msg[1]);
		log("received error from arbiter: %s", v.c_str());
		return false;
	}
	log("arbiter registration succeeded!");
	return true;
}

bool ln_arbiter_client::update_pid() {
	_connect_or_start_arbiter();
	vector<string> msg = _send_request(format_string("update_pid %d\n", getpid()));
	if(msg[0] == "error") {
		string v = *eval_full(msg[1]);
		log("received error from arbiter: %s", v.c_str());
		return false;
	}
	return true;
}

list<string> ln_arbiter_client::list_daemons() {
	_connect_or_start_arbiter();

	vector<string> msg = _send_request("list\n");
	py_value* arg = eval_full(msg[1]);
	if(msg[0] == "error")
		throw str_exception_tb("arbiter-client received error while requesting list of running daemons: %s", ((string)*arg).c_str());

	py_list* pylist = dynamic_cast<py_list*>(arg);
	if(!pylist)
		throw str_exception_tb("arbiter-client received wrong response while requesting list of running daemons: (type: %s), %s", 
							   arg->type_name().c_str(), repr(msg[1]).c_str());

	list<string> ret;
	for(py_list_value_t::iterator i = pylist->value.begin(); i != pylist->value.end(); ++i)
		ret.push_back((**i));
	delete arg;
	return ret;
}

void ln_arbiter_client::quit_arbiter() {
	bool is_running = _connect_or_start_arbiter(false);
	if(!is_running) {
		log("no arbiter running...\n");
		return;
	}
	_send_request("quit\n");
	return;
}
void ln_arbiter_client::unregister_from_arbiter() {
	bool is_running = _connect_or_start_arbiter(false);
	if(!is_running) {
		log("no arbiter running...\n");
		return;
	}
	_send_request("unregister\n");
	return;
}

vector<string> ln_arbiter_client::_send_request(string msg) {
	int ret = ::send(fd, msg.c_str(), msg.size(), 0);
	if(ret == -1)
		throw errno_exception("send");
	string response = _wait_for_line();
	return split_string(response, " ", 1);
}

void ln_arbiter_client::_received_line(void* instance, string line) {
	ln_arbiter_client* self = (ln_arbiter_client*)instance;
	self->_have_line = true;
	self->_last_line = line;
}

string ln_arbiter_client::_wait_for_line() {
	_have_line = false;
	while(!_have_line) {
		char buffer[1024];
		int ret = ::recv(fd, buffer, 1024, 0);
		if(ret == -1)
			throw errno_exception("recv");
		if(ret == 0)
			throw str_exception_tb("got EOF on arbiter-connection while waiting for answer!");
		la.write(buffer, ret);
	}
	return _last_line;
}

ln_arbiter::ln_arbiter() {
	fd = -1;
}

class ln_arbiter_connection {
	line_assembler la;

	static void _received_line(void* instance, string line) {
		ln_arbiter_connection* self = (ln_arbiter_connection*)instance;
		self->received_line(line);
	}
	void received_line(string line) {
		vector<string> msg = split_string(line, " ", 1);
		static py_value* arg = NULL;
		if(arg) {
			delete arg;
			arg = NULL;
		}
			
		if(msg.size() > 1)
			arg = eval_full(msg[1]);
		string& request = msg[0];
		if(request == "register" && arg != NULL) {
			// other side is a new daemon which wants to register
			py_list* pylist = dynamic_cast<py_list*>(arg);
			if(pylist->value.size() != 4) {
				send_error("register requests expects exactly 4 arguments: (uid, user_name, daemon_port, daemon_pid)\n");
				return;
			}
			py_list_value_t::iterator i = pylist->value.begin();
			int uid = **i++;
			string user_name = **i++;
			unsigned int daemon_port = **i++;
			pid_t daemon_pid = (int)**i++;
			log("got register request for uid %d, user_name %s, daemon_port %d\n", uid, repr(user_name).c_str(), daemon_port);
			unsigned int existing_daemon_port = arbiter->get_daemon_port_for_uid(uid);
			if(existing_daemon_port != 0) {
				send_error(format_string("user id %d/%s is already registered for daemon at port %d and pid %d", 
							 uid, user_name.c_str(),
							 existing_daemon_port,
							 arbiter->get_daemon_pid_for_uid(uid)));
				return;
			}
			this->uid = uid;
			this->user_name = user_name;
			this->daemon_port = daemon_port;
			this->daemon_pid = daemon_pid;
			registered = true;
			id = format_string("ln_daemon from %s:%d at fd %d for uid %d/%s at daemon_port %d and startup pid %d", 
							   ip_address.c_str(), port, fd, uid, user_name.c_str(), daemon_port, daemon_pid);
			send_ok("successfully registered!");

		} else if(request == "update_pid" && arg != NULL) {
			// other side is a new daemon which wants to register
			daemon_pid = (int)*arg;
			id = format_string("ln_daemon from %s:%d at fd %d for uid %d/%s at daemon_port %d and pid %d", 
							   ip_address.c_str(), port, fd, uid, user_name.c_str(), daemon_port, daemon_pid);
			send_ok("successfully registered!");

		} else if(request == "unregister") {
			send_ok("successfully un-registered!");
			close();

		} else if(request == "list") {
			// other side is a new daemon which wants to register
			py_list* l = new py_list();
			list<string> stdl = arbiter->get_daemons_list();
			for(list<string>::iterator i = stdl.begin(); i != stdl.end(); ++i)
				l->value.push_back(new py_string(*i));
			send_ok(l);
			delete l;

		} else if(request == "query_daemon_port") {
			// other side is a manager which needs a daemon for the given userid
			if(!arg) {
				send_error("query_daemon_port needs to have a user-id as argument!\n");
				return;
			}
			unsigned int daemon_port = 0;
			unsigned int requested_uid = 0;
			py_int* int_arg = dynamic_cast<py_int*>(arg);
			py_string* string_arg = dynamic_cast<py_string*>(arg);
			if(string_arg) {
				log("arbiter got string user id: '%s'\n", string_arg->value.c_str());
				requested_uid = getuid_by_name(string_arg->value.c_str());
			} else {
				log("arbiter got numeric user id: %d\n", int_arg->value);
				requested_uid = (int)*int_arg;
			}
			log("arbiter received daemon_query for user_id %d\n", requested_uid);
			daemon_port = arbiter->query_daemon_port(requested_uid);
			if(daemon_port)
				send_ok(format_string("%d", daemon_port), true);
			else
				send_error(format_string("no daemon running for uid %d", requested_uid));

		} else if(request == "quit") {
			log("arbiter received quit command!\n");
			arbiter->keep_running = false;
			send_ok("quitting...");
		} else {
			log("received unknown request: %s\n", repr(line).c_str());
			send_error(format_string("unknown request: %s", repr(line).c_str()));
		}
	}

	void send_error(string msg) {
		string error_msg = format_string("error %s\n", repr(msg).c_str());
		::send(fd, error_msg.c_str(), error_msg.size(), 0);
	}

	void send_ok(py_value* pyv) {
		send_ok(repr(pyv), true);
	}
	void send_ok(string msg, bool is_repr=false) {
		string ok_msg;
		if(is_repr)
			ok_msg = format_string("ok %s\n", msg.c_str());
		else
			ok_msg = format_string("ok %s\n", repr(msg).c_str());
		::send(fd, ok_msg.c_str(), ok_msg.size(), 0);
	}

public:
	string id;
	int fd;
	struct sockaddr_in addr;
	string ip_address;
	unsigned int port;
	bool connected;
	ln_arbiter* arbiter;

	string user_name;
	unsigned int uid;
	unsigned int daemon_port;
	pid_t daemon_pid;
	bool registered;

	ln_arbiter_connection(ln_arbiter* parent, int fd, struct sockaddr_in* addr) : la(this, &_received_line) {
		this->arbiter = parent;
		this->fd = fd;
		this->addr = *addr;
		connected = true;
		registered = false;
		ip_address = inet_ntoa(addr->sin_addr);
		port = ntohs(addr->sin_port);
		id = format_string("ln_daemon from %s:%d at fd %d", ip_address.c_str(), port, fd);
	}

	operator string() {
		return id;
	}


	bool read() {
		const unsigned int buffer_size = 1024 * 2;
		char buffer[buffer_size];
		int n = recv(fd, buffer, buffer_size, 0);
		if(n == -1)
			throw errno_exception_tb("recv");
		if(n == 0) {
			// eof
			log("got EOF from arbiter-client at fd %d\n", fd);
			close();
			return false;
		}
		la.write(buffer, n);
		return connected;
	}
	void close() {
		connected = false;
		if(fd != -1)
			_ln_close_socket(fd);
		fd = -1;
	}
};


void ln_arbiter::run(int arbiter_port) {
	const char* arbiter_port_env = getenv("LN_ARBITER_PORT");
	if(arbiter_port_env)
		arbiter_port = atoi(arbiter_port_env);
	
	struct sockaddr_in arbiter_addr;
	arbiter_addr.sin_family = AF_INET;
	arbiter_addr.sin_port = htons(arbiter_port);
	arbiter_addr.sin_addr.s_addr = INADDR_ANY;

	fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(fd == -1)
		throw errno_exception("socket");
		
#ifdef __VXWORKS__
	/*
	  did not work on sigyn:
	  setsockopt(SO_REUSEADDR): invalid argument (errno 22)
	  char on = 1;
	  if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0)
	  throw errno_exception("setsockopt(SO_REUSEADDR)");
	*/
#elif __WIN32__
	char on = 1;
	if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0)
		throw errno_exception("setsockopt(SO_REUSEADDR)");
#else
	int on = 1;
	if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0)
		throw errno_exception("setsockopt(SO_REUSEADDR)");
#endif
	
	if(bind(fd, (struct sockaddr*) &arbiter_addr, sizeof(arbiter_addr)))
		throw errno_exception("bind");
	
	if(listen(fd, 5))
		throw errno_exception("listen");
		
	log("ln_daemon arbiter started on port %d pid %d", arbiter_port, getpid());

	// close all file deskriptors
#if !defined(__VXWORKS__) && !defined(__WIN32__)
	int table_size = os::get_max_number_of_fds();
	for(int i = 0; i < table_size; i++) {
		if(fd == i)
			continue;
		if(fd <= 2)
			continue;
		close(i);
	}
#endif
	/*
	if(setsid() == -1)
		throw errno_exception("setsid");
	*/

#if !defined(__WIN32__)	
	// redirect input from /dev/null
	int ifd = open("/dev/null", O_RDONLY);
	if(ifd == -1)
		throw errno_exception("open /dev/null");
	dup2(ifd, 0); // stdin
	// redirect output to
	dup2(ifd, 1); // stdout
	dup2(ifd, 2); // stderr
#if !defined(__VXWORKS__)
	if(setsid() == -1)
		throw errno_exception("setsid");
#endif
#endif

	keep_running = true;
	while(keep_running) {
		fd_set read_fds, write_fds, except_fds;
		int max_fd = 0;
		FD_ZERO(&read_fds);
		FD_ZERO(&write_fds);
		FD_ZERO(&except_fds);

		FD_SET(fd, &read_fds);
		if(fd > max_fd)
			max_fd = fd;
		
		for(connections_t::iterator i = connections.begin(); i != connections.end(); ++i) {
			ln_arbiter_connection* c = *i;
#if !defined(__WIN32__)
			if(c->fd >= FD_SETSIZE) {
				log("warning, can not listen to connection %d because its higher than FD_SETSIZE!", c->fd);
				continue;
			}
#endif
			FD_SET(c->fd, &read_fds);
			if(c->fd > max_fd)
				max_fd = c->fd;
		}
		int n = select(max_fd + 1, &read_fds, NULL, NULL, NULL);
		
		if(n == -1)
			throw errno_exception("select");

		if(n > 0) {
			if(FD_ISSET(fd, &read_fds)) {
				// new client
				struct sockaddr_in client_address;
				os::socklen_t_type addr_len = sizeof(client_address);
				int cfd = accept(fd, (struct sockaddr*)&client_address, &addr_len);
				if(cfd == -1)
					throw errno_exception("accept");
				connections.push_back(new ln_arbiter_connection(this, cfd, &client_address));
			}

			for(connections_t::iterator i = connections.begin(); i != connections.end(); ++i) {
				ln_arbiter_connection* c = *i;
				if(c->fd > max_fd)
					continue;
				if(!FD_ISSET(c->fd, &read_fds))
					continue;
				try {
					c->read();
				}
				catch(exception& e) {
					log("uncaught exception in connection->read on fd %d: %s", fd, e.what());
					c->close();
				}
			}
			for(connections_t::iterator i = connections.begin(); i != connections.end(); ) {
				ln_arbiter_connection* c = *i;
				if(c->connected) {
					i ++;
					continue;
				}
				log("removing client: %s\n", ((string)*c).c_str());
				connections.erase(i);
				i = connections.begin();
			}
		}
	}
	log("arbiter quitting\n");
}


unsigned int ln_arbiter::get_daemon_port_for_uid(unsigned int uid) {
	return query_daemon_port(uid);
}

unsigned int ln_arbiter::get_daemon_pid_for_uid(unsigned int uid) {
	for(connections_t::iterator i = connections.begin(); i != connections.end(); i++) {
		ln_arbiter_connection* c = *i;
		if(!c->registered)
			continue;
		if(c->uid == uid)
			return c->daemon_pid;
	}
	return 0;
}

list<string> ln_arbiter::get_daemons_list() {
	list<string> ret;
	for(connections_t::iterator i = connections.begin(); i != connections.end(); i++) {
		ln_arbiter_connection* c = *i;
		if(!c->registered)
			continue;
		ret.push_back(*c);
	}
	return ret;
}

unsigned int ln_arbiter::query_daemon_port(unsigned int uid) {
	for(connections_t::iterator i = connections.begin(); i != connections.end(); i++) {
		ln_arbiter_connection* c = *i;
		if(!c->registered)
			continue;
		if(c->uid == uid)
			return c->daemon_port;
	}
	// no matching daemon found for this requested uid. is there a root daemon running?
	for(connections_t::iterator i = connections.begin(); i != connections.end(); i++) {
		ln_arbiter_connection* c = *i;
		if(!c->registered)
			continue;
		if(c->uid == 0)
			return c->daemon_port;
	}
#ifdef UID0_TAKES_ANY_DAEMON_HACK
	if(uid == 0) {
		for(connections_t::iterator i = connections.begin(); i != connections.end(); i++) {
			ln_arbiter_connection* c = *i;
			if(!c->registered)
				continue;
			return c->daemon_port;
		}
	}
#endif
	return 0;
}
