
/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include "os.h"
#include "ln_instance.h"

ln_instance::ln_instance(ln_daemon* daemon, string instance_name, ln_manager_connection* connection)
	: name(instance_name), firewall_test(this), connection(connection) {
	this->daemon = daemon; // to avoid warning

	int ret = os::create_mutex(&log_tokens_mutex);
	if(ret)
		throw retval_exception_tb(ret, "create_mutex log_tokens");
}

ln_instance::~ln_instance() {
	log("ln_instance %s terminating! will stop %d processes and %d ports!", repr(name).c_str(), processes.size(), ports.size());
	release_all_resources();

	os::lock_mutex(&log_tokens_mutex);
	for(log_tokens_t::iterator i = log_tokens.begin(); i != log_tokens.end(); i++)
		delete i->second;
	os::unlock_mutex(&log_tokens_mutex);
	os::destroy_mutex(&log_tokens_mutex);
}

void ln_instance::release_all_resources() {
	for(processes_t::iterator i = processes.begin(); i != processes.end(); i++) {
		ln_process* p = i->second;
		log(" stop ln_process %s at pid %d\n", repr(p->config.name).c_str(), p->pid);
		delete p;
	}
	processes.clear();
	for(states_t::iterator i = states.begin(); i != states.end(); ++i)
		delete i->second;
	states.clear();
	
	while(tcp_forwards.size())
		tcp_forwards.front()->destroy(true);
	
	for(ports_t::iterator i = ports.begin(); i != ports.end(); i++) {
		ln_port* p = i->second;
		log(" stop ln_port %s\n", p->get_id().c_str());
		delete p;
	}
	ports.clear();
}

// process management
void ln_instance::register_process(ln_process* process) {
	processes[process->get_pid()] = process;
}

void ln_instance::remove_process(int pid) {
	ln_process* process = processes[pid];

	for(processes_to_stop_t::iterator i = processes_to_stop.begin(); i != processes_to_stop.end(); ) {
		if(i->process == process) {
			daemon->remove_timeout(i->timeout_id);
			processes_to_stop.erase(i);
			i = processes_to_stop.begin();
		} else
			i++;
	}

	processes.erase(pid);
	delete process;
}

ln_process* ln_instance::get_process(int pid) {
	if(processes.find(pid) == processes.end())
		throw str_exception_tb("unknown process with pid %d", pid);
	return processes[pid];
}

void ln_instance::stop_process(ln_process* process, unsigned int term_signal) {
	// immediately send SIGTERM, after 2 seconds of not terminating, send SIGKILL
#ifdef __WIN32__
	process->output_line("\nsending SIGKILL\n");
	process->stop();
#else
	process->output_line(format_string("\r\nsending term_signal %d - will send SIGKILL after %.1fs\r\n", term_signal, process->config.term_timeout));
	int ret = os::send_signal(-process->get_pid(), term_signal);
	if(ret)
		process->output_line(format_string("\r\nerror sending term_signal %d to pid %d: %s\r\n",
						   term_signal, -process->get_pid(), strerror(errno)));
	// add timeout of 2 seconds
	string hint = format_string("stop_process timeout for pid %d", process->get_pid());
	int timeout_id = daemon->add_timeout(process->config.term_timeout, &_stop_process_timeout, this, hint);
	process_to_stop_t p = {timeout_id, ln_get_monotonic_time(), process};
	processes_to_stop.push_back(p);
#endif
}

bool ln_instance::_stop_process_timeout(void* instance) {
	ln_instance* self = (ln_instance*)instance;
	return self->stop_process_timeout();
}

bool ln_instance::stop_process_timeout() {
	double deadline = ln_get_monotonic_time() - SIGTERM_TIMEOUT;
	bool found_one = false;
	for(processes_to_stop_t::iterator i = processes_to_stop.begin(); i != processes_to_stop.end(); i++) {
		if(deadline < i->term_time)
			continue; // not yet
		// send sigterm to this process!
		log("process %d did not terminate yet. sending SIGKILL!", i->process->get_pid());
		i->process->output_line("\nsending SIGKILL\n");
		i->process->stop();
		processes_to_stop.erase(i);
		found_one = true;
		break;
	}
	return !found_one; // no repetition!
}



// port management
void ln_instance::delete_port(string port) {
	ln_port* p = get_port(port);
	log("will delete port %p %s\n", p, p->get_id().c_str());
	delete p;
	ports.erase(port);
}

void ln_instance::connect_ports(string source, string target) {
	ln_source_port* source_port = dynamic_cast<ln_source_port*>(get_port(source));
	ln_sink_port* sink_port = dynamic_cast<ln_sink_port*>(get_port(target));
	if(!source_port)
		throw str_exception("port %s is not a source port!", source.c_str());
	if(!sink_port) {
		throw str_exception("port %s is not a sink port!", target.c_str());
	}

	if(sink_port->get_source() == source_port) {
		log("warning: port %s already connected to source port %s! doing nothing.",
		    sink_port->get_id().c_str(), source_port->get_id().c_str());
		return;
	}
	if(sink_port->get_source()) {
		log("warning: port %s already had source port %s set. overwriting with %s...",
		    sink_port->get_id().c_str(), sink_port->get_source()->get_id().c_str(), source_port->get_id().c_str());
		sink_port->get_source()->remove_sink(sink_port);
	}
	source_port->add_sink(sink_port);
}

void ln_instance::disconnect_ports(string source, string target) {
	ln_source_port* source_port = dynamic_cast<ln_source_port*>(get_port(source));
	ln_sink_port* sink_port = dynamic_cast<ln_sink_port*>(get_port(target));
	if(!source_port)
		throw str_exception("port %s is not a source port!", source.c_str());
	if(!sink_port)
		throw str_exception("port %s is not a source port!", target.c_str());

	if(!source_port->remove_sink(sink_port))
		throw str_exception("ports were not connected!");
}

ln_port* ln_instance::get_port(string port) {
	if(ports.find(port) == ports.end())
		throw str_exception_tb("port of name %s not found!", repr(port).c_str());
	return ports[port];
}

void ln_instance::register_port(ln_port* port) {
	ports[port->get_id()] = port;
}
#ifdef __ANDROID__
bool ln_instance::get_shm_fd(string shm_name, int* fd, unsigned int* shm_size) {
	for(ports_t::iterator i = ports.begin(); i != ports.end(); ++i) {
		ln_port* p = i->second;
		log("found port %s, type %s\n", p->get_id().c_str(), repr(p->port_type).c_str());
		if(p->port_type != "shm")
			continue;
		ln_shm_port* shm_port = dynamic_cast<ln_shm_port*>(p);
		if(!shm_port) {
			log("  -> dynamic cast to shm_port failed!\n");
			continue;
		}
		log(" -> its a shm port with name %s\n", repr(shm_port->name).c_str());
		if(shm_port->name == shm_name) {
			log("shm_name %s found in instance %s at port %s\n",
			    repr(shm_name).c_str(),
			    repr(name).c_str(),
			    p->get_id().c_str());
			*fd = shm_port->fd;
			*shm_size = shm_port->shm_size;
			return true;
		}
	}
	return false;
}
#endif

void ln_instance::fill_ports_list(py_list& l, int with_last_packet) {
	for(ports_t::iterator i = ports.begin(); i != ports.end(); i++)
		l.value.push_back(i->second->get_tuple(NULL, with_last_packet));
}

void ln_instance::fill_processes_list(py_list& l) {
	for(processes_t::iterator i = processes.begin(); i != processes.end(); i++)
		l.value.push_back(i->second->get_tuple());
}


ln_state* ln_instance::get_state(string name, string check, string up, string down, bool os_state) {
	ln_state::config_t config = {
		check, up, down, os_state
	};

	if(states.find(name) == states.end()) {
		// create new state!
		states[name] = new ln_state(this, name, config);
	} else {
		states[name]->update(config);
	}
	return states[name];
}

ln_state* ln_instance::get_state(string name) {
	if(states.find(name) == states.end())
		return NULL;
	return states[name];
}

void ln_instance::register_tcp_forward(ln_tcp_forward* forward) {
	tcp_forwards.push_back(forward);
}

void ln_instance::delete_tcp_forward(ln_tcp_forward* forward, bool do_delete) {
	for(tcp_forwards_t::iterator i = tcp_forwards.begin(); i != tcp_forwards.end(); ++i) {
		if(*i == forward) {
			tcp_forwards.erase(i);
			break;
		}
	}
	if(do_delete)
		delete forward;
}

bool ln_instance::delete_tcp_forward(string name) {
	for(tcp_forwards_t::iterator i = tcp_forwards.begin(); i != tcp_forwards.end(); ++i) {
		if((*i)->data == name) {
			delete *i;
			return true;
		}
	}
	return false;
}

string ln_instance::is_tcp_forward_source_port(int port) {
	for(tcp_forwards_t::iterator i = tcp_forwards.begin(); i != tcp_forwards.end(); ++i) {
		ln_tcp_forward* forward = *i;
		string from_ip;
		int from_port;
		if(forward->is_source_port(port, from_ip, from_port))
			return format_string("(%s, %d)", repr(from_ip).c_str(), from_port);
	}
	return "''";
}
void ln_instance::fill_tcp_forwards_list(py_list& l) {
	unsigned int protocol = 24;
	if(connection)
		protocol = connection->manager_protocol;
	for(tcp_forwards_t::iterator i = tcp_forwards.begin(); i != tcp_forwards.end(); i++)
		l.value.push_back((*i)->get_tuple(protocol));
}

void ln_instance::register_log_token(request_t& answer, std::string data)
{
	log_token* lt = new log_token(this, data);
	std::string token = lt->get_token();
	os::lock_mutex(&log_tokens_mutex);
	log_tokens[token] = lt;
	os::unlock_mutex(&log_tokens_mutex);
	
	answer["token"] = repr(token);
	answer["port"] = format_string("%d", lt->get_port());
}

void ln_instance::remove_log_token(std::string token)
{
	os::lock_mutex(&log_tokens_mutex);
	log_tokens_t::iterator i = log_tokens.find(token);
	if(i != log_tokens.end()) {
		delete i->second;
		log_tokens.erase(i);
	}
	os::unlock_mutex(&log_tokens_mutex);
}

void ln_instance::remove_log_tokens(py_list* tokens_pylist)
{
	for(py_list_value_t::iterator i = tokens_pylist->value.begin(); i != tokens_pylist->value.end(); i++) {
		py_string* token = dynamic_cast<py_string*>(*i);
		remove_log_token(token->value);
	}
}
