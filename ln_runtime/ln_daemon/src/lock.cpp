/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include <stdio.h>
#include <string_util/exceptions.h>
#include "lock.h"

using namespace string_util;
flolock::flolock(bool locked) {
	this->locked = false;
	int ret = os::create_mutex(&rwlock);
	// printf("created flolock %p, this: %p\n", &rwlock, this);
	if(ret)
		throw retval_exception_tb(ret, "create_mutex");
	if(locked)
		(*this)();
}

flolock::~flolock() {
	// printf("destroy flolock %p, this: %p\n", &rwlock, this);
	if(this->locked)
		unlock();
	os::destroy_mutex(&rwlock);
}

void flolock::operator()() {
	// printf("locking flolock %p, this: %p\n", &rwlock, this);
	int ret = os::lock_mutex(&rwlock);
	if(ret)
		throw retval_exception_tb(ret, "lock_mutex");
	this->locked = true;
}

bool flolock::trylock() {
	if(os::trylock_mutex(&rwlock) == 0) {
		this->locked = true;
		return true;
	}
	return false;
		
}

void flolock::unlock() {
 	this->locked = false;
	int ret = os::unlock_mutex(&rwlock);
	if(ret) {
		fprintf(stderr, "error unlocking mutex\n");
		throw retval_exception_tb(ret, "unlock_mutex");
	}
}

bool flolock::free() {
	bool was_free = trylock();
	if(was_free)
		unlock();
	return was_free;
}

