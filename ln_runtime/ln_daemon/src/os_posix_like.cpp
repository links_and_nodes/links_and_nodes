/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#if !defined(__WIN32__)

#include "os.h"
#include "os_posix_like.h"
#include "logger.h"

#include <stdio.h>
#include <string.h>
#include <sys/resource.h>

#include <string_util/string_util.h>

using namespace string_util;

namespace os {

//! set current thread priority and scheduling policy
/*!
  \param policy new thread scheduling policy
  \param priority new thread priority
  \return N/A
*/
void set_own_prio(int policy, int priority, int affinity) {
	if(priority > 0 || policy != -1) {
		struct sched_param param;
		sched_getparam(0, &param);
		param.sched_priority = priority;
		if(policy == -1)
			policy = SCHED_OTHER;
		int ret = sched_setscheduler(0, policy, &param);
		if(ret) {
			fprintf(stderr, "warning: sched_setscheduler(policy=%d, priority=%d) failed with %d: %d/%s\n",
				policy, priority, ret, errno, strerror(errno));
			int minp = sched_get_priority_min(policy);
			int maxp = sched_get_priority_max(policy);
			fprintf(stderr, "allowed priority range for this policy: from %d to %d\n", minp, maxp);
		}
	}
	if(affinity != -1)
		set_affinity_of_current_thread(affinity);
}

int create_mutex(mutex_handle_t* mutex_handle) {
	int ret;
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
#ifndef __ANDROID__
	pthread_mutexattr_setprotocol(&attr, PTHREAD_PRIO_INHERIT);
#endif
	ret = pthread_mutex_init(mutex_handle, &attr);
	if(ret != 0) {
            fprintf(stderr, "warning: pthread_mutex_init with protocol PTHREAD_PRIO_INHERIT failed with error code %d (%s),"\
		            " try again with PTHREAD_PRIO_NONE\n", ret, strerror(ret));
            pthread_mutexattr_setprotocol(&attr, PTHREAD_PRIO_NONE);
            ret = pthread_mutex_init(mutex_handle, &attr);
	}
	pthread_mutexattr_destroy(&attr);
	return ret;
}

int destroy_mutex(mutex_handle_t* mutex_handle) {
	return pthread_mutex_destroy(mutex_handle);
}

int lock_mutex(mutex_handle_t* mutex_handle) {
	return pthread_mutex_lock(mutex_handle);
}

int trylock_mutex(mutex_handle_t* mutex_handle) {
	return pthread_mutex_trylock(mutex_handle);
}

int unlock_mutex(mutex_handle_t* mutex_handle) {
	return pthread_mutex_unlock(mutex_handle);
}


void condition_create(condition_handle_t* handle) {
	int ret;
	if((ret = pthread_cond_init(handle, NULL)))
		throw retval_exception_tb(ret, "pthread_cond_init");
}

void condition_destroy(condition_handle_t* handle) {
	pthread_cond_destroy(handle);
}

void condition_wait(condition_handle_t* cond, mutex_handle_t* mutex) {
	int ret = pthread_cond_wait(cond, mutex);
	if(ret)
		throw retval_exception_tb(ret, "pthread_cond_wait");
}

void condition_signal(condition_handle_t* handle) {
	int ret = pthread_cond_signal(handle);
	if(ret)
		throw retval_exception_tb(ret, "pthread_cond_signal");
}

void condition_broadcast(condition_handle_t* handle) {
	int ret = pthread_cond_broadcast(handle);
	if(ret)
		throw retval_exception_tb(ret, "pthread_cond_broadcast");
}

unsigned int get_max_number_of_fds() {
#ifndef __VXWORKS__
	struct rlimit limit;
	int ret = getrlimit(RLIMIT_NOFILE, &limit);
	if(ret) {
		log("could not getrlimit() for RLIMIT_NOFILE: %s\n", strerror(errno));
		return 1024;
	}
	return limit.rlim_cur;
#else
	return 0;
#endif
}

}

#endif


