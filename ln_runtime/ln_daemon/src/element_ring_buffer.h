#ifndef ELEMENT_RING_BUFFER_H
#define ELEMENT_RING_BUFFER_H

#include <string>
#include "os.h"

using namespace std;

class element_ring_buffer {
	os::mutex_handle_t mutex_handle;

	unsigned int element_size;

	unsigned int read_element;
	unsigned int write_element;
	bool full;

	char* elements;
	unsigned int elements_size;

public:
	element_ring_buffer();
	~element_ring_buffer();

	void configure(unsigned int max_elements, unsigned int element_size);
	void append(void* element, double* element_ts, uint32_t* element_counter, bool* is_full=NULL);

	unsigned int get_count();
	unsigned int get_size();
	string get_string(unsigned int* count);

};

#endif // ELEMENT_RING_BUFFER_H
