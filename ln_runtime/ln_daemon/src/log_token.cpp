#include "os.h"
#include "ln_daemon.h"

#include "logger.h"
#include "log_token.h"

#define TOKEN_LEN 32

log_token::log_token(ln_instance* instance, std::string data)
	: instance(instance), data(data)
{
	token = generate_random_message(TOKEN_LEN, (uint8_t)'A', (uint8_t)'Z');
	token_len = 0;
	client_fd = -1;
	token_input.resize(TOKEN_LEN);
	
	fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(fd == -1)
		throw errno_exception("socket");
		

	struct sockaddr_in server_address;
	os::socklen_t_type server_address_len = sizeof(server_address);
	
#if defined(__WIN32__)
	// win32 always needs a bind"!
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(0);
	server_address.sin_addr.s_addr = INADDR_ANY;
	if(bind(fd, (struct sockaddr*)&server_address, server_address_len))
		throw errno_exception_tb("bind");
#endif
	
	if(listen(fd, 5))
		throw errno_exception_tb("listen");
	
	// create listening socket
	if(getsockname(fd, (struct sockaddr*)&server_address, &server_address_len) == -1)
		throw errno_exception("getsockname");	
	port = ntohs(server_address.sin_port);
	instance->daemon->add_io_watch(fd, IO_WATCH_READ, on_new_log_connection, this, "incoming log connection");
	
	log("log_token on port %d listening!\n", port);
}

log_token::~log_token() {
	if(client_fd != -1)
		_close_client();
	
	if(fd != -1) {
		instance->daemon->remove_io_watch(fd);
		_ln_close_socket(fd);
	}
	log("log_token on port %d destructed!\n", port);
}

void log_token::on_new_log_connection(int fd, int why, void* data)
{
	log_token* self = (log_token*) data;
	self->on_new_log_connection(fd, why);
}

void log_token::on_new_log_connection(int fd, int why) {

	if(why & IO_WATCH_EXCEPT) {
		log("exception on listening socket fd %d. closing: %d, %s", fd, errno, strerror(errno));
		// self->running = false;
		return;
	}
	
	struct sockaddr_in client_addr;
	os::socklen_t_type client_addr_len = sizeof(client_addr);
	client_fd = accept(fd, (struct sockaddr*)&client_addr, &client_addr_len);
	if(client_fd == -1)
		log("log_token error, accept() returned -1-manager filedescriptor: listen fd: %d, errno %d %s\n", fd, errno, strerror(errno));

#if defined(__WIN32__)
	// https://docs.microsoft.com/en-us/windows/win32/api/winsock/nf-winsock-ioctlsocket
	// Set the socket I/O mode: In this case FIONBIO enables or disables the blocking mode for the 
	// socket based on the numerical value of iMode.
	// If iMode = 0, blocking is enabled;
	// If iMode != 0, non-blocking mode is enabled.
	u_long mode = 1;
	int ret = ioctlsocket(client_fd, FIONBIO, &mode);
	if (ret != NO_ERROR)
		log("log_token on port %d: ioctlsocket failed with error: %ld\n", port, ret); // continue blocking?!
#else
	fcntl(client_fd, F_SETFL, O_NONBLOCK);
#endif
	
	instance->daemon->add_io_watch(client_fd, IO_WATCH_READ, _on_token_input, this, "log_token input");
	instance->daemon->remove_io_watch(fd);
	_ln_close_socket(fd);
	this->fd = -1;
}

void log_token::_close_client() {
	instance->daemon->remove_io_watch(client_fd);
	_ln_close_socket(client_fd);
	client_fd = -1;
}

void log_token::_on_token_input(int fd, int why, void* data)
{
	log_token* self = (log_token*) data;
	self->on_token_input(fd, why);
}

void log_token::on_token_input(int fd, int why)
{
	unsigned int read_max = TOKEN_LEN - token_len;
	int ret = ::recv(client_fd, &token_input[token_len], read_max, 0);
	if(ret == -1)
		throw errno_exception("log_token read");
	
	token_len += ret;
	if(token_len < TOKEN_LEN)
		return; // expect more
	// check token!
	std::string received_token(&token_input[0], TOKEN_LEN);
	if(received_token != token) {
		log("log_token on port %d received invalid token %s, expect %s!\n",
		    port, repr(received_token).c_str(), repr(token).c_str());
		_close_client();
		return;
	}
	total_written = 0;
	instance->daemon->remove_io_watch(client_fd);
	instance->daemon->add_io_watch(client_fd, IO_WATCH_WRITE, _on_data_output, this, "log_token output");	
}

void log_token::_on_data_output(int fd, int why, void* data)
{
	log_token* self = (log_token*) data;
	self->on_data_output(fd, why);
}

void log_token::on_data_output(int fd, int why)
{
	uint32_t data_size = data.size();
	
	void* src;
	unsigned int to_write = sizeof(data_size) - total_written;
	
	if(total_written < sizeof(data_size)) {
		to_write = sizeof(data_size) - total_written;
		src = (uint8_t*)&data_size + total_written;
	} else {
		unsigned int read_pos = total_written - sizeof(data_size);
		to_write = data_size - read_pos;
		src = (uint8_t*)data.c_str() + read_pos;
	}
			
	int ret = send(client_fd, (const char*)src, to_write, 0);
	if(ret == -1) {
		log("log_token on port %d got error on send: %d %s\n", port, ret, strerror(errno));
		_close_client();
		return;
	}
	total_written += ret;

	if(total_written == sizeof(data_size) + data_size) {
		shutdown(client_fd, 2);
		_close_client();
		instance->remove_log_token(token);
	}
}
