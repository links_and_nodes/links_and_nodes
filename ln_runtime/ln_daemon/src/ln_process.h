// -*- mode: cpp -*-
#ifndef LN_PROCESS
#define LN_PROCESS

#include <string>
#include <list>

#include <string_util/string_util.h>
#include "logger.h"

class ln_process;
#include "ln_instance.h"
#include "char_ringbuffer.h"
#include "linux_process_listing.h"

using namespace std;
using namespace string_util;

void output_buffer_notification(int fd, int why, void* instance);

class ln_process {
	double start_time;
	ln_instance* instance;
	bool is_ready;

	typedef list<pair<string, string> > env_t;
	env_t env;

	typedef py_dict props_t;
	props_t* props;

	thread_handle_t output_thread_tid;
	bool output_thread_running;
	bool triggered_output_buffer;
	bool output_thread_joined;

	char_ringbuffer* output_buffer;
	char_ringbuffer last_output_buffer;

	char** envp;

	// exit status
	int status;

	// exit callback
	callback_t exitcb;
	void* exitcb_arg;

	// pipes
	ln_pipe_t pipe;
	ln_pipe_t buffer_pipe;
	ln_pipe_t stdin_pipe;

#if defined(__WIN32__)
	friend class ln_daemon;
	HANDLE win32_process_handle;
#endif
	friend void output_buffer_notification(int fd, int why, void* instance);
	void after_execute(); // called after execute finished

	const char* seen_PATH;
	os::mutex_handle_t PATH_env_mutex;
	
	void _forked_child();
	void _fork_child();
	void _qnx_spawn_child();
	void _win32_create_process();
public:
	pid_t pid;

	typedef struct {
		string name;
		string cmdline;

		bool start_in_shell;
		bool no_pty;
		bool log_output;
		unsigned int buffer_size;

		string change_directory;
		float term_timeout;

		// scheduling
		int priority;
		int policy;
		int affinity;

		double max_output_frequency;

		bool is_state;

		string change_user_to;
		int stack_size;

		py_list* resource_limits;

		bool blocking_output;
		bool use_execvpe;

		unsigned int term_rows;
		unsigned int term_cols;
	} config_t;

	config_t config;
	bool no_output_rate_limit;

	ln_process(ln_instance* instance, py_list* env, config_t& config, bool autorun=true);
	~ln_process();

	//! execute/spawn process
	/*
	   \return success or error code
	   */
	int execute();

	void set_exitcb(callback_t cb, void* cb_arg) {
		exitcb = cb;
		exitcb_arg = cb_arg;
	};
	int get_exit_status() {
		return status;
	}
	unsigned int get_pid() {
		return pid;
	}
	double get_start_time() {
		return start_time;
	}

	static void send_signal(pid_t pid, int signo);

	void set_log_output(bool log_output, unsigned int buffer_size);
	void set_ready();
	void set_property(string name, py_value* value);
	void stop();

	void* output_thread();

	void output_line(string line);
	void send_output_buffer();

	void terminated(int status);

	static py_list* get_process_list(bool with_env=false, bool with_threads=true, string filter="");

	py_tuple* get_tuple(py_tuple* t=NULL);

	void send_stdin(string text);
	void send_winch(int rows, int cols);

};

#endif // LN_PROCESS

