#include "condition.h"

namespace os {

condition::condition() {
	condition_create(&handle);
}

condition::~condition() {
	condition_destroy(&handle);
}

void condition::wait(flolock& l) {
	condition_wait(&handle, &l.rwlock);
}

void condition::signal() {
	condition_signal(&handle);
}

void condition::broadcast() {
	condition_broadcast(&handle);
}

}
