#ifndef LN_TCP_FORWARD
#define LN_TCP_FORWARD

class ln_tcp_forward;
#include "ln_instance.h"

#include <string>
#include <set>
using namespace std;

class buffer {
	std::vector<uint8_t> _mem;
public:
	unsigned int size;
	int len;
	void* data;

	buffer(unsigned int size);
};

class tcp_connection;
class tcp_bridge {
	tcp_connection* forward;
	int fd;
	string name;
	buffer to_send;
	tcp_bridge* target;
	ln_daemon* daemon;

	int current_watch;

	void set_watch(int watch);
	static void _on_data(int fd, int why, void* data);
	void on_data(int why);
public:
	tcp_bridge(tcp_connection* forward, int fd, string name);
	~tcp_bridge();

	void set_target(tcp_bridge* target);
	void insert_received_data(const string& data);
};

class tcp_connection {
public:
	ln_tcp_forward* forward;
	tcp_bridge client;
	
	string client_ip;
	int client_port; // outgoing port from client towards us
	
	tcp_bridge remote;
	int remote_port; // own outgoing port towards the remote
	
	tcp_connection(ln_tcp_forward* forward, int client_fd, int remote_fd, int listen_port, bool send_magic);
	void destroy();
};

class ln_tcp_forward {
	void _create_listen_side();
public:
	ln_instance* instance;

	string to_ip;
	int to_port;
	string other_target;
	string listen_ip;
	int listen_port;

	int listen_fd;
	bool keep_listening;	
	bool is_x11_forwarding;
	bool destroyed;
	string data;
	bool send_source;
	
	typedef enum {
		TCP_FORWARD_LISTENING,
		TCP_FORWARD_CONNECTED
	} state_t;
	state_t state;

	string remote_type;
	struct sockaddr* remote_addr;
	ssize_t remote_addr_size;
	
	struct sockaddr_in remote_addr_in;
#if !defined(__WIN32__)
	struct sockaddr_un remote_addr_un;
#endif
	
	typedef set<tcp_connection*> connections_t;
	connections_t connections;

	unsigned int last_n_connections;
	time_t last_n_connections_time;

	void _init();
	ln_tcp_forward(ln_instance* instance, string to_ip, int to_port, string listen_ip="", int listen_port=-1, bool keep_listening=false, string data="", bool send_source=false);
	ln_tcp_forward(ln_instance* instance, string other_target, string listen_ip="", int listen_port=-1, bool keep_listening=false, string data="", bool send_source=false);
	
	~ln_tcp_forward();
	void destroy(bool do_delete=true);

	string get_id();

	static void _on_connection(int fd, int why, void* data);
	void on_connection();

	bool is_source_port(int port, string& from_ip, int& from_port);
	py_tuple* get_tuple(unsigned int protocol=23);

	void notify_manager();
};

#endif // LN_TCP_FORWARD
