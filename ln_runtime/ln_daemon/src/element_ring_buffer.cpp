/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include "os.h"

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "element_ring_buffer.h"

#include <vector>
#include <string>

element_ring_buffer::element_ring_buffer() {
	elements = NULL;
	elements_size = 0;
	element_size = 0;
	write_element = 0;
	full = false;
	os::create_mutex(&mutex_handle);
}
element_ring_buffer::~element_ring_buffer() {
	os::destroy_mutex(&mutex_handle);
	free(elements);
}

void element_ring_buffer::configure(unsigned int max_elements, unsigned int element_size) {
	os::lock_mutex(&mutex_handle);

	element_size += sizeof(uint32_t) + sizeof(double) * 2;
	
	if(this->element_size != element_size || this->elements_size != max_elements) {
		void* new_mem = realloc(elements, max_elements * element_size);
		if(new_mem) {
			elements = (char*)new_mem;
			this->element_size = element_size;
			this->elements_size = max_elements;
		}
	}
	write_element = 0;
	full = false;

	os::unlock_mutex(&mutex_handle);
}

void element_ring_buffer::append(void* element, double* element_ts, uint32_t* element_counter, bool* is_full) {
	//printf("now buffer append\n");
	if(os::trylock_mutex(&mutex_handle)) {
		// printf("could not get logging mutex.... ignore...\n");
		return; // could not get lock!
	}

	double ts = ln_get_time();
	memcpy(elements + element_size * write_element, &ts, sizeof(double));
	memcpy(elements + element_size * write_element + sizeof(double), element_ts, sizeof(double));
	memcpy(elements + element_size * write_element + sizeof(double) * 2, element_counter, sizeof(uint32_t));
	uint32_t to_read = element_size - sizeof(double)*2 - sizeof(uint32_t);
	if(to_read)
		memcpy(elements + element_size * write_element + sizeof(double) * 2 + sizeof(uint32_t), element, to_read);
	write_element++;

	if(write_element == this->elements_size) {
		full = true;
		write_element = 0;
	}

	if(is_full)
		*is_full = full;
	os::unlock_mutex(&mutex_handle);
}

unsigned int element_ring_buffer::get_count() {
	unsigned int n;
	os::lock_mutex(&mutex_handle);
	if(!full)
		n = write_element;
	else
		n = elements_size;
	os::unlock_mutex(&mutex_handle);
	return n;
}

unsigned int element_ring_buffer::get_size() {
	return elements_size;
}

string element_ring_buffer::get_string(unsigned int* count) {
	if(!elements) {
		if(count)
			*count = 0;
		return "";
	}
	os::lock_mutex(&mutex_handle);

	unsigned int n_elements;
	if(full)
		n_elements = elements_size;
	else
		n_elements = write_element;
	
	if(count)
		*count = n_elements;
	
	std::vector<char> data(n_elements * element_size);
	char* dp = &data[0];	

	if(full) {
		// append from [write_element to the end]
		unsigned int n_bytes = (elements_size - write_element) * element_size;
		memcpy(dp, elements + element_size * write_element, n_bytes);
		dp += n_bytes;
	}
	
	// append from [0 to write_element)
	if(write_element)
		memcpy(dp, elements, write_element * element_size);
	
	os::unlock_mutex(&mutex_handle);
	return std::string(&data[0], n_elements * element_size);
}
