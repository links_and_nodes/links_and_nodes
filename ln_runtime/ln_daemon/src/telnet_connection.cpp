/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include "os.h"

#include <sstream>
#include <stdio.h>
#include <string_util/string_util.h>

#include "telnet_connection.h"

using namespace os;
using namespace string_util;

telnet_connection::telnet_connection(string host, int port) {
	struct sockaddr_in sa;

	fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(fd == -1)
		throw errno_exception_tb("socket");
	
#ifdef __WIN32__
	sa.sin_addr.s_addr = inet_addr(host.c_str());
#else	
	inet_aton(host.c_str(), &sa.sin_addr);
#endif
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	
	if(connect(fd, (struct sockaddr*)&sa, sizeof(sa)))
		throw errno_exception_tb("connect");
#ifndef __WIN32__	
	fcntl(fd, F_SETFL, O_NONBLOCK);
#endif
	iaclen = 0;
}
telnet_connection::~telnet_connection() {
	closesocket(fd);
}

void telnet_connection::sendall(const char* data, uint32_t len) {
	unsigned int offset = 0;
	while(true) {
		int n = send(fd, data + offset, len - offset, 0);
		if(n == -1)
			throw errno_exception_tb("send");
		if((n + offset) == len)
			break;
		offset += n;
	}
}
void telnet_connection::write(string data) {
	output.str("");
	sendall(data.c_str(), data.size());
}

#define IAC  255

#define WILL 251 // 0373
#define DO   253 // 0375
#define DONT 254
#define WONT 252

string telnet_connection::process_raw(stringstream& ret, const char* data, unsigned int data_len) {
	for(unsigned int i = 0; i < data_len; i++) {
		uint8_t c = (uint8_t)data[i];
		
		if(!iaclen) {
			if(c == 0)
				continue;
			if(c == '\021')
				continue;
			if(c != IAC) {
				ret.write((char*)&c, 1);
				continue;
			}
			iacseq[iaclen++] = c;
			continue;
		}
		if(iaclen == 1) {
			// 'IAC: IAC CMD [OPTION only for WILL/WONT/DO/DONT]'
			if(c == DO || c == DONT || c == WILL || c == WONT) {
				iacseq[iaclen++] = c;
				continue;
			}
			iaclen = 0;
			printf("warning - size1 strange\n");
			continue;
		}
		if(iaclen == 2) {
			uint8_t cmd = iacseq[1];
			iaclen = 0;
			uint8_t opt = c;
			if (cmd == DO || cmd == DONT) {
				// printf("IAC DO/DONT %d %d\n", cmd, opt);
				char wont[3];
				wont[0] = (char)IAC;
				wont[1] = (char)WONT;
				wont[2] = (char)opt;
				sendall(wont, 3);
			} else if(cmd == WILL || cmd == WONT) {
				// printf("IAC WILL/WONT %d %d\n", cmd, opt);
				char wont[3];
				wont[0] = (char)IAC;
				wont[1] = (char)DONT;
				wont[2] = (char)opt;
				sendall(wont, 3);
			}
		}
	}

	return ret.str();	
}

string telnet_connection::read(double timeout, bool once) {
	stringstream data;
	fd_set read_fds, except_fds;
	while(true) {
		FD_ZERO(&read_fds);
		FD_ZERO(&except_fds);
		
		FD_SET(fd, &read_fds);
		FD_SET(fd, &except_fds);
		struct timeval tv;
		struct timeval* tvp;
		if(timeout > 0) {
			tv.tv_sec = (int)(timeout);
			tv.tv_usec = (unsigned int)(timeout * 1e6 - tv.tv_sec * 1e6);
			tvp = &tv;
		} else
			tvp = NULL;
			
		int n = select(fd + 1, &read_fds, NULL, &except_fds, tvp);
		// printf("select n: %d\n", n);

		if(n == -1)
			throw errno_exception_tb("select");
		
		if(n == 0)
			return data.str(); // done

		char buffer[1024];
		n = recv(fd, buffer, 1024, 0);
		if(n == -1)
			throw errno_exception_tb("recv");

		if(n == 0) {
			if(data.str().size())
				return data.str();
			throw str_exception_tb("recv EOF");
		}
		process_raw(data, buffer, n);

		if(timeout <= 0 || once)
			return data.str();

	}
}

bool telnet_connection::is_prompt(string data) {
	if(data.find("[vxWorks") != string::npos)
		return true;
	return false;
}

string telnet_connection::wait_prompt() {
	string data;
	while(true) {
		data = read(0, true);
		output.write(data.c_str(), data.size());
		if(is_prompt(data))
			break;
	}
	return data;
}
