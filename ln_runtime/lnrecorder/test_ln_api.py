#!/usr/bin/python

import os
import sys
import time
import links_and_nodes as ln

lnrecorder_python_path = os.path.join(os.path.dirname(__file__), "python")
if lnrecorder_python_path not in sys.path:
    sys.path.insert(0, lnrecorder_python_path)
from lnrecorder import *

clnt = ln.client("test_ln_api", sys.argv)

recorder_name = sys.argv[1]
rec = lnrecorder_service_wrapper(clnt, recorder_name)

print "start recording..."
rec.record("/tmp/test.lnrecord",
           #patterns=["*/1"])
)
print "recording..."

time.sleep(2)

print "done recording."
rec.stop()
