#!/usr/bin/python

import random
import links_and_nodes as ln
import sys
import time

clnt = ln.client(sys.argv[0], sys.argv)

port = clnt.publish("test.topic/2", "lnrecorder_test/topic2")

print "ready"

rate = 60
port.packet.count = 0
while True:
    port.packet.count += 1
    port.packet.time = time.time()
    
    port.packet.depth[3] = time.time()
    
    port.write()
    time.sleep(1. / rate)
    
