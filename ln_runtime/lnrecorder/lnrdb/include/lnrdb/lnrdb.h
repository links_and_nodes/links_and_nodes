#ifndef LNRDB_H
#define LNRDB_H

#include <stddef.h>
#include <stdint.h>
#include <string>
#include <memory>

#ifndef array_length
#  define array_length(a) (sizeof((a)) / sizeof((a)[0]))
#endif

namespace lnrdb {

extern const unsigned int version;


struct record_item {
	void* data;
	size_t len;
};

const uint32_t variable = 0xffffffff;

struct field_description_item;
typedef std::vector<field_description_item> field_description;
struct field_description_item {
	std::string name;
	std::string type_desc; // or "subtype"
	field_description subtype;
	uint32_t n;

	field_description_item() {
	}
	field_description_item(
		const std::string& name,
		const std::string& type_desc,
		uint32_t n=1
		) :
		name(name), type_desc(type_desc), n(n) {
	}
	field_description_item(
		const std::string& name,
		const field_description& subtype,
		uint32_t n=1
		) :
		name(name), type_desc("subtype"), subtype(subtype), n(n) {
	}
};
field_description get_field_description_from_msg_def(const std::string& msg_def, const std::string& req_resp="fields");
std::string to_string(const field_description& fd);
bool is_fixed(const field_description& fd);
uint32_t get_size(const field_description& fd);
uint32_t get_item_size(const field_description_item& field);

class meta_info {
public:
	virtual ~meta_info() {};
	virtual void add(std::string name, const char* format, void* data) = 0;
	virtual void add(std::string name, std::string value) = 0;
	virtual void add(std::string name, unsigned int value) = 0;

	virtual std::string get(std::string name) = 0;
	virtual std::string get_string(std::string name) = 0;
	virtual unsigned int get_unsigned_int(std::string name) = 0;
	virtual double get_double(std::string name) = 0;	
};

class table {
protected:
	table() {};
public:
	std::unique_ptr<meta_info> meta;
	
	virtual ~table() {};
	virtual void set_fixed_type(uint32_t record_size) = 0;
	virtual void set_fixed_type(const field_description& fd) = 0;
	
	virtual uint64_t add_record(record_item* items, unsigned int n_items) = 0;
	virtual uint64_t add_record(record_item** items, unsigned int* n_items) = 0;
	virtual void update_record(uint64_t record, uint32_t offset, record_item* items, unsigned int n_items) = 0;
	virtual uint32_t get_field_offset(std::string field) = 0;

	// for reading:	
	virtual bool read_record(record_item* items, unsigned int n_items, uint64_t* record=NULL) = 0;
	virtual uint64_t get_n_records() = 0;
	virtual void seek_record(uint64_t record) = 0;
	
	virtual void close() = 0;
};

class db {
protected:
	db() {};
public:
	std::unique_ptr<meta_info> meta;
	
	virtual ~db() {};
	virtual table* create_table(std::string name)  = 0;

	// for reading:
	virtual std::list<std::string> get_table_names() = 0;
	virtual table* open_table(std::string name) = 0;
};

db* create_raw(std::string name);
db* open_for_reading_raw(std::string name);
#if __cplusplus >= 201103L
std::unique_ptr<db> create(std::string name);
std::unique_ptr<db> open_for_reading(std::string name);
#endif

bool is_file(std::string fn);
bool is_dir(std::string dn);
void mkdir(std::string dir);

}

#endif /* LNRDB_H */

