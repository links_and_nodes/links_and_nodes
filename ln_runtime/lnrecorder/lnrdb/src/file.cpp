#include <stdio.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <string_util/string_util.h>

#include "file.h"

namespace lnrdb {

file::file(std::string fn, const char* mode)
	: name(fn)
{
	fp = fopen(fn.c_str(), mode);
	if(!fp)
		throw errno_exception_tb("could not open file '%s' mode '%s'", fn.c_str(), mode);
	for_reading = strchr(mode, 'r') != NULL;
}

file::~file()
{
	fclose(fp);
}

void file::write(const format& fmt)
{
	fmt.write(this);
}
void file::write(const std::string& s)
{
	write(s.c_str(), s.size());
}
void file::write(const std::vector<std::string>& v)
{
	for(unsigned int i = 0; i < v.size(); i++)
		write(v[i]);
}

void file::write(const void* data, size_t len)
{
	if(len == 0)
		return;
	
	int ret = fwrite(data, len, 1, fp);
	if(ret != 1)
		throw errno_exception_tb("could not write %u bytes of data, ret: %d", (unsigned int)len, ret);
}

void file::flush()
{
	fflush(fp);
}

void file::seek(uint64_t offset)
{
	int ret = fseeko(fp, offset, SEEK_SET);
	if(ret != 0)
		throw errno_exception_tb("could not seek to pos %" PRIu64 " of file!", offset);
}

void file::seek_end()
{
	int ret = fseeko(fp, 0, SEEK_END);
	if(ret != 0)
		throw errno_exception_tb("could not seek to end of file!");
}

uint64_t file::get_file_size()
{
	struct stat sbuf;
	int ret = fstat(fileno(fp), &sbuf);
	if(ret == -1)
		throw errno_exception_tb("could not stat file %s", string_util::repr(name).c_str());
	return sbuf.st_size;
}

bool file::read(void* data, size_t len)
{
	size_t n = fread(data, len, 1, fp);
	if(n == 1)
		return false;
	// eof or error
	if(feof(fp))
		return true;
	throw errno_exception_tb("fread error");
}

std::string file::read_all()
{
	std::stringstream ss;
	while(true) {
		char page[4096];

		size_t n = fread(page, 1, sizeof(page), fp);
		if(n == 0) {
			if(feof(fp))
				break;
			throw errno_exception_tb("fread error");
		}
		ss.write(page, n);
	}
	return ss.str();
}

}
