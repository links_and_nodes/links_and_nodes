#include <string>

#include <string_util/string_util.h>

#include "meta_info_impl.h"

namespace lnrdb {

meta_info_impl::meta_info_impl(std::unique_ptr<file> _file)
{
	this->_file = std::move(_file);
	if(this->_file->for_reading)
		_read();
}

void meta_info_impl::_read()
{
	for(std::string& line : string_util::split_string(_file->read_all(), "\n")) {
		std::vector<std::string> parts = string_util::split_string(line, ": ", 1);
		if(parts.size() != 2)
			printf("error: invalid meta-line in %s: %s\n", _file->name.c_str(), string_util::repr(line).c_str());
		else
			data[parts[0]] = string_util::strip(parts[1]);
	}
}

void meta_info_impl::add(std::string name, const char* fmt, void* data_ptr)
{
	if(data.find(name) != data.end())
		throw str_exception_tb("meta %s for in %s already specified!",
				       string_util::repr(name).c_str(),
				       string_util::repr(_file->name).c_str());
	
	format meta_data(fmt, data_ptr);
	std::string meta_data_pyrepr = meta_data.tostring();
	_file->write({ name, ": ", meta_data_pyrepr, "\n" });
	_file->flush();
	data[name] = meta_data_pyrepr;
}

void meta_info_impl::add(std::string name, unsigned int value)
{
	std::string meta_data_pyrepr = std::to_string(value);
	_file->write({ name, ": ", meta_data_pyrepr, "\n" });
	_file->flush();
	data[name] = meta_data_pyrepr;
}

void meta_info_impl::add(std::string name, std::string value)
{
	add(name, "s", &value);
}

// for reading:
std::string meta_info_impl::get(std::string name)
{
	data_t::iterator i = data.find(name);
	if(i == data.end())
		throw str_exception_tb("meta %s does not contain meta key %s!", _file->name.c_str(), name.c_str());
	return i->second;
}

std::string meta_info_impl::get_string(std::string name)
{
	std::string pyrepr = get(name);
	try {
		return string_util::eval(pyrepr);
	}
	catch(const std::exception& e) {
		throw str_exception_tb("can not convert %s value %s to string: %s",
				       name.c_str(), string_util::repr(pyrepr).c_str(), e.what());
	}
}

unsigned int meta_info_impl::get_unsigned_int(std::string name)
{
	std::string value = get(name);
	try {
		return std::stoul(value);
	}
	catch(const std::exception& e) {
		throw str_exception_tb("can not convert %s value %s to unsigned int: %s",
				       name.c_str(), string_util::repr(value).c_str(), e.what());
	}
}

double meta_info_impl::get_double(std::string name)
{
	std::string value = get(name);
	try {
		return std::stod(value);
	}
	catch(const std::exception& e) {
		throw str_exception_tb("can not convert %s value %s to double: %s",
				       name.c_str(), string_util::repr(value).c_str(), e.what());
	}
}

}
