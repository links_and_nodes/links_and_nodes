#ifndef FORMAT_H
#define FORMAT_H

#include <string>

namespace lnrdb {

class file;

class format {
	std::string fmt;
	void* data = nullptr;
	bool own_data = false;
public:
	format() { }
	format(const char* fmt, void* data)
		: fmt(fmt), data(data) {
	}
	~format();
	
	size_t get_size() const;
	void write(file* fp) const;

	std::string tostring();
	template<typename T>
	const T& get_as() {
		return *(T*)data;
	}
};

}

#endif /* FORMAT_H */

