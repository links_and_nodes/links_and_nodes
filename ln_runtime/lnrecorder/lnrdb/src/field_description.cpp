#include <string>
#include <memory>
#include <map>

#include <string_util/string_util.h>

#include <lnrdb/lnrdb.h>

using namespace string_util;

namespace lnrdb {

// keep in sync with python/links_and_nodes_base/message_definitions.py data_type_list
static std::map<std::string, std::string> lntype_map {
	{ "float", "f4" },  { "float32_t", "f4" },
	{ "double", "f8" }, { "float64_t", "f8" },

	{ "uint8_t", "u1" },
	{ "uint16_t", "u2" },
	{ "uint32_t", "u4" },
	{ "uint64_t", "u8" },
    
	{ "int8_t", "i1" },
	{ "int16_t", "i2" }, { "short", "i2" },
	{ "int32_t", "i4" }, { "int", "i4" },
	{ "int64_t", "i8" },
	
	{ "char", "u1" },
	{ "int", "i4" },
};

static std::map<std::string, unsigned int> type_desc_size {
	{ "f4", 4 },
	{ "f8", 8 },
    
	{ "u1", 1 },
	{ "u2", 2 },
	{ "u4", 4 },
	{ "u8", 8 },
    
	{ "i1", 1 },
	{ "i2", 2 },
	{ "i4", 4 },
	{ "i8", 8 },
	
	{ "u1", 1 },
	{ "i4", 4 },
};


typedef std::map<std::string, py_value*> defines_t;
static field_description _get_field_description(py_value* fields_, const defines_t& defines)
{
	field_description ret;
	auto fields = dynamic_cast<py_list*>(fields_);
	if(!fields)
		throw str_exception_tb("msg-def dict fields item has non-list type: %s", repr(fields_).c_str());
	
	for(auto& field_ : fields->value) {
		field_description_item fdi;
		auto field = dynamic_cast<py_list*>(field_);
		if(!field)
			throw str_exception_tb("msg-def field item has non-list type: %s", repr(field_).c_str());
		if(field->value.size() != 3)
			throw str_exception_tb("msg-def field item list is not length 3: %s", repr(field).c_str());
		
		auto&& fit = field->value.begin();
		
		auto str_field = dynamic_cast<py_string*>(*(fit++));
		if(!str_field)
			throw str_exception_tb("msg-def field item list item0 is not of type string: %s", repr(field).c_str());
		std::string type_name = *str_field;
		bool is_pointer = type_name.substr(type_name.size() - 1) == "*";
		if(is_pointer)
			type_name = type_name.substr(0, type_name.size() - 1);
		
		str_field = dynamic_cast<py_string*>(*(fit++));
		if(!str_field)
			throw str_exception_tb("msg-def field item list item1 is not of type string: %s", repr(field).c_str());
		fdi.name = str_field->value;

		if(is_pointer) {
			fdi.n = variable;
			if(ret.size() >= 1 and ret.back().name == (fdi.name + "_len")) // remove length field before
				ret.pop_back();
		} else {
			auto n_ = dynamic_cast<py_int*>(*(fit++));
			if(!n_)
				throw str_exception_tb("msg-def field item list item2 is not of type int: %s", repr(field).c_str());
			fdi.n = *n_;
		}

		auto&& defines_iter = defines.find(type_name);
		if(defines_iter != defines.end()) {
			fdi.type_desc = "subtype";
			try {
				fdi.subtype = _get_field_description(defines_iter->second, defines);
			}
			catch(const std::exception& e) {
				throw str_exception_tb("while reading field %s subtype description %s:\n",
						       repr(fdi.name).c_str(),
						       repr(type_name).c_str(),
						       e.what());
			}
		} else {
			auto&& tdi = lntype_map.find(type_name);
			if(tdi == lntype_map.end())
				throw str_exception_tb("msg-def field has unknown lntype: %s", repr(field).c_str());
			fdi.type_desc = tdi->second;
		}
		ret.push_back(fdi);
	}
	return ret;

}

field_description get_field_description_from_msg_def(const std::string& msg_def_str, const std::string& req_resp)
{
	auto msg_def = std::unique_ptr<py_value>(eval_full(msg_def_str));
	auto msg_def_dict = dynamic_cast<py_dict*>(msg_def.get());
	if(!msg_def_dict)
		throw str_exception_tb("msg-def has non-dict type!");

	auto defines_ = msg_def_dict->value["defines"];
	defines_t defines;
	if(defines_) {
		auto pydefines = dynamic_cast<py_dict*>(defines_);
		if(!pydefines)
			throw str_exception_tb("msg-def dict defines item has non-dict type: %s", repr(defines_).c_str());
		defines = pydefines->value;
	}
	
	auto fields = msg_def_dict->value[req_resp];
	if(!fields)
		throw str_exception_tb("msg-def dict item fields not found!");

	return _get_field_description(fields, defines);
}

std::string to_string(const field_description& fd)
{
	std::stringstream ret;
	ret << "[";
	bool first = true;
	std::string field_before = "";
	for(auto&& field : fd) {
		if(first)
			first = false;
		else
			ret << ", ";

		if(field.n == variable) {
			// offset and length instead!

			if(field_before != (field.name + "_len")) {
				ret << "[";
				ret << repr(field.name + "_len");
				ret << ", ";
				ret << repr("u4");
				ret << "], ";
			}
			ret << "[";
			ret << repr(field.name + "_offset");
			ret << ", ";
			ret << repr("u8");
			ret << "]";

			field_before = field.name + "_offset";
			continue;
		}
		
		ret << "[";
		ret << repr(field.name);
		ret << ", ";
		if(field.type_desc == "subtype")
			ret << to_string(field.subtype);
		else
			ret << repr(field.type_desc);
		if(field.n > 1) {
			ret << ", ";
			ret << std::to_string(field.n);
		}
		ret << "]";
		field_before = field.name;
	}
	ret << "]";
	return ret.str();
}

uint32_t get_item_size(const field_description_item& field)
{
	if(field.type_desc == "subtype")
		return get_size(field.subtype);
		
	auto&& i = type_desc_size.find(field.type_desc);
	if(i == type_desc_size.end())
		throw str_exception_tb("field %s type_desc %s is unknown!", repr(field.name).c_str(), repr(field.type_desc).c_str());
	return i->second;
}
uint32_t get_size(const field_description& fd)
{
	uint32_t ret = 0;
	for(auto&& field : fd) {
		if(field.n == variable) {
			// will have uint64_t offset and uint32_t repetition
			ret += 4 + 8;
			continue;
		}

		ret += get_item_size(field) * field.n;
	}
	return ret;
}

bool is_fixed(const field_description& fd)
{
	for(auto&& field : fd) {
		if(field.n == variable)
			return false;
		
		if(field.type_desc == "subtype")
			if(!is_fixed(field.subtype))
				return false;
	}
	return true;
}


}
