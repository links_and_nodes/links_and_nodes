#include <stdio.h>
#include <stdlib.h>

#include <string_util/string_util.h>

#include "file.h"
#include "format.h"

namespace lnrdb {

format::~format()
{
	if(own_data)
		free(data);
}

size_t format::get_size() const
{
	if(fmt == "s")
		return ((std::string*)data)->size();
	if(fmt == "r")
		return ((std::string*)data)->size();	
	if(fmt.substr(0, 1) == "u" || fmt.substr(0, 1) == "i") {
		// unsigned or signed
		return atoi(fmt.c_str() + 1);
	} else if(fmt == "d")
		return sizeof(double);
	else if(fmt == "f")
		return sizeof(float);
	else
		throw str_exception_tb("format %s is not implemented!", string_util::repr(fmt).c_str());
}

void format::write(file* fp) const
{
	// first \0-terminated format string
	fp->write(fmt.c_str(), fmt.size() + 1);

	// then optionally length and data
	if(fmt == "s" || fmt == "s") {
		std::string& s = *(std::string*)data;
		uint32_t slen = s.size();
		fp->write(&slen, sizeof(slen));
		fp->write(s.c_str(), slen);
	} else if(fmt.substr(0, 1) == "u" || fmt.substr(0, 1) == "i") {
		// unsigned or signed
		fp->write(data, get_size());
	} else if(fmt == "d" || fmt == "f") {
		fp->write(data, get_size());
	} else
		throw str_exception_tb("format %s is not implemented!", string_util::repr(fmt).c_str());
}

std::string format::tostring()
{
	if(fmt == "s")
		return string_util::repr(*(std::string*)data, true);
	if(fmt == "r")
		return *(std::string*)data;
	if(fmt == "u1")
		return string_util::format_string("%u", *(uint8_t*)data);
	if(fmt == "u2")
		return string_util::format_string("%u", *(uint16_t*)data);
	if(fmt == "u4")
		return string_util::format_string("%u", *(uint32_t*)data);
	if(fmt == "i1")
		return string_util::format_string("%d", *(int8_t*)data);
	if(fmt == "i2")
		return string_util::format_string("%d", *(int16_t*)data);
	if(fmt == "i4")
		return string_util::format_string("%d", *(int32_t*)data);
	if(fmt == "f")
		return string_util::format_string("%f", *(float*)data);
	if(fmt == "d")
		return string_util::format_string("%f", *(double*)data);
	else
		throw str_exception_tb("format %s is not implemented!", string_util::repr(fmt).c_str());
}

}
