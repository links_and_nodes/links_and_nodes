#ifndef BASE_DIR_H
#define BASE_DIR_H

#include <string>
#include <memory>

#include "file.h"

namespace lnrdb {

class base_dir {
protected:
	std::string dir_name;

	base_dir() {
	}
	void set_base_dir(std::string base_dir) {
		this->dir_name = base_dir;
	}
public:
	base_dir(std::string base_dir)
		: dir_name(base_dir) {
	}
	
	std::string dir_entry(std::string entry, bool canonicalize=true);
	std::vector<std::string> list(bool dirs=true, bool files=false);
	
	std::unique_ptr<file> open_for_writing(std::string entry);
	std::unique_ptr<file> open_for_reading(std::string entry);
	std::string get_base_dir_basename();
};

}

#endif /* BASE_DIR_H */
