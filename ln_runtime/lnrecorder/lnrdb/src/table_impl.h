#ifndef TABLE_IMPL_H
#define TABLE_IMPL_H

#include <lnrdb/lnrdb.h>

#include <stdint.h>

#include <string>
#include <map>
#include <set>
#include <memory>

#include "base_dir.h"

namespace lnrdb {

class db_impl;
class table_impl;

class write_instruction {
public:
	enum action_t {
		copy, // only uses elemsize
		to_subtable
	} action;

	std::string field;
	uint32_t elemsize;
	std::shared_ptr<table_impl> subtable;

	write_instruction(
		action_t action, std::string field, uint32_t elemsize,
		std::shared_ptr<table_impl> subtable=std::shared_ptr<table_impl>())
		: action(action), field(field), elemsize(elemsize), subtable(subtable) {}
	
	void print();
};


class table_impl
	: public table,
	  public base_dir
{
	db_impl* db;
	base_dir* parent;
	bool marked = false;
	
	std::unique_ptr<file> data_file;

	field_description fd;
	uint64_t next_record_id = 0; // when opened for reading: record-id of next record to read
	bool only_fixed_fields = true;
	
	bool last_is_copy = false;
	unsigned int field_idx = 0;
	std::vector<write_instruction> write_instructions;
	void _generate_write_instructions(const field_description& fd, std::string prefix="");
	
	uint32_t record_size;
	
	friend class db_impl;
	void create(std::string name);
	void open(std::string table_dir);
	table_impl(db_impl* db, base_dir* parent=nullptr);

	void _copy_ins(record_item*& itemp, unsigned int& items_left, uint64_t to_copy);
	void _subtable_ins(record_item*& itemp, unsigned int& items_left, std::shared_ptr<table_impl> subtable);

	bool is_data_at_end = true;
	void _seek_data_end();
	void _seek_data(uint64_t offset);

	uint32_t current_field_offset;
	std::map<std::string, uint32_t> field_offsets;

public:	
	std::string name;
	
	~table_impl();
	
	void set_fixed_type(uint32_t record_size);
	void set_fixed_type(const field_description& fd);
	
	uint64_t add_record(record_item* items, unsigned int n_items);
	uint64_t add_record(record_item** items, unsigned int* n_items);
	uint64_t add_multiple_records(record_item** items, unsigned int* n_items, uint32_t len);
	uint32_t get_field_offset(std::string field);
	void update_record(uint64_t record, uint32_t offset, record_item* items, unsigned int n_items);
	
	// for reading:
	bool read_record(record_item* items, unsigned int n_items, uint64_t* record=NULL);
	uint64_t get_n_records();
	void seek_record(uint64_t record);
	
	void close();
};

}


#endif /* TABLE_IMPL_H */

