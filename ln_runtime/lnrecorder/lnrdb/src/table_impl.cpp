#include <stdio.h>

#include <string_util/string_util.h>

#include "db_impl.h"
#include "format.h"
#include "table_impl.h"
#include "meta_info_impl.h"

namespace lnrdb {

table_impl::table_impl(db_impl* db, base_dir* parent)
	: db(db)
{
	if(!parent)
		this->parent = db;
	else
		this->parent = parent;
}

table_impl::~table_impl()
{
}

void table_impl::create(std::string name)
{
	std::string table_dir = parent->dir_entry(name);
	mkdir(table_dir);
	set_base_dir(table_dir);
	this->name = name;

	data_file = open_for_writing("data");

	meta.reset(new meta_info_impl(open_for_writing("meta")));
	meta->add("name", name);

	uint16_t bom = 0x1002;
	std::string endianess;
	if(((uint8_t*)&bom)[0] == 0x02)
		endianess = "little";
	else
		endianess = "big";
	meta->add("endianess", endianess);
}

void table_impl::open(std::string dir_entry)
{
	std::string table_dir = parent->dir_entry(dir_entry, false);
	set_base_dir(table_dir);

	meta.reset(new meta_info_impl(open_for_reading("meta")));
	
	name = meta->get_string("name");

	if(meta->get_string("table-format") != "fixed")
		throw str_exception_tb("currently only support table-format fixed, not %s", meta->get("table-format").c_str());
	record_size = meta->get_unsigned_int("fixed-table-record-size");
	only_fixed_fields = true; // todo: asssumption?!
	// todo: read field description?
	
	uint16_t bom = 0x1002;
	std::string endianess;
	if(((uint8_t*)&bom)[0] == 0x02)
		endianess = "little";
	else
		endianess = "big";
	if(meta->get_string("endianess") != endianess)
		printf("warning: database table %s was written with different endianess!", table_dir.c_str());
	
	data_file = open_for_reading("data");
	next_record_id = 0;
}

void table_impl::set_fixed_type(uint32_t record_size)
{
	meta->add("table-format", std::string("fixed"));
	meta->add("fixed-table-record-size", "u4", &record_size);
	only_fixed_fields = true;
}

uint32_t table_impl::get_field_offset(std::string field)
{
	auto&& iter = field_offsets.find(field);
	if(iter == field_offsets.end())
		throw str_exception_tb("unknown table field %s!", string_util::repr(field).c_str());
	return iter->second;
}

void table_impl::set_fixed_type(const field_description& fd)
{
	meta->add("table-format", std::string("fixed"));
	std::string fds = to_string(fd);
	meta->add("field-description", "r", &fds);
	record_size = get_size(fd);
	meta->add("fixed-table-record-size", "u4", &record_size);
	only_fixed_fields = is_fixed(fd);
	this->fd = fd;

	// generate write_instructions and fill field offsets
	current_field_offset = 0;
	_generate_write_instructions(fd);

	auto field_subtables = new string_util::py_list();
	for(auto&& ins : write_instructions) {
		if(ins.action != write_instruction::to_subtable)
			continue;
		auto subdir = ins.subtable->get_base_dir_basename();
		auto tuple = new string_util::py_list();
		tuple->value.push_back(new string_util::py_string(ins.field));
		tuple->value.push_back(new string_util::py_string(subdir));
		field_subtables->value.push_back(tuple);
	}
	std::string field_subtables_repr = string_util::repr(field_subtables);
	meta->add("field-subtables", "r", &field_subtables_repr);
	delete field_subtables;

	/*
	if(marked) {
		for(auto&& ins : write_instructions)
			ins.print();
	}
	*/

	if(!write_instructions.size())
		throw str_exception_tb("empty write instructions not allowed!");
	if(only_fixed_fields && !(write_instructions.size() == 1 && write_instructions.front().action == write_instruction::copy))
		throw str_exception_tb("fixed fields table should only have a single copy write instruction!");
}

void write_instruction::print() {
	if(action == copy) {
		printf("  copy %d bytes for fields %s\n", elemsize, field.c_str());
		return;
	}
	printf("  put variable number of %d-bytes-each to subtable %s for field %s\n", elemsize, subtable->name.c_str(), field.c_str());
}

void table_impl::_generate_write_instructions(const field_description& fd, std::string prefix)
{
	for(auto&& field : fd) {

		std::string full_field_name;
		if(prefix.size()) {
			full_field_name = prefix;
			full_field_name += "/";
			full_field_name += field.name;
		} else
			full_field_name = field.name;
		field_offsets[full_field_name] = current_field_offset;
		
		auto elem_size = get_item_size(field);
		if(field.n == variable) {
			std::shared_ptr<table_impl> subtable(new table_impl(db, this));
			std::string subtable_name = "sub_" + string_util::string_replace(full_field_name, "/", "_");
			subtable->create(subtable_name);
			subtable->meta->add("parent_table", "s", &name);
			subtable->meta->add("field_in_parent_table", "s", (void*)&full_field_name);
			if(field.type_desc == "subtype") {
				subtable->set_fixed_type(field.subtype);
			} else {
				lnrdb::field_description subfd { field_description_item("data", field.type_desc) };
				subtable->set_fixed_type(subfd);
			}
			/*
			write_instruction wi { write_instruction::to_subtable, field_idx++, 0, elem_size, subtable };
			write_instructions.push_back(wi);
			*/
			write_instructions.push_back(
				write_instruction(
					write_instruction::to_subtable, full_field_name, elem_size, subtable));
			last_is_copy = false;
			current_field_offset += 4 + 8;
		} else {
			if(field.type_desc == "subtype") {
				for(unsigned int i = 0; i < field.n; i++)
					_generate_write_instructions(field.subtype, field.name);
			} else {
				// simple copy
				if(!last_is_copy) {
					write_instructions.push_back(
						write_instruction(
							write_instruction::copy, field.name, elem_size * field.n));
					last_is_copy = true;
				} else {
					write_instructions.back().field += ",";
					write_instructions.back().field += field.name;
					write_instructions.back().elemsize += elem_size * field.n;
				}
				current_field_offset += elem_size * field.n;
			}
		}
	}
}

uint64_t table_impl::add_record(record_item* items, unsigned int n_items)
{
	// no feedback to caller about any unused n_items!
	return add_record(&items, &n_items);
}

uint64_t table_impl::add_record(record_item** items, unsigned int* n_items)
{
	// items & n_items has to be interpreted only as (concatenated) byte-stream! nothing more!
	// variable length fields need an uint32_t before describing the multiplicity!
	return add_multiple_records(items, n_items, 1);
}

void table_impl::_copy_ins(record_item*& itemp, unsigned int& items_left, uint64_t to_copy)
{
	while(to_copy > 0) {
		if(items_left == 0)
			throw str_exception_tb("not enough data in items for copy instruction!");
		uint32_t take = (uint32_t)to_copy; // truncate
		if(take > itemp->len)
			take = itemp->len;
		to_copy -= take;
		data_file->write(itemp->data, take);
		if(take == itemp->len) {
			itemp ++;
			items_left --;
		} else {
			itemp->data = (uint8_t*)itemp->data + take;
			itemp->len -= take;
		}
	}
}

void table_impl::_subtable_ins(record_item*& itemp, unsigned int& items_left, std::shared_ptr<table_impl> subtable)
{					
	if(itemp->len < sizeof(uint32_t))
		throw str_exception_tb("expect variable length field count to be complete within one item! (%p itemp->len %d)",
				       itemp, itemp->len);
	uint32_t* lenp = (uint32_t*)itemp->data;
	uint32_t len = *lenp;
	itemp->len -= sizeof(uint32_t);
	if(itemp->len > 0)
		itemp->data = (uint8_t*)itemp->data + sizeof(uint32_t);
	else {
		itemp ++;
		items_left --;
	}
				
	data_file->write(&len, sizeof(len)); // first have length field!
	uint64_t offset;
	if(len == 0) { // keep offset even if there is no data!
		offset = 0; 
		data_file->write(&offset, sizeof(offset));
		return;
	}
	offset = subtable->add_multiple_records(&itemp, &items_left, len);
	data_file->write(&offset, sizeof(offset));
}

uint64_t table_impl::add_multiple_records(record_item** items, unsigned int* n_items, uint32_t n_records)
{
	record_item* itemp = *items;
	unsigned int items_left = *n_items;
	_seek_data_end();
	/*
	if(marked) {
		printf("add %d records from:\n", n_records);
		for(unsigned int i = 0; i < *n_items; i++) {
			printf("  item[%d] at %p, len %ld\n", i, itemp[i].data, itemp[i].len);
		}
	}
	*/
	
	if(write_instructions.size() == 1 && write_instructions.front().action == write_instruction::copy) {
		// simple optimization
		_copy_ins(itemp, items_left, (uint64_t)n_records * write_instructions.front().elemsize);
	} else {
		for(uint32_t i = 0; i < n_records; i++) {
			for(auto&& ins : write_instructions) {
				while(items_left && (itemp->data == NULL || itemp->len == 0)) {
					items_left --;
					itemp ++;
				}
				if(!items_left)
					throw str_exception_tb("run out of items before processing record %d (had %d items)", i + 1, n_items);
				switch(ins.action) {
				case write_instruction::copy:
					_copy_ins(itemp, items_left, ins.elemsize);
					break;
				case write_instruction::to_subtable:
					_subtable_ins(itemp, items_left, ins.subtable);
					break;
				}
			}
		}
	}
	*items = itemp;
	*n_items = items_left;
	uint64_t ret = next_record_id;
	next_record_id += n_records;
	return ret;
}

void table_impl::_seek_data_end()
{
	if(is_data_at_end)
		return;
	data_file->seek_end();
	is_data_at_end = true;
}

void table_impl::_seek_data(uint64_t offset)
{
	data_file->seek(offset);
	is_data_at_end = false;
}

void table_impl::update_record(uint64_t record, uint32_t offset, record_item* items, unsigned int n_items)
{
	uint64_t file_offset = record * (uint64_t)record_size + (uint64_t)offset;
	_seek_data(file_offset);
	for(unsigned int i = 0; i < n_items; i++)
		data_file->write(items[i].data, items[i].len);
}

// for reading:
bool table_impl::read_record(record_item* items, unsigned int n_items, uint64_t* record)
{
	if(record)
		*record = next_record_id;

	size_t bytes_left_in_record = record_size;
	for(unsigned int i = 0; i < n_items; i++) {
		uint32_t to_read = std::min(bytes_left_in_record, items[i].len);
		if(data_file->read(items[i].data, to_read))
			return true; // eof
		bytes_left_in_record -= to_read;
		if(!bytes_left_in_record)
			break;
	}	
	if(bytes_left_in_record)
		throw str_exception_tb("error: there are still %ul bytes left to read in this record!", bytes_left_in_record);

	next_record_id ++;
	return false;
}

uint64_t table_impl::get_n_records()
{
	return data_file->get_file_size() / (uint64_t)record_size;
}

void table_impl::seek_record(uint64_t record)
{
	uint64_t file_offset = record * (uint64_t)record_size;
	_seek_data(file_offset);
	next_record_id = record;
}

void table_impl::close()
{
	if(data_file)
		data_file.reset();
}

}
