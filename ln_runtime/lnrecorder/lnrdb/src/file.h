#ifndef FILE_H
#define FILE_H

#include <stdio.h>
#include <stdint.h>

#include <string>
#include <vector>

#include "format.h"

namespace lnrdb {

class file {	
public:
	std::string name;
	FILE* fp;
	bool for_reading;
	
	file(std::string fn, const char* mode);
	~file();

	void write(const format& fmt);
	void write(const std::string& s);
	void write(const std::vector<std::string>& v);
	void write(const void* data, size_t len);
	void flush();

	std::string read_all();
	bool read(void* data, size_t len); // returns true on EOF
	
	void seek(uint64_t offset);
	void seek_end();

	uint64_t get_file_size();
};

}

#endif /* FILE_H */

