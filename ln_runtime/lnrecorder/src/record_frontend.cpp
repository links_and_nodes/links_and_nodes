#include <unistd.h>

#include "lnrecorder.h"
#include "record_frontend.h"

namespace lnrecorder {

record_frontend::record_frontend(lnrecorder* parent, const TopicPatterns& topic_patterns, unsigned int n_buffers) :
	frontend(parent, topic_patterns), n_buffers(n_buffers)
{

	n_topics = n_packets = n_service_requests = n_service_responses = 0;
	last_service_id = 0;
	last_peer_id = 0;
	last_topic_id = 0;
}

void record_frontend::write_frames(std::shared_ptr<backend> backend, bool* keep_running_ptr)
{
	parent->current_frontend = this;
	if(keep_running_ptr == NULL) {
		keep_running = true;
		keep_running_ptr = &keep_running;
	}
	target_be = backend;

	std::string service_patterns = topic_patterns.get_service_patterns();
	if(service_patterns.size()) {
		// register logger_service
		std::string logger_service_name = parent->name + ".log_service";
		register_log_service(parent->clnt, logger_service_name);
		parent->clnt->register_as_service_logger(logger_service_name, service_patterns);
	}
	
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Waddress-of-packed-member" // all those members are "pointer"-aligned
	ln::resource_event_connect_t evconnect;
	ln::string_buffer ev_pattern(&evconnect.event_pattern, "*");
	ln::string_buffer name(&evconnect.name_pattern, "*");
	ln::string_buffer md(&evconnect.md_pattern, "*");
	ln::string_buffer client(&evconnect.client_pattern, "*");
#pragma GCC diagnostic pop
	connect_to_resource_event(parent->clnt, "ln.resource_event", &evconnect);

	parent->clnt->handle_service_group_in_thread_pool(NULL, "main");
	parent->clnt->set_max_threads("main", 10);
	
	printf("ready\n");
	
	while(*keep_running_ptr)
		sleep(1);
	
	parent->clnt->disconnect_from_event(resource_event_base::connection);
	parent->clnt->remove_service_group_from_thread_pool(NULL);
	unregister_log_service();

	// count n_packets from topics that are still subscribed
	for(auto& item : topics_by_id) {
		item.second->stop_subscribing();
		n_packets += item.second->n_packets;
	}
	
	printf("saw %d topics with %d packets, %d service calls and %d service responses\n",
	       n_topics, n_packets, n_service_requests, n_service_responses);
}

any_frame* record_frontend::store_service(ln::log_service_t& svc) {
	lock_holder lh(lookup_lock);	
	std::string client_name(svc.req.client_name, svc.req.client_name_len);
	std::string service_name(svc.req.service_name, svc.req.service_name_len);
	std::string service_interface(svc.req.service_interface, svc.req.service_interface_len);

	any_frame& si = service_clients[client_name][service_name][service_interface];
	unsigned int message_size;
	std::string current_msg_def;
	std::string current_msg_def_hash;
	get_message_definition(service_name, service_interface, current_msg_def, message_size, current_msg_def_hash);
	if(si.new_service.service_id == 0 || si.new_service.message_definition != current_msg_def) { // invalid
		last_service_id ++;
		si.type = NEW_SERVICE_FRAME;
		si.timestamp = ln_get_time();
		new_service_frame& f = si.new_service;
		f.service_id = last_service_id;
		f.client_name = client_name;
		f.service_name = service_name;
		f.service_interface = service_interface;
		f.message_definition = current_msg_def;
		f.msg_def_hash = current_msg_def_hash;
		target_be->write_frame(&si);
		
		new_service_event_frame& e = si.new_service_event;
		e.service_id = f.service_id;		
	}
	return &si;
}

uint32_t record_frontend::store_peer(std::string peer) {
	lock_holder lh(lookup_lock);	
	seen_peers_t::iterator i = seen_peers.find(peer);
	if(i == seen_peers.end()) {
		uint32_t peer_id = ++last_peer_id;
		seen_peers[peer] = peer_id;
		any_frame frame;
		frame.type = NEW_SERVICE_PEER_FRAME;
		frame.timestamp = ln_get_time();
		new_service_peer_frame& new_frame = frame.new_service_peer;		
		new_frame.peer = peer;
		new_frame.peer_id = peer_id;
		target_be->write_frame(&frame);
		return peer_id;
	}
	return i->second;
}

int record_frontend::on_log_service(ln::service_request& req, ln::log_service_t& svc) {
	try {
		if(parent->service_verbosity >= 1) {
			const char* service_what = svc.req.item_type == 0 ? "request " : "response";
			char handling_time[64] = "";
			if(svc.req.item_type == 1)
				snprintf(handling_time, 64, ", svc-handler-duration: %.3fms", (svc.req.completion_time - svc.req.request_time) * 1e3);
			
			printf("%s %#x/%d %*.*s from %s\n         transfer_time: %5.3fms, data_len: %d bytes, thread_id %#lx%s\n",
			       service_what,
			       svc.req.client_id, svc.req.request_id,
			       svc.req.service_name_len, svc.req.service_name_len, svc.req.service_name,
			       string_util::repr(svc.req.client_name, svc.req.client_name_len, false).c_str(),			       
			       svc.req.transfer_time * 1e3,
			       svc.req.data_len,
			       svc.req.thread_id,
			       handling_time
			       );
		}
		
		if(svc.req.item_type == 0)
			n_service_requests ++;
		else if(svc.req.item_type == 1)
			n_service_responses ++;

		uint32_t peer_id = store_peer(std::string(svc.req.peer_address, svc.req.peer_address_len)); // might emit NEW_SERVICE_PEER_FRAME
		any_frame* frame = store_service(svc); // might emit NEW_SERVICE_FRAME

		// now emit NEW_SERVICE_EVENT_FRAME
		frame->type = NEW_SERVICE_EVENT_FRAME;
		frame->timestamp = ln_get_time();
		new_service_event_frame& f = frame->new_service_event;		
		f.item_type = svc.req.item_type;
		f.flags = svc.req.flags;
		f.completion_time = svc.req.completion_time;
		f.transfer_time = svc.req.transfer_time;
		f.request_time = svc.req.request_time;
		f.peer_id = peer_id;
		f.client_id = svc.req.client_id;
		f.request_id = svc.req.request_id;
		f.thread_id = svc.req.thread_id;
		f.data.assign((char*)svc.req.data, svc.req.data_len);
		
		target_be->write_frame(frame);
		
		svc.resp.error_message_len = 0;
		req.respond();
	}
	catch(const exception& e) {
		printf("exception in log_service handler:\n%s\n", e.what());
		svc.resp.error_message = (char*)e.what(); // sadly, in this case, pointer to error_message might really not be aligned
		svc.resp.error_message_len = strlen(svc.resp.error_message);
		req.respond();
	}
	return 0;
}

void record_frontend::get_message_definition(std::string user_name, std::string name, std::string& message_definition, unsigned int& message_size, std::string& msg_def_hash) {
	lock_holder lh(cached_message_definitions_lock);
	cached_message_definitions_t::iterator i = cached_message_definitions.find(name);
	cached_message_definition* cmsgdef;
	if(i == cached_message_definitions.end()) {
		cmsgdef = &cached_message_definitions[name];
		parent->clnt->get_message_definition(name, cmsgdef->msg_def, cmsgdef->message_size, cmsgdef->msg_def_hash);
	} else {
		cmsgdef = &i->second;
	}
	cmsgdef->users.insert(user_name);	
	message_definition = cmsgdef->msg_def;
	message_size = cmsgdef->message_size;
	msg_def_hash = cmsgdef->msg_def_hash;
}

void record_frontend::remove_message_definition_user(std::string user, std::string name) {
	lock_holder lh(cached_message_definitions_lock);
	cached_message_definitions_t::iterator i = cached_message_definitions.find(name);
	if(i == cached_message_definitions.end())
		return;
	i->second.users.erase(user);
	if(i->second.users.size() == 0) {
		// remove cache entry
		cached_message_definitions.erase(i);
	}
}

void record_frontend::on_resource_event(ln::resource_event_call_t& ev)
{
	std::string event(ev.event, ev.event_len);
	std::string name(ev.name, ev.name_len);
	std::string md(ev.md, ev.md_len);
	std::string client(ev.client, ev.client_len);

	if(parent->verbosity >= 2) {
		printf("received event: %s, name: %s, md: %s, client: %s\n",
		       event.c_str(),
		       name.c_str(),
		       md.c_str(),
		       client.c_str());
	}

	try {
		if(event == "topic_new_publisher") {
			if(topic_ids.find(name) != topic_ids.end()) {
				printf("got topic_new_publisher-event but we are already recording topic %s ... ignoring...\n", name.c_str());
				return;
			}
			double rate;
			bool only_on_change;
			if(!topic_patterns.have_match(name, &rate, &only_on_change))
				return;
			
			std::string msg_def;
			unsigned int message_size;
			std::string msg_def_hash;
			get_message_definition(name, md, msg_def, message_size, msg_def_hash);
				
			if(parent->verbosity >= 1)
				printf("recording new topic %s\n", name.c_str());

			last_topic_id++;
			topic_frame.type = NEW_TOPIC_FRAME;
			topic_frame.timestamp = ln_get_time();
			
			new_topic_frame& f = topic_frame.new_topic;
			f.topic_id = last_topic_id;
 			f.message_size = message_size;
			f.name = name;
			f.md = md;
			f.msg_def = msg_def;
			f.client = client;
			f.msg_def_hash = msg_def_hash;
			f.topic = topics_by_id[f.topic_id] = new lnrecorder_topic(parent, &f);
			topic_ids[name] = f.topic_id;
			
			process_frame(&topic_frame); // do this here such that backend can prepare for incoming packets!
			
			f.topic->start_subscribing(target_be, rate, only_on_change, n_buffers);
			n_topics ++;
		}
		else if(event == "topic_del_publisher") {
			topic_ids_t::iterator t = topic_ids.find(name);
			if(t != topic_ids.end()) {
				topic_frame.type = DEL_TOPIC_FRAME;
				topic_frame.timestamp = ln_get_time();				
				del_topic_frame& f = topic_frame.del_topic;
				f.topic_id = t->second;
				f.topic = topics_by_id[f.topic_id];
				f.topic->stop_subscribing();
				
				process_frame(&topic_frame);
				
				remove_message_definition_user(f.topic->name, f.topic->md);

				n_packets += f.topic->n_packets;
				delete f.topic;
				topics_by_id.erase(f.topic_id);
				topic_ids.erase(t);
			}
		}
		else if(event == "service_del_provider")
			remove_message_definition_user(name, md);
	}
	catch(exception& e) {
		printf("exception in resource event handling:\n%s\n", e.what());
	}
}

}
