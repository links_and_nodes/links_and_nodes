#include <stdio.h>
#include <stdint.h>

#include <vector>
#include <string>

#include <string_util/string_util.h>

#include "frames.h"

namespace lnrecorder {

void any_frame::print()
{
	printf("ts %.4f ", timestamp);
	switch(type) {
	case HEADER_FRAME:
		header.print();
		break;
	case NEW_TOPIC_FRAME:
		new_topic.print();
		break;
	case NEW_PACKET_FRAME:
		new_packet.print();
		break;
	case DEL_TOPIC_FRAME:
		del_topic.print();
		break;
	case NEW_SERVICE_PEER_FRAME:
		new_service_peer.print();
		break;
	case NEW_SERVICE_FRAME:
		new_service.print();
		break;
	case NEW_SERVICE_EVENT_FRAME:
		new_service_event.print();
		break;
	}
}

void header_frame::print()
{
	printf("header:\n"
	       "  frame_type: %d\n"
	       "  version: %d\n",
	       frame_type,
	       version);
}

void new_topic_frame::print()
{
	printf("new_topic:\n"
	       "  topic_id: %d\n"
	       "  message_size: %d\n"
	       "  name: '%s'\n"
	       "  md: '%s'\n"
	       "  msg_def: '%s'\n"
	       "  client: '%s'\n"
	       "  msg_def_hash: '%s'\n",
	       
	       topic_id,
	       message_size,
	       name.c_str(),
	       md.c_str(),
	       msg_def.c_str(),
	       client.c_str(),
	       msg_def_hash.c_str());
}

void new_packet_frame::print()
{
	printf("new_packet:\n"
	       "  topic_id: %d\n"
	       "  publisher_timestamp: %f\n",
	       topic_id,
	       publisher_timestamp);
}

void del_topic_frame::print()
{
	printf("del_topic:\n"
	       "  topic_id: %d\n",
	       topic_id);
}


void new_service_peer_frame::print()
{
	printf("new_service_peer:\n"
	       "  peer_id: %d\n"
	       "  peer: '%s'\n",
	       peer_id,
	       peer.c_str());
}

void new_service_frame::print()
{
	printf("new_service:\n"
	       "  client_name: '%s'\n"
	       "  service_name: '%s'\n"
	       "  service_interface: '%s'\n"
	       "  message_definition: '%s'\n"
	       "  service_id: %d\n"
	       "  msg_def_hash: '%s'\n",
	       client_name.c_str(),
	       service_name.c_str(),
	       service_interface.c_str(),
	       message_definition.c_str(),
	       service_id,
	       msg_def_hash.c_str());
}

void new_service_event_frame::print()
{
	printf("new_service_event:\n"
	       "  service_id: %d\n"
	       "  item_type: %d\n"
	       "  flags: %d\n"
	       "  completion_time: %f\n"
	       "  transfer_time: %f\n"
	       "  request_time: %f\n"
	       "  peer_id: %d\n"
	       "  client_id: %d\n"
	       "  request_id: %d\n"
	       "  thread_id: %ld\n"
	       "  data: %s\n",
	       
	       service_id,
	       item_type,
	       flags,
	       completion_time,
	       transfer_time,
	       request_time,
	       peer_id,
	       client_id,
	       request_id,
	       thread_id,
	       string_util::repr(data).c_str());
}


}
