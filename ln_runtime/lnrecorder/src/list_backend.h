#ifndef LIST_BACKEND_H
#define LIST_BACKEND_H

#include <stdint.h>

#include <map>
#include <string>

#include "lock.h"
#include "frames.h"
#include "backend.h"

namespace lnrecorder {

struct topic_info_t {	
	uint32_t topic_id;
	double first_seen;
	unsigned int n_packets;
	double last_seen;

	std::string name;
	std::string md;
	unsigned int message_size;

	bool printed;
	
	void print(double start_time, unsigned int max_topic_name_len);
};

class list_backend : public backend {	
	double file_start_time;
	
	typedef std::map<uint32_t, topic_info_t> topic_infos_t;	
	topic_infos_t topic_infos;

	lock file_lock;
	bool had_start_time;
	unsigned int max_topic_name_len;
	uint64_t total;
public:	
	list_backend(lnrecorder* parent);
	~list_backend();
	
	bool write_frame(any_frame* frame);
};

}

#endif // LIST_BACKEND_H

