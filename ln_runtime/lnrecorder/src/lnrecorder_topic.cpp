#include <stdio.h>
#include <string>
#include <vector>
#include <signal.h>

#include <string_util/string_util.h>

#include "lnrecorder.h"
#include "frames.h"

using namespace std;
using namespace string_util;

namespace lnrecorder {

// as created by frontends
lnrecorder_topic::lnrecorder_topic(lnrecorder* parent, new_topic_frame* frame) :
	parent(parent),
	topic_id(frame->topic_id),
	name(frame->name),
	md(frame->md),
	msg_def(frame->msg_def),
	client(frame->client),
	msg_def_hash(frame->msg_def_hash)
{
	n_packets = 0;
	
	table = NULL;
	inport = NULL;
	outport = NULL;
	packet.resize(frame->message_size);

	keep_running = false;
}

lnrecorder_topic::~lnrecorder_topic()
{
	stop_publishing();
	stop_subscribing();
}

// subscribe to topic / record
void lnrecorder_topic::start_subscribing(std::shared_ptr<backend> target_be, double rate, bool only_on_change, unsigned int n_buffers)
{
	if(is_subscribing)
		return;
	this->target_be = target_be;
	this->only_on_change = only_on_change;
	
	inport = parent->clnt->subscribe(name, md, rate, n_buffers);
	if(only_on_change) {
		last_packet.resize(inport->message_size);
		memset(&last_packet[0], 0, inport->message_size);
	}
	is_subscribing = true;

	keep_running = true;
	int ret;
	if((ret = pthread_create(&thread, NULL, _run_subscriber, this)))
		throw retval_exception_tb(ret, "could not start topic subscriber thread!");
}
void lnrecorder_topic::stop_subscribing()
{
	if(!is_subscribing)
		return;
	keep_running = false;
	pthread_join(thread, NULL);	
	parent->clnt->unsubscribe(inport);	
	inport = NULL;
	target_be.reset();
	is_subscribing = false;
}

// publish topic / replay
void lnrecorder_topic::ensure_publishing()
{
	if(is_publishing)
		return;	
	is_publishing = true;
	outport = parent->clnt->publish(name, md);
}

void lnrecorder_topic::stop_publishing()
{
	if(!is_publishing)
		return;
	parent->clnt->unpublish(outport);
	outport = NULL;
	is_publishing = false;
}

void* lnrecorder_topic::_run_subscriber(void* arg) {
	lnrecorder_topic* self = (lnrecorder_topic*)arg;
	try {
		self->run_subscriber();
	}
	catch(exception& e) {
		printf("exception in topic-listener thread:\n%s\n", e.what());
	}
	return NULL;
}

void lnrecorder_topic::run_subscriber() {
	subscriber_frame.type = NEW_PACKET_FRAME;
	new_packet_frame& pframe = subscriber_frame.new_packet;
	pframe.topic = this;
	pframe.topic_id = topic_id;
	while(keep_running) {
		if(inport->read(&packet[0], 0.1)) {
			subscriber_frame.timestamp = ln_get_time();
			if(only_on_change) {
				if(n_packets == 0 && memcmp(&last_packet[0], &packet[0], packet.size()) == 0)
					continue; // no change, skip
				memcpy(&last_packet[0], &packet[0], packet.size());
			}
			pframe.publisher_timestamp = inport->timestamp;
			target_be->write_frame(&subscriber_frame);
			
			if(parent->verbosity >= 2)
				printf("%s: new packet with ts %.3fs\n", name.c_str(), inport->timestamp);
			
			n_packets ++;
		}
				
	}
}

void lnrecorder_topic::write(double ts) {
	outport->write(&packet[0], ts);
	n_packets ++;
}


}
