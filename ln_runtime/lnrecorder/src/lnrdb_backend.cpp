#include <string>
#include <memory>

#include <string_util/string_util.h>

#include "lnrecorder.h"
#include "lnrecorder_topic.h"
#include "lnrdb_backend.h"

namespace lnrecorder {

static bool have_service_foreign_key_offsets = false;
static uint32_t service_request_response_offset;
static uint32_t service_response_request_offset;

lnrdb_backend::lnrdb_backend(lnrecorder* parent, std::string output) :
	backend(parent)
{
	db = lnrdb::create(output);
	db->meta->add("lnrecorder-version", FILE_VERSION);
}


void lnrdb_backend::link_req_and_response(const table_and_offset_t& req, const table_and_offset_t& resp)
{
	if(!have_service_foreign_key_offsets) {
		have_service_foreign_key_offsets = true;
		service_request_response_offset = req.table->get_field_offset("response_service_id"); // needs to be followed by response_offset
		service_response_request_offset = resp.table->get_field_offset("request_service_id");
	}
	lnrdb::record_item resp_items[] = {
		{ (void*)&req.table_id, sizeof(req.table_id) },
		{ (void*)&req.offset, sizeof(req.offset) }
	};
	resp.table->update_record(resp.offset, service_response_request_offset, resp_items, array_length(resp_items));

	// client/requests table also needs table_id as foreign key part (providers service_id)
	lnrdb::record_item req_items[] = {
		{ (void*)&resp.table_id, sizeof(resp.table_id) },
		{ (void*)&resp.offset, sizeof(resp.offset) }
	};
	req.table->update_record(req.offset, service_request_response_offset, req_items, array_length(req_items));
}

void lnrdb_backend::try_link_req_and_response(const table_and_offset_t& this_record, const client_and_request_id& this_id, bool is_request)
{
	open_req_or_resp_t* search_in;
	open_req_or_resp_t* add_to;
	if(is_request) {
		search_in = &open_responses;
		add_to = &open_requests;
	} else {
		search_in = &open_requests;
		add_to = &open_responses;
	}
	
	// can we add this offset to an already existing response record?
	// is there already a response-record that is missing its request?
	open_req_or_resp_t::iterator i = search_in->find(this_id);
	if(i == search_in->end()) {
		// nope, remember this as an open request
		(*add_to)[this_id] = this_record;
		return;
	}
	// yes! set foreign key in response record!
	if(is_request)
		link_req_and_response(this_record, i->second);
	else
		link_req_and_response(i->second, this_record);
	search_in->erase(i);
}



bool lnrdb_backend::write_frame(any_frame* frame)
{
	lock_holder lh(&file_lock);
	this->frame = frame;
	switch(frame->type) {
	case HEADER_FRAME:
		break;
	case NEW_TOPIC_FRAME:
		consume(frame->new_topic);
		break;
	case NEW_PACKET_FRAME:
		consume(frame->new_packet);
		break;
	case DEL_TOPIC_FRAME:
		consume(frame->del_topic);
		break;
	case NEW_SERVICE_PEER_FRAME:
		consume(frame->new_service_peer);
		break;
	case NEW_SERVICE_FRAME:
		consume(frame->new_service);
		break;
	case NEW_SERVICE_EVENT_FRAME:
		consume(frame->new_service_event);
		break;
	}
	return true;
}

void lnrdb_backend::consume(new_topic_frame& frame)
{
	lnrdb::table* table = db->create_table(string_util::format_string("topics/%s/%d", frame.name.c_str(), frame.topic_id));
	table->meta->add("topic.id", "u4", &frame.topic_id);
	table->meta->add("topic.first_seen", "d", &this->frame->timestamp);
	table->meta->add("topic.name", "s", &frame.name);
	table->meta->add("topic.md", "s", &frame.md);
	table->meta->add("topic.msg_def", "s", &frame.msg_def);
	table->meta->add("topic.msg_def_hash", "s", &frame.msg_def_hash);
	table->meta->add("topic.publisher", "s", &frame.client);
	table->meta->add("topic.message_size", "u4", &frame.message_size);
	
	try {
		lnrdb::field_description fd;
		fd.push_back(lnrdb::field_description_item("timestamp", "f8", 1));
		fd.push_back(lnrdb::field_description_item("publisher_timestamp", "f8", 1));
		
		lnrdb::field_description mdfd = lnrdb::get_field_description_from_msg_def(frame.msg_def);
		fd.insert(fd.end(), mdfd.begin(), mdfd.end());
		
		table->set_fixed_type(fd);
	}
	catch(const std::exception& e) {
		printf("failed to parse message-definition:\n   %s\n%s\nwill continue without field-description.\n",
		       frame.msg_def.c_str(),
		       e.what());

		table->set_fixed_type(
			sizeof(any_frame::timestamp)
			+ sizeof(new_packet_frame::publisher_timestamp)
			+ frame.message_size
			);
	}
	frame.topic->table = table;
}

void lnrdb_backend::consume(new_packet_frame& frame)
{
	if(frame.topic) {
		lnrdb::record_item items[] = {
			{ &this->frame->timestamp, sizeof(this->frame->timestamp) },
			{ &frame.publisher_timestamp, sizeof(frame.publisher_timestamp) },
			{ &frame.topic->packet[0], frame.topic->packet.size() },
		};
		frame.topic->table->add_record(items, array_length(items));
	}
}

void lnrdb_backend::consume(del_topic_frame& frame)
{
	if(frame.topic) {
		frame.topic->table->meta->add("topic.last_seen", "d", &this->frame->timestamp);
		frame.topic->table->close();
	}
}


void lnrdb_backend::consume(new_service_peer_frame& frame)
{
	if(service_peers.table == NULL) {
		service_peers.table = db->create_table("services/peers");
		lnrdb::field_description fd;
		fd.push_back(lnrdb::field_description_item("address", "u1", lnrdb::variable));
		service_peers.table->set_fixed_type(fd);
	}

	uint32_t len = frame.peer.size();
	lnrdb::record_item items[] = {
		{ &len, sizeof(len) },
		{(void*)frame.peer.c_str(), len }
	};
	service_peers.keys[frame.peer_id] = (uint32_t)service_peers.table->add_record(
		items, array_length(items));
	
}

void lnrdb_backend::consume(new_service_frame& frame)
{
	new_service_frames[frame.service_id] = frame;	
}

void lnrdb_backend::consume(new_service_event_frame& evframe)
{
	auto && peer_iter = service_peers.keys.find(evframe.peer_id);
	if(peer_iter == service_peers.keys.end()) {
		if(parent->current_frontend->file_version >= 3)
			throw str_exception_tb("got new_service_event frame with unknown peer_id %d!", evframe.peer_id);
		// create a dummy, also related to bug #205!
		new_service_peer_frame peerframe;
		peerframe.peer = "<unknown peer, see bug #205>";
		peerframe.peer_id = evframe.peer_id;
		consume(peerframe);
		peer_iter = service_peers.keys.find(evframe.peer_id);
	}
	uint32_t peer_id = peer_iter->second;
			
	bool is_request = evframe.item_type == 0;
	uint8_t* data = (uint8_t*)evframe.data.c_str();
	uint32_t data_len = evframe.data.size();
	table_and_offset_t this_record;
	this_record.table_id = evframe.service_id;
			
	auto&& service_table_iter = service_tables.find(evframe.service_id);
	if(service_table_iter != service_tables.end())
		this_record.table = service_tables[evframe.service_id];
	else {
		// create new table for this serivce
		new_service_frames_t::iterator new_service_frame_iter =
			new_service_frames.find(evframe.service_id);
		if(new_service_frame_iter == new_service_frames.end()) {
			if(parent->current_frontend->file_version >= 3)
				throw str_exception_tb("got new event for service_id %d but had no NEW_SERVICE_FRAME record!", evframe.service_id);
			// this happend because of bug #205.
			// it is now fixed but we still want to be able to convert all those old pcap files!

			// create a dummy!
			new_service_frame newframe;
			newframe.client_name = "<unknown, see bug #205>";
			newframe.service_name = string_util::format_string("unknown_service_id_%d_bug205", evframe.service_id);
			newframe.service_interface = "unknown, see bug #205";
			newframe.message_definition = string_util::format_string(
				"{'resp_fields': [['uint8_t*', 'data', 1]], 'defines': {}, 'fields': [['uint8_t*', 'data', 1]]}");
			newframe.msg_def_hash = "";
			newframe.service_id = evframe.service_id;
			consume(newframe);
			new_service_frame_iter = new_service_frames.find(evframe.service_id);
		}
		new_service_frame& newframe = new_service_frame_iter->second;

		std::string req_or_resp;
		std::string table_name;
		if(is_request) {
			req_or_resp = "req";
			table_name = string_util::format_string(
				"services/%s/client_%d",
				newframe.service_name.c_str(),
				this_record.table_id);
		} else {
			req_or_resp = "resp";
			table_name = string_util::format_string(
				"services/%s/provider_%d",
				newframe.service_name.c_str(),
				this_record.table_id);
		}
				
		auto table = this_record.table = service_tables[newframe.service_id] = db->create_table(table_name);
		table->meta->add("service.first_seen", "d", &this->frame->timestamp);
		table->meta->add("service.id", "u4", &newframe.service_id);
		table->meta->add("service.client_name", "s", &newframe.client_name);
		table->meta->add("service.name", "s", &newframe.service_name);
		table->meta->add("service.md", "s", &newframe.service_interface);
		table->meta->add("service.msg_def", "s", &newframe.message_definition);
		table->meta->add("service.msg_def_hash", "s", &newframe.msg_def_hash);
		table->meta->add("service.req_or_resp", "s", &req_or_resp);
				
		lnrdb::field_description fd;
		fd.push_back(lnrdb::field_description_item("timestamp", "f8", 1));

		// flags 0x1 if endianess swap was needed
		// flags 0x2 if there is a packet header in request data
		fd.push_back(lnrdb::field_description_item("flags", "u1"));
		fd.push_back(lnrdb::field_description_item("completion_time", "f8"));
		fd.push_back(lnrdb::field_description_item("transfer_time", "f8"));
		fd.push_back(lnrdb::field_description_item("peer_id", "u4"));
		fd.push_back(lnrdb::field_description_item("request_id", "u4"));
		fd.push_back(lnrdb::field_description_item("thread_id", "u8"));

		uint16_t client_version;
		if(is_request) {
			table->meta->add("service.req.client_id", "u4", &evframe.client_id);
			if(evframe.flags & 2) {
				client_version = *(uint16_t*)(data + 4); // skip request length
				table->meta->add("service.req.client_version", "u2", &client_version);
			}
					
			fd.push_back(lnrdb::field_description_item("response_service_id", "u4"));
			fd.push_back(lnrdb::field_description_item("response_offset", "u8"));
			lnrdb::field_description reqfd = lnrdb::get_field_description_from_msg_def(
				newframe.message_definition, "fields");
			fd.push_back(lnrdb::field_description_item("req_data", reqfd, 1));
		} else {
			// only for responses!
			fd.push_back(lnrdb::field_description_item("request_time", "f8"));
			fd.push_back(lnrdb::field_description_item("request_service_id", "u4"));
			fd.push_back(lnrdb::field_description_item("request_offset", "u8"));
			fd.push_back(lnrdb::field_description_item("response_error", "u4"));
			lnrdb::field_description respfd = lnrdb::get_field_description_from_msg_def(
				newframe.message_definition, "resp_fields");
			if(respfd.size())
				fd.push_back(lnrdb::field_description_item("resp_data", respfd, lnrdb::variable));
		}
		table->set_fixed_type(fd);
		new_service_frames.erase(new_service_frame_iter);				
	}

	unsigned int request_time_size;
	if(is_request)
		request_time_size = 0; // no meaning for requests
	else
		request_time_size = sizeof(evframe.request_time);

	uint32_t other_table_id = (uint32_t)-1; // don't know yet, service_id of other table
	uint64_t other_offset = (uint64_t)-1; // foreign key to other req/ersp table
	lnrdb::record_item items[] = {
		{ &this->frame->timestamp, sizeof(this->frame->timestamp) },
		{ &evframe.flags, sizeof(evframe.flags) },
		{ &evframe.completion_time, sizeof(evframe.completion_time) },
		{ &evframe.transfer_time, sizeof(evframe.transfer_time) },
		{ &peer_id, sizeof(peer_id) }, // foreign key into services/clients table via peer_ids[]
		{ &evframe.request_id, sizeof(evframe.request_id) },
		{ &evframe.thread_id, sizeof(evframe.thread_id) },
		{ &evframe.request_time, request_time_size },
		{ &other_table_id, sizeof(other_table_id) },
		{ &other_offset, sizeof(other_offset) },
		{ NULL, 0 }, // response_error or req_data
		{ NULL, 0 }, // [n_resp_data]
		{ NULL, 0 }, // [resp_data]
	};
	uint32_t resp_len;
	uint32_t response_error;
	unsigned int items_len = array_length(items);
	lnrdb::record_item* data_item = &items[items_len - 3];
	if(is_request) { // its a request
		unsigned int to_skip = 4; // length
		if(evframe.flags & 2) // there is a service_request_header at beginning of data!
			to_skip += 2 + 4 + 4;
		data += to_skip;
		data_len -= to_skip;

		data_item[0].data = data;
		data_item[0].len = data_len;
		items_len -= 2;
	} else { // its a response!
		data += 4; data_len -= 4; // skip length
				
		response_error = *((uint32_t*)data);
		data_item[0].data = &response_error;
		data_item[0].len = sizeof(response_error);
		data += 4; data_len -= 4; // skip response_error

		if(response_error == 0) {
			resp_len = 1;
			data_item[2].data = data;
			data_item[2].len = data_len;
		} else {
			resp_len = 0;
			items_len -= 1;
		}
				
		data_item[1].data = &resp_len;
		data_item[1].len = sizeof(resp_len);
	}
			
	this_record.offset = this_record.table->add_record(items, items_len);
	client_and_request_id this_id(evframe.client_id, evframe.request_id);
	try_link_req_and_response(this_record, this_id, is_request);
}

}
