#include <stdio.h>

#include <string_util/string_util.h>

#include "piped_file_backend.h"

#if defined(HAVE_PIPED_FILE_BACKEND)

using namespace std;
using namespace string_util;

namespace lnrecorder {

piped_file_backend::piped_file_backend(std::string command,
				       std::string magic,
				       std::string compress_to_stdout_options,
				       std::string decompress_to_stdout_options) {
	this->magic = magic;
	
	compress_command = command;
	if(compress_to_stdout_options.size()) {
		compress_command += " ";
		compress_command += compress_to_stdout_options;
	}
	decompress_command = command;
	if(decompress_to_stdout_options.size()) {
		decompress_command += " ";
		decompress_command += decompress_to_stdout_options;
	}
}

file_backend::file* piped_file_backend::open(const char* name, const char* mode) {
	string cmd;
	const char* pmode;
	if(strchr(mode, 'w')) {
		cmd = format_string("trap '' SIGINT SIGTERM; %s > '%s'", compress_command.c_str(), name);
		pmode = "w";
	} else {
		cmd = format_string("%s '%s'", decompress_command.c_str(), name);
		pmode = "r";
	}
	FILE* fp = popen(cmd.c_str(), pmode);
	if(!fp)
		throw errno_exception_tb("popen('%s', %s)", cmd.c_str(), pmode);
	plain_file* ret = new plain_file();
	ret->fp = fp;
	return ret;
}

void piped_file_backend::close(file* fp_) {	
	plain_file* fp = (plain_file*)fp_;
	fflush(fp->fp);
	pclose(fp->fp);
	delete fp;
}

bool piped_file_backend::magic_matches(const char* name) {
	FILE* fp = fopen(name, "rb");
	if(!fp)
		throw errno_exception_tb("could not check magic of file '%s'", name);
	char buffer[1024];
	size_t ret = fread(buffer, magic.size(), 1, fp);
	if(ret != 1)
		throw errno_exception_tb("failed to read %d magic bytes from file '%s'", magic.size(), name);
	fclose(fp);
	return memcmp(magic.c_str(), buffer, magic.size()) == 0;
}

}

#endif
