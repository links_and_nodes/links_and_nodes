#include <stdio.h>
#include <string>
#include <vector>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <memory>

#include <string_util/string_util.h>

#include "lnrecorder.h"
#include "plain_file_backend.h"
#include "piped_file_backend.h"

#include "record_frontend.h"
#include "lnrdb_frontend.h"
#include "pcap_frontend.h"
#include "old_frontend.h"

#include "replay_backend.h"
#include "lnrdb_backend.h"
#include "pcap_backend.h"
#include "list_backend.h"
#include "msg_def_writer_backend.h"

using namespace std;

namespace lnrecorder {

using namespace string_util;

lnrecorder* singleton = NULL;

lnrecorder::lnrecorder() {
	singleton = this;
	clnt = NULL;
	
	verbosity = 0;
	service_verbosity = 0;
	
	replay_start = 0;
	
	state = IDLE_STATE;
	service_command = "";

	file_backends[""] = new plain_file_backend();
#if defined(HAVE_PIPED_FILE_BACKEND)
	file_backends["gzip"] = new piped_file_backend("gzip", "\037\213");
	file_backends["bzip2"] = new piped_file_backend("bzip2", "BZh");
	file_backends["xz"] = new piped_file_backend("xz", string("\3757zXZ\0", 6));
#endif
}

lnrecorder::~lnrecorder() {
	for(file_backends_t::iterator i = file_backends.begin(); i != file_backends.end(); i++)
		delete i->second;
}

file_backend* lnrecorder::find_file_backend(const std::string& filename) {
	if(access(filename.c_str(), R_OK) == -1)
		throw errno_exception_tb("file %s does not exist", filename.c_str());
	
	file_backend* fallback = NULL;
	for(file_backends_t::iterator i = file_backends.begin(); i != file_backends.end(); i++) {
		if(i->first == "")
			fallback = i->second;
		else if(i->second->magic_matches(filename.c_str()))
			return i->second;
	}
	return fallback;
}
file_backend* lnrecorder::get_file_backend(const std::string& compress) {
	file_backends_t::iterator i = file_backends.find(compress);
	if(i == file_backends.end())
		throw str_exception_tb("unknown file backend '%s'!", compress.c_str());
	return i->second;
}

int lnrecorder::show_usage(string msg) {
	printf("lnrecorder by Florian Schmidt 2015\n"
	       "\n");
	if(msg.size())
		printf("%s\n\n", msg.c_str());
	printf("usage: lnrecorder [OPTIONS...] VERB [ARGS...]\n"
	       "\n"
	       "VERBS can be one of:\n"
	       "\n"
	       "  record OUTPUT_FILE [name-pattern...]\n"
	       "          start recording of matching topics and services. further arguments\n"
	       "\t  are intrepreted as glob patterns. in this case only topics or services\n"
	       "\t  with matching names are recorded. each pattern can be prefixed with 'topic:'\n"
	       "\t  or 'service:' to only match topics or services.\n"
	       "\t  prepend a '!' to exclude matching topics/patterns.\n"
	       "\t  each topic pattern can be followed by a '@'-sign and a subscription rate\n"
	       "\t  like this: 'large_topics*@3' for 3Hz subscription.\n"
	       "\n"
	       "\t  example: 'record topics:* !topics:do_not_log.*' (would record all topics that\n"
	       "\t  do not start with 'do_not_log.')\n"
	       "\n"
	       "  replay INPUT_FILE [name-pattern...]\n"
	       "          start replaying topics. further glob-patterns select\n"
	       "\t  which topics to replay, if not all.\n"
	       "\n"
	       "  list INPUT_FILE\n"
	       "          list contents of given file\n"
	       "\n"
	       "  idle\n"
	       "          do nothing, just wait for LN-service requests\n"
	       "\n"
	       "  convert INPUT_FILE OUTPUT\n"
	       "          convert from one logging format to another\n"
	       "\n"
	       "  write_msg_defs INPUT_FILE FOLDER\n"
	       "          (unfinished, output topic msg defs to stdout)\n"
	       "\n"
	       "OPTIONS can be:\n"
	       "\n"
	       "  -v      produce more verbose output (repeat up to 3 times)\n"
	       "\n"
	       "  -s RELATIVE_START_TIMESTAMP\n"
	       "          (only for replay) start replaying at given timestamp\n"
	       "          this timestamp is relative to the logfile start-timestamp\n"
	       "\n"
	       "  -vs\n"
	       "          show verbose service request/response events when they happen\n"
	       "\n"
	       "  -name NAME\n"
	       "          use this NAME as prefix for all LN-services\n"
	       "\n"
	       "  -z --gzip\n"
	       "          filter recorded log through gzip before writing to pcap\n"
	       "  -j --bzip2\n"
	       "          filter recorded log through bzip2 before writing to pcap\n"
	       "  -J --xz\n"
	       "          filter recorded log through xz before writing to pcap\n"
	       "  --record-n-buffers N\n"
	       "          how many shared-memory-buffers to request when subscribing to topics (default: 10)\n"
		);

	if(msg.size())
		return 1;
	return 0;
}


int lnrecorder::process_command_line(int argc, char** argv) {
	unsigned int skip = 0;
	string verb;
	compress = "";
	replay_start = 0;
	replay_stop = 0;

	bool need_ln_client = true;
	for(int i = 1; i < argc; i++) {
		std::string arg = argv[i];
	
		if(arg == "-h" || arg == "--help" || arg == "help")
			return show_usage();
		
		if(arg == "convert" || arg == "list" || arg == "write_msg_defs") {
			need_ln_client = false;
			break;
		}
	}
	
	vector<string> args;
	if(need_ln_client) {
		clnt = new ln::client("lnrecorder", argc, argv);
		name = clnt->name;
		args = clnt->get_remaining_args();
		
	} else {
		args.resize(argc);
		for(int i = 0; i < argc; i++)
			args[i] = argv[i];
	}

	string output_file;
	
	for(unsigned int i = 1; i < args.size(); ++i) {
		if(skip) {
			skip--;
			continue;
		}
		string& arg = args[i];
		
		if(!verb.size()) {
			if(arg == "-v") {
				verbosity ++;
				continue;
			}
			if(arg == "-vs") {
				service_verbosity ++;
				continue;
			}
			if(arg == "-s" && i + 1 < args.size()) {
				replay_start = atof(args[i + 1].c_str());
				skip = 1;
				continue;
			}
			if(arg == "-name" && i + 1 < args.size()) {
				name = args[i + 1];
				skip = 1;
				continue;
			}
			if(arg == "--record-n-buffers" && i + 1 < args.size()) {
				record_n_buffers = atoi(args[i + 1].c_str());
				skip = 1;
				continue;
			}
			if(arg == "-z" || arg == "--gzip") {
				compress = "gzip";
				continue;
			}
			if(arg == "-j" || arg == "--bzip2") {
				compress = "bzip2";
				continue;
			}
			if(arg == "-J" || arg == "--xz") {
				compress = "xz";
				continue;
			}
			
			verb = arg;
			if(verb != "record" && verb != "replay" && verb != "list" && verb != "idle" && verb != "convert"
				&& verb != "write_msg_defs")
				return show_usage(format_string("invalid verb: %s", repr(verb).c_str()));
		} else {
			// for record and play:
			if(!filename.size())
				filename = arg;
			else if(verb == "convert" || verb == "write_msg_defs") {
				if(!output_file.size())
					output_file = arg;
				else
					return show_usage(format_string("unknown argument to convert: %s", repr(arg).c_str()));
			} else
				topic_patterns.add(arg);
			
		}
	}
	if(!verb.size())
		return show_usage("no verb specified!");


	if(clnt) {
		register_replay(clnt, name + ".replay", "controls");
		register_record(clnt, name + ".record", "controls");
		register_stop(clnt, name + ".stop", "controls");

		clnt->handle_service_group_in_thread_pool("controls", "pool1");
		clnt->set_max_threads("pool1", 3);
	}
	
	keep_running = true;
	
	signal(SIGINT, on_signal);
	signal(SIGTERM, on_signal);

	try {
		if(verb == "record")
			return record(&keep_running);
		if(verb == "replay")
			return replay(&keep_running);
		if(verb == "list")
			return list_contents();
		if(verb == "idle")
			return idle();
		if(verb == "convert")
			return convert(output_file);
		if(verb == "write_msg_defs")
			return write_msg_defs(output_file);
	}
	catch(exception& e) {
		printf("%s exception:\n%s\n", verb.c_str(), e.what());
		return 2;
	}

	if(clnt) {
		unregister_replay();
		unregister_record();
		unregister_stop();
		delete clnt;
		clnt = nullptr;
	}

	return 0;
}

void on_signal(int signo) {
	if(signo == SIGINT)
		printf("received SIGINT...\n");
	else if(signo == SIGTERM)
		printf("received SIGTERM...\n");
	else
		printf("received signal %d...\n", signo);
	if(singleton) {
		singleton->keep_idle_running = false;
		singleton->new_service_command.signal();
		singleton->stop(false);
	}
}

void lnrecorder::stop(bool wait) {
	keep_running = false;
	keep_running_from_idle = false;
	if(wait)
		wait_idle();
}
void lnrecorder::wait_idle() {
	printf("waiting until idle...\n");
	now_idle.lock();
	while(state != IDLE_STATE)
		now_idle.wait();
	now_idle.unlock();
}

void lnrecorder::new_command(string cmd) {
	new_service_command.lock();
	service_command = cmd;
	new_service_command.unlock();
	new_service_command.signal();
	
	recv_service_command.lock();
	while(state == IDLE_STATE)
		recv_service_command.wait();
	recv_service_command.unlock();
}

string lnrecorder::wait_new_command() {
	new_service_command.lock();
	while(service_command.size() == 0 && keep_idle_running)
		new_service_command.wait();
	if(!keep_idle_running)
		return "quit";
	string verb = service_command;
	service_command = "";
	recv_service_command.lock();
	state = GOT_CMD_STATE;
	recv_service_command.unlock();
	recv_service_command.signal();
	new_service_command.unlock();
	return verb;
}
// LN-service API
int lnrecorder::idle() {
	// wait for command via ln-service calls and execute them here!
	printf("recorder %s is ready, waiting for service requests...\n", repr(name).c_str());
	keep_idle_running = true;
	keep_running_from_idle = false;
	while(keep_idle_running) {
		state = IDLE_STATE;
		now_idle.signal();

		string verb = wait_new_command();
		printf("have new service command: %s\n", repr(verb).c_str());
		
		if(verb == "record") {
			keep_running_from_idle = true;
			record(&keep_running_from_idle);
		} else if(verb == "replay") {
			keep_running_from_idle = true;
			replay(&keep_running_from_idle);
		} else
			printf("unknwon service command: %s\n", repr(verb).c_str());
	}
	return 0;
}

int lnrecorder::on_replay(ln::service_request& req, replay_t& data) {
	try {
		if(state != IDLE_STATE)
			stop();
		
		filename = string(data.req.filename, data.req.filename_len);
		
		topic_patterns.clear();
		for(unsigned int i = 0; i < data.req.patterns_len; i++) {
			std::string arg(data.req.patterns[i].string, data.req.patterns[i].string_len);
			topic_patterns.add(arg);
		}

		replay_start = data.req.start_time;
		replay_stop = data.req.stop_time; // todo
		
		new_command("replay");
		
		if(data.req.wait)
			wait_idle();
		
		req.respond();
	}
	catch(const exception& e) {
		printf("%s: exception:\n%s\n", __func__, e.what());
		data.resp.error_message = (char*)e.what();
		data.resp.error_message_len = strlen(data.resp.error_message);
		req.respond();
	}
	return 0;
}

int lnrecorder::on_record(ln::service_request& req, record_t& data) {
	try {
		if(state != IDLE_STATE)
			stop();
		
		filename = string(data.req.filename, data.req.filename_len);
		
		topic_patterns.clear();
		for(unsigned int i = 0; i < data.req.patterns_len; i++) {
			string arg(data.req.patterns[i].string, data.req.patterns[i].string_len);
			topic_patterns.add(arg);
		}
		new_command("record");		
		req.respond();
	}
	catch(const exception& e) {
		printf("%s: exception:\n%s\n", __func__, e.what());
		data.resp.error_message = (char*)e.what();
		data.resp.error_message_len = strlen(data.resp.error_message);		
		req.respond();
	}
	return 0;
}

int lnrecorder::on_stop(ln::service_request& req, stop_t& data) {
	try {
		if(state != IDLE_STATE)
			stop();
		else
			throw str_exception_tb("was already stopped!");
		req.respond();
	}
	catch(const exception& e) {
		printf("%s: exception:\n%s\n", __func__, e.what());
		data.resp.error_message = (char*)e.what();
		data.resp.error_message_len = strlen(data.resp.error_message);		
		req.respond();
	}
	return 0;
}

string replace_filename_patterns(string filename) {
	char date_time[64];
	time_t now = time(NULL);
	struct tm* btime = localtime(&now);
	strftime(date_time, 64, "%Y%m%d_%H%M%S", btime);
	filename = string_replace(filename, "%T", date_time);
	return filename;
}

std::unique_ptr<frontend> lnrecorder::open_for_reading(std::string filename) {
	printf("open %s for reading.\n", filename.c_str());
	
	try {
		if(lnrdb::is_dir(filename))
			return std::unique_ptr<frontend>(new lnrdb_frontend(this, filename, topic_patterns));
	}
	catch(const exception& e) {
		throw str_exception_tb("could not open lnrdb %s:\n%s", string_util::repr(filename).c_str(), e.what());
	}
	
	try {
		return std::unique_ptr<frontend>(new pcap_frontend(this, filename, topic_patterns));
	}
	catch(const exception& e) {
		// try old format
		printf("failed reading pcap file:\n%s\ntry reading old log file\n",
		       e.what());

		return std::unique_ptr<frontend>(new old_frontend(this, filename, topic_patterns));
	}
}

int lnrecorder::replay(bool* keep_running_ptr) {
	if(!filename.c_str())
		return show_usage("no replay filename specified!");	
	state = REPLAY_STATE;
	std::unique_ptr<frontend> fe = open_for_reading(filename);
	std::shared_ptr<backend> be(
		new replay_backend(this, replay_start, replay_stop, service_verbosity));
	
	fe->write_frames(be, keep_running_ptr);
	
	printf("replay finshed\n");
	return 0;
}

int lnrecorder::record(bool* keep_running_ptr) {
	if(!filename.c_str())
		return show_usage("no record filename specified!");
	state = RECORD_STATE;	
	filename = replace_filename_patterns(filename);
	std::unique_ptr<frontend> fe(new record_frontend(this, topic_patterns, record_n_buffers));
	
	std::shared_ptr<backend> be;
	bool is_pcap_filename = filename.size() > 5 && filename.substr(filename.size() - 5) == ".pcap";
	if(is_pcap_filename)
		be.reset(new pcap_backend(this, filename, compress));
	else
		be.reset(new lnrdb_backend(this, filename));
	
	fe->write_frames(be, keep_running_ptr);
	
	printf("record finished\n");
	return 0;
}

int lnrecorder::convert(std::string output_file)
{
	std::unique_ptr<frontend> fe = open_for_reading(filename);
	std::shared_ptr<backend> be;

	bool reading_from_lnrdb = dynamic_cast<lnrdb_frontend*>(fe.get());
	bool is_pcap_filename = output_file.size() > 5 && output_file.substr(output_file.size() - 5) == ".pcap";
	if(reading_from_lnrdb || is_pcap_filename)
		be.reset(new pcap_backend(this, output_file, compress));
	
	else
		be.reset(new lnrdb_backend(this, output_file));
	
	fe->write_frames(be);
	
	return 0;
}

int lnrecorder::list_contents()
{
	std::unique_ptr<frontend> fe = open_for_reading(filename);
	std::shared_ptr<backend> be(new list_backend(this));
	
	fe->write_frames(be);
	
	return 0;
}

int lnrecorder::write_msg_defs(std::string output_dir)
{
	std::unique_ptr<frontend> fe = open_for_reading(filename);
	std::shared_ptr<backend> be(new msg_def_writer_backend(this, output_dir));
	
	fe->write_frames(be);
	
	return 0;
}

}
