
#include <stdio.h>
#include <time.h>

#include <inttypes.h>

#include <string_util/string_util.h>

#include "list_backend.h"


namespace lnrecorder {

list_backend::list_backend(lnrecorder* parent) :
	backend(parent)
{
	had_start_time = false;
	max_topic_name_len = 0;
	total = 0;
}

list_backend::~list_backend()
{
	for(topic_infos_t::iterator i = topic_infos.begin(); i != topic_infos.end(); i++) {
		if(!i->second.printed)
			i->second.print(file_start_time, max_topic_name_len);
	}
	printf("total of %" PRIu64 " topic packets\n", total);
}


std::string format_size(unsigned int total_bytes)
{
	double total = total_bytes;
	const char* units[] = { "B", "kiB", "MiB", "GiB", "TiB", NULL };
	const char** unit = units;
	while(unit[1]) {
		if(total < 1024)
			break;
		total /= 1024;
		unit++;
	}
	if(unit == units)
		return string_util::format_string("%.0f%s", total, *unit);
	return string_util::format_string("%.1f%s", total, *unit);
}

void topic_info_t::print(double start_time, unsigned int max_topic_name_len)
{
	printed = true;
	double d = last_seen - first_seen;
	double rate = n_packets / d;
	printf("%3d: %8.3fs->%8.3fs: %-*.*s, %d packets %.1fpkts/s, md: %s (%s/pkt, %s total, %s/s)\n",
	       topic_id,
	       first_seen - start_time, last_seen - start_time,
	       max_topic_name_len, max_topic_name_len, name.c_str(),
	       n_packets, rate,
	       md.c_str(), format_size(message_size).c_str(),
	       format_size(message_size * n_packets).c_str(),
	       format_size(message_size * rate).c_str());
}

bool list_backend::write_frame(any_frame* frame)
{
	lock_holder lh(&file_lock);

	if(!had_start_time) {
		had_start_time = true;
		file_start_time = frame->timestamp;
		
		time_t st = (time_t)frame->timestamp;
		struct tm* btime = localtime(&st);
		char sts[128];
		strftime(sts, 128, "%Y-%m-%d %H:%M:%S", btime);
		printf("file_start_time: %s (ts: %.3f)\n", sts, file_start_time);

		printf("%s  %9.9s->%-9.9s  %s\n",
		       "TID",
		       "start", "end",
		       "topic");		
	}

	switch(frame->type) {
	case NEW_TOPIC_FRAME: {
		new_topic_frame& f = frame->new_topic;

		topic_info_t* topic_info = &topic_infos[f.topic_id];
		topic_info->topic_id = f.topic_id;
		topic_info->first_seen = frame->timestamp;
		topic_info->name = f.name;
		topic_info->md = f.md;
		topic_info->message_size = f.message_size;
		
		if(f.name.size() > max_topic_name_len)
			max_topic_name_len = f.name.size();
		
		break;
	}
	case DEL_TOPIC_FRAME: {
		del_topic_frame& f = frame->del_topic;
		topic_infos_t::iterator t = topic_infos.find(f.topic_id);
		if(t != topic_infos.end()) {
			topic_info_t* topic_info = &t->second;
			topic_info->last_seen = frame->timestamp;
			// topic_info->print(file_start_time);
		}
		break;
	}
	case NEW_PACKET_FRAME: {
		new_packet_frame& f = frame->new_packet;
		topic_infos_t::iterator t = topic_infos.find(f.topic_id);
		if(t != topic_infos.end()) {
			t->second.n_packets ++;
			t->second.last_seen = frame->timestamp;
		}
		total ++;
		break;
	}
	case HEADER_FRAME:
	case NEW_SERVICE_PEER_FRAME:
	case NEW_SERVICE_FRAME:
	case NEW_SERVICE_EVENT_FRAME:
		break;
	}
	return true;
}

}
