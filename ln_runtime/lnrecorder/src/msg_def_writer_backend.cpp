#include <stdio.h>

#include <string_util/string_util.h>

#include "lnrecorder.h"
#include "msg_def_writer_backend.h"

using namespace string_util;

namespace lnrecorder {

msg_def_writer_backend::msg_def_writer_backend(lnrecorder* parent, std::string output) :
	backend(parent)
{
	
}

typedef std::map<std::string, py_value*> defines_t;
void msg_def_writer_backend::write_md(std::string name, std::string body)
{
	printf("\nwrite msg_def %s\n", name.c_str());
	printf("  body: %s\n", body.c_str());


	auto msg_def = std::unique_ptr<py_value>(eval_full(body));
	auto msg_def_dict = dynamic_cast<py_dict*>(msg_def.get());
	if(!msg_def_dict)
		throw str_exception_tb("msg-def has non-dict type!");

	auto defines_ = msg_def_dict->value["defines"];
	if(defines_) {
		auto pydefines = dynamic_cast<py_dict*>(defines_);
		if(!pydefines)
			throw str_exception_tb("msg-def dict defines item has non-dict type: %s", repr(defines_).c_str());
		
		for(auto&& define : pydefines->value) {
			printf("  write define %s / %s\n", name.c_str(), define.first.c_str());
		}
	}
	
	auto fields = msg_def_dict->value["fields"];
	if(!fields)
		throw str_exception_tb("msg-def dict item fields not found!");
}

bool msg_def_writer_backend::write_frame(any_frame* frame)
{
	switch(frame->type) {
	case NEW_TOPIC_FRAME: {
		new_topic_frame& f = frame->new_topic;
		write_md(f.md, f.msg_def);
		break;
	}
	case NEW_SERVICE_FRAME: {
		new_service_frame& f = frame->new_service;
		write_md(f.service_interface, f.message_definition);
		break;
	}
	case NEW_PACKET_FRAME:
		break;
	case DEL_TOPIC_FRAME:
		break;
	case NEW_SERVICE_PEER_FRAME:
		break;
	case NEW_SERVICE_EVENT_FRAME:
		break;
	case HEADER_FRAME:
		break;
	}
	return true;
}


}
