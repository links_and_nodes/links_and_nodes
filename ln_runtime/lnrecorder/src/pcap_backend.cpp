#include <stdio.h>

#include "lnrecorder.h"
#include "pcap_backend.h"

namespace lnrecorder {

pcap_backend::pcap_backend(lnrecorder* parent, std::string output, std::string compress) :
	backend(parent)
{
	printf("open %s for writing.\n", output.c_str());
	file_backend* fbe = parent->get_file_backend(compress);
	plog.reset(new pcaplog(output, fbe, 0));
	write_header_frame();
}

void pcap_backend::write_field(const void* field, uint32_t field_len)
{
	plog->packet_add(field_len);
	plog->packet_add(field, field_len);
}

void pcap_backend::write_field(const std::string& v)
{
	write_field(v.c_str(), v.size());
}

void pcap_backend::write_header_frame()
{
	uint32_t frame_type = HEADER_FRAME;
	uint32_t this_version = 0xffff0000 | FILE_VERSION;
	
	plog->packet_start(); // todo: this might have now as timestamp!
	plog->packet_add(&frame_type, sizeof(frame_type));
	plog->packet_add(&this_version, sizeof(this_version));
	plog->packet_done();
}

bool pcap_backend::write_frame(any_frame* frame)
{
	lock_holder lh(&file_lock);

	struct timespec ts = { (uint32_t)frame->timestamp, 0 };
	ts.tv_nsec = (frame->timestamp - ts.tv_sec) * 1e9;
	
	plog->packet_start(&ts);
	
	uint32_t frame_type = frame->type;
	plog->packet_add(frame_type);
	
	switch(frame->type) {
	case HEADER_FRAME:
		break;
	case NEW_TOPIC_FRAME: {
		new_topic_frame& f = frame->new_topic;		
		plog->packet_add(f.topic_id);
		plog->packet_add(f.message_size);
		write_field(f.name);
		write_field(f.md);
		write_field(f.msg_def);
		write_field(f.client);
		write_field(f.msg_def_hash);
		break;
	}
	case NEW_PACKET_FRAME: {
		new_packet_frame& f = frame->new_packet;
		plog->packet_add(f.topic_id);
		plog->packet_add(&f.topic->packet[0], f.topic->packet.size());
		plog->packet_add(f.publisher_timestamp); // todo: checfk that record backend does fill this! (inport->timestamp)
		break;
	}
	case DEL_TOPIC_FRAME: {
		del_topic_frame& f = frame->del_topic;
		plog->packet_add(f.topic_id);
		break;
	}
		
	case NEW_SERVICE_PEER_FRAME: {
		new_service_peer_frame& f = frame->new_service_peer;
		write_field(f.peer);
		plog->packet_add(f.peer_id);
		break;
	}
	case NEW_SERVICE_FRAME: {
		new_service_frame& f = frame->new_service;
		write_field(f.client_name); // todo: make sure that record fe fills these fields from svc!
		write_field(f.service_name);
		write_field(f.service_interface);
		write_field(f.message_definition);
		plog->packet_add(f.service_id);
		write_field(f.msg_def_hash);
		break;
	}
	case NEW_SERVICE_EVENT_FRAME: {
		new_service_event_frame& f = frame->new_service_event;
		plog->packet_add(f.service_id);
		plog->packet_add(f.item_type); // todo: make sure record fe fill these from &svc->req
		plog->packet_add(f.flags);
		plog->packet_add(f.completion_time);
		plog->packet_add(f.transfer_time);
		plog->packet_add(f.request_time);
		plog->packet_add(f.peer_id);
		plog->packet_add(f.client_id);
		plog->packet_add(f.request_id);
		plog->packet_add(f.thread_id);
		write_field(f.data);
		break;
	}
	}
	plog->packet_done();
	return true;
}


}
