#ifndef TOPIC_PATTERNS_H
#define TOPIC_PATTERNS_H

#include <string>
#include <vector>
#include <list>

namespace lnrecorder {

class TopicPatterns {
	class Pattern {
	public:
		std::string pattern;
		double rate;
		bool only_topics;
		bool only_services;
		bool only_on_change;
		bool exclude;
		Pattern(std::string pattern, std::string rate_or_change, bool only_topics,
			bool only_services, bool exclude);
	};
	typedef std::list<Pattern> container_t;
	container_t container;
	
public:
	void add(std::string arg);
	void clear();
	bool is_empty() const;

	bool have_match(const std::string& search, double* rate=NULL, bool* only_on_change=NULL) const;
	std::string get_service_patterns() const;
};

}

#endif // TOPIC_PATTERNS_H
