#include <stdio.h>
#include "lnrecorder.h"

int main(int argc, char* argv[]) {
	lnrecorder::lnrecorder lnr;
	return lnr.process_command_line(argc, argv);
}
