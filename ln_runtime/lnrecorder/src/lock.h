#ifndef LOCK_H
#define LOCK_H

#include "pthread.h"

#if (defined(__PPC__) || defined(__QNX__)) && defined(free)
#undef free
#endif

namespace lnrecorder {

class lock {
	pthread_mutex_t mutex;
	bool locked;
public:
	lock();
	~lock();

	bool free();

	void operator()();
	bool trylock();
	void unlock();  
};

class lock_holder {
	lock* l;
public:
	lock_holder(lock* l) : l(l) {
		(*this->l)();
	}
	lock_holder(lock& l) : l(&l) {
		l();
	}
	~lock_holder() {
		l->unlock();
	}
};

}

#endif // LOCK_H
