#ifndef PCAP_BACKEND_H
#define PCAP_BACKEND_H

#include <stdint.h>

#include <memory>
#include <string>

#include "frames.h"
#include "backend.h"
#include "lock.h"
#include "pcaplog.h"

namespace lnrecorder {

class pcap_backend :
		public backend
{
	std::unique_ptr<pcaplog> plog;
	lock file_lock;
	
	void write_field(const void* field, uint32_t field_len);
	void write_field(const std::string& v);
	void write_header_frame();	
public:	
	pcap_backend(lnrecorder* parent, std::string output, std::string compress="");
	
	bool write_frame(any_frame* frame);
};

}

#endif // PCAP_BACKEND_H

