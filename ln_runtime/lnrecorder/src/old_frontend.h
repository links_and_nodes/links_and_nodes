#ifndef OLD_FRONTEND_H
#define OLD_FRONTEND_H

#include <stdio.h>

#include <vector>
#include <string>

#include "frontend.h"
#include "frames.h"

namespace lnrecorder {

class old_frontend :
		public frontend
{
	FILE* fp;
	
	std::vector<char> field_buffer;
	
	bool read_frame_type(any_frame* frame);
	std::string read_field();
	void read_header_frame();
	
public:	
	old_frontend(lnrecorder* parent, std::string input, const TopicPatterns& topic_patterns);
	~old_frontend();
	
	void write_frames(std::shared_ptr<backend> backend, bool* keep_running_ptr=NULL);
};

}

#endif // OLD_FRONTEND_H
