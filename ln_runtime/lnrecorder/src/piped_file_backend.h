#ifndef PIPED_FILE_BACKEND_H
#define PIPED_FILE_BACKEND_H

#include <stdio.h>
#include <string>
#include "plain_file_backend.h"

#if !defined(MISSING_POPEN)
#define HAVE_PIPED_FILE_BACKEND

namespace lnrecorder {

class piped_file_backend : public plain_file_backend {
	std::string magic;
	std::string compress_command;
	std::string decompress_command;
public:
	piped_file_backend(std::string command,
			   std::string magic,
			   std::string compress_to_stdout_options="",
			   std::string decompress_to_stdout_options="-d -c");
	
	virtual file* open(const char* name, const char* mode);
	virtual void close(file* fp);
	virtual bool magic_matches(const char* name);
};

}

#endif

#endif /* PIPED_FILE_BACKEND_H */

