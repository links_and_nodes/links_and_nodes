#ifndef CONDITION_HELPER_H
#define CONDITION_HELPER_H 1

#include <pthread.h>

class condition_helper {
	bool locked;
public:
	pthread_mutex_t mutex;
	pthread_cond_t cond;

	condition_helper();
	~condition_helper();

	// mutex
	void lock();
	void unlock();

	// condition
	/// wait -> locks needs to be hold -- otherwise error!
	void wait();
	bool timedwait(struct timespec* ts); // relative timeout! false -> timeout
	/// signal
	void signal();
	void broadcast();
};

#endif /* CONDITION_HELPER_H */

