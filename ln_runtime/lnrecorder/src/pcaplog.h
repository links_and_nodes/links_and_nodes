#ifndef PCAPLOG_H
#define PCAPLOG_H

// stolen from module_jaco

#include <stdio.h>
#include <string>
#include <vector>

#include "file_backend.h"

namespace lnrecorder {

class pcaplog {
	file_backend* fb;
	file_backend::file* fp;
	
	unsigned int current_size;
	
	void _open(bool choose_next=true, std::string alt_name="");

	struct packet_header_t {
		uint32_t sec;
		uint32_t nsec;
		uint32_t n_bytes;
		uint32_t full_n_bytes;	
	};

	packet_header_t phdr;
	std::vector<uint8_t> current_packet;
	unsigned int current_packet_len;
	unsigned int current_packet_offset;

	pcaplog(); // for reading
	void _open_for_reading(std::string log_fn, file_backend* fb);
public:
	unsigned int max_log_size;

	std::string fns[2];
	unsigned int current_fn;

	static pcaplog* open_for_reading(std::string log_fn, file_backend* fb);
	pcaplog(std::string log_fn, file_backend* fb, unsigned int max_log_size=0);
	~pcaplog();
	
	void close();

	struct timespec local_ts;
	struct timespec* packet_ts;
	unsigned int packet_len;
	
	void packet_start(struct timespec* ts=NULL);
	void packet_add(const void* data, unsigned int len);
	template<typename T>
	void packet_add(const T& data) { packet_add(&data, sizeof(T)); }
	void packet_done();

	bool read_next_frame(struct timespec* ts);
	template<typename T>
	void consume(T& value);
	void consume_data(uint8_t* dst, unsigned int len);
	uint8_t* get_ptr(unsigned int inc_after=0);
	void skip(unsigned int len);
};

}

#endif
