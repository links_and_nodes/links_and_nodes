#ifndef RECORD_FRONTEND_H
#define RECORD_FRONTEND_H

#include <set>
#include <map>
#include <string>

#include "frames.h"
#include "frontend.h"
#include "lock.h"
#include "condition_helper.h"
#include "topic_patterns.h"

#include "ln_messages.h"

namespace lnrecorder {

class lnrecorder;
class backend;

class record_frontend :
		public frontend,
		public ln::resource_event_base,
		public ln::log_service_base

{
	unsigned int n_buffers = 1;
  
	// generated statistics
	unsigned int n_topics;
	unsigned int n_packets;
	unsigned int n_service_requests;
	unsigned int n_service_responses;

	// state
	bool keep_running;
	
	uint32_t last_topic_id;
	typedef std::map<std::string, uint32_t> topic_ids_t;
	topic_ids_t topic_ids;
	any_frame topic_frame;
	
	struct cached_message_definition {
		std::set<std::string> users;
		std::string msg_def;
		unsigned int message_size;
		std::string msg_def_hash;
	};
	typedef std::map<std::string, cached_message_definition> cached_message_definitions_t;
	cached_message_definitions_t cached_message_definitions;
	lock cached_message_definitions_lock;
	void get_message_definition(std::string user, std::string name, std::string& message_definition, unsigned int& message_size, std::string& msg_def_hash);
	void remove_message_definition_user(std::string user, std::string name);
	// todo: haeh? uint32_t store_message_definition(std::string user, std::string name);

	lock lookup_lock;
	typedef std::map<std::string, any_frame> service_t; // interface_name -> (service_id, msg_def)
	typedef std::map<std::string, service_t> service_client_t; // service_name ->
	typedef std::map<std::string, service_client_t> service_clients_t; // client_name ->
	service_clients_t service_clients;
	uint32_t last_service_id;
	any_frame* store_service(ln::log_service_t& svc);	
	typedef std::map<std::string, uint32_t> seen_peers_t;
	seen_peers_t seen_peers;
	uint32_t last_peer_id;
	uint32_t store_peer(std::string peer);


protected:
	virtual int on_log_service(ln::service_request& req, ln::log_service_t& data);
	virtual void on_resource_event(ln::resource_event_call_t& ev);
	

public:
	record_frontend(lnrecorder* parent, const TopicPatterns& topic_patterns, unsigned int n_buffers);
	
	void write_frames(std::shared_ptr<backend> backend, bool* keep_running_ptr=NULL);
};

}

#endif // RECORD_FRONTEND_H
