#ifndef PLAIN_FILE_BACKEND_H
#define PLAIN_FILE_BACKEND_H

#include "file_backend.h"

namespace lnrecorder {

class plain_file_backend : public file_backend {
public:
	struct plain_file : public file {
		FILE* fp;
	};
	
	virtual file* open(const char* name, const char* mode);
	virtual size_t write(const void* src, unsigned int size, unsigned int n_elem, file* fp);
	virtual size_t read(void* dst, unsigned int size, unsigned int n_elem, file* fp);
	virtual void close(file* fp);
	virtual bool magic_matches(const char* name) { return true; };
};

}

#endif /* PLAIN_FILE_BACKEND_H */

