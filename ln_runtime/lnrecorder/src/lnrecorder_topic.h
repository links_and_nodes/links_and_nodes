#ifndef LNRECORDER_TOPIC_H
#define LNRECORDER_TOPIC_H

#include <stdint.h>
#include <pthread.h>

#include <string>
#include <vector>
#include <map>
#include <memory>

#include <ln/ln.h>

#include "frames.h"

namespace lnrdb {
class table;
}

namespace lnrecorder {

class lnrecorder;
class backend;

class lnrecorder_topic {
public:
	lnrecorder* parent;
	
	uint32_t topic_id;
	std::string name;
	std::string md;
	lnrdb::table* table;
	pthread_t thread;
	bool keep_running {};
	std::vector<uint8_t> packet;
	unsigned int n_packets {};
	
	bool is_publishing {};
	ln::outport* outport;
	
	bool is_subscribing {};
	ln::inport* inport;
	bool only_on_change;
	any_frame subscriber_frame;
	std::shared_ptr<backend> target_be;
	
	std::string msg_def;
	std::string client;
	std::string msg_def_hash;
	std::vector<uint8_t> last_packet;

	lnrecorder_topic(lnrecorder* parent, new_topic_frame* frame);		
	~lnrecorder_topic();

	static void* _run_subscriber(void* self);
	void run_subscriber();

	void start_subscribing(std::shared_ptr<backend> target_be, double rate, bool only_on_change, unsigned int n_buffers);
	void stop_subscribing();
	void ensure_publishing();
	void stop_publishing();

	void write(double ts);
};

typedef std::map<std::string, lnrecorder_topic*> topics_t;
typedef std::map<uint32_t, lnrecorder_topic*> topics_by_id_t;

}

#endif // LNRECORDER_TOPIC_H
