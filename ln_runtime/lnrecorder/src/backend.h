#ifndef LNRECORDER_BACKEND_H
#define LNRECORDER_BACKEND_H

#include "frames.h"

namespace lnrecorder {

class lnrecorder;

class backend {
protected:
	lnrecorder* parent;
public:
	backend(lnrecorder* parent) :
		parent(parent) {}
	virtual ~backend() {};
	
	virtual bool write_frame(any_frame* frame) = 0;
};

}

#endif // LNRECORDER_BACKEND_H
