#ifndef LNRECORDER_H
#define LNRECORDER_H

#include <string>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <memory>

#include <stdint.h>
#include <pthread.h>

#include <ln/ln.h>
#include <string_util/string_util.h>

#include "frames.h"
#include "lock.h"
#include "condition_helper.h"
#include "frontend.h"
#include "backend.h"
#include "file_backend.h"
#include "topic_patterns.h"

#include "ln_messages.h"

using namespace std;

namespace lnrecorder {
	
class lnrecorder;
class lnrecorder_topic;

extern lnrecorder* singleton;

// #define FILE_VERSION 1
// #define FILE_VERSION 2 // new_packet frames have local wall timestamp in pcap header and have ln timestamp double after packet data
                          // and msg_def_hash for new {topic, service} packets
#define FILE_VERSION 3 // fixed bug when in idle mode and starting a 2nd or following files

class lnrecorder :
		public replay_base,
		public record_base,
		public stop_base
{
	friend class lnrecorder_topic;
	friend class lnrdb_backend;
	
public:
	unsigned int verbosity;
	unsigned int service_verbosity;
	frontend* current_frontend;
	string name;
	ln::client* clnt;
	
protected:
	
	string filename;
	TopicPatterns topic_patterns;
	unsigned int record_n_buffers = 10;

	bool keep_running;
	bool keep_idle_running;
	bool keep_running_from_idle;
		
	typedef map<string, file_backend*> file_backends_t;
	file_backends_t file_backends;

	friend void on_signal(int signo);

	// parameters for replay
	double replay_start;
	double replay_stop;

	// parameters for record
	string compress;

	enum recorder_state {
		IDLE_STATE,
		GOT_CMD_STATE,
		RECORD_STATE,
		REPLAY_STATE
	};
	recorder_state state;
	string service_command;
	condition_helper new_service_command;
	condition_helper recv_service_command;
	condition_helper now_idle;
	void new_command(string cmd);
	string wait_new_command();
	void wait_idle();

public:
	lnrecorder();
	~lnrecorder();

	int process_command_line(int argc, char** argv);
	int show_usage(string msg="");

	int record(bool* keep_running);
	int replay(bool* keep_running);
	int list_contents();
	int convert(std::string output_file);
	int write_msg_defs(std::string output_dir);
	
	void stop(bool wait=true);

	// LN-service API
	int idle();
	int on_replay(ln::service_request& req, replay_t& data);
	int on_record(ln::service_request& req, record_t& data);
	int on_stop(ln::service_request& req, stop_t& data);

	// internal services
	file_backend* find_file_backend(const std::string& filename);
	file_backend* get_file_backend(const std::string& compress);

private:
	std::unique_ptr<frontend> open_for_reading(std::string filename);
	bool read_frame_type(uint32_t* frame_type);

};

void on_signal(int signo);

}
#endif /* LNRECORDER_H */

