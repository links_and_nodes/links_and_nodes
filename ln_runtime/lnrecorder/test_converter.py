#!/usr/bin/python
# -*- mode: python -*-
#
# Convert flat.pickle ln_daemon log of single or multiple topic(s) to lnrecorder pcap file
#

import os
import sys
import cPickle
import struct
import numpy as np
import links_and_nodes as ln
import collections
import yaml

lnrecorder_python_path = os.path.join(os.path.dirname(__file__), "python")
if lnrecorder_python_path not in sys.path:
    sys.path.insert(0, lnrecorder_python_path)

from lnrecorder import *


def add_md(md, parent, offset):
    fields = []
    defines = dict(md.defines)

    for ftype, fname, count in md.fields:
        if not parent:
            real_fname = fname
        else:
            real_fname = "%s_%s" % (parent, fname)

        if ftype in defines:
            for c in xrange(count):
                if count > 1:
                    fn = "%s[%d]" % (real_fname, c)
                else:
                    fn = real_fname
                child_md = ln.message_definition(ftype, defines[ftype])
                _, offset = add_md(child_md, fn, offset)
        else:
            dt = ln.data_type_map[ftype]
            #print "%r: %r %r" % (ftype, dt, dt.npformat)
            fields.append((real_fname, dt, count, offset))
            offset += count * dt.get_sizeof()
    return fields, offset



if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Script takes two arguments!")
        print("Usage:")
        print("  %s <pickle-file> <topics-declaration yaml-file>" % sys.argv[0])
        sys.exit(1)

    # read yaml input
    with open(sys.argv[2], 'r') as stream:
        try:
            yaml_data = yaml.load(stream, Loader=yaml.SafeLoader)
            topics = yaml_data["topics"]
            publisher_name =  yaml_data["publisher_name"]
        except yaml.YAMLError as exc:
            print(exc)
            sys.exit(1)

    # read pickle input and save converted output to same location
    flat_pickle_fn = sys.argv[1]
    dataset = cPickle.load(file(flat_pickle_fn, "rb"))
    if "_flat.pickle" in flat_pickle_fn:
        output_fn = flat_pickle_fn.rsplit("_flat.pickle", 1)[0] + ".pcap"
    if ".pickle" in flat_pickle_fn:
        output_fn = flat_pickle_fn.rsplit(".pickle", 1)[0] + ".pcap"

    log = lnrecorder_log.open_for_writing(output_fn)

    pcap_data = {}
    for topic_name, topic_md in topics.iteritems():
        print("\n=== Topic name: %s, message definition: %s ===" % (topic_name, topic_md))
        # check if we have a multi-topic pickle file (is there a smarter way?)
        if "_packet_source_ts" not in dataset:
            data = dataset[topic_name]
        else:
            data = dataset

        # do local/own filesystem search?
        md_fn = ln.search_message_definition(topic_md)
        md = ln.message_definition(topic_md, md_fn)
        msg_def, message_size = repr(md.dict()), md.calculate_size()
        msg_def_hash = md.get_hash()

        # now undo, what manager/Logger.py LoggerTopic::get_flattened_data() did!
        fields, _ = add_md(md, None, 8 + 8 + 4)
        field_values = []
        for field_name, field_type, count, offset in fields:
            print("Field_name: %s, field_type: %s" % (field_name, field_type))
            try:
                field_value = [field_name]
            except:
                raise Exception("could not find field %s in flat-pickle! the message definition does not match or changed!" % field_name)
            field_values.append((field_name, field_type, count, field_value))

        topic_id = log.write_new_topic(data["_packet_source_ts"][0], message_size, topic_name, topic_md, msg_def, publisher_name, msg_def_hash)
        for i, ts in enumerate(data["_packet_source_ts"]):
            packet_data = []
            for field_name, field_type, count, value in field_values:
                format = "%s%s" % (count, field_type.pyformat)
                if count == 1:
                    bin = struct.pack(log.endianess + format, data[field_name][i])
                else:
                    bin = struct.pack(log.endianess + format, *data[field_name][i, :])
                packet_data.append(bin)
            pcap_data[ts] = (topic_id, "".join(packet_data))

    # sort pcap data with respect to timestamps
    sorted_pcap_data = collections.OrderedDict(sorted(pcap_data.items()))
    for ts, data in sorted_pcap_data.iteritems():
        log.write_new_topic_packet(ts, data[0], data[1], ts) # ts is _packet_source_ts and with that publisher_timestamp!

    log.close()
