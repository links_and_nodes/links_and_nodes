## nb properties ##
{'current_cell': 4, 'window_position': (1935, 0), 'window_size': (1316, 1328)}
## nb properties end ##
## cell 0 input ##
import sys
import os
tests = os.path.dirname(os.path.abspath(nb.filename))
lnrecorder = os.path.dirname(tests)
ln_runtime = os.path.dirname(lnrecorder)
base = os.path.dirname(ln_runtime)
pp = os.path.join(base, "python")
if pp not in sys.path:
    sys.path.insert(0, pp)

import lnrdb
lnrdb = reload(lnrdb)

#db = lnrdb.Db("/home/flo/workspace/ln_base_lnrecorder_field_storage/subtypes")
db = lnrdb.Db("small_db")

## cell 0 end ##
## cell 1 input ##
tables = db.tables.keys()
tables.sort()
for tname in tables:
    table = db.tables[tname]
    try:
        print "%-10.10s %s" % (str(table.mmap().shape), tname)
    except:
        print tname
        raise

## cell 1 output ##
# (3537,)    topics/alfred.telemetry/94
# (14,)      topics/verbose.semantic_log/93
# (42,)      topics/world_repr.Antenna.frame/76
# (42,)      topics/world_repr.Antenna_Drawer.frame/80
# (43,)      topics/world_repr.DIP.frame/78
# (44,)      topics/world_repr.Floor.frame/88
# (45,)      topics/world_repr.Justin.frame/91
# (46,)      topics/world_repr.Lander.frame/82
# (47,)      topics/world_repr.Lower_Drawer.frame/86
# (48,)      topics/world_repr.Map.frame/85
# (49,)      topics/world_repr.Motivator.frame/87
# (49,)      topics/world_repr.Panel1.frame/92
# (51,)      topics/world_repr.Panel3.frame/81
# (53,)      topics/world_repr.SPU1.frame/77
# (53,)      topics/world_repr.SPU2.frame/90
# (54,)      topics/world_repr.SPU2_Door.frame/89
# (55,)      topics/world_repr.SPU2_Drawer.frame/79
# (56,)      topics/world_repr.SPU3.frame/83
# (7,)       topics/world_repr.last_update/84
# 
## cell 1 end ##
## cell 2 input ##
tables = db.tables.keys()
tables.sort()
for tname in tables:
    if "peers" in tname: continue
    print "\n%s" % tname
    %% db.tables[tname].meta
    db.tables[tname].show_rows(limit=1)

## cell 2 output ##
# 
# topics/alfred.telemetry/94
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['clock', 'f8'],
#                         ['controller', 'u1', 19],
#                         ['emergency', 'u1', 19],
#                         ['power', 'i4', 19],
#                         ['q_cmd', 'f8', 19],
#                         ['q_act', 'f8', 19],
#                         ['q_poti', 'f8', 19],
#                         ['tau_cmd', 'f8', 19],
#                         ['tau_act', 'f8', 19],
#                         ['hand_q_cmd', 'f8', 24],
#                         ['hand_q_act', 'f8', 24],
#                         ['hand_tau_act', 'f8', 24],
#                         ['appl_id', 'i4'],
#                         ['hl_controller', 'i1'],
#                         ['is_collision_avoidance_enabled', 'u1'],
#                         ['is_physical_collision_detected', 'u1'],
#                         ['did_guard_stop', 'u1'],
#                         ['tau_ext', 'f8', 44],
#                         ['ext_annotation', 'f8'],
#                         ['state', 'f8'],
#                         ['odometry', 'f8', 3],
#                         ['steering_angles', 'f8', 4],
#                         ['leg_extensions', 'f8', 4],
#                         ['cmd_state', 'f8'],
#                         ['bumper_state', 'f8', 4],
#                         ['Hrd', 'f8', 12],
#                         ['Hld', 'f8', 12],
#                         ['T_head', 'f8', 12],
#                         ['rt_state_bits', 'u1', 19],
#                         ['t4_ts', 'f8'],
#                         ['t4_count', 'u4'],
#                         ['t4_position', 'f8'],
#                         ['Jsr', 'f8', 66],
#                         ['Jsl', 'f8', 66],
#                         ['T_right', 'f8', 12],
#                         ['T_left', 'f8', 12],
#                         ['tath_have_command', 'u1'],
#                         ['platform_have_command', 'u1'],
#                         ['q_act_link', 'f8', 20],
#                         ['f_spring_right', 'f8', 6],
#                         ['f_spring_left', 'f8', 6],
#                         ['hand_ok', 'u1'],
#                         ['right_hand_state', 'u1'],
#                         ['left_hand_state', 'u1']],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 3814,
#  u'name': 'topics/alfred.telemetry/94',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726584.078021,
#  u'topic.id': 94,
#  u'topic.last_seen': 1531726587.684191,
#  u'topic.md': 'alfred_telemetry',
#  u'topic.message_size': 3798,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'clock', 1], ['uint8_t', 'controller', 19], ['uint8_t', 'emergency', 19], ['int', 'power', 19], ['double', 'q_cmd', 19], ['double', 'q_act', 19], ['double', 'q_poti', 19], ['double', 'tau_cmd', 19], ['double', 'tau_act', 19], ['double', 'hand_q_cmd', 24], ['double', 'hand_q_act', 24], ['double', 'hand_tau_act', 24], ['int32_t', 'appl_id', 1], ['int8_t', 'hl_controller', 1], ['uint8_t', 'is_collision_avoidance_enabled', 1], ['uint8_t', 'is_physical_collision_detected', 1], ['uint8_t', 'did_guard_stop', 1], ['double', 'tau_ext', 44], ['double', 'ext_annotation', 1], ['double', 'state', 1], ['double', 'odometry', 3], ['double', 'steering_angles', 4], ['double', 'leg_extensions', 4], ['double', 'cmd_state', 1], ['double', 'bumper_state', 4], ['double', 'Hrd', 12], ['double', 'Hld', 12], ['double', 'T_head', 12], ['uint8_t', 'rt_state_bits', 19], ['double', 't4_ts', 1], ['uint32_t', 't4_count', 1], ['double', 't4_position', 1], ['double', 'Jsr', 66], ['double', 'Jsl', 66], ['double', 'T_right', 12], ['double', 'T_left', 12], ['uint8_t', 'tath_have_command', 1], ['uint8_t', 'platform_have_command', 1], ['double', 'q_act_link', 20], ['double', 'f_spring_right', 6], ['double', 'f_spring_left', 6], ['uint8_t', 'hand_ok', 1], ['uint8_t', 'right_hand_state', 1], ['uint8_t', 'left_hand_state', 1]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'alfred.telemetry',
#  u'topic.publisher': 'alfred topless'}
# 
# topics/alfred.telemetry/94 has 3537 rows:
# row 0:
#   timestamp: 1531726584.0765805
#   publisher_timestamp: 1531726584.0765805
#   clock: 1767.962
#   controller: array([4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 0, 0],
#       dtype=uint8)
#   emergency: array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
#       dtype=uint8)
#   power: array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
#       dtype=int32)
#   q_cmd: array([-1.54647517e-07, -6.92337162e-01,  1.20363157e+00, -1.24530977e+00,
#        -1.67354382e+00, -2.81800858e-01,  1.90005266e+00, -1.33954018e-01,
#        -6.98131687e-01,  1.56992354e-01, -1.24530993e+00, -1.67354393e+00,
#        -2.81800720e-01,  1.90005274e+00, -1.33954224e-01, -6.98129961e-01,
#         1.56990731e-01, -3.75000000e-02,  6.21000000e-01])
#   q_act: array([-8.95353878e-06, -6.92334175e-01,  1.20364106e+00, -1.24531758e+00,
#        -1.67355359e+00, -2.81809509e-01,  1.90004933e+00, -1.33963764e-01,
#        -6.98231637e-01,  1.57091379e-01, -1.24531877e+00, -1.67355013e+00,
#        -2.81792164e-01,  1.90005660e+00, -1.33963346e-01, -6.98034167e-01,
#         1.56897649e-01, -3.74945588e-02,  6.20992899e-01])
#   q_poti: array([-2.50594370e-04, -6.84779525e-01,  1.20214784e+00, -1.23845804e+00,
#        -1.67525399e+00, -2.87580401e-01,  1.89792860e+00, -1.29126996e-01,
#        -6.98231637e-01,  1.57091379e-01, -1.24319315e+00, -1.67518699e+00,
#        -2.91365802e-01,  1.89510286e+00, -1.20852202e-01, -6.98034167e-01,
#         1.56897649e-01, -2.98452172e-02,  6.39232755e-01])
#   tau_cmd: array([  0.        ,  31.76559179, -66.68694767, -17.15902183,
#         11.38161207,   4.41904889,  17.25908452,   0.96893469,
#         -4.08527101,  -1.20890921, -17.47541613,  11.37795267,
#          3.87242351,  15.99925762,   1.04148217,  -3.45681776,
#         -0.92725108,   0.        ,   0.        ])
#   tau_act: array([ -4.76000023,  23.29000092, -78.05000305, -22.28000069,
#         13.38000011,   4.03999996,  16.69000053,   0.74000001,
#         -4.25      ,  -1.00999999, -20.18000031,  13.77999973,
#          6.11999989,  15.85999966,   0.75      ,  -4.67999983,
#         -0.81999999,   0.        ,  -0.        ])
#   hand_q_cmd: array([-0.0198,  0.0102,  0.036 ,  0.0174,  0.0029, -0.0115,  0.0192,
#        -0.0005,  0.023 ,  0.0046,  0.0007, -0.0211, -0.0371,  0.0334,
#         0.0834,  0.0099,  0.0238,  0.158 ,  0.0227,  0.0447,  0.0311,
#         0.0137,  0.029 ,  0.0242])
#   hand_q_act: array([-0.01982942,  0.01018055,  0.03599897,  0.01744512,  0.00279357,
#        -0.0115442 ,  0.01925508, -0.00050133,  0.02297965,  0.00463061,
#         0.00068852, -0.0211457 , -0.03522005,  0.03502994,  0.08338531,
#         0.00696074,  0.02129032,  0.15794098,  0.02083545,  0.0433118 ,
#         0.03108306,  0.01043269,  0.02604275,  0.02419037])
#   hand_tau_act: array([ 0.00338461,  0.01305294,  0.        , -0.00468294, -0.0037036 ,
#         0.00148777, -0.0011882 , -0.00376392, -0.00202097, -0.0034858 ,
#        -0.00362521, -0.00221109,  0.03248442,  0.02774015,  0.00227498,
#        -0.02964029, -0.03357554, -0.00393359, -0.03282844, -0.03264435,
#        -0.00377495, -0.02678464, -0.04014728, -0.00608333])
#   appl_id: 0
#   hl_controller: 5
#   is_collision_avoidance_enabled: 0
#   is_physical_collision_detected: 0
#   did_guard_stop: 0
#   tau_ext: array([ 0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,
#        -5.12173918e+00,  1.99198199e+00, -3.69512243e-01, -5.99783834e-01,
#        -2.33048081e-01, -1.47366732e-01,  1.83489412e-01, -2.60937604e+00,
#         2.39284025e+00,  2.21682216e+00, -1.62538952e-01, -2.72564635e-01,
#        -1.20400816e+00,  1.12577248e-01,  0.00000000e+00,  0.00000000e+00,
#         3.38460808e-03,  1.30529364e-02,  0.00000000e+00, -4.68294136e-03,
#        -3.70360154e-03,  1.48776709e-03, -1.18820497e-03, -3.76392296e-03,
#        -2.02097464e-03, -3.48579988e-03, -3.62520874e-03, -2.21108546e-03,
#         3.24844220e-02,  2.77401524e-02,  2.27497984e-03, -2.96402882e-02,
#        -3.35755413e-02, -3.93358874e-03, -3.28284376e-02, -3.26443518e-02,
#        -3.77495162e-03, -2.67846426e-02, -4.01472759e-02, -6.08333334e-03])
#   ext_annotation: 1.0
#   state: 0.0
#   odometry: array([-2.77865454e-05,  6.32110932e-06,  4.74185629e-05])
#   steering_angles: array([ 2.07394198,  4.70625305, -4.68324327,  2.68753441])
#   leg_extensions: array([0.        , 0.0508338 , 0.00394784, 0.        ])
#   cmd_state: 5.0
#   bumper_state: array([0., 0., 0., 0.])
#   Hrd: array([-0.24947844,  0.18903109,  0.94975142,  0.01777084,  0.93128566,
#         0.3156851 ,  0.1817964 , -0.33251894, -0.2654572 ,  0.92984416,
#        -0.25479857,  0.33037478])
#   Hld: array([-0.2494771 , -0.18903273,  0.94975144,  0.01777092, -0.93128592,
#         0.31568542, -0.18179455,  0.33251918, -0.26545757, -0.92984372,
#        -0.2547998 ,  0.33037469])
#   T_head: array([ 8.12729254e-01, -5.81433836e-01,  3.74947215e-02,  4.32945202e-02,
#        -3.04945000e-02,  2.18160402e-02,  9.99296826e-01, -3.87639165e-07,
#        -5.81842973e-01, -8.13301146e-01, -2.08087848e-18,  8.33059170e-01])
#   rt_state_bits: array([3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 3, 1, 1, 3, 3],
#       dtype=uint8)
#   t4_ts: 0.0
#   t4_count: 0
#   t4_position: 0.0
#   Jsr: array([-6.90942541e-02, -1.35735800e-01, -2.71670179e-02,  8.33901950e-02,
#        -1.11292038e-01, -1.07490017e-01, -4.26729329e-01, -1.04438948e-01,
#        -8.87152544e-02, -6.93889390e-18,  1.38000000e-01,  7.02012689e-02,
#         1.38174593e-01, -8.49276166e-02, -1.08670543e-01,  8.21634478e-02,
#        -5.64298463e-02, -7.33463847e-02,  4.29281489e-01,  1.65371734e-02,
#        -1.36300737e-01,  0.00000000e+00,  3.17960193e-01,  4.55078773e-01,
#         2.86442408e-01, -2.38312683e-01,  4.16044365e-01, -1.86839469e-01,
#        -8.85169188e-02,  2.37261888e-01,  0.00000000e+00,  2.77555756e-17,
#         6.93889390e-18, -2.63464042e-01,  0.00000000e+00,  0.00000000e+00,
#         9.31264661e-01, -9.38230875e-01,  3.05469751e-01,  2.57897998e-01,
#        -9.65404144e-01, -1.19834590e-01,  9.87686503e-01,  0.00000000e+00,
#         9.28347049e-01,  0.00000000e+00,  0.00000000e+00,  3.15676406e-01,
#         1.90789737e-01,  8.48554776e-01, -5.10479302e-01, -1.02306944e-01,
#        -6.42864162e-01,  0.00000000e+00,  1.00000000e+00, -2.62218335e-01,
#         0.00000000e+00,  0.00000000e+00,  1.81919042e-01, -2.88655680e-01,
#        -4.32022017e-01, -8.20304519e-01, -2.39850219e-01,  7.56548306e-01,
#         1.56446067e-01,  0.00000000e+00])
#   Jsl: array([ 6.90356484e-02, -1.35642434e-01, -2.71106662e-02,  8.33442177e-02,
#        -1.11214795e-01, -1.07539577e-01, -4.26769112e-01, -1.04385984e-01,
#        -8.86943790e-02,  0.00000000e+00,  1.38000000e-01,  7.02717868e-02,
#        -1.38265068e-01,  8.48714860e-02,  1.08722695e-01, -8.22552435e-02,
#         5.64491231e-02,  7.33283112e-02, -4.29337424e-01, -1.65196840e-02,
#         1.36304917e-01,  0.00000000e+00, -3.17993809e-01,  4.55079132e-01,
#         2.86464383e-01, -2.38317850e-01,  4.16081212e-01, -1.86784730e-01,
#        -8.84402162e-02,  2.37221672e-01, -1.38777878e-17,  2.77555756e-17,
#        -1.73472348e-17, -2.63503672e-01,  0.00000000e+00,  0.00000000e+00,
#        -9.31305623e-01,  9.38286164e-01, -3.05394683e-01, -2.57723220e-01,
#         9.65453846e-01,  1.19707855e-01, -9.87716793e-01,  0.00000000e+00,
#        -9.28299711e-01,  0.00000000e+00,  0.00000000e+00,  3.15698684e-01,
#         1.90746775e-01,  8.48466966e-01, -5.10641084e-01, -1.02323580e-01,
#        -6.42712892e-01,  0.00000000e+00,  1.00000000e+00, -2.62346071e-01,
#         0.00000000e+00,  0.00000000e+00, -1.81670517e-01,  2.88504318e-01,
#         4.32247492e-01,  8.20258755e-01,  2.39642977e-01, -7.56696880e-01,
#        -1.56254719e-01,  0.00000000e+00])
#   T_right: array([-0.25165181,  0.19626837,  0.94770781,  0.01565588,  0.93126691,
#         0.31567465,  0.18191056, -0.33249934, -0.26346404,  0.92834705,
#        -0.26221833,  0.32989449])
#   T_left: array([-0.25147534, -0.1964507 ,  0.94771688,  0.0156645 , -0.93130337,
#         0.31570044, -0.181679  ,  0.3325338 , -0.26350367, -0.92829971,
#        -0.26234607,  0.32988366])
#   tath_have_command: 1
#   platform_have_command: 1
#   q_act_link: array([-8.95353878e-06, -6.92334175e-01,  1.20364106e+00, -5.19189950e-01,
#        -1.24531758e+00, -1.67355359e+00, -2.81809509e-01,  1.90004933e+00,
#        -1.33963764e-01, -6.98231637e-01,  1.57091379e-01, -1.24531877e+00,
#        -1.67355013e+00, -2.81792164e-01,  1.90005660e+00, -1.33963346e-01,
#        -6.98034167e-01,  1.56897649e-01, -3.74945588e-02,  6.20992899e-01])
#   f_spring_right: array([0., 0., 0., 0., 0., 0.])
#   f_spring_left: array([0., 0., 0., 0., 0., 0.])
#   hand_ok: 1
#   right_hand_state: 1
#   left_hand_state: 1
#  
# topics/verbose.semantic_log/93
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['type', 'u1', 64],
#                         ['action', 'u1', 512],
#                         ['parameters', 'u1', 1028],
#                         ['start_time', 'f8'],
#                         ['end_time', 'f8']],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 1636,
#  u'name': 'topics/verbose.semantic_log/93',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.767909,
#  u'topic.id': 93,
#  u'topic.last_seen': 1531726587.743738,
#  u'topic.md': 'verbose/semantic_log',
#  u'topic.message_size': 1620,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['char', 'type', 64], ['char', 'action', 512], ['char', 'parameters', 1028], ['double', 'start_time', 1], ['double', 'end_time', 1]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'verbose.semantic_log',
#  u'topic.publisher': 'alfred verbose2'}
# 
# topics/verbose.semantic_log/93 has 14 rows:
# row 0:
#   timestamp: 1531726582.8484592
#   publisher_timestamp: 1531726582.8484592
#   type: array([115, 105, 109,  32, 111, 112,   0,   0, 105, 111, 110,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
#       dtype=uint8)
#   action: array([103, 111, 116, 111,  95, 112, 111, 115,   0, 115, 116, 114,  97,
#        105, 103, 104, 116,   0,   0, 109, 112,   0,   0, 111, 116, 105,
#        118,  97, 116, 111, 114,   0,  83,  80,  85,  50,  32,  74, 117,
#        115, 116, 105, 110,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
#          0,   0,   0,   0,   0], dtype=uint8)
#   parameters: array([ 33,  33, 112, ...,  32,  32,  45], dtype=uint8)
#   start_time: 1531726582.846953
#   end_time: 1531726582.848253
#  
# topics/world_repr.Antenna.frame/76
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['frame', 'f8', 12]],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 112,
#  u'name': 'topics/world_repr.Antenna.frame/76',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.637238,
#  u'topic.id': 76,
#  u'topic.last_seen': 1531726587.77268,
#  u'topic.md': 'ln/frame34',
#  u'topic.message_size': 96,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'frame', 12]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.Antenna.frame',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.Antenna.frame/76 has 42 rows:
# row 0:
#   timestamp: 1531726583.5465086
#   publisher_timestamp: 1531726583.5465086
#   frame: array([ 0.   ,  1.   ,  0.   ,  7.032, -1.   ,  0.   , -0.   ,  1.407,
#        -0.   ,  0.   ,  1.   ,  0.994])
#  
# topics/world_repr.Antenna_Drawer.frame/80
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['frame', 'f8', 12]],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 112,
#  u'name': 'topics/world_repr.Antenna_Drawer.frame/80',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.659256,
#  u'topic.id': 80,
#  u'topic.last_seen': 1531726587.875542,
#  u'topic.md': 'ln/frame34',
#  u'topic.message_size': 96,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'frame', 12]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.Antenna_Drawer.frame',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.Antenna_Drawer.frame/80 has 42 rows:
# row 0:
#   timestamp: 1531726583.647052
#   publisher_timestamp: 1531726583.647052
#   frame: array([ 0.70710678, -0.70710678,  0.        ,  7.23686292,  0.70710678,
#         0.70710678,  0.        ,  3.34686292,  0.        ,  0.        ,
#         1.        ,  1.89      ])
#  
# topics/world_repr.DIP.frame/78
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['frame', 'f8', 12]],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 112,
#  u'name': 'topics/world_repr.DIP.frame/78',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.647763,
#  u'topic.id': 78,
#  u'topic.last_seen': 1531726587.974341,
#  u'topic.md': 'ln/frame34',
#  u'topic.message_size': 96,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'frame', 12]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.DIP.frame',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.DIP.frame/78 has 43 rows:
# row 0:
#   timestamp: 1531726583.6470978
#   publisher_timestamp: 1531726583.6470978
#   frame: array([-0.12305504, -0.02800267,  0.99200469,  5.96332944,  0.31803193,
#         0.94576957,  0.06614839,  0.94099409, -0.94006018,  0.32362906,
#        -0.10747597,  0.60827945])
#  
# topics/world_repr.Floor.frame/88
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['frame', 'f8', 12]],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 112,
#  u'name': 'topics/world_repr.Floor.frame/88',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.728211,
#  u'topic.id': 88,
#  u'topic.last_seen': 1531726588.075311,
#  u'topic.md': 'ln/frame34',
#  u'topic.message_size': 96,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'frame', 12]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.Floor.frame',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.Floor.frame/88 has 44 rows:
# row 0:
#   timestamp: 1531726583.646989
#   publisher_timestamp: 1531726583.646989
#   frame: array([ 1.        ,  0.        ,  0.        ,  4.05000019,  0.        ,
#         1.        ,  0.        ,  2.32500005,  0.        ,  0.        ,
#         1.        , -0.2       ])
#  
# topics/world_repr.Justin.frame/91
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['frame', 'f8', 12]],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 112,
#  u'name': 'topics/world_repr.Justin.frame/91',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.744937,
#  u'topic.id': 91,
#  u'topic.last_seen': 1531726588.175262,
#  u'topic.md': 'ln/frame34',
#  u'topic.message_size': 96,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'frame', 12]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.Justin.frame',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.Justin.frame/91 has 45 rows:
# row 0:
#   timestamp: 1531726583.6469016
#   publisher_timestamp: 1531726583.6469016
#   frame: array([ 9.95208644e-01, -3.13393982e-02,  9.26153166e-02,  6.04013100e+00,
#         3.12568943e-02,  9.99508641e-01,  2.34159856e-03,  1.27787324e+00,
#        -9.26431935e-02,  5.64488034e-04,  9.95699212e-01,  6.49408274e-02])
#  
# topics/world_repr.Lander.frame/82
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['frame', 'f8', 12]],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 112,
#  u'name': 'topics/world_repr.Lander.frame/82',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.67376,
#  u'topic.id': 82,
#  u'topic.last_seen': 1531726588.275919,
#  u'topic.md': 'ln/frame34',
#  u'topic.message_size': 96,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'frame', 12]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.Lander.frame',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.Lander.frame/82 has 46 rows:
# row 0:
#   timestamp: 1531726583.6469598
#   publisher_timestamp: 1531726583.6469598
#   frame: array([1.00000000e+00, 0.00000000e+00, 0.00000000e+00, 7.71935368e+00,
#        0.00000000e+00, 1.00000000e+00, 0.00000000e+00, 4.26470137e+00,
#        0.00000000e+00, 0.00000000e+00, 1.00000000e+00, 5.72204590e-06])
#  
# topics/world_repr.Lower_Drawer.frame/86
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['frame', 'f8', 12]],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 112,
#  u'name': 'topics/world_repr.Lower_Drawer.frame/86',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.705884,
#  u'topic.id': 86,
#  u'topic.last_seen': 1531726588.38834,
#  u'topic.md': 'ln/frame34',
#  u'topic.message_size': 96,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'frame', 12]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.Lower_Drawer.frame',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.Lower_Drawer.frame/86 has 47 rows:
# row 0:
#   timestamp: 1531726583.6470113
#   publisher_timestamp: 1531726583.6470113
#   frame: array([ 0.70710678, -0.70710678,  0.        ,  7.11311923,  0.70710678,
#         0.70710678,  0.        ,  3.22311923,  0.        ,  0.        ,
#         1.        ,  1.02      ])
#  
# topics/world_repr.Map.frame/85
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['frame', 'f8', 12]],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 112,
#  u'name': 'topics/world_repr.Map.frame/85',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.695454,
#  u'topic.id': 85,
#  u'topic.last_seen': 1531726588.477116,
#  u'topic.md': 'ln/frame34',
#  u'topic.message_size': 96,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'frame', 12]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.Map.frame',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.Map.frame/85 has 48 rows:
# row 0:
#   timestamp: 1531726583.6469362
#   publisher_timestamp: 1531726583.6469362
#   frame: array([ 0.   , -1.   ,  0.   ,  4.05 ,  1.   ,  0.   ,  0.   ,  2.325,
#         0.   ,  0.   ,  1.   , -0.2  ])
#  
# topics/world_repr.Motivator.frame/87
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['frame', 'f8', 12]],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 112,
#  u'name': 'topics/world_repr.Motivator.frame/87',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.722809,
#  u'topic.id': 87,
#  u'topic.last_seen': 1531726588.579626,
#  u'topic.md': 'ln/frame34',
#  u'topic.message_size': 96,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'frame', 12]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.Motivator.frame',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.Motivator.frame/87 has 49 rows:
# row 0:
#   timestamp: 1531726583.646923
#   publisher_timestamp: 1531726583.646923
#   frame: array([ 2.06420739e-03, -9.99806779e-01,  1.95484858e-02,  6.80741327e+00,
#         9.99680705e-01,  2.55546715e-03,  2.51387802e-02,  1.24581857e+00,
#        -2.51838783e-02,  1.94903524e-02,  9.99492821e-01,  6.15734982e-01])
#  
# topics/world_repr.Panel1.frame/92
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['frame', 'f8', 12]],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 112,
#  u'name': 'topics/world_repr.Panel1.frame/92',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.754741,
#  u'topic.id': 92,
#  u'topic.last_seen': 1531726588.681046,
#  u'topic.md': 'ln/frame34',
#  u'topic.message_size': 96,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'frame', 12]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.Panel1.frame',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.Panel1.frame/92 has 49 rows:
# row 0:
#   timestamp: 1531726583.747528
#   publisher_timestamp: 1531726583.747528
#   frame: array([ 0.    ,  1.    ,  0.    ,  1.9331, -1.    ,  0.    , -0.    ,
#         3.7865, -0.    ,  0.    ,  1.    ,  0.994 ])
#  
# topics/world_repr.Panel3.frame/81
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['frame', 'f8', 12]],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 112,
#  u'name': 'topics/world_repr.Panel3.frame/81',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.665154,
#  u'topic.id': 81,
#  u'topic.last_seen': 1531726588.778431,
#  u'topic.md': 'ln/frame34',
#  u'topic.message_size': 96,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'frame', 12]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.Panel3.frame',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.Panel3.frame/81 has 51 rows:
# row 0:
#   timestamp: 1531726583.6470864
#   publisher_timestamp: 1531726583.6470864
#   frame: array([-3.80973912e-03,  9.99967849e-01, -7.05600425e-03,  4.18064644e+00,
#        -9.99990206e-01, -3.82552900e-03, -2.22564930e-03,  3.78162114e+00,
#        -2.25257069e-03,  7.04745600e-03,  9.99972629e-01,  9.59831262e-01])
#  
# topics/world_repr.SPU1.frame/77
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['frame', 'f8', 12]],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 112,
#  u'name': 'topics/world_repr.SPU1.frame/77',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.642189,
#  u'topic.id': 77,
#  u'topic.last_seen': 1531726588.879414,
#  u'topic.md': 'ln/frame34',
#  u'topic.message_size': 96,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'frame', 12]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.SPU1.frame',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.SPU1.frame/77 has 53 rows:
# row 0:
#   timestamp: 1531726583.5466766
#   publisher_timestamp: 1531726583.5466766
#   frame: array([-0.70715101,  0.70706255,  0.        ,  1.827     , -0.70706255,
#        -0.70715101,  0.        ,  4.034     ,  0.        ,  0.        ,
#         1.        ,  0.        ])
#  
# topics/world_repr.SPU2.frame/90
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['frame', 'f8', 12]],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 112,
#  u'name': 'topics/world_repr.SPU2.frame/90',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.738302,
#  u'topic.id': 90,
#  u'topic.last_seen': 1531726588.980014,
#  u'topic.md': 'ln/frame34',
#  u'topic.message_size': 96,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'frame', 12]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.SPU2.frame',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.SPU2.frame/90 has 53 rows:
# row 0:
#   timestamp: 1531726583.6470366
#   publisher_timestamp: 1531726583.6470366
#   frame: array([1.   , 0.   , 0.   , 6.932, 0.   , 1.   , 0.   , 1.157, 0.   ,
#        0.   , 1.   , 0.   ])
#  
# topics/world_repr.SPU2_Door.frame/89
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['frame', 'f8', 12]],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 112,
#  u'name': 'topics/world_repr.SPU2_Door.frame/89',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.733353,
#  u'topic.id': 89,
#  u'topic.last_seen': 1531726589.080505,
#  u'topic.md': 'ln/frame34',
#  u'topic.message_size': 96,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'frame', 12]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.SPU2_Door.frame',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.SPU2_Door.frame/89 has 54 rows:
# row 0:
#   timestamp: 1531726583.647075
#   publisher_timestamp: 1531726583.647075
#   frame: array([-1.00000000e+00,  1.22464680e-16,  0.00000000e+00,  6.93200016e+00,
#        -1.22464680e-16, -1.00000000e+00,  0.00000000e+00,  1.18700004e+00,
#         0.00000000e+00,  0.00000000e+00,  1.00000000e+00,  3.90000008e-02])
#  
# topics/world_repr.SPU2_Drawer.frame/79
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['frame', 'f8', 12]],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 112,
#  u'name': 'topics/world_repr.SPU2_Drawer.frame/79',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.654131,
#  u'topic.id': 79,
#  u'topic.last_seen': 1531726589.186643,
#  u'topic.md': 'ln/frame34',
#  u'topic.message_size': 96,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'frame', 12]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.SPU2_Drawer.frame',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.SPU2_Drawer.frame/79 has 55 rows:
# row 0:
#   timestamp: 1531726583.647025
#   publisher_timestamp: 1531726583.647025
#   frame: array([1.   , 0.   , 0.   , 6.655, 0.   , 1.   , 0.   , 1.187, 0.   ,
#        0.   , 1.   , 0.629])
#  
# topics/world_repr.SPU3.frame/83
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['frame', 'f8', 12]],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 112,
#  u'name': 'topics/world_repr.SPU3.frame/83',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.679484,
#  u'topic.id': 83,
#  u'topic.last_seen': 1531726589.293255,
#  u'topic.md': 'ln/frame34',
#  u'topic.message_size': 96,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'frame', 12]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.SPU3.frame',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.SPU3.frame/83 has 56 rows:
# row 0:
#   timestamp: 1531726583.6471124
#   publisher_timestamp: 1531726583.6471124
#   frame: array([-0.70715101,  0.70706255,  0.        ,  4.064     , -0.70706255,
#        -0.70715101,  0.        ,  4.034     ,  0.        ,  0.        ,
#         1.        ,  0.        ])
#  
# topics/world_repr.last_update/84
# db.tables[tname].meta: {u'endianess': 'little',
#  u'field-description': [['timestamp', 'f8'],
#                         ['publisher_timestamp', 'f8'],
#                         ['counter', 'f8'],
#                         ['time', 'f8']],
#  u'field-subtables': [],
#  u'fixed-table-record-size': 32,
#  u'name': 'topics/world_repr.last_update/84',
#  u'table-format': 'fixed',
#  u'topic.first_seen': 1531726583.685039,
#  u'topic.id': 84,
#  u'topic.last_seen': 1531726589.351181,
#  u'topic.md': 'world_representation/state_topic',
#  u'topic.message_size': 16,
#  u'topic.msg_def': "{'resp_fields': [], 'defines': {}, 'fields': [['double', 'counter', 1], ['double', 'time', 1]]}",
#  u'topic.msg_def_hash': '',
#  u'topic.name': 'world_repr.last_update',
#  u'topic.publisher': 'world_representation'}
# 
# topics/world_repr.last_update/84 has 7 rows:
# row 0:
#   timestamp: 1531726583.241733
#   publisher_timestamp: 1531726583.241733
#   counter: 7.0
#   time: 1531726426.412612
# 
## cell 2 end ##
## cell 3 input ##
% db.meta
## cell 3 output ##
# db.meta: {u'lnrecorder-version': 3, u'version': 1}
# 
## cell 3 end ##
## cell 4 input ##
import numpy as np
dt = db.tables["topics/alfred.telemetry/94"].dtype
for some in dt.names:
    % some, dt[some].type
    print dt[some] is np.float64
## cell 4 output ##
# some, dt[some].type: ('timestamp', <type 'numpy.float64'>)
# False
# some, dt[some].type: ('publisher_timestamp', <type 'numpy.float64'>)
# False
# some, dt[some].type: ('clock', <type 'numpy.float64'>)
# False
# some, dt[some].type: ('controller', <type 'numpy.void'>)
# False
# some, dt[some].type: ('emergency', <type 'numpy.void'>)
# False
# some, dt[some].type: ('power', <type 'numpy.void'>)
# False
# some, dt[some].type: ('q_cmd', <type 'numpy.void'>)
# False
# some, dt[some].type: ('q_act', <type 'numpy.void'>)
# False
# some, dt[some].type: ('q_poti', <type 'numpy.void'>)
# False
# some, dt[some].type: ('tau_cmd', <type 'numpy.void'>)
# False
# some, dt[some].type: ('tau_act', <type 'numpy.void'>)
# False
# some, dt[some].type: ('hand_q_cmd', <type 'numpy.void'>)
# False
# some, dt[some].type: ('hand_q_act', <type 'numpy.void'>)
# False
# some, dt[some].type: ('hand_tau_act', <type 'numpy.void'>)
# False
# some, dt[some].type: ('appl_id', <type 'numpy.int32'>)
# False
# some, dt[some].type: ('hl_controller', <type 'numpy.int8'>)
# False
# some, dt[some].type: ('is_collision_avoidance_enabled', <type 'numpy.uint8'>)
# False
# some, dt[some].type: ('is_physical_collision_detected', <type 'numpy.uint8'>)
# False
# some, dt[some].type: ('did_guard_stop', <type 'numpy.uint8'>)
# False
# some, dt[some].type: ('tau_ext', <type 'numpy.void'>)
# False
# some, dt[some].type: ('ext_annotation', <type 'numpy.float64'>)
# False
# some, dt[some].type: ('state', <type 'numpy.float64'>)
# False
# some, dt[some].type: ('odometry', <type 'numpy.void'>)
# False
# some, dt[some].type: ('steering_angles', <type 'numpy.void'>)
# False
# some, dt[some].type: ('leg_extensions', <type 'numpy.void'>)
# False
# some, dt[some].type: ('cmd_state', <type 'numpy.float64'>)
# False
# some, dt[some].type: ('bumper_state', <type 'numpy.void'>)
# False
# some, dt[some].type: ('Hrd', <type 'numpy.void'>)
# False
# some, dt[some].type: ('Hld', <type 'numpy.void'>)
# False
# some, dt[some].type: ('T_head', <type 'numpy.void'>)
# False
# some, dt[some].type: ('rt_state_bits', <type 'numpy.void'>)
# False
# some, dt[some].type: ('t4_ts', <type 'numpy.float64'>)
# False
# some, dt[some].type: ('t4_count', <type 'numpy.uint32'>)
# False
# some, dt[some].type: ('t4_position', <type 'numpy.float64'>)
# False
# some, dt[some].type: ('Jsr', <type 'numpy.void'>)
# False
# some, dt[some].type: ('Jsl', <type 'numpy.void'>)
# False
# some, dt[some].type: ('T_right', <type 'numpy.void'>)
# False
# some, dt[some].type: ('T_left', <type 'numpy.void'>)
# False
# some, dt[some].type: ('tath_have_command', <type 'numpy.uint8'>)
# False
# some, dt[some].type: ('platform_have_command', <type 'numpy.uint8'>)
# False
# some, dt[some].type: ('q_act_link', <type 'numpy.void'>)
# False
# some, dt[some].type: ('f_spring_right', <type 'numpy.void'>)
# False
# some, dt[some].type: ('f_spring_left', <type 'numpy.void'>)
# False
# some, dt[some].type: ('hand_ok', <type 'numpy.uint8'>)
# False
# some, dt[some].type: ('right_hand_state', <type 'numpy.uint8'>)
# False
# some, dt[some].type: ('left_hand_state', <type 'numpy.uint8'>)
# False
# 
## cell 4 end ##
## cell 5 input ##
% 24*8
## cell 5 output ##
# 24*8: 192
# 
## cell 5 end ##
## cell 6 input ##

import lnrdb.converter
lnrdb.converter = reload(lnrdb.converter)
import sqlite3
db_fn = "out.db"
if os.path.isfile(db_fn): os.unlink(db_fn)
conv = lnrdb.converter.converter(connection=sqlite3.connect(db_fn))
conv.convert_from(db)

## cell 6 output ##
# use tname: 'topics_alfred_telemetry_94' for 'topics/alfred.telemetry/94'
# 
# for table topics_alfred_telemetry_94
# dtype: {'hand_tau_act': (dtype(('<f8', (24,))), 1282), 'did_guard_stop': (dtype('uint8'), 1481), 'bumper_state': (dtype(('<f8', (4,))), 1946), 'hl_controller': (dtype('int8'), 1478), 'hand_ok': (dtype('uint8'), 3811), 't4_count': (dtype('uint32'), 2293), 'tath_have_command': (dtype('uint8'), 3553), 'tau_cmd': (dtype(('<f8', (19,))), 594), 'Hrd': (dtype(('<f8', (12,))), 1978), 'is_physical_collision_detected': (dtype('uint8'), 1480), 'f_spring_right': (dtype(('<f8', (6,))), 3715), 'power': (dtype(('<i4', (19,))), 62), 'clock': (dtype('float64'), 16), 'Jsl': (dtype(('<f8', (66,))), 2833), 'hand_q_cmd': (dtype(('<f8', (24,))), 898), 'rt_state_bits': (dtype(('u1', (19,))), 2266), 'leg_extensions': (dtype(('<f8', (4,))), 1906), 't4_ts': (dtype('float64'), 2285), 'state': (dtype('float64'), 1842), 'ext_annotation': (dtype('float64'), 1834), 'Jsr': (dtype(('<f8', (66,))), 2305), 'q_cmd': (dtype(('<f8', (19,))), 138), 'steering_angles': (dtype(('<f8', (4,))), 1874), 'odometry': (dtype(('<f8', (3,))), 1850), 'publisher_timestamp': (dtype('float64'), 8), 'emergency': (dtype(('u1', (19,))), 43), 'q_poti': (dtype(('<f8', (19,))), 442), 'timestamp': (dtype('float64'), 0), 'cmd_state': (dtype('float64'), 1938), 'is_collision_avoidance_enabled': (dtype('uint8'), 1479), 'q_act_link': (dtype(('<f8', (20,))), 3555), 'controller': (dtype(('u1', (19,))), 24), 'left_hand_state': (dtype('uint8'), 3813), 'T_head': (dtype(('<f8', (12,))), 2170), 'right_hand_state': (dtype('uint8'), 3812), 'tau_ext': (dtype(('<f8', (44,))), 1482), 'f_spring_left': (dtype(('<f8', (6,))), 3763), 'tau_act': (dtype(('<f8', (19,))), 746), 'hand_q_act': (dtype(('<f8', (24,))), 1090), 'platform_have_command': (dtype('uint8'), 3554), 't4_position': (dtype('float64'), 2297), 'q_act': (dtype(('<f8', (19,))), 290), 'appl_id': (dtype('int32'), 1474), 'T_left': (dtype(('<f8', (12,))), 3457), 'Hld': (dtype(('<f8', (12,))), 2074), 'T_right': (dtype(('<f8', (12,))), 3361)}
# use tname: 'topics_verbose_semantic_log_93' for 'topics/verbose.semantic_log/93'
# 
# for table topics_verbose_semantic_log_93
# dtype: {'publisher_timestamp': (dtype('float64'), 8), 'parameters': (dtype(('u1', (1028,))), 592), 'timestamp': (dtype('float64'), 0), 'start_time': (dtype('float64'), 1620), 'end_time': (dtype('float64'), 1628), 'action': (dtype(('u1', (512,))), 80), 'type': (dtype(('u1', (64,))), 16)}
# use tname: 'topics_world_repr_Antenna_frame_76' for 'topics/world_repr.Antenna.frame/76'
# 
# for table topics_world_repr_Antenna_frame_76
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'frame': (dtype(('<f8', (12,))), 16)}
# use tname: 'topics_world_repr_Antenna_Drawer_frame_80' for 'topics/world_repr.Antenna_Drawer.frame/80'
# 
# for table topics_world_repr_Antenna_Drawer_frame_80
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'frame': (dtype(('<f8', (12,))), 16)}
# use tname: 'topics_world_repr_DIP_frame_78' for 'topics/world_repr.DIP.frame/78'
# 
# for table topics_world_repr_DIP_frame_78
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'frame': (dtype(('<f8', (12,))), 16)}
# use tname: 'topics_world_repr_Floor_frame_88' for 'topics/world_repr.Floor.frame/88'
# 
# for table topics_world_repr_Floor_frame_88
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'frame': (dtype(('<f8', (12,))), 16)}
# use tname: 'topics_world_repr_Justin_frame_91' for 'topics/world_repr.Justin.frame/91'
# 
# for table topics_world_repr_Justin_frame_91
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'frame': (dtype(('<f8', (12,))), 16)}
# use tname: 'topics_world_repr_Lander_frame_82' for 'topics/world_repr.Lander.frame/82'
# 
# for table topics_world_repr_Lander_frame_82
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'frame': (dtype(('<f8', (12,))), 16)}
# use tname: 'topics_world_repr_Lower_Drawer_frame_86' for 'topics/world_repr.Lower_Drawer.frame/86'
# 
# for table topics_world_repr_Lower_Drawer_frame_86
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'frame': (dtype(('<f8', (12,))), 16)}
# use tname: 'topics_world_repr_Map_frame_85' for 'topics/world_repr.Map.frame/85'
# 
# for table topics_world_repr_Map_frame_85
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'frame': (dtype(('<f8', (12,))), 16)}
# use tname: 'topics_world_repr_Motivator_frame_87' for 'topics/world_repr.Motivator.frame/87'
# 
# for table topics_world_repr_Motivator_frame_87
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'frame': (dtype(('<f8', (12,))), 16)}
# use tname: 'topics_world_repr_Panel1_frame_92' for 'topics/world_repr.Panel1.frame/92'
# 
# for table topics_world_repr_Panel1_frame_92
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'frame': (dtype(('<f8', (12,))), 16)}
# use tname: 'topics_world_repr_Panel3_frame_81' for 'topics/world_repr.Panel3.frame/81'
# 
# for table topics_world_repr_Panel3_frame_81
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'frame': (dtype(('<f8', (12,))), 16)}
# use tname: 'topics_world_repr_SPU1_frame_77' for 'topics/world_repr.SPU1.frame/77'
# 
# for table topics_world_repr_SPU1_frame_77
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'frame': (dtype(('<f8', (12,))), 16)}
# use tname: 'topics_world_repr_SPU2_frame_90' for 'topics/world_repr.SPU2.frame/90'
# 
# for table topics_world_repr_SPU2_frame_90
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'frame': (dtype(('<f8', (12,))), 16)}
# use tname: 'topics_world_repr_SPU2_Door_frame_89' for 'topics/world_repr.SPU2_Door.frame/89'
# 
# for table topics_world_repr_SPU2_Door_frame_89
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'frame': (dtype(('<f8', (12,))), 16)}
# use tname: 'topics_world_repr_SPU2_Drawer_frame_79' for 'topics/world_repr.SPU2_Drawer.frame/79'
# 
# for table topics_world_repr_SPU2_Drawer_frame_79
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'frame': (dtype(('<f8', (12,))), 16)}
# use tname: 'topics_world_repr_SPU3_frame_83' for 'topics/world_repr.SPU3.frame/83'
# 
# for table topics_world_repr_SPU3_frame_83
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'frame': (dtype(('<f8', (12,))), 16)}
# use tname: 'topics_world_repr_last_update_84' for 'topics/world_repr.last_update/84'
# 
# for table topics_world_repr_last_update_84
# dtype: {'timestamp': (dtype('float64'), 0), 'publisher_timestamp': (dtype('float64'), 8), 'counter': (dtype('float64'), 16), 'time': (dtype('float64'), 24)}
# 
## cell 6 end ##
## cell 7 input ##

table = db.tables["services/test_service/2"]
%% table.meta["service.provider"]
rows = table.mmap()
for row in rows[:2]:
    for k, v in zip(rows.dtype.names, row):
        print "  %s: %r" % (k, v)
    for k in "req_data", "resp_data":
        print "  %s: %r" % (k, table.get_sub(k, row))
    print "  peer: %r" % peers.get_sub("address", peer_data[row.peer_id]).tostring()
    print
## cell 7 output ##
# 
# Traceback (most recent call last):
#   File "<string>", line 2, in <module>
# KeyError: 'services/test_service/2'
## cell 7 end ##
## cell 8 input ##
% 8+1+1+8+8+8+4+4+4+8+8+4+8+4
## cell 8 output ##
# 8+1+1+8+8+8+4+4+4+8+8+4+8+4: 78
# 
## cell 8 end ##
## cell 9 input ##
peers = db.tables["services/peers"]
% peers.mmap()
%% peers.meta
for row in peers:
    % peers.get_sub("address", row).tostring()
## cell 9 end ##
## cell 10 input ##
#db = "/tmp/new-big/topics/alfred.telemetry/956"
#db = "/tmp/new-small/topics/alfred.telemetry/94"
db = "/tmp/test/topics_test.topic_1_0"
base = "/home/flo/workspace/ln_base_lnrecorder_field_storage/subtypes"
db = os.path.join(base, "topics_test.topic_1_0")
meta_fn = os.path.join(db, "meta")
data_fn = os.path.join(db, "data")

with open(meta_fn, "rb") as fp:
    meta_text = fp.read()
% meta_text
"""
ret = dict()
for line in meta_text.split("\n"):
    if not line.strip(): continue
    key, value = line.split(": ", 1)
    ret[key] = eval(value)
%% ret
"""
import yaml
meta = yaml.load(meta_text, Loader=yaml.SafeLoader)
%% meta
## cell 10 output ##
# meta_text: name: 'topics/test.topic/1/0'
# endianess: 'little'
# topic.id: 0
# topic.first_seen: 1584351423.043929
# topic.name: "test.topic/1"
# topic.md: "lnrecorder_test/topic1"
# topic.msg_def: "{\x27resp_fields\x27: [], \x27defines\x27: {\x27other\x27: [[\x27uint32_t\x27, \x27count\x27, 1], [\x27double\x27, \x27time\x27, 1], [\x27double\x27, \x27depth\x27, 25000]]}, \x27fields\x27: [[\x27uint32_t\x27, \x27count\x27, 1], [\x27double\x27, \x27time\x27, 1], [\x27other\x27, \x27last\x27, 1], [\x27other\x27, \x27last3\x27, 3]]}"
# topic.msg_def_hash: "2fb1fd3459aa3e83d450797385e3a114b4b512831471cb22b970b8963a31391b"
# topic.publisher: "test publisher 1"
# table-format: "fixed"
# field-description: [['timestamp', 'f8'], ['publisher_timestamp', 'f8'], ['count', 'u4'], ['time', 'f8'], ['last', [['count', 'u4'], ['time', 'f8'], ['depth', 'f8', 25000]]], ['last3', [['count', 'u4'], ['time', 'f8'], ['depth', 'f8', 25000]], 3]]
# fixed-table-record-size: 800076
# topic.last_seen: 1584351429.740791
# 
# meta: {'endianess': 'little',
#  'field-description': [['timestamp', 'f8'],
#                        ['publisher_timestamp', 'f8'],
#                        ['count', 'u4'],
#                        ['time', 'f8'],
#                        ['last',
#                         [['count', 'u4'],
#                          ['time', 'f8'],
#                          ['depth', 'f8', 25000]]],
#                        ['last3',
#                         [['count', 'u4'],
#                          ['time', 'f8'],
#                          ['depth', 'f8', 25000]],
#                         3]],
#  'fixed-table-record-size': 800076,
#  'name': 'topics/test.topic/1/0',
#  'table-format': 'fixed',
#  'topic.first_seen': 1584351423.043929,
#  'topic.id': 0,
#  'topic.last_seen': 1584351429.740791,
#  'topic.md': 'lnrecorder_test/topic1',
#  'topic.msg_def': "{'resp_fields': [], 'defines': {'other': [['uint32_t', 'count', 1], ['double', 'time', 1], ['double', 'depth', 25000]]}, 'fields': [['uint32_t', 'count', 1], ['double', 'time', 1], ['other', 'last', 1], ['other', 'last3', 3]]}",
#  'topic.msg_def_hash': '2fb1fd3459aa3e83d450797385e3a114b4b512831471cb22b970b8963a31391b',
#  'topic.name': 'test.topic/1',
#  'topic.publisher': 'test publisher 1'}
# 
## cell 10 end ##
## cell 11 input ##
data_dtype = get_numpy_dtype(meta)
% data_dtype.names
%% data_dtype.itemsize == meta["fixed-table-record-size"]
## cell 11 output ##
# data_dtype.names: ('timestamp', 'publisher_timestamp', 'count', 'time', 'last', 'last3')
# data_dtype.itemsize == meta["fixed-table-record-size"]: True
# 
## cell 11 end ##
## cell 12 input ##
data = memmap(data_fn, data_dtype, "r")
recdata = data.view(recarray)
%% type(recdata)
## cell 12 output ##
# type(recdata): <class 'numpy.recarray'>
# 
## cell 12 end ##
## cell 13 input ##
%% recdata.shape
%% recdata.shape[0] * data_dtype.itemsize
i = 5
%% recdata[i].count
%% recdata[i].time
%% recdata[i].last.dtype.names
%% recdata[i].last.count
%% recdata[i].last.time
%% recdata[i].last.depth.shape
%% recdata[i].last.depth[0], recdata[i].last.depth[-1]
%% recdata[i].last3.dtype
for last in recdata[i].last3:
    %% last.count
    %% last.time

## cell 13 output ##
# recdata.shape: (877,)
#  recdata.shape[0] * data_dtype.itemsize: 701666652
#  recdata[i].count: 6
#  recdata[i].time: 1584351423.076522
#  recdata[i].last.dtype.names: ('count', 'time', 'depth')
#  recdata[i].last.count: 5
#  recdata[i].last.time: 1584351423.070466
#  recdata[i].last.depth.shape: (25000,)
#  recdata[i].last.depth[0], recdata[i].last.depth[-1]: (5.0, 5.0)
#  recdata[i].last3.dtype: dtype((numpy.record, [('count', '<u4'), ('time', '<f8'), ('depth', '<f8', (25000,))]))
#  last.count: 5
#  last.time: 1584351423.070466
#  last.count: 4
#  last.time: 1584351423.062638
#  last.count: 3
#  last.time: 1584351423.058365
# 
## cell 13 end ##
## cell 14 input ##
# check for errors
% (diff(recdata.last.depth[:, -1]) != 1).any()
% (recdata.last.depth[:, -1] != recdata.last.depth[:, 0]).any()
## cell 14 output ##
# (diff(recdata.last.depth[:, -1]) != 1).any(): False
# (recdata.last.depth[:, -1] != recdata.last.depth[:, 0]).any(): False
# 
## cell 14 end ##
## cell 15 input ##
figure(1)
clf()
grid(True)
plot(recdata.hand_tau_act.min(axis=0))
plot(recdata.hand_tau_act.max(axis=0))

plot(recdata.hand_tau_act.mean(axis=0))
## cell 15 end ##
