#!/usr/bin/python

from __future__ import print_function
import time
import sys
import os
import numpy as np
tests = os.path.dirname(os.path.abspath(__file__))
lnrecorder = os.path.dirname(tests)
ln_runtime = os.path.dirname(lnrecorder)
base = os.path.dirname(ln_runtime)
pp = os.path.join(base, "python")
if pp not in sys.path:
    sys.path.insert(0, pp)

import lnrdb

pcap = "big"
target_fn = "big_db"

lnrecorder = os.path.join(base, "build/ln_runtime/lnrecorder/lnrecorder")

a = time.time()
os.system("%s convert '%s' '%s'" % (lnrecorder, pcap, target_fn))
b = time.time()
print("conversion time: %.3fs" % (b - a))

db = lnrdb.Db(target_fn)
table = db.tables["topics/alfred.telemetry/956"]
data = table.mmap()
clock_diff = np.diff(data.clock, axis=0)
print("clock_diff shape: %s, clock ptp: %.3fms, mean: %.3fms, std: %.3fms" % (
    clock_diff.shape,
    clock_diff.ptp() * 1e3,
    clock_diff.mean() * 1e3,
    clock_diff.std() * 1e3))

c = time.time()
print("only statistics: %.3fs" % (c - b))

print("complete runtime: %.3fs" % (c - a))
