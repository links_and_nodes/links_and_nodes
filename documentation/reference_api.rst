********************
Client API Reference
********************

The library is available for different programming languages, namely
C, C++, and Python. The next sections shall describe how to use the
specific language-bindings. Thereafter all available functions are
described.


.. toctree::
   :maxdepth: 1
   :caption: Contents:

   reference_python
   reference_cpp
   reference_c
   ln_client_environment
