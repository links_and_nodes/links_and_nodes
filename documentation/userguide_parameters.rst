C++ Parameters
==============
.. highlight:: cpp

.. default-domain:: cpp

Parameters allow to view and modify data from running programs. They consist of an input and output signal
where the output signal can be overridden either from the ln manager or from python clients.

Creating C++ Parameters is a two-step process. You first need to instantiate a parameter block

.. cpp:function:: ln::parameters::parameter_block(ln::client *clnt_, std::string parameter_group_name_, bool always_publish_ = false, std::string topic_name_ = "")

and then register parameters to it using

.. cpp:function:: template<typename... Types> ln::parameters::parameter_block::register_parameters(port_description<Types>... port_descriptions)

Note that this function can only be called once, you cannot register more than one set of parameters to a `parameter_block`.
It is easiest to supply brace-enclosed initializer lists to this functions, e.g. `{"sig_name", &sig_in, &sig_out, before_update_callback, after_update_callback}`.
The callbacks can also be omitted and are explained in the next section. Please see the following code example for details. Inside your main loop, you then have to cyclically call

.. cpp:function:: ln::parameters::parameter_block::update()

to allow overriding parameters.

.. sourcecode:: c++

        #include <chrono>
        #include <thread>
        #include <iostream>
        
        #include <ln/ln.h>
        #include <ln/parameters.h>
        #include <ln/parameters_eigen.h>
        
        int main(void) {
            // Init the client
            ln::client clnt("ln_parameters_example");
        
            // Create parameter block
            ln::parameters::parameter_block params(&clnt, "ln.parameters.example.cpp");
        
            // Signal
            double sig_in = 0, sig_out = 0;
        
            params.register_parameters<double>({"sig", "human-readable sig description", &sig_in, &sig_out});
        
            // Loop
            while (true) {
                std::this_thread::sleep_for(std::chrono::seconds(5));
                sig_in += 1;
                params.update();
        
                std::cout << "sig input: " << sig_in << ", sig output: " << sig_out << std::endl;
            }
        }
        

Parameter Callbacks
^^^^^^^^^^^^^^^^^^^
As mentioned above, you can supply the two callbacks

.. cpp:function:: std::function<bool(T &)> before_update_callback
.. cpp:function:: std::function<void(T &)> after_update_callback

where the function `before_update_callback` is called from the service handler when a new override arrives. This function can be used to check the parameter value for validity,
if the value is e.g. out of range it can `return false;` and the update is rejected. Otherwise it has to `return true;` to accept the update.

The function `after_update_callback` is called within your main loop's call to `update()` after a parameter has been updated. It can be used to recompute dependant values.

Debugging Parameters
^^^^^^^^^^^^^^^^^^^^
The env variable `LN_PARAMETER_DEBUG` can be set to enable some debug output when using ln parameters (also for Simulink models).
