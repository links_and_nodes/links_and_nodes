``topic`` section
=================

``topic``-sections are used to specify per-topic settings. these include
per-host scheduling priority, policy, cpu-affinities,
endianess-swap-preferences and preferred networks for udp or tcp transport.

scheduling settings only affect ln_daemon threads. the actual publisher or
subscriber threads of an application need to be controlled by other means
(see the :ref:`process priority setting<process-priority>` for one option)

``topic``-sections are optional, usually only needed for more fine grained
real-time tuning.

syntax::

  TOPIC_SECTION := "topic " + TOPIC_NAME + "\n" + PER_HOST_SETTING*
  PER_HOST_SETTING = HOST_NAME + ": " + SETTINGS + "\n"
  SETTINGS := SETTING + [ ", " + SETTING ]*
  SETTING := SETTING-NAME + "=" + SETTING-VALUE

variable substitution will be performed on ``TOPIC_NAME``, ``HOST_NAME`` and ``SETTINGS``.

multiple ``topic``-sections for the same ``TOPIC_NAME`` can be used to add more
per-host settings. if the same ``HOST_NAME`` is specified multiple times for the
same topic, then only the last mentioned setting will have effect.

example:
  
.. code-block:: lnc

  topic robot_telemetry
  localhost: policy=fifo, priority=55, affinity=0x04, preferred_network=rtnet, offer_endianess_swap
  robot: priority=75, affinity=2, preferred_network=rtnet
  view_node: priority=4, preferred_network=rtnet

  topic robot_telemetry
  view_node: priority=40

in this example for topic ``robot_telemetry`` a UDP transport from
``robot`` to ``localhost`` would use the ``rtnet``. the receiver thread on
localhost will have fifo prio 55 with affinity 0x04 (only cpu-id 3). the sender
thread on ``robot`` will have fifo prio 75 on cpu-id 2. a subscriber on
``view_node`` would cause a UDP-receiver thread to have priority 40 WITHOUT any
network-preferences (the 2nd mention of ``view_node`` overwrites the first one).

scheduling settings are also used for subscriptions where the publisher is on
the same host, IFF the subscriber specifies a ``rate != -1``. (only then
ln_daemon needs to span a thread)

valid settings:

policy
------
which scheduling policy to use for ln_daemon-threads that work on this topic.
typical values:

- ``FIFO``
- ``RR``
- ``NORMAL``
- ``BATCH``

ln_daemon threads are used for UDP/TCP-senders, -receivers and whenever a
subscriber uses a subscription rate other than ``-1`` (full rate).

priority
--------
scheduling priority to be used for threads working on this topic. for linux and
the FIFO-policy this is an integer value from 0 to 99 where 99 is the highest
possible priority.

affinity
--------
specifies which cpu affinity to use for threads that work for this topic.
only if specified as hex-value it is interpreted as a bitmask (provide the
``0x``-prefix). other number formats are interpreted as list of cpu-id's
(first cpu has id 0).

bitmask examples::
  0x01: only cpu-id 0
  0x02: only cpu-id 1
  0x04: only cpu-id 3
  0x08: only cpu-id 4
  0x10: only cpu-id 5
  0x12: cpu-id 5 or cpu-id 1

preferred_network
-----------------
name of a defined/known network to use for transports of this topic.
this is useful if there are multiple possible network connections between a
publisher and subscriber.


offer_endianess_swap
--------------------
if this flag is present then the named host will be used to do the
endianess-swap if needed between hosts of different endianess.
if not specified, ln_manager will decide which side does the endianess swap.


deny_endianess_swap
-------------------
if this flag is present then the named host will NOT be used to do the
endianess-swap if needed between hosts of different endianess.
if not specified, ln_manager will decide which side does the endianess swap.

