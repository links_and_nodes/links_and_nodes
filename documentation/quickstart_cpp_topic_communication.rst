.. index::
   single: topic communication; example (C++)
   
Topic Communication in C++
==========================

.. contents::

.. note::

   The example uses C++14. If you need to know more about which
   versions of C++ are supported, see section
   :ref:`tutorial/prerequisites/language_standards` in the
   tutorial. Section :ref:`tutorial/prerequisites/installation` gives
   detailes about the required installation.

   
This section shows a minimal, self-contained example of how
:term:`processes <process>` can exchange information in
:term:`real-time`, via :term:`topics <LN topic>` which exchange
:term:`messages <message>` using a :term:`publish/subscribe`
pattern. (If you want to look up what these terms mean, they are
defined in the :ref:`communication <intro/communication>` bullet point
of the :doc:`introduction` chapter).

.. note::

   This chaper mostly matches the respective example in Python, so if
   you need to get started on topics communication in Python, please
   look into :doc:`quickstart_python_topic_communication` instead.


To summarize, the main points we will demonstrate are these:

* Different processes can exchange information via messages, which are
  compound data structures a fixed data type.
* Messages are associated with topics, which is basically a label for
  a specific stream of messages
* One process can send messages. It is the :term:`publisher` in respect
  to that topic.
* Other processes (one or more) can receive messages, they are :term:`subscribers <subscriber>`.

We will show now how that works with an example in C++. The first
thing we need to do is to define what data elements the messages we
want to send will contain.  This is done using a piece of text
information which we call a :term:`message definition`.
 
Creating Message Definitions
----------------------------

.. index::
   single: message definitions; simple example (C++)


Message definitions are usually stored in plain text files which are
part of the set-up of a distributed system.

So, to create a message definition with name
``counter/count_message``, we create a plain ASCII text file with this
content:

.. literalinclude:: examples/quickstart/cpp/topics/msg_defs/counter/count_message

and store this text file in :file:`quickstart/cpp/topics/msg_defs/counter/count_message`.

What does the content of the message definition mean? it means that we have
two fields:

* time
* num_counts

and that the "time" field has the type `double` (which is a 64-bit floating point value),
and the "num_counts" field has the type `uint32_t` (which is the C type name for
an unsigned integer value with 32 bits).

So, we can imagine messages of this type as a packet of data which has
the same structure as a C or C++ struct: Each name is mapped to a
value, and each value has a fixed type.

However, we can use the same definition in other languages such as
Python, so that we can beam them back and forth between different
processes and processes on different computers. Also, we can even
share messages between systems which have a different
:term:`endianness`, for example.


The Publisher Program
---------------------

.. index::
   single: topic publisher; quickstart example (C++)


We start with the publisher programm, which we call :program:`counter_publisher`,
by creating a source code file :file:`quickstart/cpp/topics/counter_publisher.cpp` like this:

.. literalinclude:: examples/quickstart/cpp/topics/counter_publisher.cpp
   :language: c++
   :linenos:

Let's go just through a few interesting aspects of the script:

* In line 2, the ``links_and_nodes`` C++ header is imported as
  ``ln/ln.h`` (it works also for C).

.. only:: rmc
	  
	  .. note::

	     The ``links_and_nodes`` module needs to be provided by
	     the package manager that you use. If you use Conan, you
	     can provide it using a conan python environment. Please
	     look into the Conan examples for further information
	     about this.
	     
* The header ``ln_messages.h`` is generated from the
  message definitions, and defines the type ``counter_mount_message_t``
  which is the type of the message buffer. That buffer is
  allocated on the stack in line 25, and zero-initialized using "{}".
  
* in line 17, an :cpp:class:`ln::client` instance is initialized. The
  parameter that is passed for the initialization is the default name
  of the client, which can be used by the LN Manager to display its
  state.

* In line 18, a publisher :term:`port <port object>` is initialized, using the
  :cpp:func:`ln::client::publish()` method. The method returns a handle to the port,
  and the object is owned by the LN client instance. Note that the name of the message
  definition and the name of the topic that uses it are distinguished:
  The two arguments are the names of the topic which is used
  (``counter1.count_message``), and the name of the message definition
  (``counter/count_message``).

  Here, the name of the topic is almost equal to the name of the
  message definition.  However this is optional: While it is often
  practical and makes things simpler to keep names in sync, they can
  be different. The reason for this is that message definitions and
  topic names serve two different purposes: The topic is like the
  frequency of a radio broadcast, which determines which content we
  will get where, and under which title. (For further reading, the
  section on :ref:`Message definitions and topics
  <tutorial/intro/topics>` in the tutorial gives a more in-depth
  explanation of the relationships between message definitions,
  topics, and their names.)

  The message definition is, in contrast, just the *type* of the transmitted
  data. This means that one can use the same message definitions in
  different places. For example, a message definition which defines
  a vector of three floating point elements could be used
  to signal the place of a vehicle, its speed vector, its acceleration,
  and perhaps also the place it should go to.

* After the initialization, the program starts its main processing
  loop in line 23. This is interspersed with any kind of activity (like the ``sleep`` command),
  and then generating and sending data. The latter is done in two steps:

* The assignment to ``out_data.time`` puts data into the message
  buffer.  The elements of the message are member values of
  ``out_data``, and the ``time`` field is defined by the message
  definition. In the same way, the assignment to ``out_data.num_counts``
  generates some data which is updated within each step. 

* When the message data is prepared, it is sent using the
  :func:`ln::outport::write()` method. This function :term:`atomically <atomic>`
  transmits the message and returns after the transmission has been
  initiated. Both ``port.write()`` and :func:`ln::outport::read()` shown below are
  safte to use from multiple threads.

  
      
The Subscriber Program
----------------------

.. index::
   single: topic subscriber; quickstart example (C++)


The folliwing code, in :file:`quickstart/cpp/topics/counter_subscriber.cpp` is an example
for a matching subscriber program:

.. literalinclude:: examples/quickstart/cpp/topics/counter_subscriber.cpp
   :language: c++
   :linenos:

* The initialization of the program is very similar to the
  initialization of the publisher, with one difference: the method
  ``clnt.publish(...)`` is replaced by the method
  :cpp:func:`ln::client::subscribe()` in line 15. This means that this
  port will wait for data which was written by the publisher process.

  .. index::
     pair: initializing clients; timing and performance considerations
     
  .. note:: This port initialization is done only once. This is
	    recommended, since the initialization of a publisher or
	    subscriber client (or other communication channels) can
	    cost much more time than using the communication link.

* The main loop uses the :func:`ln::outport::read()` method in order
  to receive data. If there is no data available, the client will wait
  for a maximum of ``time_out`` seconds, and return ``false`` if no
  data was received.

* When the ``port.read()`` method has completed, it returns a packet
  data in the ``in_data`` buffer, whose members ``time`` and ``num_counts``
  are populated with the values that have been sent by the publisher.
  If no data was received by ``port.read()``, the existing data in
  the buffer will remain unchanged.


Configuring the LN Manager for Message Exchange
-----------------------------------------------

.. index::
   triple: quickstart (C++); message definitions; configuring the LN manager
   triple: quickstart (C++); message definitions; LN manager configuration


Now, we add a minimal configuration for the LN manager that supports
exchange of these messages. First, we will do that in a bare minimum
way, without using the manager for process management. After that, we
will add automatic process management again.

Create a ln-manager config file :file:`quickstart/cpp/topics/main.lnc`:

.. literalinclude:: examples/quickstart/cpp/topics/main.lnc
   :language: lnc
   :linenos:

This creates a unique instance name and manager listening port.  As in
the previous `quickstart example on basic process configuration
<quickstart/basic_process_configuration>`, it sets the instance name
and the port number derived from environment variables.

In addition to that example, there is also a directive which
tells the LN manager where it can find the message
definition which we defined in the first step. This
is necessary because the LN manager needs to start
some background processing that supports message
exchange, and for this it needs to know
the message definitions which we are using.

.. index::
   pair: quickstart example (C++); %(CURDIR) variable

The ``%(CURDIR)`` expression defines the folder in which the search
path starts, which is the folder in which the configuration file which
we are discussing is included. We have put the message definition into
the folder "msg_defs`, so we need to add its name. Once the LN manager
knows this folder, it and the client programs can find the correct
message definition, as the rest of the path is defined in the name of
the message definition.  (And, of course, it is possible to use
multiple folders with message definitions, so that we can compose
larger systems from smaller ones).


Finally, we need a short Makefile, like this:

.. literalinclude:: examples/quickstart/cpp/topics/Makefile
   :language: make
   :linenos:


Running Publisher and Subscriber  with manual Process Management
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. index::
   pair: quickstart (C++); process management; manual

First, compile both publisher and subscriber, using "make"::


.. code:: bash

  cd documentation/examples/quickstart/cpp/topics/
  make clean
  make


Start the ln_manager in one terminal:

.. code:: bash

  ln_manager -c quickstart/cpp/topics/main.lnc

.. on rmc-host:
.. source rmc/conan_source ln_manager
.. cd documentation/examples
.. ../../python/links_and_nodes_manager/ln_manager -c quickstart/cpp/topics/main.lnc
  
The manager will open its GUI as shown below. In the terminal, it will
output a log-message similar to ``Manager: listening on '<hostname>:<port>' for
ln-clients!``, we need the ``<port>``.

.. figure:: images/quickstart_ln_manager_on_start.png

	    The LN manager serving a publisher and subscriber

In another terminal, start the publisher:

.. code:: bash

  cd documentation/examples/quickstart/cpp/topics/
  
  LN_MANAGER=localhost:<port> python3 ./counter_publisher 

.. on rmc-host:
   source rmc/conan_source ln_manager
   export PYTHONPATH=$(pwd)/python:$PYTHONPATH
   cd documentation/examples

If all goes right, no output should be generated except ``"counter publisher running"``.

In a third terminal, now start the subscriber:

.. code:: bash

  LN_MANAGER=localhost:<port> ./counter_subscriber

The process should output lines like this::

  time: 1647435215.0, num_counts: 15
  ...

Within the ln-manager GUI, when clicking on the "topics" tab, you
should see our ``counter`` topic published with a rate of ~10Hz as
shown below:

.. figure:: images/quickstart_ln_manager_with_subscriber.png

	    The counter topic, when selecting the "topics" tab.

	    This shows in the top half a list of topics. In the
	    bottom half, after selecting the "counter" topic,
	    it shows a list of the clients and the averaged
	    frequency of exchanged messages.


Delegating the Process Management to the LN Manager
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. index::
   pair: quickstart (C++); process management; by LN manager
   pair: quickstart (C++); process management


The example above requires that we start the processes with the right
port number, so that they can reach the manager.  This is not very
practical, as we need to copy the port number each time.

We can do this with less manual typing, and in a more
comfortable way, when we use the LN manager for process
management, as shown in section :ref:`Process Management`.

To do this, we can put those command lines in the ln-manager config
file so that we don't have to remember them:

.. literalinclude:: examples/quickstart/cpp/topics/topics_with_process_definitions.lnc
   :linenos:
   :language: lnc

The example uses a few additional flags for the processes. The details are explained
in the reference part in :doc:`config_process`. To explain and highlight
the flags and attributes used here:

.. index::
   pair: passing environment variables; quickstart example


* as before, each process has a name, which is given at the start of
  each process section. This name overrides the name by the LN client,
  which is passed as constructor parameter.
* the ``change_directory`` directive defines the working directory of
  the process. This makes it easy to start the compiled programs, whose name is a
  parameter with a relative path to the python command.
* The LN manager keeps the :term:`environment` of each process as
  clean as possible. However, certain environment variables sometimes
  need to be passed, so that the program can find shared
  libraries. These are defined with the directive
  ``pass_environment``, which for example, tells the LN Manager to
  pass the environment variable ``LD_LIBRARY_PATH`` to the defined
  process. This can be necessary if a required shared library is
  installed in a non-standard location, like $HOME/.local/lib.
* As shown in the previous quickstart chapter
  :doc:`quickstart_python_process_management` in section
  :ref:`quickstart/basic_process_configuration`, we can define
  dependencies between processes which need being started in a
  specific order.
* The instruction "depends_on: publisher" tells the LN manager that
  the "publisher" process needs to be started first, before the
  "subscriber" process is started.
* Here, the directive "ready_regexp" for the publisher tells the LN
  manager to wait for that specific string in the
  output of the publisher before it starts the subscriber.    

.. seealso::

   Details on the last two points can be found in page
   :doc:`config_process`.

.. only:: rmc

      *Note on the RMC packaging system:* The environment could also
      be managed by the packaging system. Especially, Cissy/Conan pass
      information about library and executable paths in the
      environment, and pass them automatically to LN, using the
      ``cissy run`` command and cissy workspaces. Please refer to the
      cissy examples for detailed examples on this.
   

  
After starting both processes you should get something like this:

.. figure:: images/quickstart_ln_manager_topics_processes_running.png

	    Processes communicating as publisher and subscribe which are managed by the LN Manager


A final note: We have simplified a lot to make the main points
clearer. The chapters :doc:`tutorial_cpp` and :doc:`user_guide` gives
a lot more information on details. One thing worth noting here is that
a process can be both a subscriber and a publisher, and can of course
access more than one topic.
	    
	    
.. seealso:: See section
   :ref:`guide/hints_on_using_message_definitions` for some hints on
   how to use message definitions effectively.


  

  
