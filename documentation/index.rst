.. https://docutils.sourceforge.io/docs/ref/rst/restructuredtext.html
.. https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html
.. https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html
.. https://pygments.org/docs/lexers/
   
Welcome to links_and_nodes documentation!
=========================================

Version: |release|

.. toctree::
   :maxdepth: 3
   :numbered:
   :caption: Contents:

   introduction
   getting_started
   quickstart
   tutorial
   user_guide
   reference
   changelog
   glossary
   howto_report_lnm_bug.rst
   appendix

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
