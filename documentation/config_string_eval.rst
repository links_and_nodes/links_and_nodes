string evaluation
=================

within the config file strings are used for all values. these
value-strings will be evaluated while the config file is read. strings can
optionally be delimited by double-quotes ``"`` - most of the time this is
not necessary, but sometimes they help the config-parser to get things
right.
the evaluation will replace ``%()``-references to :ref:`defines` with their
values. example:

.. code-block:: lnc

   defines
   my_name: flo
   message: Hello %(my_name)

here ``message`` will evaluate to the string ``Hello flo``.

these ``%()``-references can not only refer to user-defined defines, but
also to builtin-defines, builtin-functions and user-functions.

builtin-defines
---------------

manager
  ``HOST_OR_IP:PORT`` of managers TCP-listening-socket
instance_name
  name of this ln_manager instance as defined by the
  ``instance_name:``-field of the ``instance``-section described below.
hostname
  hostname as reported from ``socket.gethostname()`` (host where manager runs)
CURDIR
  directory-name of this config file
CURRENT
  filename of this config file
_PROCESS
  display-name of currently(/or last)-defined ``process``-section
_FULL_PROCESS
  full name of currently(/or last)-defined ``process``-section
LN_LD_LIBRARY_PATH
  ``LD_LIBRARY_PATH`` as used by the ln_manager itself
LN_PYTHONPATH
  ``PYTHONPATH`` as used by the ln_manager itself
LN_PYTHON
  filename of python executable used by the ln_manager itself
LN_PREFIX
  prefix where ln_manager was installed to. (e.g. ``/usr/local`` or
  ``/usr`` or somewhere in your conan-cache)
LN_SHARE_DIR
  share-directoy of ln_manager
LN_BASE_MANAGER
  *base_dir* of the ln_manager itself

builtin-functions
-----------------
.. note::
   todo: ifeq?

env
  ``%(env NAME[, DEFAULT_VALUE])`` read value from ln_managers
  environment. if there is no variable of that name, return
  ``DEFAULT_VALUE`` if specified. if ``DEFAULT_VALUE`` is not specified a
  error will be generated if that variable does not exist.
  example: ``%(env "HOME")`` will evaluate to the contents of the HOME
  environment variable of the ln_manager's environment. it will throw an
  error if that variable does not exist...
ifip
  ``%(ifip IFNAME)`` will query the system's network interface ``IFNAME``
  and returns it's (first) IPv4 in dottet-decimal form. (uses linux SIOCGIFADDR ioctl)
shell
  ``%(shell CMD)`` executes a *shell command* via ``/bin/sh -c "CMD"``
  and evaluates to the produced output on stdout.
  example: ``%(shell date)`` will evaluate to the ln_managers systems
  current date in some locale's representation.
realpath
  ``%(realpath PATH)`` will evaluate to the return-value of python's ``os.path.realpath(PATH)``
dirname
  ``%(dirname PATH)`` will evaluate to the return-value of python's ``os.path.dirname(PATH)``
basename
  ``%(basename PATH)`` will evaluate to the return-value of python's ``os.path.basename(PATH)``
subst
  ``%(subst SEARCH, REPLACE, HAYSTACK)`` will evaluate to HAYSTACK but
  with all occourances of SEARCH replaced by REPLACE.
  example: ``%(substr "World", "Flo", "Hello World")`` will evaluate to
  ``Hello Flo``.
substring
  ``%(substring STR, START[, N])`` will evaluate to the sub-string of
  STR, skipping the first START-characters. if N is provided only
  N-characters will be returned (at most).
  example: ``%(substring "Hello World", 6, 3)`` will evaluate to ``Wor``.
length
  ``%(substring STR)`` will evaluate to the length of STR
strip
  ``%(strip STR)`` will evaluate to STR but with leading and trailing
  whitespace removed
findstring
  ``%(findstring SEARCH, HAYSTACK)`` will evaluate to SEARCH if HAYSTACK
  contains SEARCH, otherwise it evaluates to the empty string. (similar
  to make's ``$(findstring find,in)``)
ifndef
  ``%(ifndef DEFINE, IF_NOT_DEFINED)`` will evaluate to IF_NOT_DEFINED in
  case DEFINE is not a known define, otherwise to the value of DEFINE:

.. code-block:: lnc

   defines
   known_define: 42
   message: answer is %(ifndef "known_define", "unknown")!

``%(message)`` will evaluate to ``answer is 42!``.

ifexists
  ``%(ifexists PATHNAME, IF_TRUE, IF_FALSE)`` checks if PATHNAME exists
  on manager's filesystem and evaluates to either IF_TRUE or IF_FALSE. (uses the python function
  ``os.path.exists()``)  
get_port_from_string
  ``%(get_port_from_string ARGS...)`` will evaluate to a fixed port
  number between 1024 and 65535, depending on the value of ARGS....
get_host_ifip
  ``%(get_host_ifip NODENAME, IFNAME)`` will evaluate to the ip-address
  of named node (can be a node-name or a host-name) and interface.
  example:

.. code-block:: lnc

   hosts
   host1: eth0:192.168.0.1, eth1:192.168.1.1
   host2: eth0:192.168.0.2, eth1:192.168.1.2
   
   node_map
   rthost: host2
   
   defines
   ip10: %(get_host_ifip "host1", "eth0")
   # -> 192.168.0.1
   ip21: %(get_host_ifip "rthost", "eth1")
   # -> 192.168.1.2


runtime-variables
-----------------

aside from the above ``%()``-defines which are substituted when the config file is read, there are also some so-called
runtime-variables of the form ``$(NAME)``. these are substituted only shortly before the command is executed and they
can only be use in some contexts: in the ``command:``-string of a process or within a value of an ``environment:``-variable.

these runtime-variables exist:

``$(ARCH)``
  some string describing this hosts architecture. provided by the ln_daemon running on that host. can be configured via
  the ``arch:`` key in a hosts-line.

``$(gui_*)``
  some environment variable from (a possible remote) ln_manager gui. only selected env-vars are available.

``$(DISPLAY)`` ``$(XAUTHORITY)``
  values of the corresponding env-vars of the remote-gui or local manager gui. you probably want to use the
  :ref:`process/forward_x11`-flag instead.

``$(gui_ip)``
  ip-address of a connected remote gui.

``$(manager_host)``
  host-part of the instance-config ``manager:`` directive. you probably want to use ``$(LN_MANAGER)`` instead.

``$(manager_port)``
  listening port of the ln-manager (on the host where the manager is running). you probably want to use ``$(LN_MANAGER)`` instead.

``$(LN_MANAGER)``
  computed ``ip-address:port`` where to reach this ln_manager. this is usually automatically provided to all processes
  via the :envvar:`LN_MANAGER` environment-variable.
