########
Concepts
########

Here is a `presentation which explains main concepts of LN <https://wiki.robotic.dlr.de/mediawiki_wiki/images/6/61/Rmc_seminar_ln_2018.pdf>`_.


.. index::
   single: state objects; overview
   single: LN states
   single: managing system configuration using LN states


   
Topics and Services
===================
   
.. _guide/rules_for_topic_names:

Rules for Topic Names
---------------------


.. _guide/message_definitions:

Message Definitions
===================


   
state objects and their management
==================================


.. index::
   single: LN parameters; overview

.. _guide/parameters_concepts:

Overview on LN parameters
=========================


Clients
=======

.. _guide/concepts/clients/client-names:

Client Name Selection
---------------------

After initializing a client, the client name is selected as follows:

1. If the client process was started by the LN manager, the manager
   creates and passes an environment variable named
   :envvar:`LN_PROGRAM_NAME` to the process, with the name taken from
   the corresponding process section of the LN manager configuration.

2. If the client was not started by the LN manager, the LN client code
   tries to use the parameter which is passed in the client
   constructor as name of the client.

3. This requested name is passed to the LN manager, which does the
   registration, and watches that there is no name collision. If there
   is already another client with the same name registered for the
   same LN instance, the LN manager takes this suggested name and
   appends an integer number to it, until it reaches a unique name.

   
The rationale for this selection steps is that it is
possible to start independent processes with some parameters
which control different parts of the same robot,
for example a left and a right arm, or a specific wheel
on a four-wheeled robot. It is also possible that
clients defined in a library :term:`module` are sometimes running
in the same process, and sometimes running separately,
for example, for debugging, or in a entirely different
hardware configuration.
