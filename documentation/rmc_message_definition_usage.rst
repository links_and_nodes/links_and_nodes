 
How message definitions should be used
**************************************

.. warning::
   this is only relevant for LN versions >= 1.1 in a cissy/conan environment!

.. note::
   explaining comments in source-snippets might only be mentioned on first occurrence of a <construct>. so you might want
   to search backwards to <construct> to check for an explaining comment.

.. note::
   please keep in mind that for your first own conan packages at DLR-RM you probably don't want to immediately release
   them to the ``common/stable``-channel. this is done here just as an example. for testing you could use the
   ``common/snapshot`` channel.

.. note::
   todo: check [[Links and nodes/Message Definitions]] for a description of how to write your own message definitions.

.. note::
   todo: make sure that Matlab generator example is correct after https://rmc-github.robotic.dlr.de/tools/cissy/pull/420
   is merged

.. note::
   todo: search for all occurrences of "## issue-link" and check for outcome of issue and fix following lines
   careful: each "## issue-link" only has to be mentioned once. when fixing/changing, search for & fix other occurrences of
   same pattern!
   
scenario 1: C++ publisher
=========================

developer1 has a package PKG1 that has an executable ``EXE1`` which is publishing a topic with a fixed,
non-standard message-definition ``PKG1_major/MD1`` (this is the globally unique name of that message-definition!).

this message definition is stored in a file WITHIN that source package in a sub-directory like
``own_msg_defs/PKG1_major/MD1``. (because PKG1 is the authority to decide the contents of that message-definition)

at *BUILD TIME*
---------------
the build system wants to generate C-structs into a ``ln_messages.h`` file that match MD1 such that the
publisher-C++-code can easily create messages for this topic.
for this the build system should call ln_generate like this:

.. code-block:: sh

   ln_generate --md-dir ./own_msg_defs PKG1_major/MD1

as a rule within a Makefile:

.. code-block:: Makefile

   ln_messages.h: $(CURDIR)/own_msg_defs/PKG1_major/MD1
       ln_generate -o $@ --md-dir $(CURDIR)/own_msg_defs PKG1_major/MD1

or in a CMakeLists.txt file:

.. code-block:: cmake

   add_custom_command(
     OUTPUT ln_messages.h
     COMMAND ln_generate -f --md-dir ${CMAKE_SOURCE_DIR}/own_msg_defs PKG1_major/MD1
     DEPENDS ${CMAKE_SOURCE_DIR}/own_msg_defs/PKG1_major/MD1
   )

       
that ln_generate script is provided by the links_and_nodes_base_python conan package (pure python). so PKG1 has
a *BUILD TIME* dependency to links_and_nodes_base_python.

``EXE1`` also wants to use the C/C++ ln_client-library (headers & shared-lib) which is provided by the liblinks_and_nodes
conan package. so PKG1 has a *BUILD TIME* and a *RUNTIME* dependency to liblinks_and_nodes:

* ``ln/ln.h`` header in the include-path of the compiler
* ``libln.so`` for linking and the dynamic linker at runtime

Such that everyone can BUILD the package, we create a conan recipe (containing architecture specific conan settings) and
release it using the conan reference ``PKG1/1.2.0@common/stable``:

.. literalinclude:: examples/conan/PKG1_conanfile.py
   :language: python

at *RUNTIME*
------------
that conan recipe is also used to generate an environment such that everyone can *RUN* ``EXE1``:

* ``EXE1`` needs LD_LIBRARY_PATH to point to a ``libln.so``.
* **EXE1 does NOT need to know the path to MD1 at RUNTIME!**
* ``EXE1`` does *NOT* have a *RUNTIME* dependency to links_and_nodes_base_python!

for LN communication to work, the ln_manager will need to have filesystem access to all used message-definitions. that is
why we will create a new conan package to only include our ``own_msg_defs/`` and also add this new package to our own
requirements().

.. note::
   see below: cissy's ``requires_from_all``-feature can be used to collect all those packages for the
   ln_manager, but this only works if they appear somewhere in any of the dependency trees.

also, other developers might want to generate language-specific source-files for ``PKG1_major/MD1`` aswell (e.g. java
classes) at *BUILD TIME* of their packages.

so developer1 creates a 2nd conan recipe within the ``own_msg_defs/`` directory that only packages ``PKG1_major/MD1``
and releases it as ``PKG1_ln_msgdef/1.2.0@common/stable``. now others can use those message definitions in their
``requirements`` to automatically generate their own language specific sources from them. (it would be nice if those
other packages only need to mention this package in their ``build_requirements``, but the ``requires_from_all``-feature
does not support `this <https://rmc-github.robotic.dlr.de/tools/cissy/issues/415#issuecomment-22437>`__)

conan's package_info() for this recipe at ``own_msg_defs/conanfile.py`` looks like this:

.. literalinclude:: examples/conan/PKG1_msg_def_conanfile.py
   :language: python

as shown later, it is important to follow the convention to have the ``_ln_msgdef``-postfix in the name of these
packages.

	      
LN-communication
----------------
user1 wants to use ``EXE1`` and subscribe to its topic in user1's python script ``PY1.py``.

user1 needs to decide which version of PKG1 and ln_manager is to be used (ln_manager is needed for LN-communication).

all this needs to be written to files such that they can be version-controlled by git.

user1 creates a ``cissy_workspace.yaml`` file to specify which versions to use for these two packages. it looks
something like this::


   config:
     # to tell cissy to generate a ".inc.lnc" file per process at "install"-time (cissy deploy):
     generators: ln_env
     
     ## https://rmc-github.robotic.dlr.de/tools/cissy/issues/413
     ### to tell cissy how to reach mentioned nodes
     ##ssh_config: cissygen/my_ssh_config
     process_manager:
       # used to execute the ln_manager GUI when calling 'cissy run' within the workspace
       start_command: ln_manager -c my_setup.lnc
       
       ## https://rmc-github.robotic.dlr.de/tools/cissy/issues/413
       ### to generate an ssh config from ln host configuration
       ##ssh_config_generator: ln_manager -c my_hosts.inc.lnc --export-hosts cissygen/my_ssh_config
       
       requires:
        - links_and_nodes_manager/[~2]@common/stable
       requires_from_all: ".*_ln_msgdef"

       Here, you should replace
       ``links_and_nodes_manager/[~2]@common/stable`` by the current LN
       version, which is |DOCUMENTED_LN_MANAGER_VERSION|. 

   
   processes:
     PKG1:
       requires:
        - PKG1/[~1.2]@common/stable
       ## https://rmc-github.robotic.dlr.de/tools/cissy/issues/400
       ##node: computehost
       node: rmc-lx0142
   
     python2:
       requires:
        - python2-dist/[~1]@tools/stable
       ## https://rmc-github.robotic.dlr.de/tools/cissy/issues/400
       ##node: visionhost
       node: localhost

user1 also needs to create an ln_manager-config file ``my_setup.lnc`` to be able to start the ln_manager:

.. code-block:: lnc

   instance
   name: user1 setup
   manager: :%(get_port_from_string %(instance_name))

   ## https://rmc-github.robotic.dlr.de/tools/cissy/issues/413
   ##include my_hosts.inc.lnc
   include_glob cissygen/**/*.inc.lnc

   process exe1
   use_template: PKG1_gen
   command: EXE1 --with-some-arg -v
   ## https://rmc-github.robotic.dlr.de/tools/virtuallnenv/issues/10#issuecomment-22412
   node: rmc-lx0142
   
   process py1 script
   use_template: python2_gen
   change_directory: %(CURDIR)
   command: python PY1.py
   ## https://rmc-github.robotic.dlr.de/tools/virtuallnenv/issues/10#issuecomment-22412
   node: localhost

and a ``my_hosts.inc.lnc`` might look like this:

.. code-block:: lnc

   hosts
   hostXY: eth0: 192.168.132.2, additional_ssh_arguments: -i /some/secret/key

   node_map
   visionhost: hostXY
   computehost: hostZ
   
now user1 needs to make sure that all needed conan packages are installed on their respective hosts by running the
following command once on the manager's host::
  
  cissy deploy

this will execute all necessary ``conan install`` commands on their respective hosts.
  
now user1 can start the ln_manager via cissy with the command::
  
  cissy run

  
conan called by cissy will provide an ``LN_MESSAGE_DEFINITION_DIRS``-environment variable for the ln_manager that points
to the ``own_msg_defs/`` folder installed via the ``PKG1_ln_msgdef/1.2.0@common/stable`` package.

the contents of the generated ``cissygen/process/PKG1/rmc-lx0142.robotic.dlr.de/PKG1.inc.lnc`` file might look like this:

.. code-block:: lnc

   template PKG1_gen
   # always generated because with `conan` we have to tell the OS to always search the PATH env var for the executed commands:
   add flags: use_execvpe
   
   # generated because of the `requires: [ "PKG1/[~1.2]@common/stable" ]` entry in cissy_workspace.yaml:
   add environment: PATH=/volume/conan_cache/user1/.conan/data/PKG1..../bin

   # generated because of the `self.requires("liblinks_and_nodes/[~1]@common/stable")` line in PKG1-conanfile:
   add environment: LD_LIBRARY_PATH=/volume/conan_cache/user1/.conan/data/liblinks_and_nodes..../lib

   ## https://rmc-github.robotic.dlr.de/tools/virtuallnenv/issues/10#issuecomment-22412
   ### we already mentioned a node in our cissy_workspace.yaml file, so this is given here as a default:
   #node: computehost
   
   # node: localhost # only if there was no node mentioned in the workspace file to provide a nice default
   

scenario 2: C++ subscriber
==========================

developer2 wants to publish a PKG2 that includes an ``EXE2`` which is a C++ program subscribing to a topic with a message
definition of MD1.
   
to provide this C++ program a C-struct that exactly matches MD1, developer2's buildsystem uses ln_generate which expects
the ``LN_MESSAGE_DEFINITION_DIRS``-environment variable to be set to point to the ``own_msg_defs/`` folder installed by
the ``PKG1_ln_msgdef/1.2.0@common/stable`` package.

ln_generate DOES NOT need the ``--md-dir XY``-option from the publisher above. as a rule within a Makefile:

.. code-block:: makefile

   .PHONY: ln_messages.h # always build! ln_generate will only write on change!
   ln_messages.h:
       ln_generate -o $@ PKG1_major/MD1

to make sure that MD1 can be found at *BUILD TIME* of PKG2, developer2 writes a conanfile for his own package that looks
like this:

.. literalinclude:: examples/conan/PKG2_conanfile.py
   :language: python

and releases it as ``PKG2/2.1.0@common/stable``.


LN-setup
--------
user2 (not necessarily developer2) now wants to use ``EXE1`` and ``EXE2`` from some fixed versions of PKG1 & PKG2. user2 generates a
``cissy_workspace.yaml`` file like this:

.. sourcecode:: yaml
   :emphasize-lines: 5,14,19

   config:
     generators: ln_env
     ##ssh_config: cissygen/my_ssh_config
     process_manager:
       start_command: ln_manager -c pubsub_setup.lnc
       ##ssh_config_generator: ln_manager -c my_hosts.inc.lnc --export-hosts cissygen/my_ssh_config
       requires:
        - links_and_nodes_manager/[~1]@common/stable
       requires_from_all: ".*_ln_msgdef"

   processes:
     PKG1:
       requires:
       - PKG1/1.2.0@common/stable
       node: computehost
   
     PKG2:
       requires:
       - PKG2/2.1.0@common/stable
       node: visionhost


user2's ln_manager-config file ``pubsub_setup.lnc`` could look as simple as this:

.. code-block:: lnc
   :emphasize-lines: 6

   instance
   name: user2 setup
   manager: :%(get_port_from_string %(instance_name))

   include my_hosts.inc.lnc
   include_glob cissygen/**/*.inc.lnc

   process exe1
   use_template: PKG1_gen
   command: EXE1 --with-some-args
   
   process exe2
   use_template: PKG2_gen
   command: EXE2 --with-some-other-args

now user2 can start the ln_manager via cissy with a command line like this::
  
  cissy run
  
cissy will set some environment variables and then run the ln_manager command in a similar way as these bash-commands do:

.. code-block:: sh

   $ export            PATH=/volume/conan_cache/user2/.conan/data/links_and_nodes_manager.../bin
   $ export LD_LIBRARY_PATH=/volume/conan_cache/user2/.conan/data/liblinks_and_nodes.../lib
   $ export      PYTHONPATH=/volume/conan_cache/user2/.conan/data/links_and_nodes_python.../python
   $ export LN_MESSAGE_DEFINITION_DIRS=/volume/conan_cache/user2/.conan/data/PKG1_ln_msgdef.../own_msg_defs
   $ exec ln_manager -c pubsub_setup.lnc


scenario 3: MATLAB/Simulink subscriber (in Simulation, no RTW!)
===============================================================

user3 is working on `their <https://en.wikipedia.org/wiki/Singular_they>`__ simulink model ``u3_sim.mdl`` and now wants to subscribe to a topic that uses MD1.

user3 wants to use the links_and_nodes simulink library that is distributed within the ``links_and_nodes_simulink``
conan package.

.. note::
   todo: its unclear whether links_and_nodes_simulink should provide a dependency to ``links_and_nodes_base_python`` to get
   ln_generate. its not needed on target at runtime, also not needed at build-time for target. its also not needed to
   build the mex files. its only needed when user edits their simulink diagram end presses the OK button on the
   ln_publish_subscribe-sfunction.
   
user3 creates a ``cissy_workspace.yaml`` file like user2 from above, but adds a ``dev_matlab`` section for their
matlab-simulink-development environment:

.. sourcecode:: yaml

   ...
   processes:
     ...
     dev_matlab:
       requires:
         - links_and_nodes_simulink/[~1]@common/stable
         - PKG1_ln_msgdef/[~1.2]@common/stable
         - links_and_nodes_base_python/[~=1]@common/stable # for ln_generate while editing diagram
         - conan-matlab-helper/PR-5@tools/snapshot # for MatlabGenerator to create init_conan.m
       profiles:
         - osl42-x86_64
         - matlab2018b
       add generators:
         - MatlabGenerator # to get init_conan.m
       node: localhost

now user3 can start matlab like this::

  LN_MANAGER=lnm_host:port cissy run -p dev_matlab /opt/matlab/2018b/bin/matlab_acad

.. note::
   todo: NO, this will not work: DLRRM's matlab start scripts will need systems ``awk``, ``expr``... in PATH to work
   properly. those can not be added via cissy_workspace.yaml! 

or, maybe more convenient if they already have an ln-setup, they can add this development matlab process:

.. code-block:: lnc

   ...
   process dev_matlab
   use_template: dev_matlab_gen
   append environment PATH: :/usr/bin:/bin
   add flags: forward_x11
   change_directory: %(CURDIR)
   command: /opt/matlab/2018b/bin/matlab_acad
   node: localhost

this has the benefit that a user does not need to specify which ln_manager instance to use for communication (which
before was done via the LN_MANAGER env-var setting above) -- ln_manager will automatically provide this information.

and then just run::

  cissy run

to start the ln_manager and then continue to start the "dev_matlab" process.

matlab will extend its internal matlab-search-path from the MATLABPATH environment variable. user3 should
now execute ``ln_init`` to set a few workspace variables used as default values for the links_and_nodes sfunction-masks.

now user3 can open the links_and_nodes simulink library with this matlab command: ``open links_and_nodes``.
(which will open the simulink library file ``/volume/conan_cache/user3/.conan/data/links_and_nodes_simulink..../simulink/links_and_nodes.mdl``)

this simulink library includes an ``ln_publish_subscribe_sfun`` s-function which has a matlab gui that asks
for topic name & message definition (-> ``'PKG1_major/MD1'``). when pressing the OK button, matlab scripts are run which in turn call

.. code-block:: matlab

   system('ln_generate --sfunction-parameter-hash --idf PKG1_major/MD1')

to generate a matlab-vector describing MD1 that can be stored within the block's mask.

ln_generate will make use of the ``LN_MESSAGE_DEFINITION_DIRS`` environment variable to find ``PKG1_major/MD1`` on the
local filesystem.

user3 can now start the simulink model in *simulation mode* which causes matlab to load the mex-file (which is just a
shared library)
``/.../conan_cache/user3/.../links_and_nodes_simulink..../simulink/ln_publish_subscribe_sfun.mex{glx,a64,w64}``
found via the adjusted matlab path and which in turn needs ``libln.so`` which is found via ``LD_LIBRARY_PATH`` via the

  - cissy_workspace.yaml "dev_matlab" ->

    - links_and_nodes_simulink ->

      - liblinks_and_nodes

dependency-chain.


scenario 4: MATLAB/Simulink  (with RTW!)
========================================

developer4 has a running robotkernel setup and wants to use the *lnrk_interface* to get telemetry and sends commands to
his robotkernel devices. (this example deliberately does not use any ln_publish_subscribe-blocks for clarity)

**lnrk_generate** from the conan package ``module_lnrkinterface_simulink`` will be used to contact a running robotkernel
and query auto-generated message definitions for telemetry & command topics for LN-services.

that conan package also provides a simulink library named ``module_lnrkinterface.mdl`` with contains the s-function
``lnrk_interface_sfun`` and depends on ``liblinks_and_nodes``.

but ``lnrk_generate`` needs to use the links_and_nodes communication python package ``links_and_nodes_python``  which in
turn has a runtime dependency to ``libboost_python.so`` which is provided by the ``boost_python`` conan package. but that
package also might conflict with boost_python libraries that are already
used & provided by matlab itself.

.. note::
   todo: it is not yet known how to describe a good example for this use case. similar to the ln_generate use-case
   described above, lnrk_generate is only needed while editing the simulink-diagram (when user clicks the query
   button). so its not a good fit to have this requirement in any conan reference that is needed on the final
   target. its only needed for the matlab process that is used to edit the diagram. i would let the user explicitely
   specify that we needs/want to use this. extreme suggestion: have a ``module_lnrkinterface_simulink_lnrkgenerate``
   package that is only required from the matlab process when user is editing the diagram.

