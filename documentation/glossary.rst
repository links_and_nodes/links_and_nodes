########
Glossary
########

please start reading here to get an overview and a common
understanding of the terms used later in this document.

.. NOTE: the ideal order of the entries below is not alphabetical but
   a logically connected series of concept so that it is an
   interesting piece to read the items linearly from the top to the
   bottom.

.. glossary:: 

  Application Programming Interface
  API

       is a set of :term:`interfaces <interface>` which are provided
       to use or control a piece of software or a system.

  actor

       a mechatronic element which receives some signal, typically in
       the form of digital information, interprets that as a command,
       and initiates a process which has a physical effect.

  address sanitizer

       is a function module in a C++ compiler which detects
       illegal memory accesses that cause :term:`undefined behavior`.


  ASCII
  American Standard Code for Information Interchange

       is a standard published in 1963 for representing English
       characters in a 7-bit encoding, as well as a number of control
       symbols used for communication.  It is fundamental for
       representing programs and code, as well as many communication
       protocols. However, it is today impractical to represent text
       of most written languages, like for example names, and has
       therefore for more general purposes mostly been replaced by
       Unicode and its encodings such as UTF-8.
     


  atomic

       means the property of an operation that the user of this
       operation does not need to be concerned about whether the
       operation is interrupted, or enters otherwise partial states:
       From the user point of view, the operation either takes place
       entirely, or not at all. For example, if a program increments a
       word-sized number on a modern CPU, this operation will either
       succeed or not take place at all, it will not leave the memory
       location with a broken number. In the same way, if a file is
       renamed or deleted, this operation will only be observable when
       it is completed, so that programs do not need to deal with
       files which are in the middle of a renaming operation.


  augmented assignment

       is an assignment where the left-hand side of the assignment is
       the value of an expression which includes the old value of the
       variable and some change, as in the expression ``x += 1``.

  authentication

       is any method which ensures that a part of a system
       is only accessed by users and components which
       it is intended for.
  
  bisection

       a method to find places in software which causes a behavior
       change, such as a bug, which works by testing a subset of a
       space of possibilities, and narrowing down that subset more and
       more, very much like a binary search in an ordered array.


  blocking

       property of an I/O function to remain suspended until a
       data transfer is complete.

  boilerplate code

       is an expression for repeated, redundant code which is needed
       to make a specific function work, while being tedious to write
       and providing (because of its redundancy) almost no information
       of practical value.

	
  CLI
  Command-Line Interface

       As an alternative to its :term:`GUI`, the :term:`LN manager`
       also provides an interface via the command line, which makes it
       easier to automate certain set-ups.
  
  client object

       is a Python object which is created when a :term:`process`
       registers as a :term:`LN client` as a :term:`LN subscriber`,
       :term:`LN publisher`, :term:`LN service provider` or :term:`LN
       service client` in order to participate in the
       :term:`inter-process communication` services of LN.
     
  communication
  inter-process communication
  IPC

       A general term for a set of OS facilities and
       :term:`middleware` which allow different separate
       :term:`processes <process>` on the same computer to exchange
       data, control messages, and synchronize access to shared data

  Conan

       a packet manager and build tool for building and packing
       software applications and their dependencies and build
       requirements. A particular feature of conan is that it can
       build software for cross-platform applications.

  core dump

       a core dump is a file that an operation system like Linux
       can store when a program crashes. It contains the complete
       state of the crashed program, which makes it possible
       to identify the exact place where an error did become present.


  CPU-bound

       is a process which does not block or wait for I/O, but rather
       has always work to do for the processor. This can be caused,
       for example, by an faulty endless loop.  In :term:`real-time`
       systems, CPU-boundedness is a rather undesirable condition,
       because it causes lack of responsiveness.

  current working directory
  CWD

       is a default directory which is set by the operating system as
       parameter to a process, and is uses when the process opens any
       files with a relative path (that is, any path that does not
       start with a slash '/'). In Links and Nodes, it needs to be set
       explicitly to avoid unwanted environment dependencies;
       otherwise, the CWD is set to ``/tmp``.

       In the shell, the CWD of the current shell is printed by the
       ``pwd`` (= print work directory) command, and also the content
       of the ``PWD`` environment variable. The CWD is changed by the
       ``cd`` command.

  deadlock

       An error in which one part of a multi-threaded or multi-process
       system waits on another part and this part in turn waits on the
       first part, so that parts or the whole system stops.  This is
       often caused by improper use of locks, or use of
       :term:`blocking` function calls which depend on each other.
       Avoiding deadlocks requires careful design in the first place,
       and avoiding circular dependencies at run-time.


  determinism

       The property of a component of software to always deliver the
       same result when being activated with the same digital input,
       independently from influences such as, for example, the state
       of some piece of hardware. This is very desirable: Because the
       behavior is completely derived from the input, and the input
       can be exactly reproduced, it is much easier to debug the
       software and find the cause for errors. Such a system is called
       *deterministic*.

  dependency
  start-up dependency

       Dependency means in general that a piece of a system depends on
       another part before it can operate. The term is used in two
       common meanings: Either, a piece of software needs to be
       available and running before a second piece of software can
       start. Or, the first piece of software is needed to compile and
       build the second piece of software.
      
       Links and Nodes exclusively deals with the first kind of
       dependency. (In order to deal with the second kind, you need to
       use a package manager).o

  
  distributed system

       a computer system which consists out of :term:`nodes <node>`
       and computers which are connected via communication links such
       as TCP/IP networking.
     

  DRY
  don't repeat yourself
   
       principle that repeated code with the same content should in
       many cases be avoided and be replaced by common code with few
       specializations

  embedded system

       is the term for a computer or digital system which is embedded in
       another machine or device, as opposed to a terminal or desktop
       computer which has a visible keyboard and display. Such systems
       can range from tiny devices like a digital clinical thermometer
       or a watch up to complex CNC machines, a truck with automatic
       brakes, or a part of a rocket.


  endianness

       is the property of a computer architecture or CPU type to
       represent numbers which are larger than one byte either with
       the most-significant bytes first, or with the least-significant
       bytes first. This is relevant when in a networked systems,
       :term:`hosts <host>` with different CPU architectures work
       together. (see also: `endianness
       <https://en.wikipedia.org/wiki/Endianness>`__ for further
       information).

  
  environment
  environment variable

       environment variables are used to configure programs
       specifically for an individual user, and can modify the
       behavior of programs in a complex way. A typical example is a
       variable which points Python or Matlab to read start-up
       commands from a specific file.  They allow for a lot of
       flexibility but they are hard to manage when sharing code
       between different users, because each user will normally have a
       slightly different environment, and it is hard to track down
       differences in behavior to such settings.  Another problem is
       that environment variables are global variables, which means
       they are visible from anywhere in a system setup, and it is
       therefore hard to know which parts of a system might be
       affected by a change.
  
  
  exit status

       is a small number which is registered by the operating system
       when a process terminates. By convention, an exit status of
       zero always means that no error occurred. Non-zero status
       values mean that something went wrong - what it means depends
       entirely on the program in question and is usually documented
       in its manual page or other documentation.

  falsy

       The property of an expression or object to evaluate to a
       logical ``False`` when it is type-casted to a Boolean
       value. For example, empty lists and the number zero evaluates
       to False in Python.
    
  GUI
  Graphical User Interface
  LNM GUI

       An interface which allows to control some software via
       graphical means, like buttons, menus, lists, graphs, and
       control elements. In Links and Nodes, the :term:`Links and
       Nodes Manager <LNM>` can be controlled by a GUI.
       
  IEEE-754 floating point

       is a ubiquitous standard that defines formats and meaning of
       floating point numbers and operations. Among other things, it
       defines inter-operable layouts for 32-bit, 64-bit and 80-bit
       floating point numbers, as well as symbolic values such as
       Infinity, signed zero, and NaN (not-a-number values) which can
       signal undetermined results.

  interface

       is a term that we use narrowly for a strictly defined, narrow
       set of functions, constants, and behavior definitions which are
       used to access the functionality of a software :term:`module`
       with minimum coupling and API surface area. This pursued minimum
       coupling makes it easier to changes the internals and
       implementation of a module without affecting the clients of the
       module, making the system more robust against change.

       A second meaning of the term which is used frequently in the LN
       Manager configuration reference is that of a network interface,
       which is the software representation of a piece of networking
       hardware, such as an Ethernet card.
       
  interface definition
  message definition

       an LN message definition is a piece of text, stored in a text file,
       which defines a name for a type of :term:`message`, and which elements
       and data types it has. elements of messages can be basic types,
       arrays, and be composed from other types. they are conceptually
       similar to structs in C/C++, or class members in python, with
       the difference that they are designed to work across different
       programming languages, CPU architectures, and operating systems.

       A message definition for services is called here *service
       message definition* (or, in some places of the code, *interface
       definition*).

  HDF5

       is a scientific file format which is designed for storage of
       large time series, such as geophysical data. It supports
       especially well the storage of meta data. Libraries to access
       such files are available in many programming languages.

  host

       a host is a unique computer. it has to have a unique name. it can
       have multiple network-interfaces (each with its own IP-address).

       In the context of LN, a host is a computer which can run clients
       by starting an :term:`LN daemon` on it.
       
  host name

       Is a name for a networked :term:`host` or :term:`node`. A host
       can have several interfaces and network addresses, and also can
       have several names. Each name needs to be unique in a domain.

  instance name

       is a string that uniquely identifies an LN instance which is
       running on a given set of hosts. More than one instance
       can run on the same hosts at the same time, but their instance
       names need to be different.

  latency

       is the maximum time with which a :term:`real-time` process will
       respond to a request. Real-time processes usually have strict
       requirements in the maximum latency, which can be achieved by
       adjusting the :term:`scheduling policy`.

  LED Button
  LED

       means "light emitting diode", in the context of this
       description it means buttons which have a color signal like a
       diode which indicates the current state of a setting or a
       process.

  life time

       of an object in C++ is the time where the object and its member
       data can be correctly used. This is determined by the memory
       management: Traditionally, objects are either allocated
       on the stack and have a life time that ends with
       the block scope of the block they were defined in, or
       on the heap, and their life time ends when the memory
       is either explicitly freed by functions such as free or
       delete[], or by their destructor, when RAII is used
       for memory management. In the latter case, the
       life time of objects can also be limited to
       expressions, for example they might be valid when evaluated
       as an R-value, but any pointers to their members
       become invalid when the statement is finished.
       

  LN
  Links and Nodes

       is the term we use for the compound system of LN Manager, LN daemon,
       and LN client libraries

       
  LN API
  LN library

       a set of :term:`application programming interfaces<API>`
       which can be used by an LN
       client to connect with other clients and exchange data,
       and to get certain information from the LN manager.
       These programming interfaces are available for different
       programming languages, which are called language bindings.
       
  LN arbiter

       is a specific function of the :term:`LN daemon` which manages
       access to the daemon's services, especially when different
       users are using the same host.

  LN client
  ln_client

       is a program which uses the communication facilities of LN to
       send messages to other programs, or to request or provide
       :term:`LN services <LN service>`. In the most cases, an LN
       client is also a :term:`process` which is managed by LN, but
       this is not necessarily the case in general.
       
       For the communication, there are library bindings for different
       languages available: C, C++, Python and Java
       

  LN daemon
  ln_daemon

       to start- stop processes on remote :term:`hosts<host>`, the
       :term:`ln_manager` needs to
       connect to some kind of server running on those remote
       hosts. because ssh-servers are not common on all supported
       platforms and do not provide sufficient functionality a new
       server was needed.
  
       on every host there should be one ln_daemon running for your
       user-id. multiple daemons of different users do not interfere
       with each other.
       multiple instances can share the same ln_daemon.

       under normal circumstances, the necessary ln_daemons are
       started by the ln_manager if there is not already one running.

  LN instance
  Links and Nodes instance

       a Links and Nodes instance identifies a Links and Nodes
       configuration. each instance has to have a unique name and exactly
       one ln_manager with a unique TCP-address.

       Several instances can run at the same time on the same computers,
       but their instance names have to be different.

  LN manager
  ln_manager
  LNM

       the ln_manager is the central information hub within a
       :term:`Links and Nodes instance`. the manager knows who is providing what and on
       which host. the manager needs a human-readable config file which
       defines things like the manager TCP-address, what processes to
       start on which hosts and many other details of your system. the
       manager itself is a python program with an optional graphical
       user interface. you can use the user interface to manually
       start-/stop processes, inspect what is published by whom, at
       which rate and many other details.
  
  
  LN Manager configuration
  configuration file

       the ln_manager configuration file is a text file which specifies
       unambiguously which :term:`process` need to be started for a computing
       network, how they :term:`depend <dependency>` on each other, which
       :term:`priorities <process priorities>` they
       have, how they are :term:`grouped <process group>`,
       and what happens when processes terminate
       with an error.
       The configuration file provides a single definition, but it can
       include and re-use other files in order to compose the definition.
       
  LN publisher
  publisher

       a process which periodically sends a certain type of data to
       other processes (called :term:`subscribers <subscriber>`) which
       might be interested in the data and process it further.

  LN service
  service

       the general meaning of the term service is a piece of data processing
       functionality which might be provided over a network.
       
       in the context of Links and Nodes, a service is some unit of
       computation which a client can send data to and will get the
       result returned, similar as in a function call, but the call can be
       performed across a network. This is similar to an RPC service.
       The call can also change the behavior or state of another
       part of the system, for example it could re-assign the
       target which a robot is currently moving to.
       
  LN service client

       is a process which has registered to be client for a :term:`LN
       service`.  (note that processes can be both clients and
       providers or publishers).


  LN service provider

       is a process that has registered to provide an :term:`LN service`,
       a kind of :term:`remote procedure call`.

  LN state

       in LN, a binary state living somewhere in your system.
       This can be, for example, the fact that a specific network
       interface is configured and running or not.
       you can configure shell-commands to set- or unset a state. processes can
       depend on a state being set (= up) or unset (= down). an example
       might be whether or not an additional network interface is
       configured (= has an IP address) or not.
       
  LN subscriber
  subscriber

       a subscriber is a :term:`process` which receives
       :term:`messages <message passing>` and data from other
       processes, called publishers.  For each kind of data, called
       :term:`topic`, there can only be one publisher, but many
       subscribers, which means that a publisher can broadcast data. A
       publisher process can also be a subscriber, and vice versa.
     


  LN topic
  topic

       in the context of Links and Nodes, a topic is a named
       :term:`message` channel which transports messages of a specific
       type.
       
       It can be published by one publisher, and may be read by one or
       more subscribers.  The name of the topic defines the identity
       and meaning of the data stream, while the used :term:`message
       definition` determines its data type.

  LN parameter
  parameter

       a parameter in the context of LN is a value which can be set by
       one part of the system, typically the :term:`LN manager`
       process, and is then distributed to :term:`LN client <LN
       client>`. The intended use is to provide configuration values
       and adjustments during operation which can be done at run time
       in an uniform way.

  
  memory protection

       is a feature of modern operating systems that each process has
       its own address space and normally cannot access the memory of
       another process. This has the advantage that bugs in one
       program cannot affect other processes and cause them (or the
       whole system) to crash.


  message

       a message is a piece of data that transmits information from
       one :term:`process` to another one.
       
       In the context of distributed systems means, "message" means a
       piece of data which is transmitted between different
       :term:`hosts <host>`.  Usually, the transmission is made
       visible in an all-or-nothing manner: Either, the complete
       message has arrived, or it is not yet there. This simplifies
       error handling and flow control. As a contrasting example, data
       transmitted via sockets is continuous and can arrive piece by
       piece as a "byte stream", requiring the software to re-assemble the pieces.
  
       In the context of LN, messages have a fixed data type and is
       associated with a named :term:`topic`, which determines its
       meaning.

       See also :term:`message passing`.

  message passing

       message passing is a general paradigm for data exchange in
       distributed systems and :term:`real-time` systems: Data is
       exchanged by routines and infrastructure which allow senders to
       write a block of data, and sent it in one piece, and receivers
       to receive it in one piece. Reading and writing routines can be
       :term:`blocking` (waiting until a data buffer is complete) or
       :term:`non-blocking` (if data is not yet complete, the read
       fails and must be re-tried later).

  middleware

       Software which provides communication and program
       management services in a larger system. For more information, see
       `middleware <https://en.wikipedia.org/wiki/Middleware>`_.

  module

       A module means, in the context of this documentation, one of
       two things: Either, a piece of software which is part of a
       bigger system, but has clear boundaries so that it can be
       developed, tested and run independently from the bigger system,
       and potentially re-used in another system. A basic unit of
       composition is a process, and a module can of course consist
       out of smaller modules, such as libraries.

       In a more narrow sense, the term is also used for a
       Python module, which is usually a program component
       or a library in the sense of the first meaning,
       but written in Python.
       
  multi-threaded

       a program or process in which several :term:`threads` of
       computation are executed simultaneously - either actually at
       the same time by running on different CPUs of a computer that
       has several CPUs/cores, or by quickly switching between
       different threads after the current thread has run a certain
       time or is blocked by waiting on something.


  mutex
       is a kind of object that acts as a lock which ensures that
       some data object can be accessed and modified only by
       a single thread at the same time, to prevent memory
       corruption and :term:`race conditions <race condition>`.

    
  name space

       Is a general term in software where a identifier defines a
       room in which names can have an unique meaning, even
       when they have been used before in another name
       space with a different name. They are used pervasively,
       for example in python, where each python module
       forms an own name space, as well as in C++, where
       the ``namespace`` keyword is used.

       In Links and Nodes, names of topics and services can use name
       spaces, which are part of the name that are separated by a dot
       ("."). They are often given in correspondence to the path names
       of message definitions, however this correspondence is a
       convention.

  nanosecond

       the billionth part of a second, or 1e-9 seconds.

  
  network

       in LN, a network has a unique name. a host is reachable from within a
       network if it has at least one network-interface within that network.


  NTP
  Network Time Protocol

       the Network Time Protocol (NTP) is a service which ensures that
       the system clocks of different hosts stay closely synchronized.
       To use it, a special NTP client like `ntpd` or `chrony`
       needs to be started by the system, which connect to so-called
       time servers that distribute highly precise time measurements.
       Under the right conditions, this is very precise with an accuracy
       well under the millisecond range. With limited network connectivity,
       like for example on a Mars rover, it might be necessary to
       provide an own time server on the robot.

       Using time synchronization can be very helpful for error
       analysis and is especially important when capturing larger
       amounts of data for later analysis and processing, e.g. when
       you want to analyze and compare the telemetry data from a robot
       with a video from a specific failure. It is therefore highly
       recommended to use it.

  network transparency

       Network transparency is a term for the capability of a protocol
       or library software to transmit data over the network in a
       manner which is invisible (or transparent, as glass) to clients
       that use the protocol or library. 
       
       Network-transparent software shows identical interfaces and
       nearly identical behavior regardless whether pieces of the
       software are run on the same computer or on a different
       networked computer. Remote data or resources can be accessed in
       the same manner as local data. For example, the X11 windowing
       system is network-transparent: the GUI works regardless where
       the client program runs.

  node  

       a node is a computer (or :term:`host`) which is part of a network.
       Especially, it has the capability to relay commands and data to other
       hosts which are not directly reachable.
         
  non-blocking

       property of an I/O transfer function to return with a "not
       successful" result when data is being transmitted, but is not
       yet complete.
  
  non-deterministic

       property of a system or system part to be able to produce
       output which is different in an unpredictable way, even with
       the same input data. Causes for this can for example be I/O,
       physical unstable behavior of hardware, or race conditions.
       Non-deterministic software components are often extremely
       difficult to debug. An example for such software is a program
       running in several :term:`threads` which has some :term:`race
       condition`.

  persistence

       Persistence the property of an entity to be available for
       longer than the run-time of a process, for example the content
       of a file, or its name. In the LN system, for example message
       definitions and their names are persistent, while the names of
       topics and services are not persistent.

  
  POD
  Plain Old Data

       POD or "plain old data", sometimes also called "passive data
       structures", are what is called structs in the C programming
       language: Objects which have a type and contain several members
       in a precisely defined layout, which can be copied across
       memory, represented as a number of bytes, stored in files and
       sent across networks without losing anything.  They do not have
       any virtual methods and therefore they do not need to contain
       stored information about their place in a class inheritance
       tree.  Therefore, they can easily be passed and copied when
       calling into code written in other programming languages.

       PODs are, which might be surprising, really a subset of C++
       classes: Structs are classes without user-defined constructors
       and destructors, where all data members are public and all
       member functions and base classes are non-virtual.

  POSIX

       is a standard for Unix-like systems such as Linux which defines
       interfaces to the operating systems. Using this standard API
       makes it much easier to write portable programs which work
       across different OS implementations.

     
  port number

       Is an integer number which specifies an TCP or UDP port. To
       reach any kind of server, its host name or network address and
       port number needs to be known; specific servers often have
       default port numbers.

  port object

       is an object which connects a :term:`client object` that
       requests a specific service with a :term:`message definition`
       that defines the type of data that is transmitted. A port
       object has one of either a read(), a write(), or a call()
       method that transfers the data. It is similar to a Unix socket
       with the important difference that data is not a byte stream,
       but a typed message buffer, which is transmitted
       :term:`atomically <atomic>`.  For an overview, see section
       :ref:`tutorial/Python_API/overview`.

  
  precondition

       A precondition is a condition that data or the state of a
       program must meet in order to have a valid algorithm. For
       example, if one divides by an integer number, one must make
       sure beforehand that that number is not zero. Preconditions are
       most frequently used for function definitions, where we put
       conditions on with which input values a function can be
       called. They are complemented by promises that we make about
       data and program state at the end of a function call (or
       another piece of code), which are called post-conditions. Both
       preconditions and post-conditions are called invariants of a
       program. Invariants are very useful to analyze and debug
       correctness of a program.


  process

       A process is a concept in operating systems. It is a program
       which has its own memory and cannot disturbed by actions of
       another process.  This compartmentalization makes it easier to
       debug complex programs which consist of several processes. In
       contrast to the separation of processes are :term:`threads`,
       which share data, and are significantly more difficult to
       debug. One process can have several threads.

       Processes run on a specific :term:`host`. 

       Single processes are isolated from each other, which makes it
       easier to debug them.  Processes can also depend on other
       processes - meaning other processes need to be running before
       this process can be started.

       In the context of LN, when we talk about process we usually
       mean processes which are managed by the :term:`LN manager`,
       which means that they are always started and stopped by the
       manager. Which process to start on which host and with which
       arguments can be specified in the ln_manager-config file.
       
  
  process management

       The task of starting and stopping individual :term:`processes
       <process>` as parts of a whole distributed system

  process name

       is a name which the LN manager uses to identify and display
       processes. They are defined using the "process" keyword of the
       :term:`LN configuration file <configuration file>`, which
       starts a process section.
  
  process group

       The term process group has a general meaning and a specific
       meaning in Links and Nodes. In general, a process group means
       in systems programming that a number of :term:`processes
       <process>` was created by the same parents and has some common
       means of controlling them. For example, all processes in a
       process group might receive signals which are sent to that
       group.
      
       In Links and Nodes, the meaning is more specific: Processes in
       a process group are grouped together in the Links and Nodes
       configuration, and can be started and stopped by a single
       command. They do not need to run on the same host.
  
  process priorities
  scheduling priorities

       in situations where there are limited CPU resources on a
       system, priorities are used to determine which processed are
       advanced next. If processes use :term:`real-time` scheduling,
       :term:`processes <process>`, :term:`threads` or tasks with
       higher priorities are always completed before processes with
       lower priorities are run.

       This is useful to guarantee that important time-critical
       functions are completed before less important functions consume
       CPU resources.

       Note: Process priorities for the default scheduling algorithm
       in Linux are, by tradition, numerically expressed as
       nice value, which is a small signed integer number.  A
       high nice value always indicated a low priority, and vice
       versa. This is however completely different from scheduling
       priorities for real-time processes, where processes with the
       highest priority value will always run before a process with a
       lower value can run.

       *see also*: :term:`scheduling policy`
       
  process synchronization

       is the task to make sure that before one process starts,
       another process has reached a state where it can support the
       first process, for example for accepting commands or service
       requests over the network

  pthreads

       a threading library for low-level programming of threads that
       share the same memory.



  publish/subscribe
  publish/subscribe communication

       an approach to exchange data in a system via :term:`publisher`
       and :term:`subscriber`.
       Publish-subscribe communication is a general communication
       pattern where some sender makes periodically information
       available, and one or more receivers can request
       to get this information, so they they can periodically
       read the transmitted data.
       
       in LN, the communication is done via :term:`message passing` and
       these data sequences are organized as :term:`topics <LN topic>`,
       and data can be transmitted very fast and efficient,
       so that it is suitable for real-time processing
       with thousands of messages per second.

  public-key cryptography

       a method to either authenticate (or, alternatively, encrypt)
       data with a key that is allowed to be transmitted
       as cleartext, i.e. without further encryption. This works
       by generating *two* keys, a public one, and a private one,
       which are matched by a mathematical property (basically, they
       are large numbers which share a prime factor). This allows
       to send the public key over an insecure network, and
       perform later authentication by using the private, or secret,
       key. This scheme is used in SSH connections and also
       in the method for authenticating the LN manager to
       an LN daemon on another host.

  race condition

       a kind of error in which two threads or processes access the
       same resource, for example a piece of memory, without proper
       synchronization. This leads usually to
       :term:`non-deterministic` behavior and is difficult to
       debug. Race conditions also can lead to some kind of
       :term:`undefined behavior` which is even worse to debug,
       because the program as a whole might cease to do what the code
       says.

  RAII
  resource acquisition is initialization

       is a pattern in object-oriented programming when a resource
       that is created and acquired is also initialized by its
       constructor, and the constructor only returns completely and
       working instances. If the construction fails, the constructor
       would throw an exception. And, an important aspect of the
       concept, whenever the destructor for the object is called, its
       resources are also automatically de-initialized, so no
       automatic resource management is necessary. For resources such
       as memory, this means that the user of the object has not to
       worry about memory management, or about getting a
       half-initialized but not usable instance.
     

  rate limit
  
       is a limit of the maximum number of messages received per second.
       Such a limit can be set by LN topic subscribers.

  real-time

       real-time means in embedded systems that an operation must be
       completed within a maximum allowed time. Typically (but not
       necessarily), this is a very short time span. Generally, one
       distinguishes between "soft" real-time (a process which exceeds
       time limits is defective, but does not cause serious failure)
       and "hard" real-time requirements (a failure to meet timing
       requirements means a catastrophic failure, for example if an
       airbag does not reacts in time during an accident, or a rocket
       gets out of control).
  
  reliable transport

       a reliable transport transmits data so that the sending and
       receiving components usually do not need to check for an
       error-free transmission. This is achieved by check sums, test
       for completeness, and automatic sending of data. A reliable
       transport can be significantly slower than an unreliable one.
  
  regression

       is the name for a type of program bug where something that worked
       before has stopped to work because of the introduction of some new
       code or feature.

     
  regular expression
  regexp

       regular expressions are textual expressions that describe in a
       specific syntax which classes of character sequences match them
       (or do not match).  They form a mostly standardized syntax used to
       specify strings with specific patterns, for example for the
       purpose of searching or identifying specific character
       sequences in a string.

       A common example is the argument for the :program:`grep`
       command, that searches for specific lines of text in a file,
       and prints them.

       .. seealso:: `Regular Expression <https://en.wikipedia.org/wiki/Regular_expression>`_.

  reproducibility

       reproducibility is the property of a piece of software, a
       component or a system to generate identical results between
       different runs.  A reproducible systems leads always to the
       same result when feeding it with the same input. Ideally, it
       also allows to easily revise and oversee said inputs so that
       one can trace back which changes in configuration cause
       specific changes in behavior.
       
       For complex system setup like build systems, this is highly
       desirable, and can be very hard to achieve. Sources of the
       difficulty are non-deterministic behavior of hardware, but also
       subtle differences in software setups, for example between an
       individual programmer's user account, and the setup of an
       account used in a system demonstration. A system that lacks
       reproducibility can become unmanageable very quickly.
  
  RPC service
  remote procedure call

       RPC, or "remote procedure call" is a standard way to expose
       functionality across a computer network and allow client
       computers to call computations or request other functionality
       from one networked computer. In difference to :term:LN services <LN service>`,
       they are not suitable for real-time systems, but otherwise,
       they are conceptually very similar.
       

  scheduling policy

       is the algorithm with which the scheduler of the operating
       system, a component that allocates CPU time for each process,
       decides which processes should be run next, or kept
       running. There are general-purpose scheduling policies, and
       special ones which are suitable for real-time processes.


  sensor

       a mechatronic element which receives some data about the world
       outside of a system, or its internal state, and transforms it
       to some signal, typically in the form of some digital
       information.
       
  sentinel value

       A sentinel value is a special value of some sequence of data
       that indicates the end of the sequence. The most prominent
       example is the use of the null character (NUL or '\\0') in C to
       indicate the end of a string. Such zero-delimited strings are
       **not** used in LN message buffers.

  service group

       is a named group of LN service handlers that are assigned to run in
       the same thread. This can be the main program thread (which is
       the common case), or an extra thread which runs service calls
       in parallel.  If service handlers run in several different
       threads and they access and modify the same data concurrently,
       they need to use :term:`mutexes <mutex>` and similar means in
       order to avoid :term:`race conditions <race condition>` and
       similar concurrency bugs and the resulting :term:`undefined
       behavior`.

  service handler

       is code, usually a function or method, that gets called
       when a :term:`LN service provider` process receives
       a :term:`service request` from a :term:`LN service client`.

  service message definition

       a special :term:`message definition` for defining the data
       structures for service calls. This kind of message definitions
       defines both the invocation of a method, and its response, and
       has two sections, called ``request`` and ``response``. Both are
       like the function call parameters for the request, and a
       function return type for the response.

  service request
       a call to an :term:<LN service>.
  
  shared memory

       is a mechanism provided by the operating system for exchanging
       data (for example, :term:`messages <message>`) between
       processes which are running on the same computer. It is
       implementing by allowing different processes to access the same
       memory, and is extremely fast in comparison to communication
       via network.
  
  ssh
  secure shell

       is a program which allows to connect to a remote computer
       running an authenticated command shell, via an encrypted,
       authenticated, and authorized connection. ``ssh`` is used by
       default by the LN manager to initially connect to the nodes of
       a distributed system, and to start :term:`LN daemons <LN
       daemon>` and clients.

  struct

       an elementary compound data type in C and C++, consisting
       of a list of member elements and their type.

  synchronization

       is steps that are taken to ensure that two processes are in a
       well-defined temporal relationship in terms of the effects of
       their execution. For example, if a process provides a TCP
       service, it usually needs to be started *before* a client
       process which uses that TCP service. It can also be required
       that two processes are past a certain point of their respective
       execution.


  sys.argv

       Is a name used in python for the argument list of a program,
       which is a member of the built-in ``sys`` module.


  terminal
  
       A terminal is a piece of software that displays text which was
       printed by some program, and allows to format and inspect it,
       as well as to enter text commands (if the program accepts text
       input). Terminals are often stand-alone programs in a desktop
       system. Links and Nodes provides terminal facilities for
       processes that are started by the LN Manager, so that the
       output of the processes can be tracked.


  threads

       When a program runs in several threads, this means that several
       parts of the program are being run virtually at the same time,
       sharing their data. This also allows to use several CPUs at
       once. However, any data structures which are changed and
       accessed by more than one thread need to be synchronized and
       protected against uncoordinated modification, otherwise the
       program will not run correctly.  Programming with multiple
       threads is significantly more difficult than developing a
       single-threaded program, because such a program can quickly
       start to behave in a :term:`non-deterministic` way which is not
       easy to analyze and debug.

  thread safety
  thread-safe

       is the property of functions and code regions to run safely
       even if the same code is executed in parallel in several
       threads, potentially modifying the same variables at the same
       time. It usually requires significant care and advanced
       programming skills to make such code correct and avoid problems
       such as :term:`race conditions <race condition>`.
  
  time-out

       is, for example in the context of LN message-passing, or
       process management, a specified maximum waiting time which an
       operation will wait for a desired result.  After that time has
       been exceeded, the operation or function call would return with
       an error status.
   

  truthy
  
       The property of an expression or object to evaluate to a
       logical ``True``, if it is type-casted to a Boolean value. For
       example, non-empty lists and all numbers different from zero
       evaluate to True in Python.

  undefined behavior

       Is a type of program error where a bug in the program destroys
       required invariants of the program. This leads very possibly to
       corruption of the run-time environment.  Especially for
       programs in C/C++ which are translated by modern optimizing
       compilers, this means that no consistent behavior at all can be
       warranted for the whole program, because the violation of
       invariants can easily have non-local consequences.
      
  
  unit test

       is a test that checks specifically whether a software component
       performs a single, well defined function correctly. Usually, they
       are implemented in combination with API definitions, which state
       a precise, fixed behavior for such components.

       
  UNIX signal

       is a kind of numbered message which the OS kernel can deliver to
       a process. Depending on the type of the signal, the process can
       define a signal handler which runs a small function that can
       define or start a response to this signal, like re-loading a
       configuration file.  Certain signals request an orderly
       termination (like SIGTERM), others cause an immediate
       termination of the program (such as SIGKILL). SIGHUP means
       closing of a terminal connection, and SIGSEGV means a violation
       of the memory address space.

  user environment

       *see* :term:`environment variable`

  version control

       names a class of tools and practices which allow it to save and
       inspect each change to the textual source code of a software
       project, and to manage changes and versions. The tool of choice
       is nowadays git, because based in its design, it can
       automatically merge changes from different branches of
       development.

  YAML

       means "yet another markup language" and is a standard file format
       used for configurations.



