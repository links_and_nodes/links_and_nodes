.. shown with python examples
.. topics & services

##########
Quickstart
##########

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   quickstart_python_process_management.rst
   
   quickstart_python_topic_communication.rst
   quickstart_python_services.rst

   quickstart_cpp_topic_communication.rst
   quickstart_cpp_services.rst
   

.. index::
   single: quickstart sections; overview
   
For some people, the easiest and quickest way to understand something
new is to get started with some short and sweet sample code they can
read and modify. If you are like this, this chapter is for you!

Each of the next five sections show some essential code
examples for the most important features of links
and nodes:

1. an example how to start the :term:`LN manager` and manage
   interdependent processes with it.
2. an example how to let processes communicate via :term:`publish/subscribe`
   and :term:`topics <LN topic>`.
3. an example how to integrate :term:`LN services <ln service>`, which are similar
   to :term:`remote procedure call <remote procedure call>` into a program.

They assume that you have read the :doc:`introduction`, specifically
the sections named ":ref:`overview/functions`" and
":ref:`overview/main_components`", and have a ready installed system,
as described in the chapter ":doc:`getting_started`". All three are
written in Python; we assume that you know how to create, edit and run
python scripts. If you need an explanation for some specific term, you
can look it up in the :doc:`glossary`.

As mentioned, the examples show the necessary code, but do not try to
explain all of the underlying magic. For some more explanation of the
concepts which are used here, you can then go to the next chapter, the
:doc:`tutorial`.



   
