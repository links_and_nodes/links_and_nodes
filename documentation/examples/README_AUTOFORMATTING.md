# Auto-Formatting Samples (for developers)

This sample use the following atomatic formatters:

* for C++, "astyle --style=linux --indent=tab=8

* for Python, "black" (can be installed via "pip3 install black")

For using this with Emacs, install the "format-all" package
from MELPA, and add this to .emacs:

..............
(setq format-all-default-formatters '(("C++" astyle)
				      ("Python" black)))

(add-hook 'c++-mode-hook 'format-all-mode)
(add-hook 'python-mode-hook 'format-all-mode)

..............



This will format Python and C++ files automatically when they are saved.
Both formatters can be switched off for specific code
regions by using special comments.

