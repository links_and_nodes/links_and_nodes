#include <ln/ln.h>

#include <cstdint>
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>


#include "ln_messages.h"
#include "error_codes.h"


//////////////////////////////////////////////////////////////////


using std::cout;

static const uint64_t min_argument = 1;
static const int64_t max_argument = 1000000;

// compute list of primes up to a specified number, using
// Euclid's sieve algorithm
// The resulting primes are sorted beginning from
// the smallest to the largest
// If the number is smaller than min_argument,
// or larger than max_argument, an exception
// should be thrown.

std::vector<uint64_t> compute_primes(int64_t n)
{
	// error: n is promoted to unsigned here, so the check
	// for it being smaller 0 fails
	if(n < min_argument)
		throw FactorizerArgumentTooSmallError("you gave: "s + std::to_string(n));
	if(n > max_argument)
		throw FactorizerArgumentTooLargeError("you gave: "s + std::to_string(n));

	std::vector<bool> is_marked_prime(n+1, true);
	std::vector<uint64_t> result;

	result.push_back(1);
	cout << "[0] prime found: " << 1 << '\n';
	int p=2;
	// style: max_factor is double, which works here,
	// but is less robust than comparing to p * p with n
	auto max_factor = sqrt(n);

	// error: it must be p <= max_factor
	while(p < max_factor) {
		if (is_marked_prime[p]) {
			result.push_back(p);
			cout << "[1] prime found: " << p << '\n';
			for(int k=p; p * k <= n; k++) {
				is_marked_prime[k*p] = false;
			}
		}
		p++;
	}
	// error: it must be p <= n
	for(p = ((int) max_factor) + 1; p < n; p++) {
		if (is_marked_prime[p]) {
			result.push_back(p);
			cout << "[2] prime found: " << p << '\n';
		}
	}
	return result;
}

// reverses a list of primes, so that the largest prime is at the start
void reverse_list(std::vector<uint64_t> &primes)
{
	auto len = primes.size();
	// error: left_mid is unsigned, therefore the loop
	// comparison will always fail
	// also, a standard algorithm should be used
	auto left_mid = len / 2 - 1;

	for(auto left=left_mid; left >= 0; left--) {
		auto right = len - left - 1;
		// swap right and left element in-place, down to index 0
		auto tmp=primes[left];
		cout << "swap primes["<<left<<"] = " << tmp
		     << " and primes["<<right<<"] = " << primes[right] << "\n";
		primes[left] = primes[right];
		primes[right] = tmp;
	}
}

// takes a number and computes its prime factors, returning them in a
// vector of uint64_t
std::vector<uint64_t> compute_prime_factors(int64_t n, bool reverse_order=false)
{

	std::vector<uint64_t> primes = compute_primes(n);
	std::vector<uint64_t> prime_factors;

	if (reverse_order) {
		std::cout << "reversing... \n";
		reverse_list(primes);
	}

	uint32_t test = n;
	uint32_t n_prime =0;
	while(n_prime < primes.size()) {
		uint32_t p = primes[n_prime];
		if ((test % p) == 0) {
			prime_factors.push_back(p);
			if (p == 1) { // omit 1
				n_prime++;
				continue; // skip 1
			}

			// p factorizes test, add to list
			test = test / p;
			if (test == 1) {
				break;
			}
		} else {
			// prime is no factor of test,
			// continue to probe with next prime
			n_prime++;

		}
	}

	return prime_factors;
}


// remove multiple identical entries in a sorted vector of ints
std::vector<uint64_t> remove_multiple(std::vector<uint64_t> v)
{
	auto last = std::unique(v.begin(), v.end());
	// error: it must be followed by
	// 		v.erase(last, v.end());
	// in order to remove the remaining elements
	return v;
}

// for a list of numbers of type std::vector<uint64_t>, compute a
// list of prime factors for each entry, and return it.
std::vector<std::vector<uint64_t>> compute_all_factors(std::vector<uint64_t> &nums)
{
	std::vector<std::vector<uint64_t>> result;
	for (auto n : nums) {
		result.push_back(remove_multiple(compute_prime_factors(n)));
	}
	return result;
}


// for a list of numbers, return the prime factors that are
// common for each element of the list.
std::vector<uint64_t> get_common_factors(std::vector<std::vector<uint64_t>> &all_factors)
{
	std::vector<uint64_t> common_factors;
	for (auto i=0; i < all_factors.size(); i++) {
		if (i == 0) {
			common_factors = all_factors[0];
		} else {
			std::vector<uint64_t> new_factors = all_factors[i];
			std::vector<uint64_t> intersection;

			std::set_intersection(common_factors.begin(), common_factors.end(),
			                      new_factors.begin(), new_factors.end(),
			                      std::back_inserter(intersection));
			common_factors = intersection;
		}

	}
	return common_factors;
}


// Implementation of number factorizing services.
//
// Implements three service calls:
//
//  (a) factorizing a number into prime factors
//  (b) computing the largest prime factor
//  (c) computing prime factors that are common to all numbers of a list

class FactorizerServer :
	public factorizer::request::get_prime_factors_base,
	public factorizer::request::get_largest_prime_factor_base,
	public factorizer::request::get_common_factors_base
{


	ln::client clnt;

public:
	FactorizerServer() :
		clnt("factorizer service")
	{

		register_get_prime_factors(&clnt, "factorizer.compute.factors", "default group");
		register_get_largest_prime_factor(&clnt, "factorizer.compute.largest", "default group");
		register_get_common_factors(&clnt, "factorizer.compute.common", "default group");


		cout << "service thread started!\n";
	}


	int on_get_prime_factors(ln::service_request& req,
	                         factorizer::request::get_prime_factors_t& data) override
	{


		const auto n = data.req.n;
		const auto reverse_order = (data.req.reverse_order != 0);

		try {

			std::vector<uint64_t> prime_factors = compute_prime_factors(n, reverse_order);

			data.resp.prime_factors = prime_factors.data();
			data.resp.prime_factors_len = prime_factors.size();
			data.resp.error_code = 0;
			data.resp.error_message = (char*)"";
			data.resp.error_message_len = 0;
			req.respond();

		} catch (const FactorizerException &e) {
			data.resp.error_code = static_cast<uint32_t>(e.error_code);
			data.resp.error_message = (char*)e.what();
			data.resp.error_message_len = strlen(data.resp.error_message);
			req.respond();
		}


		return 0;

	}


	int on_get_largest_prime_factor(ln::service_request& req,
	                                factorizer::request::get_largest_prime_factor_t& data) override
	{


		const int32_t n = data.req.n;
		const bool reverse_order = true;

		std::cout << "getting largest factor\n";
		std::vector<uint64_t> prime_factors(compute_prime_factors(n, reverse_order));
		std::cout << "getting largest factor[1]\n";
		std::cout << "size: " << prime_factors.size() << std::endl;

		data.resp.largest_prime_factor = prime_factors[0];
		std::cout << "getting largest factor[2]\n";
		data.resp.error_code = 0;
		data.resp.error_message = (char*)"";
		data.resp.error_message_len = 0;
		req.respond();

		return 0;

	}


	int on_get_common_factors(ln::service_request& req,
	                          factorizer::request::get_common_factors_t& data) override
	{


		std::vector<uint64_t> nums;
		for (auto i=0; i < data.req.nums_len; i++) {
			nums.push_back(data.req.nums[i]);
		}

		// error: the life time of v is wrong
		if (data.req.debug) {
			// print the data for some debugging...
			for(auto i=0; i < nums.size(); i++) {
				cout << "factor series[" << i << "]: ";
				for(auto v: compute_all_factors(nums)[i]) {
					cout << v << ", ";
				}
				cout << "\n";
			}
		}

		std::vector<std::vector<uint64_t>> all_factors = compute_all_factors(nums);

		std::vector<uint64_t> common_factors = get_common_factors(all_factors);


		data.resp.common_factors = common_factors.data();
		data.resp.common_factors_len = common_factors.size();
		data.resp.error_code = 0;
		data.resp.error_message = (char*)"";
		data.resp.error_message_len = 0;
		req.respond();

		return 0;

	}

	int run()
	{
		cout << "ready to receive service calls\n";
		while (1) {
			/// processing...
			double time_out = -1; // means blocking
			clnt.wait_and_handle_service_group_requests("default group", time_out);
		}
	}


};


int main(int argc, char* argv[])
{
	FactorizerServer factorizer_server;
	return factorizer_server.run();
}
