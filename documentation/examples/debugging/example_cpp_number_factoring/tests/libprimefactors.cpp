#include <math.h>
#include <iostream>
#include <sstream>
#include <algorithm>


#include "error_codes.h"

#include "libprimefactors.h"

//////////////////////////////////////////////////////////////////
using std::cout;


std::vector<uint64_t> compute_primes(int64_t n)
{
	if(n < min_argument)
		throw FactorizerArgumentTooSmallError("you gave: "s + std::to_string(n));
	if(n > max_argument)
		throw FactorizerArgumentTooLargeError("you gave: "s + std::to_string(n));

	std::vector<bool> is_marked_prime(n+1, true);
	std::vector<uint64_t> result;

	result.push_back(1);
	cout << "[0] prime found: " << 1 << '\n';
	int p=2;
	auto max_factor = sqrt(n);

	while(p < max_factor) {
		if (is_marked_prime[p]) {
			result.push_back(p);
			cout << "[1] prime found: " << p << '\n';
			for(int k=p; p * k <= n; k++) {
				is_marked_prime[k*p] = false;
			}
		}
		p++;
	}
	for(p = ((int) max_factor) + 1; p <= n; p++) {
		if (is_marked_prime[p]) {
			result.push_back(p);
			cout << "[2] prime found: " << p << '\n';
		}
	}
	return result;
}

// reverses a list of primes, so that the largest prime is at the start
void reverse_list(std::vector<uint64_t> &primes)
{
	auto len = (int) primes.size();
	auto left_mid = len / 2 - 1;

	for(auto left=left_mid; left >= 0; left--) {
		auto right = len - left - 1;
		// swap right and left element in-place, down to index 0
		auto tmp=primes[left];
		cout << "swap primes["<<left<<"] = " << tmp
		     << " and primes["<<right<<"] = " << primes[right] << "\n";
		primes[left] = primes[right];
		primes[right] = tmp;
	}
}

uint64_t get_largest_prime_factor(int64_t n)
{

	const bool reverse_order = true;

	std::vector<uint64_t> prime_factors(compute_prime_factors(n, reverse_order));
	return prime_factors[0];
}


// takes a number and computes its prime factors, returning them in a
// vector of uint64_t
std::vector<uint64_t> compute_prime_factors(int64_t n, bool reverse_order)
{

	std::vector<uint64_t> primes = compute_primes(n);
	std::vector<uint64_t> prime_factors;

	if (reverse_order) {
		std::cout << "reversing... \n";
		reverse_list(primes);
	}

	uint32_t test = n;
	uint32_t n_prime =0;
	while(n_prime < primes.size()) {
		uint32_t p = primes[n_prime];
		if ((test % p) == 0) {
			prime_factors.push_back(p);
			if (p == 1) { // omit 1
				n_prime++;
				continue; // skip 1
			}

			// p factorizes test, add to list
			test = test / p;
			if (test == 1) {
				break;
			}
		} else {
			// prime is no factor of test,
			// continue to probe with next prime
			n_prime++;

		}
	}

	return prime_factors;
}


// remove multiple identical entries in a sorted vector of ints
std::vector<uint64_t> remove_multiple(std::vector<uint64_t> v)
{
	auto last = std::unique(v.begin(), v.end());
	v.erase(last, v.end());
	return v;
}

// for a list of numbers of type std::vector<uint64_t>, compute a
// list of prime factors for each entry, and return it.
std::vector<std::vector<uint64_t>> compute_all_factors(std::vector<uint64_t> &nums)
{
	std::vector<std::vector<uint64_t>> result;
	for (auto n : nums) {
		result.push_back(remove_multiple(compute_prime_factors(n)));
	}
	return result;
}


// for a list of numbers, return the prime factors that are
// common for each element of the list.
std::vector<uint64_t> get_common_factors(std::vector<std::vector<uint64_t>> &all_factors)
{
	std::vector<uint64_t> common_factors;
	for (auto i=0u; i < all_factors.size(); i++) {
		if (i == 0) {
			common_factors = all_factors[0];
		} else {
			std::vector<uint64_t> new_factors = all_factors[i];
			std::vector<uint64_t> intersection;

			std::set_intersection(common_factors.begin(), common_factors.end(),
			                      new_factors.begin(), new_factors.end(),
			                      std::back_inserter(intersection));
			common_factors = intersection;
		}

	}
	return common_factors;
}

