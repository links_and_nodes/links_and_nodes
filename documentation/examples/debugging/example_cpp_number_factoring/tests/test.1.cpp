#include <catch_amalgamated.hpp>

#include "libprimefactors.h"
#include "error_codes.h"

#include <vector>


TEST_CASE("some trivial base cases", "[prime numbers]")
{
	SECTION("testing with n = 10") {
		uint64_t n = 10;
		std::vector<uint64_t> result=compute_primes(n);

		REQUIRE(result.size() == 5);

		std::vector<uint64_t> expected{1, 2, 3, 5, 7};
		REQUIRE(result == expected);


	}

	SECTION("testing with n = 17") {
		uint64_t n = 17;
		std::vector<uint64_t> result=compute_primes(n);

		REQUIRE(result.size() == 8);

		std::vector<uint64_t> expected{1, 2, 3, 5, 7, 11, 13, 17};
		REQUIRE(result == expected);

	}

}

TEST_CASE("internal edge cases", "[prime numbers]")
{
	SECTION("testing with n = 8") {
		uint64_t n = 8;
		std::vector<uint64_t> result=compute_primes(n);

		REQUIRE(result.size() == 5);

		std::vector<uint64_t> expected{1, 2, 3, 5, 7};
		REQUIRE(result == expected);


	}

	SECTION("testing with n = 9") {
		uint64_t n = 9;
		std::vector<uint64_t> result=compute_primes(n);

		REQUIRE(result.size() == 5);

		std::vector<uint64_t> expected{1, 2, 3, 5, 7};
		REQUIRE(result == expected);

	}

	SECTION("testing with n = 10") {
		uint64_t n = 10;
		std::vector<uint64_t> result=compute_primes(n);

		REQUIRE(result.size() == 5);

		std::vector<uint64_t> expected{1, 2, 3, 5, 7};
		REQUIRE(result == expected);

	}

}


TEST_CASE("Limits", "[prime numbers]")
{
	std::vector<uint64_t> result;

	REQUIRE_NOTHROW(result=compute_primes(1));
	REQUIRE_THROWS_AS(result=compute_primes(0), FactorizerArgumentTooSmallError);
	REQUIRE_THROWS_AS(result=compute_primes(-1), FactorizerArgumentTooSmallError);
	REQUIRE_THROWS_AS(result=compute_primes(1e9), FactorizerArgumentTooLargeError);


}

TEST_CASE("base cases", "[reverse_list]")
{
	SECTION("testing with n = 1") {


		std::vector<uint64_t> input{1};
		std::vector<uint64_t> expected{1};

		reverse_list(input);
		REQUIRE(input == expected);

	}

	SECTION("testing with n = 10") {
		std::vector<uint64_t> input{1, 2, 3, 5, 7};
		std::vector<uint64_t> expected{7, 5, 3, 2, 1};

		reverse_list(input);
		REQUIRE(input == expected);

	}


}


TEST_CASE("factors", "[prime factor]")
{
	SECTION("testing with n = 6") {
		uint64_t n = 6;
		bool reverse_order = false;

		std::vector<uint64_t> result=compute_prime_factors(n, reverse_order);

		REQUIRE(result.size() == 3);

		std::vector<uint64_t> expected{1, 2, 3};
		REQUIRE(result == expected);


	}

	SECTION("testing with n = 12") {
		uint64_t n = 12;
		bool reverse_order = false;

		std::vector<uint64_t> result=compute_prime_factors(n, reverse_order);

		REQUIRE(result.size() == 4);

		std::vector<uint64_t> expected{1, 2, 2, 3};
		REQUIRE(result == expected);


	}

	SECTION("testing with n = 59") {
		uint64_t n = 59;
		bool reverse_order = false;

		std::vector<uint64_t> result=compute_prime_factors(n, reverse_order);

		REQUIRE(result.size() == 2);

		std::vector<uint64_t> expected{1, 59};
		REQUIRE(result == expected);


	}
}



TEST_CASE("base case", "[largest prime factor]")
{
	SECTION("testing with n = 10") {
		uint64_t n = 10;
		uint64_t result=get_largest_prime_factor(n);
		REQUIRE(result == 5);
	}

	SECTION("testing with n = 17") {
		uint64_t n = 17;
		uint64_t result=get_largest_prime_factor(n);
		REQUIRE(result == 17);
	}

	SECTION("testing with n = 69") {
		uint64_t n = 69;
		uint64_t result=get_largest_prime_factor(n);
		REQUIRE(result == 23);
	}
}


TEST_CASE("multiple", "[remove_multiple]")
{
	SECTION("testing with input for 600 and 60") {
		std::vector<uint64_t> input{1, 2, 2, 2, 3, 5, 5};
		std::vector<uint64_t> expected{1, 2, 3, 5};
		std::vector<uint64_t> result =  remove_multiple(input);

		REQUIRE(result == expected);

	}
}



TEST_CASE("factors", "[common prime factors]")
{
	SECTION("testing with input = 600, 60") {
		std::vector<std::vector<uint64_t>> input{{1,2,3,5},
			{1,2,3,5}};

		std::vector<uint64_t> expected{1, 2, 3, 5};
		std::vector<uint64_t> result = get_common_factors(input);

		REQUIRE(result == expected);


	}


}
