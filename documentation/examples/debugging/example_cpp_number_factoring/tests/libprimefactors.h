#include <cstdint>
#include <vector>

static const int64_t min_argument = 1;
static const int64_t max_argument = 1000000;

// Compute list of primes up to a specified number n, using Euclid's
// sieve algorithm.  The resulting primes are sorted beginning from
// the smallest to the largest. The result is returned in a vector of
// uint64_t.
//
// If the number is smaller than min_argument, an exception of type
// FactorizerArgumentTooSmallError is thrown. If the argument is
// larger than max_argument, an exception of type
// FactorizerArgumentTooLargeError is thrown.
std::vector<uint64_t> compute_primes(int64_t n);

// Reverses a list of numbers in-place, doing nothing for an empty
// list or a list of length 1. The argument of this function is
// changed.
void reverse_list(std::vector<uint64_t> &primes);

// Computes a list of prime factors for a number, including 1, sorted
// from the smallest to the largest factor. If a factor
// occurs multiple times, it is returned that many times
// in the result list. The result is returned as a std::vector
// of uint64_t.
//
// If the number is smaller than min_argument, an exception of type
// FactorizerArgumentTooSmallError is thrown. If the argument is
// larger than max_argument, an exception of type
// FactorizerArgumentTooLargeError is thrown.
std::vector<uint64_t> compute_prime_factors(int64_t n, bool reverse_order=false);

// Get the largest prime factor among the prime factors of the number n.
// If n is 1, return 1.
//
// If the number is smaller than min_argument, an exception of type
// FactorizerArgumentTooSmallError is thrown. If the argument is
// larger than max_argument, an exception of type
// FactorizerArgumentTooLargeError is thrown.
uint64_t get_largest_prime_factor(int64_t n);

// Remove multiple identical entries in a list of numbers,
// and return the changed list as the result. The
// argument is not changed.
std::vector<uint64_t> remove_multiple(std::vector<uint64_t> v);

// Take a list of numbers, and returns a list of lists
// with the prime factors for each number, with the
// elements ordered from small to large.
//
// If any input number is smaller than min_argument, an exception of type
// FactorizerArgumentTooSmallError is thrown. If an input number is
// larger than max_argument, an exception of type
// FactorizerArgumentTooLargeError is thrown.
std::vector<std::vector<uint64_t>> compute_all_factors(std::vector<uint64_t> &nums);

// Take a list of lists of prime factors, whith the entries in the
// inner list ordered from smaller to larger values, and returns a
// list with only the factors which occur in every list. As a
// precondition, the numbers in each input list must occur not more
// than once. The factors in the output list will not occur more than
// once.
//
// If the input list is empty, the output list will also be
// empty.
std::vector<uint64_t> get_common_factors(std::vector<std::vector<uint64_t>> &all_factors);
