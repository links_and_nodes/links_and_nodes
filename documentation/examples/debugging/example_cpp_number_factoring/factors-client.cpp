#include <ln/ln.h>

#include "ln_messages.h"
//using namespace std::string_literals;

#include <iostream>

using std::cout;

class FactorizerClient
{
	ln::client* clnt;
	ln::service* get_factors_svc;
	ln::service* get_largest_prime_factor_svc;
	ln::service* get_common_factors_svc;

public:
	FactorizerClient(ln::client* _clnt) : clnt(_clnt)
	{
		get_factors_svc = clnt->get_service("factorizer.compute.factors",
		                                    "factorizer/request/get_prime_factors",
		                                    factorizer_request_get_prime_factors_signature);

		get_largest_prime_factor_svc = clnt->get_service("factorizer.compute.largest",
		                               "factorizer/request/get_largest_prime_factor",
		                               factorizer_request_get_largest_prime_factor_signature);

		get_common_factors_svc = clnt->get_service("factorizer.compute.common",
		                         "factorizer/request/get_common_factors",
		                         factorizer_request_get_common_factors_signature);
	}

	void get_factors(int64_t n, bool reverse_order=false)
	{
		factorizer::request::get_prime_factors_t data{};

		data.req.n = n;
		data.req.reverse_order = reverse_order;
		printf("calling service...\n");

		get_factors_svc->call(&data);

		std::string err_msg(data.resp.error_message,
		                    data.resp.error_message_len);

		cout << "the resulting " << data.resp.prime_factors_len << " values are: \n";
		for (unsigned i=0; i < data.resp.prime_factors_len; i++) {
			cout << data.resp.prime_factors[i] << ' ';
		}

		cout << ",\n  with error code "
		     << (unsigned) data.resp.error_code
		     << " = \"" << err_msg << "\"\n";

	}

	void get_largest_factor(int64_t n)
	{
		factorizer::request::get_largest_prime_factor_t data{};

		data.req.n = n;

		printf("calling service...\n");

		get_largest_prime_factor_svc->call(&data);

		std::string err_msg(data.resp.error_message,
		                    data.resp.error_message_len);

		cout << "the resulting value is: \n"
		     << data.resp.largest_prime_factor
		     << ",\n  with error code "
		     << (unsigned) data.resp.error_code
		     << " = \"" << err_msg << "\"\n";

	}

	void get_common_factors(std::vector<int64_t> &nums, bool debug=false)
	{
		factorizer::request::get_common_factors_t data{};

		data.req.nums = nums.data();
		data.req.nums_len = nums.size();
		data.req.debug = debug;

		printf("calling service...\n");

		get_common_factors_svc->call(&data);

		std::string err_msg(data.resp.error_message,
		                    data.resp.error_message_len);

		cout << "the resulting values are: \n";
		for (unsigned i=0; i<data.resp.common_factors_len; i++) {
			cout << data.resp.common_factors[i] << ' ';
		}

		cout << ",\n  with error code "
		     << (unsigned) data.resp.error_code
		     << " = \"" << err_msg << "\"\n";

	}

	int run()
	{
		while(1) {

			cout << ("type (a) for prime factors, (b) for largest factor,"
			         "  (c) for common factors\n $");
			char choice;
			std::cin >> choice;
			switch (choice) {

			case 'a': {
				int64_t n;

				cout << "factorization: type the requested input number >" << std::flush;
				std::cin >> n;
				cout << "waiting for factorizer....\n";
				get_factors(n);
				break;
			}
			case 'b': {
				int64_t n;

				cout << "largest factor: type the requested input number >" << std::flush;
				std::cin >> n;
				cout << "waiting for factorizer....\n";
				get_largest_factor(n);
				break;
			}
			case 'c': {
				std::vector<int64_t> nums;
				while (true) {
					int64_t n;

					cout << "common factors: type an input number or 0 to finish>" << std::flush;
					std::cin >> n;
					if (n == 0) {
						break;
					}
					nums.push_back(n);

				}
				cout << "debug [y/n]?";
				std::cin >> choice;
				bool debug = choice == 'y';
				cout << "waiting for factorizer....\n";
				get_common_factors(nums, debug);

				break;
			}
			default:
				cout << "choice not recognized\n";
			}

		}
		return 0;
	}
};

int main()
{
	ln::client clnt("factorizer_client_cpp");
	FactorizerClient factorizer_client(&clnt);
	return factorizer_client.run();
}
