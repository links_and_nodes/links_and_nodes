The factorization module should do the following things:

1. compute and return all prime factors of positive numbers (option
   a). The computation should take less than ten seconds,
   on a normal computer.
   
   1.1 If a number is larger than 1000000, an error code and message should be returned. 
   
   1.2 If a number is smaller than 1, an error code should be
   returned.
   
   1.3 The number 1 should be included.
   
   1.4 If a factor value occurs multiple times, it should be returned
   that number of times; For example, the prime factors of 36 should
   be returned as 2,2,3,3.
   
   1.5 The numbers need to be returned in ascending order.
   
2. compute and return only the largest prime factor of a number
   (option b). 
   
   2.1 Numbers that are too large or too small should be
   handled in the same way as for option (a).
   
3. Takes a list of numbers, and computes their common prime factors
   (option c). 
   
   3.1 Any common factor should only be returned once, regardless of
	   how many times it appears in the input numbers.
