#ifndef ERROR_CODES_H
#define ERROR_CODES_H 1

using namespace std::string_literals;

// numerical error codes (for handling in software)
enum class E_FactorizerErrorStatus: uint32_t {
	NO_ERROR                     = 0,
	ARGUMENT_TOO_SMALL           = 1,
	ARGUMENT_TOO_LARGE           = 2,
};

// string error messages (for humans and python clients)
namespace FactorizerErrorMessage
{

static const std::string ARGUMENT_TOO_SMALL  = "Argument is too small"s;
static const std::string ARGUMENT_TOO_LARGE  = "Argument is too large"s;

}


class FactorizerException: public std::exception
{
public:
	const std::string message {};
	const E_FactorizerErrorStatus error_code {};
	FactorizerException(std::string _message, E_FactorizerErrorStatus _error_status):
		std::exception(),
		message("Error "s + std::to_string((unsigned int)_error_status) + ": "s + _message),
		error_code(_error_status)
	{
	}

	const char* what() const noexcept override
	{
		return message.c_str();
	}
};

class FactorizerArgumentTooSmallError: public FactorizerException
{
public:
	FactorizerArgumentTooSmallError(std::string _message="") :
		FactorizerException(
		        FactorizerErrorMessage::ARGUMENT_TOO_SMALL + (_message.empty() ? ""s : (", "s + _message)),
		        E_FactorizerErrorStatus::ARGUMENT_TOO_SMALL)
	{
	}
};

class FactorizerArgumentTooLargeError: public FactorizerException
{
public:
	FactorizerArgumentTooLargeError(std::string _message="") :
		FactorizerException(
		        FactorizerErrorMessage::ARGUMENT_TOO_LARGE + (_message.empty() ? ""s : (" "s + _message)),
		        E_FactorizerErrorStatus::ARGUMENT_TOO_LARGE)
	{
	}
};



#endif
