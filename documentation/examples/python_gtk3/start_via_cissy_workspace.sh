#!/bin/bash

THIS_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
source /opt/rmc-build-tools/sourceme.sh

if [ -z "CISSY_SOURCE_CMD" ]; then
    source /opt/rmc-build-tools/sourceme.sh
fi

exec cissy run
