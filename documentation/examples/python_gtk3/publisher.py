#!/usr/bin/env python3

import sys
import time
import datetime
import gi

gi.require_version("Gtk", "3.0")
gi.require_version("GLib", "2.0")
from gi.repository import Gtk, GLib  # noqa: E402

import links_and_nodes as ln


class MyApp:
    def __init__(self):
        self._init_gui()

        self.clnt = ln.client(sys.argv[0])

        self.time_and_value_port = self.clnt.publish(
            "example.time_and_value", "python_gtk3_example/time_and_value"
        )

        self.publish_timeout_id = None
        self.on_publish_rate_entry_activate(self.publish_rate_entry)

    def _init_gui(self):
        self.window = Gtk.Window(title="example publisher")
        self.window.connect("delete-event", lambda *args: Gtk.main_quit())

        grid = Gtk.Grid(column_spacing=2)
        self.window.add(grid)

        grid.attach(Gtk.Label(label=".date_time_str:", xalign=1), 0, 0, 1, 1)
        self.date_time_str_label = Gtk.Label(xalign=0)
        grid.attach(self.date_time_str_label, 1, 0, 1, 1)

        grid.attach(Gtk.Label(label=".timestamp:", xalign=1), 0, 1, 1, 1)
        self.timestamp_label = Gtk.Label(xalign=0)
        grid.attach(self.timestamp_label, 1, 1, 1, 1)

        grid.attach(Gtk.Label(label=".value:", xalign=1), 0, 2, 1, 1)
        self.value_entry = Gtk.Entry(text="42")
        grid.attach(self.value_entry, 1, 2, 1, 1)

        grid.attach(Gtk.Label(label="publish rate:", xalign=1), 0, 3, 1, 1)
        self.publish_rate_entry = Gtk.Entry(text="20")
        grid.attach(self.publish_rate_entry, 1, 3, 1, 1)
        self.publish_rate_entry.connect("activate", self.on_publish_rate_entry_activate)

        self.window.show_all()

    def on_publish_rate_entry_activate(self, entry):
        publish_rate = float(entry.get_text())

        if self.publish_timeout_id is not None:
            GLib.source_remove(self.publish_timeout_id)

        self.publish_timeout_id = GLib.timeout_add(
            int(1e3 / publish_rate), self.on_publish_timeout
        )

    def on_publish_timeout(self):
        p = self.time_and_value_port.packet

        p.date_time_str = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.date_time_str_label.set_text(p.date_time_str)

        p.timestamp = time.time()
        self.timestamp_label.set_text("%.3f" % p.timestamp)

        try:
            p.value = int(self.value_entry.get_text())
        except:
            pass  # keep old value

        self.time_and_value_port.write()
        return True

    def run(self):
        print("ready")
        Gtk.main()


if __name__ == "__main__":
    app = MyApp()
    app.run()
