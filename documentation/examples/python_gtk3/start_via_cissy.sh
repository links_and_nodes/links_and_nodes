#!/bin/bash

THIS_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
source /opt/rmc-build-tools/sourceme.sh

if [ -z "CISSY_SOURCE_CMD" ]; then
    source /opt/rmc-build-tools/sourceme.sh
fi

exec cissy run -k -p links_and_nodes_manager/[~2]@common/stable ln_manager -c $THIS_DIR/main.lnc
