## python Gtk3 examples

this subdir shows examples how to integrate events from links_and_nodes with Gtk3 in python3. WITHOUT using (python) threads.

there is a simple `publisher`-gui with a corresponding `subscriber`-gui. the subscriber uses a multiwaiter object to have a callback called whenever a new message is received for that topic.

then there are two sets of service provider- & client-guis.
the "wrapped"-variants might feel a little bit more pythonic and should work well for simple & common szenarios.

to start the example while you already have an `ln_manager` in your environment:

    ./start.sh

to start the ln_manager via cissy:

    ./start_via_cissy.sh

to start the ln_manager via a cissy workspace:

    ./start_via_cissy_workspace.sh


there can be only one of the two `service provider`'s running! (they both provide the same service-name).
