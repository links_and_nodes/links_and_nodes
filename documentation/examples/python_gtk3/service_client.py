#!/usr/bin/env python3

import sys
import time
import datetime
import gi

gi.require_version("Gtk", "3.0")
gi.require_version("GLib", "2.0")
from gi.repository import Gtk, GLib  # noqa: E402

import links_and_nodes as ln


class MyApp:
    def __init__(self):
        self._init_gui()

        self.clnt = ln.client(sys.argv[0])

        self.my_mainloop = ln.GLibMainloop()

        self.set_time_and_value_client = self.clnt.get_service(
            "example.set_time_and_value",
            "python_gtk3_example/set_time_and_value",
            mainloop=self.my_mainloop,
        )

    def _init_gui(self):
        self.window = Gtk.Window(title="example service client")
        self.window.connect("delete-event", lambda *args: Gtk.main_quit())

        grid = Gtk.Grid(column_spacing=2)
        self.window.add(grid)

        grid.attach(Gtk.Label(label=".req.date_time_str:", xalign=1), 0, 0, 1, 1)
        self.date_time_str_label = Gtk.Label(xalign=0)
        grid.attach(self.date_time_str_label, 1, 0, 1, 1)

        grid.attach(Gtk.Label(label=".req.timestamp:", xalign=1), 0, 1, 1, 1)
        self.timestamp_label = Gtk.Label(xalign=0)
        grid.attach(self.timestamp_label, 1, 1, 1, 1)

        grid.attach(Gtk.Label(label=".req.value:", xalign=1), 0, 2, 1, 1)
        self.value_entry = Gtk.Entry(text="42")
        grid.attach(self.value_entry, 1, 2, 1, 1)
        self.value_entry.connect("activate", self.on_send_request_button)

        grid.attach(Gtk.Label(label=".req.name:", xalign=1), 0, 3, 1, 1)
        self.name_entry = Gtk.Entry(text="example name")
        grid.attach(self.name_entry, 1, 3, 1, 1)
        self.name_entry.connect("activate", self.on_send_request_button)

        grid.attach(Gtk.Label(label="respone time:", xalign=1), 0, 4, 1, 1)
        self.response_time_label = Gtk.Label(xalign=0)
        grid.attach(self.response_time_label, 1, 4, 1, 1)

        grid.attach(
            Gtk.Label(label=".resp.error_message:", xalign=1, yalign=0), 0, 5, 1, 1
        )
        self.error_message_label = Gtk.Label(xalign=0)
        grid.attach(self.error_message_label, 1, 5, 1, 1)

        grid.attach(Gtk.Label(label=".resp.timestamp:", xalign=1), 0, 6, 1, 1)
        self.resp_timestamp_label = Gtk.Label(xalign=0)
        grid.attach(self.resp_timestamp_label, 1, 6, 1, 1)

        self.send_request_button = Gtk.Button(label="send request")
        grid.attach(self.send_request_button, 0, 7, 2, 1)
        self.send_request_button.connect("clicked", self.on_send_request_button)

        self.window.show_all()

    def on_send_request_button(self, btn_or_entry):
        self.send_request_button.set_sensitive(False)

        # [optional] ask manager to start provider for this service
        self.clnt.needs_provider("example.set_time_and_value")

        req = self.set_time_and_value_client.req

        req.date_time_str = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.date_time_str_label.set_text(req.date_time_str)
        req.timestamp = time.time()
        self.timestamp_label.set_text("%.3f" % req.timestamp)

        try:
            req.value = int(self.value_entry.get_text())
        except:
            pass  # keep old value

        req.name.string = self.name_entry.get_text()

        self.call_start = time.time()
        self.call_done = None
        self.my_mainloop.timeout_add(0.010, self.on_update_response_time)
        # or: GLib.timeout_add(10, self.on_update_response_time)

        self.set_time_and_value_client.call()  # send the request and wait for the response

        self.call_done = time.time()

        resp = self.set_time_and_value_client.resp
        self.error_message_label.set_label(resp.error_message)
        self.resp_timestamp_label.set_label("%.3f" % resp.timestamp)

        self.send_request_button.set_sensitive(True)
        return True

    def on_update_response_time(self):
        if self.call_done is None:
            self.response_time_label.set_text("%.3fs" % (time.time() - self.call_start))
            return True  # not yet finished, let this timeout keep coming
        self.response_time_label.set_text("%.3fs" % (self.call_done - self.call_start))
        return False  # done, timeout no longer needed

    def run(self):
        print("ready")
        Gtk.main()


if __name__ == "__main__":
    app = MyApp()
    app.run()
