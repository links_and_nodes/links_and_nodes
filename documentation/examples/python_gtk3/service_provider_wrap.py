#!/usr/bin/env python3

import sys
import time
import traceback
import datetime
import gi

gi.require_version("Gtk", "3.0")
gi.require_version("GLib", "2.0")
from gi.repository import Gtk, GLib  # noqa: E402

import links_and_nodes as ln


class MyApp(ln.service_provider):
    def __init__(self):
        self._init_gui()

        self.clnt = ln.client(sys.argv[0])

        self.my_mainloop = ln.GLibMainloop()
        ln.service_provider.__init__(
            self, self.clnt, "example", mainloop=self.my_mainloop
        )

        self.wrap_service_provider(
            "set_time_and_value", "python_gtk3_example/set_time_and_value"
        )

    def _init_gui(self):
        self.window = Gtk.Window(title="example service provider")
        self.window.connect("delete-event", lambda *args: Gtk.main_quit())

        grid = Gtk.Grid(column_spacing=2)
        self.window.add(grid)

        grid.attach(Gtk.Label(label=".req.date_time_str:", xalign=1), 0, 0, 1, 1)
        self.date_time_str_label = Gtk.Label(xalign=0)
        grid.attach(self.date_time_str_label, 1, 0, 1, 1)

        grid.attach(Gtk.Label(label=".req.timestamp:", xalign=1), 0, 1, 1, 1)
        self.timestamp_label = Gtk.Label(xalign=0)
        grid.attach(self.timestamp_label, 1, 1, 1, 1)

        grid.attach(Gtk.Label(label=".req.value:", xalign=1), 0, 2, 1, 1)
        self.value_label = Gtk.Label(xalign=0)
        grid.attach(self.value_label, 1, 2, 1, 1)

        grid.attach(Gtk.Label(label=".req.name:", xalign=1), 0, 3, 1, 1)
        self.name_label = Gtk.Label(xalign=0)
        grid.attach(self.name_label, 1, 3, 1, 1)

        grid.attach(Gtk.Label(label=".resp.error_message:", xalign=1), 0, 4, 1, 1)
        self.error_message_entry = Gtk.Entry(text="")
        grid.attach(self.error_message_entry, 1, 4, 1, 1)

        self.window.show_all()

    def set_time_and_value(self, date_time_str, timestamp, value, name):
        self.date_time_str_label.set_text(date_time_str)
        self.timestamp_label.set_text("%.3f" % timestamp)
        self.value_label.set_text("%d" % value)
        self.name_label.set_text(name)
        self._check_for_slow_value(value)
        error_message = self.error_message_entry.get_text()
        if error_message:
            raise Exception("user wants error: %s" % error_message)

        return time.time()

    def _check_for_slow_value(self, value):
        if value < 40:
            print("%s is not a slow value" % value)
            return False
        print("%s is a slow value..." % value)
        # simulate some work, but allow gui loop to process events
        self._waiting = True

        def wait_done():
            self._waiting = False
            return False

        self.my_mainloop.timeout_add(value - 40, wait_done)
        while self._waiting:
            self.my_mainloop.iterate()
        print("now responding.")

    def run(self):
        print("ready")
        Gtk.main()


if __name__ == "__main__":
    app = MyApp()
    app.run()
