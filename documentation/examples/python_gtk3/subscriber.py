#!/usr/bin/env python3

import sys
import time
import datetime
import gi

gi.require_version("Gtk", "3.0")
gi.require_version("GLib", "2.0")
from gi.repository import Gtk, GLib  # noqa: E402

import links_and_nodes as ln


class MyApp:
    def __init__(self):
        self._init_gui()

        self.clnt = ln.client(sys.argv[0])

        self.my_mainloop = ln.GLibMainloop()
        self.mw = ln.MainloopMultiWaiter(self.clnt, self.my_mainloop)

        self.time_and_value_port = self.clnt.subscribe(
            "example.time_and_value", "python_gtk3_example/time_and_value"
        )
        self.packet_count = 0
        self.last_avg_rate = 0

        self.mw.add_port(self.time_and_value_port, self.on_time_and_value)

    def _init_gui(self):
        self.window = Gtk.Window(title="example subscriber")
        self.window.connect("delete-event", lambda *args: Gtk.main_quit())

        grid = Gtk.Grid(column_spacing=2)
        self.window.add(grid)

        grid.attach(Gtk.Label(label=".date_time_str:", xalign=1), 0, 0, 1, 1)
        self.date_time_str_label = Gtk.Label(xalign=0)
        grid.attach(self.date_time_str_label, 1, 0, 1, 1)

        grid.attach(Gtk.Label(label=".timestamp:", xalign=1), 0, 1, 1, 1)
        self.timestamp_label = Gtk.Label(xalign=0)
        grid.attach(self.timestamp_label, 1, 1, 1, 1)

        grid.attach(Gtk.Label(label=".value:", xalign=1), 0, 2, 1, 1)
        self.value_label = Gtk.Label(xalign=0)
        grid.attach(self.value_label, 1, 2, 1, 1)

        grid.attach(Gtk.Label(label="publish rate:", xalign=1), 0, 3, 1, 1)
        self.publish_rate_label = Gtk.Label(xalign=0)
        grid.attach(self.publish_rate_label, 1, 3, 1, 1)

        self.window.show_all()

    def on_time_and_value(self, port):
        p = port.packet

        self.packet_count += 1
        now = time.time()
        if now > self.last_avg_rate + 1:
            self.avg_rate = self.packet_count / (now - self.last_avg_rate)
            self.packet_count = 0
            self.last_avg_rate = now

        self.date_time_str_label.set_text(p.date_time_str)
        self.timestamp_label.set_text("%.3f" % p.timestamp)
        self.value_label.set_text("%d" % p.value)
        self.publish_rate_label.set_text("%.1f" % self.avg_rate)
        return True

    def run(self):
        print("ready")
        Gtk.main()


if __name__ == "__main__":
    app = MyApp()
    app.run()
