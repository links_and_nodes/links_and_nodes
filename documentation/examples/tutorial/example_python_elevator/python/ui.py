#!/usr/bin/env python3
import links_and_nodes as ln
import ast

import collections
from sys import exit

from ElevatorConstants import (
    ElevatorErrorStatus,
    ElevatorException,
    Elevator_Floor_too_small_Error,
    Elevator_Floor_too_large_Error,
    Elevator_Fire_Alarm_Exception,
)


class ElevatorServiceClient:
    def __init__(self):
        self.clnt = ln.client("elevator_service_client")

        self.elevator_call_svc = self.clnt.get_service(
            "elevator03.prompt", "elevator/request/elevator_call"
        )

    def prompt(self, floor_number):
        svc = self.elevator_call_svc
        svc.req.requested_floor = floor_number
        svc.call()
        if len(svc.resp.error_message) or bool(svc.resp.error_code):
            raise Exception(svc.resp.error_message)
        return {
            "error_code": svc.resp.error_code,
            "arrived_floor": svc.resp.arrived_floor,
        }


if __name__ == "__main__":
    elevator_client = ElevatorServiceClient()

    while True:
        floor_string = input("type the number of the floor you want to go to! > ")
        try:
            floor_number = int(floor_string)
        except ValueError:
            print("seems that was not a valid number")
            continue

        print("OK, sending request to go to floor {}".format(floor_number))

        try:
            result = elevator_client.prompt(floor_number)

        except Exception as exc:
            # needed for LN up to version 2.0.x:
            # parse str(exception) to get error code and message
            # (will also work with C++ service providers)
            msg_parts = str(exc).split(":")
            message = msg_parts[1].strip()
            try:
                error_code = int(msg_parts[0].strip().split()[1])
            except TypeError:
                error_code = -1

            print(
                """Info: An error occurred: message = "{}", error_code = {}""".format(
                    message, error_code
                )
            )

            if error_code == ElevatorErrorStatus.FLOOR_NUMBER_TOO_HIGH:
                print("floor number out of range, try a smaller number!")

            elif error_code == ElevatorErrorStatus.FLOOR_NUMBER_TOO_LOW:
                print("floor number out of range, try a larger number!")

            elif error_code == ElevatorErrorStatus.FIRE_ALARM_OPERATION_STOPPED:
                print("fire detected, please take the stairs to leave the building")
                exit(-1)

            else:
                raise

        else:
            # print resulting dictionary
            if result["error_code"] == 0:
                print("The resulting floor is: {arrived_floor}".format(**result))
            else:
                print(
                    "The resulting floor is: {arrived_floor} with "
                    "error code {error_code}".format(**result)
                )
