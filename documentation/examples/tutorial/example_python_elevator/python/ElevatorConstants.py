class MovementDirection:
    UP = 1  # means to move the elevator up, or it is moving up
    DOWN = -1  # means to move the elevator down, or it is moving down
    STOP = 0  # means to not move the elevator, or it is not moving


# numerical error codes
class ElevatorErrorStatus:
    NO_ERROR = 0  # no error happened
    FLOOR_NUMBER_TOO_HIGH = 1  # the floor number was too large
    FLOOR_NUMBER_TOO_LOW = 2  # the floor number was too small
    OUT_OF_ORDER = 3  # the hardware is not working properly
    FIRE_ALARM_OPERATION_STOPPED = 4  # Fire alarm, elevator has stopped at ground floor


class ElevatorException(Exception):
    def __init__(self, msg, error_code):
        Exception.__init__(self, "Error {}: {}".format(error_code, msg))
        self.error_code = error_code


class Elevator_Floor_too_small_Error(ElevatorException):
    def __init__(self, msg=""):
        ElevatorException.__init__(
            self,
            "floor number is too small" + msg,
            ElevatorErrorStatus.FLOOR_NUMBER_TOO_LOW,
        )


class Elevator_Floor_too_large_Error(ElevatorException):
    def __init__(self):
        ElevatorException.__init__(
            self,
            "floor number is too large",
            ElevatorErrorStatus.FLOOR_NUMBER_TOO_HIGH,
        )


class Elevator_OutOfOrderError(ElevatorException):
    def __init__(self, msg=""):
        ElevatorException.__init__(
            self,
            "currently, hardware does not work" + msg,
            ElevatorErrorStatus.OUT_OF_ORDER,
        )


class Elevator_Fire_Alarm_Exception(ElevatorException):
    def __init__(self, msg=""):
        ElevatorException.__init__(
            self,
            "fire detected - elevator stopped at ground floor!" + msg,
            ElevatorErrorStatus.FIRE_ALARM_OPERATION_STOPPED,
        )
