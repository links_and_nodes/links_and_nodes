# LN services

This shows how to run a simple service written in Python in LN.

## Using Cissy

If you conan / cissy installed, you can run this in Cissy.

Just do:

```
rm -rf cissygen

cissy run
```

## Using Conan

If you use Cona, you can use:

```
conan install links_and_nodes_manager/[~2]@common/stable -if conan -g virtualenv -g virtualenv_python -g virtualrunenv -g virtualbuildenv
for i in conan/activate*sh; do source $i; done

ln_manager -c elevator.lnc
```


Otherwise, just run the ln manager config:


```
ln_manager -c elevator.lnc
```

