#!/usr/bin/env python3
import time
import collections
import links_and_nodes as ln

from ElevatorConstants import (
    MovementDirection,
    ElevatorException,
    Elevator_Floor_too_small_Error,
    Elevator_Floor_too_large_Error,
    Elevator_Fire_Alarm_Exception,
)

_clnt = ln.client("elevator controller")
_sensor_port = _clnt.subscribe("elevator03.sensors", "elevator/sensors/floor_count")

# This defines a container class with class name 'ElevatorStatus'
# with conveniently named members.
# See https://docs.python.org/3/library/collections.html#namedtuple-factory\
#     -function-for-tuples-with-named-fields for details.

ElevatorStatus = collections.namedtuple(
    "ElevatorStatus", ["floor_number", "movement_direction", "smoke_detected"]
)


def receive_current_sensor_data(timeout=0.1):

    if not _sensor_port.read(timeout):
        print("warning: no sensor data received from hardware!")

    return ElevatorStatus(
        _sensor_port.packet.floor_number,
        _sensor_port.packet.movement_direction,
        _sensor_port.packet.smoke_detected,
    )


_actor_port = _clnt.publish("elevator03.actors", "elevator/actors/motor_control")


def send_hw_command(move_command):
    _actor_port.packet.move_command = move_command
    _actor_port.write()


class ElevatorProvider(object):
    def __init__(self):
        self.clnt = ln.client("elevator_provider")

        self.elevator_call_svc = self.clnt.get_service_provider(
            "elevator03.prompt", "elevator/request/elevator_call"
        )
        self.elevator_call_svc.set_handler(self.prompt)
        self.elevator_call_svc.do_register("default group")

        self.current_status = receive_current_sensor_data(timeout=-1)

    def run(self):
        print("ready to receive service calls")
        while True:
            self.clnt.wait_and_handle_service_group_requests("default group", 0.5)

    def prompt(self, conn, req, resp):

        MAX_FLOOR = 50
        MIN_FLOOR = -3

        fire_alarm_op = False

        requested_floor = req.requested_floor

        try:
            if requested_floor < MIN_FLOOR:
                raise Elevator_Floor_too_small_Error()

            if requested_floor > MAX_FLOOR:
                raise Elevator_Floor_too_large_Error()

            print("controller: request to go to floor {}".format(requested_floor))
            while True:
                # in case the elevator is moving, wait for it to stop
                while True:
                    self.current_status = receive_current_sensor_data()
                    if self.current_status.smoke_detected:
                        print(
                            "while moving, there was smoke detected, going to the ground floor"
                        )
                        if requested_floor != 0:
                            requested_floor = 0
                            fire_alarm_op = True

                    print(
                        "we are at level {}, going to floor {}".format(
                            self.current_status.floor_number, requested_floor
                        )
                    )
                    if self.current_status.movement_direction == MovementDirection.STOP:
                        break

                # check whether we are already at the requested floor
                if self.current_status.floor_number == requested_floor:
                    # ... and if so, return
                    if fire_alarm_op:
                        raise Elevator_Fire_Alarm_Exception()

                    print(
                        "huzza, we have successfully arrived at floor {}!".format(
                            requested_floor
                        )
                    )

                    resp.arrived_floor = round(self.current_status.floor_number)
                    resp.error_code = 0
                    resp.error_message = None
                    conn.respond()
                    return 0

                # request motor to move
                if self.current_status.floor_number < requested_floor:
                    command = MovementDirection.UP
                    cstr = "UP"
                else:
                    command = MovementDirection.DOWN
                    cstr = "DOWN"
                print("sending command to go {}".format(cstr))

                send_hw_command(command)

                # wait for command to kick in
                while self.current_status.movement_direction == MovementDirection.STOP:
                    # wait for new command to kick in
                    self.current_status = receive_current_sensor_data()

        except ElevatorException as exc:
            print("Error: {}".format(exc))
            resp.arrived_floor = round(self.current_status.floor_number)
            resp.error_code = exc.error_code
            resp.error_message = str(exc)
            conn.respond()
            return 0


if __name__ == "__main__":
    p = ElevatorProvider()
    p.run()
