#!/usr/bin/env python3
import links_and_nodes as ln
from os import path

_sensor_clnt = ln.client("elevator, floor count sensor")
_sensor_port = sensor_clnt.publish("elevator.sensors", "elevator/sensors/floor_count")

_actor_clnt = ln.client("elevator, motor drive controller")
_actor_port = actor_clnt.subscribe("elevator.actors", "elevator/actors/motor_control")


def publish_current_count(floor_number, movement_direction):

    _sensor_port.packet.floor_number = floor_number
    _sensor_port.packet.movement_direction = movement_direction
    # if we find smoke, we raise a fire alarm
    _sensor_port.packet.smoke_detected = path.exists("smoke")

    _sensor_port.write()


def receive_commands():
    packet = _actor_port.read()
    return packet.move_command
