#!/usr/bin/env python3
import links_and_nodes as ln
from os import path
import numpy
import time

_sensor_clnt = ln.client("elevator, floor count sensor")
_sensor_port = sensor_clnt.publish("elevator.sensors", "elevator/sensors/floor_count")

_actor_clnt = ln.client("elevator, motor drive controller")
_actor_port = actor_clnt.subscribe("elevator.actors", "elevator/actors/motor_control")


class MovementDirection:
    UP = 1
    DOWN = -1
    STOP = 0


def publish_current_count(floor_number, movement_direction):

    print(
        "the current floor number is {}, the movement direction is {}".format(
            floor_number, movement_direction
        )
    )

    _sensor_port.packet.floor_number = floor_number
    _sensor_port.packet.movement_direction = movement_direction
    # if we find smoke, we raise a fire alarm
    _sensor_port.packet.smoke_detected = path.exists("smoke")

    _sensor_port.write()


def receive_commands():
    packet = _actor_port.read()
    cmd = packet.move_command
    print("got command move to {}".format(cmd))
    return cmd


current_floor_number = 0
current_command = float(MovementDirection.STOP)
print("Started!")


while True:
    current_movement_direction = int(numpy.sign(current_command))
    publish_current_count(current_floor_number, current_movement_direction)
    print("The current command is to move by {}".format(current_command))

    if current_movement_direction != 0:
        step = current_movement_direction / 16.0
        print("moving elevator by {}".format(step))

        current_floor_number += step
        current_command -= step
        time.sleep(0.2)

    else:
        print("elevator_simulator: waiting for commands...")
        current_command = receive_commands()
