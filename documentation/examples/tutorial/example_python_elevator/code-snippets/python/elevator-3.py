#!/usr/bin/env python3
import links_and_nodes as ln

_sensor_clnt = ln.client("elevator, floor count sensor")
_sensor_port = sensor_clnt.publish("elevator.sensors", "elevator/sensors/floor_count")

_actor_clnt = ln.client("elevator, motor drive controller")
_actor_port = actor_clnt.subscribe("elevator.actors", "elevator/actors/motor_control")


def publish_current_count(floor_number, movement_direction):

    _sensor_port.packet.floor_number = floor_number
    _sensor_port.packet.movement_direction = movement_direction

    _sensor_port.write()
