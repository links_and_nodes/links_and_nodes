#!/usr/bin/env python3
import links_and_nodes as ln
from os import path
import numpy
import time

from ElevatorConstants import MovementDirection

_sensor_clnt = ln.client("elevator, floor count sensor")
_sensor_port = sensor_clnt.publish("elevator.sensors", "elevator/sensors/floor_count")

_actor_clnt = ln.client("elevator, motor drive controller")
_actor_port = actor_clnt.subscribe("elevator.actors", "elevator/actors/motor_control")


def publish_current_count(floor_number, movement_direction):

    print(
        "the current floor number is {}, the movement direction is {}".format(
            floor_number, movement_direction
        )
    )

    _sensor_port.packet.floor_number = floor_number
    _sensor_port.packet.movement_direction = movement_direction
    # if we find smoke, we raise a fire alarm
    _sensor_port.packet.smoke_detected = path.exists("smoke")

    _sensor_port.write()


def receive_commands():
    packet = _actor_port.read()
    cmd = packet.move_command
    print("got command move to {}".format(cmd))
    return cmd


current_floor_number = 0
current_movement_direction = MovementDirection.STOP
current_command = MovementDirection.STOP
print("Started!")


while True:
    publish_current_count(current_floor_number, current_movement_direction)

    if current_command != 0:
        step = numpy.sign(current_command)
        print("moving elevator by {}".format(step))

        current_command -= step
        current_floor_number += step
        current_movement_direction = step
        time.sleep(1)

    else:
        current_command = receive_commands()
