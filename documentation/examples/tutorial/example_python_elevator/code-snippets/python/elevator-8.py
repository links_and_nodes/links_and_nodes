#!/usr/bin/env python3
import time
import collections
import links_and_nodes as ln

from ElevatorConstants import (
    MovementDirection,
    ElevatorException,
    Elevator_Floor_too_small_Error,
    Elevator_Floor_too_large_Error,
    Elevator_Fire_Alarm_Exception,
)

_clnt = ln.client("elevator controller")
_sensor_port = _clnt.subscribe("elevator03.sensors", "elevator/sensors/floor_count")

# This defines a container class with class name 'ElevatorStatus'
# with conveniently named members.
# See https://docs.python.org/3/library/collections.html#namedtuple-factory\
#     -function-for-tuples-with-named-fields for details.

ElevatorStatus = collections.namedtuple(
    "ElevatorStatus", ["floor_number", "movement_direction", "smoke_detected"]
)


def receive_current_sensor_data(timeout=0.1):

    if not _sensor_port.read(timeout):
        print("warning: no sensor data received from hardware!")

    return ElevatorStatus(
        _sensor_port.packet.floor_number,
        _sensor_port.packet.movement_direction,
        _sensor_port.packet.smoke_detected,
    )


_actor_port = _clnt.publish("elevator03.actors", "elevator/actors/motor_control")


def send_hw_command(move_command):
    _actor_port.packet.move_command = move_command
    _actor_port.write()


class ElevatorProvider(object):
    def __init__(self):
        self.clnt = ln.client("elevator_provider")

        self.elevator_call_svc = self.clnt.get_service_provider(
            "elevator03.prompt", "elevator/request/elevator_call"
        )
        self.elevator_call_svc.set_handler(self.prompt)
        self.elevator_call_svc.do_register("default group")

        self.current_status = receive_current_sensor_data(timeout=-1)

    def run(self):
        print("ready to receive service calls")
        while True:
            self.clnt.wait_and_handle_service_group_requests("default group", 0.5)

    def prompt(self, conn, req, resp):

        MAX_FLOOR = 50
        MIN_FLOOR = -3

        requested_floor = req.requested_floor

        while True:

            ## control logic is still missing here

            # in case the elevator is moving, wait for it to stop
            resp.arrived_floor = round(self.current_status.floor_number)
            resp.error_code = 0
            resp.error_message = None

            conn.respond()
            return 0


if __name__ == "__main__":
    p = ElevatorProvider()
    p.run()
