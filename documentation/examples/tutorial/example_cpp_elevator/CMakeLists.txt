cmake_minimum_required(VERSION 3.5)

set(CMAKE_CXX_STANDARD 14)

project (elevator_project)

include_directories(.)

set(md_dir ${CMAKE_SOURCE_DIR}/msg_defs)

# we need to generate two file lists here:
# one to define the dependencies (absolute paths)
# and one to pass the message names (relative paths)

file(GLOB_RECURSE elevator_messages LIST_DIRECTORIES false RELATIVE ${md_dir} "${md_dir}/*")
file(GLOB_RECURSE elevator_messages_path LIST_DIRECTORIES false  "${md_dir}/*")

add_custom_command(
  OUTPUT ln_messages.h
  COMMAND ln_generate -f -md_dir ${md_dir} ${elevator_messages}
  DEPENDS ${elevator_messages_path}
)
include_directories(${CMAKE_CURRENT_BINARY_DIR})


add_executable(controller controller.cpp elevator_constants.h ln_messages.h)

add_executable(elevator-simulation elevator-simulation.cpp elevator_constants.h ln_messages.h)

add_executable(ui ui.cpp elevator_constants.h ln_messages.h)

find_package(Boost 1.58.0 REQUIRED)
find_package(liblinks_and_nodes REQUIRED)

include_directories(${Boost_INCLUDE_DIRS})

include_directories(${liblinks_and_nodes_INCLUDE_DIRS})

target_link_libraries(ui ${liblinks_and_nodes_LIBRARIES})
target_link_libraries(controller ${liblinks_and_nodes_LIBRARIES})
target_link_libraries(elevator-simulation ${liblinks_and_nodes_LIBRARIES})

 
install(TARGETS ui controller elevator-simulation DESTINATION bin)
