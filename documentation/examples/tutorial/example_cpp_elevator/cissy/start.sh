#!/bin/bash

if [ -z "$CISSY_SOURCE_CMD" ]; then
    DEFAULT_CISSY_SOURCE=/opt/rmc-build-tools/sourceme.sh
    echo "sourcing $DEFAULT_CISSY_SOURCE ..."
    source $DEFAULT_CISSY_SOURCE
fi

THIS=$(readlink -f ${BASH_SOURCE[0]})
THIS_DIR=$(dirname $THIS)

cd $THIS_DIR
(
    pushd ..
    conan create . elevator/0.0.1@user/testing
)

exec cissy run -k

