# -*- coding: utf-8 -*-

import os
from conans import ConanFile, tools, CMake


class elevatorConan(ConanFile):

    name = "elevator"

    author = "Johannes Nix <johannes.nix@dlr.de>"

    developers = ["Johannes Nix"]

    license = "GPL"

    url = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"

    homepage = "https://wiki.robotic.dlr.de/Links_and_Nodes"

    description = "example for a C++ project build definition"

    topics = (
        "c++",
        "links_and_nodes",
    )

    requires = [
        "links_and_nodes_python/[~=2]@common/stable",
        "liblinks_and_nodes/[~=2]@common/stable",
        "boost/1.58.0@3rdparty/stable",
    ]

    build_requires = [
        "links_and_nodes_base_python/[~=2]@common/stable",
    ]

    default_user = "common"
    default_channel = "testing"

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = ["*", "!.gitignore"] + [
        "!%s" % x for x in tools.Git().excluded_files()
    ]

    no_copy_source = True

    source_dir = "."

    def package_info(self):

        from cissy.conantools import autoset_package_info

        autoset_package_info(self)

    generators = (
        "cmake_find_package",
        "ln_env",
        "virtualenv",
        "virtualenv_python",
        "virtualrunenv",
        "virtualbuildenv",
    )

    def init_cmake(self):
        cmake = CMake(self)
        cmake.verbose = True
        cmake.configure()
        return cmake

    def build(self):
        cmake = self.init_cmake()
        cmake.build()

    def package(self):
        cmake = self.init_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.bindirs = ["bin"]
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
