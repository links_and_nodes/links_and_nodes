#ifndef ELEVATOR_CONSTANTS_H
#define ELEVATOR_CONSTANTS_H 1

using namespace std::string_literals;

enum class E_MovementDirection: int8_t {
	UP   =  1,
	DOWN = -1,
	STOP =  0,
};

// numerical error codes (for handling in software)
enum class E_ElevatorErrorStatus: uint32_t {
	NO_ERROR                     = 0,
	FLOOR_NUMBER_TOO_HIGH        = 1, // the requested floor number is too high
	FLOOR_NUMBER_TOO_LOW         = 2, // the requested floor number is too low
	OUT_OF_ORDER                 = 3, // the hardware is not working properly
	FIRE_ALARM_OPERATION_STOPPED = 4  // Fire alarm, elevator has stopped at ground floor

};

class ElevatorException: public std::exception
{
public:
	const E_ElevatorErrorStatus error_code {};
	const std::string message {};
	ElevatorException(std::string _message, E_ElevatorErrorStatus _error_status):
		std::exception(),
		message("Error "s + std::to_string((unsigned int)_error_status) + ": "s + _message),
		error_code(_error_status)
	{
	}

	const char* what() const noexcept override
	{
		return message.c_str();
	}
};

class ElevatorFloorNumberTooLowError: public ElevatorException
{
public:
	ElevatorFloorNumberTooLowError(std::string _message=""):
		ElevatorException(
		        "floor number is too small"s + (
		                _message.empty() ? ""s : (", "s + _message)),
		        E_ElevatorErrorStatus::FLOOR_NUMBER_TOO_LOW)

	{
	}
};

class ElevatorFloorNumberTooHighError: public ElevatorException
{
public:
	ElevatorFloorNumberTooHighError(std::string _message=""):
		ElevatorException(
		        "floor number is too large"s + (
		                _message.empty() ? ""s : (", "s + _message)),
		        E_ElevatorErrorStatus::FLOOR_NUMBER_TOO_HIGH)

	{
	}
};

class ElevatorOutOfOrderError: public ElevatorException
{
public:
	ElevatorOutOfOrderError(std::string _message=""):
		ElevatorException(
		        "currently, hardware does not work"s + (
		                _message.empty() ? ""s : (", "s + _message)),
		        E_ElevatorErrorStatus::OUT_OF_ORDER)

	{
	}
};

class ElevatorFireAlarmException: public ElevatorException
{
public:
	ElevatorFireAlarmException(std::string _message=""):
		ElevatorException(
		        "fire detected - elevator stopped at ground floor!"s + (
		                _message.empty() ? ""s : (", "s + _message)),
		        E_ElevatorErrorStatus::FIRE_ALARM_OPERATION_STOPPED)

	{
	}
};




#endif
