enum class E_MovementDirection: int8_t {
	UP   =  1,
	DOWN = -1,
	STOP =  0,
};
