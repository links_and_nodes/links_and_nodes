#include <ln/ln.h>
#include <iostream>
#include <cstdint>
#include <chrono>
#include "elevator_constants.h"

#include "ln_messages.h"


using std::cout;
using namespace std::chrono_literals;



class ElevatorServer :
	public elevator::request::elevator_call_base
{

	using t_sensors = elevator_sensors_floor_count_t;
	using t_motor_control = elevator_actors_motor_control_t;

	ln::client clnt;
	ln::inport* sensor_port;
	ln::outport* actor_port;

	t_motor_control motor_command {};
	t_sensors sensor_data {};
public:
	ElevatorServer() :
		clnt("elevator controller")
	{

		sensor_port = clnt.subscribe("elevator03.sensors",
		                             "elevator/sensors/floor_count");

		actor_port = clnt.publish("elevator03.actors",
		                          "elevator/actors/motor_control");


		cout << "getting hardware state...\n";

		receive_current_sensor_data();

		cout << "floor number is: " << sensor_data.floor_number << '\n';

		cout << "service thread started!\n";
	}

	int run()
	{
		// to be done
	}

private:

	void receive_current_sensor_data(double time_out=1.0)
	{
		cout << "getting state\n";
		if (! sensor_port->read(&sensor_data, time_out)) {
			cout << "Warning: no sensor data was received!\n";
		} else {
			cout << "getting state ... ok\n";
		}
	}

	void send_hw_command(int32_t command)
	{

		motor_command.move_command = command;
		actor_port->write(&motor_command);
	}


};


int main(int argc, char* argv[])
{
	ElevatorServer elevator_server;
	return elevator_server.run();
}
