#include <ln/ln.h>

#include "ln_messages.h"


// [ ... ]

int32_t receive_commands(ln::inport* in_port, int32_t current_command)
{
	elevator_actors_motor_control_struct in_data {};
	if (in_port->read(&in_data)) {
		current_command = in_data.move_command;
	}
	return current_command;
}

