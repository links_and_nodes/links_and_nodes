#include <ln/ln.h>
#include <cstdint>
#include "elevator_constants.h"

#include "ln_messages.h"

using std::cout;
using namespace std::chrono_literals;



using t_sensors = elevator_sensors_floor_count_t;

ln::inport* sensor_port;

t_sensors sensor_data {};

ln::client clnt ln::client("elevator controller");

sensor_port = clnt.subscribe("elevator03.sensors",
                             "elevator/sensors/floor_count");

// receiving data
using std::cout;
void receive_current_sensor_data(double time_out=1.0)
{
	if (!sensor_port->read(&sensor_data, time_out)) {
		cout << "Warning: no sensor data was received!\n";
	}

}

// sending commands
using t_motor_control = elevator_actors_motor_control_t;

ln::outport* actor_port;

t_motor_control motor_command {};

actor_port = clnt.publish("elevator03.actors",
                          "elevator/actors/motor_control");

void send_hw_command(int32_t command)
{
	motor_command.move_command = command;
	actor_port->write(&motor_command);
}
