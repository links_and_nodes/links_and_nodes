#include <ln/ln.h>
#include <iostream>
#include <cstdint>
#include <chrono>
#include <thread>


#include "ln_messages.h"


using std::cout;
using namespace std::chrono_literals;


int sign(int32_t value)
{
	if (value > 0)
		return 1;
	else if (value < 0)
		return -1;
	else
		return 0;
}

int main(int argc, char* argv[])
{

	ln::client clnt("elevator simulation", argc, argv);

	ln::outport* oport = clnt.publish("elevator.sensors", "elevator/sensors/floor_count");
	ln::inport* iport = clnt.subscribe("elevator.actors", "elevator/actors/motor_control");

	float current_floor_number = 0.0f;
	int32_t current_command = (int32_t) E_MovementDirection::STOP;
	cout << "elevator hardware running\n";
	while(true) {
		int current_movement_direction = sign(current_command);
		publish_current_count(oport, current_floor_number, current_movement_direction);

		if (current_movement_direction != 0) {
			float step = current_movement_direction / 16.0f;

			cout << "moving elevator by " << step << '\n';

			current_floor_number += step;
			current_command -= step;

			std::this_thread::sleep_for(200ms);
		} else {
			cout << "elevator_simulator: waiting for commands...\n";
			current_command = receive_commands(iport, current_command);
		}

	};
}
