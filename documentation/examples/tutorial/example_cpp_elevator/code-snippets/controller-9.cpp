#include <ln/ln.h>
#include <cstdint>
#include <chrono>
#include "elevator_constants.h"

// ln_messages.h needs to be generated by invoking the command
// ln_generate --md-dir msg_defs/ elevator/sensors/floor_count elevator/actors/motor_control
// (either manually or through the build system)

#include "ln_messages.h"
#include <sstream>




//////////////////////////////////////////////////////////////////

// copied from https://wiki.robotic.dlr.de/Links_and_nodes/c%2B%2B_examples

using std::cout;
using namespace std::chrono_literals;


class ElevatorServer
{

	using t_sensors = elevator_sensors_floor_count_t;
	using t_motor_control = elevator_actors_motor_control_t;

	ln::client clnt;
	ln::inport* sensor_port;
	ln::outport* actor_port;
	ln::service* elevator_call_svc = nullptr;

	t_motor_control motor_command {};
	t_sensors sensor_data {};
public:
	ElevatorServer() :
		clnt("elevator controller")
	{

		sensor_port = clnt.subscribe("elevator03.sensors",
		                             "elevator/sensors/floor_count");

		actor_port = clnt.publish("elevator03.actors",
		                          "elevator/actors/motor_control");


		elevator_call_svc = clnt.get_service_provider(
		                            "elevator03.prompt",
		                            "elevator/request/elevator_call",
		                            elevator_request_elevator_call_signature);

		elevator_call_svc->set_handler(handle_elevator_call, this);
		elevator_call_svc->do_register("default group");

		cout << "getting hardware state...\n";

		receive_current_sensor_data();

		cout << "floor number is: " << sensor_data.floor_number << '\n';
	}

	int run()
	{
		cout << "ready to receive service calls\n";
		while (1) {
			/// processing...
			double time_out = 0.5; // seconds
			clnt.wait_and_handle_service_group_requests("default group", time_out);
		}
	}

private:
	static int handle_elevator_call(::ln::client& clnt, ::ln::service_request& req, void* self_)
	{
		ElevatorServer* self = (ElevatorServer*)self_;
		elevator::request::elevator_call_t data{};

		req.set_data(&data, elevator_request_elevator_call_signature);

		return self->on_elevator_call(req, data);
	}



	// Try to move the elevator to the requested floor, by sending
	// commands to the hardware.
	// It returns the floor which the elevator has actually arrived at.
	// Input parameter is the requested floor.
	//
	// Attention: This function can throw an exception of the type
	// EelvatorException, with an error code as documented in
	// elevator_constants.h and
	// msg_defs/elevator/request/elevator_call .

	int32_t request_floor(int32_t requested_floor)
	{
		using MovDir = E_MovementDirection;
		bool fire_alarm_op = false;

		while (true) {
			while(sensor_data.movement_direction != static_cast<int8_t>(MovDir::STOP)) {
				receive_current_sensor_data(0.5);

				if (sensor_data.smoke_detected != 0) {
					fire_alarm_op = true;
					// go to ground floor
					requested_floor = 0;
				}
			}

			if (requested_floor == sensor_data.floor_number) {
				if (fire_alarm_op) {
					throw ElevatorFireAlarmException();
				}
				break;
			}
			MovDir command;
			if (sensor_data.floor_number < requested_floor) {
				command = MovDir::UP;
			} else {
				command = MovDir::DOWN;
			}
			send_hw_command(static_cast<int32_t>(command));
			while(sensor_data.movement_direction == static_cast<int8_t>(MovDir::STOP)) {
				receive_current_sensor_data(0.5);
			}

		}
		return sensor_data.floor_number;
	}

	int on_elevator_call(ln::service_request& req,
	                     elevator::request::elevator_call_t& data) override
	{

		namespace EEM = ElevatorErrorMessage;
		using EES = E_ElevatorErrorStatus;
		const int MAX_FLOOR = 50;
		const int MIN_FLOOR = -3;

		try {

			if (data.req.requested_floor < MIN_FLOOR) {
				throw ElevatorFloorNumberTooLowError();

			}

			if (data.req.requested_floor > MAX_FLOOR) {
				throw ElevatorFloorNumberTooHighError();
			}

			const int32_t arrived_floor = request_floor(data.req.requested_floor);

			data.resp.arrived_floor = arrived_floor;
			data.resp.error_code = 0;
			data.resp.error_message = (char*)"";
			data.resp.error_message_len = 0;
			req.respond();

		} catch (ElevatorException &e) {

			data.resp.error_code = static_cast<uint32_t>(e.error_code);
			char msg[512];
			int len=0;
			len = snprintf(msg, sizeof(msg), "('%s', %i)", e.what(),
			               static_cast<uint32_t>(e.error_code));
			data.resp.error_message = msg;
			data.resp.error_message_len = len;
			data.resp.arrived_floor = sensor_data.floor_number;
			req.respond();
		}

		return 0;
	}


	void receive_current_sensor_data(double time_out=1.0)
	{
		cout << "getting state\n";
		if (! sensor_port->read(&sensor_data, time_out)) {
			cout << "Warning: no sensor data was received!\n";
		} else {
			cout << "getting state ... ok\n";
		}
	}

	void send_hw_command(int32_t command)
	{

		motor_command.move_command = command;
		actor_port->write(&motor_command);
	}



};


int main(int argc, char* argv[])
{
	ElevatorServer elevator_server;
	return elevator_server.run();
}
