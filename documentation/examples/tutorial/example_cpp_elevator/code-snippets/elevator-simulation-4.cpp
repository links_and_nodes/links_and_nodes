#include <ln/ln.h>

#include "ln_messages.h"

using t_motor_control=elevator_actors_motor_control_t;

// [ ... ]

int32_t receive_commands(ln::inport* in_port, int32_t current_command)
{
	double time_out = 0.5;
	elevator_actors_motor_control_struct in_data {};
	if (in_port->read(&in_data, time_out)) {
		current_command = in_data.move_command;
	}
	return current_command;
}

