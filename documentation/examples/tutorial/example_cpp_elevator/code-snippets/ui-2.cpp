#include <ln/ln.h>
#include "ln_messages.h"

#include <iostream>

using std::cout;

class ElevatorClient
{
	ln::client* clnt;
	ln::service* elevator_call_svc;

public:
	ElevatorClient(ln::client* _clnt) : clnt(_clnt)
	{
		elevator_call_svc = clnt->get_service("elevator03.prompt",
		                                      "elevator/request/elevator_call",
		                                      elevator_request_elevator_call_signature);
	}

	void elevator_call(int called_floor)
	{
		elevator::request::elevator_call_t data {};

		data.req.requested_floor = called_floor;

		elevator_call_svc->call(&data);

		std::string err_msg(data.resp.error_message,
		                    data.resp.error_message_len);

		cout << "the resulting floor is: "
		     << (int) data.resp.arrived_floor
		     << ",  with error code "
		     << (unsigned) data.resp.error_code
		     << "\n";

	}

	int run()
	{
		while(true) {
			int called_floor;

			cout << "type the requested floor number >" << std::flush;
			std::cin >> called_floor;
			cout << "waiting for elevator....\n";
			elevator_call(called_floor);
		}
	}
};

int main(int argc, char* argv[])
{
	ln::client clnt("elevator_client_cpp");
	ElevatorClient elevator_client(&clnt);
	return elevator_client.run();
}

