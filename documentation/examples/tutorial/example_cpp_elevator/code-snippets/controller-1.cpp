#include <ln/ln.h>
#include <cstdint>
#include "elevator_constants.h"

#include "ln_messages.h"

using std::cout;
using namespace std::chrono_literals;



using t_sensors = elevator_sensors_floor_count_t;

ln::inport* sensor_port;

t_sensors sensor_data {};

ln::client clnt ln::client("elevator controller");

sensor_port = clnt.subscribe("elevator03.sensors",
                             "elevator/sensors/floor_count");


void receive_current_sensor_data()
{
	sensor_port->read(&sensor_data);
}


