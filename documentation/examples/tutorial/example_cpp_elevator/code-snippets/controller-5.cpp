#include <ln/ln.h>
#include <iostream>
#include <cstdint>
#include "elevator_constants.h"

#include "ln_messages.h"


using std::cout;
using namespace std::chrono_literals;



class ElevatorServer
{

	using t_sensors = elevator_sensors_floor_count_t;
	using t_motor_control = elevator_actors_motor_control_t;

	ln::client clnt;
	ln::inport* sensor_port;
	ln::outport* actor_port;
	ln::service* elevator_call_svc = nullptr;

	t_motor_control motor_command {};
	t_sensors sensor_data {};
public:
	ElevatorServer() :
		clnt("elevator controller")
	{

		sensor_port = clnt.subscribe("elevator03.sensors",
		                             "elevator/sensors/floor_count");

		actor_port = clnt.publish("elevator03.actors",
		                          "elevator/actors/motor_control");


		elevator_call_svc = clnt.get_service_provider(
		                            "elevator03.prompt",
		                            "elevator/request/elevator_call",
		                            elevator_request_elevator_call_signature);

		elevator_call_svc->set_handler(handle_elevator_call, this);
		elevator_call_svc->do_register("default group");

		cout << "getting hardware state...\n";

		receive_current_sensor_data();

		cout << "floor number is: " << sensor_data.floor_number << '\n';
	}

	int run()
	{
		cout << "ready to receive service calls\n";
		while (1) {
			/// processing...
			double time_out = 0.5; // seconds
			clnt.wait_and_handle_service_group_requests("default group", time_out);
		}
	}


private:

	static int handle_elevator_call(::ln::client& clnt, ::ln::service_request& req, void* self_)
	{
		ElevatorServer* self = (ElevatorServer*)self_;
		elevator::request::elevator_call_t data{};

		req.set_data(&data, elevator_request_elevator_call_signature);

		return self->on_elevator_call(req, data);
	}

	void receive_current_sensor_data(double time_out=1.0)
	{
		cout << "getting state\n";
		if (! sensor_port->read(&sensor_data, time_out)) {
			cout << "Warning: no sensor data was received!\n";
		} else {
			cout << "getting state ... ok\n";
		}
	}

	void send_hw_command(int32_t command)
	{

		motor_command.move_command = command;
		actor_port->write(&motor_command);
	}


};


int main(int argc, char* argv[])
{
	ElevatorServer elevator_server;
	return elevator_server.run();
}
