#include <ln/ln.h>
#include "ln_messages.h"

using t_sensors = elevator_sensors_floor_count_t;


void publish_current_count(ln::outport* out_port, float floor_number,
                           int8_t movement_direction)
{
	t_sensors out_data {};
	out_data.floor_number = floor_number;
	out_data.movement_direction = static_cast<int8_t>(movement_direction);
	out_port->write(&out_data);
}
