#include <ln/ln.h>
#include <unistd.h>

#include "ln_messages.h"


using namespace std::chrono_literals;

using t_sensors = elevator_sensors_floor_count_t;

using std::cout;


void publish_current_count(ln::outport* out_port, float floor_number,
                           int8_t movement_direction)
{

	t_sensors out_data {};
	out_data.floor_number = floor_number;
	out_data.movement_direction = static_cast<int8_t>(movement_direction);
	// check whether smoke is found,
	// represented by a file "smoke" in the CWD
	out_data.smoke_detected = (access("smoke", F_OK) != -1);
	out_port->write(&out_data);

}
