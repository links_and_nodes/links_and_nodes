class PKG2(ConanFile):
    name = "PKG2"
    settings = "os", "compiler", "build_type", "arch"
    ...
    def build_requirements(self):
        self.build_requires("links_and_nodes_base_python/[~1]@common/stable") # for ln_generate
        self.build_requires("PKG1_ln_msgdef/[~1.2]@common/stable") # for PKG1_major/MD1
        
    def requirements(self):
        self.requires("liblinks_and_nodes/[~1]@common/stable") # for <ln/ln.h> & libln.so

    def build(self):
        ...
