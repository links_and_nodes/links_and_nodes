...
class PKG1(ConanFile):
    name = "PKG1"
    settings = "os", "compiler", "build_type", "arch"
    ...
    def build_requirements(self):
        self.build_requires("links_and_nodes_base_python/[~1]@common/stable") # for ln_generate
        
    def requirements(self):
        self.requires("liblinks_and_nodes/[~1]@common/stable") # for <ln/ln.h> & libln.so
        self.requires("PKG1_ln_msgdef/1.2.0@common/stable") # own msg-defs, see below...

    def build(self):
        ...
