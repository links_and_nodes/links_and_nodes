#!/usr/bin/env python3

import time
import links_and_nodes as ln

clnt = ln.client("counter publisher")
port = clnt.publish("counter1.count_message", "counter/count_message")
while True:
    time.sleep(0.1)  # or wait for an event of interrest

    port.packet.time = time.time()  # fill new message packet
    port.packet.num_counts = (port.packet.num_counts + 1) % 1024

    port.write()  # send/publish
