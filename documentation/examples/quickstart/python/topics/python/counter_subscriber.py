#!/usr/bin/env python3
import links_and_nodes as ln

clnt = ln.client("first subscriber")
port = clnt.subscribe("counter1.count_message", "counter/count_message")
while True:
    count_packet = port.read()
    print("time: %.1f, num_counts: %s" % (count_packet.time, count_packet.num_counts))
