# LN services

This shows how to run a simple service written in Python in LN.

## If you have cissy installed, you can run this in Cissy.

Just do:

```
rm -rf cissygen

cissy run
```

## If you use Conan, you can do:

## Using Conan

If you use Conan, you can use:

```
conan install links_and_nodes_manager/[~2]@common/stable -if conan -g virtualenv -g virtualenv_python -g virtualrunenv -g virtualbuildenv
for i in conan/activate*sh; do source $i; done

ln_manager -c sample_services.lnc
```


Otherwise, just run the ln manager config:


```
ln_manager -c sample_services.lnc
```

