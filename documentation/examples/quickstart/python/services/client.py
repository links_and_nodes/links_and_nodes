#!/usr/bin/env python3

import random
import links_and_nodes as ln


class area_service_client(object):
    def __init__(self):
        self.clnt = ln.client("area_service_client")

        self.compute_circle_svc = self.clnt.get_service(
            "area_service.compute_circle_area", "area_service/circle_area"
        )
        self.compute_triangle_svc = self.clnt.get_service(
            "area_service.compute_triangle_area", "area_service/triangle_area"
        )
        self.compute_ellipse_svc = self.clnt.get_service(
            "area_service.compute_ellipse_area", "area_service/ellipse_area"
        )

    def compute_circle_area(self, radius):
        svc = self.compute_circle_svc
        svc.req.radius = radius
        svc.call()
        if svc.resp.error_code != 0:
            print(
                "Error: code = {}, message = {}".format(
                    svc.resp.error_code, svc.resp.error_message
                )
            )
        return svc.resp.area

    def compute_triangle_area(self, base_length, height):
        svc = self.compute_triangle_svc
        svc.req.base_length = base_length
        svc.req.height = height
        svc.call()
        if svc.resp.error_code != 0:
            print(
                "Error: code = {}, message = {}".format(
                    svc.resp.error_code, svc.resp.error_message
                )
            )
        return svc.resp.area

    def compute_ellipse_area(self, a, b):
        svc = self.compute_ellipse_svc
        svc.req.a = a
        svc.req.b = b
        svc.call()
        if svc.resp.error_code != 0:
            print(
                "Error: code = {}, message = {}".format(
                    svc.resp.error_code, svc.resp.error_message
                )
            )
        return svc.resp.area


if __name__ == "__main__":
    c = area_service_client()

    for n in range(10):
        a = random.uniform(-2, 10)
        A = c.compute_circle_area(a)

        print("circle result: %5.2f" % A)

    for n in range(10):
        a = random.uniform(-2, 10)
        b = random.uniform(0, 10)
        A = c.compute_triangle_area(a, b)

        print("triangle result: %5.2f" % A)

    for n in range(10):
        a = random.uniform(-2, 10)
        b = random.uniform(0, 10)
        A = c.compute_ellipse_area(a, b)

        print("ellipse result: %5.2f" % A)
