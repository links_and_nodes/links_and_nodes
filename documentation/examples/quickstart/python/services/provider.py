#!/usr/bin/env python3

import links_and_nodes as ln
import traceback
import math
from math import pi

from area_api_constants import ErrorCodes as ec


class area_provider(object):
    def __init__(self):
        self.clnt = ln.client("test_provider")

        self.circle_provider = self.clnt.get_service_provider(
            "area_service.compute_circle_area", "area_service/circle_area"
        )
        self.circle_provider.set_handler(self.compute_circle_area)
        self.circle_provider.do_register("default group")

        self.triangle_provider = self.clnt.get_service_provider(
            "area_service.compute_triangle_area", "area_service/triangle_area"
        )
        self.triangle_provider.set_handler(self.compute_triangle_area)
        self.triangle_provider.do_register("default group")

        self.ellipse_provider = self.clnt.get_service_provider(
            "area_service.compute_ellipse_area", "area_service/ellipse_area"
        )
        self.ellipse_provider.set_handler(self.compute_ellipse_area)
        self.ellipse_provider.do_register("default group")

    def compute_circle_area(self, conn, req, resp):
        if req.radius < 0:
            resp.area = math.nan
            resp.error_message = "compute_circle_area(): negative radius input value"
            resp.error_code = ec.NEGATIVE_INPUT
            print(
                "compute_circle_area(): we return an error and NaN as value for a negative radius"
            )

        else:
            if req.radius == 0:
                resp.error_message = "compute_circle_area(): zero in radius input value"
                resp.error_code = ec.ZERO_INPUT
            else:
                resp.error_code = ec.NO_ERROR

            area_val = pi * req.radius**2
            print(
                "compute_circle_area(): circle with radius %5.2f has area %5.2f\n"
                % (req.radius, area_val)
            )
            resp.area = area_val

        conn.respond()
        return 0

    def compute_triangle_area(self, conn, req, resp):
        if req.base_length < 0 or req.height < 0:
            resp.area = math.nan
            resp.error_message = (
                "compute_triangle_area(): negative side length input value"
            )
            resp.error_code = ec.NEGATIVE_INPUT
            print(
                "compute_triangle_area(): we return an error and NaN as"
                " value for a negative side length"
            )

        else:
            if req.base_length == 0 or req.height == 0:
                resp.error_message = (
                    "compute_triangle_area(): zero in side length input value"
                )
                resp.error_code = ec.ZERO_INPUT
            else:
                resp.error_code = ec.NO_ERROR

            area_val = (req.base_length * req.height) / 2.0
            print(
                "compute_triangle_area(): triangle with base length = %5.2f "
                "and height = %5.2f has area  %f\n"
                % (req.base_length, req.height, area_val)
            )
            resp.area = area_val

        conn.respond()
        return 0

    class EllipseException(Exception):
        def __init__(self, code, message, optret=None):
            self.error_code = code
            self.error_message = message
            self.optret = optret
            Exception.__init__(self)

        def __str__(self):
            return "EllipseException: %r (code %r)" % (
                self.error_message,
                self.error_code,
            )

    def lib_compute_ellipse_area(self, a, b):
        # this simulates a function from an external library
        if a < 0 or b < 0:
            raise self.EllipseException(
                ec.NEGATIVE_INPUT,
                "compute_ellipse_area(): negative axis length input value",
                math.nan,
            )

        if a == 0 or b == 0:
            raise self.EllipseException(
                ec.ZERO_INPUT, "compute_ellipse_area(): zero in axis length input value"
            )

        area_val = pi * a * b

        print(
            "compute_ellipse_area(): ellipse with semi-major axis a %5.2f "
            "and semi-minor axis b %5.2f has area  %5.2f\n" % (a, b, area_val)
        )
        return area_val

    def compute_ellipse_area(self, conn, req, resp):
        try:
            resp.area = self.lib_compute_ellipse_area(req.a, req.b)
            resp.error_code = ec.NO_ERROR
            resp.error_message = ""  # todo: report as bug against LN api

        except self.EllipseException as e:
            print("compute_ellipse_area(): %s" % str(e))
            resp.error_code = e.error_code
            resp.error_message = e.error_message
            if e.optret is not None:
                resp.area = e.optret

        except:
            resp.error_code = ec.OTHER_ERROR
            resp.error_message = traceback.format_exc()
            print("compute_ellipse_area(): unknown exception:\n%s" % resp.error_message)

        conn.respond()
        return 0

    def run(self):
        print("ready to receive service calls")
        while True:
            self.clnt.wait_and_handle_service_group_requests("default group", 0.5)


if __name__ == "__main__":
    p = area_provider()
    p.run()
