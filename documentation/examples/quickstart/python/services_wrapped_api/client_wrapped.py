#!/usr/bin/env python3

import random
import links_and_nodes as ln


class area_service_client(ln.services_wrapper):
    def __init__(self):
        self.clnt = ln.client("area_service_client")
        ln.services_wrapper.__init__(self, self.clnt, "area_service")

        self.wrap_service("compute_circle_area", "area_service/circle_area")
        self.wrap_service("compute_triangle_area", "area_service/triangle_area")
        self.wrap_service("compute_ellipse_area", "area_service/ellipse_area")


if __name__ == "__main__":
    c = area_service_client()

    for n in range(10):
        r = random.uniform(-5, 20)
        try:
            print("computing circle area with radius r = {:.2f}".format(r))
            Result = c.compute_circle_area(r)
            # look up result from a call that returns multiple fields in a dict
            print("circle result: %5.2f" % Result["area"])
        # responses with a non-empty error message are considered
        # as errors, and are turned into exceptions
        except Exception as e:
            print("Error: ", str(e))
            print("continuing...")

    for n in range(10):
        a = random.uniform(-2, 10)
        b = random.uniform(0, 10)
        try:
            Result = c.compute_triangle_area(a, b)
        except Exception as e:
            print("Error: ", str(e))
            print("continuing...")

        print("triangle result: %5.2f" % Result["area"])

    for n in range(10):
        a = random.uniform(-2, 10)
        b = random.uniform(0, 10)
        try:
            Result = c.compute_ellipse_area(a, b)
        except Exception as e:
            print("Error: ", str(e))
            print("continuing...")

        print("ellipse result: %5.2f" % Result["area"])
