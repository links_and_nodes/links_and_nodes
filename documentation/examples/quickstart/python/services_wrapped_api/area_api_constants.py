class ErrorCodes:
    NO_ERROR = 0  # no error
    ZERO_INPUT = 1  # an input value was zero
    NEGATIVE_INPUT = 2  # an input value was negative
