#!/usr/bin/env python3

import links_and_nodes as ln
import math
from math import pi
from area_api_constants import ErrorCodes as ec


class area_provider(ln.service_provider):
    def __init__(self):
        self.clnt = ln.client("test_provider")
        ln.service_provider.__init__(self, self.clnt, "area_service")

        self.wrap_service_provider("compute_circle_area", "area_service/circle_area")
        self.wrap_service_provider(
            "compute_triangle_area", "area_service/triangle_area"
        )
        self.wrap_service_provider("compute_ellipse_area", "area_service/ellipse_area")

    def compute_circle_area(self, radius):
        """This service handler function demonstrates error
        checking and returning error values, using the
        wrapped API.
        """

        if radius < 0:
            print("we return an error and NaN as value for a negative radius")
            area_val = math.nan
            error_message = "negative radius input value"
            error_code = ec.NEGATIVE_INPUT

        else:
            if radius == 0:
                error_message = "zero in radius input value"
                error_code = ec.ZERO_INPUT
            else:
                error_message = ""
                error_code = ec.NO_ERROR

            area_val = pi * radius**2
            print("circle with radius %5.2f has area %5.2f" % (radius, area_val))

        # multiple fields are returned in a dict
        return {
            "area": area_val,
            "error_code": error_code,
            "error_message": error_message,
        }

    def compute_triangle_area(self, base_length, height):
        if base_length < 0 or height < 0:
            print("we return an error and NaN as value for a negative input lengths")
            area_val = math.nan
            error_message = "negative input length value"
            error_code = ec.NEGATIVE_INPUT

        else:
            if base_length == 0 or height == 0:
                error_message = "zero in input length value"
                error_code = ec.ZERO_INPUT
            else:
                error_message = ""
                error_code = ec.NO_ERROR

            area_val = (base_length * height) / 2.0
            print(
                "triangle with base length = %5.2f  and height = %5.2f has area  %f\n"
                % (base_length, height, area_val)
            )

        # multiple fields are returned in a dict
        return {
            "area": area_val,
            "error_code": error_code,
            "error_message": error_message,
        }

    def compute_ellipse_area(self, a, b):
        if a < 0 or b < 0:
            print("we return an error and NaN as value for a negative radius")
            area_val = math.nan
            error_message = "negative axis length input value"
            error_code = ec.NEGATIVE_INPUT

        else:
            if a == 0 or b == 0:
                error_message = "zero in axis length input value"
                error_code = ec.ZERO_INPUT
            else:
                error_message = ""
                error_code = ec.NO_ERROR

            area_val = pi * a * b
            print(
                "ellipse with semi-major axis a %5.2f  and semi-minor axis b %5.2f"
                " has area  %5.2f\n" % (a, b, area_val)
            )

        # multiple fields are returned in a dict
        return {
            "area": area_val,
            "error_code": error_code,
            "error_message": error_message,
        }

    def run(self):
        self.handle_service_group_requests()


if __name__ == "__main__":
    p = area_provider()
    p.run()
