#include <ln/ln.h>
#include <cstdint>
#include "area_api_constants.h"
#include <iostream>
#include <cmath>

#include "ln_messages.h"


using std::cout;



class AreaServer :
	public area_service::circle_area_base,
	public area_service::ellipse_area_base
{

	ln::client clnt;
public:
	AreaServer() :
		clnt("C++ area service")
	{

		register_circle_area(&clnt, "area_service.circle_area");
		register_ellipse_area(&clnt, "area_service.ellipse_area");

	}


	int on_circle_area(ln::service_request& req,
	                   area_service::circle_area_t& data) override
	{


		if (data.req.radius == 0.0) {
			data.resp.error_code = static_cast<uint32_t>(E_AreaErrorStatus::ZERO_INPUT);
			data.resp.error_message = (char*)"on_circle_area(): input was zero";

		} else if (data.req.radius < 0.0) {
			data.resp.area = NAN;
			data.resp.error_code = static_cast<uint32_t>(E_AreaErrorStatus::NEGATIVE_INPUT);
			data.resp.error_message = (char*)"on_circle_area(): input was negative";

		} else {
			const double radius = data.req.radius;
			data.resp.area = M_PI * radius * radius;

			data.resp.error_code = static_cast<uint32_t>(E_AreaErrorStatus::NO_ERROR);
			data.resp.error_message = (char*)"";
		}
		data.resp.error_message_len = strlen(data.resp.error_message);
		req.respond();


		cout << "request finished!\n";
		return 0;
	}

	int on_ellipse_area(ln::service_request& req,
	                    area_service::ellipse_area_t& data) override
	{


		if ((data.req.a < 0.0) || (data.req.b < 0.0)) {

			data.resp.area = NAN;
			data.resp.error_code = static_cast<uint32_t>(E_AreaErrorStatus::NEGATIVE_INPUT);
			data.resp.error_message = (char*)"on_ellipse_area(): input was negative";

		} else if ((data.req.a == 0.0) || (data.req.b == 0.0)) {

			data.resp.error_code = static_cast<uint32_t>(E_AreaErrorStatus::ZERO_INPUT);
			data.resp.error_message = (char*)"on_ellipse_area(): input was zero";

		} else  {
			const double a = data.req.a;
			const double b = data.req.a;
			data.resp.area = M_PI * a * b;

			data.resp.error_code = static_cast<uint32_t>(E_AreaErrorStatus::NO_ERROR);
			data.resp.error_message = (char*)"";
		}
		data.resp.error_message_len = strlen(data.resp.error_message);
		req.respond();


		cout << "request finished!\n";
		return 0;
	}


	int run()
	{
		cout << "ready to receive service calls\n";
		while (1) {
			/// processing...
			double time_out = -1; // means blocking
			clnt.wait_and_handle_service_group_requests(NULL, time_out);
		}
	}


};


int main(int argc, char* argv[])
{
	AreaServer area_server;
	return area_server.run();
}
