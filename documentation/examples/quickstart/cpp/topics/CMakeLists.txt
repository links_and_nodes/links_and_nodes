cmake_minimum_required (VERSION 3.1)

set(CMAKE_CXX_STANDARD 14)

project (counter_project)

add_executable(counter_subscriber counter_subscriber.cpp ln_messages.h)

add_executable(counter_publisher counter_publisher.cpp ln_messages.h)

target_link_libraries(counter_subscriber "libln.so")
target_link_libraries(counter_publisher "libln.so")


set(md_dir ${CMAKE_SOURCE_DIR}/msg_defs)

file(GLOB_RECURSE elevator_messages LIST_DIRECTORIES false "${md_dir}/*")

add_custom_command(
  OUTPUT ln_messages.h
  COMMAND ln_generate -f -md_dir ${md_dir} ${counter_message_set}
  DEPENDS ${counter_message_set}
)

 
