[requires]
links_and_nodes_manager/2.0.4@common/stable

[options]
*:python_version=3

[generators]
virtualenv
ln_env
virtualenv_python
virtualrunenv
virtualbuildenv

