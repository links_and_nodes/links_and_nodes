#include <ln/ln.h>
#include "ln_messages.h"
#include "area_api_constants.h"
using namespace std::string_literals;

#include <iostream>

using std::cout;

class AreaClient
{
	ln::client* clnt;
	ln::service* circle_area_svc;
	ln::service* ellipse_area_svc;

public:
	AreaClient(ln::client* _clnt) : clnt(_clnt)
	{
		circle_area_svc = clnt->get_service(
		                          "area_service.compute_circle_area",
		                          "area_service/circle_area",
		                          area_service_circle_area_signature);

		ellipse_area_svc = clnt->get_service("area_service.compute_ellipse_area",
		                                     "area_service/ellipse_area",
		                                     area_service_ellipse_area_signature);
	}

	double circle_area(double radius)
	{
		area_service::circle_area_t data{};

		data.req.radius = radius;

		circle_area_svc->call(&data);


		if (data.resp.error_code > 0) {
			std::string err_msg(data.resp.error_message,
			                    data.resp.error_message_len);

			cout << "WARNING: request returned  with error code "
			     << (unsigned) data.resp.error_code
			     << ", message = \"" << err_msg << "\"\n";
		}
		return data.resp.area;
	}

	double ellipse_area(double a, double b)
	{
		area_service::ellipse_area_t data{};

		data.req.a = a;
		data.req.b = b;


		ellipse_area_svc->call(&data);


		if (data.resp.error_code > 0) {
			std::string err_msg(data.resp.error_message,
			                    data.resp.error_message_len);

			cout << "WARNING: request returned  with error code "
			     << (unsigned) data.resp.error_code
			     << ", message = \"" << err_msg << "\"\n";
		}
		return data.resp.area;
	}

	int run()
	{
		for(double radius=-1; radius < 11; radius += 1.0) {

			cout << "calling circle service for r = " << radius << '\n';

			double area = circle_area(radius);

			cout << "the resulting circle area is: "
			     <<  area << '\n';
		}

		for(double a=-1; a < 5; a += 1.0)
			for(double b = 2; b < 5; b += 2.0) {

				cout << "calling ellipse service for a = " << a
				     << ", b = " << b << '\n';

				double area = ellipse_area(a, b);

				cout << "the resulting ellipse area is: "
				     <<  area << '\n';
			}

		return 0;
	}
};

int main(int argc, char* argv[])
{
	ln::client clnt("area_client (C++)");
	AreaClient area_client(&clnt);
	return area_client.run();
}
