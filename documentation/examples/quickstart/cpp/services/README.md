# LN services

This shows how to run a simple service written in Python in LN.

## Using Cissy

If you have conan / cissy installed, you can run this in Cissy.

Just do:

```
rm -rf cissygen

cissy run
```

## Using Conan

Using Conan, you can run the example with:

```bash

conan install links_and_nodes_manager/[~2]@common/stable -if conan -g virtualenv  -g virtualrunenv -g virtualbuildenv
for i in conan/activate*sh; do source $i; done

ln_manager -c sample_services.lnc
```


Otherwise, just run the ln manager config:


```
ln_manager -c test.lnc
```

