#ifndef AREA_SERVICE_CONSTANTS_H
#define AREA_SERVICE_CONSTANTS_H 1

// numerical error codes (for handling in software)
enum class E_AreaErrorStatus: uint32_t {
	NO_ERROR                     = 0,
	ZERO_INPUT                   = 1, // an input value was zero
	NEGATIVE_INPUT               = 2, // an input value was negative

};




#endif
