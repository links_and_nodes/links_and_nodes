#include <ln/ln.h>
#include <cstdint>
#include "area_api_constants.h"
#include <iostream>
#include <cmath>

#include "ln_messages.h"

using std::cout;

class AreaServer
{

	ln::client clnt;
	ln::service* area_circle_svc = nullptr;
	ln::service* area_ellipse_svc = nullptr;

public:
	AreaServer() :
		clnt("C++ area service")
	{

		area_circle_svc = clnt.get_service_provider(
		                          "area_service.compute_circle_area",
		                          "area_service/circle_area",
		                          area_service_circle_area_signature);
		area_circle_svc->set_handler(call_circle_area, this);
		area_circle_svc->do_register("default group");

		area_ellipse_svc = clnt.get_service_provider(
		                           "area_service.compute_ellipse_area",
		                           "area_service/ellipse_area",
		                           area_service_ellipse_area_signature);
		area_ellipse_svc->set_handler(call_ellipse_area, this);
		area_ellipse_svc->do_register("default group");
	}

	~AreaServer()
	{
		clnt.release_service(area_circle_svc);
		clnt.release_service(area_ellipse_svc);
	}

	static int call_circle_area(::ln::client& clnt, ::ln::service_request& req, void* self_)
	{
		AreaServer* self = (AreaServer*)self_;
		area_service::circle_area_t data{};
		req.set_data(&data, area_service_circle_area_signature);

		return self->on_circle_area(req, data);
	}

	int on_circle_area(ln::service_request& req,
	                   area_service::circle_area_t& data) const
	{


		if (data.req.radius == 0.0) {
			data.resp.error_code = static_cast<uint32_t>(E_AreaErrorStatus::ZERO_INPUT);
			data.resp.error_message = (char*)"on_circle_area(): input was zero";

		} else if (data.req.radius < 0.0) {
			data.resp.area = NAN;
			data.resp.error_code = static_cast<uint32_t>(E_AreaErrorStatus::NEGATIVE_INPUT);
			data.resp.error_message = (char*)"on_circle_area(): input was negative";

		} else {
			const double radius = data.req.radius;
			data.resp.area = M_PI * radius * radius;

			data.resp.error_code = static_cast<uint32_t>(E_AreaErrorStatus::NO_ERROR);
			data.resp.error_message = (char*)"";
		}
		data.resp.error_message_len = strlen(data.resp.error_message);
		req.respond();


		cout << "request finished!\n";
		return 0;
	}

	static int call_ellipse_area(::ln::client& clnt, ::ln::service_request& req, void* user_data)
	{
		AreaServer* self = (AreaServer*)user_data;
		area_service::ellipse_area_t data{};
		req.set_data(&data, area_service_ellipse_area_signature);
		return self->on_ellipse_area(req, data);
	}

	int on_ellipse_area(ln::service_request& req,
	                    area_service::ellipse_area_t& data) const
	{


		if ((data.req.a < 0.0) || (data.req.b < 0.0)) {

			data.resp.area = NAN;
			data.resp.error_code = static_cast<uint32_t>(E_AreaErrorStatus::NEGATIVE_INPUT);
			data.resp.error_message = (char*)"on_ellipse_area(): input was negative";

		} else if ((data.req.a == 0.0) || (data.req.b == 0.0)) {

			data.resp.error_code = static_cast<uint32_t>(E_AreaErrorStatus::ZERO_INPUT);
			data.resp.error_message = (char*)"on_ellipse_area(): input was zero";

		} else  {
			const double a = data.req.a;
			const double b = data.req.a;
			data.resp.area = M_PI * a * b;

			data.resp.error_code = static_cast<uint32_t>(E_AreaErrorStatus::NO_ERROR);
			data.resp.error_message = (char*)"";
		}
		data.resp.error_message_len = strlen(data.resp.error_message);
		req.respond();


		cout << "request finished!\n";
		return 0;
	}


	int run()
	{
		cout << "ready to receive service calls\n";
		while (true) {
			clnt.wait_and_handle_service_group_requests("default group", 0.5);
		}
	}


};


int main(int argc, char* argv[])
{
	AreaServer area_server;
	return area_server.run();
}
