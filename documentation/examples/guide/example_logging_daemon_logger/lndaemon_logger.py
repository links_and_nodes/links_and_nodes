#!/usr/bin/python
# -*- mode: python -*-

import sys
import time
import links_and_nodes as ln

from numpy import *

logfile = "/tmp/log_test.topics.pcap"

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Please specify logfile and command as arguments to script!")
        print("Usage:")
        print("  %s <pickle logfile> <command>" % sys.argv[0])
        sys.exit(1)

    clnt = ln.client("ln_daemon log test", sys.argv)
    logger = clnt.get_logger("test_logger")

    logfile = sys.argv[1]
    command = sys.argv[2]

    if command == "start":
        logger.add_topic("test.topic1", 100000)
        logger.add_topic("test.topic2", 100000)
        logger.enable()
        print("Started logging...")

    elif command == "stop":
        print("Stop logging...")
        logger.disable()

        print("Saving logfile as %s at manager" % logfile)
        logger.manager_save(logfile)

    else:
        raise Exception(
            "Command '%s' not known! Only commands supported: start, stop" % command
        )
