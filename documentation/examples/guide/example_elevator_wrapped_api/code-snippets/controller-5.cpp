#include <ln/ln.h>
#include <iostream>
#include <cstdint>
#include "elevator_constants.h"

#include "ln_messages.h"


using std::cout;
using namespace std::chrono_literals;



class ElevatorServer :
	public elevator::request::elevator_call_base
{

	using t_sensors = elevator_sensors_floor_count_t;
	using t_motor_control = elevator_actors_motor_control_t;

	ln::client clnt;
	ln::inport* sensor_port;
	ln::outport* actor_port;

	t_motor_control motor_command;
	t_sensors sensor_data;
public:
	ElevatorServer() :
		clnt("elevator controller")
	{

		sensor_port = clnt.subscribe("elevator03.sensors",
		                             "elevator/sensors/floor_count");

		actor_port = clnt.publish("elevator03.actors",
		                          "elevator/actors/motor_control");


		register_elevator_call(&clnt, "elevator03.prompt");

		cout << "getting hardware state...\n";

		receive_current_sensor_data();

		cout << "floor number is: " << sensor_data.floor_number << '\n';
		clnt.handle_service_group_in_thread_pool(NULL, "main_pool");
		cout << "service thread started!\n";
	}


	int run()
	{
		cout << "ready to receive service calls\n";
		while (1) {
			/// processing...
			double time_out = -1; // means blocking
			clnt.wait_and_handle_service_group_requests(NULL, time_out);
		}
	}

private:

	int on_elevator_call(ln::service_request& req,
	                     elevator::request::elevator_call_t& svc) override
	{
		// .... to be implemented
	}


};


int main(int argc, char* argv[])
{
	ElevatorServer elevator_server;
	return elevator_server.run();
}
