#include <ln/ln.h>
#include "ln_messages.h"
#include "elevator_constants.h"
using namespace std::string_literals;

#include <iostream>

using std::cout;

class ElevatorClient
{
	ln::client* clnt;
	ln::service* elevator_call_service;

public:
	ElevatorClient(ln::client* _clnt) : clnt(_clnt)
	{
		elevator_call_service = clnt->get_service("elevator03.prompt",
		                        "elevator/request/elevator_call",
		                        elevator_request_elevator_call_signature);
	}

	void elevator_call(int called_floor)
	{
		elevator::request::elevator_call_t svc;

		svc.req.requested_floor = called_floor;

		printf("calling service...\n");

		elevator_call_service->call(&svc);

		std::string err_msg(svc.resp.error_message,
		                    svc.resp.error_message_len);

		cout << "the resulting floor is: "
		     << (int) svc.resp.arrived_floor << '\n';

		if (svc.resp.error_code > 0) {
			cout << "request returned  with error code "
			     << (unsigned) svc.resp.error_code
			     << " = \"" << err_msg << "\"\n";
		}

		const E_ElevatorErrorStatus ecode =
		        static_cast<E_ElevatorErrorStatus>(svc.resp.error_code);
		switch (ecode) {
		case E_ElevatorErrorStatus::NO_ERROR :
			break;
		case E_ElevatorErrorStatus::FLOOR_NUMBER_TOO_LOW :
			throw ElevatorFloorNumberTooLowError(err_msg);
		/* break omitted, this line is unreachable */
		case E_ElevatorErrorStatus::FLOOR_NUMBER_TOO_HIGH :
			throw ElevatorFloorNumberTooHighError(err_msg);
		/* break omitted, this line is unreachable */
		case E_ElevatorErrorStatus::OUT_OF_ORDER :
			throw ElevatorOutOfOrderError(err_msg);
		/* break omitted, this line is unreachable */
		case E_ElevatorErrorStatus::FIRE_ALARM_OPERATION_STOPPED :
			throw ElevatorFireAlarmException(err_msg);
		/* break omitted, this line is unreachable */
		default:
			throw ElevatorException("unknown error code", ecode);

		}
	}

	int run()
	{
		while(1) {
			int called_floor;

			cout << "type the requested floor number >" << std::flush;
			std::cin >> called_floor;
			cout << "waiting for elevator....\n";
			try {
				elevator_call(called_floor);
			} catch (ElevatorFloorNumberTooLowError &e) {
				cout << "request out of range - try a larger number!\n";
				continue;
			} catch (ElevatorFloorNumberTooHighError &e) {
				cout << "request out of range - try a smaller number!\n";
				continue;
			}

			catch (ElevatorFireAlarmException &e) {
				cout << ("Fire alarm - elevator stopped, please "
				         "leave the building!\n");
				return -1;
			}

		}
		return 0;
	}
};

int main(int argc, char* argv[])
{
	ln::client clnt("elevator_client_cpp");
	ElevatorClient elevator_client(&clnt);
	return elevator_client.run();
}
