# -*- encoding: utf-8 -*-

from __future__ import print_function

import sys
import time
import math

import links_and_nodes as ln

clnt = ln.client("publish", sys.argv)
args = clnt.get_remaining_args()[1:]

rate = 3000.0

port0 = clnt.publish("topic0", "lnrdb_example/two_doubles") # does not publish any messages
port1 = clnt.publish("topic1", "lnrdb_example/two_doubles")
port2 = clnt.publish("topic2", "lnrdb_example/two_vectors")

print("ready")

t0 = time.time()

p1 = port1.packet
p2 = port2.packet

step = 1

N = len(p2.q)
t = 0
while True:
    t += step

    # generate example data
    p1.a = t
    p1.b = t / 2
    for i in range(N):
        p2.q[i] = t + 0.1 * i

        for r in range(6):
            p2.J[r * N + i] = r + t + 0.1 * i
        p2.flag = int(t) % 5
    
    port1.write()
    port2.write()

    sim_time = t / step / rate
    real_time = time.time() - t0

    to_sleep = sim_time - real_time
    if to_sleep > 0:
        time.sleep(to_sleep)
