
from __future__ import print_function, division

import sys
import h5py
import numpy as np

list_of_datasets = []
def printshape(name):
    try:
        print(name, f[name].shape)
        list_of_datasets.append(name)
        # note that in LNRDB, the final part of the dataset name
        # (after the topic) is a process-global index that numbers the
        # order in which the topic was started to be
        # recorded. Multiple runs generate multiple datasets (and the
        # order in which topic recording starta can change if topics
        # start in parallel!).
    except AttributeError:
        print(name, "has no shape")

def show_extract(f, dataset_name):
    dset = f[dataset_name]

    print("dataset %s" % dataset_name)
    print("   shape = %r" % dset.shape)

    if dset.shape[0] > 0:
        print("   first record:", dset[0])
        print("   last record:", dset[-1])

        if "q" in dset.dtype.names:   # about equivalent to dset.has_key("q")
            n_rec = dset.shape[0]
            middle_idx = n_rec // 2
            print("   middle idx = ", middle_idx)
            # note that it is much faster to combine indices here,
            # or to extract a small object first!
            print("   middle q   = ", dset[middle_idx, "q"])
            print("   middle J   = ", np.reshape(dset[middle_idx, "J"], (7,6)))
            print("   middle flag   = ", dset[middle_idx, "flag"])
        
with h5py.File(sys.argv[1], "r") as f: 
    f.visit(printshape)

    for ds in list_of_datasets:
        show_extract(f, ds)
    
