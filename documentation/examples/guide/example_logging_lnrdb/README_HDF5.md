## Pros / Cons LNRDB / Matlab

### Advantages of LNRDB

* binary format that is easy to process without further tools / libs
* metadata available as human-readable data
* good support for Python/Numpy
* possibly good long-term storage format because of its simplicity
* efficient to process if environment makes use of memory-mapping

### Disadvantages of LNRDB

* accessing data and metadata in object-oriented way needs to be coded
  as a support library for each language
* without library support, correct manual decoding requires
  that offsets and addresses match message definitions exactly.
  Any change of message definitions (such as adding/deleting fields)
  may require careful updates to functions that read from topics,
  to avoid mis-interpreting binary data.
* only simple indexing operations are directly supported
* efficient support limited for some environments

### Advantages of HDF5

* format that is easy to process and well integrated into libraries such as Numpy
* very efficient storage format
* very easy to inspect and extract time series, even if message definitions change
* all metadata is stored in the same file
* very easy to extract and view metadata
* efficient indexing and reading of subsets (especially if environment
  supports memory-mapped data, such as Python/Numpy)
* libraries to read and write files already available for all relevant
  platforms, such as Matlab
* supports many complex operations

### Potential Disadvantages of HDF5

* complex open-source library from a single provider, thus possibly
  not ideal for long-term archival storage (20+ years)
* reproduces several abstractions that are already provided by file systems

