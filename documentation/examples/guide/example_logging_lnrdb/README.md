# example

use

    ./start.sh

to start lnm via cissy and workspace.

## generating data

start `publisher` process. it will publish 3 topics with ~3kHz.

## recording data

start `lnrecorder` it will log all those topics with ~500Hz subscriber rate.
stop this reocrder after some seconds.

ATTENTION:
  you need to use lnrecorder from links_And_nodes_runtime/2.1.0@common/unstable!!
  earlier versions might not close files correctly before shutting down, which might lead to dataloss!


## reading data from LNRDB files

now you can start the `python example read log` process, to test a simple python script, reading and displaying recorded data.

## converting LNRDB to HDF5 files

start "python example convert log". This converts the data from lnrdb to HDF5 format
(the first parameter being the output HDF5 file, and the following ones the recordings from lnrecorder).

## reading HDF5 files from Python

those data files can easily be read in 3rd party programs:

start "python example read hdf5". This read and shows the HDF5 data.

## reading HDF5 files from Matlab

start "isolated matlab" process, and execute example_hdf5.m .

This shows how to read data from specific points of
a very large file with time series, using h5read without
loading all the data into memory. (Note: Matlab does NOT
create memory-mapped vectors or matrices when reading hdf5 files,
which means that any objects once they have been read using h5read,
need to fit into RAM completely.)

## reading LNRDB files from Matlab

start the `isolated matlab` process, wait for it to startup and then, on the matlab prompt open the `example.m` file:

    open example.m

this is a notebook-like file with cells.
place the cursor in the first cell, i.e. in the 2nd line on `data = ...`.
press `Ctrl-Enter` to execute the cell and observe its output on the matlab console.

there are two naive functions `read_topic1()` and `read_topic2()` shows.
they each can only read the lnrdb data file with correct/expected ln-md.

For example.m to work unmodified, the whole topic that is read
needs to fit into memory, because all of the time series is loaded
into one matlab object.

a more sophisticated matlab code could read the human- and machine-readable `meta`-files aside each `data` file to automatically extract all fields.

files too large to completely read into matlab memory can be read in parts by using `fseek` to
seek to some specific position within the file, and then only read a fixed amount of records from there...


## Advantages of HDF5

* format that is easy to use and process 
* well integrated into Python / Numpy 
* standardized libraries to read and write files already available for
  all relevant platforms, such as Matlab
* support random access pattern if samples even in very large data files,
  without needing to load them into memory
* very efficient storage format
* very easy to inspect and extract time series by name
* all metadata and time series are stored in the same file
* very easy to attach, extract and view metadata, including message definitions
* efficient indexing and reading of subsets 
* supports memory-mapped data structures in Numpy
* supports many complex operations





# info

the data-format used by lnrdb for "simple" fixed-length records (this includes all LN-topics) looks like this:

one records is always stored like this:

  <double logger-timestamp>
  <double publisher-timestamp>
  <...fields from ln message definition>

ieee doubles / floats are used.
there are no padding bytes!

the `meta` file describes the fields within a record, for LN-md `two_doubles`:

    double a
    double b

the `meta` files looks like this:

    name: "topics/topic1/2"
    endianess: "little"
    topic.id: 2
    topic.first_seen: 1657809096.441762
    topic.name: "topic1"
    topic.md: "lnrdb_example/two_doubles"
    topic.msg_def: "{\x27fields\x27: [[\x27double\x27, \x27a\x27, 1], [\x27double\x27, \x27b\x27, 1]], \x27resp_fields\x27: [], \x27defines\x27: {}}"
    topic.msg_def_hash: "d7be448ee8ec99cfe8e7e531d1d8047db368a61c3f67d45423d0779ac96c488b"
    topic.publisher: "publisher"
    topic.message_size: 16
    table-format: "fixed"
    field-description: [['timestamp', 'f8'], ['publisher_timestamp', 'f8'], ['a', 'f8'], ['b', 'f8']]
    fixed-table-record-size: 32
    field-subtables: []

the `field-description` tells us which fields to read per record.

