#!/bin/bash

if [ -z "$CISSY_SOURCE_CMD" ]; then
    DEFAULT_CISSY_SOURCE=/opt/rmc-build-tools/sourceme.sh
    echo "sourcing $DEFAULT_CISSY_SOURCE ..."
    source $DEFAULT_CISSY_SOURCE
fi

THIS=$(readlink -f ${BASH_SOURCE[0]})
THIS_DIR=$(dirname $THIS)

cd $THIS_DIR

#exec cissy run -k -cf conanfile.txt ln_manager -c lnrdb.lnc
exec cissy run -k

