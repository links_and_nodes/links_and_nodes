function data = read_topic1(fn)
% reads lnrdb data file from topics of md `lnrdb_example/two_doubles`

% there are always two timestamp-doubles per row!
% first is logger-timestamp,
% second is publisher-timestmap

fid = fopen(fn);
data = fread(fid, [2 + 2, Inf], 'double')';
fclose(fid);
