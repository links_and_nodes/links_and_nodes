%% first directly read all of first topic1 recording
data = read_topic1('logs/test_log/topics_topic1_2/data');
% this only works because read_topic1 knows the data format!
% -> lnrdb_example/two_doubles
whos data
l = length(data)
log_ts = data(:, 1);
pub_ts = data(:, 2);

disp(sprintf('first row [%f, %f]', data(1, 3), data(1, 4)));
disp(sprintf('last row [%f, %f]', data(l, 3), data(l, 4)));

%% directly read all of first topic2 recording
[log_ts, pub_ts, q, J, flag] = read_topic2('logs/test_log/topics_topic2_3/data');
% this only works because read_topic2 knows the data format!
% -> lnrdb_example/two_vectors
whos q J
middle = floor(length(q) / 2) + 1
disp(['middle q: ', sprintf('%f ', q(middle, :))]);

middle_J = reshape(J(middle, :, :), 6, 7)
middle_J_size = size(middle_J)

middle_flag = flag(middle)
