function [log_ts, pub_ts, q, J, flag] = read_topic2(fn)
% reads lnrdb data file from topics of md `lnrdb_example/two_vectors`

% there are always two timestamp-doubles per row!
% first is logger-timestamp,
% second is publisher-timestmap

% first read & interpret all double values
fid = fopen(fn);
n_doubles = 2 + 7 + 42 % 51
data = fread(fid, [n_doubles, Inf], '51*double', 1)'; % skip the flag byte at the end of each row
fclose(fid);

[l, cols] = size(data);

log_ts = data(:, 1);
pub_ts = data(:, 2);

data = data(:, 3:cols);
[l_, cols] = size(data);

q = data(:, 1:7);

J = data(:, 1+7:cols);
% restore matrices, and transpose last two dims
J = reshape(J, length(J), 7, 6);
J = permute(J, [1, 3, 2]);

% now read uint8-flag
fid = fopen(fn);
fseek(fid, n_doubles * 8, 'bof'); % skip first n_doubles in first row
flag = fread(fid, [Inf], 'uint8', n_doubles * 8)'; % after each flag skip n_doubles of next row
fclose(fid);

