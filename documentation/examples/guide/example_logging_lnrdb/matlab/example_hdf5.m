%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% first topic
h5disp('test_log.hdf5', '/logs/test_log/topics/topic0/1')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% second topic

h5disp('test_log.hdf5', '/logs/test_log/topics/topic1/2')
data2 = h5read('test_log.hdf5', '/logs/test_log/topics/topic1/2',1,1)

timestamp_start = data2.timestamp(1)
publisher_timestamp_start = data2.publisher_timestamp(1)
a_start = data2.a(1)
b_start = data2.b(1)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% third topic

%% shows how to read time series data from a large file that
%% dnoes not fit into memory
%% key is to do the indexing in h5read so that matlab
%% does not load the whole data set into memory

h5disp('test_log.hdf5', '/logs/test_log/topics/topic2/3')

% read first record
data3_s = h5read('test_log.hdf5', '/logs/test_log/topics/topic2/3',1,1)

q_start = data3_s.q
J_start = data3_s.J

% read last record
data3_info = h5info('test_log.hdf5', '/logs/test_log/topics/topic2/3')

data3_len = data3_info.Dataspace.Size

data3_e = h5read('test_log.hdf5', '/logs/test_log/topics/topic2/3',data3_len,1)


q_end = data3_e.q
J_end = data3_e.J

% read record in the middle

% remember that umpy uses zero-based indices, while Matlab uses
% indices starting from one
mid_idx = floor(data3_len/2) + 1

data3_mid = h5read('test_log.hdf5', '/logs/test_log/topics/topic2/3',mid_idx,1)

q_mid = data3_mid.q

J_mid = reshape(data3_mid.J, [6,7])
flag_mid = data3_mid.flag






