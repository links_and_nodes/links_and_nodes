
from __future__ import print_function, division

import sys
import h5py
import numpy as np

list_of_datasets = []
def printshape(name):
    try:
        print(name, f[name].shape)
        list_of_datasets.append(name)
        # note that in LNRDB, the final part of the dataset name
        # (after the topic) is a process-global index that numbers the
        # order in which the topic was started to be
        # recorded. Multiple runs of a publisher generate multiple
        # datasets (and the order in which topic recording starta can
        # change if topics start in parallel!).
    except AttributeError:
        print(name, "has no shape")

def show_extract(f, dataset_name):
    dset = f[dataset_name]

    print("+" * 20, "\n\n\n")
    print("dataset %s" % dataset_name)
    print("   shape = %r" % dset.shape)

    if dset.shape[0] > 0:
        print("   initial time stamp = {:>10.3f}, publisher_timestamp = {:>10.3f}, "
              "timestamp delta = {:>5.3f}".format(dset[0, "timestamp"],
                                            dset[0, "publisher_timestamp"],
                                            dset[0, "publisher_timestamp"] - dset[0, "timestamp"]))
        print("   final time stamp   = {}, publisher_timestamp = {}, "
              "timestamp delta = {:>5.3f}".format(dset[-1, "timestamp"],
                                            dset[-1, "publisher_timestamp"],
                                            dset[-1, "publisher_timestamp"] - dset[-1, "timestamp"]))
        
        print("   first record:", dset[0])

        n_rec = dset.shape[0]
        middle_idx = n_rec // 2
        for idx in [0, middle_idx, -1]:
            for col in dset.dtype.names :                
                print("   {}({}) = {}".format(col, idx, dset[idx,col]))
                                    
        
with h5py.File(sys.argv[1], "r") as f: 
    f.visit(printshape)

    for ds in list_of_datasets:
        show_extract(f, ds)
    
