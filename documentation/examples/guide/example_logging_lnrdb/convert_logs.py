import sys
import lnrdb
import h5py


def convert_recording(f, recording_name):
    grp = f.create_group(recording_name)

    log = lnrdb.Db(recording_name)

    print("converting logged tables / topic instances:")

    for tn in log.tables.keys():
        vars_log = vars(log.tables[tn])
        # print(vars(log.tables[tn]))
        print("vars_log.keys() ", vars_log.keys())
        if "rows" in vars_log.keys():
            print("found rows")
        print(" - %r %d rows" % (tn, log.tables[tn].n_rows))

        table = log.tables[tn]
        print("     original topic name: %r" % table.meta["topic.name"])
        print("     original topic md: %r" % table.meta["topic.md"])
        print("     numpy dtype: %r" % (table.dtype,))

        if table.n_rows > 0:

            data_mmap = table.mmap()  # this numpy mmap object handles mostly like
            #                           any other numpy recarray!
            dset = grp.create_dataset(tn, data=data_mmap, dtype=table.dtype)
        else:
            dset = grp.create_dataset(tn, (0,), dtype=table.dtype)

        # keep all meta information
        for meta_name, meta_value in table.meta.items():
            dset.attrs[meta_name] = meta_value


if len(sys.argv) < 3:
    print(
        """Usage: convert_logs <hdf5_filename> <recording1> [<recording2> ...]
    converts one or more lnrecorder recordings in lnrdb format into one HDF5 file,
    conserving meta data"""
    )

print("converting lnrecorder logs (using h5py from %s)" % h5py.__file__)
print("converting lnrecorder logs (using lnrdb from %s)" % lnrdb.__file__)


with h5py.File(sys.argv[1], "w") as f:
    for recording_name in sys.argv[2:]:
        convert_recording(f, recording_name)

print("conversion finished.")
