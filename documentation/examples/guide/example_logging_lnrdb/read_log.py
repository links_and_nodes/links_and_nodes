import lnrdb

log = lnrdb.Db("logs/test_log")

print("logged tables / topic instances:")
for tn in log.tables.keys():
    print(" - %r %d rows" % (tn, log.tables[tn].n_rows))

print("first & last row::")
for tn in log.tables.keys():
    print(" - %r" % tn)
    table = log.tables[tn]
    print("     original topic name: %r" % table.meta["topic.name"])
    print("     original topic md: %r" % table.meta["topic.md"])
    print("     numpy dtype: %r" % (table.dtype, ))
    if table.n_rows > 0:
        
        data_mmap = table.mmap() # this numpy mmap object handles mostly like any other numpy recarray!
        
        print("     first row: %r" % (data_mmap[0], ))
        print("     last row: %r" % (data_mmap[-1], ))
        if table.meta["topic.md"] == "lnrdb_example/two_vectors":
            i = int(table.n_rows / 2)
            last = data_mmap[i]
            print("     middle idx: %r" % (i, ))
            print("     middle q: %r" % (last.q, ))
            N = last.q.shape[0]
            print("     middle J: %r" % (last.J.reshape(-1, N), ))
            print("     middle flag: %r" % (last.flag, ))
            
