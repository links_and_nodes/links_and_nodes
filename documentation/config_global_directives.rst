global directives
=================
these directives can be used anywhere in your config file. they are not affected by the active section.

.. _include:

include
-------
syntax::
  
   INCLUDE_DIRECTIVE := "include" + FILENAME + "\n"

example:
  
.. code-block:: lnc
		
   include my_subsystem.inc.lnc

this directive can be used to read ln-config-directives from another file.
it is useful to split up larger configs across multiple files. also parts of your config might be provided by
some other project that provides such an include file.

the included file will be read exactly the same way the main config file is read, with the only exception that
names-prefixes :ref:`pushed<push_name_prefix>` in an include file will automatically be pop'ed at the end of
that file.

optional_include
----------------
syntax::
  
   OPT_INCLUDE_DIRECTIVE := "optional_include" + FILENAME + "\n"

example:
  
.. code-block:: lnc
		
   optional_include my_subsystem_for_%(host).inc.lnc

same as the above :ref:`include` directive, but it's not considered as an error if the specified file does
not exist.

this is useful to conditionally include many directives only if some condition is matched - this can be
achieved by using ``%()``-defines in that ``FILENAME``.

include_glob & optional_include_glob
------------------------------------
syntax::
  
   INCLUDE_GLOB_DIRECTIVE := "include_glob" + PATTERN + "\n"
   OPT_INCLUDE_GLOB_DIRECTIVE := "optional_include_glob" + PATTERN + "\n"

example:
  
.. code-block:: lnc
		
   include_glob cissygen/**/*.inc.lnc

this directive can search for multiple include files and include them in the same way as the above
:ref:`include` directive would do it. this is useful if you want to include many, possibly generated files
where you don't want to explicitely spell the exact-filename.

patterns are matches with python's ``fnmatch.fnmatch()``-function with the addition that ``**`` can match any
sub-directory.

pipe_include
----------------
syntax::
  
   PIPE_INCLUDE_DIRECTIVE := "pipe_include" + COMMANDLINE + "\n"

example:
  
.. code-block:: lnc
		
   pipe_include python generate_ln_config.py

this directive will execute the specified ``COMMANDLINE`` while the current config file is parsed. the
standard-output from that command will be interpreted as if it would be contents of an include file, in
exactly the same way as above with the :ref:`include`-directive.

this can be used to procedurally generate processes, defines or any other config-directives.

.. _push_name_prefix:

push_name_prefix
----------------
syntax:
  
.. code-block:: lnc
		
   push_name_prefix: <PREFIX>

all objects (processes, groups, states) which are defined after this directive will get their name prefixed
with ``<PREFIX>/`` (note the additional ``/``).

example:
  
.. code-block:: lnc

   # ...
   push_name_prefix: MyPrefix

   process proc1
   # ...
   
   process proc2
   # ...
   
   push_name_prefix: SubSection

   process proc3
   # ...
   
   pop_name_prefix

   process proc4
   # ...

will create 4 processes with these names:

.. code-block:: lnc

   MyPrefix/proc1
   MyPrefix/proc2
   MyPrefix/SubSection/proc3
   MyPrefix/proc4

this is mainly useful when having the instance-flag :ref:`instance-enable_auto_groups` set to ``true`` (see
its description for more information).


pop_name_prefix
---------------

syntax:
  
.. code-block:: lnc
		
   pop_name_prefix

will remove the last :ref:`pushed<push_name_prefix>` name prefix.

warning
-------

this directive can be used to output warnings to ln-managers log while reading the configuration. this can be
useful for debugging: e.g. to print out the value of a define at a certain point within the configuration:

syntax::
  
   WARNING_DIRECTIVE := "warning: " + MESSAGE + "\n"
   MESSAGE := EVALUATED_STRING

example:
  
.. code-block:: lnc

   defines
   SOME_SETTING: value42

   warning: value of SOME_SETTING: %(SOME_SETTING)

the ``MESSAGE`` will be prefixed with the current configration file's name and the line-number within that
file.

example log-output could look like this::

 2021-11-16 10:54:32.97 warning SystemConfiguratio path/to/my/config.lnc:10: value of SOME_SETTING: value42


   
 

.. _global/add_message_definition_dir:

add_message_definition_dir
--------------------------

TODO: explain add_message_definition_dir configuration setting




 
