********
Overview
********

We start with an quick overview on the tutorial.

.. _tutorial/prerequisites/installation:

Prerequisites
=============

This tutorial assumes the following items to be present:

* You have an installed and working system. If this is not the case,
  the :doc:`getting_started` chapter explains how to set it up.
* You can manage the requires packages and are able to use the
  available tools to install packages and adjust their environment.

.. only:: rmc
	  
	 .. note:: If you need to familiarize yourself with Conan and Cissy,
            `have a look into the Cissy documentation which explains
            all the details. <https://rmc-github.robotic.dlr.de/pages/tools/cissy/master/>`_.
	    
  .. attention:: If you are unsure how to use these systems,
		 you should definitively spend time to familiarize
		 yourself with them - it is not possible to cover
		 them in this tutorial.
	  
* For the client code examples, it is needed that you know at least
  roughly how to program in Python. If this is not yet the case, you
  might have a look into `the Python tutorial which is part of the
  official Python documentation
  <https://docs.python.org/3/tutorial/index.html>`_ . Alternatively,
  we will give examples in C++, using the GCC compiler and later
  the GDB debugger [#guide-on-debugging]_.

* It is helpful if you had already a look at the :doc:`quickstart`
  chapter - but this is explicitly not required. We will return to
  some code examples and explain them step by step in more detail.


.. _tutorial/prerequisites/language_standards:  

Language Standards we use and which are supported by LN
=======================================================

.. index::
   single: supported language standards and versions
   pair: python; supported versions
   pair: C++; supported versions
   single: compatibility; with C++ / Python versions
   single: C++98
   single: C++11
   single: Python 2
   single: Python 3

Standards used in this documentation
------------------------------------

In this tutorial, and most of the documentation, we will use the
Python 3 and the C++11 language standards for example.  We will
occasionally some newer elements in example code if this results in
clearly better code. Python 3 is used because Python 2 has reached its
end-of-life and will be supported less and less in the near
future. Python 2 code is not forward-compatible with Python 3, so
writing new code in Python 2 would probably not be useful for you.


Python 3 is fully supported from links and nodes version
2.0.1. Regarding cross-compatibility, Python 2 is also still supported
and using old Python 2 clients will work with newer LN managers and
never clients.


For C++, we chose C++11 as the oldest useful C++ standard used in this
documentation. Existing features and include headers will continue to
work for C++98, but new features are likely to require at least a
C++11 compiler.  Code written in C++11 will in almost all cases
continue to run with newer language versions, and should be easy to
understand for users of all modern versions.


Standards supported by LN
-------------------------

.. index::
   single: support of language standards
   single: Python 2 support
   single: C++11 support
   single: C++14 support
   single: C language standard support


Links and Nodes version 2.0 and newer, which is documented here,
supports and will continue to support Python 3, C++98, C++11, C99,
C11, and newer C++ dialects such as C++14 and C++17.

The version documented here also supports Python 2.  For Python 2,
this is bounded by practical limits and does not imply any support for
third-party modules, as the Python 2 ecosystem is being scrapped.

Similarly, using some new features in LN require C++11 or newer.




Goals of this Tutorial
======================

The tutorial has the following goals:

* to introduce you to some essential concepts and terms in Links and Nodes

* to give an overview on some important parts of the system, without
  burdening you with too many details.

* to enable you to program your first simple client applications, and control them.

* to enable you to understand and use the :doc:`user_guide` and at
  least some pieces of the :doc:`reference` part of this
  documentation.
  

What this part will **not** try is to give you every detail on every
aspect of the system, because this would be overwhelming to a
beginner. If you are looking for more detailed information, especially
*how* some things are working or *how* some interfaces are called and
defined in detail, you will probably find in either the (not yet
complete) :doc:`user_guide` or the :doc:`reference` parts what you are
looking for (if you are just stumbling on an unknown term, you can
also try the :doc:`glossary` for help).
  

Overview / Road Map on the tutorial
===================================

We will proceed as follows:

* In the part :doc:`tutorial_concepts`, we will quickly reiterate what LN
  is useful and intended for, and how it achieves these goals. This is
  important, because otherwise, you could end up tragically (and
  frustratingly) trying to solve a difficult problem with an excellent
  but wrongly chosen tool.

* In :doc:`tutorial_lnm`, we will introduce the :term:`Links and Nodes
  Manager (LNM) <LN Manager>` and its configuration file. For a start,
  we will show you the simplest possible configuration, and go on from
  there.

* In the same chapter, we will introduce you also to the :term:`LNM GUI`. This
  will allow you to start, stop, and otherwise control processes, and
  also to look what they are currently doing.

* We will explain the most important :term:`communication` facilities, and how
  they essentially work.

* As practical examples, in the parts :doc:`tutorial_python` and
  :doc:`tutorial_cpp`, we will show how to run :term:`LN topics <LN topic>` and :term:`LN services <LN service>`
  client applications in Python and C++, and extend them a little bit
  in a few iterations. We will also show how you can work with LN in
  the development cycle of such a software :term:`module`.

  
.. rubric:: Footnotes:

.. [#guide-on-debugging] The :doc:`user_guide` part includes a
                         tutorial on how to use the GDB debugger to
                         debug programs, including client programs
                         running under Links and Nodes.
	    
