######################
Fixing Common Problems
######################

LN Manager sees processes from another instance
===============================================

* instance name must be set to a unique name

  
LN Manager cannot start processes on other hosts
================================================

* secure-shell setup needs to allow for password-less logins

LN Manager cannot connect to daemon, error message about wrong DSA keys for daemon
==================================================================================

* might need to delete and re-generate keys?

Cannot import links_and_nodes python module
===========================================

* is the PYTHONPATH correctly set?
* are you running the right Python version?
* possibly you need to use the excevpe process flag? (explain when to use it)

LN Manager cannot start process
===============================

* possibly path name wrong
* might use relative path but the CWD is not set

Topic names with trailing slashes
---------------------------------

* work sometimes (in python, but not always)


  
Conan environment for CPP Builds
----------------------------------

One needs to define the environment as follows::
  
  conan install links_and_nodes_manager/[~2]@common/stable -if conan -g virtualenv -g virtualenv_python -g virtualrunenv -g virtualbuildenv

Here, you should replace
``links_and_nodes_manager/[~2]@common/stable`` by the current LN
version, which is |DOCUMENTED_LN_MANAGER_VERSION|. 


  
