Avaliable Functions
-------------------

``guess_display``
     this function tries to guess a valid X-display-string suitable for
     a `DISPLAY`-environment variable. 
	 
	 **Consider** using the :ref:`process/forward_x11` process flag instead - its
	 designed to be easier and more robust!
	 	 
     The guess is based on the current value of the managers ``DISPLAY`` environment variable. It has no arguments.

     When the process is to be started on the same host as the manager
     run's on, ``%(guess_display)`` will equal to ``%(env DISPLAY)``.
     when the process is to be started on a remote host, and the
     managers local ``%(env DISPLAY)`` contains no host-part the manager-ip
     address (as seen from the remote host) is prepended. if there is a
     host-part the ``DISPLAY`` environment variable it is passed unmodified.
     example usage::

         process pyXpferd
         node: guihost
         environment: PYTHONPATH=%(LN_PYTHONPATH), DISPLAY=%(guess_display)
         pass_environment: USER, HOME, XAUTH
         command: /usr/bin/python %(ln_base)/library/tests/python/slow_subscribe.py 500

``ifeq``
     This function checks for equality (python ``==``) of the two parameters
     specified first. In case they are equal, the third parameter is returned,
     else th fourth one.
	 
     Example::

       %(ifeq "3.0", "3", "equal", "not equal")

     Note that this will get replaced by ``not equal``, as a string-comparison is done on the parameters.
