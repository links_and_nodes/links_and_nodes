#############################################
Environment Variables which affect LN clients
#############################################


.. envvar:: LN_MANAGER

TCP/IP address of a listening ln-manager for the client to connect to. usually this will be automatically provided by
the ln-manager in the :ref:`default environment<process-environment>` for each process.
it is in the form of ``<HOST>:<PORT>`` where ``<HOST>`` can either be an IPv4-address in
`dotted-decimal notation <https://en.wikipedia.org/wiki/Dot-decimal_notation>`_ or a domain-name that can be resolved by the local name resolver. ``PORT`` is the numeric
listening TCP port of the manager to connect to.

this env-var is only used when a client is constructed via :c:func:`ln_init()` or :cpp:class:`ln::client()` and no
manager address was explicitly provided either via function-arguments or the ``--ln-manager`` command-line parameter. (or when ``NULL`` is
passed for the ``ln_manager``-argument to :c:func:`ln_init_to_manager()`).

.. envvar:: LN_DEBUG

when set to ``1`` ln-clients will output internal debug messages to stdout.

.. envvar:: LN_DEBUG_TO_FTRACE

when set to ``1`` ln-clients will output internal debug messages to ``/sys/kernel/debug/tracing/trace_marker``
independent of the setting of :envvar:`LN_DEBUG`.


.. envvar:: LN_FTRACE

when set to ``1`` ln-clients will output messages to ``/sys/kernel/debug/tracing/trace_marker`` before and after
writing/reading to/from a topic shared memory.
