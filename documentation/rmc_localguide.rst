############################################
Local Guide for using Links and Nodes at RMC
############################################


Installation
============

* for instructions on how to use Cissy, see `the current Cissy documentation <https://rmc-github.robotic.dlr.de/pages/tools/cissy/master/>`_.

  

.. index::
   pair: LN services; Cissy environment
   pair: LN Manager; Cissy workspace
	  

Running the Quickstart/services example in a Cissy environment
==============================================================

All examples work by using Conan. Cissy can provide packages for more
complex set-ups and processes with involved dependencies, but it is
not required for the cases described here.

Just as an example, if we use Cissy at RMC, we can also start the ln_manager like this:

.. code:: bash

	  cissy run -k -p links_and_nodes_manager/[~=2]@common/stable ln_manager -c sample_services.lnc

Here, you should replace
``links_and_nodes_manager/[~2]@common/stable`` by the current LN
version, which is |DOCUMENTED_LN_MANAGER_VERSION|. 

	  
Alternatively, we can use use a ``cissy_workspace.yaml`` file like the one in
:file:`examples/quickstart/python/services/cissy_workspace.yaml` which looks like that:

.. literalinclude:: examples/quickstart/python/services/cissy_workspace.yaml
   :language: yaml
   :linenos:

This file defines the exact version of the ``links_and_nodes`` package
that we are going to use, and sets the correct python
environment. To start the sample with it, simply run:

.. code:: bash
	  
	  cissy run

in the shell.

``mobilnetz`` setup
===================

the institute has an isolated network named ``mobilnetz`` which uses IP network
``192.168.128.0/20`` (see `Mobilnetz Page on RMC-Wiki
<https://wiki.robotic.dlr.de/Mobilnetz>`_).

there is no IP-routing between this network and the rest of the institutes
network (``rmc-network``).
so a typical RMC-workstation/PC can not directly communicate with a host in
``mobilnetz``. (with the exception of a few workstations that have an additional
network card & ip-address within the ``mobilnetz``).

hosts which are only connected to ``mobilnetz`` can not mount RMC-network-drives.

there is a host named ``rmc-mobilproxy`` which has a connection to the
``rmc-network`` (``ens3: 129.247.189.41``) and ``mobilnetz`` (``ens10:
192.168.128.1``).

you can login to ``rmc-mobilproxy`` via ssh with your DLR username & password (or
kerberos authentication).

on ``rmc-mobilproxy`` there are all RMC-network-drives mounted at the
typical locations (your ``/home/$USER``, ``/opt``, ``/volume/conan_cache``...).

this can be used for file-transfers between ``mobilnetz``-hosts and
RMC-network-drives or RMC-network-hosts. (e.g. use ``rsync`` to copy files between
``rmc-mobilproxy`` and any host in ``mobilnetz``).

``rmc-mobilproxy`` is not using our standard RMC-workstation linux distribution
(currently ``osl15.4``) but instead some other distribution (currently
``AlmaLinux 8.8``).
this is currently not a supported ``cissy``-platform.

a common lab scenario is that you want to run your ln_manager on some
RMC-workstation and have it start- and communicate with- processes on hosts in
``mobilnetz``.

for this to work the ln_manager needs to know about the non-routed networks, and
which host can be reached from which network. with that information it can
decide to instruct ``ln_daemon``-processes on relevant hosts to forward data
between participants from different networks.

example
-------
in this example we want to run our ln_manager on ``rmc-lx0142`` which has no
direct connection to ``mobilnetz``. we want to use it to start a process
on ``miiwa2-server`` which is a host mounted inside a mobile robot and only
connected to ``mobilnetz``.

ln-config file:

.. code-block:: lnc

  instance
  name: mobilnetz test for %(env USER)@%(hostname)
  manager: :%(get_port_from_string %(instance_name))
  enable_auto_groups: true

  hosts
  rmc-mobilproxy: ens10:192.168.128.1, ens3:129.247.189.41, daemon_start_script: %(env HOME)/.ln_daemon/start
  miiwa2-server: en0:192.168.128.214, daemon_public_key_file: none, expected_user: l_mobil, additional_ssh_arguments: -i %(CURDIR)/miiwa2_l_mobil

  network rmc-network
  member_interfaces: 129.247.189.0/24, 10.97.0.0/16, 192.168.8.0/24

  network mobilnetz
  member_interfaces: 192.168.128.0/23, 192.168.136.0/21

  node_map
  robot_server: miiwa2-server


  push_name_prefix: all

  process test process
  command: /usr/bin/date
  flags: no_error_on_successful_stop
  node: robot_server

now the ln_manager will automatically detect that ``rmc-lx0142`` can not
directly communicate with ``miiwa-server2`` and will use an ln_daemon on
``rmc-mobilproxy`` for forwarding.

.. note::
   this exact same config can be used on an RMC-workstation that HAS a 2nd
   interface directly into ``mobilnetz`` (for example
   ``rmc-lx0168``). ln_manager will detect this at startup and will communicate
   directly without the need of ``rmc-mobilproxy``.

   it is always desirable to create ln-config files which make as little
   assumptions as possible of the users environment (here: the host the user
   decides to start the ln_manager on).
   for "real robots" this might not seem too important, as often the same
   "console workstation" is used all the time.


prerequisites:

1. for the ln_manager to be able to start an ln_daemon on ``rmc-mobilproxy``
   your ssh-client needs to be configured in a way that allows a
   password-less login.

   as this host supports the kerberos authentication, your default ssh-config
   on RMC-workstations should be fine.

   on your manager-workstation (``rmc-lx0142`` in this example) you can try
   to run::

     ssh rmc-mobilproxy.robotic.dlr.de date

   if this works and prints a date & time your ssh config should be fine.
   if not, check your ``~/.ssh/config``.

2. to be able to start the ``ln_daemon`` the manager also needs to know a
   command line to start a daemon that works on this AlmaLinux 8.8. we
   provided this information via ``daemon_start_script: %(env
   HOME)/.ln_daemon/start``, see :ref:`below <ln_daemon_on_mobilproxy>` for explanation.

3. as above, for the ln_manager to be able to start an ln_daemon on ``miiwa2-server``
   your ssh-client needs to be configured in a way that allows a
   password-less login.

   as this host can not authenticate via kerberos (it can not communicate with
   DLR-AD), we fallback to using private/public-key pairs.

   with ``additional_ssh_arguments: -i %(CURDIR)/miiwa2_l_mobil`` we tell
   the ln-manager to instruct the ssh-client to use the private key from
   ``miiwa2_l_mobil`` in the same directory as the config file.
   the corresponding public key of that needs to be listed in the file
   ``/home/l_mobil/.ssh/authenticated_keys`` on ``miiwa2-server``.

   you can create a new key-pair with this command::

     ssh-keygen -t rsa -f miiwa2_l_mobil

   this will generate two files: one with the public key ``miiwa2_l_mobil.pub`` and
   another one ``miiwa2_l_mobil`` with the private key.
   on ``miiwa2-server`` you can append your new public key to
   ``authenticated_keys`` via::

     cat miiwa2_l_mobil.pub >> /home/l_mobil/.ssh/authenticated_keys

4. as we did not specify a ``daemon_start_script`` for ``miiwa2-server``, the
   ln_manager will default to use this command-line to try to start an
   ln_daemon (or similar)::

     /opt/rmc-build-tools/cissy run -p links_and_nodes_runtime/2.3.0@common/stable ln_daemon

   this will only work if cissy is installed at that location and you did run
   ``cissy setup`` successfully there once.


.. _ln_daemon_on_mobilproxy:

``ln_daemon`` on ``rmc-mobilproxy``
-----------------------------------

as explained above, that host does not use one of the currently supported
cissy-platforms.
because of this the ln_managers default daemon-start-command of::

  /opt/rmc-build-tools/cissy run -p links_and_nodes_runtime/2.3.0@common/stable ln_daemon

can not work there.

so we need to use the ``daemon_start_script:`` option on the hosts-line for
``rmc-mobilproxy``.
we can use any command-line that starts a compatible ``ln_daemon`` -- a
quick-and-dirty hack is to use::

  daemon_start_script: /volume/software/common/packages/links_and_nodes/latest/bin/osl153-x86_64/ln_daemon -d

this is known to work but has these problems:

1. we are starting a daemon from december 2021. so it will not include any
   bugfixes or new features since then. (that old daemon also requires the
   ``-d``-option to "daemonize" into background)

2. that daemon was compiled for osl153-x86_64 which isn't necessarily 100%
   compatible with that AlmaLinux running on ``rmc-mobilproxy``.

   but it still works, because it is x86_64 and it doesn't use any
   non-standard / distribution-specific libraries (only basic standard stuff that
   is unlikely to get incompatible soon).


instead we suggest to use an up-to-date version of ln_daemon from your
conan-cache.

but as already explained, we can not use cissy / conan to build a perfectly
matching ln_daemon for that AlmaLinux as that is not a configured/known
cissy-platform.

so we can avoid the first problem, but not the second.

to get a modern ln_daemon in your conan-cache you can use::

  conan install links_and_nodes_runtime/[~2]@common/stable

on any RMC workstation which happens to be compatible enough to
``rmc-mobilproxy``. but those modern ln_daemons want to load the ``libln.so``
dynamically at runtime -- so they require a properly setup ``LD_LIBRARY_PATH``.

we could now write a shell-script by hand that sets a proper ``LD_LIBRARY_PATH``
pointing somewhere in your conan-cache
(``/volume/conan_cache/$USER/.conan/data/liblinks_and_nodes/.../lib``)
and then start an ln_daemon from there
(``/volume/conan_cache/$USER/.conan/data/links_and_nodes_runtime/.../bin/ln_daemon``) and
place it in a well-known location somewhere on the network-drives such that it
can be used by the ln_manager to start an ln_daemon via ssh on
``rmc-mobilproxy``.

that is a very valid approach, but those path names are very long and tedious to
write. also in future ln_daemon might need more dynamic libraries which then
also need to be added to the ``LD_LIBRARY_PATH``.

instead it is better to let conan generate the proper environment for us.
this is done via conan-generators: they write scripts that contain all
necessary environment variables -- we only need to place them at a good location
and "source" them before we try to start the ln_daemon.

since version 2.3.0 of ln_manager there is a convenience script provided that
does exactly the above stuff. you can call it like this from any valid
cissy-supported RMC-workstation that has access to your $HOME-directory on the
network-drive (but we suggest an ``osl15.4``-host)::

  cissy run -k -p "links_and_nodes_manager/[~=2.3.0]@common/stable" export_ln_daemon

this will install an ln_daemon in your conan-cache and place the
conan-generator-output-files in your home directory at::

  $HOME/.ln_daemon/$DLRRM_HOST_PLATFORM/*

and also create a start-script called ``start`` which will "source" that
environment and then start the ln_daemon.
there is also a symlink to that start-script placed in
``$HOME/.ln_daemon/start`` -- and that is what we want to use in our hosts-line
for ``rmc-mobilproxy``::

  daemon_start_script: %(env HOME)/.ln_daemon/start

this start-script can now be called on any host whose platform is compatible
enough to whatever RMC-workstation you used.

tested, known to be working cissy-platforms for rmc-mobilproxy are
``osl154-x86_64`` and ``osl153-x86_64`` as of 2023-11-08.

check your RMC-workstation cissy platform with ``echo $DLRRM_HOST_PLATFORM``.


rationale
---------

* there are no DLR user-accounts on ``mobilnetz``-hosts like ``miiwa2-server``
  (only local users like ``l_mobil``).

  we need to tell the ln_manager that it should accept an ln_daemon for user
  ``l_mobil`` instead of your ``$USER``. this is done via the ``expected_user:
  l_mobil`` option. see :ref:`hosts/expected_user` for more information.

* as there are only local users, there is also no user-private or otherwise
  sensitive data on those ``mobilnetz``-hosts.

  the passwords to those local accounts are also typically known by many people,
  so those accounts can not be considered as "secure".

  because of that we do not require a locked ln_daemon on those hosts -- they
  don't need to authenticate a connecting ln_manager.

  this is configured via ``daemon_public_key_file: none``. see
  :ref:`hosts/daemon_public_key_file`.

  this allows different RMC-users to share the same ln_daemon (there will be no
  conflicts, as long as different users also use different :ref:`instance names
  <instance/name>`)

* as mentioned above, the real ip-network for ``mobilnetz`` is
  ``192.168.128.0/20``, with a 20-bit netmask (``255.255.240.0``).
  here instead we used ``member_interfaces: 192.168.128.0/23, 192.168.136.0/21``
  because the ``/20`` network would also include ip-addresses ``192.168.132.*``, ``192.168.131.*`` and
  ``192.168.130.*`` which are often already used for local interfaces connecting
  cameras, scopes or similar equipment, often on a desk in an office.

  as those are not part of ``mobilnetz`` we don't want the ln_manager to match those.
  (this does not pose a problem for the ip-routing, as there is no ip-routing
  between those local additional interfaces and the ``mobilnetz``...)

  you can always explicitly add host-interfaces that use these
  ip-addresses when needed. example:

  .. code-block:: lnc

    hosts
    ...
    rmc-ap0107: en0:192.168.131.2

    network mobilnetz
    member_interfaces: 192.168.128.0/23, 192.168.136.0/21, rmc-ap0107/en0
