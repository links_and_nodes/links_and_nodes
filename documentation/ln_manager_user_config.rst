LN Manager user config
======================

a user can customize some aspects of the LN-Manager in a private config file
located in the users home directory: ``~/.ln_config`` (a different file name can
be used by setting the ``LN_CONFIG``-env-var before starting the LNM).

this config file is user-specific and independent of any LNM-instances. it uses
the same syntax as a python module and can be used to override / extend any of
the default configuration from ``python/links_and_nodes_manager/config.py`` --
read that file to see which settings you might want to change.

the following shows some examples.


GUI: alternate color scheme
---------------------------

default LNM-GUI uses red, green, yellow and orange to visualize the current
state of processes / states. by pressing the keyboard combination "Alt + c"
within the LNM-GUI you can toggle to an alternate color scheme.

both color schemes (default & alternate) can be change within your ``~/.ln_config`` file:

.. sourcecode:: python

    default_color_scheme = dict(
        inactive="#7f7f7f", # gray
        starting="yellow",
        ready="#00ff00",
        warning="#ff7f00", # orange
        stopped_or_error="red",
        unknown="#ffffff",
    )
    alternate_color_scheme = dict(
        inactive="#7f7f7f", # gray
        starting="yellow",
        ready="#00ff00",
        warning="#ff7f00", # orange
        stopped_or_error="red",
        unknown="#ffffff",
    )
    start_with_alternate_color_scheme = True

a quick way to test/find these color-values is to start the LNM with one of your
ln-configs and use the "manager notebook" to test color values at runtime:

* start the LN-Manager GUI

* select the "topics"-tab on the top, click the "manager notebook"-button

* within the notebook create a new cell with this content

  .. sourcecode:: python

      from links_and_nodes_manager import config
      config.alternate_color_scheme = dict(
          inactive="#7f7f7f", # gray
          starting="light blue",
          ready="blue",
          warning="brown",
          stopped_or_error="fuchsia",
          unknown="#d3d3d3", # light-gray
      )
      # redraw gui
      gui.pg.on_changed_color_scheme(gui, config.alternate_color_scheme)

* adjust the color values and press "Ctrl + Enter" to execute that cell. in the
  LNM-GUI switch back to the "processes"-tab again and observe the new colors.


file-system location of dynamically generated message definitions
-----------------------------------------------------------------

an LN-client can register new/dynamically generated message-definitions with the
LNM at runtime (see :c:func:`ln_put_message_definition()`). the default is to place those
dynamically generated message definitions in
``~/ln_message_definitions/gen`` (which usually requires the existence of the
``HOME``-env-var). if you want to keep keep them in a different location you can
do that in your ``~/.ln_config`` file:

.. sourcecode:: python

    default_gen_msg_defs = "/tmp/lnm_gen_md"
