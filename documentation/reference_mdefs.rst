###################
Message Definitions
###################


.. index:
   pair: message definitions; search path
   pair: message definitions; details on look-up


This parts explains some details on the look-up
of message definitions. For an introduction on
the involved concepts, please see the section
:ref:`tutorial/message_definitions/names` in the tutorial.

.. _reference/message_definitions/lookup:

How message definitions are looked up
=====================================

ln-generate expects names of needed message definitions as command
line parameters.  These names are used to search for message
definitions in the file system. However, they are not absolute
path names of files, and by design could be stored differently,
similar as Java classes can be stored in a JAR archive.

Importantly, message definitions are never looked up in the
current working directory of an LNM config file. One should
also never use the "parent directory" shorthand ".." to
refer to message definitions.

The lookup for message-definition is always done like this:

1.  given a message definition name (example my_project/some_type),
    iterate across the list of known md-directories and form a filename md-dir+ "/" + md-name.
    If that file exists, form a message-definition from its contents.

2. the list of known md-directories is assembled in a deterministic way:

    a. first any md-dirs added via add_message_definition_dir:
       directive (in order shown in config file) or the --md-dir
       option given to ln_generate
       
    b. then some built-in md-dirs from ln's install
       $PREFIX/share/links_and_nodes/message_definitions (or when run
       from source tree: SRC/share/message_definitions (this might not
       result in useful dir-names when lnm or python was run from
       within a conan env) and from
       $PREFIX/share/ln_runtime/message_definitions
       
    c. then add ~/ln_message_definitions for user-private md's (bad
       idea) contents of the $LN_MESSAGE_DEFINITION_DIRS env var

After assembling this initial list, the LN tools (LN Manager and
ln_generate) iterate it and check in each md-dir whether it contains a
file named md.conf: If so, additional md-directories are read from it
(one per line) and append it to the list of known md-dirs.

Both :program:`ln_generate` and :program`ln_manager` use this look-up.
The mechanism to pass additional directories is sligthly different:

:program:`ln_generate` uses the option ``--md-dir``, while
the :program:`ln_manager` uses the global directive ``add_message_definition_dir``.
