.. _defines:

``defines`` sections
====================

there can be multiple defines-sections within an ln-config file.

syntax::
  
   DEFINES_SECTION := "defines\n" + DEFINE_DEF*
   DEFINE_DEF := DEFINE_NAME + ": " + DEFINE_VALUE + "\n" + "\n"*

   DEFINE_NAME := EVALUATED_STRING
   DEFINE_VALUE := EVALUATED_STRING | ( "[" + EVALUATED_MULTILINE_STRING + "]" )

example:

.. code-block:: lnc

   defines
   my_name: flo
   
   %(my_name)_name: Florian
   other_name: Other
   
   message: Hello %(flo_name)
   message2: Hello %(%(my_name)_name)2
   
   my_name: other
   message3: Hello %(%(my_name)_name)3

resulting defines after this example block::

 my_name: other
 flo_name: Florian
 other_name: Other
 message: Hello Florian
 message2: Hello Florian2
 message3: Hello Other3

as can be seen above with ``my_name``: defines can be overwritten and can have different values at different
positions within a config file.
