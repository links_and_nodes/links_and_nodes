builtin-templates
=================

ln_manager provides some built-in process templaes which can be used in
config files:

.. _internal notebook template:

internal notebook template
--------------------------

this can be used to start a python notebook like its done when pressing
one of the "open notebook"-button on the ln_manager-UI (e.g. for
processes, within the topic inspector, or when double clicking a service
name).

it can be used like this:

.. code-block:: lnc

   process my notebook
   use_template: internal notebook template("%(CURDIR)/my_notebook.nb.py", "some title")

this will start a notebook-UI on the same host as your ln-manager-UI is
currently running on (usually ``localhost``, where the manager is
running).


see also instance-:ref:`instance/notebook_process_template`.

.. _internal scope template:

internal scope template
-----------------------

this will start a scope-UI subscribing to a specified topic and plotting a
specified field versus time.

it can be used like this:

.. code-block:: lnc

   process my topic1 scope
   use_template: internal scope template("topic1", "field1")

this will start a scope-UI on the same host as your ln-manager-UI is
currently running on (usually ``localhost``, where the manager is
running).
it will subscribe to topic ``topic1`` and plot values of ``field1``.

see also instance-:ref:`instance/scope_process_template`.

.. note::
   todo: scope's are not yet ported to Gtk3

