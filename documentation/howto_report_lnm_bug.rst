how to report a possible ln-manager bug
***************************************

.. index::
   single: report bug
   pair: report bug; LN manager


if you experience buggy behavior from the ln-manager, it is crucial for
efficient debugging to provide as much exact information as possible, from the
beginning on.

as manually collecting this information is tedious and error-prone, the
ln-manager GUI provides a `save all` function to create a compressed `tarball
<https://en.wikipedia.org/wiki/Tar_(computing)>`_ containing the ln-manager log,
your config files and a lot of internal state (like which processes are started
on which hosts, ...).

this tarball can then be attached to bug-reports.

so after you experience erroneous behavior from ln-manager, immediately click on
the button shown below:

.. image:: images/lnm_save_all_button.png

a new `save debug info` dialog will be shown, please enter a filename of the to
be created tarball, like `lnm-error-USER.tar.bz2`:

.. image:: images/lnm_save_all_dialog.png

and click on the `OK` button.

the operation might take a few minutes, depending on the size of your ln-manager
configuration.

please attach this file then to your bug-report,

* e.g. your mattermost post in `~sara users <https://mattermost.hzdr.de/rmc-rm/channels/sara>`_
  
* or in a `rmc-github issue <https://rmc-github.robotic.dlr.de/common/links_and_nodes/issues/new>`_

github annoyingly only allows files whose filename ends in ``.txt`` and ``.zip``
to be attached to issues. to work around this nonsense simply rename your file::

  mv lnm-error-USER.tar.bz2 lnm-error-USER.tar.bz2.zip

or::

  mv lnm-error-USER.tar.bz2 lnm-error-USER.zip

