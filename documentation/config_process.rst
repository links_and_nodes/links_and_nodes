``process`` objects
===================

A process-object within an ln config file is an exact description of how to start & stop a process:

* on which node/host to start the process on
* which environment to pass to the process
* a shell-commandline to start the process OR the name of the executable plus arguments to pass
* how to stop that process
* whether the process is expected to stay running or to terminate on its own
* which text-encoding the processes uses for its console output
* whether to log the output of that process to some log file
* does the process output some line to signal that its ready / finished starting up?
  OR does the process have a constant startup time?
* are there other processes that need to be started & finished before trying to start this process?
* are there other processes that need to be ready (and stay ready) before this process can be started?
* are there other processes that should be started as soon as this process is ready?

A process needs to have a unique name within the ln-config file.

syntax::

  PROCESS_DEFINITION := "process " + PROCESS_NAME + "\n" + PROC_SETTINGS
  PROC_SETTINGS := PROC_SETTING + [ PROC_SETTING ]*
  PROC_SETTING := SETTING_NAME + ": " + SETTING_VALUE + "\n"

  PROCESS_NAME := EVALUATED_STRING
  SETTING_NAME := STRING
  SETTING_VALUE := EVALUATED_STRING | ( "[" + EVALUATED_MULTILINE_STRING + "]" )

example:

.. code-block:: lnc

  process just show date & time
  node: localhost
  command: /bin/date
  add flags: no_error_on_successful_stop

this will add a new process named ``just show date & time`` to the ln-config. we decided we want to run this
process on node ``localhost`` which is always the same host the ln-manager is running on.
``/bin/date`` is the absolute pathname to an executable. in this case it will simply print one line of text
and then stop itself.

this process will be started with very few environment variables set. see the :ref:`environment setting<process-environment>`.

valid ``SETTING_NAME``'s are:

command
-------

type: *required, string, command line*

command to execute to start this process.

this is usually the path-name of an executable program:

.. code-block:: lnc

  process xlogo
  node: localhost
  flags: forward_x11
  command: /usr/bin/xlogo

in case you make a typo like here:

.. code-block:: lnc

  command: /usr/bin/Xlogo

you will see an error message like this::

  could not execve()
  /usr/bin/Xlogo
  errno 2: No such file or directory

arguments can also be specified:

.. code-block:: lnc

  command: /usr/bin/xlogo -bg blue

if you don't want to specify an absolute path-name, but instead want to instruct
your operating system kernel to search for that executable, you can use the
``use_execvpe``-flag AND set a value for the ``PATH``-environment-variable:

.. code-block:: lnc

  process xlogo
  node: localhost
  flags: forward_x11, use_exevpe
  pass_environment: PATH
  command: xlogo -bg blue

this way the ``PATH`` environment variable will be passed from the ln_manager's
environment to the environment of this xlogo-process.

.. note:: by using ``pass_environment: PATH`` it is assumed that the value of
	  the ``PATH`` variable is also useful on whatever (possibly remote)
	  node/computer this process is started on! for standard pathes like
	  ``/usr/bin:/usr/sbin:...`` this is usually correct.

sometimes it is not convenient to start a single executable with arguments, but
instead we want to provide a "command-line" which is interpreted by a shell
(like ``/bin/sh``, which is often a symlink to ``bash``).

shell-command-lines can do much more than just causing a single executable to be
started. they can be used to start multiple commands, one after the other:

.. code-block:: lnc

  process xlogo
  node: localhost
  flags: forward_x11, start_in_shell
  command: echo hello world; xlogo -bg blue

this ``;`` (semicolon) is one of the special characters that can be used in
shell-command-lines -- in this case first ``echo hello world`` is executed by
the shell, then ``xlogo -bg blue`` (search for ``compound commands`` in `man
bash <https://man7.org/linux/man-pages/man1/bash.1.html>`_).

note that here we don't need to specify the absolute path-name for ``xlogo`` as
the shell will always use an `execvpe
<https://linux.die.net/man/3/execvpe>`_-equivalent mechanism to find executable
programs.

note also that we don't have to provide a value for the
``PATH``-environment variable here: if not specified, the shell will look for
executables in a minimal set of standard locations like ``/usr/local/bin``, ``/usr/bin``...

for non-standard tools you should not rely on this implicitely defined list of
directories which are searched.
you should either explicitely specify an absolute path name of your executable,
or explicitely specify a search-path like shown in this example:

.. code-block:: lnc

  process xlogo
  node: localhost
  flags: forward_x11, start_in_shell
  add environment: PATH=/path/to/my-executables:/path/to/other/executables:...
  command: echo hello world; my-special-program

this would allow the shell to find your
``/path/to/my-executables/my-special-program`` executable. the shell will output
``/bin/sh: my-special-program: command not found`` if an executable or
internal-command can't be found.


node
----

type: *required, string*

node_name where this process shall be started on. this can also be a host_name. an
ip-address is a valid host_name.

.. _process-environment:

environment
-----------

type: *list of strings*

if nothing is specified, a process will be started in a nearly empty environment with only these variables set:

:envvar:`LN_MANAGER`
   you should not manually specify this value! ln-manager will compute this value
   and will also automatically create needed tcp-forwardings if manager and client are not within the same IP-network.

:envvar:`LN_PROGRAM_NAME`
   this will contain the unique ``PROCESS_NAME``. the ln-client library will use this value to suggest a
   unique client-name to the ln-manager name.
   ln-manager will only set this variable if it is not already explicitely specified in the
   process-definition.

:envvar:`LN_MESSAGE_DEFINITION_DIRS`
   this will contain all search pathes for message definitions that the ln-manager uses (this is only
   useful if manager and this process share a common filesystem layout).
   this environment variable is only used by the :ref:`guide/ln-generate_tool`. normal ln-clients do not read it.
   if already specified in the process-definition, ln-manager will append this value to the end where it
   should not do any harm.

``LD_LIBRARY_PATH``
   will always include the path to ``libln.so`` where ln-manager found it (this is only useful if manager and
   this process share a common filesystem layout).
   if already specified in the process-definition, ln-manager will append its libln-path to the end where it
   should not do any harm.

``TERM``
   ln-manager will set this to ``xterm`` to tell the started process which terminal control sequences to use
   for (*curses-like*) console UI's. (ln-manager uses VTE-widgets to display the process output, VTE supports
   most of xterm's control sequences)
   ln-manager will only set this variable if it is not already explicitely specified in the
   process-definition.

arbitrary additional variables can be specified in the process-definition. example:

.. code-block:: lnc

  process print env vars
  node: localhost
  command: /usr/bin/printenv
  add flags: no_error_on_successful_stop
  add environment: TERM=vt100
  add environment: LICENSE_PATH=/opt/expensive-license
  add environment: LN_PROGRAM_NAME=printenv

when this process is run, it should output something similar to this::

 TERM=vt100
 LICENSE_PATH=/opt/expensive-license
 LN_PROGRAM_NAME=printenv
 LD_LIBRARY_PATH=/path/to/ln_manager/libln
 LN_MESSAGE_DEFINITION_DIRS=/path/to/ln_msg_def:...
 LN_MANAGER=127.0.0.1:3069

in this case ``LN_PROGRAM_NAME`` and ``TERM`` was set explicitely in the process-definition so ln-manager did
not change their values.
``LICENSE_PATH`` is just an example of some arbitrary environment variable added to the process environment.

.. note::
   please notice that ``environment``'s type is *list of strings*, as described in the following:

you can set this list completely at once (multiple values separated by ``,``):

.. code-block:: lnc

  process print env vars
  node: localhost
  command: /usr/bin/printenv
  add flags: no_error_on_successful_stop
  environment: TERM=vt100, LICENSE_PATH=/opt/expensive-license, LN_PROGRAM_NAME=printenv

this can also be spread across multiple lines:

.. code-block:: lnc

  process print env vars
  #...
  environment: [
     TERM=vt100,
     LICENSE_PATH=/opt/expensive-license,
     LN_PROGRAM_NAME=printenv
  ]

(the trailing ``,`` is needed!)

be careful to not accidentally set the environment list multiple times! only the last setting will have an
effect:

.. code-block:: lnc

  process print env vars
  #...
  environment: [
     LICENSE_PATH=/opt/expensive-license,
     NAME=name
  ]
  environment: SOME=value, OTHER=1

this will result in these environment variables::

  SOME=value
  OTHER=1
  ...

the is no ``LICENSE_PATH`` or ``NAME`` variable set! only the last setting of ``environment: VARS`` has effect!

especially when using templates it is often convenient to just *add* to the list of
already set environment variables, instead of overwriting:

.. code-block:: lnc

  process print env vars
  #...
  add environment: TERM=vt100
  add environment: LICENSE_PATH=/opt/expensive-license
  add environment: LN_PROGRAM_NAME=printenv

this is usually the preferred way of adding environment variables.

for certain environment variables like ``PATH``, ``LD_LIBRARY_PATH``, ``PYTHONPATH`` it is often convenient to
just append something to an already set value.
this can be done like this:

.. code-block:: lnc

  process print env vars
  #...
  append environment PATH: :/usr/bin
  append environment PATH: :/sbin
  append environment PATH: :/usr/X11/bin:/usr/sbin
  append environment LD_LIBRARY_PATH: :/usr/lib
  add environment: LN_PROGRAM_NAME=printenv

this will result in these environment variables::

  PATH=:/usr/bin:/sbin:/usr/X11/bin:/usr/sbin
  LD_LIBRARY_PATH=/usr/lib:/path/to/ln_manager/libln
  LN_PROGRAM_NAME=printenv

for bash-like shells the unnecessary leading ``:`` in ``PATH`` does no harm.

pass_environment
----------------

type: *list of strings, names of environment variables*

.. note::
   todo!

The environment variables listed here are read from the
managers environment and passed unmodified to the environment of the process.

term_signal
-----------

Defines :term:`signal <UNIX signal>` that causes termination.

type: *string of ["SIG"] [ "INT" "TERM"  ...] or int signal, default: SIGTERM*

.. note::
   todo!

term_timeout
------------

type: *float, time in seconds*

.. note::
   todo!

.. _config/lnm/processes/ready-regex:
   
ready_regex
-----------

type: *string, regular expression*

.. note::
   todo!

ready_time
----------

type: *float, time in seconds*

.. note::
   todo!

policy
------

type: *string of [ "SCHED_FIFO" "SCHED_RR" ... ]*

.. note::
   todo!

scheduling policy. supported values depend on the node's operating system.

.. _process-priority:

priority
--------

type: *int*

scheduling priority. supported values depend on selected policy and the node's
operating system. for linux policy FIFO valid values are ``0 .. 99`` where
``99`` is the highest possible priority.

affinity
--------

type: *list of int*

.. note::
   todo!

list of cpu-id's (zero based) where this process is allowed to run on.

warning_regex
-------------

type: *string, regular expression*

.. note::
   todo!

warning_msg
-----------

type: *string*

.. note::
   todo!

documentation
-------------

type: *string, URL*

when provided, ln_manager GUI will display an additional button with label
"documentation" for this process. when it is pressed the given ``URL`` will be
passed to the `xdg-open <https://linux.die.net/man/1/xdg-open>`_-command which can for example open a web-browser
showing the page at that URL.

example:

.. code-block:: lnc

  process show pwd
  flags: start_in_shell
  node: localhost
  command: pwd
  documentation: https://man7.org/linux/man-pages/man1/pwd.1.html

when user presses this ``documentation`` button a browser should show that
`pwd man-page <https://man7.org/linux/man-pages/man1/pwd.1.html>`_.

use_template
------------

type: *string, name of template*

templates can help to avoid repetitions in your config files.

example:

.. code-block:: lnc

  process echoA
  add environment: HOME=%(CURDIR)/home
  flags: start_in_shell, no_error_on_successful_stop
  command: echo "this is process A, with HOME=$HOME"
  node: localhost

  process echoB
  add environment: HOME=%(CURDIR)/home
  flags: start_in_shell, no_error_on_successful_stop
  command: echo "this is process B, with HOME=$HOME"
  node: localhost

using templates, this can be reduced to:

.. code-block:: lnc

  template local shell command
  add environment: HOME=%(CURDIR)/home
  flags: start_in_shell, no_error_on_successful_stop
  node: localhost

  process echoA
  use_template: local shell command
  command: echo "this is process A, with HOME=$HOME"

  process echoB
  use_template: local shell command
  command: echo "this is process B, with HOME=$HOME"

the ln_manager will produce the exact same two processes for these two examples!

you can imagine that the directives from the named template are directly
inserted at this ``use_template:``-point!

templates are used in the order you name them -- if the same directive is used
in multiple templates for the same process, only the last one will have effect:

.. code-block:: lnc

  DELIBERATELY BAD EXAMPLE
  template local shell command
  add environment: HOME=%(CURDIR)/home
  node: localhost
  flags: start_in_shell

  template no error on stop
  flags: no_error_on_successful_stop

  process echoA
  use_template: local shell command
  use_template: no error on stop
  command: echo "this is process A, with HOME=$HOME"

this will probably not behave as intended, after template-instantiation it is equivalent to:

.. code-block:: lnc

  DELIBERATELY BAD EXAMPLE
  process echoA
  add environment: HOME=%(CURDIR)/home
  node: localhost
  flags: start_in_shell
  flags: no_error_on_successful_stop
  command: echo "this is process A, with HOME=$HOME"

and with that the ``flags:`` list will be set to only
``no_error_on_successful_stop``.

this example should be fixed to look like this:

.. code-block:: lnc

  template local shell command
  add environment: HOME=%(CURDIR)/home
  node: localhost
  add flags: start_in_shell

  template no error on stop
  add flags: no_error_on_successful_stop

  process echoA
  use_template: local shell command
  use_template: no error on stop
  command: echo "this is process A, with HOME=$HOME"

in this case the ``local shell command`` template will add ``start_in_shell`` to
the ``flags``-list and the ``no error on stop``-template will add
``no_error_on_successful_stop``. (the important thing here is the use of ``add``
before naming the ``flags``-list)

a template can also provide "parameters" which can be used via the
``%(parameter-name)``-syntax:

.. code-block:: lnc

  template local shell(cmd)
  add environment: HOME=%(CURDIR)/home
  node: localhost
  add flags: start_in_shell
  command: %(cmd)

  template echo(message)
  use_template: local shell("echo %(message)")

  process echoA
  use_template: echo("this is process A, with HOME=$HOME")

there the first template has a template-parameter named ``cmd``, which is used
in the value of the ``command:``-directive.
the ``echo``-template has a parameter named ``message`` which is used when
instantiating the ``local shell`` template.
these parameters are positional parameters, they have to be specified at
"call-site"(-> ``use_template:``-site) in the same order as they are given when the template is defined.

this also demonstrates that templates itself can make use of other templates.

here an example showing how multiple parameters can be used:

.. code-block:: lnc

  template shell with home(home, cmd)
  add environment: HOME=%(CURDIR)/%(home)
  node: localhost
  add flags: start_in_shell
  add flags: no_error_on_successful_stop
  command: %(cmd)

  process echoA
  use_template: shell with home("home_A", "echo this is process A, with HOME=$HOME")

change_directory
----------------

type: *string, directory*

this will change the current working directory of this process. if not specified
``/tmp`` will be used as working directory.

example:

.. code-block:: lnc

  process default process
  node: localhost
  flags: start_in_shell
  command: pwd

should output ``/tmp`` as current working directory, while

.. code-block:: lnc

  process in local dir
  node: localhost
  flags: start_in_shell
  command: pwd
  change_directory: %(CURDIR)

will change the current working directory to be the directory of this config file!

.. _reference/config/process/flags:

flags
-----

type: *list of strings*

.. note::
   todo!

valid process flags:

.. _process/forward_x11:

forward_x11
   With this flag the ln_manager tries to provide all necessary information such
   that the process can open a connection (open windows) on the same X-Server on
   which the manager GUI is currently displayed.

   This sets all necessary environment variables and might even establish tcp-tunnels.

   This is equivalent to ssh's ForwardX11 option but without encryption!

   Environment variables overwritten are: DISPLAY and XAUTHORITY. This is all
   that is needed for a simple X11 connection.

start_in_shell
   the most efficient way to start a process is via operating system-specific system calls. this also guarantees
   the most exact control about the environment the process is started in.

   but sometimes you want to execute the command-line via a shell. this is done when this flags is specified. the
   default system shell will be used to execute the command-line (``/bin/sh``).

   this allows to execute internal shell-commands which have no own program-binary to start (like bash's
   ``ulimit``, ``if``, ``;``...)

no_pty
   when this flag is specified, no pseudo-terminal will be allocated for this process -- which is more resource
   efficient. **BUT**: typical standard-C libraries change their behaviour depending on whether they are used
   within a terminal or not. for example: they switch their stdout-buffering from line-based to page-based -
   which means that the process-output will only be visible when a complete memory-page is filled (~4kB). On
   windows there is nothing comparable to a pseudo-terminal (the win32-console window is the only available
   terminal).

use_vte
   when this flag is given a full-featured terminal-emulator-widget is used to display the process output within
   the ln_manager-gui. this will only have an effect when the python-vte package is available on the
   manager-gui-host.  this terminal widget allows processes to display colors and advanced console-based user
   interfaces (like emacs or vim).

remove_color_codes
   you can remove color-escape-sequences from process output if you are not interested in having colored
   process-output and/or you don't use the ``use_vte`` flag.

no_state_led
   this flag avoids the display of a square (red, yellow, green) indicator box for this process.

no_warning_window
   as soon as you specify a ``warning_regex`` a dialog-window will pop-up every time that ``warning_regex`` matches a
   process-output line. by using this flag the display of the warning-dialog can be disabled.

no_error_on_stop
   when a process terminates on its own - without the user requesting it - its interpreted as an error if this
   flag is not specified

disable_stop
   this flag disabled the the stop-button in the manager gui.

.. _config/process/depends_on:

depends_on
----------

type: *list of strings, process- or state names*

all the processes listed here need to be in ``ready`` state (or ``started`` if they don't have a ``ready_regex`` or
``ready_time``) before this process can be started. if you try to start this process, the ln_manager will make sure to
start all of these dependencies.
if any of these dependencies terminate later, this process gets a error-flag to signal to the user that it probably
can no longer function as expected. this error disappears as soon as all dependencies are ready again.

.. _config/process/depends_on_restart:


depends_on_restart
------------------

type: *list of strings, process- or state names*

similar as ``depends_on`` but here a dependency that terminates will permanently set an error-flag for this
process. this signals to the user that this process needs to be restarted before it can function again.
if a user deliberately stops one of these dependencies, then the ln_manager will first stop this process, as it can be
of no more use when configured like this.

start_before
------------

type: *list of strings, process names*

before this process starts, all listed processes will be started and waited upon until they stop successfully.
these other process all need to use the ``no_error_on_successful_stop``-flag.

start_on_ready
--------------

type: *list of strings, process- or state names*

the processes or states listed here will be started as soon as this process reaches its ready state. They do
not need to keep running for the whole lifespan of this process and are not needed for this process to be in a
valid state.

Use-cases include starting small shell-scripts or other helper programs every time when this process is
started. It is advisable to specify the ``no_error_on_stop``-flag for processes listed here!

*since version 0.2.5*

start_on_successful_stop
------------------------

type: *list of strings, process names*

the processes listed here will be started as soon as this process stopped successfully. For this to work you also
need to use the flag ``no_error_on_successful_stop``.

*since version 1.1.6*


provides
--------

type: *list or strings*

.. note::
   todo!

output_encoding
---------------

type: *string, default: utf-8*

what text encoding does this process use for its otuput to stdout / stderr.

output_logfile
--------------

type: *string*

a filename where ln_manager should write all collected stdout/stderr from that
process.
use ``output_logfile_size_limit`` to limit the size of that file and
``output_logfile_keep_count`` to set how many files to keep when that size limit
is reached.

output_logfile_size_limit
-------------------------

type: *size, default: 10MB*

max size in bytes of the ``output_logfile``. you can also use common post-fixes
like ``MB`` or ``GB``. if the logfile reaches this size-limit, a new logfile
will be started. use ``output_logfile_keep_count`` to set how many log files to
keep around before starting to delete them.

output_logfile_keep_count
-------------------------

type: *integer, default: 2*

number of log files to keep if ``output_logfile_size_limit`` is set.
older log files will get a timestamp in the form of ``.YYYYmmdd_HHMMSS`` attached
to their filenames. the timestamp is of the moment when a new log file was started.

output_buffer_size
------------------

type: *integer, default: 100kB*

buffer size in bytes for ln_daemon for collecting output from this
process. output needs to be sent from daemon to ln_manager. increase this buffer
size if the network connection towards the manager is too slow or your process
generates lots of output! (e.g. more that 100kB per ``max_output_frequency``)


max_output_lines
----------------

type: *int, number of lines, default: 1000*

number of lines in ln_manager's VTE-widgets (terminal widget showing process output)
scroll-back buffer.

max_output_frequency
--------------------

type: *float, frequency in Hz, default: 10Hz*

try to batch process output to send updates towards ln_manager only at this
rate. faster process output updates will be sent if fill-level of
`output_buffer`` reaches 75%.

.. _process-add_io_script:

add_io_script
-------------

this adds an I/O script to this process. I/O scripts can be used to send
pre-defined text-input to this process if it is started/ready.

this can be useful for "uncooperative" or foreign processes that provide no
LN-service-interface, but react on text input.
examples include any remote-shells (like an interactive bash shell via ssh, a
serial terminal to a linux text console or any telnet connection) or programs
which provide a text-based / terminal-based text-interface.

I/O scripts have a per-process unique name (given by the first ``#``-comment
line in the script itself).

here an example I/O script to login on a linux serial console and execute a
command:

.. code-block:: lnc

  process ifconfig via serial console
  node: localhost
  command: /home/schm_fl/foreign_packages/picocom/2.1/sled11-x86_64-gcc4.x/picocom -b 115200 /dev/ttyUSB0
  ready_regex: Terminal ready
  add_io_script: [
    # autostart
    # send a single newline to re-trigger display of login prompt:
    send_eval: \x04
    wait: login:
    send: root
    wait: Password:
    send: secret-password
    # wait for shell-prompt:
    wait_regex: ^root@.*#
    send: ifconfig
  ]

this script is named ``autostart`` so it will be started as soon as ``picocom``
outputs the ``Terminal ready`` string which matches the ``ready_regex``.

the script will then send a single `0x04`-byte (corresponds to a user pressing
Ctrl-d signaling "end of transmission") and then wait for text output ``login:`` to appear.
after that it will send the username ``root`` followed by a newline, then wait
for the text ``Password:`` and send a password. next it waits for a regular
expression which should match the remote's shell PS1 prompt to then send the
``ifconfig`` command.

you can add multiple I/O scripts to a single process, but they have to have a
unique name. if no name is provided an automatically generated name will be
used.

this means that you can only have one script that is called ``autostart``.

here the list of available I/O script commands:

* ``send: TEXT`` send some text to stdin of this process. a newline will be
  appended to ``TEXT``.

* ``wait: TEXT`` wait until ``TEXT`` appears in the output of this process.

* ``wait_regex: REGEX`` wait until the output of this process includes a match
  for the given regular expression ``REGEX``. (those are python regex'es with
  the ``re.M``-multiline flag set)

* ``pause`` stop executing this script here. user can select the next line and
  continue executing via the LN-manager-ui.

* ``insert: TEXT`` will insert ``TEXT`` (plus newline) into the output of this
  process. this text will not be sent to the process, it will only be shown in
  the LN-manager-ui.

* ``_eval:``-variants are available for all commands but ``pause``: ``send_eval:
  TEXT``  for example will only send ``TEXT`` without a trailing newline! but
  now ``TEXT`` can include python-escape-sequences for special characters like
  ``\n`` or ``\t`` see `table of python escape sequences <https://docs.python.org/3/reference/lexical_analysis.html#string-and-bytes-literals>`_.

* ``if: TEXT`` followed by a block of script commands terminated with an
  ``endif``-line or further ``elif: TEXT`` blocks.
  this can be used to wait for multiple expressions and react on which specific
  expression was found.
  example::

    if: 1+1
      send: 2
    elif: 2+2
      send: 4
    elif: 3+3
      send 6
    endif

  this will wait for the process to output either ``1+1``, ``2+2`` or ``3+3``
  and then send the respective response.
  there can not be an "empty" ``else:`` block as we can not wait for the empty
  string.
  as with ``wait`` there are ``_regex`` and ``_eval`` variants for ``if`` and
  ``elif``.
  "if blocks" can also be nested. an ``if``-block without any ``elif`` blocks
  behaves exactly the same as the corresponding ``wait`` command.

processes that have I/O scripts configured provide an additional ``io
scripts``-tab when selected in the LN-manager-gui:

.. image:: images/config_process_io_scripts.png
  :alt: I/O scripts tab of a process

the left-most "play" tool-button can be used to start & stop running of scripts.
the next "play" tool-button can be used to only execute the currently selected
script-line (you can also just double-click on a script line for this).
the third tool-button can be used to restart an I/O script from the beginning.

I/O scripts can only be started when the corresponding process is in started /
ready state.
stopping a process also stops I/O scripts.


add_io_script_file
------------------
same as :ref:`process-add_io_script` but here the I/O script is read from the specified
file:

.. code-block:: lnc

  process some process
  ...
  add_io_script_file: %(CURDIR)/remoteHost_login
  add_io_script_file: %(CURDIR)/remoteHost_setup_network

this will add two new scripts named ``remoteHost_login`` and `remoteHost_setup_network`` to this process.

contents of file ``remoteHost_login`` could look like this::

  wait: password:
  send: secret-password

per default the filename will be used to choose a unique script-name.
if you want to use a different script-name (especially useful if you want to
name it ``autostart``), you can explicitly provide a script name before the
colon ``:``:

.. code-block:: lnc

  process some process
  ...
  add_io_script_file autostart: %(CURDIR)/remoteHost_login
