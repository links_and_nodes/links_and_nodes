``hosts`` section
=================

``hosts``-sections can be used to specify per-host settings. these include ip-addresses, ln_daemon-start-commands,
authentication settings and more, as described below.

it is not needed to always have a ``hosts``-section, ln_manager will guess reasonable default values and will use the
systems name-resolver to get ip-addresses when trying to contact remote hosts.

syntax::

  HOSTS_SECTION := "hosts\n" + HOST_SPEC*
  HOST_SPEC := HOST_NAME + ": " + HOST_SETTINGS + "\n"
  HOST_SETTINGS := HOST_SETTING + [ ", " + HOST_SETTING ]*
  HOST_SETTING := SETTING-NAME + ":" + SETTING-VALUE

example:
  
.. code-block:: lnc

  hosts
  localhost: process_start_rate_limit:5
  rmc-mobilproxy: ens3:129.247.189.41, ens10:192.168.128.1

in this example, at most 5 processes per second will be started on localhost. for ``rmc-mobilproxy`` we specify two
ip-addresses with assigned interface-names ens3 and ens10.

an ln-config file can have multiple ``hosts``-sections. they will be merged -- later ones can overwrite settings or add
more interfaces.

valid settings:

<interface-name>
----------------
any ``<SETTING-NAME>`` that does not match one of the below listed, known settings, will be assumed to be an
interface-name. ``<SETTING-VALUE>`` in this case has to be a valid IPv4-address in dotted-decimal form.
example::

  en0:129.247.189.41

the interface name is only used to refer to that interface. it doesn't have to match the actual interface name.

process_start_rate_limit
------------------------
this setting can be used to limit how many processes are started per second at most on this host. without this setting
there is no limit. this can lead to an unresponsive system if you try to start many processes "at once".
example::

  process_start_rate_limit:2

will only allow 2 processes to be started within the same second on this host.

.. _hosts/daemon_start_script:

daemon_start_script
-------------------
in case ln_manager can not contact an already running daemon, it will try to start a new one. this setting can be used
to specify the command that should be used.

default is::

  <LN_PREFIX>/bin/ln_daemon

where ``<LN_PREFIX>`` depends on how LN was installed on your system.

this default can be overwritten by environment variable :envvar:`LN_DAEMON_START` to set a new site-local start-command
(e.g., via conan's ``package_info()`` method).
  
it can be set to special value ``none`` to NOT have the manager try to start a daemon here.

can also be set globally via instance-:ref:`instance/daemon_start_script`.

.. _hosts/additional_ssh_arguments:

additional_ssh_arguments
------------------------

LN daemons on other :term:`nodes <node>` can be started on-demand via
an :term:`SSH <ssh>` connection.

This option passes one or more additional arguments to append to the ssh-command
line when ln_manager tries to start an ln_daemon on a remote host.
default is empty. they will be appended to this ssh-command::

  <SSH-BINARY> -l <USERNAME> -T -v -a -x \
    -o UserKnownHostsFile=/dev/null \
    -o StrictHostKeyChecking=no \
    -o PasswordAuthentication=no \
    -o PreferredAuthentications=gssapi-with-mic,publickey \
    -K <HOST-NAME-OR-IP> <CMD>

can also be set globally via instance-:ref:`instance/additional_ssh_arguments`.

.. index::
   pair: LN daemon startup; secure shell access
   triple: LN daemon; path to secure shell binary; ssh
   single: LN daemon; starting via remote ssh command


.. _hosts/ssh_binary:

ssh_binary
----------

this setting can be used to cause ln_manager to use a different binary
than ``ssh`` for the :term:`ssh <ssh>` connection.

The manager tries to use this ssh-program when a new daemon process has to be started on a remote
machine. (you want to adjust this settings especially when the manager runs on a windows host)

can also be set globally via instance-:ref:`instance/ssh_binary`.

.. seealso::
   
      For more information on how processes on remote hosts are
      started, please refer to section :ref:`guide/ln_daemon` in the
      user guide part.


arch
----
this can be an arbitrary string that will be passed to newly started ln_daemons. daemons will report this arch when
ln_manager connects them. this can be used in ``process`-sections as runtime-variable ``$(ARCH)`` e.g. in the
``command:``-field.

.. index::
   triple: LN daemon; configuration of private key file; reference
   single: LN daemon; location of DSA keys


.. _hosts/daemon_private_key_file:

daemon_private_key_file
-----------------------

Specifies a private key to use for user-authentication towards daemons. This should be a DSA private key in PEM format as used
by OpenSSH. Unlocked daemons will be locked with this private key on
connection. 

.. note:: to disable daemon authentication set ``daemon_public_key_file: none``
	  in the ``hosts``-line for that respective host. might look like this::

	    hosts
	    miiwa2-server: daemon_public_key_file: none, expected_user: l_mobil

	  you should disable authentication only for hosts where you don't login
	  with your personal credentials. usually hosts where many people share
	  the same local user-account (like ``l_mobil``) and only if those local
	  user-accounts do not store any sensitive data.

.. note:: LN instances use daemons, which are owned by users

    LN is designed so that users start LN instances, which start and
    use LN daemons to communicate between LN manager, clients, and
    hosts. A user can start an arbitrary number of differently named
    LN instances at the same time, which will in turn (using
    system-provided arbiter processes) start any needed LN daemons on
    the corresponding nodes. All the daemons which are started by such
    instances belong to that user, and are designed to be controlled
    only by this user.  By default, they therefore cannot be accessed
    by other users, and also won't accept commands from other user
    IDs. This is designed that way for security and safety reasons.

To protect daemons from being used by unauthorized users/managers, they are by default started with a public-key (see
daemon_public_key_file below) on their command line (see :ref:`starting a daemon`). 

.. index::
   triple: LN daemon; public-key authentication; configuration

.. note:: Use of public-key authentication for accessing LN daemons

   By default, the LN daemons which are started by the LN manager will
   be locked so that they cannot be accessed without
   authorization. For this, so-called *public key authentication* is
   used: A key pair is generated with one public and one matching
   private key.  The manager which starts an LN daemon sends a *public*
   key to the daemon after starting it. When connecting again, any LN
   manager has to authenticate itself for the daemon by proving that
   it has a matching private key.
   
   This is done by challenge-response authentication: The LN daemon
   sends some random data to the LN manager, and the LN manager signs
   that data with the corresponding *private* key, and sends it back.
   The daemon can then check the validity of this signature with the
   specified public-key.

   This is considered to provide a state-of-the-art authentication
   mechanism.  Due to the used set-up, reading/sniffing a cleartext
   TCP connection does not provide reasonable information about the
   private key!

   But beware: it all depends on how secret your private key really
   is! **Make sure only authorized users can read that key-file!**

.. index::   
   pair: LN message communication; security of

.. Warning:: This does not cause the daemon-manager connection to be encrypted! 

    The only use of this authentication is to protect daemons from
    being used by unauthorized users. While starting a new daemon
    might make use of the ssh command, the data links in the LN system
    are *not* encrypted. 

For those daemons the manager needs to know the matching private-key to authenticate 
itself against the daemon.

The default name is :file:`$HOME/.ssh/id_dsa_ln_daemons`. If no such
default key file is found, and authentication is not explicitly
disabled, one with that name is automatically generated and used.  The
whole process should normally work automatically without any specific
user intervention or configuration (apart from possibly setting up an
SSH connection for automatically starting daemons).

Alternatively, keys pairs can be generated manually and stored in two
files, one file for the public key, and one file for the private key.
This config opion allows to specify a different public key by its file
name.  Currently only DSA keys in PEM-format are supported (like the
ones created with OpenSSH's `ssh-keygen -m PEM -t dsa`). 


This can also be set globally via instance - see :ref:`instance/daemon_private_key_file`.

For disabling this authentication, see the next section
:ref:`hosts/daemon_public_key_file`.

Changing the key file for a given LN instance
requires to either stop the daemon via the LN manager before stopping
the manager, or to manually kill and then restart the daemon, which then updates the
authentication key to the new value.

*see also*:

.. seealso::
   
  * For more information on how processes on remote hosts are started,
    please refer to section :ref:`guide/ln_daemon` in the user guide
    part.
    
  * :ref:`guide/daemon_authentication` for an explanation on authentication for the LN daemon


   
.. index::
   triple: LN daemon; configuration of public key file; reference
   single: LN daemon; location of DSA keys

   
.. _hosts/daemon_public_key_file:

daemon_public_key_file
----------------------

public key to use for user-authentication towards daemons. should be a
DSA-encoded public key as used by OpenSSH. ln_manager will pass this
public key to ln-daemons to lock them and for them to verify signatures from managers trying to authenticate themselfes.
:file:`$HOME/.ssh/id_dsa_ln_daemons.pub`. The value can be set to
``none`` or ``None`` to disable manager-daemon authentication for this
host.
You can also set it to ``never`` to disable authentication and never allow this daemon to be locked in future.
(this can be useful for shared daemons on shared systems with shared user-accounts)

.. warning:: Disabling authentication allows to execute arbitrary commands via the daemon, including retrieval of private user data and uncontrolled movements of robots. 

  For safety and security reasons, this should not be done when
  the host controls robotic actors, or is a networked host which
  has access to any private or confidential data.

When the manager needs to start a new daemon and this config-key is
specified, the manager passes this filename via an command line option
to the newly started daemon.  The file must be visible and accessible
for the daemon.

Daemons started with a public key require managers to authenticate
themselves with a matching private key. See :ref:`hosts/daemon_private_key_file`.

can also be set globally via instance -  see :ref:`instance/daemon_public_key_file`.


.. seealso::
   
  * For more information on how processes on remote hosts are started,
    please refer to section :ref:`guide/ln_daemon` in the user guide
    part.
    
  * :ref:`guide/daemon_authentication` for an explanation on authentication for the LN daemon


preferred_network
-----------------
if a host is reachable via multiple networks, this setting can be used force the use of the named network.

.. _hosts/expected_user:

expected_user
-------------
when ln_manager is  contacting ln_daemons, it defaults to expect the daemon to be running as the same user.
use this setting if you want to override this, e.g. if the remote host only has some few local users.

can also be set globally via instance-:ref:`instance/default_expected_user`.

.. _hosts/expected_user_id:

expected_user_id
----------------
similar as ``expected_user`` this can be used to require a specify user-id from the remote ln_daemon.

arbiter_port
------------
ln_daemons will always start an `arbiter`-process per host if there is not already one running. this arbiter is usually
expected to run behind port-number ``54376``.

.. _hosts/default_change_user_to:

default_change_user_to
----------------------
if the ln_daemon itself is running with root-rights, this setting can be used to switch to a different user when
starting processes where no specific ``change_user_to``-setting was specified.

can also be set globally via instance-:ref:`instance/default_change_user_to`.

requires_tunnel
---------------
this setting is useful if some other state or process has to be UP or ready before ln_manager is allowed to try to
contact this host.
this can be used to bring up a new interface (possibly a vpn) or start other kind of tunnels.
``<SETTING-VALUE>`` should be the name of a process or state in your config.

filesystems
-----------
a list of file-system names this host has access to.

.. note::
   todo: i think this is not yet used for anything?

lookup_aliases
--------------
boolean, whether or not to use domain-name-system to search for aliases of this host.
set to ``false`` if you use host-names that are not registered with your domain-name-system:

.. code-block:: lnc

 hosts
 arbitrary-name: lookup_aliases:false, ...

this will speedup ln_manager startup as it doesn't has to wait for your DNS/name-lookup to answer.

you can see which names are looked up if you set the :envvar:`LN_DEBUG` environment variable to ``1`` before starting
the ln_manager.

skip_fw_test
------------
boolean, default: false

whether to skip the check if this host has a firewall blocking incoming UDP or TCP
connections. if ``skip_fw_test:false``, a few UDP packets will be sent and a TCP connection
established the first time the ln_manager connects it. currently this is only
done in "reverse direction" as the "forward direction" is assumed to work after
we made a successful connection to that daemon... (and that test can only be
done when the ln_daemon connection was successful).

this will generate an error message in ln_managers log if there is a firewall
detected.
