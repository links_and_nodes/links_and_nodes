==================
Config File Syntax
==================

This section describes the syntax used for the links_and_nodes manager
config file. The used notation is the same as in the [Python Language
Refernce](http://docs.python.org/2/reference/introduction.html#notation)
-- a modified [Backus–Naur
Form](http://en.wikipedia.org/wiki/Backus%E2%80%93Naur_Form) - please
refer to these pages for more information about this notation.

here is the lexical definition::

    config_file ::= config_section*
    config_section ::= section_header "\n" key_value_pairs*
    
    section_header ::= section_type [ WS section_name ]
    section_type ::= terminal_string
    section_name ::= config_string
    
    key_value_pairs ::= [WS] section_key [WS] ": " section_value "\n"
    section_key ::= terminal_string
    section_value ::= config_string | ( "[" config_string_multiline "]" )
    
    config_string_multiline ::= config_string "\n" [ config_string_multiline ]
    config_string ::= ( replacement | [ terminal_string ] )*
    
    replacement ::= "%(" config_string ")"
    terminal_string ::= any_text_wo_newline+
    
    any_text_wo_newline ::= <any character except "\n">
    
    WS ::= [ " " | "\t" | "\r" | "\n" ]+
    
(note: empty lines and lines starting with "#" are ignored, trailing
whitespace is ignored)
