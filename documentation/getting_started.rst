Getting Started
***************

Installation
============

check `README.md` how to install LN.

.. only:: rmc

    .. RMC-specific section, which is only added when "-a -t rmc" is added to the
       sphinx-build command.
    .. for info on how this works, see:
       https://ourpython.com/python/how-can-i-configure-sphinx-to-conditionally-exclude-some-pages

   
    Environment setup on RMC systems
    ================================
    
    to setup your environment with our conan install you can do::
    
      conan install links_and_nodes_manager/[~2]@common/stable -if conan -g virtualenv -g virtualenv_python
      source conan/activate.sh
      source conan/activate_run_python.sh

    Here, you should replace
    "links_and_nodes_manager/[~2]@common/stable" by the current LN
    version, which is "|DOCUMENTED_LN_MANAGER_VERSION|".

      
    See the :doc:`rmc_localguide` section in the Appendix for detailed further information.
    
    .. todo: maybe find simple/short cissy command
    
Test your Installation
======================

to test whether your LN install was successful and whether your environment is
setup correctly try to execute::

  python3 -c "import links_and_nodes"
  ln_manager --help

this should not output any errors and you should see the ln-manager help::
  
  2022-03-16 15:25:01.79 info    Manager:           ln_manager <source tree>
  2022-03-16 15:25:01.79 info    Manager:           python3.6 from /opt/python/osl153-x86_64/python3/stable/1.4.4/bin/python3.6 on linux-x86_64
  
  usage: ln_manager [-h] ( -c config_filename [MANAGER_OPTIONS] | --connect HOST_OR_IP:PORT )
  
          -h                  show this usage info and exit
          -c config_filename  read configuration from given file
  
  MANAGER_OPTIONS:
          -i instance_name       overwrite configured the name of this ln-instance!
          -p PORT                overwrite configured manager listening port
          --log-level LEVEL      sets the log-level. valid values are: debug, info, warning, error
          --start NAME           starts process/state or group NAME after manager startup (can be specified multiple times)
          --without-gui          start manager without gui
          --present              present manager window after startup
          --profile              collect cProfile data and write to ln_manager.stats
          --show-stats fn        read cProfile file fn and print stats
          --console              start command console
          --console-exec CMD     start console and execute CMD
          --mi-console           start command console in machine-interface-mode
          --export-hosts SSHCFG  read given ln-config file (maybe only hosts.inc.lnc?) and generate ssh config file
          --isolated-test        use unique, generated instance-name & manager-port and use private ln_daemon on localhost
  
          --connect HOST_OR_IP:PORT  start only a gui connected to an already running manager at HOST_OR_IP:PORT
  
