``state`` objects
=================

a state has a unique name. other that processes, states can be defined without a fixed node-assignment. a state without
an explicit node assignment will inherit the node from a depending process.

there are two types of states:

* a set of 3 shell-commands: CHECK, UP and DOWN
  the shell exit-code (return value) of the CHECK command is interpreted to decide whether a state is UP or DOWN.

* a TCP tunnel state
  a TCP/IP tunnel established by the ln_daemons. only if tcp_tunnel_exit_target and tcp_tunnel_exit_node is specified.


.. note::
   their CHECK command is executed as soon as daemon-connection is established
   ``checking all states of newly connected daemon rmc-lx0142``
   not good. after reload all state are unknown, thats better behaviour!

.. note::
   daemon always executes CHECK command before UP or DOWN and only executes UP/DOWN command if needed!
   change this! this is ugly!
   
.. note::
   a state with a "depends_on:"-configuration only startes depends when requested UP or DOWN.
   NOT on CHECK!
   todo: add flag to also require depends on CHECK. but then also add flag that explicitly disables CHECK on connect.

   
.. note::
   todo: check how often and when a ``state_notification`` is sent by the daemon


check
-----
shell command to execute to decide whether a state is currently UP or DOWN.
the shell exit-code (return value) of the CHECK command is interpreted to decide whether a state is UP or DOWN.
a 0 exit-code is commonly interpreted as "success" and the state is assumed to be UP.
every non-zero exit-code will cause the state to be assumed DOWN.

up
--
shell command to execute to bring a state UP.

down
----
shell command to execute to bring a state DOWN.


min_check_interval
------------------
number of seconds to assume that last state-result is still valid.
(default: 30 since version 0.13.11)

tcp_tunnel_exit_node
--------------------
only for TCP tunnel states.


tcp_tunnel_exit_target
----------------------
only for TCP tunnel states.

tcp_tunnel_entry_port
---------------------
only for TCP tunnel states.


flags
-----
list of one of these flags:

* ``os_state``
* ``autostart``
* ``need_check_after_up``

node
----
node where this state is assigned to.

depends_on
----------
list of dependencies that need to be fulfilled before the UP or DOWN command can be executed.

see depends_on_restart-description of the ``process`` object.
todo: link
todo: suggestion: list of other states / processes where this state's UP & DOWN commands depend on.

depends_on_restart
------------------
see depends_on_restart-description of the ``process`` object.
todo: link

provides
--------
see provides-description of the ``process`` object.
todo: link

output_buffer_size
------------------
todo!

arch
----
todo!

max_queued_output_bytes
-----------------------
todo!

max_output_lines
----------------
todo!

