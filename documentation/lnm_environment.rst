###########################################
Environment Variables which control the LNM
###########################################


.. envvar:: LN_MESSAGE_DEFINITION_DIRS

TODO: explain LN_MESSAGE_DEFINITION_DIRS


.. envvar:: LN_OPEN_SCOPE_ON_PARAMETER_DOUBLE_CLICK

.. envvar:: LN_DAEMON_START

see :ref:`hosts/daemon_start_script`.

.. envvar:: LN_MANAGER_ASK_SHUTDOWN

defaults to ``yes``

if set to "yes" will ask the user whether to only close the gui and keep the
manager process alive when user wants to close the gui window while there are
still clients connected.
set this to ``no`` to disable this question and terminate ln_manager as soon as
users closes the gui window.


#################################################
Environment Variables which control the LN daemon
#################################################

.. envvar:: LN_DAEMON_NO_HIDE

default ``no``

if set to ``yes`` will show each and every received request from the ln_manager
connection.
