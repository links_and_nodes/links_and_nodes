import os
import re
import shutil

from conan import ConanFile, conan_version
from conan.tools.files import copy
from conan.tools.scm import Version


class links_and_nodes_manager_conan(ConanFile):
    package_type = "application"
    python_requires = "ln_conan/[^6.0.3]@common/stable", "cissy_conantools/[~1]@tools/stable"
    python_requires_extend = "ln_conan.Base"

    @property
    def cissy_conantools(self):
        return self.python_requires["cissy_conantools"].module

    name = "links_and_nodes_manager"
    homepage = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"
    license = "GPLv3"
    description = "links_and_nodes is a framework for easy creation and maintanance of distributed computing networks."
    settings = "os", "arch", "compiler", "build_type"  # those are needed for conan 2 builds but deleted from the package id
    exports = []
    exports_sources = [
        # all of python for scons to be able to build python
        "python*",
        "documentation*",
        "rmc/export_ln_daemon",
        "!python/links_and_nodes/*",
        "!python/links_and_nodes_manager/#*#",
        "!python/pyutils*",
        "share*",
        # will need base-build-system
        "site_scons*",
        "SConstruct",
    ]

    def init(self):
        # add python dist
        pydist_options = {
            "python_version": ["2", "3"],
            "python_dist_version": self.cissy_conantools.get_python_dist_versions(),
        }
        pydist_default_options = {
            "python_version": "3",
            "python_dist_version": self.cissy_conantools.get_python_dist_latest_version(),
        }
        if Version(conan_version) < "2.0.0":
            self.options = {**self.options, **pydist_options}
            self.default_options = {**self.default_options, **pydist_default_options}
        else:
            self.options.update(pydist_options, pydist_default_options)

    def package_id(self):
        # the package id should not depend on any settings, delete all of them
        del self.info.settings.os
        del self.info.settings.arch
        del self.info.settings.build_type
        del self.info.settings.compiler

    def build_requirements(self): # would be nice to get rid of this boilerplate and have it in ln_conan only once...DRY
        super().build_requirements()
        self.tool_requires(
            self.cissy_conantools.get_python_dist_requirement(
                self.options.python_version.value, self.options.python_dist_version.value
            )
        )

    def requirements(self):
        self.requires("links_and_nodes_runtime/%s" % self.same_branch_or("[~2]"))
        self.requires("links_and_nodes_python/%s" % self.same_branch_or("[~2]"))
        self.requires("links_and_nodes_ln_msgdef/%s" % self.same_branch_or("[>=1.2 <3]"))
        self.requires("links_and_nodes_cissy_helper/[~1]@common/stable")

    def configure(self):
        self.output.write(
            "python_version: %s, python_dist_version: %s\n" % (self.options.python_version, self.options.python_dist_version)
        )
        self.options["links_and_nodes_python"].python_version = self.options.python_version
        self.options["links_and_nodes_python"].python_dist_version = self.options.python_dist_version

    def source(self):
        self.write_version_file("version")

    def build(self):
        self.scons_build(
            "python documentation", "--only-manager --rmc-doc --without-daemon-authentication", with_python_lib=True
        )

    def _copy_replace(self, src, dst_dir, search, replace):
        dst = os.path.join(dst_dir, os.path.basename(src))
        with open(src, "r") as fp:
            export_ln_daemon = fp.read()
        export_ln_daemon = re.sub(search, replace, export_ln_daemon, flags=re.M)
        if not os.path.isdir(dst_dir):
            os.makedirs(dst_dir)
        with open(dst, "w") as fp:
            fp.write(export_ln_daemon)
        shutil.copymode(src, dst)
        copy(self, dst, self.build_folder, self.package_folder)

    def package(self):
        copy(
            self, "version", self.build_folder, os.path.join(self.package_folder, self.python_path, "links_and_nodes_manager")
        )  # todo: this is strange

        self._copy_replace(
            os.path.join(self.build_folder, "rmc", "export_ln_daemon"),
            "bin",
            "^REF=.*$",
            'REF="links_and_nodes_runtime/%s@%s/%s"' % (self.version, self.user, self.channel),
        )

        install = os.path.join("build", "python", self.install_sandbox, self.prefix[1:])
        copy(self, "*", os.path.join(self.build_folder, install), self.package_folder)
        doc_install = os.path.join("build", "documentation", self.install_sandbox, self.prefix[1:])
        copy(self, "*", os.path.join(self.build_folder, doc_install), self.package_folder)

    def package_info(self):
        if Version(conan_version) < "2.0.0":
            self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
            self.env_info.PYTHONPATH.append(os.path.join(self.package_folder, self.python_path))
        self.runenv_info.append_path("PATH", os.path.join(self.package_folder, "bin"))
        self.runenv_info.append_path("PYTHONPATH", os.path.join(self.package_folder, self.python_path))

        if self.channel == "unstable":
            allow = "-au"  # unstable
        elif self.channel != "stable":
            allow = "-as"  # snapshot
        else:
            allow = ""

        cissy_exe = "/opt/rmc-build-tools/cissy"
        if Version(conan_version) >= "2.0.0":
            cissy_exe = "/opt/rmc-build-tools/any/cissy2/bin/linux/cissy"
        ln_daemon_start = f"{cissy_exe} run {allow} -p links_and_nodes_runtime/{self.version}@{self.user}/{self.channel} ln_daemon"
        if Version(conan_version) < "2.0.0":
            self.env_info.LN_DAEMON_START = ln_daemon_start
        self.runenv_info.define("LN_DAEMON_START", ln_daemon_start)

    def deploy(self):
        self.copy("bin/*")
        self.copy("lib/*")

        self.copy_deps("bin/ln*")
        self.copy_deps("bin/file_services")
        self.copy_deps("bin/fs_sync")
        self.copy_deps("lib/*", excludes=("*.a", "*libboost*"))
        self.copy_deps("lib/libboost_python*")
        self.copy_deps("share/ln_runtime/*")
        self.copy_deps("message_definitions/*", dst="share/ln_runtime")
