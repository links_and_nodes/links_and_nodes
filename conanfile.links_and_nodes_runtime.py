import os

from conan import ConanFile, conan_version
from conan.tools.files import copy
from conan.tools.scm import Version


class links_and_nodes_runtime_conan(ConanFile):
    package_type = "application"
    python_requires = "ln_conan/[^6.0.3]@common/stable"
    python_requires_extend = "ln_conan.Base"

    name = "links_and_nodes_runtime"
    homepage = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"
    license = "LGPLv3"
    description = "links_and_nodes is a framework for easy creation and maintanance of distributed computing networks."
    settings = "os", "compiler", "build_type", "arch"
    options = {"with_libprocps": [True, False]}
    exports = []
    exports_sources = [
        "site_scons*",
        "SConstruct",
        "libln*",
        # excludes are checked with fnmatch against results of positive patterns!
        "!libln/.*",
        "!libln/tests/*",
        "!libln/examples/*",
        "share*",
        "python*",
        "external*",
        "!python/links_and_nodes/*",
        "!python/links_and_nodes_manager/*",
        "!python/pyparsing/*",
        "!python/pyutils/*.pyc",
        "ln_runtime*",
        "!ln_runtime/.*",
    ]

    resdir = "share/ln_runtime"

    def config_options(self):
        self.options.with_libprocps = self.settings.get_safe("os") == "Linux"

    def package_id(self):
        self.info.requires["libstring_util"].full_package_mode()

    def requirements(self):
        # runtime only requirements
        self.requires("liblinks_and_nodes/%s" % self.same_branch_or("[^2.5.0]"))

        # runtime & build requirements
        self.requires("libstring_util/[~1 >=1.2.3]@common/stable")

        if self.options.with_libprocps:
            self.requires("procps/3.3.15@3rdparty/stable")

    def source(self):
        self.write_version_header("LIBLN_VERSION", os.path.join("libln", "include", "ln", "version.h"))
        self.write_version_header("LN_RUNTIME_VERSION", os.path.join("ln_runtime", "ln_daemon", "src", "version.h"))
        self.write_version_file(os.path.join("ln_runtime", "version"))

    def build(self):
        opts = "--use-private-libtomcrypt"
        if not self.options.with_libprocps:
            opts += " --no-libprocps"
        if self.settings.get_safe("os") == "Neutrino":
            opts += " --without-daemon-authentication"
            opts += " --without-lnrecorder"  # needs -std=c++11
        self.scons_build("ln_runtime", opts=opts, verbose=True)

    def package(self):
        copy(self, "COPYING", os.path.join(self.build_folder, "ln_runtime"), os.path.join(self.package_folder, self.resdir))
        copy(self, "version", os.path.join(self.build_folder, "ln_runtime"), os.path.join(self.package_folder, self.resdir))

        self.output.info("cwd: %s" % os.getcwd())
        for binary in "ln_daemon,file_services,lnrecorder".split(","):
            install = os.path.join("build", "ln_runtime", binary, self.install_sandbox, self.prefix[1:])
            self.output.info("install: copy from %s/*" % install)
            copy(self, "*", os.path.join(self.build_folder, install), self.package_folder)

    def package_info(self):
        if Version(conan_version) < "2.0.0":
            self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
        self.cpp_info.resdirs = [self.resdir]
