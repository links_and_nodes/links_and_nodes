# conanfile to be able to run ln_manager from source dir

[requires]
## for python extension:
boost_python/1.66.0@3rdparty/stable

## for python programs (ln_manager, ln_generate) we need:
python3-dist/[~2]@tools/stable

[options]
boost_python:python_version=3

[generators]
virtualenv
virtualrunenv
virtualbuildenv

