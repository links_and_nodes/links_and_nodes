#!/bin/bash

pyver=3

SCRIPT=$(readlink ${BASH_SOURCE[0]} || echo ${BASH_SOURCE[0]})
RELPATH=$(dirname ${SCRIPT})
pushd ${RELPATH} > /dev/null
SCRIPTPATH=
cd ..
SRC_ROOT=`pwd -P`
popd > /dev/null


# install & get path to a conan python binding
BASE=/tmp/ln_binding_install_$USER
mkdir -p $BASE
cd $BASE
conan install links_and_nodes_python/2.0.0@common/snapshot -b missing -o python_version=$pyver -g txt
LN_PY_BASE=$(grep -e rootpath_links_and_nodes_python -A 1 conanbuildinfo.txt  | tail -1)

# place symlink into our source tree
cd $SRC_ROOT/python/links_and_nodes
ext_dir=$(python$pyver $SRC_ROOT/python/check_config.py | python$pyver -c 'import sys; print(eval(sys.stdin.read())["ext_dir"])')
ln -sf $LN_PY_BASE/lib/python/site-packages/links_and_nodes/$ext_dir src-$ext_dir

# cleanup
rm -rf $BASE
