#!/bin/bash

pushd `dirname ${BASH_SOURCE[0]}` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

export LN_BASE=$SCRIPTPATH/..

set -e

export SPHINXBUILDFLAGS="-a -t rmc"
(cd $LN_BASE; scons --from-env --subdirs=documentation --without-daemon-authentication --use-private-libtomcrypt --rmc-doc)

TMP=$(mktemp -d)
echo temp dir: $TMP
git clone -b gh-pages git@rmc-github.robotic.dlr.de:common/links_and_nodes.git $TMP
rsync -ravI --delete --exclude=.git $LN_BASE/build/documentation/html/ $TMP/
(
    cd $TMP/
    git add --all
    git commit -m "doc update"
    git push
)
rm -rf $TMP
