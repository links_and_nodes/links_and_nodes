#ifndef LINKS_AND_NODES_LN_PARAMETERS_EIGEN_H
#define LINKS_AND_NODES_LN_PARAMETERS_EIGEN_H

#include <ln/parameters.h>
#include <Eigen/Dense>
#include <type_traits>

using namespace std;

namespace ln {
namespace parameters {
template<typename T>
struct port_description<T, typename std::enable_if<std::is_base_of<Eigen::EigenBase<T>, T>::value, T>::type> {
	string signal_name;
	string signal_description;
	uint32_t width, type_id, num_dims;
	uint32_t *dims;
	void *input, *output;
	std::function<bool(void *)> before_update_callback;
	std::function<void(void *)> after_update_callback;

	port_description(
		string signal_name,
		string signal_description,
		T *input,
		T *output,
		std::function<bool(T &)> before_update_callback = std::function<bool(T &)>(),
		std::function<void(T &)> after_update_callback = std::function<void(T &)>()) :
		signal_name(signal_name),
		signal_description(signal_description),
		input(input->data()),
		output(output->data()) {
		assert(!input->IsRowMajor && !output->IsRowMajor && "Only column-major matrices are supported!");
		width = input->rows() * input->cols();
		type_id = _get_param_typeid<typename T::Scalar>();

                // Dimension treatment: Eigen has column vectors, they should only have one dimension for nicer display / handling from Python
		num_dims = input->cols() > 1 ? 2 : 1;
		dims = new uint32_t[num_dims];
		dims[0] = input->rows();
                if(num_dims > 1) {
                    dims[1] = input->cols();
		}

		if(static_cast<bool>(before_update_callback))
			this->before_update_callback = [=](void *new_param) {
				T data = Eigen::Map<T>((typename T::Scalar *)new_param);
				return before_update_callback(data);
			};
		if(static_cast<bool>(after_update_callback))
			this->after_update_callback = [=](void *new_param) {
				T data = Eigen::Map<T>((typename T::Scalar *)new_param);
				after_update_callback(data);
			};
	}

	~port_description() {
		delete[] dims;
	}
};
}  // namespace parameters
}  // namespace ln
#endif  // LINKS_AND_NODES_LN_PARAMETERS_EIGEN_H
