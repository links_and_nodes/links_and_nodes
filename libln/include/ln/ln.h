/*
  Copyright (c) 2015, DLR e.V., Florian Schmidt, Maxime Chalon
  All rights reserved.
  
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met:
  
  1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
  
  2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifdef __cplusplus
#ifndef NO_CPP_WRAPPER
#include "cppwrapper.h"
#endif
#endif

#ifndef LN_H
#define LN_H

#include <stdint.h>
#include <float.h>
#include <math.h>

#ifndef __WIN32__
#include <sys/socket.h> // scatter gather arrays -> struct iovec
#endif

#ifdef __cplusplus
extern "C" {
#endif

// #define LN_LIBRARY_VERSION 1
// #define LN_LIBRARY_VERSION 2 // client supports svn info
// #define LN_LIBRARY_VERSION 3 // client supports call_service with request data
// #define LN_LIBRARY_VERSION 4 // client supports shm buffers and async shm subscription
// #define LN_LIBRARY_VERSION 5 // client supports manager ping
// #define LN_LIBRARY_VERSION 6 // client can enable debug via manager at runtime, client automatically sends pings on win32
// #define LN_LIBRARY_VERSION 7 // provide pointer size info in registration
// #define LN_LIBRARY_VERSION 8 // log-divisor, manager_library_version
// #define LN_LIBRARY_VERSION 9 // unblock ports, service provider groups, thread pool
// #define LN_LIBRARY_VERSION 10 // unsubscribe per topic and rate!
// #define LN_LIBRARY_VERSION 11 // ln_logger_clear_topics
// #define LN_LIBRARY_VERSION 12 // service provider sockets are created on-demand, unix domain sockets, shm version 2
// #define LN_LIBRARY_VERSION 13 // register_for_service logging
// #define LN_LIBRARY_VERSION 14 // added ln_thread_pool_setschedparam
// #define LN_LIBRARY_VERSION 15 // 0.12.3
// #define LN_LIBRARY_VERSION 16 // shm version 3 & IS_GLIBC_ATLEAST_2_25
// #define LN_LIBRARY_VERSION 17 // get_message_definition_v17 with md hash in pub/sub answer
// #define LN_LIBRARY_VERSION 18 // ln_logger_direct_download
// #define LN_LIBRARY_VERSION 19 // manager host & port in register client
// #define LN_LIBRARY_VERSION 20 // more validity checks on service packets
// #define LN_LIBRARY_VERSION 21 // ln_get_message_definition_v21()
// #define LN_LIBRARY_VERSION 22 // removed some deprecated stuff
// #define LN_LIBRARY_VERSION 23 // ln_dsa_dss1_sign_message and stuff, ln_describe_message_definition
// #define LN_LIBRARY_VERSION 24 // fixed bug#429, deleyed init of multiple subscribers on same topic
#define LN_LIBRARY_VERSION 25 // add ln_remove_service_group_from_thread_pool() and shm_source_get_last_counter()

/* 
   low-level api
   all functions return 0 on success
   otherwise errno is set
*/
typedef void* shm_target_t;
int create_shm_target(shm_target_t*, const char* name, unsigned int element_size, unsigned int n_elements, unsigned int version);
int destroy_shm_target(shm_target_t*);
int open_shm_target(shm_target_t*, const char* name);
int close_shm_target(shm_target_t*);
int write_to_shm_target(shm_target_t, double ts, void* data, unsigned int len);
int get_next_buffer_from_shm_target(shm_target_t, void** buffer);

typedef void* shm_source_t;
int create_shm_source(shm_source_t*, const char* name, unsigned int element_size, unsigned int n_elements, unsigned int version);
int destroy_shm_source(shm_source_t*);
int open_shm_source(shm_source_t*, const char* name);
int shm_source_get_last_counter(shm_source_t* source_, uint32_t* counter);
int close_shm_source(shm_source_t*);
int read_from_shm_source(shm_source_t, double* ts, uint32_t* packet_counter, void* data, unsigned int len, int blocking);
int read_from_shm_source_timeout(shm_source_t, double* ts, uint32_t* packet_counter, void* data, unsigned int len, double timeout);
int read_last_from_shm_source(shm_source_t, double* ts, uint32_t* packet_counter, void* data, unsigned int len);
int block_only_on_shm_source(shm_source_t source_, uint32_t* packet_counter, double timeout);
int unblock_shm_source(shm_source_t source_);

#ifdef __ANDROID__
int get_shm_fd(void* source_or_target, unsigned int* shm_size);
#define ANDROID_FD_UNIX_SOCKET "\0ln_android_fd_unix_socket"
#endif


/*
  utility api
 */
double ln_get_time();
	
double ln_get_monotonic_time();  /* returns monotonic time where avaliable, will return 
				    value from ln_get_time() on a platform without monotonic time */
void ln_sleep_seconds(double seconds);

/*
  user-level api
  all functions return 0 on success
*/

enum ln_error_t {
	LNE_CHECK_ERRNO = 1,            /* check errno for error description */
	LNE_NO_MEM = 2,                 /* out of memory */
	LNE_NO_MANAGER_ADDRESS,         /* no --ln-manager HOST:PORT command line argument and no LN_MANAGER environment var */
	LNE_INVALID_MANAGER_ADDRESS,    /* specified ln_manager address is not in format HOST_OR_IP:PORT_NUMBER */
	LNE_INVALID_HOSTNAME,           /* invalid hostname specified - has to be a hostname or an ip */
	LNE_UNKNOWN_HOSTNAME,           /* could not resolve hostname */
	LNE_HOST_WRONG_NET,             /* host resolved to wrong network family - should be IPv4 - AF_INET */
	LNE_GOT_INVALID_RESPONSE,       /* client library received an invalid formatted response from ln_manager */
	LNE_REGISTER_DENIED,            /* ln_manager denied register-request */
	LNE_CLIENT_NOT_INITIALIZED,     /* provided client instance is not initalized! */
	LNE_REQUEST_FAILED,             /* the request to ln_manager failed / was not successfull - see ln_get_error_message() */
	LNE_FIELD_NOT_IN_HEADER,        /* the requested field name was not found in this request object */
	LNE_STRING_LENGTH_SPEC_MISSING, /* string length specification is missing in string format! */
	LNE_PORT_NOT_FOUND,             /* the specified port was not found! */
	LNE_INVALID_PORT_OBJECT,        /* the specified object is not or a wrong port object! */
	LNE_SHM_IS_TOO_SMALL,           /* the requested shared memory region is too small! */
	LNE_SHM_IS_TOO_BIG,             /* the requested shared memory region is too big! */
	LNE_SHM_DOES_NOT_EXIT,          /* the requested shared memory region does not exist! */
	LNE_PACKET_TOO_LARGE,           /* the provided packet is too large to write to this port! */
	LNE_SHM_READ_ERROR,             /* error reading from shared memory source! */
	LNE_LOST_CONNECTION,            /* lost connection to ln_manager */
	LNE_REGISTER_FAILED,            /* could not register client to manager! is a ln_daemon running on this host? */
	LNE_SVC_IS_PROVIDER,            /* this is a service provider, not a service client! */
	LNE_SVC_IS_CLIENT,              /* this is a service client, not a service provider! */
	LNE_SVC_RECV_RESP,              /* error receiving service response */
	LNE_SVC_RECV_RESP_MEM,          /* error receiving service response: can not allocate sufficiently sized buffer */
	LNE_SVC_ALREADY_REGISTERED,     /* this service already has an opened socket - is it already registered? */
	LNE_SVC_NOT_REGISTERED,         /* this service is not yet registered - it has no opened socket! */
	LNE_SVC_RECV_REQ,               /* error receiving service request */
	LNE_SVC_RECV_REQ_MEM,           /* error receiving service request: can not allocate sufficiently sized buffer */
	LNE_SVC_REQ_SET_DATA_WRONG,     /* wrong signature passed to ln_service_request_set_data() for this service! */
	LNE_SVC_HANDLER_NO_RESP,        /* programming error in service provider handler: handler returned success(==0) but did not call ln_service_request_respond()! */
	LNE_INVALID_REQ_FIELD_FORMAT,   /* init_request failed because of an invalid format character! use one of r,O,d,f! */
	LNE_USER_DEFINED_ERROR,         /* user-defined error number */
	LNE_SVC_NO_HANDLER,             /* can't register service without service-handler! use ln_service_provider_set_handler()! */
	LNE_SVC_HANDLER_EXC,            /* exception was thrown in service provider handler! */
	LNE_SVC_REQ_RUNNING,            /* there is already a service request/call running for this service! */
	LNE_SVC_REQ_NOT_RUNNING,        /* there is not async service request running! */
	LNE_SVC_REQ_ABORTED,            /* this async service request was aborted! */
	LNE_NO_PROVIDER_FOUND,          /* there is no known provider for this service/topic! */
	LNE_START_PROVIDER_FAILED,      /* there was an error starting a provider for this service/topic! */
	LNE_ERROR_GETTING_SHM_FD,       /* there was an error trying to get a fd for this shm name */
	LNE_SHM_NAME_TOO_LONG,          /* the shared memory name is too long */
	LNE_NOT_YET_IMPLEMENTED,        /* the requested feature is not yet implemented */
	LNE_NO_LOGGER_TOPICS,           /* there are no topics defined for this logger */
	LNE_INTERNAL_ERROR,             /* internal error, should not happen! */
	LNE_TOPIC_ID_OUT_OF_RANGE,      /* supplied logging_data topic_id is out of range! */
	LNE_LOG_FILE_TOO_NEW,           /* this logfile uses an unknown/too new fileformat! */
	LNE_WRONG_BYTE_ORDER,           /* data has wrong byte order. usually this means its different than the host-byte-order */
	LNE_INVALID_SHM_MAGIC,          /* invalid shm magic - recompile creator of this shm! */
	LNE_SHM_VERSION_TOO_NEW,        /* shm version too new! recompile this client! */
	LNE_WRONG_SHM_SIZE,             /* shm reports too small / invalid segment size! */
	LNE_INVALID_SIGNATURE,          /* passed signature is not a valid event signature! */
	LNE_INVALID_PARAMETER,          /* user-provided parameter is invalid! */
	LNE_SVC_RESP_LATER,             /* this service request will be answered later... */
	LNE_SVC_GRP_UNKNOWN,            /* this service group name is not known! */
	LNE_THREADP_INIT,               /* could not get thread pool! */
	LNE_NOT_ON_THIS_OS,             /* feature not avaliable on this os! */
	LNE_PORT_UNBLOCKED,             /* another thread unblocked this blocking read */
	LNE_UNKNOWN_PARAMETER,          /* unknown parameter specified */
	LNE_SVC_HANDLER_MULTI_RESP,     /* programming error in service provider handler: handler tried to call ln_service_request_respond() more than once!! */
	LNE_MANAGER_TOO_OLD,            /* the connected ln_manager is too old */
	LNE_INVALID_DSA_SIGNATURE,      /* the dss1-dsa signature is not valid for this message & public-key */
	LNE_SVC_TIMEOUT                 /* service call timed out */

};


const char* ln_format_error(int ret);

typedef void* ln_client;
/**
   connect to ln_manager specified via command line option "--ln-manager HOST:PORT"
   use LN_MANAGER env-var if there is no such option.
   will initialize ln_client at address clnt and return 0 on success.
*/
int ln_init(ln_client* clnt, const char* client_name, int argc, char** argv);

/**
   connect to specified ln_manager, only use LN_MANAGER env-var if ln_manager is empty/NULL
   will initialize ln_client at address clnt and return 0 on success.
 */
int ln_init_to_manager(ln_client* clnt, const char* client_name, const char* ln_manager);

int ln_client_get_name(ln_client clnt, char** name);
int ln_deinit(ln_client* clnt);
void ln_debug(ln_client clnt_, const char* format, ...);
int ln_ping(ln_client clnt, double timeout, double* rtt);
int ln_client_set_thread_safe(ln_client clnt, int enable);

int ln_get_remaining_args(ln_client clnt, unsigned int* n_args, char** args_ptr);

char* ln_get_error_message(ln_client clnt_); // to display the dynamically generated error message on this client connection

typedef void* ln_packet;

int ln_get_message_definition_for_topic(
	ln_client clnt, 
	const char* topic_name, 
	// outputs (strings need to be freed by application!):
	char** message_definition_name, 
	char** message_definition,
	unsigned int* message_size);
int ln_get_message_definition_for_topic_v17(
	ln_client clnt, 
	const char* topic_name, 
	// outputs (strings need to be freed by application!):
	char** message_definition_name, 
	char** message_definition,
	unsigned int* message_size,
	char** hash);
int ln_get_message_definition(
	ln_client clnt, 
	const char* message_definition_name, 
	// outputs (strings need to be freed by application!):
	char** message_definition,
	unsigned int* message_size);
int ln_get_message_definition_v17(
	ln_client clnt, 
	const char* message_definition_name, 
	// outputs (strings need to be freed by application!):
	char** message_definition,
	unsigned int* message_size,
	char** hash);
int ln_get_message_definition_v21(
	ln_client clnt,
	const char* message_definition_name,
	// outputs (strings need to be freed by application!):
	char** message_definition,
	unsigned int* message_size,
	char** hash,
	char** flat_message_definition // might return (char*)0 if manager is too old!
);

int ln_describe_message_definition(
	ln_client clnt,
	const char* message_definition_name,
	char** message_definition // \0-terminated c-string, needs to be freed by application!
);

int ln_locate_member_in_message_definition(ln_client clnt, const char* message_definition_name, const char* member, unsigned int* member_offset, unsigned int* member_byte_len);
int ln_put_message_definition(
	ln_client clnt,
	const char* message_definition_name,
	const char* message_definition,
	char** real_message_definition_name);
/**
   ln_needs_provider
   asks manager whether a service or topic of this name is already provided/published.
   if not the manager shall try to start a process which provides this service/topic
   
   timeout:
   -1 blocking
   0 non blocking
   >0 timeout in [s]
 */
int ln_needs_provider(ln_client clnt, const char* service_or_topic_name, double timeout);


/**
   subscribe a topic

   rate specified the needed update rate for this topic.
   rate == -1 means unmodified publisher rate
   rate >   0 is a given update rate in Hz
   rate == RATE_ON_REQUEST_LAST no rate-monotonic automatic background updates. each ln_read() will cause a service call
                                to the current provider of this topic. this service call is done via ln-services i.e. uses tcp
				and can not terminate in a guaranteed time.
				currently only non-blocking semantics are implemented. this means: if there is no publisher for
				this topic, ln_read() will return 0.
				if there is a provider the last published packet for this topic will be retrieved - not the next
				one!
				this is mode can not be used for synchronisation or polling.
				this is meant to be used for sporadic topic reads!
 */
typedef void* ln_inport;
#define RATE_AS_PUBLISHED    -1
#define RATE_ON_REQUEST_LAST -2
#define RATE_ON_REQUEST_NEXT -3 // not yet implemented
int ln_subscribe(ln_client clnt, const char* topic_name, const char* message_definition_name, ln_inport* port, double rate, int reliable_transport);
int ln_subscribe_with_buffers(ln_client clnt, const char* topic_name, const char* message_definition_name, ln_inport* port, double rate, int reliable_transport, unsigned int buffers);
int ln_unsubscribe(ln_inport* port);
int ln_has_publisher(ln_inport port);
/*
  ln_read returns:
  < 0 on error
  0 if there was no new packet
  1 if there was a new packet
 */
int ln_read(ln_inport port, void* packet, unsigned int size, double* timestamp, int blocking);
/*
  ln_read_timeout
  timeout parameter:
  0 - non-blocking
  >0 - blocking in s
  return value:
  0 - timeout
  1 - packet!
 */
int ln_read_timeout(ln_inport port, void* packet, unsigned int packet_size, double* ts, double timeout);
int ln_unblock(ln_inport port);

typedef void* ln_outport;
int ln_publish(ln_client clnt, const char* topic_name, const char* message_definition_name, ln_outport* port);
int ln_publish_with_buffers(ln_client clnt, const char* topic_name, const char* message_definition_name, unsigned int buffers, ln_outport* port);
int ln_unpublish(ln_outport* port);
int ln_has_subscriber(ln_outport port);
int ln_write(ln_outport port, void* packet, unsigned int size, double* timestamp); // timestamp is optinal
int ln_outport_get_next_buffer(ln_outport port, void** buffer, unsigned int* buffer_len);

// for ln_outport or ln_inport:
int ln_get_message_size(void* p_);
const char* ln_get_msg_def_hash(void* p_);

// multi waiter
typedef void* ln_multi_waiter;
int ln_multi_waiter_init(ln_client clnt, ln_multi_waiter* waiter);
int ln_multi_waiter_deinit(ln_multi_waiter* waiter);
int ln_multi_waiter_add_port(ln_multi_waiter waiter, ln_inport port); // returns waiter_id
int ln_multi_waiter_remove_port(ln_multi_waiter waiter, ln_inport port);
int ln_multi_waiter_wait(ln_multi_waiter waiter, double timeout); // waits until atleast on port can be read from
int ln_multi_waiter_can_read(ln_multi_waiter waiter, ln_inport port); // 0 - can not read, 1 - can read without blocking
#ifdef __WIN32__
#  ifndef HAVE_LN_PIPE_FD
#    define HAVE_LN_PIPE_FD
typedef void* ln_pipe_fd_t;
#  endif
#else
typedef int ln_pipe_fd_t;
#endif
int ln_multi_waiter_start_pipe_notifier_thread(ln_multi_waiter waiter, ln_pipe_fd_t* fd);
int ln_multi_waiter_ack_pipe_notification(ln_multi_waiter waiter);

// services
typedef void* ln_service;
typedef void* ln_service_request;
// user:
int ln_service_init(ln_client clnt, ln_service* svc, const char* service_name, const char* service_interface, const char* signature);
int ln_service_deinit(ln_service* svc);
int ln_service_call(ln_service svc, void* data);
int ln_service_call_with_timeout(ln_service svc, void* data, double timeout);
int ln_service_call_abort(ln_service svc); // to be called from another thread when blocking call() is running
int ln_service_callv(ln_service svc, struct iovec* iov, unsigned int iov_len, uint8_t* iov_element_lens, char** response_buffer, uint32_t* response_len, uint8_t* need_swap);
int ln_service_callv_with_timeout(ln_service svc, struct iovec* iov, unsigned int iov_len, uint8_t* iov_element_lens, char** response_buffer, uint32_t* response_len, uint8_t* need_swap, double timeout);
// async user:
int ln_service_call_async(ln_service svc, void* data, ln_service_request* req);
int ln_service_callv_async(ln_service svc, struct iovec* iov, unsigned int iov_len, uint8_t* iov_element_lens, ln_service_request* req);
int ln_service_callv_async_get_response(ln_service svc, char** response_buffer, uint32_t* response_len);
int ln_service_callv_async_get_response_data(ln_service svc, void* data); // needs full service packet! (calls _get_response())
int ln_service_request_abort(ln_service_request req); // aborts an outstanding async service call
int ln_service_request_finished(ln_service_request req, int* retval); // returns 0 if not finished, 1 if finished with retval set
int ln_service_request_get_finished_notification_fd(ln_service_request req, int* fd);

// provider
typedef enum {
	LN_NEW_SVC_CLIENT,
	LN_REMOVE_SVC_CLIENT,
	LN_NEW_SVC_FD,
	LN_REMOVE_SVC_FD
} ln_svc_client_event_t;
typedef void (*ln_service_fd_handler)(ln_client clnt, ln_svc_client_event_t event, int fd, void* user_data);
typedef int (*ln_service_handler)(ln_client clnt, ln_service_request req, void* user_data);
int ln_service_provider_set_handler(ln_service svc, ln_service_handler handler, void* user_data);
int ln_service_provider_register(ln_client clnt, ln_service svc);
int ln_service_provider_register_in_group(ln_client clnt, ln_service svc, const char* group_name);
int ln_service_provider_get_fd(ln_service svc); // pipe nofitication for new provider fd
int ln_service_provider_set_client_fd_handler(ln_service svc, ln_service_fd_handler handler, void* user_data); // per service client fd
int ln_service_request_set_data(ln_service_request req, void* data, const char* signature);
int ln_service_request_get_request_data(ln_service_request req, char** buffer, uint64_t* buffer_len);
int ln_service_request_respond(ln_service_request req);
int ln_service_request_respondv(ln_service_request req, struct iovec* iov, unsigned int iov_len);
int ln_service_request_respond_data(ln_service_request req, void* data); // needs full service packet! not only response part!
int ln_service_request_is_aborted(ln_service_request req); // returns 0 if not aborted, 1 if request was aborted.

/** wait and handle incoming requests to services of a given group.

    @param group_name name of service-group. can be NULL (which is a valid group-name)
    @param timeout how long to wait for requests in seconds. set to -1 to block without timeout.
           use a value of 0 to do a non-blocking check.
    @return error-code < 0 in case of error.
*/
int ln_wait_and_handle_service_group_requests(ln_client clnt, const char* group_name, double timeout);
int ln_handle_service_group_in_thread_pool(ln_client clnt, const char* group_name, const char* pool_name);
int ln_remove_service_group_from_thread_pool(ln_client clnt_, const char* group_name);
int ln_set_max_threads(ln_client clnt, const char* pool_name, unsigned int n_threads);
int ln_thread_pool_setschedparam(ln_client clnt, const char* pool_name, int policy, int priority, unsigned int affinity_mask);

int ln_get_service_signature(ln_client clnt, const char* service_interface, char** definition, char** signature);
int ln_get_service_signature_for_service(ln_client clnt, const char* service_name, char** service_interface, char** definition, char** signature);

int ln_get_library_version();

int ln_register_as_service_logger(ln_client clnt, const char* logger_service_name, const char* service_name_patterns_to_log);

typedef void* ln_logger;
typedef void* ln_logger_data;
int ln_client_get_logger(ln_client clnt, ln_logger* logger, const char* logger_name); // returns new or existing logger of that name
int ln_logger_add_topic(ln_logger logger, const char* topic_name, unsigned int log_size, unsigned int divisor);
int ln_logger_clear_topics(ln_logger logger);
int ln_logger_set_only_ts(ln_logger logger, int only_ts);
int ln_logger_enable(ln_logger logger);
int ln_logger_disable(ln_logger logger);
int ln_logger_download(ln_logger logger, ln_logger_data* logger_data); // returns new logger_data
int ln_logger_direct_download(ln_logger logger, ln_logger_data* logger_data); // returns new logger_data, directly connect to daemons
int ln_logger_manager_save(ln_logger logger, const char* filename, const char* format);

int ln_logger_data_save(ln_logger_data logger_data, const char* filename);
int ln_logger_data_load(ln_logger_data* logger_data, const char* filename); // returns new logger_data
int ln_logger_data_deinit(ln_logger_data* logger_data); // only needed when created with ln_logger_data_load()

#include "packed.h"
typedef struct LN_PACKED {
	double log_ts;
	double src_ts;
	uint32_t packet_counter;
	uint8_t data;
} logger_sample;
#include "endpacked.h"

struct logger_topic_;
typedef struct logger_topic_ logger_topic;

struct logger_topic_ {
	char* name;
	uint32_t log_size;
	uint32_t divisor;

	// after download avaliable:
	uint32_t n_samples;
	uint32_t sample_size;
	char* md_name;
	char* md;
	logger_sample* samples;
};
int ln_logger_data_get_info(ln_logger_data logger_data, unsigned int* n_topics);
int ln_logger_data_get_topic(ln_logger_data logger_data, unsigned int topic_id, logger_topic** topic);

int ln_client_find_services_with_interface(ln_client clnt, const char* interface_name, char** services);

typedef void* ln_event_connection;
typedef void* ln_event_call;
typedef void (*ln_event_handler)(ln_client clnt, ln_event_call call, void* user_data);
int ln_event_connect(ln_client clnt, ln_event_connection* conn, const char* event_name, const char* connect_signature, void* connect_data, ln_event_handler handler, void* user_data);
int ln_event_connect_flat(ln_client clnt, ln_event_connection* conn, const char* event_name, const char* connect_signature, void* connect_data_flat, uint32_t connect_data_flat_len, ln_event_handler handler, void* user_data);
int ln_event_connection_deinit(ln_event_connection* conn);

unsigned int ln_event_connection_get_id(ln_event_connection conn);
int ln_event_call_set_data(ln_event_call call, const char* call_signature, void* call_data);
int ln_event_call_get_buffer(ln_event_call call, void** ptr, uint32_t* len);

int ln_client_set_float_parameter(ln_client clnt, const char* name, float value);
void ln_get_v16_features(unsigned int* is_with_sys_futex, unsigned int* is_glibc_atleast_2_25);

/**
   return 0 on success, <0 on error.
   *signature_out has to be free'd by the caller if 0 is returned!
 */
int ln_dsa_dss1_sign_message(
	const char* pem_private_key_filename,
	const uint8_t* message, unsigned int message_len,
	uint8_t** signature_out, unsigned int* signature_out_len
);
/**
   return 0 on success & valid signature, <0 on error.
   -LNE_INVALID_SIGNATURE is returned if signature is invalid.
 */
int ln_dsa_dss1_verify_message_signature(
	const char* openssh_public_key_filename,
	const uint8_t* message, unsigned int message_len,
	const uint8_t* signature, unsigned int signature_len
);
int ln_dsa_dss1_verify_message_signature_memkey(
	const char* openssh_public_key_contents,
	const uint8_t* message, unsigned int message_len,
	const uint8_t* signature, unsigned int signature_len
);

#ifdef __cplusplus
}
#endif

#endif // LN_H
