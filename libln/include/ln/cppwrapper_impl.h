/*
  Copyright (c) 2015, DLR e.V., Florian Schmidt, Maxime Chalon
  All rights reserved.
  
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met:
  
  1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
  
  2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef LN_CPPWRAPPER_IMPL
#define LN_CPPWRAPPER_IMPL

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>

#include <string>
#include <vector>
#include <stdexcept>


#ifndef __QNX__
inline void ln::client::throw_error(int ret, const std::string& format, ...) {
	char msg[1024];
	va_list ap;
	va_start(ap, format);
	vsnprintf(msg, 1024, format.c_str(), ap);
	va_end(ap);
	std::string stlmsg(msg);

	char* em = ln_get_error_message(clnt);
	if(ret == -LNE_REQUEST_FAILED && clnt && em && em[0]) {
		stlmsg += ": LNE_REQUEST_FAILED:\n";
		stlmsg += em;
	} else {
		stlmsg += ": ";
		stlmsg += ln_format_error(ret);
		if(clnt && em && em[0]) {
			stlmsg += "\n";
			stlmsg += em;
		}
	}
	throw exception(stlmsg);
}
#endif

inline void ln::client::throw_error(int ret, const char* format, ...) {
	char msg[1024];
	va_list ap;
	va_start(ap, format);
	vsnprintf(msg, 1024, format, ap);
	va_end(ap);
	std::string stlmsg(msg);

	char* em = ln_get_error_message(clnt);
	if(ret == -LNE_REQUEST_FAILED && clnt && em && em[0]) {
		stlmsg += ": LNE_REQUEST_FAILED:\n";
		stlmsg += em;
	} else {
		stlmsg += ": ";
		stlmsg += ln_format_error(ret);
		if(clnt && em && em[0]) {
			stlmsg += "\n";
			stlmsg += em;
		}
	}
	throw exception(stlmsg);
}

inline void ln::client::throw_error_no_client(int ret, const char* format, ...) {
	char msg[1024];
	va_list ap;
	va_start(ap, format);
	vsnprintf(msg, 1024, format, ap);
	va_end(ap);
	std::string stlmsg(msg);
	stlmsg += ": ";
	stlmsg += ln_format_error(ret);

	throw exception(stlmsg);
}

inline void ln::client::throw_error(const char* format, ...) {
	char msg[1024];
	va_list ap;
	va_start(ap, format);
	vsnprintf(msg, 1024, format, ap);
	va_end(ap);
	throw exception(std::string(msg));
}

inline void ln::client::throw_retval_error(int retval, const char* format, ...) {
	char msg[1024];
	va_list ap;
	va_start(ap, format);
	vsnprintf(msg, 1024, format, ap);
	va_end(ap);
	std::string stl_msg(msg);
	stl_msg += ": ";
	stl_msg = strerror(retval);
	throw exception(stl_msg);
}

#if defined(__WIN32__) || defined(WIN32) || defined(WIN)
#define LN_GET_MUTEX_PTR() ((CRITICAL_SECTION*)this->priv)

inline ln::mutex_t::mutex_t() {
	priv = new CRITICAL_SECTION();
	InitializeCriticalSection(LN_GET_MUTEX_PTR());
}
inline ln::mutex_t::~mutex_t() {
	DeleteCriticalSection(LN_GET_MUTEX_PTR());
	delete LN_GET_MUTEX_PTR();
}
inline void ln::mutex_t::lock() {
	EnterCriticalSection(LN_GET_MUTEX_PTR());
}
inline void ln::mutex_t::unlock() {
	LeaveCriticalSection(LN_GET_MUTEX_PTR());
}

#else // use posix mutex
#include <pthread.h>
#define LN_GET_MUTEX_PTR() ((pthread_mutex_t*)this->priv)

inline ln::mutex_t::mutex_t() {
	priv = new pthread_mutex_t();
	int ret = pthread_mutex_init(LN_GET_MUTEX_PTR(), NULL);
	if(ret)
		ln::client::throw_retval_error(ret, "pthread_mutex_init");
}
inline ln::mutex_t::~mutex_t() {
	pthread_mutex_destroy(LN_GET_MUTEX_PTR());
	delete LN_GET_MUTEX_PTR();
}
inline void ln::mutex_t::lock() {
	int ret = pthread_mutex_lock(LN_GET_MUTEX_PTR());
	if(ret)
		ln::client::throw_retval_error(ret, "pthread_mutex_lock");
}
inline void ln::mutex_t::unlock() {
	int ret = pthread_mutex_unlock(LN_GET_MUTEX_PTR());
	if(ret)
		ln::client::throw_retval_error(ret, "pthread_mutex_unlock");	
}

#endif // LN_USE_WIN_MUTEX
#undef LN_GET_MUTEX_PTR

namespace ln {
class mutex_holder {
	mutex_t& mutex;
	bool invalid_mutex;
public:
	mutex_holder(mutex_t& mutex, bool invalid_mutex=false) : mutex(mutex), invalid_mutex(invalid_mutex) {
		if(!invalid_mutex)
			mutex.lock();
	}
	~mutex_holder() {
		if(!invalid_mutex)
			mutex.unlock();
	}
};
}

#define __as_string(what) #what
#define _as_string(what) __as_string(what)
#define LN_LIBRARY_VERSION_STR _as_string(LN_LIBRARY_VERSION)
#define LN_CLIENT_MAGIC 0x746e6c63
#define ln_clnt_hold_mutex(name) mutex_holder name(mutex, this->__client_magic != LN_CLIENT_MAGIC || this->__client_version < 13)

#define check_atleast_version(min_version) {														\
		__client_magic = LN_CLIENT_MAGIC;													\
		__client_version = LN_LIBRARY_VERSION;													\
		if(ln_get_library_version() < min_version)												\
			throw_error("this binary was compiled against newer libln version %d and needs atleast version " _as_string(min_version) "!\n"	\
				    "your libln.so is version %d\n"											\
				    "please recompile or provide LD_LIBRARY_PATH to newer libln.so!",							\
				    LN_LIBRARY_VERSION,													\
				    ln_get_library_version());												\
	}
		/*
		if(ln_client_set_float_parameter(NULL, "__LN_CPP_WRAPPER_VERSION", LN_LIBRARY_VERSION) == -1)						\
			throw_error("this binary was compiled against an older libln version %d which is incompatible against your version %d!\n"	\
				    "please recompile or provide LD_LIBRARY_PATH to older libln.so!",							\
				    LN_LIBRARY_VERSION,													\
				    ln_get_library_version());									\
	}
		*/

#define check_min_version() check_atleast_version(1)

inline ln::client::client(std::string client_name) : clnt(0), do_deinit(true) {
	check_min_version();
	int ret = ln_init(&clnt, client_name.c_str(), 0, NULL);
	if(ret)
		throw_error(ret, "ln_init");
	char* name;
	ln_client_get_name(clnt, &name);
	this->name = name;
}
inline ln::client::client(std::string client_name, int argc, char* argv[]) : clnt(0), do_deinit(true) {
	check_min_version();
	int ret = ln_init(&clnt, client_name.c_str(), argc, argv);
	if(ret)
		throw_error(ret, "ln_init");
	char* name;
	ln_client_get_name(clnt, &name);
	this->name = name;
}
inline ln::client::client(std::string client_name, std::string ln_manager) : clnt(0), do_deinit(true) {
	check_min_version();
	int ret = ln_init_to_manager(&clnt, client_name.c_str(), ln_manager.c_str());
	if(ret)
		throw_error(ret, "ln_init");
	char* name;
	ln_client_get_name(clnt, &name);
	this->name = name;
}
inline ln::client::client(ln_client clnt) : clnt(clnt), do_deinit(false) {
	check_min_version();
	char* name;
	ln_client_get_name(clnt, &name);
	this->name = name;
}

inline double ln::client::ping(double timeout) {
	double rtt;
	int ret = ln_ping(clnt, timeout, &rtt);
	if(ret)
		throw_error(ret, "ln_ping: %d", ret);
	return rtt;
}

inline std::vector<std::string> ln::client::get_remaining_args() {
	unsigned int n_args;
	char* args_ptr;
	int ret = ln_get_remaining_args(clnt, &n_args, &args_ptr);
	if(ret)
		throw_error(ret, "ln_get_remaining_args: %d", ret);
	std::vector<std::string> args;
	char* cp = args_ptr;
	for(unsigned int i = 0; i < n_args; i++) {
		args.push_back(std::string(cp));
		cp += strlen(cp) + 1;
	}
	return args;
}
      

inline ln::client::~client() {
	ln_clnt_hold_mutex(h);
	for(event_connections_t::iterator i = event_connections.begin(); i != event_connections.end(); ++i)
		delete i->second;
	for(loggers_t::iterator i = loggers.begin(); i != loggers.end(); ++i)
		delete i->second;
	for(multi_waiters_t::iterator i = multi_waiters.begin(); i != multi_waiters.end(); i++)
		delete *i;
	for(outports_t::iterator i = outports.begin(); i != outports.end(); i++)
		delete *i;
	for(inports_t::iterator i = inports.begin(); i != inports.end(); i++)
		delete *i;
	for(services_t::iterator i = services.begin(); i != services.end();) {
		delete *i; // will remove itself from the list of serivces...
		i = services.begin();
	}
	if(do_deinit)
		ln_deinit(&clnt);	
}

inline void ln::client::needs_provider(std::string service_or_topic_name, double timeout) {
	int ret = ln_needs_provider(clnt, service_or_topic_name.c_str(), timeout);
	if(ret)
		throw_error(ret, "ln_needs_provider %s: %d", service_or_topic_name.c_str(), ret);
}

inline void ln::client::get_message_definition_for_topic(std::string topic_name, 
							 std::string& message_definition_name, 
							 std::string& message_definition, 
							 unsigned int& message_size,
							 std::string& hash) {
	char* mdn;
	char* md;
	char* chash = NULL;
	int ret = ln_get_message_definition_for_topic_v17(clnt, topic_name.c_str(), &mdn, &md, &message_size, &chash);
	if(ret)
		throw_error(ret, "ln_get_message_definition_for_topic %s", topic_name.c_str());
	message_definition_name = mdn;
	message_definition = md;
	free(mdn);
	free(md);
	
	if(chash) {
		hash = chash;
		free(chash);
	} else
		hash = "";
}
inline void ln::client::get_message_definition_for_topic(std::string topic_name, 
							 std::string& message_definition_name, 
							 std::string& message_definition, 
							 unsigned int& message_size)
{
	char* mdn;
	char* md;
	int ret = ln_get_message_definition_for_topic(clnt, topic_name.c_str(), &mdn, &md, &message_size);
	if(ret)
		throw_error(ret, "ln_get_message_definition_for_topic %s", topic_name.c_str());
	message_definition_name = mdn;
	message_definition = md;
	free(mdn);
	free(md);
}

inline void ln::client::get_message_definition(std::string message_definition_name, 
					       std::string& message_definition, 
					       unsigned int& message_size,
					       std::string& hash,
					       std::string& flat_message_definition)
{
	char* md;
	char* chash = NULL;
	char* cflat_message_definition = NULL;
	int ret = ln_get_message_definition_v21(clnt, message_definition_name.c_str(), &md, &message_size, &chash, &cflat_message_definition);
	if(ret)
		throw_error(ret, "ln_get_message_definition %s", message_definition_name.c_str());
	message_definition = md;
	free(md);
	
	if(chash) {
		hash = chash;
		free(chash);
	} else
		hash = "";

	if(cflat_message_definition) {
		flat_message_definition = cflat_message_definition;
		free(cflat_message_definition);
	} else
		flat_message_definition = "";
}
inline void ln::client::get_message_definition(std::string message_definition_name,
					       std::string& message_definition,
					       unsigned int& message_size,
					       std::string& hash)
{
	char* md;
	char* chash = NULL;
	int ret = ln_get_message_definition_v17(clnt, message_definition_name.c_str(), &md, &message_size, &chash);
	if(ret)
		throw_error(ret, "ln_get_message_definition %s", message_definition_name.c_str());
	message_definition = md;
	free(md);
	
	if(chash) {
		hash = chash;
		free(chash);
	} else
		hash = "";
}
inline void ln::client::get_message_definition(std::string message_definition_name, 
					       std::string& message_definition, 
					       unsigned int& message_size)
{
	char* md;
	int ret = ln_get_message_definition(clnt, message_definition_name.c_str(), &md, &message_size);
	if(ret)
		throw_error(ret, "ln_get_message_definition %s", message_definition_name.c_str());
	message_definition = md;
	free(md);
}

inline std::string ln::client::describe_message_definition(std::string message_definition_name)
{
	char* desc_cstr;
	int ret = ln_describe_message_definition(clnt, message_definition_name.c_str(), &desc_cstr);
	if(ret)
		throw_error(ret, "ln_describe_message_definition %s", message_definition_name.c_str());
	std::string desc = desc_cstr;
	free(desc_cstr);
	return desc;
}

inline std::string ln::client::put_message_definition(std::string message_definition_name, 
					       std::string message_definition) {
	char* md_name;
	int ret = ln_put_message_definition(clnt, message_definition_name.c_str(), message_definition.c_str(), &md_name);
	if(ret)
		throw_error(ret, "ln_put_message_definition %s", message_definition_name.c_str());
	message_definition_name = md_name;
	free(md_name);
	return message_definition_name;
}

inline void ln::client::locate_member_in_message_definition(std::string message_definition_name, std::string member, unsigned int& member_offset, unsigned int& member_byte_len) {
	int ret = ln_locate_member_in_message_definition(clnt, message_definition_name.c_str(), member.c_str(), &member_offset, &member_byte_len);
	if(ret)
		throw_error(ret, "ln_locate_member_in_message_definition %s %s", message_definition_name.c_str(), member.c_str());
}



inline void ln::client::get_service_signature(std::string service_interface, 
									   std::string& service_definition, 
									   std::string& service_signature) {
	char* definition;
	char* signature;
	int ret = ln_get_service_signature(clnt, service_interface.c_str(), &definition, &signature);
	if(ret)
		throw_error(ret, "ln_get_service_signature %s", service_interface.c_str());
	service_definition = definition;
	service_signature = signature;
	free(definition);
	free(signature);
}
inline void ln::client::get_service_signature_for_service(std::string service_name, std::string& service_interface, std::string& service_definition, std::string& service_signature) {
	char* interface;
	char* definition;
	char* signature;
	int ret = ln_get_service_signature_for_service(clnt, service_name.c_str(), &interface, &definition, &signature);
	if(ret)
		throw_error(ret, "ln_get_service_signature %s", service_interface.c_str());
	service_interface = interface;
	service_definition = definition;
	service_signature = signature;
	free(interface);
	free(definition);
	free(signature);

}
inline ln::outport* ln::client::publish(std::string topic_name, std::string message_def_name, unsigned int buffers) {
	ln_clnt_hold_mutex(h);
	outport* port = new ln::outport(this, topic_name.c_str(), message_def_name.c_str(), buffers);
	outports.push_back(port);
	return port;
}
inline void ln::client::unpublish(ln::outport* port) {
	ln_clnt_hold_mutex(h);
	outports.remove(port);
	delete port;
}

inline ln::inport* ln::client::subscribe(std::string topic_name, std::string message_def_name, double rate, unsigned int buffers, bool need_reliable_transport) {
	ln_clnt_hold_mutex(h);
	inport* port = new ln::inport(this, topic_name.c_str(), message_def_name.c_str(), rate, buffers, need_reliable_transport);
	inports.push_back(port);
	return port;
}
inline void ln::client::unsubscribe(ln::inport* port) {
	ln_clnt_hold_mutex(h);
	inports.remove(port);
	delete port;
}

inline void ln::client::set_float_parameter(std::string name, float value) {
	int ret = ln_client_set_float_parameter(clnt, name.c_str(), value);
	if(ret)
		throw_error(ret, "set_float_parameter %s", name.c_str());
}

inline ln::outport::outport(ln::client* clnt, std::string topic_name, std::string message_def_name, unsigned int buffers) 
	: port_base(clnt) {
	
	int ret = ln_publish_with_buffers(clnt->clnt, topic_name.c_str(), message_def_name.c_str(), buffers, &port);
	if(ret)
		clnt->throw_error(ret, "ln_publish %s", topic_name.c_str());
	this->topic_name = topic_name;
	this->topic_md = message_def_name;
	msg_def_hash = ln_get_msg_def_hash(port);
	message_size = ln_get_message_size(port);
}
inline ln::outport::~outport() {
	ln_unpublish(&port);
}

inline void ln::outport::write(void* data) {
	int ret = ln_write(port, data, message_size, NULL);
	if(ret)
		clnt->throw_error(ret, "ln_write %s", topic_name.c_str());
}

inline void ln::outport::write(void* data, double timestamp) {
	int ret = ln_write(port, data, message_size, &timestamp);
	if(ret)
		clnt->throw_error(ret, "ln_write %s", topic_name.c_str());
}
inline void* ln::outport::get_next_buffer(unsigned int* buffer_len) {
	void* data;
	int ret = ln_outport_get_next_buffer(port, &data, buffer_len);
	if(ret)
		clnt->throw_error(ret, "ln_outport_get_next_buffer %s", topic_name.c_str());
	return data;
}

inline unsigned int ln::outport::has_subscriber() {
	return ln_has_subscriber(port);
}

inline ln::inport::inport(ln::client* clnt, std::string topic_name, std::string message_def_name, double rate, unsigned int buffers, bool need_reliable_transport) 
	: port_base(clnt) {
	
	int ret = ln_subscribe_with_buffers(clnt->clnt, topic_name.c_str(), message_def_name.c_str(), &port, rate, need_reliable_transport ? 1 : 0, buffers);
	if(ret)
		clnt->throw_error(ret, "ln_subscribe %s", topic_name.c_str());
	this->topic_name = topic_name;
	this->topic_md = message_def_name;
	message_size = ln_get_message_size(port);
	timestamp = 0;
}

inline ln::inport::~inport() {
	ln_unsubscribe(&port);	
}

inline bool ln::inport::read(void* data, bool blocking) {
	int ret = ln_read(port, data, message_size, &timestamp, blocking ? 1 : 0);
	if(ret < 0)
		clnt->throw_error(ret, "ln_read %s", topic_name.c_str());
	return ret == 1;
}

inline bool ln::inport::read(void* data, double timeout) {
	int ret = ln_read_timeout(port, data, message_size, &timestamp, timeout);
	if(ret < 0)
		clnt->throw_error(ret, "ln_read %s", topic_name.c_str());
	return ret == 1;
}

inline bool ln::inport::has_publisher() {
	if(ln_has_publisher(port))
		return true;
	return false;
}

inline void ln::inport::unblock() {
	ln_unblock(port);
}
inline ln::multi_waiter* ln::client::get_multi_waiter() {
	ln_clnt_hold_mutex(h);
	ln::multi_waiter* w = new ln::multi_waiter(this);	
	multi_waiters.push_back(w);
	return w;
}

inline ln::multi_waiter::multi_waiter(ln::client* clnt) : clnt(clnt) {
	int ret = ln_multi_waiter_init(clnt->clnt, &waiter);
	if(ret < 0)
		clnt->throw_error(ret, "ln_multi_waiter_init");
}

inline ln::multi_waiter::~multi_waiter() {
	ln_multi_waiter_deinit(&waiter);
}

inline void ln::multi_waiter::add_port(inport* port) {
	int ret = ln_multi_waiter_add_port(waiter, port->port);
	if(ret)
		clnt->throw_error(ret, "ln::multi_waiter::add_port topic_name %s", port->topic_name.c_str());
}

inline void ln::multi_waiter::remove_port(inport* port) {
	int ret = ln_multi_waiter_remove_port(waiter, port->port);
	if(ret)
		clnt->throw_error(ret, "ln::multi_waiter::remove_port topic_name %s", port->topic_name.c_str());
}

inline bool ln::multi_waiter::wait(double timeout) {
	int ret = ln_multi_waiter_wait(waiter, timeout);
	if(ret < 0)
		clnt->throw_error(ret, "ln::multi_waiter::wait");
	return ret > 0;
}

inline bool ln::multi_waiter::can_read(inport* port) {
	int ret = ln_multi_waiter_can_read(waiter, port->port);
	return ret > 0;
}

inline int ln::multi_waiter::start_pipe_notifier_thread(ln_pipe_fd_t* fd) {
	int ret = ln_multi_waiter_start_pipe_notifier_thread(waiter, fd);
	if(ret < 0)
		clnt->throw_error(ret, "ln::multi_waiter::start_pipe_notifier_thread");
	return ret;
}

inline int ln::multi_waiter::ack_pipe_notification() {
	int ret = ln_multi_waiter_ack_pipe_notification(waiter);
	if(ret < 0)
		clnt->throw_error(ret, "ln::multi_waiter::ack_pipe_notification");
	return ret;
}

inline ln::service_request::service_request(ln::service* svc, ln_service_request req)
	  : req(req), is_async_req(false), svc(svc) {

}

inline void ln::service_request::set_data(void* data, const char* signature) {
	int ret = ln_service_request_set_data(req, data, signature);
	if(ret)
		svc->clnt->throw_error(ret, "ln::service_request('%s')::set_data(data=%p, signature='%s' vs. '%s')",
				       svc->name.c_str(),
				       data, signature, svc->signature.c_str());
}

inline void ln::service_request::get_request_data(char** buffer, uint64_t* buffer_len) {
	int ret = ln_service_request_get_request_data(req, buffer, buffer_len);
	if(ret)
		svc->clnt->throw_error(ret, "ln::service_request('%s')::get_request_data()", svc->name.c_str());
}


inline int ln::service_request::respond() {
	int ret = ln_service_request_respond(req);
	if(ret)
		svc->clnt->throw_error(ret, "ln::service_request('%s')::respond()", svc->name.c_str());
	return ret;
}

inline int ln::service_request::respondv(struct iovec* iov, unsigned int iov_len) {
	int ret = ln_service_request_respondv(req, iov, iov_len);
	if(ret)
		svc->clnt->throw_error(ret, "ln::service_request('%s')::respondv()", svc->name.c_str());
	return ret;
}
inline int ln::service_request::respond_data(void* data) {
	int ret = ln_service_request_respond_data(req, data);
	if(ret)
		svc->clnt->throw_error(ret, "ln::service_request('%s')::respond_data()", svc->name.c_str());
	return ret;
}

inline bool ln::service_request::is_aborted() {
	int ret = ln_service_request_is_aborted(req);
	if(ret < 0)
		svc->clnt->throw_error(-1, "ln::service_request('%s')::is_aborted()", svc->name.c_str());
	return ret > 0;
}

inline bool ln::service_request::finished() {
	if(!is_async_req)
		svc->clnt->throw_error(-1, "ln::service_request('%s')::finished() this is NOT an async-request!", svc->name.c_str());
	int retval = 0;
	int ret = ln_service_request_finished(req, &retval);
	if(!ret)
		return false; // not yet finished
	// finished
	if(retval < 0)
		svc->clnt->throw_error(retval, "ln::service_request('%s')::finished()", svc->name.c_str());
	if(retval > 0)
		svc->clnt->throw_error(retval, "ln::service_request('%s')::finished() handler returned user defined error %d", svc->name.c_str(), retval);
	return true;
}

inline void ln::service_request::abort() {
	if(!is_async_req)
		svc->clnt->throw_error(-1, "ln::service_request('%s')::abort() this is NOT an async-request!", svc->name.c_str());
	ln_service_request_abort(req);
}

inline int ln::service_request::get_finished_notification_fd() {
	int fd;
	int ret = ln_service_request_get_finished_notification_fd(req, &fd);
	if(ret)
		svc->clnt->throw_error(ret, "ln::service_request('%s')::get_finished_notification_fd()", svc->name.c_str());
	return fd;
}

inline ln::service::service(std::string service_name, std::string service_interface, std::string signature) 
	: name(service_name), interface(service_interface), signature(signature) {
	handler = NULL;
	fd_handler = NULL;
	is_provider = true;
	async_req = NULL;
}
inline ln::service::~service() {
	// no lock, as this shall only be called by ln::~client() which already holds the lock
	if(async_req) {
		// todo: deinit async_req->req!?
		delete async_req;
	}
	clnt->services.remove(this);
	int ret = ln_service_deinit(&svc);
	if(ret)
		clnt->throw_error(ret, "ln::~service('%s')::ln_service_deinit", name.c_str());
}

inline void ln::service::_init(ln::client& clnt) {
	this->clnt = &clnt;
	int ret = ln_service_init(clnt.clnt, &svc, name.c_str(), interface.c_str(), signature.c_str());
	if(ret)
		clnt.throw_error(ret, "ln::service('%s')::_init()", name.c_str());
}


// subscribe a service
inline ln::service* ln::client::get_service(std::string service_name, std::string service_interface, std::string signature) {
	ln::service* s = new ln::service(service_name, service_interface, signature);	
	s->is_provider = false;
	s->_init(*this);
	{
		ln_clnt_hold_mutex(h);
		services.push_back(s);
	}
	return s;
}
inline void ln::service::call(void* data, double timeout) {
	int ret = ln_service_call_with_timeout(svc, data, timeout);
	if(ret < 0) // LNE_* error
		clnt->throw_error(ret, "ln::service('%s')::call() returned error %d", name.c_str(), ret);
	else if(ret > 0) // custon / user error
		clnt->throw_error(-LNE_USER_DEFINED_ERROR, "ln::service('%s')::call() returned error %d", name.c_str(), ret);
}

inline void ln::service::callv(struct iovec* iov, unsigned int iov_len, uint8_t* iov_element_lens, char** response_buffer, uint32_t* response_len, uint8_t* need_swap, double timeout) {
	int ret = ln_service_callv_with_timeout(svc, iov, iov_len, iov_element_lens, response_buffer, response_len, need_swap, timeout);
	if(ret < 0) // LNE_* error
		clnt->throw_error(ret, "ln::service('%s')::callv() returned error %d", name.c_str(), ret);
	else if(ret > 0) // custon / user error
		clnt->throw_error(-LNE_USER_DEFINED_ERROR, "ln::service('%s')::callv() returned error %d", name.c_str(), ret);
}

inline void ln::service::abort() {
	int ret = ln_service_call_abort(svc);
	if(ret)
		clnt->throw_error(ret, "ln::service('%s')::abort() returned error %d", name.c_str(), ret);
}

inline ln::service_request* ln::service::call_async(void* data) {
	ln_service_request req;
	int ret = ln_service_call_async(svc, data, &req);
	if(ret < 0) // LNE_* error
		clnt->throw_error(ret, "ln::service('%s')::call_async() returned error %d", name.c_str(), ret);
	if(!async_req) {
		async_req = new service_request(this, req);
		async_req->is_async_req = true;
	} else
		async_req->req = req;
	return async_req;
}

inline ln::service_request* ln::service::callv_async(struct iovec* iov, unsigned int iov_len, uint8_t* iov_element_lens) {
	ln_service_request req;
	int ret = ln_service_callv_async(svc, iov, iov_len, iov_element_lens, &req);
	if(ret < 0) // LNE_* error
		clnt->throw_error(ret, "ln::service('%s')::callv_async() returned error %d", name.c_str(), ret);

	if(!async_req) {
		async_req = new service_request(this, req);
		async_req->is_async_req = true;
	} else
		async_req->req = req;
	return async_req;
}

inline void ln::service::callv_async_get_response(char** response_buffer, uint32_t* response_len) {
	int ret = ln_service_callv_async_get_response(svc, response_buffer, response_len);
	if(ret < 0) // LNE_* error
		clnt->throw_error(ret, "ln::service('%s')::callv_async_get_reponse() returned error %d", name.c_str(), ret);
	else if(ret > 0) // custon / user error
		clnt->throw_error(-LNE_USER_DEFINED_ERROR, "ln::service('%s')::callv_async_get_response() returned error %d", name.c_str(), ret);
	return;
}

inline void ln::service::callv_async_get_response_data(void* data) {
	int ret = ln_service_callv_async_get_response_data(svc, data);
	if(ret < 0) // LNE_* error
		clnt->throw_error(ret, "ln::service('%s')::callv_async_get_reponse_data() returned error %d", name.c_str(), ret);
	else if(ret > 0) // custon / user error
		clnt->throw_error(-LNE_USER_DEFINED_ERROR, "ln::service('%s')::callv_async_get_response_data() returned error %d", name.c_str(), ret);
	return;
}

// provide a service
inline ln::service* ln::client::get_service_provider(std::string service_name, std::string service_interface, std::string signature) {
	ln::service* s = new ln::service(service_name, service_interface, signature);	
	s->is_provider = true;
	s->_init(*this);
	{
		ln_clnt_hold_mutex(h);
		services.push_back(s);
	}
	return s;
}
inline void ln::client::release_service(ln::service* s) {
	delete s;
}

inline void ln::service::set_client_fd_handler(ln::service::fd_handler_t handler, void* user_data) {
	if(!is_provider)
		clnt->throw_error(0, "ln::service('%s')::set_client_fd_handler() this is a service-client! you need to call ln::client::get_service_provider() to provide a service!", name.c_str());
	this->fd_handler = handler;
	this->fd_user_data = user_data;
	int ret = ln_service_provider_set_client_fd_handler(svc, &ln::service::_fd_handler, this);
	if(ret)
		clnt->throw_error(ret, "ln::service('%s')::set_client_fd_handler()", name.c_str());
}
inline void ln::service::_fd_handler(ln_client, ln_svc_client_event_t event, int fd, void* user_data) {
	ln::service* self = (ln::service*)user_data;
	if(!self->fd_handler)
		return;
	try {
		self->fd_handler(*self->clnt, event, fd, self->fd_user_data);
	} 
	catch (exception& e) {
		fprintf(stderr, "service_client_fd_handler('%s') throw:\n%s\n", self->name.c_str(), e.what());
	}
}


inline void ln::service::set_handler(ln::service::handler_t handler, void* user_data) {
	if(!is_provider)
		clnt->throw_error(0, "ln::service('%s')::set_handler() this is a service-client! you need to call ln::client::get_service_provider() to provide a service!", name.c_str());
	this->handler = handler;
	this->user_data = user_data;
}
static int ln::_service_handler(ln_client, ln_service_request req, void* user_data) {
	ln::service* self = (ln::service*)user_data;
	ln::service_request r(self, req);
	int ret;
	try {
		ret = self->handler(*self->clnt, r, self->user_data);
	} 
	catch (exception& e) {
		fprintf(stderr, "service_handler('%s') throw:\n%s\n", self->name.c_str(), e.what());
		ret = -LNE_SVC_HANDLER_EXC;
	}
	return ret;
}
inline void ln::service::do_register(const char* group_name) {
	if(!is_provider)
		clnt->throw_error(0, "ln::service('%s')::register_service_provider() this is a service-client! got create a service provider use the ln::service() constructor directly!", name.c_str());
	if(!handler)
		clnt->throw_error(0, "ln::service('%s')::register_service_provider() can't register service without service-handler! use ln::service::set_handler()!", name.c_str());
	int ret = ln_service_provider_set_handler(svc, &ln::_service_handler, this);
	if(ret)
		clnt->throw_error(ret, "ln::service('%s')::set_handler()", name.c_str());
	ret = ln_service_provider_register_in_group(clnt->clnt, svc, group_name);
	if(ret)
		clnt->throw_error(ret, "ln::service('%s')::register_service_provider()", name.c_str());
}
inline int ln::service::get_fd() {
       int ret = ln_service_provider_get_fd(svc);
       if(ret < 0)
               clnt->throw_error(ret, "ln::service('%s')::get_fd()", name.c_str());
       return ret;
}

inline void ln::client::wait_and_handle_service_group_requests(const char* group_name, double timeout) {
	int ret = ln_wait_and_handle_service_group_requests(clnt, group_name, timeout);
	if(ret < 0)
		throw_error(ret, "ln::wait_and_handle_service_group_requests()");
}

inline void ln::client::set_thread_safe(bool enable) {
	ln_client_set_thread_safe(clnt, enable ? 1 : 0);
}

inline void ln::client::register_as_service_logger(std::string logger_service_name, std::string service_name_patterns_to_log) {
	int ret = ln_register_as_service_logger(clnt, logger_service_name.c_str(), service_name_patterns_to_log.c_str());
	if(ret < 0)
		throw_error(ret, "ln::register_as_service_logger(%s, %s)",
			logger_service_name.c_str(), service_name_patterns_to_log.c_str());
}

inline void ln::client::handle_service_group_in_thread_pool(const char* group_name, std::string pool_name) {
	int ret = ln_handle_service_group_in_thread_pool(clnt, group_name, pool_name.c_str());
	if(ret < 0)
		throw_error(ret, "ln::handle_service_group_in_thread_pool()");
}
inline void ln::client::remove_service_group_from_thread_pool(const char* group_name) {
	int ret = ln_remove_service_group_from_thread_pool(clnt, group_name);
	if(ret < 0)
		throw_error(ret, "ln::remove_service_group_from_thread_pool()");
}

inline void ln::client::set_max_threads(std::string pool_name, unsigned int n_threads) {
	int ret = ln_set_max_threads(clnt, pool_name.c_str(), n_threads);
	if(ret < 0)
		throw_error(ret, "ln::set_max_threads()");
}

inline void ln::client::thread_pool_setschedparam(std::string pool_name, int policy, int priority, unsigned int affinity_mask) {
	int ret = ln_thread_pool_setschedparam(clnt, pool_name.c_str(), policy, priority, affinity_mask);
	if(ret < 0)
		throw_error(ret, "ln::thread_pool_setschedparam()");
}


/*
ln::message_definition(std::string name, std::string src) : name(name), src(src) {
	
}

void get_offset(std::string spec, unsigned int* offset) {

}
*/

inline void ln::string_buffer::_init(char** target_ptr, const std::string& src) {
	mem_len = (uint32_t*)target_ptr - 1;
	*mem_len = src.size();
	*target_ptr = mem = new char[*mem_len];
	memcpy(mem, src.c_str(), *mem_len);
}
inline ln::string_buffer::string_buffer(char** target_ptr, const std::stringstream& src) {
	_init(target_ptr, src.str());
}
inline ln::string_buffer::string_buffer(char** target_ptr, const std::string& src) {
	_init(target_ptr, src);
}
inline ln::string_buffer::~string_buffer() {
	delete[] mem;
}
inline ln::string_buffer::operator std::string() {
	return std::string(mem, *mem_len);
}

inline const char* ln::string_buffer::c_str() {
	return std::string(mem, *mem_len).c_str();
}

inline std::string ln::client::find_services_with_interface(std::string interface_name) {
	char* services;
	int ret = ln_client_find_services_with_interface(clnt, interface_name.c_str(), &services);
	if(ret)
		throw_error(ret, "ln::find_services_with_interface('%s')", interface_name.c_str());
	std::string services_(services);
	free(services);
	return services_;
}


inline ln::logger* ln::client::get_logger(std::string logger_name) {
	ln_clnt_hold_mutex(h);
	loggers_t::iterator i = loggers.find(logger_name);
	if(i != loggers.end())
		return i->second;
	ln_logger logger;
	int ret = ln_client_get_logger(clnt, &logger, logger_name.c_str());
	if(ret)
		throw_error(ret, "ln::get_logger('%s')", logger_name.c_str());
	ln::logger* l = new ln::logger(this, logger_name, logger);
	loggers[logger_name] = l;
	return l;
}

inline ln::logger::logger(ln::client* clnt, std::string logger_name, ln_logger logger) 
	: clnt(clnt), _logger(logger), name(logger_name) {
	data = NULL;
}

inline ln::logger::~logger() {
	if(data)
		delete data;
}
inline void ln::logger::set_only_ts(bool only_ts) {
	ln_logger_set_only_ts(_logger, only_ts);
}
inline void ln::logger::add_topic(std::string topic_name, unsigned int log_size, unsigned int divisor) {
	int ret = ln_logger_add_topic(_logger, topic_name.c_str(), log_size, divisor);
	if(ret)
		clnt->throw_error(ret, "ln::logger::add_topic('%s')", topic_name.c_str());
}
inline void ln::logger::clear_topics() {
	int ret = ln_logger_clear_topics(_logger);
	if(ret)
		clnt->throw_error(ret, "ln::logger::clear_topics()");
}
inline void ln::logger::enable() {
	int ret = ln_logger_enable(_logger);
	if(ret)
		clnt->throw_error(ret, "ln::logger::enable('%s')", name.c_str());
}

inline void ln::logger::disable() {
	int ret = ln_logger_disable(_logger);
	if(ret)
		clnt->throw_error(ret, "ln::logger::disable('%s')", name.c_str());
}

inline ln::logger_data* ln::logger::download() {
	ln_logger_data logger_data;
	int ret = ln_logger_download(_logger, &logger_data);
	if(ret)
		clnt->throw_error(ret, "ln::logger::download('%s')", name.c_str());
	if(!data)
		data = new ln::logger_data(this, logger_data, false);
	else
		data->set_data(logger_data, false);
	return data;
}

inline ln::logger_data* ln::logger::direct_download() {
	ln_logger_data logger_data;
	int ret = ln_logger_direct_download(_logger, &logger_data);
	if(ret)
		clnt->throw_error(ret, "ln::logger::direct_download('%s')", name.c_str());
	if(!data)
		data = new ln::logger_data(this, logger_data, false);
	else
		data->set_data(logger_data, false);
	return data;
}

inline void ln::logger::manager_save(std::string filename, std::string format) {
	int ret = ln_logger_manager_save(_logger, filename.c_str(), format.c_str());
	if(ret)
		clnt->throw_error(ret, "ln::logger::manager_save('%s')", filename.c_str());	
}

inline ln::logger_data::logger_data(ln::logger* _logger, ln_logger_data data, bool own_data)
	: _logger(_logger) {
	set_data(data, own_data);
}

inline ln::logger_data::~logger_data() {
	for(topics_t::iterator i = topics.begin(); i != topics.end(); ++i)
		delete *i;
	if(data && own_data)
		ln_logger_data_deinit(&data); // in case user loaded it with logger_data::load()
}


inline void ln::logger_data::set_data(ln_logger_data data, bool own_data) {
	this->data = data;
	this->own_data = own_data;
	for(topics_t::iterator i = topics.begin(); i != topics.end(); ++i)
		delete *i;
	topics.clear();
	unsigned int n_topics;
	ln_logger_data_get_info(data, &n_topics);
	for(unsigned int i = 0; i < n_topics; i++) {
		logger_topic* t;
		ln_logger_data_get_topic(data, i, &t);
		topics.push_back(new topic_cursor(t));
	}
}

inline void ln::logger_data::save(std::string filename) {
	int ret = ln_logger_data_save(data, filename.c_str());
	if(ret)
		_logger->clnt->throw_error(ret, "ln::logger_data::save('%s')", filename.c_str());
}

inline ln::logger_data* ln::logger_data::load(std::string filename) {
	ln_logger_data data;
	int ret = ln_logger_data_load(&data, filename.c_str());
	if(ret)
		ln::client::throw_error_no_client(ret, "ln::logger_data::load('%s')", filename.c_str());
	return new logger_data(NULL, data, true); // does not belong to any client or logger!
}

inline ln::logger_data::topic_cursor::topic_cursor(logger_topic* t) {
	topic = t;
	name = t->name;
}

inline logger_sample* ln::logger_data::topic_cursor:: get_sample(unsigned int i) {
	return (logger_sample*)((uint8_t*)topic->samples + topic->sample_size * i);
}

inline ln::event_connection* ln::client::connect_to_event(std::string event_name, std::string event_connect_signature, void* connect_data, ln::event_connection::handler_t handler, void* user_data) {
	ln::event_connection* conn = new ln::event_connection(
		this, event_name,
		event_connect_signature, connect_data, false,
		handler, user_data, 0);
	{
		ln_clnt_hold_mutex(h);
		event_connections[conn->id] = conn;
	}
	return conn;
}

inline ln::event_connection* ln::client::connect_to_event_flat(std::string event_name, std::string event_connect_signature, void* connect_data, uint32_t connect_data_len, ln::event_connection::handler_t handler, void* user_data) {
	ln::event_connection* conn = new ln::event_connection(
		this, event_name,
		event_connect_signature, connect_data, connect_data_len,
		handler, user_data, true);
	{
		ln_clnt_hold_mutex(h);
		event_connections[conn->id] = conn;
	}
	return conn;
}
inline void ln::client::disconnect_from_event(ln::event_connection* conn) {
	if(!conn || event_connections.find(conn->id) == event_connections.end())
		ln::client::throw_error_no_client(0, "ln::client::disconnect_from_event: was not connected!");
	event_connections.erase(conn->id);
	delete conn;	
}
	
inline ln::event_connection::event_connection(client* clnt, std::string event_name, std::string event_connect_signature, void* connect_data, uint32_t connect_data_len, handler_t handler, void* user_data, bool is_flat)
	: handler(handler), user_data(user_data), clnt(clnt), event_name(event_name) {

	int ret;

	if(!is_flat)
		ret = ln_event_connect(
			clnt->clnt, &conn,
			event_name.c_str(),
			event_connect_signature.c_str(), connect_data,
			_on_event, this);
	else
		ret = ln_event_connect_flat(
			clnt->clnt, &conn,
			event_name.c_str(),
			event_connect_signature.c_str(), connect_data, connect_data_len,
			_on_event, this);
	if(ret)
		clnt->throw_error(ret, "ln::client::connect_to_event('%s')", event_name.c_str());
	id = ln_event_connection_get_id(conn);
}

inline ln::event_connection::~event_connection() {
	int ret = ln_event_connection_deinit(&conn);
	if(ret)
		clnt->throw_error(ret, "ln::client::ln_event_connection_deinit('%s')", event_name.c_str());
}


inline void ln::event_connection::_on_event(ln_client /*clnt*/, ln_event_call call_, void* user_data) {
	ln::event_connection* self = (ln::event_connection*)user_data;
	ln::event_call call(self, call_);
	try {
		self->handler(call, self->user_data);
	} 
	catch (exception& e) {
		fprintf(stderr, "ln::event_connection::handler('%s') throw:\n%s\n", self->event_name.c_str(), e.what());
	}
}

inline ln::event_call::event_call(ln::event_connection* connection, ln_event_call& call)
	: connection(connection), call(call) {
	
}

inline void ln::event_call::set_data(void* host_ptr, const char* call_signature) {
	ln_event_call_set_data(call, call_signature, host_ptr);
}

inline void* ln::event_call::get_buffer(uint32_t* len) {
	void* ptr;
	int ret = ln_event_call_get_buffer(call, &ptr, len);
	if(ret)
		connection->clnt->throw_error(ret, "ln::event_call::get_buffer('%s')", connection->event_name.c_str());
	return ptr;
}

namespace ln {

inline std::string dsa_dss1_sign_message(const std::string& pem_private_key_filename, const std::string& message)
{
	uint8_t* signature_out;
	unsigned int signature_out_len;

	int err = ln_dsa_dss1_sign_message(
		pem_private_key_filename.c_str(),
		(uint8_t*)message.c_str(), message.size(),
		&signature_out, &signature_out_len);
	if(err < 0) {
		char msg[1024];
		snprintf(msg, sizeof(msg), "ln_dsa_dss1_sign_message returned %d: %s", err, ln_format_error(err));
		throw std::runtime_error(msg);
	}
	std::string ret((char*)signature_out, signature_out_len);
	free(signature_out);
	return ret;
}

namespace detail {
template<typename T>
bool dsa_dss1_verify_message_signature_fcn(
	T* fcn,
	const std::string& openssh_public_key,
	const std::string& message,
	const std::string& signature)
{
	int err = fcn(
		openssh_public_key.c_str(),
		(const uint8_t*)message.c_str(), message.size(),
		(const uint8_t*)signature.c_str(), signature.size());

	if(err == 0)
		return true;

	if(err == -LNE_INVALID_SIGNATURE)
		return false;

	char msg[1024];
	snprintf(msg, sizeof(msg), "ln_dsa_dss1_verify_message_signature returned %d: %s", err, ln_format_error(err));
	throw std::runtime_error(msg);
}
}

inline bool dsa_dss1_verify_message_signature(
	const std::string& openssh_public_key_filename,
	const std::string& message,
	const std::string& signature)
{
	return detail::dsa_dss1_verify_message_signature_fcn(
		ln_dsa_dss1_verify_message_signature,
		openssh_public_key_filename,
		message,
		signature
	);
}
inline bool dsa_dss1_verify_message_signature_memkey(
	const std::string& openssh_public_key_contents,
	const std::string& message,
	const std::string& signature)
{
	return detail::dsa_dss1_verify_message_signature_fcn(
		ln_dsa_dss1_verify_message_signature_memkey,
		openssh_public_key_contents,
		message,
		signature
	);
}

}

#endif // LN_CPPWRAPPER_IMPL
