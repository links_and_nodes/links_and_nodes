#ifndef LINKS_AND_NODES_LN_PARAMETERS_H
#define LINKS_AND_NODES_LN_PARAMETERS_H

#include <cassert>
#include <condition_variable>
#include <functional>
#include <list>
#include <map>
#include <mutex>
#include <string>
#include <vector>
#include <type_traits>

#include <ln/ln.h>

#if __cplusplus < 201103L
#warning a C++11 compiler is needed for this header!
#endif

namespace ln {
namespace parameters {

typedef enum { LN_DOUBLE, LN_SINGLE, LN_INT8, LN_UINT8, LN_INT16, LN_UINT16, LN_INT32, LN_UINT32, LN_BOOLEAN, LN_INT64, LN_UINT64 } LnParameterTypes;

struct port_description_void2 {
    const char *signal_name;
    const char *signal_description;
    uint32_t width, type_id, num_dims;
    uint32_t *dims;
    void *input, *output;
    std::function<bool(void *)> before_update_callback;
    std::function<void(void *)> after_update_callback;
};

// obsolete - left for compatibility reasons
struct port_description_void {
	const char *signal_name;
	uint32_t width, type_id, num_dims;
	uint32_t *dims;
	void *input, *output;
	std::function<bool(void *)> before_update_callback;
	std::function<void(void *)> after_update_callback;
};

template<typename T>
uint32_t _get_param_typeid() {
	if(std::is_same<T, double>::value)
		return LN_DOUBLE;
	else if(std::is_same<T, float>::value)
		return LN_SINGLE;
	else if(std::is_same<T, int8_t>::value)
		return LN_INT8;
	else if(std::is_same<T, uint8_t>::value)
		return LN_UINT8;
	else if(std::is_same<T, int16_t>::value)
		return LN_INT16;
	else if(std::is_same<T, uint16_t>::value)
		return LN_UINT16;
	else if(std::is_same<T, int32_t>::value)
		return LN_INT32;
	else if(std::is_same<T, uint32_t>::value)
		return LN_UINT32;
	else if(std::is_same<T, bool>::value)
		return LN_BOOLEAN;
	else if(std::is_same<T, int64_t>::value)
		return LN_INT64;
	else if(std::is_same<T, uint64_t>::value)
		return LN_UINT64;
	else
		return -1;
}

template<typename T, typename T2 = T>
struct port_description;

template<typename T>
struct port_description<T, typename std::enable_if<std::is_arithmetic<T>::value, T>::type> {
	std::string signal_name;
	std::string signal_description;
	uint32_t width, type_id, num_dims;
	uint32_t *dims;
	void *input, *output;
	std::function<bool(void *)> before_update_callback;
	std::function<void(void *)> after_update_callback;

	port_description(
		std::string signal_name,
		std::string signal_description,
		T *input,
		T *output,
		std::function<bool(T &)> before_update_callback = std::function<bool(T &)>(),
		std::function<void(T &)> after_update_callback = std::function<void(T &)>())
		: signal_name(signal_name),
		  signal_description(signal_description),
		  input(input),
		  output(output) {
		width = 1;
		num_dims = 1;
		type_id = _get_param_typeid<T>();
		if(static_cast<bool>(before_update_callback))
			this->before_update_callback = [=](void *new_param) { return before_update_callback(*((T *)new_param)); };
		if(static_cast<bool>(after_update_callback))
			this->after_update_callback = [=](void *new_param) { after_update_callback(*((T *)new_param)); };
	}
};

template<typename T> // support 1-dim c-style arrays
struct port_description<T,
			typename std::enable_if<
				std::is_array<T>::value &&
				std::is_arithmetic<typename std::remove_reference<decltype((*((T*)nullptr))[0])>::type>::value,
				T>::type
			> {
	std::string signal_name;
	std::string signal_description;
	uint32_t width, type_id, num_dims;
	uint32_t *dims;
	void *input, *output;
	std::function<bool(void *)> before_update_callback;
	std::function<void(void *)> after_update_callback;

	port_description(
		std::string signal_name,
		std::string signal_description,
		T *input,
		T *output,
		std::function<bool(T &)> before_update_callback = std::function<bool(T &)>(),
		std::function<void(T &)> after_update_callback = std::function<void(T &)>())
		: signal_name(signal_name),
		  signal_description(signal_description),
		  input(&(*input)[0]) {

		if(output)
			this->output = &(*output)[0];

		type_id = _get_param_typeid<typename std::remove_reference<decltype((*input)[0])>::type>();
		width = sizeof((*input)) / sizeof((*input)[0]);
		num_dims = 1;
		dims = new uint32_t[1];
		dims[0] = width;

		if(static_cast<bool>(before_update_callback))
			this->before_update_callback = [=](void* data) {
				return before_update_callback(*(T*)data);
			};
		if(static_cast<bool>(after_update_callback))
			this->after_update_callback = [=](void *data) {
				after_update_callback(*(T*)data);
			};
	}

	~port_description() {
		delete[] dims;
	}
};


class port_info;
class parameter_block;
class ln_parameter_server;

class parameter_block {
	friend class port_info;
	friend class ln_parameter_server;

  public:
	parameter_block(ln::client *clnt_, std::string parameter_group_name_, bool always_publish_ = false, std::string topic_name_ = "");

    [[deprecated("Please register_parameters with a signal description")]] bool register_parameters(std::vector<port_description_void> &port_descriptions);
    bool register_parameters(std::vector<port_description_void2> &port_descriptions);
	template<typename... Types>
	bool register_parameters(port_description<Types>... port_descriptions) {
		std::vector<port_description_void2> port_descriptions_parsed;
		_append_parameters(port_descriptions_parsed, port_descriptions...);
		bool ret = register_parameters(port_descriptions_parsed);
		assert(sizeof...(port_descriptions) == port_infos.size());
		return ret;
	}

	[[deprecated("Please pass input_signals as const void **")]] void update(void **input_signals, void **output_signals, bool only_inputs);
	void update(const void **input_signals, void **output_signals, bool only_inputs);
	void update();

	~parameter_block();

  private:
	std::string parameter_group_name;
	std::string signal_names;
	bool always_publish;
	std::string topic_name;
	typedef std::vector<port_info *> port_infos_t;
	bool parameters_registered;
	port_infos_t port_infos;

	std::mutex _mutex;
	std::condition_variable wait_cond;
	uint32_t wait_counter;
	bool wait_step;

	ln::client *clnt;

	ln::outport *outport;
	void *outport_packet;
	double no_subscriber_since;
	bool disable_outport;
	ln_parameter_server *trigger_sync_handling;
	template<typename Type1>
	void _append_parameters(
		std::vector<port_description_void2> &port_descriptions_parsed,
		port_description<Type1> &port_desc) {
		
		port_description_void2 port_description_parsed = {
			.signal_name = port_desc.signal_name.c_str(),
            .signal_description = port_desc.signal_description.c_str(),
			.width = port_desc.width,
			.type_id = port_desc.type_id,
			.num_dims = port_desc.num_dims,
			.dims = port_desc.dims,
			.input = port_desc.input,
			.output = port_desc.output,
			.before_update_callback = port_desc.before_update_callback,
			.after_update_callback = port_desc.after_update_callback
		};
		port_descriptions_parsed.push_back(port_description_parsed);
	}
	template<typename Type1, typename... Types>
	void _append_parameters(
		std::vector<port_description_void2> &port_descriptions_parsed,
		port_description<Type1> &port_desc,
		port_description<Types> &... remainder) {
		
		_append_parameters(port_descriptions_parsed, port_desc);
		_append_parameters(port_descriptions_parsed, remainder...);
	}

	std::vector<port_info *> get_matching_ports(std::string pattern);

	typedef std::pair<port_info *, std::vector<std::string>> child_list_item_t;
	typedef std::map<std::string, std::list<child_list_item_t>> childs_t;

	void _append_item(std::string name, uint32_t &offset, std::stringstream &md, std::stringstream &md_name, port_info *info);
	void _generate_child_md(uint32_t &offset, std::list<child_list_item_t> &items, std::string &md_name_);
	void _generate_md(std::string &name_);

	void publish();
	bool write();
};

}  // namespace parameters
}  // namespace ln
#endif  // LINKS_AND_NODES_LN_PARAMETERS_H
