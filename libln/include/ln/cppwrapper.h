/*
  Copyright (c) 2015, DLR e.V., Florian Schmidt, Maxime Chalon
  All rights reserved.
  
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met:
  
  1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
  
  2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef LN_CPPWRAPPER
#define LN_CPPWRAPPER

#include "ln/ln.h"

#include <string>
#include <sstream>
#include <list>
#include <vector>
#include <map>
#include <exception>

#ifdef __WIN32__
#include <windows.h>
#ifdef interface
#undef interface
#endif
#endif

namespace ln {

class client;
class outport;
class inport;
class multi_waiter;
class service;
class logger;
class logger_data;
class event_connection;
class event_call;

class mutex_t {
	void* priv;
public:
	mutex_t();
	~mutex_t();

	void lock();
	void unlock();
};
	
class exception : public std::exception {
public:
	std::string msg;
	exception(const char* msg) throw() : msg(msg) {}
	exception(const std::string msg) throw () : msg(msg) {}
	virtual ~exception() throw() {}
	virtual const char* what() const throw() { return msg.c_str(); } 
};

class event_connection {
	friend class client;

	typedef void (*handler_t)(event_call&, void* user_data);
	handler_t handler;
	void* user_data;

	event_connection(client* clnt, std::string event_name, std::string event_connect_signature, void* connect_data, uint32_t connect_data_len, handler_t handler, void* user_data, bool is_flat);
	~event_connection();

	static void _on_event(ln_client clnt, ln_event_call call, void* user_data);
public:
	client* clnt;		
	std::string event_name;
	ln_event_connection conn;
	unsigned int id;
};

class event_call {
	friend class event_connection;
	event_call(event_connection* connection, ln_event_call& call);
public:
	ln::event_connection* connection;
	ln_event_call& call;
	
	void set_data(void* host_ptr, const char* call_signature);
	void* get_buffer(uint32_t* len);
};


class client {
public:
	ln_client clnt;
	bool do_deinit;
	typedef std::list<outport*> outports_t;
	typedef std::list<inport*> inports_t;
	typedef std::list<multi_waiter*> multi_waiters_t;
	typedef std::list<service*> services_t;
	typedef std::map<std::string, logger*> loggers_t;
	typedef std::map<unsigned int, event_connection*> event_connections_t;
	outports_t outports;
	inports_t inports;
	multi_waiters_t multi_waiters;
	services_t services;
	loggers_t loggers;
	event_connections_t event_connections;

	client(std::string client_name);
	client(std::string client_name, int argc, char* argv[]);
	client(std::string client_name, std::string ln_manager);
	client(ln_client clnt);
	~client();

	std::string name;

	uint32_t __client_magic;
	uint32_t __client_version;
	mutex_t mutex;

	std::vector<std::string> get_remaining_args();

	double ping(double timeout=-1);
		
	std::string describe_message_definition(std::string message_definition_name);

	void get_message_definition(std::string message_definition_name, std::string& message_definition, unsigned int& message_size, std::string& hash, std::string& flat_message_definition);
	void get_message_definition(std::string message_definition_name, std::string& message_definition, unsigned int& message_size, std::string& hash);
	void get_message_definition(std::string message_definition_name, std::string& message_definition, unsigned int& message_size); // deprecated
	
	void get_message_definition_for_topic(std::string topic_name, std::string& message_definition_name, std::string& message_definition, unsigned int& message_size, std::string& hash);
	void get_message_definition_for_topic(std::string topic_name, std::string& message_definition_name, std::string& message_definition, unsigned int& message_size); // deprecated

	std::string put_message_definition(std::string message_definition_name, std::string message_definition);
	void get_service_signature_for_service(std::string service_name, std::string& service_interface, std::string& service_definition, std::string& service_signature);
	void get_service_signature(std::string service_interface, std::string& service_definition, std::string& service_signature);
	void locate_member_in_message_definition(std::string message_definition_name, std::string member, unsigned int& member_offset, unsigned int& member_byte_len);
		
	outport* publish(std::string topic_name, std::string message_def_name, unsigned int buffers=1);
	void unpublish(outport* port);

	/**
	   rate == -1 subscribe with publisher rate
	   rate > 0 subscribe with given rate in Hz
	   rate == -2 == RATE_ON_REQUEST_LAST meant for sporadic reads of this topic. no background udp transfer is produced.
	*/
	inport* subscribe(std::string topic_name, std::string message_def_name, double rate=-1, unsigned int buffers=1, bool need_reliable_transport=false);
	void unsubscribe(inport* port);

	multi_waiter* get_multi_waiter();

#ifndef __QNX__
	void throw_error(int ret, const std::string& format, ...);
#endif
	void throw_error(int ret, const char* format, ...);
	void throw_error(const char* format, ...);
	
	static void throw_retval_error(int retval, const char* format, ...);
	static void throw_error_no_client(int ret, const char* format, ...);

	void wait_and_handle_service_group_requests(const char* group_name, double timeout=-1);
	void handle_service_group_in_thread_pool(const char* group_name, std::string pool_name);
	void remove_service_group_from_thread_pool(const char* group_name);
	void set_max_threads(std::string pool_name, unsigned int n_threads);
	void thread_pool_setschedparam(std::string pool_name, int policy=-1, int priority=-1, unsigned int affinity_mask=0);

	/** get a service-client handle.
	    this handle can then be used to `call()` a service.
	    @returns a service-client handle
	 */
	service* get_service(std::string service_name, std::string service_interface, std::string signature);
	/** get a service-provider handle.
	    this handle can then be used to `register()` a service.
	    @returns a service-provider handle
	 */
	service* get_service_provider(std::string service_name, std::string service_interface, std::string signature);
	/** releases client resources for given service.
	    @param svc serice handle, either client or provider.
	               providers will also be de-registered with ln_manager
	 */
	void release_service(service* svc);

	void needs_provider(std::string service_or_topic_name, double timeout=-1); // -1 blocking, 0 non blocking, > 0 timeout in [s]

	logger* get_logger(std::string logger_name);

	std::string find_services_with_interface(std::string interface_name);

	event_connection* connect_to_event(std::string event_name, std::string event_connect_signature, void* connect_data, event_connection::handler_t handler, void* user_data=NULL);
	event_connection* connect_to_event_flat(std::string event_name, std::string event_connect_signature, void* connect_data_flat, uint32_t connect_data_flat_len, event_connection::handler_t handler, void* user_data=NULL);
	void disconnect_from_event(event_connection* conn);

	void set_thread_safe(bool enable);

	void register_as_service_logger(std::string logger_service_name, std::string service_name_patterns_to_log);
	void set_float_parameter(std::string name, float value);
};

class port_base {
public:
	client* clnt;
	std::string topic_name;
	std::string topic_md;
	unsigned int message_size;
	std::string msg_def_hash;

	port_base(client* clnt) : clnt(clnt) {}
};

class outport : public port_base {
	friend class client;
	outport(client* clnt, std::string topic_name, std::string message_def_name, unsigned int buffers=1);
	~outport();
public:
	ln_outport port;
		

	void write(void* data);
	void write(void* data, double timestamp);

	void* get_next_buffer(unsigned int* buffer_len);

	unsigned int has_subscriber();
};

class inport : public port_base {
	friend class client;
	inport(client* clnt, std::string topic_name, std::string message_def_name, double rate=-1, unsigned int buffers=1, bool need_reliable_transport=false);
	~inport();
public:
	ln_inport port;
	double timestamp;


	bool read(void* data, bool blocking=true); // returns true on new packet
	bool read(void* data, double timeout);

	bool has_publisher();
	void unblock();
};

class multi_waiter {
	friend class client;
	multi_waiter(client* clnt);
public:
	client* clnt;
	ln_multi_waiter waiter;

	~multi_waiter();
		
	void add_port(inport* port);
	void remove_port(inport* port);

	bool wait(double timeout=-1);
	bool can_read(inport* port);

	int start_pipe_notifier_thread(ln_pipe_fd_t* fd);
	int ack_pipe_notification();
};

static int _service_handler(ln_client clnt, ln_service_request req, void* user_data);
class service_request {
	friend int _service_handler(ln_client clnt, ln_service_request req, void* user_data);
	friend class service;
	ln_service_request req;
	bool is_async_req;

	service_request(service* svc, ln_service_request req);
public:
	service* svc;

	void set_data(void* data, const char* signature);
	void get_request_data(char** buffer, uint64_t* buffer_len);
	int respond();
	int respondv(struct iovec* iov, unsigned int iov_len);
	int respond_data(void* data);
	bool is_aborted();

	// for call_async():
	bool finished();
	void abort();
	int get_finished_notification_fd();
};

class service {
	friend class client;
	friend class service_request;
	friend int _service_handler(ln_client clnt, ln_service_request req, void* user_data);

	std::string name;
	std::string interface;
	std::string signature;
	bool is_provider;
	void _init(client& clnt);

	// async client:
	service_request* async_req;

	// provider:
	typedef int (*handler_t)(client&, service_request&, void* user_data);
	handler_t handler;
	void* user_data;
	static int _service_handler(ln_client, ln_service_request, void* user_data);
		
	typedef void (*fd_handler_t)(client&, ln_svc_client_event_t event, int fd, void* user_data);
	fd_handler_t fd_handler;
	void* fd_user_data;
	static void _fd_handler(ln_client, ln_svc_client_event_t event, int fd, void* user_data);

	service(std::string service_name, std::string service_interface, std::string signature);

public:
	~service();

	client* clnt;
	ln_service svc;

	// provider:
	void set_handler(handler_t handler, void* user_data=NULL);
	void do_register(const char* group_name=NULL);
	int get_fd();
	void set_client_fd_handler(fd_handler_t handler, void* user_data=NULL);

	// user:
	void call(void* data, double timeout=0); // data should be pointer to generated service-data struct! (contains .req and .resp fields)
	void callv(struct iovec* iov, unsigned int iov_len, uint8_t* iov_element_lens, char** response_buffer, uint32_t* response_len, uint8_t* need_swap, double timeout=0);
	void abort(); // to be called from another thread when blocking call() is running
	// async user:
	service_request* call_async(void* data);
	service_request* callv_async(struct iovec* iov, unsigned int iov_len, uint8_t* iov_element_lens);
	void callv_async_get_response(char** response_buffer, uint32_t* response_len);
	void callv_async_get_response_data(void* data);
};

/*
  class message_definition {
  public:
  std::string name;
  std::string src;
  message_definition(std::string name, std::string src);
  void get_offset(std::string spec, unsigned int* offset);
  };
*/

class string_buffer {
	uint32_t* mem_len;
	char* mem;	
		
	void _init(char** target_ptr, const std::string& src);
public:
	string_buffer(char** target_ptr, const std::stringstream& src);
	string_buffer(char** target_ptr, const std::string& src);
	~string_buffer();
		
	operator std::string();
	const char* c_str();
};

class logger {
	friend class client;
	logger(client* clnt, std::string logger_name, ln_logger logger); // get one with client::get_logger();
	~logger(); // destroyed from client
public:
	client* clnt;
	ln_logger _logger;
	std::string name;
	logger_data* data;

	void add_topic(std::string topic_name, unsigned int log_size, unsigned int divisor=1);
	void clear_topics();
	void enable();
	void disable();
		
	logger_data* download();
	logger_data* direct_download();
	void manager_save(std::string filename, std::string format="auto");

	void set_only_ts(bool only_ts);
};

class logger_data {
	friend class logger;
	logger_data(logger* _logger, ln_logger_data data, bool own_data=true); // get one with logger::download() or logger_data::load()
	void set_data(ln_logger_data data, bool own_data);
	bool own_data;
public:
	~logger_data(); // only needed when loaded with logger_data::load()

	logger* _logger;
	ln_logger_data data;
	class topic_cursor {
		friend class logger_data;
		topic_cursor(logger_topic* t); // get one from topics list
	public:
		std::string name;
		logger_topic* topic;
		logger_sample* get_sample(unsigned int i);
	};
	typedef std::list<topic_cursor*> topics_t;
	typedef topics_t::const_iterator topic_iterator;
	topics_t topics;

	void save(std::string filename);
	static logger_data* load(std::string filename);
};


std::string dsa_dss1_sign_message(const std::string& pem_private_key_filename, const std::string& message);
bool dsa_dss1_verify_message_signature(
	const std::string& openssh_public_key_filename,
	const std::string& message,
	const std::string& signature);
bool dsa_dss1_verify_message_signature_memkey(
	const std::string& openssh_public_key_contents,
	const std::string& message,
	const std::string& signature);

}

#include "ln/cppwrapper_impl.h"

#endif // LN_CPPWRAPPER


