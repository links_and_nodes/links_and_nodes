import select
import traceback
import time

class select_mainloop(object):
    IO_IN = 1
    IO_OUT = 2
    
    def __init__(self, debug=False):
        self.sources = {}
        self.next_source_id = 0
        self.io_watches = {}
        self.timeouts = {}

        self.read_fds = []
        self.write_fds = []
        
        self.stopped = False
        self.debug = debug

    def add_timeout(self, timeout_in_seconds, cb, *cbargs):
        source_id = self.next_source_id
        self.next_source_id += 1

        now = time.time()
        deadline = now + timeout_in_seconds
        
        self.timeouts[source_id] = [deadline, timeout_in_seconds, cb, cbargs, source_id]
        self.sources[source_id] = "timeout", source_id
        return source_id
        
    def add_io_watch(self, fd, why, cb, *cbargs):
        source_id = self.next_source_id
        self.next_source_id += 1
        
        self.io_watches[fd] = why, cb, cbargs, source_id
        if why & select_mainloop.IO_IN and fd not in self.read_fds:
            self.read_fds.append(fd)
        if why & select_mainloop.IO_OUT and fd not in self.write_fds:
            self.write_fds.append(fd)
        self.sources[source_id] = "io", fd
        
        return source_id

    def source_remove(self, source_id):
        if source_id not in self.sources:
            print "source_id %s not known!" % source_id
            return
        what, key = self.sources[source_id]
        del self.sources[source_id]
        if what == "io":
            if key in self.io_watches:
                why = self.io_watches[key][0]
                if why & select_mainloop.IO_IN and key in self.read_fds:
                    self.read_fds.remove(key)
                if why & select_mainloop.IO_OUT and key in self.write_fds:
                    self.write_fds.remove(key)
                del self.io_watches[key]
        if what == "timeout":
            if source_id in self.timeouts:
                del self.timeouts[source_id]

    def quit(self):
        self.stopped = True
        
    def run(self):
        while not self.stopped:

            # next timeout?
            now = time.time()
            next_timeout = None
            for to in self.timeouts.values():
                dl = to[0] - now
                if next_timeout is None or dl < next_timeout:
                    next_timeout = dl
            if next_timeout is None:
                to = None
            elif next_timeout < 0:
                next_timeout = 0

            # block for timeout or io-event
            if self.debug: print "will block on %d read_fds, %d write_fds, timeout: %r" % (len(self.read_fds), len(self.write_fds), next_timeout)
            have_read, have_write, have_exc = select.select(self.read_fds, self.write_fds, [], next_timeout)
            if self.debug: print "select unblocked with %d read_fds: %r, %d write_fds: %r" % (
                    len(have_read), have_read,
                    len(have_write), have_write)

            # handle timeouts
            now = time.time()
            expired = []
            for to in self.timeouts.values():
                if now < to[0]:
                    continue
                # expired!
                expired.append(to[-1])
            for source_id in expired:
                timeout_in_seconds, cb, cbargs, source_id = self.timeouts[source_id][1:]
                try:
                    ret = cb(*cbargs)
                except:
                    print "exception in timeout callback source_id %d:\n%s" % (source_id, traceback.format_exc())
                    ret = False
                if ret is False:
                    self.source_remove(source_id)
                elif source_id in self.timeouts:
                    self.timeouts[source_id][0] += timeout_in_seconds

            # handle io watches
            for why, src in [(select_mainloop.IO_IN, have_read), (select_mainloop.IO_OUT, have_write)]:
                for fd in src:
                    iow = self.io_watches.get(fd)
                    if iow is None:
                        continue
                    cb, cbargs, source_id = iow[1:]
                    try:
                        ret = cb(fd, why, *cbargs)
                    except:
                        print "exception in io_watch callback for fd %d:\n%s" % (fd, traceback.format_exc())
                        ret = False
                    if ret is False:
                        self.source_remove(source_id)

