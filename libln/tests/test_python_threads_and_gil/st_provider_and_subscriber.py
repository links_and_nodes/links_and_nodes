#!/usr/bin/python

import thread
import time
import sys
import links_and_nodes as ln

# create select_mainloop
from select_mainloop import select_mainloop
mainloop = select_mainloop(debug=False)

# createLN binder object
from ln_mainloop_binder import ln_mainloop_binder
ln_mainloop = ln_mainloop_binder(mainloop, debug=mainloop.debug)

clnt = ln.client("sleep_provider", sys.argv)

# provide service
def sleep_handler(call, req, resp):
    print "sleeping %.2fs" % req.sleep
    time.sleep(req.sleep)
    print "done"
    call.respond()
    return 0
svc = clnt.get_service_provider("sleep", "test/sleep")
ln_mainloop.register_service_provider(svc, sleep_handler)

# subscribe topic
def topic_handler(packet):
    print "got packet", packet
topic = sys.argv[1]
print "subscribe %r" % topic
port = clnt.subscribe(topic, "test/topic")
ln_mainloop.register_subscriber(port, topic_handler)

# publish topic
def on_timeout(port):
    #print "publish!"
    port.packet.count += 1
    port.write()
    #if port.packet.count > 30:
    #    return False
    return True
topic = sys.argv[2]
print "publish %r" % topic
port = clnt.publish(topic, "test/topic")
mainloop.add_timeout(0.05, on_timeout, port)


print "ready"
mainloop.run()

