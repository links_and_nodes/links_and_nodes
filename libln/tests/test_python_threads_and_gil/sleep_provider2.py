#!/usr/bin/python

import threading
import time
import sys
import links_and_nodes as ln

clnt = ln.client("sleep_provider", sys.argv)

def sleep_handler(call, req, resp):
    print "sleeping %.2fs" % req.sleep
    time.sleep(req.sleep)
    print "done"
    call.respond()
    return 0

svc = clnt.get_service_provider("sleep", "test/sleep")
svc.set_handler(sleep_handler)
svc.do_register() # opt arg: group_name

def threaded_work():
    print "work thread running"
    while True:
        print "working..."
        time.sleep(0.5)

thread = threading.Thread(target=threaded_work)
thread.start()

#service_handling = "in_thread_pool"
#service_handling = "in_sync_deprecated"
service_handling = "in_sync"


if service_handling == "in_thread_pool":
    # set number of threads for service handling:
    clnt.set_max_threads(None, 1) # default size: 1
    # handle all service providers without an assigned group name in thread pool "main_pool":
    clnt.handle_service_group_in_thread_pool(None, "main_pool") 

print "ready"
while True:
    print "main iteration"
    
    if service_handling == "in_thread_pool":
        # nothing todo here
        time.sleep(1)
    
    if service_handling == "in_sync":
        # preferred API, working with GIL release since 0.11.3
        clnt.wait_and_handle_service_group_requests(None, 1)
    
