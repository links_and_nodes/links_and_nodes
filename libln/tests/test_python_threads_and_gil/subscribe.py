#!/usr/bin/python

import thread
import time
import sys
import links_and_nodes as ln

topic_names = sys.argv[1:]
clnt = ln.client("subscriber", sys.argv)

def port_handler(port):
    while True:
        block_start = time.time()
        port.read(7)
        dt = time.time() - block_start
        print "%s received %d after %.2fs" % (port.topic_name, port.packet.count, dt)

threads = []
for topic in topic_names:
    port = clnt.subscribe(topic, "test/topic")
    threads.append(
        thread.start_new_thread(port_handler, (port, )))

svc = clnt.get_service("sleep", "test/sleep")

print "ready"

every = 5
every_count = 0

while True:
    block_start = time.time()
    time.sleep(2)
    dt = time.time() - block_start
    every_count += 1
    print "main iteration after %.2fs call in %d" % (dt, every - every_count)

    if every_count == every:
        every_count = 0
        svc.req.sleep = 12
        print "calling sleep service with %.2fs" % svc.req.sleep
        block_start = time.time()
        svc.call()
        dt = time.time() - block_start
        print "service returned after %.2fs" % dt
