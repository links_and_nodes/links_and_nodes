#include <stdio.h>

#include <ln.h>
#include <string_util/string_util.h>

#include "ln_messages.h"

int my_handler(ln::client& clnt, ln::service_request& req, void* user_data) {
	ln_service_mini_svc data;
	req.set_data(&data, ln_service_mini_svc_signature);
	printf("subs.msg_len: %d\n", data.req.subs.msg_len);
	printf("subs.msg: %s\n", repr(data.req.subs.msg, data.req.subs.msg_len, false).c_str());
	req.respond();
	return 0;
}

int main(int argc, char* argv[]) {
	ln::client* clnt = new ln::client(argv[0], argc, argv);

	ln::service* svc = clnt->get_service_provider("test_svc", "mini_svc", ln_service_mini_svc_signature);
	svc->set_handler(my_handler, NULL);
	svc->do_register();

	printf("ready\n");
	while(true)
		clnt->wait_and_handle_service_group_requests(NULL, 1);
	
	return 0;
}
