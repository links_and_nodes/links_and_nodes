#!/usr/bin/python

import time
import sys
import links_and_nodes as ln

topic_name, rate = sys.argv[1], sys.argv[2]
rate = int(rate)

clnt = ln.client("subscriber", sys.argv)
port1 = clnt.subscribe(topic_name, "test/topic", rate=1)
print "ready"
time.sleep(1)

port2 = clnt.subscribe(topic_name, "test/topic", rate=1)
#time.sleep(1)
port = port1
#clnt.unsubscribe(port1)
#print "press enter to read"
#raw_input()

for i in xrange(2):
#while True:
    block_start = time.time()
    port.read()
    dt = time.time() - block_start
    print "%s received %d after %.2fs" % (port.topic_name, port.packet.count[0], dt)

print "now unsubscribe 2nd!"
clnt.unsubscribe(port1)
port = port2

while True:
    block_start = time.time()
    port.read()
    dt = time.time() - block_start
    print "%s received %d after %.2fs" % (port.topic_name, port.packet.count[0], dt)
