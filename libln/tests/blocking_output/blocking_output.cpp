#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string>
#include <signal.h>
#include <unistd.h>

#include "ln.h"

using namespace std;

char line[] = "01234567890123456789012345678901234567890123456789012345678901234567890123456789\r\n"; // 80

double output_lines(unsigned int N) {
	double start = ln_get_time();

	for(unsigned int i = 0; i < N; i++)
		write(0, line, sizeof(line));

	return ln_get_time() - start;
}

int main(int argc, char* argv[]) {
	unsigned int N_lines = 100000;

	for(unsigned int i = 0; i < 5; i++) {
		double t = output_lines(N_lines);
		printf("%d lines in %.3fms (%.0f klines/s)\n", N_lines, t*1e3, N_lines / t / 1e3);

		sleep(2);
	}
	
	return 0;
}
