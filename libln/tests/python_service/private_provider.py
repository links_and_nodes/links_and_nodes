#!/usr/bin/python

import links_and_nodes as ln

import pprint
import os
import sys
from numpy import *

class provider(ln.service_provider):
    def __init__(self):
        self.clnt = ln.client(self.__class__.__name__, sys.argv)
        ln.service_provider.__init__(self, self.clnt, "test_prefix")

        self.wrap_service_provider("private_service", "private_test/private_service",
                                   preprocessors=dict(command=self.preproc_frame),
                                   postprocessor=self.postproc,
        )
        self.frame_names = dict()

    def run(self):
        self.handle_service_group_requests()

    def preproc_frame(self, provider, lnframe, field_name):
        #print "preproc lnframe:\n%s" % lnframe
        pyframe = lnframe.data.reshape((4, 4))
        self.frame_names[field_name] = lnframe.name
        return field_name, pyframe

    def postproc(self, provider, resp, ret):
        frame, name = ret
        resp.measured.data[:] = frame.flatten()
        resp.measured.name = name
        
    def private_service(self, command):
        print "received command:\n%r" % command

        measured = empty((4, 4,), dtype=float64)
        measured[:3, :3] = command[:3, :3].transpose()
        measured[:3, 3] = command[:3, 3] + 0.1
        measured[3, 3] = 1

        print "returning measured:\n%r" % measured
        
        return measured, "measured %#x" % id(measured)

if __name__ == "__main__":
    clnt = provider()
    clnt.run()
