#!/usr/bin/python

import links_and_nodes as ln

import pprint
import os
import sys

from numpy import *

class client(ln.services_wrapper):
    def __init__(self, clnt=None):
        if clnt is None:
            self.clnt = ln.client(self.__class__.__name__, sys.argv)
        else:
            self.clnt = clnt
        ln.services_wrapper.__init__(self, self.clnt, "test_prefix")

        self.wrap_service("test_service", "tests/test_service",
                          preprocessors=dict(command=self.preproc_frame),
                          postprocessors=dict(measured=self.postproc_frame)
        )

    def __del__(self):
        print "client detor"

    def destroy(self):
        self.clnt.release_service(self.services["test_prefix.test_service"].svc)
        
    def preproc_frame(self, pyframe, name, req, old_value):
        old_value.data = pyframe.flatten()
        old_value.name = "%s frame at %#x" % (name, id(pyframe))
        return old_value

    def postproc_frame(self, lnframe):
        print "frame_name: %r" % lnframe["name"]
        return lnframe["data"].reshape((4, 4))
        
    def run_test(self):
        command = zeros((4, 4), dtype=float64)
        command[0, 0] = 0.1
        command[1, 1] = 0.2
        command[2, 2] = 0.3
        command[3, 3] = 1
        command[:3, 3] = 0.11, 0.22, 0.33
        print "sending command:\n%r" % command

        measured = self.test_service(command)
        
        print "received measured:\n%r" % measured
        

if __name__ == "__main__":
    clnt = ln.client("test client", sys.argv)
    while True:
        tclnt = client(clnt)
        tclnt.run_test()

        raw_input()
        tclnt.destroy()
