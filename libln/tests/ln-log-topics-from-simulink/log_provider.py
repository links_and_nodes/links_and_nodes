#!/usr/bin/python

import sys
import time
import links_and_nodes as ln

clnt = ln.client(sys.argv[0], sys.argv[1:])
args = clnt.get_remaining_args()

log_ctrl_name = args[0]
N_samples = int(args[1])
file_base_name = args[2]
topic_names = args[3:]

logger = clnt.get_logger(log_ctrl_name) # any logger name
for topic_name in topic_names:
    logger.add_topic(topic_name, N_samples, 1)

try:
    log_ctrl = clnt.get_parameters(log_ctrl_name)
except:
    print "simulink model seems to be not yet running..."
    log_ctrl = None

print "ready"

# wait for rising edge (0->1) to start logging
is_enabled = False
last = None
while True:
    time.sleep(0.1)

    if log_ctrl is None:
        try:
            log_ctrl = clnt.get_parameters(log_ctrl_name)
            print "simlunk model now running"
        except:
            time.sleep(1)
            continue

    try:
        request = log_ctrl.get_input("request_start")
    except:
        print "simulink model disappeared..."
        log_ctrl = None
        time.sleep(1)
        continue
    
    if not is_enabled and last == 0 and request == 1:
        print "start!"
        is_enabled = True
        log_ctrl.set_override("is_logging", 1, wait_step=True)
        logger.enable()
    elif is_enabled and request == 0:
        print "stop!"
        is_enabled = False
        logger.disable()
        fn = file_base_name + "_%.0f.mat" % log_ctrl.get_input("file_number")
        print "storing to %s" % fn
        sys.stdout.flush()
        logger.manager_save(fn)        
        log_ctrl.set_override("is_logging", 0, wait_step=True)
    last = request
