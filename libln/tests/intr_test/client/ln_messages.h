#ifndef LN_MESSAGES_H
#define LN_MESSAGES_H

#include <stdint.h>

#ifdef __cplusplus
#include "ln_cppwrapper.h"
#endif
/*
  /volume/USERSTORE/schm_fl/ln_base/library/tests/intr_test/message_definitions/tests/intr_test_call
  tests/intr_test_call
*/
typedef struct __attribute__((packed)) {
	struct __attribute__((packed)) {
		uint32_t call_counter;
		uint32_t data_len;
		char* data;
	} req;
	struct __attribute__((packed)) {
		uint32_t response_counter;
		uint32_t pid;
		uint32_t data_len;
		char* data;
	} resp;
} ln_service_tests_intr_test_call;
#define ln_service_tests_intr_test_call_signature "uint32_t 4 1,uint32_t 4 1,char* 1 1|uint32_t 4 1,uint32_t 4 1,uint32_t 4 1,char* 1 1"

#ifdef __cplusplus
class ln_service_intr_test_call_base {
public:
	virtual ~ln_service_intr_test_call_base() {
#ifdef LN_UNREGISTER_SERVICE_IN_BASE_DETOR            
		unregister_intr_test_call();
#endif
	}
private:
	static int intr_test_call_cb(ln::client&, ln::service_request& req, void* user_data) {
		ln_service_intr_test_call_base* self = (ln_service_intr_test_call_base*)user_data;
		ln_service_tests_intr_test_call svc;
		req.set_data(&svc, ln_service_tests_intr_test_call_signature);
		memset(&svc.resp, 0, sizeof(svc.resp));
		return self->on_intr_test_call(req, svc);
	}
protected:
	ln::service* intr_test_call_service;
	ln_service_intr_test_call_base() : intr_test_call_service(NULL) {};
	void unregister_intr_test_call() {
		if(intr_test_call_service) {
			intr_test_call_service->clnt->release_service(intr_test_call_service);
			intr_test_call_service = NULL;
		}
	}
	void register_intr_test_call(ln::client* clnt, const std::string service_name) {
		intr_test_call_service = clnt->get_service_provider(
			service_name,
			"tests/intr_test_call", 
			ln_service_tests_intr_test_call_signature);
		intr_test_call_service->set_handler(&intr_test_call_cb, this);
		intr_test_call_service->do_register();
	}
	virtual int on_intr_test_call(ln::service_request&/* req*/, ln_service_tests_intr_test_call&/* svc*/) {
		fprintf(stderr, "ERROR: no virtual int on_intr_test_call() handler overloaded for service intr_test_call!\n");
		return 1;
	}
};
#endif
#endif // LN_MESSAGES_H
