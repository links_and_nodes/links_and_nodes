#include <stdio.h>
#include <unistd.h>

#include <string>

#include <ros/ros.h>
#include <std_msgs/Int32.h>

#include <ln.h>

using namespace std;

class job {
	ros::NodeHandle handle;
public:

};

/**
   subscribes a ros topic and published a ln topic
 */

//  : public job

template<typename ros_msg>
class topic_ros2ln {	
	ros::Subscriber ros_sub;

public:
	topic_ros2ln(string ros_topic_name, uint32_t ros_queue_len) {
		ros_sub = handle.subscribe(ros_topic_name, ros_queue_len, &topic_ros2ln::on_data, this);
	}
	void on_data(const typename ros_msg::ConstPtr& message) {
		printf("topic ros2ln\n");
	}

}

class ros2ln {
	ros::NodeHandle handle;

	list<job*> jobs;
public:
	ros2ln() {
		// jobs.push_back(new topic_ros2ln<std_msgs::Int32>("my_topic", 1));
		new topic_ros2ln<std_msgs::Int32>("my_topic", 1);
	}
	
	int run() {
		ros::SingleThreadedSpinner sp;
		sp.spin();
	}
};

int main(int argc, char* argv[]) {
	ros::init(argc, argv, "ros2ln");
	ros2ln app;
	return app.run();
}
