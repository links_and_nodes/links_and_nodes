#include <stdio.h>
#include <unistd.h>

#include <ros/ros.h>
#include <std_msgs/Int32.h>

class my_publisher {
	ros::NodeHandle handle;
	ros::Publisher pub;
public:	
	my_publisher() {
		pub = handle.advertise<std_msgs::Int32>("my_topic", 1);	
	}

	void run() {
		std_msgs::Int32 msg;
		
		msg.data = 0;
		while(true) {
			msg.data += 1;
			
			printf("publish %d\n", msg.data);
			pub.publish(msg);
			
			sleep(1);
		}
	}
};

int main(int argc, char* argv[]) {
	ros::init(argc, argv, "ros_publisher");
	my_publisher p;
	p.run();
	return 0;
}
