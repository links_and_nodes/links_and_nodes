#!/usr/bin/python

import sys
import links_and_nodes as ln

import time
import threading
import Queue

class clientA(object):
    def __init__(self):
        self.clnt = ln.client("clientA")
        print "A: have clntA at %#x" % id(self.clnt)
        
        # provider A
        self.p = self.clnt.get_service_provider("PA", "tests/binary_op")
        self.p.set_handler(self.handler)
        self.p.do_register("groupA")
        self.clnt.handle_service_group_in_thread_pool("groupA", "poolA")

        # client A
        self.c = self.clnt.get_service("PB", "tests/binary_op")
        
    def handler(self, svc_req, req, resp):
        resp.result = req.a - req.b
        svc_req.respond()

    def askB(self):
        a, b = 42, 15
        self.c.call(a=a, b=b)
        res = self.c.resp.result
        print "A: a %s + b %s = res %s" % (a, b, res) # 57
        assert(res == a + b)
        
    def destroy(self):
        self.clnt.release_service(self.c)
        print "A: did destroy service-client"
        
        self.clnt.release_service(self.p)
        print "A: did destroy service-provider"
        
        print "A: del clntA at %#x" % id(self.clnt)
        del self.clnt
        
class clientB(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.clnt = ln.client("clientB")

        # provider B
        self.p = self.clnt.get_service_provider("PB", "tests/binary_op")
        self.p.set_handler(self.handler)
        self.p.do_register("groupB")
        self.clnt.handle_service_group_in_thread_pool("groupB", "poolB")

        self.trigger = Queue.Queue()
        
        # client B
        self.c = self.clnt.get_service("PA", "tests/binary_op")
        
        self.start()

    def handler(self, svc_req, req, resp):
        resp.result = req.a + req.b
        svc_req.respond()
        self.trigger.put(None)

    def askA(self):
        a, b = 12, 78
        self.c.call(a=a, b=b)
        res = self.c.resp.result # -66
        print "B: a %s - b %s = res %s" % (a, b, res)
        assert(res == a - b)
        
    def run(self):
        while True:
            ret = self.trigger.get()
            if ret == "stop":
                break
            print "\nB: have trigger. call PA!"
            try:
                self.askA()
            except:
                if "there is no provider registered for service" in str(sys.exc_value):
                    print "B: A has already left the room!"
                    break
                raise

    def stop(self):
        self.trigger.put("stop")
        self.join()
        
if __name__ == "__main__":
    ca = clientA()
    cb = clientB()
    
    cb.askA() # warmup
    ca.askB()    
    #raw_input("press enter to start test")

    # clientA calls clientB synchronously
    ca.askB()
    # client A is being destroyed
    ca.destroy()

    cb.stop()

