#ifndef LN_MESSAGES_H
#define LN_MESSAGES_H

#include <stdint.h>

#ifdef __cplusplus
#include "ln_cppwrapper.h"
#endif
/*
  /volume/USERSTORE/schm_fl/ln_base/message_definitions/tests/sleep_service
  tests/sleep_service
*/
typedef struct __attribute__((packed)) {
	struct __attribute__((packed)) {
		double sleep_time;
	} req;
	struct __attribute__((packed)) {
		double start_time;
		double stop_time;
	} resp;
} ln_service_tests_sleep_service;
#define ln_service_tests_sleep_service_signature "double 8 1|double 8 1,double 8 1"

#ifdef __cplusplus
class ln_service_sleep_service_base {
public:
	virtual ~ln_service_sleep_service_base() {
#ifdef LN_UNREGISTER_SERVICE_IN_BASE_DETOR            
		unregister_sleep_service();
#endif
	}
private:
	static int sleep_service_cb(ln::client&, ln::service_request& req, void* user_data) {
		ln_service_sleep_service_base* self = (ln_service_sleep_service_base*)user_data;
		ln_service_tests_sleep_service svc;
		req.set_data(&svc, ln_service_tests_sleep_service_signature);
		memset(&svc.resp, 0, sizeof(svc.resp));
		return self->on_sleep_service(req, svc);
	}
protected:
	ln::service* sleep_service_service;
	ln_service_sleep_service_base() : sleep_service_service(NULL) {};
	void unregister_sleep_service() {
		if(sleep_service_service) {
			sleep_service_service->clnt->unregister_service_provider(sleep_service_service);
			sleep_service_service = NULL;
		}
	}
	void register_sleep_service(ln::client* clnt, const std::string service_name) {
		sleep_service_service = clnt->get_service_provider(
			service_name,
			"tests/sleep_service", 
			ln_service_tests_sleep_service_signature);
		sleep_service_service->set_handler(&sleep_service_cb, this);
		sleep_service_service->do_register();
	}
	virtual int on_sleep_service(ln::service_request&/* req*/, ln_service_tests_sleep_service&/* svc*/) {
		fprintf(stderr, "ERROR: no virtual int on_sleep_service() handler overloaded for service sleep_service!\n");
		return 1;
	}
};
#endif
#endif // LN_MESSAGES_H
