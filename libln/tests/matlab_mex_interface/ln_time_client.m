classdef ln_time_client
    properties
        clnt = 0;
    end
    methods
        function obj = ln_time_client(client_name, manager)
            obj.clnt = 0;
            if nargin == 0
                obj.clnt = ln_time_client_entry('new_client', 'matlab');
            elseif nargin == 1
                obj.clnt = ln_time_client_entry('new_client', client_name);
            else
                obj.clnt = ln_time_client_entry('new_client', client_name, manager);
            end
        end
        function output = get_time(obj, clock_name)
            output = ln_time_client_entry('get_time', obj.clnt, clock_name);
        end
    end
end