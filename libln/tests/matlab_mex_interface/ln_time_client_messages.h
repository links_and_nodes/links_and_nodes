#ifndef LN_TIME_CLIENT_MESSAGES_H
#define LN_TIME_CLIENT_MESSAGES_H

#include <stdint.h>

#ifdef __cplusplus
#include "ln_cppwrapper.h"
#endif
/*
  /volume/USERSTORE/schm_fl/ln_base/library/tests/matlab_mex_interface/time_provider/message_definitions/time_provider/get_time
  time_provider/get_time
*/
#include "ln_packed.h"
typedef struct LN_PACKED {
	struct LN_PACKED
#ifdef __cplusplus
		request_t
#else
		ln_service_time_provider_get_time_request_t
#endif
	{
		uint32_t clock_len;
		char* clock;
	} req;
	struct LN_PACKED
#ifdef __cplusplus
		response_t
#else
		ln_service_time_provider_get_time_response_t
#endif
	{
		uint32_t error_message_len;
		char* error_message;
		double time;
	} resp;
} ln_service_time_provider_get_time;
#include "ln_endpacked.h"
#define ln_service_time_provider_get_time_signature "uint32_t 4 1,char* 1 1|uint32_t 4 1,char* 1 1,double 8 1"

#ifdef __cplusplus
class ln_service_get_time_base {
public:
	virtual ~ln_service_get_time_base() {
#ifdef LN_UNREGISTER_SERVICE_IN_BASE_DETOR            
		unregister_get_time();
#endif
	}
private:
	static int get_time_cb(ln::client&, ln::service_request& req, void* user_data) {
		ln_service_get_time_base* self = (ln_service_get_time_base*)user_data;
		ln_service_time_provider_get_time svc;
		req.set_data(&svc, ln_service_time_provider_get_time_signature);
		memset(&svc.resp, 0, sizeof(svc.resp));
		return self->on_get_time(req, svc);
	}
protected:
	ln::service* get_time_service;
	ln_service_get_time_base() : get_time_service(NULL) {};
	void unregister_get_time() {
		if(get_time_service) {
			get_time_service->clnt->unregister_service_provider(get_time_service);
			get_time_service = NULL;
		}
	}
	void register_get_time(ln::client* clnt, const std::string service_name, const char* group_name=NULL) {
		get_time_service = clnt->get_service_provider(
			service_name,
			"time_provider/get_time", 
			ln_service_time_provider_get_time_signature);
		get_time_service->set_handler(&get_time_cb, this);
		get_time_service->do_register(group_name);
	}
	virtual int on_get_time(ln::service_request&/* req*/, ln_service_time_provider_get_time&/* svc*/) {
		fprintf(stderr, "ERROR: no virtual int on_get_time() handler overloaded for service get_time!\n");
		return 1;
	}
};
#endif
#endif // LN_TIME_CLIENT_MESSAGES_H
