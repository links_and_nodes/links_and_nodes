#!/usr/bin/python

import sys
import time
import links_and_nodes as ln

clnt = ln.client(sys.argv[0], sys.argv)
port = clnt.publish("tests.counters", "tests/counters")

port.packet.counter = 0
print "ready"
while True:
    port.write()
    port.packet.counter += 1

    time.sleep(0.1)

