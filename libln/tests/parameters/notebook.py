## nb properties ##
{'current_cell': 8, 'window_position': (2217, 43), 'window_size': (1510, 804)}
## nb properties end ##
## cell 0 input ##
## name: init
import pprint
import sys
import links_and_nodes as ln
from numpy import *
clnt = ln.client("notebook", sys.argv)
## cell 0 end ##
## cell 1 input ##
tele = clnt.get_parameters("telemetry")
## cell 1 end ##
## cell 2 input ##
tele.refresh()

## cell 2 output ##
# tele.get_input("clock"): 6320.32
# 
## cell 2 end ##
## cell 3 input ##
% tele.get_output("clock")
## cell 3 output ##
# tele.get_output("clock"): 33902.12
# 
## cell 3 end ##
## cell 4 input ##
tele.subscribe_gtk(20)
## cell 4 end ##
## cell 5 input ##
## name: test bug#114
# https://rmc-github.robotic.dlr.de/schm-fl/links_and_nodes/issues/114

require_once("init")
my_robot = clnt.get_parameters("my_robot")
my_robot.long_vector[0] = 44
## cell 5 end ##
## cell 6 input ##
v = my_robot.long_vector # as np array
#v = my_robot.long_vector.tolist() # as list -> not yet working: "parameter 'my_robot.long_vector' is of array-type while your data is a scalar!" from sfun cpp

my_robot.set_override("long_vector", v)
## cell 6 end ##
## cell 7 input ##
import time
v = random.random(3000)
N = 10
M = 100
times = []
set_printoptions(threshold=sys.maxsize)
for i in xrange(N):
    a = time.time()
    for k in xrange(M):
        # 0.9 my_robot._our_repr(v)
        # 2.3 array2string(np.asarray(v), precision=8, threshold=sys.maxsize, max_line_width=sys.maxsize, separator=",",sign="-", floatmode="maxprec")
        # 2.3 repr(v)
    times.append(time.time() - a)
% min(times)
## cell 7 output ##
# min(times): 2.30008602142
# 
## cell 7 end ##
## cell 8 input ##
# test bug #1412
for i in xrange(10):
    pp = clnt.get_parameters("test")
    % pp.get_dict()
    sleep(0.1)
## cell 8 output ##
# pp.get_dict(): {'clock_latch': 620.05000000000007, 'clock': 620.06000000000006}
# pp.get_dict(): {'clock_latch': 620.14999999999998, 'clock': 620.15999999999997}
# pp.get_dict(): {'clock_latch': 620.25, 'clock': 620.25999999999999}
# pp.get_dict(): {'clock_latch': 620.35000000000002, 'clock': 620.36000000000001}
# pp.get_dict(): {'clock_latch': 620.46000000000004, 'clock': 620.47000000000003}
# pp.get_dict(): {'clock_latch': 620.56000000000006, 'clock': 620.57000000000005}
# pp.get_dict(): {'clock_latch': 620.65999999999997, 'clock': 620.66999999999996}
# pp.get_dict(): {'clock_latch': 620.75999999999999, 'clock': 620.76999999999998}
# pp.get_dict(): {'clock_latch': 620.87, 'clock': 620.88}
# pp.get_dict(): {'clock_latch': 620.97000000000003, 'clock': 620.98000000000002}
# 
## cell 8 end ##
## cell 9 input ##
# test bug #1412
# stress test
for i in xrange(100000):
    pp = clnt.get_parameters("test")
    if i % 1000 == 0:
        % pp.get_dict()
        sleep(0.01)
## cell 9 output ##
# pp.get_dict(): {'clock_latch': 622.81000000000006, 'clock': 622.82000000000005}
# pp.get_dict(): {'clock_latch': 623.47000000000003, 'clock': 623.48000000000002}
# pp.get_dict(): {'clock_latch': 624.10000000000002, 'clock': 624.11000000000001}
# pp.get_dict(): {'clock_latch': 624.85000000000002, 'clock': 624.86000000000001}
# pp.get_dict(): {'clock_latch': 625.56000000000006, 'clock': 625.57000000000005}
# pp.get_dict(): {'clock_latch': 626.19000000000005, 'clock': 626.20000000000005}
# pp.get_dict(): {'clock_latch': 626.83000000000004, 'clock': 626.84000000000003}
# pp.get_dict(): {'clock_latch': 627.46000000000004, 'clock': 627.47000000000003}
# pp.get_dict(): {'clock_latch': 628.09000000000003, 'clock': 628.10000000000002}
# pp.get_dict(): {'clock_latch': 628.72000000000003, 'clock': 628.73000000000002}
# pp.get_dict(): {'clock_latch': 629.36000000000001, 'clock': 629.37}
# pp.get_dict(): {'clock_latch': 630.01999999999998, 'clock': 630.02999999999997}
# pp.get_dict(): {'clock_latch': 630.65999999999997, 'clock': 630.66999999999996}
# pp.get_dict(): {'clock_latch': 631.39999999999998, 'clock': 631.40999999999997}
# pp.get_dict(): {'clock_latch': 632.11000000000001, 'clock': 632.12}
# pp.get_dict(): {'clock_latch': 632.77999999999997, 'clock': 632.78999999999996}
# pp.get_dict(): {'clock_latch': 633.46000000000004, 'clock': 633.47000000000003}
# pp.get_dict(): {'clock_latch': 634.10000000000002, 'clock': 634.11000000000001}
# pp.get_dict(): {'clock_latch': 634.96000000000004, 'clock': 634.97000000000003}
# pp.get_dict(): {'clock_latch': 635.62, 'clock': 635.63}
# pp.get_dict(): {'clock_latch': 636.30000000000007, 'clock': 636.31000000000006}
# pp.get_dict(): {'clock_latch': 636.97000000000003, 'clock': 636.98000000000002}
# pp.get_dict(): {'clock_latch': 637.61000000000001, 'clock': 637.62}
# pp.get_dict(): {'clock_latch': 638.34000000000003, 'clock': 638.35000000000002}
# pp.get_dict(): {'clock_latch': 639.13, 'clock': 639.13999999999999}
# pp.get_dict(): {'clock_latch': 639.87, 'clock': 639.88}
# pp.get_dict(): {'clock_latch': 640.59000000000003, 'clock': 640.60000000000002}
# pp.get_dict(): {'clock_latch': 641.45000000000005, 'clock': 641.46000000000004}
# pp.get_dict(): {'clock_latch': 642.12, 'clock': 642.13}
# pp.get_dict(): {'clock_latch': 642.85000000000002, 'clock': 642.86000000000001}
# pp.get_dict(): {'clock_latch': 643.49000000000001, 'clock': 643.5}
# pp.get_dict(): {'clock_latch': 644.20000000000005, 'clock': 644.21000000000004}
# pp.get_dict(): {'clock_latch': 644.88, 'clock': 644.88999999999999}
# pp.get_dict(): {'clock_latch': 645.5, 'clock': 645.50999999999999}
# pp.get_dict(): {'clock_latch': 646.13, 'clock': 646.13999999999999}
# pp.get_dict(): {'clock_latch': 646.75, 'clock': 646.75999999999999}
# pp.get_dict(): {'clock_latch': 647.38999999999999, 'clock': 647.39999999999998}
# pp.get_dict(): {'clock_latch': 648.03999999999996, 'clock': 648.05000000000007}
# pp.get_dict(): {'clock_latch': 648.66999999999996, 'clock': 648.68000000000006}
# pp.get_dict(): {'clock_latch': 649.30000000000007, 'clock': 649.31000000000006}
# pp.get_dict(): {'clock_latch': 649.91999999999996, 'clock': 649.93000000000006}
# pp.get_dict(): {'clock_latch': 650.55000000000007, 'clock': 650.56000000000006}
# pp.get_dict(): {'clock_latch': 651.22000000000003, 'clock': 651.23000000000002}
# pp.get_dict(): {'clock_latch': 651.85000000000002, 'clock': 651.86000000000001}
# pp.get_dict(): {'clock_latch': 652.47000000000003, 'clock': 652.48000000000002}
# pp.get_dict(): {'clock_latch': 653.10000000000002, 'clock': 653.11000000000001}
# pp.get_dict(): {'clock_latch': 653.72000000000003, 'clock': 653.73000000000002}
# pp.get_dict(): {'clock_latch': 654.35000000000002, 'clock': 654.36000000000001}
# pp.get_dict(): {'clock_latch': 654.98000000000002, 'clock': 654.99000000000001}
# pp.get_dict(): {'clock_latch': 655.70000000000005, 'clock': 655.71000000000004}
# pp.get_dict(): {'clock_latch': 656.33000000000004, 'clock': 656.34000000000003}
# pp.get_dict(): {'clock_latch': 656.96000000000004, 'clock': 656.97000000000003}
# pp.get_dict(): {'clock_latch': 657.60000000000002, 'clock': 657.61000000000001}
# pp.get_dict(): {'clock_latch': 658.23000000000002, 'clock': 658.24000000000001}
# pp.get_dict(): {'clock_latch': 658.87, 'clock': 658.88}
# pp.get_dict(): {'clock_latch': 659.5, 'clock': 659.50999999999999}
# pp.get_dict(): {'clock_latch': 660.13999999999999, 'clock': 660.14999999999998}
# pp.get_dict(): {'clock_latch': 660.76999999999998, 'clock': 660.77999999999997}
# pp.get_dict(): {'clock_latch': 661.41999999999996, 'clock': 661.43000000000006}
# pp.get_dict(): {'clock_latch': 662.05000000000007, 'clock': 662.06000000000006}
# pp.get_dict(): {'clock_latch': 662.68000000000006, 'clock': 662.69000000000005}
# pp.get_dict(): {'clock_latch': 663.34000000000003, 'clock': 663.35000000000002}
# pp.get_dict(): {'clock_latch': 663.97000000000003, 'clock': 663.98000000000002}
# pp.get_dict(): {'clock_latch': 664.61000000000001, 'clock': 664.62}
# pp.get_dict(): {'clock_latch': 665.24000000000001, 'clock': 665.25}
# pp.get_dict(): {'clock_latch': 665.87, 'clock': 665.88}
# pp.get_dict(): {'clock_latch': 666.5, 'clock': 666.50999999999999}
# pp.get_dict(): {'clock_latch': 667.13, 'clock': 667.13999999999999}
# pp.get_dict(): {'clock_latch': 667.75999999999999, 'clock': 667.76999999999998}
# pp.get_dict(): {'clock_latch': 668.38999999999999, 'clock': 668.39999999999998}
# pp.get_dict(): {'clock_latch': 669.01999999999998, 'clock': 669.02999999999997}
# pp.get_dict(): {'clock_latch': 669.63999999999999, 'clock': 669.64999999999998}
# pp.get_dict(): {'clock_latch': 670.31000000000006, 'clock': 670.32000000000005}
# pp.get_dict(): {'clock_latch': 670.93000000000006, 'clock': 670.94000000000005}
# pp.get_dict(): {'clock_latch': 671.56000000000006, 'clock': 671.57000000000005}
# pp.get_dict(): {'clock_latch': 672.19000000000005, 'clock': 672.20000000000005}
# pp.get_dict(): {'clock_latch': 672.86000000000001, 'clock': 672.87}
# pp.get_dict(): {'clock_latch': 673.70000000000005, 'clock': 673.71000000000004}
# pp.get_dict(): {'clock_latch': 674.44000000000005, 'clock': 674.45000000000005}
# pp.get_dict(): {'clock_latch': 675.13999999999999, 'clock': 675.14999999999998}
# pp.get_dict(): {'clock_latch': 675.81000000000006, 'clock': 675.82000000000005}
# pp.get_dict(): {'clock_latch': 676.44000000000005, 'clock': 676.45000000000005}
# pp.get_dict(): {'clock_latch': 677.08000000000004, 'clock': 677.09000000000003}
# pp.get_dict(): {'clock_latch': 677.71000000000004, 'clock': 677.72000000000003}
# pp.get_dict(): {'clock_latch': 678.37, 'clock': 678.38}
# pp.get_dict(): {'clock_latch': 679.0, 'clock': 679.00999999999999}
# pp.get_dict(): {'clock_latch': 679.63, 'clock': 679.63999999999999}
# pp.get_dict(): {'clock_latch': 680.25999999999999, 'clock': 680.26999999999998}
# pp.get_dict(): {'clock_latch': 680.88999999999999, 'clock': 680.89999999999998}
# pp.get_dict(): {'clock_latch': 681.56000000000006, 'clock': 681.57000000000005}
# pp.get_dict(): {'clock_latch': 682.37, 'clock': 682.38}
# pp.get_dict(): {'clock_latch': 683.00999999999999, 'clock': 683.01999999999998}
# pp.get_dict(): {'clock_latch': 683.63999999999999, 'clock': 683.64999999999998}
# pp.get_dict(): {'clock_latch': 684.26999999999998, 'clock': 684.27999999999997}
# pp.get_dict(): {'clock_latch': 684.90999999999997, 'clock': 684.91999999999996}
# pp.get_dict(): {'clock_latch': 685.58000000000004, 'clock': 685.59000000000003}
# pp.get_dict(): {'clock_latch': 686.21000000000004, 'clock': 686.22000000000003}
# pp.get_dict(): {'clock_latch': 686.85000000000002, 'clock': 686.86000000000001}
# pp.get_dict(): {'clock_latch': 687.49000000000001, 'clock': 687.5}
# pp.get_dict(): {'clock_latch': 688.13, 'clock': 688.13999999999999}
# 
## cell 9 end ##
## cell 10 input ##
# test bug #1411
def handler(provider, req, resp):
    print "service handler. request:"
    %% req
    provider.respond()
    return 0

provider = clnt.get_service_provider("test.get_file", "tests/get_file")
provider.set_handler(handler)
provider.do_register_gobject()
## cell 10 end ##
## cell 11 input ##
clnt.release_service(provider)

## cell 11 end ##
