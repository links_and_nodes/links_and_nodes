#ifndef LN_MESSAGES_H
#define LN_MESSAGES_H

#include <stdint.h>

#ifdef __cplusplus
#include "ln_cppwrapper.h"
#endif
/*
  /volume/USERSTORE/schm_fl/ln_base/library/tests/endianess/message_definitions/endianess/test
  endianess/test 23 bytes
*/
#include "ln_packed.h"
typedef struct LN_PACKED {
	uint8_t data_uint8;
	uint16_t data_uint16;
	uint32_t data_uint32;
	uint64_t data_uint64;
	float data_float;
	float data_double;
} ln_packet_endianess_test;
#include "ln_endpacked.h"

#endif // LN_MESSAGES_H
