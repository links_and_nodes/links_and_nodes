#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <math.h>

#include <ln.h>
#include "ln_messages.h"

int main(int argc, char* argv[]) {
	ln::client clnt("my client", argc, argv);	
	
	ln::outport* port = clnt.publish("test", "endianess/test");
	ln_packet_endianess_test p;

	p.data_uint8 = 0x01;
	p.data_uint16 = 0x0203;
	p.data_uint32 = 0x04050607;
	p.data_uint64 = 0x08090a0b0c0d0e0fLL;
	p.data_float = M_PI;
	p.data_double = M_PI / 2;

	printf("ready...\n");
	while(true) {
		port->write(&p);
		// printf("sleeping...\n");
		usleep(500000);

		p.data_uint8 ++;
		p.data_uint16 ++;
		p.data_uint32 ++;
		p.data_uint64 ++;
		p.data_float += 0.1;
		p.data_double += 0.1;
	}
	return 0;
}
