#include <stdio.h>
#include <time.h>
#include <unistd.h>

#include <string>
#include <vector>

#include <ln.h>

#include "ln_messages.h"
#include <string_util/string_util.h>

using namespace std;

int main(int argc, char* argv[]) {

	ln::client clnt("service_client", argc, argv);

	ln::service* svc = clnt.get_service("all/run/publisher.request_topic", "ln/request_topic",
					    ln_service_ln_request_topic_signature);

	ln_service_ln_request_topic data;

	string mode;
	mode = "async";
	mode = "vasync";

	printf("ready...\n");
	while(true) {
		ln::string_buffer topic(&data.req.topic_name, "test");
		data.req.timeout = 0;

		double a = ln_get_time();
		if(mode == "async") {
			printf("call svc->call_async...\n");fflush(stdout);
			ln::service_request* req;
			try {
				req = svc->call_async(&data);
			}
			catch(const exception& e) {
				printf("got exception while calling call_async(): %s\n", e.what());
				usleep(10000000);
				continue;
			}
			try {
				while(!req->finished())
					usleep(100);
			}
			catch(const exception& e) {
				printf("got exception while waiting for req->finished(): %s\n", e.what());
				usleep(10000000);
				continue;
			}
		} else if(mode == "vasync") {
			printf("call svc->callv_async...\n");fflush(stdout);
			ln::service_request* req;
			const unsigned int iov_len = 3;
			struct iovec iov[iov_len];
			uint8_t iov_element_lens[iov_len];
			iov[0].iov_base = &data.req.topic_name_len;
			iov[0].iov_len = sizeof(data.req.topic_name_len);
			iov_element_lens[0] = iov[0].iov_len; // only one element
			iov[1].iov_base = data.req.topic_name;
			iov[1].iov_len = data.req.topic_name_len;
			iov_element_lens[1] = 1;
			iov[2].iov_base = &data.req.timeout;
			iov[2].iov_len = sizeof(data.req.timeout);
			iov_element_lens[2] = iov[2].iov_len;
			try {
				req = svc->callv_async(iov, iov_len, iov_element_lens);
			}
			catch(const exception& e) {
				printf("got exception while calling call_async(): %s\n", e.what());
				usleep(10000000);
				continue;
			}
			try {
				while(!req->finished())
					usleep(100);
			}
			catch(const exception& e) {
				printf("got exception while waiting for req->finished(): %s\n", e.what());
				usleep(10000000);
				continue;
			}
			printf("callv_async is finished. get response!\n"); fflush(stdout);
			
			svc->callv_async_get_response_data(&data);
			
		} else {
			printf("call svc->call...\n");fflush(stdout);			
			svc->call(&data);
		}

		double b = ln_get_time() - a;
		printf("call time: %.3fms\n", b * 1e3);
		printf("error message: %d: %s\n",
		       data.resp.error_message_len,
		       repr(string(data.resp.error_message, data.resp.error_message_len)).c_str());
		printf("data: %d: %s\n",
		       data.resp.data_len,
		       repr(string(data.resp.data, data.resp.data_len)).c_str());
		printf("counter: %d, timestamp: %.3f\n\n",
		       data.resp.counter,
		       data.resp.timestamp);

		usleep(500000);
	}
	
	return 0;
}
