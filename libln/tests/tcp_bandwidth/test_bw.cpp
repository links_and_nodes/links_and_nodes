#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

using namespace std;

int resolve_hostname(const char* hostname, struct sockaddr_in* sa) {
	// resolve hostname
	if(!hostname || hostname[0] == 0)
		return -1;

	if(isdigit(hostname[0])) {
		// printf("hostname[0] is digit: '%s'\n", hostname);
		// assume dotted decimal notation
		int ret = inet_aton(hostname, &sa->sin_addr);
#ifndef __VXWORKS__
		if(ret) {
#else
		if(ret == 0) {
#endif
			// printf("it seems to be a dotted decimal ip: %s, ret: %d\n", hostname, ret);
			sa->sin_family = AF_INET;
			return 0; // otherwise try to resolve...
		}
		printf("inet_aton failed: %d, %s\n", ret, strerror(errno));
	}/* else {
		printf("hostname[0] is not digit: '%s'\n", hostname);
	}
	 */
		
#ifdef __WIN32__
	struct addrinfo *result = NULL;

	int ret = getaddrinfo(hostname, NULL, NULL, &result);
	if(ret) {
		// error!
		debug(NULL, "resolve_hostname: gai_strerror: %s\n", gai_strerror(ret));
		return -LNE_UNKNOWN_HOSTNAME;
	}

	struct addrinfo *ptr = NULL;
	int found = 0;
    for(ptr = result; ptr != NULL; ptr = ptr->ai_next) {
        if (ptr->ai_family == AF_INET) {
			//memcpy(&sa->sin_addr, ptr->ai_addr, sizeof(sa->sin_addr));
			memcpy(&sa->sin_addr, &((struct sockaddr_in*)ptr->ai_addr)->sin_addr, sizeof(sa->sin_addr));
			sa->sin_family = AF_INET;
			found = 1;
			break;
		}
    }
    freeaddrinfo(result);
	if(!found)
		return -LNE_UNKNOWN_HOSTNAME;
#else
	// struct hostent* he = gethostbyname(hostname);
	struct addrinfo hints;
	struct addrinfo* result = NULL;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	if(getaddrinfo(hostname, NULL, &hints, &result)) {
		perror("getaddrinfo");
		return -1;
	}
	struct addrinfo *ptr = NULL;
	int found = 0;
	for(ptr = result; ptr != NULL; ptr = ptr->ai_next) {
		if (ptr->ai_family == AF_INET) {
			memcpy(&sa->sin_addr, &((struct sockaddr_in*)ptr->ai_addr)->sin_addr, sizeof(sa->sin_addr));
			sa->sin_family = AF_INET;
			found = 1;
			break;
		}
	}
	freeaddrinfo(result);
	if(!found)
		return -1;
#endif
	return 0;
}

#define BUFFER_SIZE 10240
void tcp_client(string target) {
	string::size_type pos = target.find_first_of(":");
	if(pos == string::npos) {
		printf("invalid tcp_client target spec: %s\n", target.c_str());
		return;
	}
	string hostname = target.substr(0, pos);
	int port = atoi(target.substr(pos + 1).c_str());
	printf("hostname: '%s', port: %d\n", hostname.c_str(), port);

	struct sockaddr_in server;
	server.sin_family = AF_INET;
	server.sin_port = htons(port);
	printf("resolving hostname...\n");
	if(resolve_hostname(hostname.c_str(), &server)) {
		printf("failed to resolve hostname '%s': %s\n", hostname.c_str(), strerror(errno));
		return;
	}
	printf("ip address: %s\n", inet_ntoa(server.sin_addr));

	int fd = socket(AF_INET, SOCK_STREAM, 0);
	if(fd == -1) {
		perror("socket");
		return;
	}
	printf("connecting...\n");
	if(connect(fd, (struct sockaddr*)&server, sizeof(server))) {
		perror("connect");
		return ;
	}
	printf("connected.\n");
	
	uint64_t bytes_received = 0;
	time_t last = time(NULL);
	time_t start = last;
	while(true) {
		char buffer[BUFFER_SIZE];
		int n = recv(fd, buffer, BUFFER_SIZE, 0);
		if(n == 0) {
			printf("channel closed.\n");
			return;
		}
		if(n == -1) {
			perror("recv");
			return;
		}
		bytes_received += n;
		time_t now = time(NULL);
		if(now != last) {
			last = now;
			time_t runtime = now - start;
			double bw = (double)bytes_received / runtime;
			bw /= 1024.;
			printf("bytes received: %8.3fMB, runtime: %3ds, bandwidth: %10.1fkB/s\n",
			       (double)bytes_received / 1024. / 1024.,
			       runtime,
			       bw);
		}
	}

	return;
}

void usage() {
	printf("usage: test_bw [-mode MODE] [target_spec...]\n"
	       " MODE can be one of those:\n"
	       "      tcp_client: make a tcp connection to the given target\n"
	       "                  HOSTNAME:PORT and measure receive bandwidth\n"
	       "      default: tcp_client, target_spec: localhost:54123\n"
		"\n");
}

int main(int argc, char* argv[]) {
	string mode = "tcp_client";
	string target = "localhost:54123";

	int skip = 0;
	for(int i = 1; i < argc; i++) {
		if(skip) {
			skip --;
			continue;
		}
		string arg = argv[i];
		if(arg == "-h" || arg == "--help") {
			usage();
			return 0;
		} else if(arg == "-mode" && i + 1 < argc) {
			skip = 1;
			mode = argv[i + 1];
			continue;
		} else
			target = argv[i];
	}

	if(mode == "tcp_client")
		tcp_client(target);
	else
		usage();
	return 0;
}
