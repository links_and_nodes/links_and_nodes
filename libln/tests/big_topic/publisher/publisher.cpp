#include <stdio.h>

#include "ln.h"
#include <string_util/string_util.h>

using namespace std;

void set_own_prio(int policy, int priority) {
	struct sched_param param;
	sched_getparam(0, &param);
	param.sched_priority = priority;
	if(policy == -1)
		policy = SCHED_OTHER;
	int ret = sched_setscheduler(0, policy, &param);
	if(ret) {
		fprintf(stderr, "warning: sched_setscheduler(policy=%d, priority=%d) failed with %d: %d/%s\n",
			policy, priority, ret, errno, strerror(errno));
		int minp = sched_get_priority_min(policy);
		int maxp = sched_get_priority_max(policy);
		fprintf(stderr, "allowed priority range for this policy: from %d to %d\n", minp, maxp);
	}
}

int main(int argc, char* argv[]) {
	double rate = 10;
	unsigned int topic_size = 1024;
	bool do_core_dump = false;
	
	ln::client clnt("publisher", argc, argv);
	
	vector<string> args = clnt.get_remaining_args();
	bool skip = false;
	for(vector<string>::iterator i = args.begin(); ; ) {
		if(!skip)
			++i;
		else
			skip = false;
		if(i == args.end())
			break;
		string& arg = *i;
		if(arg == "-size") {
			i++;
			if(i == args.end())
				throw ln::exception("number after -size argument missing!");
			topic_size = atoi(i->c_str());
		}
		if(arg == "-rate") {
			i++;
			if(i == args.end())
				throw ln::exception("number after -rate argument missing!");
			rate = atof(i->c_str());
		}
		if(arg == "-coredump")
			do_core_dump = true;
		if(arg == "-set-prio") {
			i++;
			if(i == args.end())
				throw ln::exception("number after -set-prio argument missing!");
			int prio = atoi(i->c_str());
			printf("try to set prio %d\n", prio);
			set_own_prio(SCHED_FIFO, prio);
		}
	}
	
	printf("topic size: %d\n", topic_size);
	string md_name = format_string("data_blob_%d", topic_size);
	string md = format_string("uint32_t data_counter1\nuint8_t data[%d]\nuint32_t data_counter2\n", topic_size - 2 * 4);
	printf("md %s: %s\n", repr(md_name).c_str(), repr(md).c_str());
	clnt.put_message_definition(md_name, md);
	
	ln::outport* port = clnt.publish("big_topic", "gen/" + md_name, 10);

	double sleep_time = 1. / rate;
	struct timespec ts = { (unsigned int)sleep_time, 0 };
	ts.tv_nsec = (unsigned int)((sleep_time - ts.tv_sec) * 1e9);

	vector<uint8_t> data(topic_size);
	unsigned int& data_counter1 = *((unsigned int*)&data[0]);
	unsigned int& data_counter2 = *((unsigned int*)(&data[0] + data.size()) - 1);	
	
	if(do_core_dump) {
		// cause coredump!
		double s = 0;
		for(unsigned int i = 0; ; i++) {
			if((i % 1000) == 0)
				printf("count: %10d, %p %d\n", i, &data[0] + i, data[i]);
			double f = data[i];
			s += f;
		}
	}

	printf("ready...\n");
	data_counter1 = 0;	
	while(true) {
		
		data_counter1 ++;
		data_counter2 = data_counter1;
		
		port->write(&data[0]);
		
		nanosleep(&ts, NULL);
	}
	
	return 0;
}
