#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#include "ln.h"

#ifdef __WIN32__
#include <windows.h>
struct timespec {
	time_t tv_sec;
	long tv_nsec;
};
void nanosleep(struct timespec* ts, struct timespec* tsout) {
	double ms = ts->tv_sec * 1e3 + ts->tv_nsec / 1e6;
	Sleep(ms);
}
#endif

int main(int argc, char* argv[]) {
	// parse arguments
	// default values
	char topic[255];
	char message[255];
	float rate = 100;
	int repeat = -1;

	strncpy(topic, "default_topic", 255);
	strncpy(message, "default_message", 255);
	
	int skip = 0;
	for(int i = 1; i < argc; i++) {
		if(skip) {
			skip --;
			continue;
		}

		if(!strncmp(argv[i], "-ln_", 4)) {
			skip = 1;
			continue;
		} else if((i + 1) < argc) {
			skip = 1;
			if(!strcmp(argv[i], "-topic")) {
				strncpy(topic, argv[i + 1], 255);

			} else if(!strcmp(argv[i], "-rate")){
				rate = atof(argv[i + 1]);

			} else if(!strcmp(argv[i], "-message")) {
				strncpy(message, argv[i + 1], 255);

			} else if(!strcmp(argv[i], "-repeat")) {
				repeat = atoi(argv[i + 1]);

			} else 
				skip = 0; // not a 2 arg option!
			if(skip)
				continue;
		}
		printf("invalid option %d: %s\n", i + 1, argv[i]);
		return -1;
	}

	int ret;
	ln_client clnt;
	if((ret = ln_init(&clnt, "test publisher", argc, argv))) {
		printf("could not init ln_client. returned error %d: %s\n", -ret, ln_format_error(ret));
		if(clnt)
			printf("dynamic error: %s\n", ln_get_error_message(clnt));
				
		return -1;
	}

	while(1) {
		ln_outport my_port;
		int size;

		// register publish/output-port
		if((ret = ln_publish(clnt, topic, message, &my_port))) {
			printf("ln_publish error %d: %s\n", -ret, ln_format_error(ret));
			if(ln_get_error_message(clnt))
				printf("dynamic error: %s\n", ln_get_error_message(clnt));
			break;
		}

		size = ln_get_message_size(my_port);
		char* my_data = (char*)malloc(size);

		// publish/write to port
		double seconds = 1. / rate;
		struct timespec ts = {(int)seconds, (seconds - (int)seconds) * 1e9};
		bool had_error = false;
		for(int i = 0; i < repeat || repeat == -1; i++) {
			*((unsigned int*)my_data) = i;
		
			printf("write %d, has_subscriber: %d\n", i, ln_has_subscriber(my_port));
			if((ret = ln_write(my_port, my_data, size, NULL))) {
				printf("ln_write error %d: %s\n", -ret, ln_format_error(ret));
				if(ln_get_error_message(clnt))
					printf("%s\n", ln_get_error_message(clnt));
				had_error = true;
				break;
			}
		
			nanosleep(&ts, NULL);
		}

		free(my_data);

		if((ret = ln_unpublish(&my_port))) {
			printf("ln_unpublish error %d: %s\n", -ret, ln_format_error(ret));
			if(ln_get_error_message(clnt))
				printf("dynamic error: %s\n", ln_get_error_message(clnt));
		}
		if(!had_error)
			printf("normal exit of publisher!\n");
		break;
	}

	printf("lnc exiting\n");
	ln_deinit(&clnt);

	return 0;
}
