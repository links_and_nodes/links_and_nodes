#include <stdio.h>
#include <ln.h>
#include <string_util/string_util.h>

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>


#include "ln_messages.h"

#define retval_check(function, args...) {						\
		int ret = function(args);						\
		if(ret)									\
			throw retval_exception_tb(ret, #function "(" #args ")");        \
	}



void wait_for_event_on_fd(int fd) {
	while(true) {
		struct timeval tv = { 0, 1000000 };
		fd_set read_fds;
		FD_ZERO(&read_fds);
		FD_SET(fd, &read_fds);
		int n = select(fd + 1, &read_fds, NULL, NULL, &tv);
		
		if(n == 1)
			return;
		
		if(n == -1) {
			if(errno == EINTR) {
				printf("interrupted...\n");
				continue;
			}
			throw retval_exception_tb(n, "select");
		}
		if(n == 0) {
			printf("timeout!?\n");
			// return; // FALSCH!!!
		}
	}
	
}


int main(int argc, char* argv[]) {
	ln_client clnt;

	retval_check(ln_init, &clnt, "ich", argc, argv);

	ln_service svc;
	retval_check(ln_service_init, clnt, &svc, "sleeper", "tests/sleep_service", tests_sleep_service_signature);
	printf("connected...\n");

	tests_sleep_service_t data;
	memset(&data, 0, sizeof(data));
	data.req.sleep_time = 0.1;

	ln_service_request req;
	retval_check(ln_service_call_async, svc, &data, &req);
	printf("call started...\n");

	
	// mein zeug machen
	sleep(2);
	printf("finished my stuff, wait until finished...\n");
	
	int req_fd;
	retval_check(ln_service_request_get_finished_notification_fd, req, &req_fd);

	// warten bis der req fertig ist
	wait_for_event_on_fd(req_fd);
	printf("should be finished now\n");

	int retval;
	int is_finished = ln_service_request_finished(req, &retval);
	if(is_finished == 0) {
		printf("req not finished yet!!! ERROR!\n");
		return -1;
	}
	if(retval < 0) {
		printf("ln reported async error: %d, %s\n", retval, ln_get_error_message(clnt));
		return -1;
	}
	
	printf("req finished with retval %d, start_time: %.2f, stop_time: %.3f\n",
	       retval, data.resp.start_time, data.resp.stop_time);

	
	return 0;
}
