#!/usr/bin/env python
import os
import sys
import time
import traceback
import links_and_nodes as ln

if __name__ == '__main__': 
    clnt = ln.client("service_client odb.svc_list_property", sys.argv)
    svc = clnt.get_service('odb.svc_list_property', "odb_interface/list_property")
    while True:
        time.sleep(0.001)
        svc.req.object_name = 'ikea_mug' # char*
        svc.req.property = 'graspset' # char*
        svc.req.inherited = 0 # uint8_t
        svc.req.category = 'grasping' # char*
        svc.req.fullpath = 0 # uint8_t

        try:
            svc.call_gobject() # blocking call but allowing gtk main loop to run
        except:
            print "could not call service:\n%s" % traceback.format_exc()
            time.sleep(1)
            continue

        if svc.resp.error_message_len:
            raise Exception(svc.resp.error_message)

        print svc.resp.result
