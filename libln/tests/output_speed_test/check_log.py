#!/usr/bin/python

import sys

last_cnt = None
fp = file(sys.argv[1], "rb")
for line in fp:
    line = line.strip()
    if "speed test" in line:
        # restart
        last_cnt = None
        continue
    try:
        cnt, rest = line.split(" ", 1)
        cnt = int(cnt)
        if cnt == 0:
            last_cnt = -1
    except:
        print "err: invalid line: %r" % line
        continue
        
    if last_cnt is not None and last_cnt + 1 != cnt:
        print last
        missing = cnt - (last_cnt + 1)
        print "err: expected count %d, %d missing" % (last_cnt + 1, missing)
        print (cnt, rest)
    last = cnt, rest
    last_cnt = cnt
