#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>

double get_time() {
	struct timespec ts;
	static time_t sec = 0;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	if(sec == 0)
		sec = ts.tv_sec;
	return (double)(ts.tv_sec - sec) + ts.tv_nsec/1e9;
}

void sleep(double t) {
	struct timespec ts = { (unsigned int)t, 0 };
	t -= ts.tv_sec;
	ts.tv_nsec = t * 1e9;
	nanosleep(&ts, NULL);
}

char* output_line = NULL;
unsigned int output_line_len = 0;
void setup_output_line(unsigned int len) {
	if(output_line)
		free(output_line);
	output_line_len = len + 1; // trailing '\n'
	output_line = (char*)malloc(output_line_len + 1); // trailing '\0';
	for(unsigned int i = 0; i < len; i++)
		output_line[i] = 'A' + (i % 26);
	output_line[len] = '\n';
	output_line[len+1] = 0;
}

void output_n_lines(unsigned int N) {
	for(unsigned int i = 0; i < N; i++) {
		snprintf(output_line, output_line_len, "%8d", i);
		output_line[8] = ' ';
		write(0, output_line, output_line_len);
	}
}

unsigned int do_test_speed(unsigned int N_start=1000) {
	double a, b;
	unsigned int N = N_start;

	// burn in
	output_n_lines(N);

	printf("speed test...\n");
	while(true) {
		a = get_time();
		output_n_lines(N);
		b = get_time() - a;
		if(b > 1)
			break;
		N *= 4;
		sleep(0.1);
	}
	printf("%d lines in %.3fs = %.1f lines/s\n", N, b, (double)N / b);
	return (unsigned int)((double)N / b);
}

int main(int argc, char* argv[]) {

	unsigned int line_len = 120;
	unsigned int n_lines = 50000;
	bool test_speed = false;
	
	unsigned int skip = 0;
	for(int i = 1; i < argc; i++) {
		if(skip) {
			skip--;
			continue;
		}
		if(!strcmp(argv[i], "-line-len") && i + 1 < argc) {
			skip = 1;
			line_len = atoi(argv[i + 1]);
			continue;
		}
		if(!strcmp(argv[i], "-n-lines") && i + 1 < argc) {
			skip = 1;
			n_lines = atoi(argv[i + 1]);
			continue;
		}
		if(!strcmp(argv[i], "test-speed")) {
			test_speed = true;
			continue;
		}
		printf("ignore arg: %s\n", argv[i]);
	}
	setup_output_line(line_len);

	if(test_speed)
		n_lines = do_test_speed(n_lines);
	
	
	return 0;
}
