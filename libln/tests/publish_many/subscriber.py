#!/usr/bin/python

import sys
import time
import links_and_nodes as ln

clnt = ln.client(sys.argv[0], sys.argv)

N = int(sys.argv[1])
ports = []
last = []
for i in xrange(N):
    ports.append(clnt.subscribe("frame%d" % i, "ln/frame34"))
    last.append(None)

while True:
    print "published:",
    for i, p in enumerate(ports):
        d = 0
        if p.read(False):
            f =  p.packet.frame[0]
            if last[i] is not None:
                d = f - last[i]
            last[i] = f
        if p.has_publisher():
            v = 1
        else:
            v = 0
        
        print "%s: %.3f" % (v, d),
    print
    time.sleep(0.3)


