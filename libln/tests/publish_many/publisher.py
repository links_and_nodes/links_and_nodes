#!/usr/bin/python

import sys
import time
import links_and_nodes as ln

clnt = ln.client(sys.argv[0], sys.argv)

N = int(sys.argv[1])
ports = []
for i in xrange(N):
    ports.append(clnt.publish("frame%d" % i, "ln/frame34"))

interleaved = True
iN = 5
iC = 0
last = None
while True:
    #print "subscribed:"
    if interleaved and iC == iN:
        last = None
    for i, p in enumerate(ports):
        if interleaved and last is not None and i <= last:
            continue
        last = i
        p.packet.frame[0] = time.time()
        p.write()        
        #print "  %d: %s" % (i, p.has_subscriber())
        if interleaved:
            if iC == iN:
                continue
            break
    if iC == iN:
        iC = 0
    else:
        iC += 1
    #print
    #time.sleep(0.25)
    time.sleep(0.01)
    if last == len(ports) - 1:
        last = None



