#!/usr/bin/python

import sys
import time

import links_and_nodes as ln

import thread


clnt = ln.client("publish", sys.argv)
args = clnt.get_remaining_args()[1:]

topic, msgdef = "topic", "test_topic"

pub_port = clnt.publish(topic, msgdef)
pub_port.packet.a = 1
pub_port.packet.b = 42
pub_port.write()

sub_port = clnt.subscribe(topic, msgdef)

def abort_thread():
    print "abort thread sleeping"
    time.sleep(2)
    print "abort thread waked up - abort!"
    sub_port.unblock()
    print "abort thread exiting"
thread.start_new_thread(abort_thread, tuple())

for i in xrange(2):
    print "sub read"
    p = sub_port.read(5)
    print repr(p)
    if p:
        print "have packet: %r" % sub_port.packet.dict()
    else:
        print "timeout..."
