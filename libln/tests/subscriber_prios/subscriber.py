#!/usr/bin/python

import time
import sys
import links_and_nodes as ln

rate = float(sys.argv[1])

clnt = ln.client("subscriber", sys.argv)
port = clnt.subscribe("test", "test/topic", rate=rate)

print "ready (rate %.1f)" % rate

while True:
    block_start = time.time()
    port.read(7)
    dt = time.time() - block_start
    print "%s received %d after %.2fs" % (port.topic_name, port.packet.count, dt)
