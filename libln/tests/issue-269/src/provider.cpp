#include <stdio.h>
#include <stdint.h>

#include <string>
#include <vector>

#include <ln/ln.h>

#include "ln_messages.h"

class test_provider
	: test::plan_grasp_base,
	  test::set_task_base

{
	ln::client& clnt;
	ln::service* plan_grasp {};
public:
	test_provider(ln::client& clnt) : clnt(clnt) {
		register_plan_grasp(&clnt, "plan_grasp");
		register_set_task(&clnt, "set_task");
	}
	int run() {
		printf("ready\n");
		while(true)
			clnt.wait_and_handle_service_group_requests(nullptr);
	}
	void print_double_vec(const std::string& name, double* data, unsigned int len) {
		printf("%s[%d]: ", name.c_str(), len);
		for(unsigned int i = 0; i < len; i++) {
			if(i > 0)
				printf(", ");
			printf("%f", data[i]);
		}
		printf("\n");
	}
	void print_string_vec(const std::string& name, test_string_t* data, unsigned int len) {
		printf("%s[%d]: ", name.c_str(), len);
		for(unsigned int i = 0; i < len; i++) {
			if(i > 0)
				printf(", ");
			test_string_t& item = data[0];
			std::string s(item.string, item.string_len);
			printf("'%*.*s'", (int)s.size(), (int)s.size(), s.c_str());
		}
		printf("\n");
	}
	void print_string(const std::string& name, const std::string& s) {
		printf("%s[%d]: '%*.*s'\n", name.c_str(), (int)s.size(), (int)s.size(), (int)s.size(), s.c_str());
	}
	int on_plan_grasp(::ln::service_request& req, test::plan_grasp_t& data) override {
		printf("got incoming plan_grasp!\n");

		print_double_vec("start_configuration", data.req.start_configuration, data.req.start_configuration_len);
		print_double_vec("closed_configuration", data.req.closed_configuration, data.req.closed_configuration_len);
		print_string("tcp-name", { data.req.tcp_name, data.req.tcp_name_len });
		print_string_vec("contact_link_names", data.req.contact_link_names, data.req.contact_link_names_len);
		print_string("object_name", { data.req.object_name, data.req.object_name_len });

		std::vector<std::vector<double>> path = {
			{ 0, 1, 2 },
			{ 3, 4, 5 },
			{ 6, 7, 8 }
		};
		std::vector<test_configuration_t> return_path;
		for(auto&& p : path)
			return_path.push_back({ (uint32_t)p.size(), &p[0] });
		data.resp.path = &return_path[0];
		data.resp.path_len = return_path.size();

		std::vector<uint8_t> link_in_contact { 0, 1 };
		data.resp.link_in_contact = &link_in_contact[0];
		data.resp.link_in_contact_len = link_in_contact.size();

		data.resp.error_message_len = 0; // no error

		req.respond();
		return 0;
	}
	int on_set_task(::ln::service_request& req, test::set_task_t& data) override {
		printf("got incoming set_task!\n");

		print_string("description", { data.req.description, data.req.description_len });

		data.resp.error_message_len = 0; // no error

		req.respond();
		return 0;
	}

};

int main(int argc, char* argv[])
{
	ln::client clnt(argv[0], argc, argv);
	test_provider t(clnt);
	return t.run();
}
