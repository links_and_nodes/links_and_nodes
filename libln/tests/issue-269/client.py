from __future__ import print_function

import sys
import pprint
import traceback

from numpy import *

import links_and_nodes as ln

clnt = ln.client(sys.argv[0], sys.argv)
svc = clnt.get_service('plan_grasp', 'test/plan_grasp')

home_configuration = [0.01] * 43
home_configuration[2] = .5

arm_home_configuration = [-1.0, -1.8, 0.4, 1.8, 0.2, 0.2, 0.2]
home_configuration[5:12] = arm_home_configuration
home_configuration[24:31] = arm_home_configuration

make_two_tries = False
grasp_configs = (
    [-0.5, 0.2, 1.2, 0.1, 0.2, 1.2, 0.05, 0.2, 1.2, 0.0, 0.2, 1.2],
)
svc.req.start_configuration = array(home_configuration, dtype=float64)
svc.req.closed_configuration = grasp_configs
svc.req.tcp_name = 'left_arm7'
pprint.pprint(svc.req)
sys.stdout.flush()
if not make_two_tries:
    svc.call()
else:
    try:
        svc.call()
        #svc.call_gobject()
    except:
        print("first exception:")
        traceback.print_exc()
        print("\ntrying again with better request:")
        svc.req.closed_configuration = grasp_configs[0]
        svc.call()
        #svc.call_gobject()
if svc.resp.error_message_len:
    raise Exception(svc.resp.error_message)
pprint.pprint(svc.resp)
