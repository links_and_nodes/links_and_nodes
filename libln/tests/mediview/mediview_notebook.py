## nb properties ##
{'current_cell': 2, 'window_position': (754, 333), 'window_size': (1165, 693)}
## nb properties end ##
## cell 0 input ##
## autoexec

# this notebook is related to mediview!
# you can use this notebook to do tests, for debugging or for data analysis
process_name = 'mediview'

import links_and_nodes as ln
class mediviewer(ln.services_wrapper):
    def __init__(self, clnt, viewer_name="MediView"):
        ln.services_wrapper.__init__(self, clnt, viewer_name)
        self.wrap_service("config.line", "MediView/config/line", method_name="config_")
    def config(self, line):
        print line
        self.config_(line)

mv = mediviewer(clnt, "mediView")
models = os.getenv("models")
## cell 0 end ##
## cell 1 input ##
## autoexec

# show rotating frames
mv.config("loadObject %s origin" % (os.path.join(models, "coord_015.obj")))
mv.config("loadObject %s frame1" % (os.path.join(models, "coord_015.obj")))
mv.config("connectPose frame1 LN frame1 frame")
mv.config("loadObject %s frame2" % (os.path.join(models, "coord_015.obj")))
mv.config("connectPose frame2 LN frame2 frame")
## cell 1 end ##
## cell 2 input ##
#             alpha                  a                  d              theta
dh = array([   
[                  0,                  0.00000000000,      0,                  0],
[   1.57079632679490,                  0,                  0,                  0],
[  -1.57079632679490,                  0,   0.40000000000000,  -1.57079632679490],
[   1.57079632679490,                  0,                  0,                  0],
[  -1.57079632679490,                  0,   0.39000000000000,  -3.14159265358979],
[   1.57079632679490,                  0,                  0,   1.57079632679490],
[   1.57079632679490,                  0,                  0,  -1.57079632679490],
#[  -1.57079632679490,                  0,   0.11310000000000,  -3.14159265358979],
])
from pyutils.matrix import *
link1_length = 0.40 / 2 # 0.39 / 2
link5_length = 0.39 - link1_length

geoms = [
    None, # no base geom
    ("right-lwr-3-link12-34.iv", transformation(z=link1_length)),
    ("right-lwr-3-link23-45.iv", eye(4)),
    ("right-lwr-3-link12-34.iv", transformation(z=link1_length)),
    ("right-lwr-3-link23-45.iv", eye(4)),
    ("right-lwr-3-link56.iv", transformation(z=link5_length)),
    ("right-lwr-3-link67.iv", eye(4)),
    ("right-lwr-3-link7-bsa.iv", eye(4))
]
#print " ".join(geoms)
base_dir = "/home/schm_fl/data/inventor/justin-mobile-flo-devel/justin"
base_dir = "/home/schm_fl/workspace/camcal_framework/justin_geoms"
lwr_geoms = []
for fn in geoms:
    if fn is not None:
        fn, trafo = fn
        #trafo = eye(4)
        fn = os.path.join(base_dir, fn) + ".obj", trafo
    lwr_geoms.append(fn)
dh = dh[:1]
lwr_geoms = lwr_geoms[:2]
%% dh
%% lwr_geoms
## cell 2 output ##
#  dh: 
# array([[ 0.,  0.,  0.,  0.]])
# lwr_geoms: [None,
#  ('/home/schm_fl/workspace/camcal_framework/justin_geoms/right-lwr-3-link12-34.iv.obj',
#   array([[ 1. ,  0. ,  0. ,  0. ],
#        [ 0. ,  1. ,  0. ,  0. ],
#        [ 0. ,  0. ,  1. ,  0.2],
#        [ 0. ,  0. ,  0. ,  1. ]]))]
# 
## cell 2 end ##
## cell 3 input ##
# load lwr kinematic
coord_fn = "/home/schm_fl/data/inventor/coord_015.iv.obj"

def add_fkine_nodes(robot_name, dh):
    mv.config("addNode MATRIXTRANSFORM %s root" % robot_name)
    last_node = robot_name
    for i, row in enumerate(dh):
        alpha, a, d, theta = row
        med_dh = a, alpha, d, theta
        #med_dh = row
        this_node = "%s_link_%d" % (robot_name, i)
        mv.config("addNode FKIN_NODE %s %s" % (this_node, last_node))
        mv.config("command %s setDH %s" % (this_node, " ".join(map(str, med_dh))))
        last_node = this_node

def load_robot_geoms(robot_name, filenames):
    for i, fn in enumerate(filenames):
        if fn is None:
            continue
        fn, trafo = fn
        if i == 0:
            parent = robot_name
        else:
            parent = "%s_link_%d" % (robot_name, i - 1)
        name = "%s_geom_%d" % (robot_name, i)
        T = map(str, trafo[:3, :3].flatten())
        T.extend(map(str, trafo[:3, 3]))
        T = map(str, trafo[:3, :].flatten())
        #mv.config("loadObject %s %s %s %s -1" % (fn, name, parent, T))
        mv.config("loadObject %s %s %s %s" % (fn, name, parent, " ".join(T)))
        mv.config("loadObject %s %s %s" % (coord_fn, name+"_c", parent))
        #mv.config("loadObject %s %s %s" % (fn, name, parent))

def connect_fkine_nodes(robot_name, N, topic, field):
    for i in xrange(N):
        node = "%s_link_%d" % (robot_name, i)
        mv.config("connectCustom %s LN %s %s[%d]" % (node, topic, field, i))

mv.config("loadObject %s %s" % (coord_fn, "origin"))
add_fkine_nodes("lwr", dh)
load_robot_geoms("lwr", lwr_geoms)
connect_fkine_nodes("lwr", dh.shape[0], "lwr_state", "frame")
## cell 3 output ##
# loadObject /home/schm_fl/data/inventor/coord_015.iv.obj origin
# addNode MATRIXTRANSFORM lwr root
# addNode FKIN_NODE lwr_link_0 lwr
# command lwr_link_0 setDH 0.0 0.0 0.0 0.0
# loadObject /home/schm_fl/workspace/camcal_framework/justin_geoms/right-lwr-3-link12-34.iv.obj lwr_geom_1 lwr_link_0 1.0 0.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 0.2
# loadObject /home/schm_fl/data/inventor/coord_015.iv.obj lwr_geom_1_c lwr_link_0
# connectCustom lwr_link_0 LN lwr_state frame[0]
# 
## cell 3 end ##
## cell 4 input ##
mv.config("loadObject %s o2" % coord_fn)

## cell 4 output ##
# loadObject /home/schm_fl/data/inventor/coord_015.iv.wrl o2
# 
## cell 4 end ##
## cell 5 input ##
trafo = transformation(z_rot_deg=40)
%% trafo
T = map(str, trafo[:3, :].flatten())
mv.config("setPose o2 %s" % " ".join(T))
## cell 5 output ##
# trafo: 
# array([[ 0.766 , -0.6428,  0.    ,  0.    ],
#        [ 0.6428,  0.766 ,  0.    ,  0.    ],
#        [ 0.    ,  0.    ,  1.    ,  0.    ],
#        [ 0.    ,  0.    ,  0.    ,  1.    ]])
# setPose o2 0.766044443119 -0.642787609687 0.0 0.0 0.642787609687 0.766044443119 0.0 0.0 0.0 0.0 1.0 0.0
# 
## cell 5 end ##
## cell 6 input ##
mv.config("listCommands")
## cell 6 output ##
# listCommands
# 
# Traceback (most recent call last):
#   File "<string>", line 1, in <module>
#   File "<string>", line 14, in config
#   File "/volume/USERSTORE/schm_fl/ln_base/library/bindings/python/links_and_nodes/ln_wrappers.py", line 1395, in __call__
#     raise Exception(self.svc.resp.error_message)
# Exception: fauked to list commands for object with name ''!
## cell 6 end ##
## cell 7 input ##
mv.config("loadObject /home/schm_fl/euroc/gazebo/challenge2_sim/models/kuka_lwr/coord_015.dae c2")
## cell 7 output ##
# 
# Traceback (most recent call last):
#   File "<string>", line 1, in <module>
#   File "/volume/USERSTORE/schm_fl/ln_base/library/bindings/python/links_and_nodes/ln_wrappers.py", line 1325, in __call__
#     raise Exception(self.svc.resp.error_message)
# 
## cell 7 end ##
## cell 8 input ##
import time
a = time.time()
mv.config("loadObject %s justin" % (
    "/home/schm_fl/data/inventor/justin_in_one_file/justin_base.wrl"))
b = time.time() - a
% b
## cell 8 output ##
#  b: 25.4149720669
# 
## cell 8 end ##
## cell 9 input ##
mv.config("help")
## cell 9 output ##
# 
# Traceback (most recent call last):
#   File "<string>", line 1, in <module>
#   File "/volume/USERSTORE/schm_fl/ln_base/library/bindings/python/links_and_nodes/ln_wrappers.py", line 1325, in __call__
#     raise Exception(self.svc.resp.error_message)
# 
## cell 9 end ##
