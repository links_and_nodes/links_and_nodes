#!/usr/bin/python
"""
this is a very simple blocking example without gtk or gobject
"""

import sys
import time
import links_and_nodes as ln

class my_watcher(ln.lnm_remote):
    def __init__(self, address="localhost:54123", use_gobject=False, debug=False):
        ln.lnm_remote.__init__(self, address, debug=debug)

        # this needs to have manager python dir in path!
        if ln.ln_manager_dir not in sys.path:
            sys.path.insert(0, ln.ln_manager_dir)
        self.sysconf = self.request("get_system_configuration")
        print "have %d processes defined in this config" % len(self.sysconf.processes)
        
        # pick one
        pname = list(self.sysconf.processes.keys())[0]

        for i in xrange(3):
            print "start %r" % pname
            self.request("set_process_state_request", ptype="Process", pname=pname, requested_state="start")

            print "wait 2s"
            time.sleep(2)

            print "stop %r" % pname
            self.request("set_process_state_request", ptype="Process", pname=pname, requested_state="stop")

            print "wait 2s"
            time.sleep(2)

    def on_output(self, name, output):
        # disable printing of process output
        return False
    def on_log_messages(self, msgs):
        # disable printing of log messages
        return False

if __name__ == "__main__":
    w = my_watcher(debug="-debug" in sys.argv)
