#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string>
#include <signal.h>
#include <unistd.h>

#include <pthread.h>

#include "ln.h"
#include "ln_messages.h"

using namespace std;

class event_test :
	public ln_event_listener_ln_resource_event {

	ln::client* clnt;
	bool keep_running;
	int max_runs;
public:
	event_test(int argc, char** argv) {
		clnt = new ln::client("event test oo", argc, argv);
		
		ln_event_connect_ln_resource_event evconnect;
		// ln::string_buffer ev_pattern(&evconnect.event_pattern, "*_new_*"); // service_{new|del}_provider | topic_{new|del}_{publisher|subscriber} }
		ln::string_buffer ev_pattern(&evconnect.event_pattern, "*"); // service_{new|del}_provider | topic_{new|del}_{publisher|subscriber} }
		ln::string_buffer name(&evconnect.name_pattern, "*");
		ln::string_buffer md(&evconnect.md_pattern, "*");
		ln::string_buffer client(&evconnect.client_pattern, "*");
		connect_to_ln_resource_event(clnt, "ln.resource_event", &evconnect);

		max_runs = 0;
	}

	~event_test() {
		delete clnt;
	}
	
	void on_ln_resource_event(ln_event_ln_resource_event& ev) {
		string event(ev.event, ev.event_len);
		string name(ev.name, ev.name_len);
		string md(ev.md, ev.md_len);
		string client(ev.client, ev.client_len);
		
		printf("received event: %s, name: %s, md: %s, client: %s\n",
		       event.c_str(),
		       name.c_str(),
		       md.c_str(),
		       client.c_str());
		max_runs --;
		if(max_runs == 0)
			keep_running = false;
	}

	int run() {
		keep_running = true;
		while(keep_running)
			clnt->wait_and_handle_service_group_requests(std::nullptr, 1);
		return 0;
	}

		
};

int main(int argc, char* argv[]) {
	event_test ev(argc, argv);
	return ev.run();
}
