#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string>
#include <signal.h>
#include <unistd.h>

#include <pthread.h>

#include "ln.h"
#include "ln_messages.h"

using namespace std;

int max_calls = 4;
bool keep_running;

void on_ln_resource_event(ln::event_call& call, void* user_data) {
	ln_event_ln_resource_event ev;
	call.set_data(&ev, ln_event_ln_resource_event_signature);

	string event(ev.event, ev.event_len);
	string name(ev.name, ev.name_len);
	string client(ev.client, ev.client_len);

	printf("received event: %s, name: %s, client: %s\n",
	       event.c_str(),
	       name.c_str(),
	       client.c_str());
	// sleep(1);
	max_calls --;
	if(max_calls == 0) {
		printf("want to exit now!\n");
		keep_running = false;
	}
}

int main(int argc, char* argv[]) {
	ln::client clnt("event test", argc, argv);

	ln_event_connect_ln_resource_event evconnect;
	ln::string_buffer ev_pattern(&evconnect.event_pattern, "*"); // service_{new|del}_provider | topic_{new|del}_{publisher|subscriber} }
	ln::string_buffer name(&evconnect.name_pattern, "*");
	ln::string_buffer md(&evconnect.md_pattern, "*");
	ln::string_buffer client(&evconnect.client_pattern, "*");
	ln::event_connection* ev_conn = clnt.connect_to_event(
		"ln.resource_event",
		ln_event_connect_ln_resource_event_signature, &evconnect,
		&on_ln_resource_event);

	keep_running = true;
	printf("waiting for requests...\n");
	while(keep_running)
		clnt.wait_and_handle_service_group_requests(std::nullptr, 1);

	printf("main exiting!\n");
	// sleep(1);
	return 0;
}
