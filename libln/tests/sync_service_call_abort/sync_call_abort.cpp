#include <stdio.h>
#include <pthread.h>

#include <string>
#include <vector>

#include <ln.h>
#include "ln_messages.h"

class test {
	ln::client* clnt;
	ln::service* slow_service;
	double abort_after;
	double deadline;
	pthread_t watcher;
public:
	test(int argc, char* argv[]) {
		clnt = new ln::client("test sync call abort", argc, argv);
		clnt->set_thread_safe(true);
		
		std::vector<std::string> args = clnt->get_remaining_args();
		if(args.size() > 1)
			abort_after = atof(args[1].c_str());
		else
			abort_after = 0;

		slow_service = clnt->get_service("slow_service", "sync_service_call_abort/slow_service",
			ln_service_sync_service_call_abort_slow_service_signature);
	}
	
	~test() {
		delete clnt;
	}

	static void* watch_deadline(void* data) {
		test* self = (test*)data;
		while(ln_get_time() < self->deadline)
			usleep(10000);
		printf("now we have to abort!\n");
		self->slow_service->abort();
		printf("aborted!\n");
		return NULL;
	}

	int run() {
		printf("ready\n");

		ln_service_sync_service_call_abort_slow_service service_data;
		ln::string_buffer what(&service_data.req.what, "i want it all!");

		if(abort_after > 0) {
			deadline = ln_get_time() + abort_after;
			pthread_create(&watcher, NULL, watch_deadline, this);
		}
		
		slow_service->call(&service_data);
		
		std::string error_message(service_data.resp.error_message, service_data.resp.error_message_len);
		if(error_message.size())
			printf("error_message: %s\n", error_message.c_str());
		else {
			std::string data(service_data.resp.data, service_data.resp.data_len);
			printf("data: %s\n", data.c_str());
		}
		
		printf("done!\n");
	}
	       
};

int main(int argc, char* argv[]) {
	test t(argc, argv);
	return t.run();
}
