#!/usr/bin/python

import links_and_nodes as ln

import pprint
import os
import sys
import gc
from numpy import *

use_gtk = True

if use_gtk:
    import gtk

verbose = False

class provider(ln.service_provider):
    def __init__(self):
        self.clnt = ln.client(self.__class__.__name__, sys.argv)
        if use_gtk:
            ln.service_provider.__init__(self, self.clnt, default_register_method="do_register_gobject")
        else:
            ln.service_provider.__init__(self, self.clnt)

        if len(sys.argv) > 1:
            sid = int(sys.argv[1])
        else:
            sid = 0
        
        self.wrap_service_provider("test%d" % sid, "test_md", method_name="test")
        if sid == 0:
            self.wrap_service_provider("horror", "horrible_md", pass_request=True,
                                       postprocessor=self.horror_post
            )

        # 22 objects per call!
        #self.stop_after = 1 # 30
        #self.stop_after = 2 # 52
        #self.stop_after = 3 # 74
        #self.stop_after = 6 # 140
        #self.stop_after = 12 # 271
        #self.stop_after = 24 # 535
        #self.stop_after = 100 * 24 # 535
        #self.stop_after = 5*24 # 2644
        self.stop_after = 0

    def run(self):
        if not use_gtk:
            self.handle_service_group_requests()
        else:
            print "gtk main"
            gtk.main()
        print "terminating"
        
    def test(self, s1, s2, s3, a, b, c):
        if verbose:
            print "received:"
            print "  s1: %r" % s1
            print "  s2: %r" % s2
            print "  s3: %r" % s3
            print "  a: %d, b: %d, c: %d" % (a, b, c)

        data = s1 + s2 + s3 + " %d %d %d" % (a, b, c)
        
        d = int32(a)
        d += int32(b)
        d += int32(c)

        if verbose:
            print "returning:"
            print "  data: %r" % data
            print "  d: %d" % d

        if self.stop_after:
            self.stop_after -= 1
            if self.stop_after == 0:
                self.keep_running = False
                #self.do_gc()
        return dict(data=data, d=d)

    def horror(self, len, request):
        print "input len: %r" % len
        data = []
        for i in xrange(len):
            data.append(48 + i % 13)
        return data
    def horror_post(self, sw, resp, data):
        resp.file_data = []
        for item in data:
            p = resp.new_vector_uint8_t_packet()
            p.data = item
            resp.file_data.append(p)
        #resp.data_len = len(resp.data)
def start():
    clnt = provider()
    clnt.run()

    if False:
        import objgraph
        import random
        objgraph.show_most_common_types() 
        for chose in xrange(20):
            objgraph.show_chain(
                objgraph.find_backref_chain(
                    random.choice(objgraph.by_type('function')),
                    objgraph.is_proper_module),
                    filename="/tmp/objgraph_back_%03d.png" % chose)

        objgraph.show_refs([clnt], filename="/tmp/objgraph.png")
    
if __name__ == "__main__":
    #gc.enable()
    #gc.set_debug(gc.DEBUG_LEAK)
    start()

    if True:
        print "\nGARBAGE OBJECTS %d:" % (len(gc.garbage))
        
        for i, x in enumerate(gc.garbage[::-1]):
            s = str(x)
            if len(s) > 120: s = s[:120]
            print "%s: %s" % (type(x).__name__, s)
            if isinstance(x, (tuple, list)):
                print "  %d items" % (len(x))
            #objgraph.show_chain(
            #    objgraph.find_backref_chain(x, objgraph.is_proper_module), filename="/tmp/br_%04d.png" % i)
        print "had %d GARBAGE OBJECTS" % (len(gc.garbage))

