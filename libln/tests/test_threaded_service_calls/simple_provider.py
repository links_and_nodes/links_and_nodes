#!/usr/bin/python

import links_and_nodes as ln

import pprint
import os
import sys
import gc
import objgraph
import numpy as np

gc.enable()
#gc.set_debug(gc.DEBUG_LEAK)

clnt = ln.client(sys.argv[0], sys.argv)
service_wrapper = clnt.get_service_provider("test0", "test_md")

last_str = None
last_req = None
def on_call(request_wrapper, req_data, resp_data):
    s1, s2, s3, a, b, c = req_data.s1, req_data.s2, req_data.s3, req_data.a, req_data.b, req_data.c
    
    data = s1 + s2 + s3 + " %d %d %d" % (a, b, c)

    d = np.int32(a)
    d += np.int32(b)
    d += np.int32(c)

    resp_data.error_message = ""
    resp_data.data = data
    resp_data.d = d

    request_wrapper.respond()
    

service_wrapper.set_handler(on_call)
service_wrapper.do_register()
N = 1000000
while N:
    clnt.wait_and_handle_service_group_requests()
    N -= 1

objgraph.show_most_common_types() 

if False:
    for chose in xrange(20):
        objgraph.show_chain(
            objgraph.find_backref_chain(
                random.choice(objgraph.by_type('function')),
                objgraph.is_proper_module),
                filename="/tmp/objgraph_back_%03d.png" % chose)

    objgraph.show_refs([clnt], filename="/tmp/objgraph.png")
    

if True:
    print "\nGARBAGE OBJECTS %d:" % (len(gc.garbage))

    for i, x in enumerate(gc.garbage):
        s = str(x)
        if len(s) > 120: s = s[:120]
        print "%s: %s" % (type(x).__name__, s)
        if isinstance(x, (tuple, list)):
            print "  %d items" % (len(x))
        objgraph.show_chain(
            objgraph.find_backref_chain(x, objgraph.is_proper_module), filename="/tmp/br_%04d.png" % i)
    print "had %d GARBAGE OBJECTS" % (len(gc.garbage))

