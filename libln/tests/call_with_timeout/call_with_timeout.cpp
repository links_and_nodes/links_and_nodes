#include <ln/ln.h>
#include "ln_messages.h"

#define CHECK_RETVAL(call)						\
	({								\
		int ret = call;						\
		if(ret != 0) {						\
			fprintf(stderr, "error: line %d " #call " returned %d\n", __LINE__, ret); \
			exit(1);					\
		}							\
	})
#define SHOW_ERROR(ret)							\
	({								\
		if(ret != 0) 						\
			fprintf(stderr, "error: line %d: ret %d: %s\n", __LINE__, ret, ret == -LNE_REQUEST_FAILED ? ln_get_error_message(clnt) : ln_format_error(ret)); \
	})

int test_c_api()
{
	ln_client clnt;
	CHECK_RETVAL(ln_init(&clnt, "test_c_api", 0, NULL));

	ln_service svc;
	CHECK_RETVAL(ln_service_init(clnt, &svc, "test.add", "test/add", test_add_signature));

	int ret;

	double start;
	test_add_t data;

	// sort blocking call
	memset(&data.resp, 0, sizeof(data.resp));
	start = ln_get_time();
	data.req.a = 1;
	data.req.b = 2;
	data.req.name.string = (char*)"test name";
	data.req.name.string_len = strlen(data.req.name.string);
	//ret = ln_service_call(svc, &data);
	ret = ln_service_call_with_timeout(svc, &data, 0);
	printf("short blocking call: ret:%d, res: %d after %.3fs\n", ret, data.resp.c, ln_get_time() - start);
	SHOW_ERROR(ret);

	start = ln_get_time();
	ret = ln_service_call_with_timeout(svc, &data, 0);
	printf("2nd short blocking call: ret:%d, res: %d after %.3fs\n", ret, data.resp.c, ln_get_time() - start);
	SHOW_ERROR(ret);

	// long blocking call
	memset(&data.resp, 0, sizeof(data.resp));
	start = ln_get_time();
	data.req.a = 43;
	data.req.b = 4445;
	ret = ln_service_call_with_timeout(svc, &data, 0);
	printf("long blocking call: ret:%d, res: %d after %.3fs\n", ret, data.resp.c, ln_get_time() - start);
	SHOW_ERROR(ret);

	// long blocking call with timeout
	memset(&data.resp, 0, sizeof(data.resp));
	start = ln_get_time();
	data.req.a = 43;
	data.req.b = 4445;
	ret = ln_service_call_with_timeout(svc, &data, 2);
	printf("long blocking call with timeout: ret:%d, res: %d after %.3fs\n", ret, data.resp.c, ln_get_time() - start);
	if(ret == 0)
		printf("ERROR: expect an error here!");
	else {
		printf("expected error report: ");
		SHOW_ERROR(ret);
	}

	CHECK_RETVAL(ln_service_deinit(&svc));
	return 0;
}

int main(int argc, char* argv[])
{
	test_c_api();
}
