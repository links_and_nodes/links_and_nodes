#include <stdio.h>

#include <ln.h>
#include <lnrk_interface.h>

using namespace std;

struct telemetry_header {
	uint8_t have_command;
	uint32_t counter;
} __attribute__((__packed__));

struct command_header {
	uint32_t counter;
} __attribute__((__packed__));

int main(int argc, char* argv[]) {
	ln::client* clnt = new ln::client(argv[0], argc, argv);

	lnrk_interface* intf = lnrk_interface::create(clnt, "test");
	string devices = "pd_slave1, pd_slave2";
	ln::inport* inport = intf->get_telemetry_port(clnt->name, devices, "", false, true, 10, -1);
	ln::outport* outport = intf->get_command_port(clnt->name, devices, 10);

	vector<uint8_t> tele(inport->message_size);
	vector<uint8_t> cmd(outport->message_size, 0);

	intf->request_command(clnt->name, outport->topic_name, inport->topic_name);

	telemetry_header* tele_hdr = (telemetry_header*)&tele[0];
	command_header* cmd_hdr = (command_header*)&cmd[0];
	
	while(true) {
		inport->read(&tele[0]);
		printf("tick, tele_cnt: %d\n", tele_hdr->counter);

		cmd_hdr->counter = tele_hdr->counter;
		
		outport->write(&cmd[0]);
	}
	
	return 0;
}
