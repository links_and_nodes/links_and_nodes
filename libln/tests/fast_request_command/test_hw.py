#!/usr/bin/python

import sys
import links_and_nodes as ln

clnt = ln.client(sys.argv[0], sys.argv)

outport = clnt.publish("test_msr", "test_msr")
inport = clnt.subscribe("test_cmd", "test_cmd")

N = 3

print "ready"
while True:
    inport.read()

    for i in xrange(N):
        outport.packet.slaves[i].state = inport.packet.slaves[i].control
        outport.packet.slaves[i].pos = inport.packet.slaves[i].pos
        print "(state: %#x, pos: %.2f)" % (
            outport.packet.slaves[i].state,
            outport.packet.slaves[i].pos),
    print

    outport.write()
