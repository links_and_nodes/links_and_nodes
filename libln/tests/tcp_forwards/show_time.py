import sys
import links_and_nodes as ln
import pprint

node = sys.argv[1]

clnt = ln.client("service_client %s.clock_gettime" % node, sys.argv)

svc = clnt.get_service('%s.clock_gettime' % node, 'ln/file_services/clock_gettime')
svc.req.clock_id = 0

def show_time():
    svc.call()

    if svc.resp.error_message:
        raise Exception(svc.resp.error_message)

    print "%s: %d %9d" % (node, svc.resp.tv_sec, svc.resp.tv_nsec)


for i in xrange(10):
    show_time()
