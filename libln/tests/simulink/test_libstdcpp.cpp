/**

   to build mex file on matlab command line:

   >> mex test_libstdcpp.cpp

   to test-run on matlab command line:

   >> test_libstdcpp

   output on success (since matlab2013b):
   "all fine! (size: 1)"

   output on error (e.g. matlab2011b):
   "Invalid MEX-file '/.../test_libstdcpp.mexa64': /opt/matlab/v26/bin/glnxa64/../../sys/os/glnxa64/libstdc++.so.6: version `GLIBCXX_3.4.15' not found (required by /.../test_libstdcpp.mexa64)
 
 */

#include <stdio.h>
#include "mex.h"
#include <list>

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
	std::list<int> l;

	l.push_back(1);
	
	mexPrintf("all fine! (size: %d)\n", l.size());
}

