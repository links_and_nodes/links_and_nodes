% to create mex files and support rtw build process:
addpath /home/that/sd/externals/make_mex;

% init ln
ln_base = '/home/schm_fl/workspace/ln_base/';
addpath([ln_base, 'library/simulink']);
ln_init();

ln_client_name = 'mein simulink test';
ln_client_stop_model_on_error = 1;
ln_client_empty_simulation = 0;
ln_manager = 'localhost:54414';
