#include <stdio.h>
#include <unistd.h>

#include <string>
using namespace std;

void go() {
	int i = 0;
	for(int k = 0; k < 3; k++) {
		printf("go %d %d\n", k, i);
		i *= 2;
	}
}

int main(int argc, char* argv[]) {
	int u = 42;
	string a = "hello";
	usleep(500000);
	printf("ready %s\n", a.c_str());
	usleep(1500000);
	printf("test %d\n", u);
	usleep(500000);
	go();
	return 0;
}
