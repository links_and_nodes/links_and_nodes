
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#include <vector>
#include <ln.h>

using namespace std;

void stop_handler(int signo) {
	printf("git signal!\n");
}

int main(int argc, char* argv[]) {
	ln::client* clnt = new ln::client("sub", argc, argv);
	ln::inport* port = clnt->subscribe("test", "tests/counters");
	vector<uint8_t> data(port->message_size);
	while(true) {
		port->read(&data[0]);
		printf("tick!\n");
	}
	

	return 0;
}

