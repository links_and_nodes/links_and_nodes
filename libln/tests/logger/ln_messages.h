#ifndef LN_MESSAGES_H
#define LN_MESSAGES_H

#include <stdint.h>

#ifdef __cplusplus
#include "ln_cppwrapper.h"
#endif
/* tests/counters */
#include "ln_packed.h"
struct LN_PACKED tests_counters_struct;
typedef struct tests_counters_struct tests_counters_t;
struct LN_PACKED tests_counters_struct {
	double duration;
	uint32_t counter;
	float frame[16];
};
#include "ln_endpacked.h"
#endif // LN_MESSAGES_H
