#!/bin/bash

if [ "x$1" == "xstop" ]; then
    echo "only stopping..."
fi

# stop all running stuff to get clean start, should also clean daemon state!
ln_manager  -c test_logger.lnc --without-gui --console 2>&1 > /dev/null <<EOF
cd ..
stop -w test publishers
print self.manager.release_all_daemon_resources("rmc-lx0141")
print self.manager.release_all_daemon_resources("rmc-taygete")
print self.manager.release_all_daemon_resources("rmc-lx0142")
EOF

if [ "x$1" == "xstop" ]; then
    exit 0
fi
    
# start publisher & subscriber, wait 3 seconds, then quit manager!
ln_manager  -c test_logger.lnc --without-gui --console <<EOF
cd ..
start -r test/subscriber publishers/fast
log enable
echo sleeping...
sleep 3
echo exiting...
EOF

# start manager again, waiting for reconnects from clients!
ln_manager  -c test_logger.lnc --without-gui --console-exec "cd ..; log enable" --console
