## nb properties ##
{'current_cell': 7, 'window_position': (1920, 428), 'window_size': (1470, 609)}
## nb properties end ##
## cell 0 input ##
## autoexec

# this notebook is related to test!
# you can use this notebook to do tests, for debugging or for data analysis
ports = []
## cell 0 end ##
## cell 1 input ##
ports.append(clnt.publish("port %d" % len(ports), "p3"))
## cell 1 end ##
## cell 2 input ##
% clnt.get_service_signature("s3")
## cell 2 output ##
# clnt.get_service_signature("s3"):
# Traceback (most recent call last):
#   File "<string>", line 1, in <module>
# 
## cell 2 end ##
## cell 3 input ##
pr = clnt.get_service("provider3", "s5")
## cell 3 end ##
## cell 4 input ##
% clnt
## cell 4 output ##
# clnt: <_ln.client object at 0x2b0f5cc0b8e8>
# 
## cell 4 end ##
## cell 5 input ##
import sys
#sys.path.sort()
for p in sys.path:
    % p
## cell 5 output ##
# p: /volume/USERSTORE/schm_fl/workspace/ln_base/library/bindings/python/links_and_nodes/obj/sled11-x86_64-gcc4.x_py2.7
# p: /volume/USERSTORE/schm_fl/workspace/ln_base/contrib/pyutils/build/lib.linux-x86_64-2.7
# p: /volume/USERSTORE/schm_fl/workspace/ln_base/manager
# p: /volume/USERSTORE/schm_fl/workspace/ln_base/library/bindings/python
# p: /volume/USERSTORE/schm_fl/workspace/ln_base/contrib
# p: /home/schm_fl/data/workspace/ipython
# p: /home/schm_fl/workspace/ln_base/library/bindings/python
# p: /home/f_soft/packages/rmpm/1.2.16/python/sled11-x86-gcc4.x
# p: /home/schm_fl/workspace
# p: /home/schm_fl/data/local-suse11-x86_64/lib/python2.6/site-packages
# p: /home/schm_fl/packages/pyutils/build/lib.linux-x86_64-2.6
# p: /home/schm_fl/data/local/lib/python2.5/site-packages
# p: /home/schm_fl/data/python-packages
# p: /home/schm_fl/packages
# p: /home/schm_fl/svn/devnull/pymacs
# p: /home/schm_fl/data/python-packages/flotest
# p: /usr/lib/python27.zip
# p: /usr/lib64/python2.7
# p: /usr/lib64/python2.7/plat-linux2
# p: /usr/lib64/python2.7/lib-tk
# p: /usr/lib64/python2.7/lib-old
# p: /usr/lib64/python2.7/lib-dynload
# p: /home/schm_fl/.local/lib/python2.7/site-packages
# p: /usr/lib64/python2.7/site-packages
# p: /usr/lib64/python2.7/site-packages/PIL
# p: /usr/local/lib64/python2.7/site-packages
# p: /usr/local/lib/python2.7/site-packages
# p: /usr/lib64/python2.7/site-packages/gst-0.10
# p: /usr/lib64/python2.7/site-packages/gtk-2.0
# p: /usr/lib/python2.7/site-packages
# p: /usr/lib64/python2.7/site-packages/wx-3.0-gtk2
# p: /home/f_soft/packages/rmpm/1.2.16/python/sled11-x86-gcc4.x
# p: /home/schm_fl/data/local-suse11-x86_64/lib/python2.6/site-packages
# 
## cell 5 end ##
## cell 6 input ##
## name: test issue #112

logger = clnt.get_logger("test logger")
logger.add_topic("some non existing topic", 1000, 1)
logger.enable()
logger.disable()
data = logger.download()
% type(data)
## cell 6 output ##
# type(data): <class 'links_and_nodes.ln_wrappers.logger_data_wrapper'>
# 
## cell 6 end ##
## cell 7 input ##
import time
logger = clnt.get_logger("other logger")
logger.clear_topics()
logger.add_topic("some_topic", 1000, 1)
logger.enable()
time.sleep(0.1)
logger.disable()
data = logger.download()
% type(data)
data.unpack_into(nb.module.__dict__)
## cell 7 output ##
# type(data): <class 'links_and_nodes.ln_wrappers.logger_data_wrapper'>
# some_topic._packet_source_ts (0,)
# some_topic._packet_counter   (0,)
# some_topic._packet_log_ts    (0,)
# 
## cell 7 end ##
## cell 8 input ##
tc = data.topics["some_topic"]
tc.c.get_sample(1000, tc.packet._data)
## cell 8 output ##
# 
# Traceback (most recent call last):
#   File "<string>", line 1, in <module>
# 
## cell 8 end ##
## cell 9 input ##
% data.get_dict()
## cell 9 output ##
# data.get_dict(): {'some_non_existing_topic': {'_packet_counter': array([], dtype=int64), '_packet_log_ts': array([], dtype=int64), '_packet_source_ts': array([], dtype=float64)}}
# 
## cell 9 end ##
