#!/usr/bin/python

import os
import sys
import time
import links_and_nodes as ln

clnt = ln.client(sys.argv[0], sys.argv)

fn = os.path.join(os.getcwd(), "out.lnlog")
print "now loading ln-log file %r!" % fn
data = ln.logger_data.load(fn)

print "have %d topics:" % len(data.topics)

log_start_time = None
cursors = []
for name, cursor in data.topics.iteritems():
    topic = cursor.topic
    print "topic: %r, md: %r, n_samples: %d" % (name, topic.md_name, topic.n_samples)
    
    p = cursor.get_sample(0)
    print "  first_packet log_ts: %.3f, src_ts: %.3f, packet_counter: %d" % (p.log_ts, p.src_ts, p.packet_counter)
    if log_start_time is None or p.src_ts < log_start_time:
        log_start_time = p.src_ts

    cursor.last_index = topic.n_samples - 1
    cursor.next_index = 0
    cursor.next_ts = p.src_ts

    cursor.port = clnt.publish(name, topic.md_name)
    cursor.port.packet = cursor.packet

    cursors.append(cursor)

rt_factor = 0.1

print "starting replay"
start_time = time.time()
last_print = start_time
while True:
    now = time.time()
    run_time = now - start_time
    sim_run_time = run_time * rt_factor
    sim_time = log_start_time + sim_run_time

    #print "\nsim_run_time: %.3fs" % sim_run_time
    if now - last_print > 1:
        last_print = now
        print "sim_run_time: %.3fs" % sim_run_time

    # search for topics that need to publish
    publish_in = None
    n_publishes = 0
    while True:
        had_publish = False
        for cursor in cursors:        
            if cursor.next_ts is None:
                continue
            if sim_time < cursor.next_ts:
                this_publish_in = cursor.next_ts - sim_time
                if publish_in is None or this_publish_in < publish_in:
                    publish_in = this_publish_in
                continue
            # publish cursor.packet!
            #print "publish %s idx %d ts %.3f" % (cursor.name, cursor.next_index, cursor.next_ts - log_start_time)
            cursor.port.write()
            cursor.next_index += 1
            if cursor.next_index <= cursor.last_index:
                p = cursor.get_sample(cursor.next_index)
                cursor.next_ts = p.src_ts
            else:
                cursor.next_ts = None
            had_publish = True
            n_publishes += 1
        if not had_publish:
            break
    #print "had %d publishes in this turn!" % n_publishes
    if publish_in is None:
        print "no more samples left! end of log!"
        break
    rt_publish_in = publish_in / rt_factor
    #print "rt sleep %.3f seconds to next publish" % rt_publish_in
    time.sleep(rt_publish_in)

        
