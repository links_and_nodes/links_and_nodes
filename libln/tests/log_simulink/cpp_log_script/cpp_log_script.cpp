#include <ln.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

using namespace std;

int main(int argc, char* argv[]) {
	try {
		ln::client clnt(argv[0], argc, argv);
		ln::logger* logger = clnt.get_logger("flo test logger");
		logger->add_topic("log1", 5000);

		printf("now starting log!\n");
		logger->enable();

		printf("waiting...\n");
		sleep(1);
		
		printf("now stopping log!\n");
		logger->disable();

		char fn[1024];
		getcwd(fn, 1024);
		strcat(fn, "/out.mat");
		printf("now ask manager to save to mat file '%s'!\n", fn);
		logger->manager_save(fn);
		printf("can not load mat files :(\n");

		struct stat sbuf;
		if(stat(fn, &sbuf)) {
			perror("stat");
			return -1;
		}
		
		printf("size of mat file: %d Bytes\n", sbuf.st_size);
	}
	catch(const exception& e) {
		printf("caught exception in main:\n%s\n", e.what());
		return -1;
	}
	return 0;
}
