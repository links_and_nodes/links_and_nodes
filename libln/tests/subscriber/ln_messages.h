#ifndef LN_MESSAGES_H
#define LN_MESSAGES_H

#include <stdint.h>

#ifdef __cplusplus
#include "ln_cppwrapper.h"
#endif
/*
  /volume/USERSTORE/schm_fl/ln_base/message_definitions/tests/counters
  tests/counters 76 bytes
*/
typedef struct __attribute__((packed)) {
	double duration;
	uint32_t counter;
	float frame[16];
} ln_packet_tests_counters;

#endif // LN_MESSAGES_H
