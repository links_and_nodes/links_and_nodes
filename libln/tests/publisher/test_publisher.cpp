#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "ln.h"

#include "ln_messages.h"

#ifdef __WIN32__
#include <windows.h>
struct timespec {
	time_t tv_sec;
	long tv_nsec;
};
void nanosleep(struct timespec* ts, struct timespec* tsout) {
	double ms = ts->tv_sec * 1e3 + ts->tv_nsec / 1e6;
	Sleep(ms);
}
#endif

int main(int argc, char* argv[]) {
	int ret;
	double rate = 100;
	time_t start_time = time(NULL);

	for(int i = 1; i < argc; i++)
		if(!strcmp(argv[i], "-rate") && (i + 1) < argc)
			rate = atof(argv[++i]);

	double start = ln_get_time();
	for(unsigned int i = 0; i < 10; i++) {
		double now;
		while(true) {
			now = ln_get_time();
			if(now != start)
				break;
		}
		printf("%f to %f delta: %f in us: %.3f[us]\n",
		       start, now, now - start,
		       (now - start) * 1e6);
		start = ln_get_time();
	}
	/*
	while(ln_get_time() - start < 11)
		sleep(1);
	start = ln_get_time();
	for(unsigned int i = 0; i < 10; i++) {
		double now;
		while(true) {
			now = ln_get_time();
			if(now != start)
				break;
		}
		printf("%f to %f delta: %f in us: %.3f[us]\n",
		       start, now, now - start,
		       (now - start) * 1e6);
		start = now;
	}
	*/
	       
	
	double seconds = 1. / rate;
	struct timespec ts = {(int)seconds, (long int)((seconds - (int)seconds) * 1e9)};
	
	ln_client clnt;
	if((ret = ln_init(&clnt, "test publisher md", argc, argv))) {
		printf("could not init ln_client. returned error %d: %s\n", -ret, ln_format_error(ret));
		if(clnt)
			printf("dynamic error: %s\n", ln_get_error_message(clnt));
				
		return -1;
	}

	// register publish/output-port
	ln_outport counters_port;
	if((ret = ln_publish(clnt, "tests.counters", "tests/counters", &counters_port))) {
		printf("ln_publish error %d: %s\n", -ret, ln_format_error(ret));
		if(ln_get_error_message(clnt))
			printf("dynamic error: %s\n", ln_get_error_message(clnt));
		goto error_exit;
	}

	printf("sizeof ln_packet_tests_counters: %d bytes\n", sizeof(ln_packet_tests_counters));
	ln_packet_tests_counters p;

	// publish/write to port
	for(unsigned int i = 0; i < 10000000; i++) {

		
		
		p.counter = i;

		if((p.counter % 10) == 0) {
			printf("counter: %d\n", p.counter);
			fflush(stdout);
		}

		
		
		if((ret = ln_write(counters_port, &p, sizeof(p), NULL))) {
			printf("ln_write error %d: %s\n", -ret, ln_format_error(ret));
			if(ln_get_error_message(clnt))
				printf("%s\n", ln_get_error_message(clnt));
			goto error_exit;
		}
		
		nanosleep(&ts, NULL);
	}

	if((ret = ln_unpublish(&counters_port))) {
		printf("ln_unpublish error %d: %s\n", -ret, ln_format_error(ret));
		if(ln_get_error_message(clnt))
			printf("dynamic error: %s\n", ln_get_error_message(clnt));
	}

error_exit:
	printf("lnc exiting\n");
	ln_deinit(&clnt);

	return 0;
}
