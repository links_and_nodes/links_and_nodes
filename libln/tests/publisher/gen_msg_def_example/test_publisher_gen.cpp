#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#include "ln.h"

string generate_md_for_x_bytes(N, md_name, md_data) {
	char tmp[1024];
	snprintf(tmp, 1024, "uint8_t data[%d]", N);
	md_data = tmp;
	snprintf(tmp, 1024, "gen/dds/data_%d", N);
	md_name = tmp;
}


int main(int argc, char* argv[]) {
	ln_client clnt;
	if((ret = ln_init(&clnt, "test publisher md", argc, argv))) {
		printf("could not init ln_client. returned error %d: %s\n", -ret, ln_format_error(ret));
		return -1;
	}

	unsigned int N = 773;

	string md_name;
	string md_data;
	generate_md_for_x_bytes(N, md_name, md_data);
	ln_put_message_definition(clnt, md_name, md_data);
	
	ln_outport out_port;
	if((ret = ln_publish(clnt, "tests.counters", md_name.c_str(), &out_port))) {
		printf("ln_publish error %d: %s\n", -ret, ln_format_error(ret));
		goto error_exit;
	}

	void* data = malloc(N);
	while(true)  {
		if((ret = ln_write(out_port, data, N, NULL))) {
			printf("ln_write error %d: %s\n", -ret, ln_format_error(ret));
			goto error_exit;
		}
		// model rechnet....
	}

	if((ret = ln_unpublish(&counters_port))) {
		printf("ln_unpublish error %d: %s\n", -ret, ln_format_error(ret));
	}

error_exit:
	printf("lnc exiting\n");
	ln_deinit(&clnt);

	return 0;
}
