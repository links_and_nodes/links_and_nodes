#!/usr/bin/python

import sys
import time
import os

import signal

def on_sigterm(signo, frame):
    print "ignoring signal", signo


#signal.signal(signal.SIGTERM, on_sigterm)

do_flush = "-flush" in sys.argv

print "env: %r" % os.environ.get("TESTENV")
print "starting..."
if do_flush: sys.stdout.flush()
time.sleep(1.25)


print "ready"
while True:
    for i in xrange(10):
        print "i feel happy!"
        if do_flush: sys.stdout.flush()
        time.sleep(.5)
    for i in xrange(30):
        print "i feel sad!", i
        if do_flush: sys.stdout.flush()
        time.sleep(0.1)
    
    
