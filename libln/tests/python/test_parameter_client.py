#!/usr/bin/python
# -*- mode: python -*-

import sys
import links_and_nodes as ln
import time
import pprint

from numpy import *

def main(args):
    clnt = ln.client("py test", args)
    
    p1 = clnt.get_parameters("many_subs")

    #pprint.pprint(p1.get_dict())

    print p1

    ports = p1.Subsystem3.ln_parameters1.subscribe(10)

    i = 0
    while True:
        i += 1
        print "i: %d, p1: %s" % (i, p1.Subsystem3.ln_parameters1.const2)
        #print p1.Subsystem3.ln_parameters1
        #p2.const = i

        p1.Subsystem3.ln_parameters1.read()

        #time.sleep(0.5)
        #p1.refresh()

if __name__ == "__main__":
   main(sys.argv)
