#!/usr/bin/python
# -*- mode: python -*-

import sys
import links_and_nodes as ln
import time

from numpy import *

def main(args):
    seconds = 0.5

    clnt = ln.client(args[0], args)
    outport = clnt.publish("tests.counters", "tests/counters", buffers=1)
    p = outport.packet

    start_time = time.time()
    p.counter = 0
    while True:
        p.counter += 1
        
        #if (p.counter % 100) == 0:
        print "sending counter: %d" % p.counter
        outport.write()
        if p.counter % 4 == 0:
            print "ERROR: its mod 4!"
        
        time.sleep(seconds)
        
    print "finishing"

if __name__ == "__main__":
   main(sys.argv)
