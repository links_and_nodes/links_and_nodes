#!/usr/bin/python
# -*- mode: python -*-

import time
import sys
import links_and_nodes as ln

from numpy import *

def main(args):
    publisher_seconds = 0.4
    if len(args) > 1:
        N_buffers = int(args[1])
    else:
        N_buffers = 3

    sleep_time = publisher_seconds * N_buffers * 0.9
    sleep_step = N_buffers # N_buffers

    clnt = ln.client(args[0], args)
    inport = clnt.subscribe("tests.counters", "tests/counters", buffers=N_buffers)

    cnt = 0
    last = None
    while True:
        p = inport.read()
        if last is None:
            missed = 0
        else:
            missed = p.counter - last - 1
        print "counter: %d, missed: %d" % (p.counter, missed)
        last = p.counter
        
        cnt += 1
        if (cnt % sleep_step) == 0:
            print "sleep", sleep_time
            time.sleep(sleep_time)
        
    print "finishing"

if __name__ == "__main__":
   main(sys.argv)
