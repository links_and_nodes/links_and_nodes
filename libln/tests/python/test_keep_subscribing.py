#!/usr/bin/python
# -*- mode: python -*-

import time
import sys
import links_and_nodes as ln

from numpy import *

def main(args):
    clnt = ln.client("py test", args)
    inport = clnt.subscribe("tests.counters", rate=-1)
    #inport = clnt.subscribe("tests.counters", rate=ln.RATE_ON_REQUEST_LAST) # dynamically request message-def for this topic! only works if topic is already active!
    #inport = clnt.subscribe("tests.counters", "tests/counters", rate=ln.RATE_ON_REQUEST_LAST) # dynamically request message-def for this topic! only works if topic is already active!

    time.sleep(0.245)
    
    for i in xrange(20):
        print "request md!"
        ret = clnt.get_message_definition("tests/counters")
        print ret
        if "resp_fields" not in ret[0]:
            break
        time.sleep(0.0045)
    
    
    while False:
        p = inport.read(blocking=False)
        if p is None:
            print "no packet..."
        elif hasattr(p, "message"):
            p.message = p.message.split("\x00", 1)[0]
            print "counter: %d, %r" % (p.counter, p.message)
            print p.frame
        else:
            print p.get_dict()
        time.sleep(1)
        
    print "finishing"

if __name__ == "__main__":
   main(sys.argv)
