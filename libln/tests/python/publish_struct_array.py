#!/usr/bin/python
# -*- mode: python -*-

import sys
import links_and_nodes as ln
import time

from numpy import *

def main(args):
    clnt = ln.client("py test pub", args)

    N = 5
    md = """
define counter_t as "tests/counters"

counter_t counter[%d]
""" % N
    
    md_name = "tests/counters_array_of_%d" % N

    clnt.put_message_definition(md_name, md)

    outport = clnt.publish("counters_array", "gen/" + md_name)

    start_time = time.time()
    while True:
        t = time.time() - start_time
        for i in xrange(N):
            p = outport.packet.counter[i]
            p.counter += 1
            p.duration = time.time() - start_time
            p.frame[0] = p.counter / 10.
            p.frame[1] = sin(t)
            p.frame[2] = cos(t)
            p.frame[3] = i

        msg = "sending counter: %d, %.3fs" % (p.counter, p.duration)
        print msg
        outport.write()
        time.sleep(0.01)
    clnt.unpublish(outport)
    print "finishing"

if __name__ == "__main__":
   main(sys.argv)
