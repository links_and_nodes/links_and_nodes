import time
import gtk

class window(object):
    def __init__(self):
        self.w = gtk.Window()
        self.w.connect("delete_event", self.on_delete)
        self.img = gtk.Image()
        self.w.add(self.img)
        self.w.show_all()

    def show(self, img):
        self.img.set_from_pixbuf(gtk.gdk.pixbuf_new_from_array(img, gtk.gdk.COLORSPACE_RGB, 8))
        self.w.present()

    def on_delete(self, *args):
        self.w.hide()
        gtk.main_quit()

w = None
def show_image(img):
    global w
    if w is None:
        w = window()
    w.show(img)
    a = time.time()
    while time.time() - a < 1.5:
        gtk.main_iteration(False)
    
