#!/usr/bin/python

import time
import sys
import links_and_nodes as ln

dt = .5

topic_name = sys.argv[1]
clnt = ln.client("publisher", sys.argv)
port = clnt.publish(topic_name, "test/topic")

if "2" in topic_name:
    time.sleep(dt / 2)

print "ready"

port.packet.count = 0
while True:
    print "%s send %d" % (topic_name, port.packet.count)
    port.write()
    time.sleep(dt)

    port.packet.count += 1
