#include <stdio.h>
#include <unistd.h>
#include <exception>

#include <ln.h>

using namespace std;

#include "ln_messages.h"

int main(int argc, char* argv[]) {
	int* segfault_ptr = NULL;
	try {
		ln::client clnt("tester", argc, argv);
		ln::outport* port = clnt.publish("simple", "tests/counters", 10);
		
		ln_packet_tests_counters p;
		
		p.duration = 0.1;
		p.counter = 0;
		
		while(true) {
			p.duration += 0.1;
			p.counter += 1;
			double my_src_time = ln_get_time();
			
			port->write(&p, my_src_time);

			usleep(10000);

			/*if(p.counter > 10)
				p.counter = *segfault_ptr;
			*/
		}
	}
	catch(const exception& e) {
		printf("got exception: %s\n", e.what());
	}
	return 0;
}
