#!/usr/bin/python

import sys
import time

import links_and_nodes as ln

clnt = ln.client("publish", sys.argv)
args = clnt.get_remaining_args()[1:]

rate = 100
if len(args):
    rate = float(args[0])

dt = 0.1

do_unsub = False

if do_unsub:
    port2 = clnt.publish("B.C.topic2", "uint32_t", buffers=10)
    time.sleep(dt)

    port8 = clnt.publish("A.B.this is a really ugly very long topic name and i don't want to distrub the lnm gui with this very long title, because sometimes people do this...", "uint32_t", buffers=10)
    time.sleep(dt)

    port3 = clnt.publish("A.topic3", "uint32_t", buffers=10)
    time.sleep(dt)
    port4 = clnt.publish("A.topic2", "uint32_t", buffers=10)
    time.sleep(dt)

    port5 = clnt.publish("B.C.topic1", "uint32_t", buffers=10)
    time.sleep(dt)

    port6 = clnt.publish("B.A.topic2", "uint32_t", buffers=10)
    time.sleep(dt)
    port7 = clnt.publish("B.A.topic1", "uint32_t", buffers=10)
    time.sleep(dt)

port = clnt.publish("A.topic1", "uint32_t", buffers=10)
time.sleep(dt)

if do_unsub:
    time.sleep(0.1)

    clnt.unpublish(port2)
    time.sleep(dt)

    clnt.unpublish(port5)
    time.sleep(dt)

    clnt.unpublish(port3)
    time.sleep(dt)

print "ready"

wl = 1 / float(rate)
while True:
    port.packet.data += 1
    port.write()
    time.sleep(wl)
