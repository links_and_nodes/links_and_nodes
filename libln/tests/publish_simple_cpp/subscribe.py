#!/usr/bin/python

import sys
import time

import links_and_nodes as ln

clnt = ln.client("subscribe", sys.argv)
args = clnt.get_remaining_args()[1:]

if args:
    if len(args) == 1:
        port = clnt.subscribe(args[0])
    else:
        port = clnt.subscribe(args[0], rate=float(args[1]))
else:
    port = clnt.subscribe("simple")

if hasattr(port.packet, "data"):
    member = "data"
else:
    member = "counter"

print "ready"

if False:
    port.read()
    d = 5
    start = time.time()
    cnt = 0
    while True:
        port.read()
        cnt += 1
        print cnt

        last = time.time()
        if last - start >= d:
            break
    d = last - start
    print "%d packets in %.3fs = %.2fpkt/s" % (cnt, d, cnt / d)
    sys.exit(0)



log = []
last = time.time()
pcnt = 0
while True:
    have_packet = port.read(0.1) is not None
    now = time.time()
    if have_packet:
        pcnt += 1
        log.append((now, pcnt)) # getattr(port.packet, member

    while len(log) > 1 and now - log[0][0] > 1:
        del log[0]

    if now - last > 1:
        # est rate
        start_t, start_v = log[0]
        end_t, end_v = log[-1]
        end_t = time.time()
        N = end_v - start_v
        t = end_t - start_t
        print "rate: %.3f" % (N / t)
        last = now
    
