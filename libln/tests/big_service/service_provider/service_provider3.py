

import sys
import time
import links_and_nodes as ln

class my_provider(ln.service_provider):
    def __init__(self, clnt, name, group=None):
        ln.service_provider.__init__(self, clnt)
        self.wrap_service_provider(
            name, "large_call",
            method=self.handler, # if method kwargs is not specified, it would expect an large_service method.
            postprocessor=self._postprocess_string_list,
            pass_request=True,
            group_name=group)
        
    def _postprocess_string_list(self, svc, resp, value):
        resp.values = []
        for item in value:
            sp = resp.new_string_packet()
            sp.string = item
            resp.values.append(sp)

    def handler(self, request, names):
        print "have %d names:" % len(names)
        for i, name in enumerate(names):
            print " %d: %r" % (i, name.string)

        values = []
        for i in xrange(10):
            values.append("value %d\r\n" % i)
        return values
        
clnt = ln.client("service_provider", sys.argv)
p = my_provider(clnt, "large_service", group="my group")


handle_synchronous = True

if handle_synchronous:
    p.handle_service_group_requests("my group")
else:
    clnt.handle_service_group_in_thread_pool("my group", "pool1");
    clnt.set_max_threads("pool1", 10)

    print "ready!"
    # dummy mainloop instead of something real...
    import time
    while True:
        print "iterate..."
        time.sleep(1)
