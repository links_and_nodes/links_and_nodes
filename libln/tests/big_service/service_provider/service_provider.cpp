#include <list>

#include "ln.h"
#include <string_util/string_util.h>

// #define LN_UNREGISTER_SERVICE_IN_BASE_DETOR
#include "ln_messages.h"

using namespace std;

class provider : public ln_service_large_call_base {
public:
	ln::client clnt;
	bool keep_running;
	
	provider(int argc, char* argv[]) : clnt("service_provider", argc, argv) {
		register_large_call(&clnt, "large_service");
		keep_running = true;
	}

	// sync version
	virtual int on_large_call(ln::service_request& req, ln_service_large_call& svc) {
		printf("got request with %d names\n", svc.req.names_len);

		svc.resp.values = new _large_call_string[svc.req.names_len];
		svc.resp.values_len = svc.req.names_len;
		// svc.resp.values_len = 100;
		
		for(unsigned int i = 0; i < svc.req.names_len; i++) {
			string name = string(svc.req.names[i].string, svc.req.names[i].string_len);
			string expected = format_string("large name %d\n", i);
			if(name != expected)
				throw str_exception_tb("entry %d is invalid: got %s expected %s\n", i, repr(name).c_str(),  repr(expected).c_str());
			
			string value = format_string("value %d\r\n", i);
			svc.resp.values[i].string = strdup(value.c_str());
			svc.resp.values[i].string_len = value.size();
		}
		
		req.respond();

		for(unsigned int i = 0; i < svc.resp.values_len; i++)
			free(svc.resp.values[i].string);
		delete[] svc.resp.values;

		keep_running = false;
		return 0;
	}
	
	int run() {
		printf("provider ready\n");
		while(keep_running)
			clnt.wait_and_handle_service_group_requests(std::nullptr, 1);
		printf("exiting\n");
		return 0;
	}
};

int main(int argc, char* argv[]) {
	provider p(argc, argv);
	return p.run();
}
