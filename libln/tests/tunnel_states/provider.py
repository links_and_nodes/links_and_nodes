#!/usr/bin/python

import links_and_nodes as ln

import pprint
import os
import sys
from numpy import *

class provider(ln.service_provider):
    def __init__(self):
        self.clnt = ln.client(self.__class__.__name__, sys.argv)
        ln.service_provider.__init__(self, self.clnt, "test_prefix")

        self.wrap_service_provider("test_service", "test/test_service")
        self.frame_names = dict()

    def run(self):
        self.handle_service_group_requests()
        
    def test_service(self, trans1, trans2):
        print "received command:\n", trans1, trans2
        isReachable = 1
        return isReachable

if __name__ == "__main__":
    clnt = provider()
    clnt.run()
