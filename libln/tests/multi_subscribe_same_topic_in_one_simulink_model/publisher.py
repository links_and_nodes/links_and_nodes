#!/usr/bin/pathon

import sys
import time
import numpy as np
import links_and_nodes as ln

clnt = ln.client(sys.argv[0], sys.argv)

print "publish!"
port = clnt.publish("test_frame", "ln/frame34")
print "publish DONE!"

T = np.eye(4)
port.packet.frame = T[:3, :]

raw_input()
single_value_port = clnt.publish("single_value", "test/single_value")


while True:
    T[0, 0] += 1
    T[0, 2] += 0.1
    single_value_port.packet.value += 1
    port.write()
    single_value_port.write()
    time.sleep(0.1)

