import sys
from numpy import *
import links_and_nodes as ln
import time

clnt = ln.client("service_client this is my longish service provider name", sys.argv)

svc = clnt.get_service('this is my longish service provider name', 'serviceA')
svc.req.a = 0 # int
svc.req.b = 0 # int

# race condition:
port = clnt.subscribe("this is my longer topic name, to test this")
#time.sleep(0.25)
time.sleep(0.24)
svc.call_gobject() # blocking call but allowing gtk main loop to run
print svc.resp

print "done..."
time.sleep(2)
