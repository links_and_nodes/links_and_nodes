#!/usr/bin/python

import sys
import time
import links_and_nodes as ln

clnt = ln.client(sys.argv[0], sys.argv)

port = clnt.publish("this is my longer topic name, to test this", "topicA")

def on_service_request(svc_req, req, resp):
    print "request:", req
    svc_req.respond()
    return 0;

provider = clnt.get_service_provider("this is my longish service provider name", "serviceA")
provider.set_handler(on_service_request)
provider.do_register("my grp")
clnt.handle_service_group_in_thread_pool("my grp", "my pool")

print "ready"

while True:
    port.packet.v = time.time()
    port.write()

    time.sleep(0.01)
