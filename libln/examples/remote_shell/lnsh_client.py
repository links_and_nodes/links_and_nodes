#!/usr/bin/env python3

"""
lnsh client python wrapper.

can be directly called via command line like this:

./lnsh_client.py -ln_manager localhost:54411 rmc-uranos uname -a

"""

import sys
import pprint
import links_and_nodes as ln

class lnsh_client(ln.services_wrapper):
    def __init__(self, clnt, node):
        ln.services_wrapper.__init__(self, clnt, node)

        self._last_command_return = None
        self.wrap_service("command", "lnsh/command")
    

class lnsh_cli_client(lnsh_client):
    def __init__(self, clnt, node):
        lnsh_client.__init__(self, clnt, node)

        self._last_command_return = None
        self.services[node + ".command"].postprocessor=self._command_post

    def _command_post(self, data):
        self._last_command_return = data # keep full info
        if data["err"]:
            sys.stderr.write(data["err"])
        sys.stdout.write(data["out"])
        return data["returncode"]
    
if __name__ == "__main__":
    clnt = ln.client("lnsh_client", sys.argv)
    
    # example usage via command line:
    args = clnt.get_remaining_args()
    sh = lnsh_cli_client(clnt, args[1])
    sys.exit(sh.command(" ".join(args[2:])))
    
