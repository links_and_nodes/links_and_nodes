#ifndef LISTS_H
#define LISTS_H

int _ln_append(void*** list, unsigned int* s, unsigned int* n, void* elem);

#define DEFINE_LIST_DATA_TYPE_NAME(type_name, name)	\
	type_name** name ## s;							\
	unsigned int n_ ## name ## s;					\
	unsigned int name ## s_size
#define DEFINE_LIST_DATA_T(name) DEFINE_LIST_DATA_TYPE_NAME(name ## _t, name)
#define DEFINE_LIST_DATA(name)   DEFINE_LIST_DATA_TYPE_NAME(name, name)

#define DECLARE_LIST_METHODS_TYPE_NAME(container_type_name, type_name, name)	\
	int _ln_## container_type_name ## _append_ ## name(container_type_name*, type_name*);			\
	int _ln_## container_type_name ## _remove_ ## name(container_type_name*, type_name*)
#define DECLARE_LIST_METHODS_T(container_type_name, name) DECLARE_LIST_METHODS_TYPE_NAME(container_type_name, name ## _t, name)
#define DECLARE_LIST_METHODS(container_type_name, name)   DECLARE_LIST_METHODS_TYPE_NAME(container_type_name, name, name)

#define DEFINE_LIST_METHODS_TYPE_NAME(container_type_name, type_name, name) \
	int _ln_## container_type_name ## _append_ ## name(container_type_name* clnt, type_name* mapping) {			\
	    return _ln_append((void***)&clnt->name ## s, &clnt->name ## s_size, &clnt->n_ ## name ## s, mapping); \
	}																	\
	int _ln_## container_type_name ## _remove_ ## name(container_type_name* clnt, type_name* p) {				\
	    for(unsigned int i = 0; i < clnt->n_ ## name ## s; i++) {		\
		    if(clnt->name ## s[i] == p) {								\
			    for(unsigned int k = i + 1; k < clnt->n_ ## name ## s; k++)	\
				    clnt->name ## s[k - 1] = clnt->name ## s[k];		\
			    clnt->n_ ## name ## s --;								\
			    return 0;												\
		    }															\
	    }																\
	    return -LNE_PORT_NOT_FOUND;										\
    }
#define DEFINE_LIST_METHODS_T(container_type_name, name) DEFINE_LIST_METHODS_TYPE_NAME(container_type_name, name ## _t, name)
#define DEFINE_LIST_METHODS(container_type_name, name)   DEFINE_LIST_METHODS_TYPE_NAME(container_type_name, name, name)

#define DESTRUCT_LIST_TYPE_NAME(container, type_name, name)				\
	if(container->name ## s) {											\
    	for(unsigned int i = 0; i < container->n_ ## name ## s; i++) {		\
			type_name* elemp = container->name ## s[i];						\
			container->name ## s[i] = NULL;								\
			_ln_ ## name ## _destroy(elemp);								\
		}																\
		free(container->name ## s);										\
		container->name ## s = NULL; \
		container->n_ ## name ## s = 0; \
	}
#define DESTRUCT_LIST_T(container, name) DESTRUCT_LIST_TYPE_NAME(container, name ## _t, name)
#define DESTRUCT_LIST(container, name)   DESTRUCT_LIST_TYPE_NAME(container, name, name)


#endif // LISTS_H
