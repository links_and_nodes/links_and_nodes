/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "ln/ln.h"
#include "service.h"
#include "request.h"
#include "repr.h"
#include "thread_pool.h"

// todo: check return values of all resources!

thread_pool* find_thread_pool(client* clnt, const char* name) {
	for(unsigned int i = 0; i < clnt->n_thread_pools; i++) {
		thread_pool* pool = clnt->thread_pools[i];
		if(!strcmp(pool->name, name))
			return pool;
	}
	return NULL;
}

thread_pool* get_thread_pool(client* clnt, const char* pool_name) {
	thread_pool* pool = find_thread_pool(clnt, pool_name);
	if(pool) 
		return pool;
	// create new pool!
	pool = (thread_pool*)calloc(1, sizeof(thread_pool));
	pool->clnt = clnt;
	pool->max_n_threads = 1;
	pool->name = strdup(pool_name);
	ln_mutex_create(&pool->mutex);
	ln_cond_create(&pool->cond);
	pool->policy = -1; // not set
	pool->priority = -1; // not set
	pool->affinity_mask = 0; // not set
	_ln_client_append_thread_pool(clnt, pool);
	
	return pool;
}

void _ln_wait_for_pool_thread_idle(pool_thread* thread)
{
	ln_mutex_lock(&thread->data_mutex);
	while(thread->handler != NULL && thread->keep_running) {
		// block in condition
		double timeout = ln_get_monotonic_time() + 0.6;
		ln_cond_timedwait(&thread->cond, &thread->data_mutex, timeout);
	}
	ln_mutex_unlock(&thread->data_mutex);
}

void _ln_stop_pool_thread(pool_thread* t) {
	t->keep_running = false;
	ln_cond_signal(&t->cond);
	ln_thread_join(t->handle, NULL);
}
int _ln_pool_thread_destroy(pool_thread* t) {
	_ln_stop_pool_thread(t);
	ln_cond_destroy(&t->cond);
	ln_semaphore_destroy(t->in_use);
	ln_mutex_destroy(&t->data_mutex);
	free(t);
	return 0;
}

int _ln_thread_pool_destroy(thread_pool* p) {
	DESTRUCT_LIST(p, pool_thread);
	ln_mutex_destroy(&p->mutex);
	ln_cond_destroy(&p->cond);
	free(p->name);
	free(p);
	return 0;
}

int ln_set_max_threads(ln_client clnt_, const char* pool_name, unsigned int n_threads) {
	client* clnt = (client*)clnt_;
	lock_client(clnt);
	thread_pool* pool = get_thread_pool(clnt, pool_name);
	unlock_client(clnt);
	if(!pool) {
		_ln_set_error(clnt, "error: failed to get thread_pool '%s'!", pool_name);
		return -LNE_THREADP_INIT;
	}
	pool->max_n_threads = n_threads;
	// todo: is there already a blocked waiting thread request? if so: unblock if this number raised!
	// stop running threads?
	return 0;
}

int ln_thread_pool_setschedparam(ln_client clnt_, const char* pool_name, int policy, int priority, unsigned int affinity_mask) {
	client* clnt = (client*)clnt_;
	lock_client(clnt);
	thread_pool* pool = get_thread_pool(clnt, pool_name);
	unlock_client(clnt);
	if(!pool) {
		_ln_set_error(clnt, "error: failed to get thread_pool '%s'!", pool_name);
		return -LNE_THREADP_INIT;
	}
	pool->policy = policy;
	pool->priority = priority;
	pool->affinity_mask = affinity_mask;

	// now apply these to already running threads 
	ln_mutex_lock(&pool->mutex);
	for(unsigned int i = 0; i < pool->n_pool_threads; i++) {
		pool_thread* thread = pool->pool_threads[i];
		ln_thread_setschedparam(thread->handle, policy, priority, affinity_mask);
	}
	ln_mutex_unlock(&pool->mutex);
	return 0;
}

DEFINE_LIST_METHODS(thread_pool, pool_thread);
// missing: DESTRUCT_LIST(clnt, outport);

int trigger_pool_thread(pool_thread* thread, pool_thread_handler handler, void* user_data) {
	ln_debug(thread->pool->clnt, "trigger_pool_thread %p\n", thread);
	ln_mutex_lock(&thread->data_mutex);
	thread->handler = handler;
	thread->user_data = user_data;
	ln_mutex_unlock(&thread->data_mutex);
	ln_debug(thread->pool->clnt, "trigger_pool_thread %p signal!\n", thread);
	ln_cond_signal(&thread->cond);
	return 0;
}

#ifdef __WIN32__
thread_return_t WINAPI _pool_thread_spinner(void* data) {
#else
thread_return_t _pool_thread_spinner(void* data) {
#endif
	pool_thread* thread = (pool_thread*)data;
	{
		char thread_name[128];
		snprintf(thread_name, 128, "ln:sp:%s", thread->pool->name);
		ln_thread_setname(thread_name);
	}
	
	while(thread->keep_running) {
		// wait on condition until there is a handler to call!
		ln_mutex_lock(&thread->data_mutex);
		while(thread->handler == NULL) {			
			// block in condition
			double timeout = ln_get_monotonic_time() + 0.6;
			ln_cond_timedwait(&thread->cond, &thread->data_mutex, timeout);
			
			if(!thread->keep_running)
				break;
		}
		if(!thread->keep_running) {
			ln_mutex_unlock(&thread->data_mutex);
			break;
		}
		// call user handler
		ln_debug(thread->pool->clnt, "pool_thread_spinner %p was signaled -> call handler!!\n", thread);
		thread->handler(thread, thread->user_data);
		ln_debug(thread->pool->clnt, "pool_thread_spinner %p finished handler!!\n", thread);
		// cleanup
		thread->user_data = NULL;
		thread->handler = NULL;
		ln_mutex_unlock(&thread->data_mutex);
		
		// unlock thread lock
		ln_semaphore_post(thread->in_use);
		ln_debug(thread->pool->clnt, "pool_thread_spinner %p signal i'm free!\n", thread);
		ln_cond_signal(&thread->pool->cond);
	}
	return THREAD_RETURN_NULL;
}

int thread_pool_request_thread(thread_pool* pool, pool_thread_handler handler, void* user_data, pool_thread** thread_out) {
	// search existing free thread
	ln_debug(pool->clnt, "thread_pool_request_thread pool %s (have %d of %d threads)\n", pool->name, pool->n_pool_threads, pool->max_n_threads);
	ln_mutex_lock(&pool->mutex);
	while(true) {
		for(unsigned int i = 0; i < pool->n_pool_threads; i++) {
			pool_thread* thread = pool->pool_threads[i];
			if(ln_semaphore_trywait(thread->in_use)) {
				ln_debug(pool->clnt, "thread %d, %#x is blocked\n", i, thread);
				continue; // failed
			}
			// got thread!
			ln_debug(pool->clnt, "thread_pool_request_thread got free thread %d: %p!\n", i, thread);
			ln_mutex_unlock(&pool->mutex);
			if(thread_out)
				*thread_out = thread;
			return trigger_pool_thread(thread, handler, user_data);
		}
		// no free thread -> spawn new one?
		
		if(pool->max_n_threads > pool->n_pool_threads) {
			// yes spawn new thread!
			ln_debug(pool->clnt, "no free thread -- spawn new one!\n");
			pool_thread* thread = (pool_thread*)calloc(1, sizeof(pool_thread));
			thread->pool = pool;
			ln_semaphore_init(&thread->in_use, 0); // in use!
			ln_mutex_create(&thread->data_mutex);
			ln_cond_create(&thread->cond);
			thread->handler = NULL;
			_ln_thread_pool_append_pool_thread(pool, thread);
			thread->keep_running = 1;
			// start asynchronous processing thread!
			ln_thread_create_with_params(&thread->handle, _pool_thread_spinner, thread, pool->policy, pool->priority, pool->affinity_mask, NULL);
			
			ln_mutex_unlock(&pool->mutex);
			if(thread_out)
				*thread_out = thread;
			return trigger_pool_thread(thread, handler, user_data);
		} else
			ln_debug(pool->clnt, "no free thread!\n");
		// no free thread. wait on condition for next free thread and try again!
		ln_cond_wait(&pool->cond, &pool->mutex);
	}
}
