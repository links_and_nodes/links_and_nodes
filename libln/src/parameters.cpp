#include <algorithm>
#include <cstdlib>
#include <stdexcept>
#include <iostream>

#include <string_util/string_util.h>

#include <ln/client_lib_messages.h>
#include <ln/parameters.h>

using namespace std;
using namespace string_util;

namespace ln {
namespace parameters {
// Port Info
class port_info {
  public:
	string name;
	string description;
	uint32_t width;
	uint32_t type_id;
	uint32_t element_size;
	uint32_t size;

	uint32_t num_dims;
	uint32_t *dims;

	void *input, *output;

	std::function<bool(void *)> before_update_callback;
	std::function<void(void *)> after_update_callback;

	bool new_override;
	ln_parameters_value_t query_value;
	parameter_block *block;

	uint32_t packet_offset;  // offset in published topic packet

	port_info(parameter_block *block, string name, string description) : name(name), description(description), dims(nullptr), input(nullptr), output(nullptr), new_override(false), block(block) {
		memset(&query_value, 0, sizeof(query_value));
		packet_offset = 0;
	};

	~port_info() {
		if(dims)
			delete[] dims;
		free(query_value.input_data);
		free(query_value.output_data);
		free(query_value.data_type);
		free(query_value.name);
	};

	string get_data_type_string() {
		stringstream ss;
		ss << _get_data_type_string(type_id);
		if(dims[0] > 1 || num_dims > 1) {
			if(num_dims > 1)
				ss << "[";
			for(unsigned int i = 0; i < num_dims; i++) {
				if(i > 0)
					ss << ", ";
				ss << dims[i];
			}
			if(num_dims > 1)
				ss << "]";
		}
		return ss.str();
	}

	string _get_md_data_type_string() {
		return _get_md_data_type_string(type_id);
	}

	string get_md_dims_string() {
		stringstream ss;
		if(width > 1)
			ss << "[" << width << "]";
		return ss.str();
	}

	void set_size(port_description_void2 &description) {
		width = description.width;
		type_id = description.type_id;
		element_size = _get_data_type_size(type_id);
		size = width * element_size;

		num_dims = description.num_dims;
		dims = new uint32_t[num_dims];
		if(num_dims > 1) {
			for(unsigned int k = 0; k < num_dims; k++)
				dims[k] = description.dims[k];
		} else {
			dims[0] = width;
		}

		input = description.input;
		output = description.output;

		before_update_callback = description.before_update_callback;
		after_update_callback = description.after_update_callback;

		query_value.input_data_len = size;
		query_value.input_data = (uint8_t *)calloc(1, size);
		query_value.output_data_len = size;
		query_value.output_data = (uint8_t *)calloc(1, size);
		query_value.override_enabled = 0;
		query_value.data_type = strdup(get_data_type_string().c_str());
		query_value.data_type_len = strlen(query_value.data_type);
		query_value.name = NULL;
	}

	void transfer(const void *in, void *out);

	void transfer() {
		transfer(input, output);
	}

	static uint32_t _get_data_type_size(uint32_t type_id) {
		switch(type_id) {
			case LN_DOUBLE:
				return sizeof(double);
			case LN_SINGLE:
				return sizeof(float);
			case LN_INT8:
			case LN_UINT8:
				return sizeof(int8_t);
			case LN_INT16:
			case LN_UINT16:
				return sizeof(int16_t);
			case LN_INT32:
			case LN_UINT32:
				return sizeof(int32_t);
			case LN_BOOLEAN:
				return 1;
			case LN_INT64:
			case LN_UINT64:
				return sizeof(int64_t);
		}
		return 0;
	}

	static string _get_data_type_string(uint32_t type_id) {
		switch(type_id) {
			case LN_DOUBLE:
				return "double";
			case LN_SINGLE:
				return "float";
			case LN_INT8:
				return "int8";
			case LN_UINT8:
				return "uint8";
			case LN_INT16:
				return "int16";
			case LN_UINT16:
				return "uint16";
			case LN_INT32:
				return "int32";
			case LN_UINT32:
				return "uint32";
			case LN_BOOLEAN:
				return "bool";
			case LN_INT64:
				return "int64";
			case LN_UINT64:
				return "uint64";
		}
		return "<unknown>";
	}

	static string _get_md_data_type_string(uint32_t type_id) {
		switch(type_id) {
			case LN_DOUBLE:
				return "double";
			case LN_SINGLE:
				return "float";
			case LN_INT8:
				return "int8_t";
			case LN_UINT8:
				return "uint8_t";
			case LN_INT16:
				return "int16_t";
			case LN_UINT16:
				return "uint16_t";
			case LN_INT32:
				return "int32_t";
			case LN_UINT32:
				return "uint32_t";
			case LN_BOOLEAN:
				return "uint8_t";
			case LN_INT64:
				return "int64_t";
			case LN_UINT64:
				return "uint64_t";
		}
		return "<unknown>";
	}

	string get_md_data_type_short() {
		string dtc = _get_md_data_type_char(type_id);
		if(width == 1)
			return dtc;
		return format_string("%s%d", dtc.c_str(), width);
	}

	static string _get_md_data_type_char(uint32_t type_id) {
		switch(type_id) {
			case LN_DOUBLE:
				return "d";
			case LN_SINGLE:
				return "f";
			case LN_INT8:
				return "b";
			case LN_UINT8:
				return "B";
			case LN_INT16:
				return "h";
			case LN_UINT16:
				return "H";
			case LN_INT32:
				return "i";
			case LN_UINT32:
				return "I";
			case LN_BOOLEAN:
				return "B";
			case LN_INT64:
				return "q";
			case LN_UINT64:
				return "Q";
		}
		return "<unknown>";
	}

	string format_input_data() {
		return format_data(query_value.input_data);
	}

	string format_output_data() {
		return format_data(query_value.output_data);
	}

	string _format_scalar(void *data) {
		switch(type_id) {
			case LN_DOUBLE:
				return repr(*(double*)data);
			case LN_SINGLE:
				return repr(*(float*)data);
			case LN_INT8:
				return repr((int32_t) *(int8_t *)data);
			case LN_UINT8:
				return repr((uint32_t) *(uint8_t *)data);
			case LN_INT16:
				return repr((int32_t) *(int16_t *)data);
			case LN_UINT16:
				return repr((uint32_t) *(uint16_t *)data);
			case LN_INT32:
				return repr(*(int32_t*)data);
			case LN_UINT32:
				return repr(*(uint32_t*)data);
			case LN_BOOLEAN:
				return repr(*(bool*)data);
			case LN_INT64:
				return repr(*(int64_t*)data);
			case LN_UINT64:
				return repr(*(uint64_t*)data);
		}
		return "<unknown scalar>";
	}

	string::size_type _write_scalar(void *data, string s) {
		switch(type_id) {
			case LN_DOUBLE:
				*(double *)data = atof(s.c_str());
				return s.size();
			case LN_SINGLE:
				*(float *)data = atof(s.c_str());
				return s.size();
			case LN_INT8:
				*(int8_t *)data = (int8_t)atoi(s.c_str());
				return s.size();
			case LN_UINT8:
				*(uint8_t *)data = (uint8_t)atoi(s.c_str());
				return s.size();
			case LN_INT16:
				*(int16_t *)data = (int16_t)atoi(s.c_str());
				return s.size();
			case LN_UINT16:
				*(uint16_t *)data = (uint16_t)atoi(s.c_str());
				return s.size();
			case LN_INT32:
				*(int32_t *)data = (int32_t)atoi(s.c_str());
				return s.size();
			case LN_UINT32:
				*(uint32_t *)data = (uint32_t)atoll(s.c_str());
				return s.size();
			case LN_BOOLEAN:
				s = strip(s);
				if(isdigit(s.substr(0, 1).c_str()[0]))
					*(bool *)data = (bool)atoi(s.c_str());
				else if(s == "True")
					*(bool*)data = (bool)1;
				else
					*(bool*)data = (bool)0;
				return s.size();
			case LN_INT64:
				*(int64_t *)data = (int64_t)strtoull(s.c_str(), nullptr, 10);
				return s.size();
			case LN_UINT64:
				*(uint64_t *)data = strtoull(s.c_str(), nullptr, 10);
				return s.size();
		}
		return 0;
	}

	bool is_array() {
		if(num_dims == 1 && width == 1) {  // scalar
			return false;
		} else if(num_dims == 1 || (num_dims == 2 && dims[0] == 1)) {  // 1d array
			return true;
		} else if(num_dims == 2) {  // 2d array
			return true;
		}
		return false;
	}

	bool is_2d_array() {
		if(num_dims == 1 && width == 1) {  // scalar
			return false;
		} else if(num_dims == 1 || (num_dims == 2 && dims[0] == 1)) {  // 1d array
			return false;
		} else if(num_dims == 2) {  // 2d array
			return true;
		}
		return false;
	}

	string format_data(void *data) {
		stringstream ss;
		if(num_dims == 1 && width == 1) {  // scalar
			ss << _format_scalar(data);
		} else if(num_dims == 1 || (num_dims == 2 && dims[0] == 1)) {  // 1d array
			ss << "array([";
			uint8_t *dp = (uint8_t *)data;
			for(unsigned int idx = 0; idx < width; idx++) {
				if(idx > 0)
					ss << ", ";
				ss << _format_scalar(dp);
				dp += element_size;
			}
			ss << "], dtype=" << _get_data_type_string(type_id) << ")";
		} else if(num_dims == 2) {  // 2d array
			ss << "array([";
			uint8_t *dp = (uint8_t *)data;
			for(unsigned int row = 0; row < dims[0]; row++) {
				if(row > 0)
					ss << ", ";
				ss << "[";
				for(unsigned int col = 0; col < dims[1]; col++) {
					uint8_t *p = dp + (row + col * dims[0]) * element_size;
					if(col > 0)
						ss << ", ";
					ss << _format_scalar(p);
				}
				ss << "]";
			}
			ss << "], dtype=" << _get_data_type_string(type_id) << ")";
		}
		return ss.str();
	}
};

// Parameter Server
static ln_parameter_server *_paramsvr = NULL;
static std::mutex _paramsvr_mutex;

#define assign_to_string_field(field, str)           do { field = (char*)str.c_str(); field ## _len = str.size(); } while (0) // str CAN NOT BE a temporary
#define assign_const_char_to_string_field(field, cc) do { field = (char*)cc;          field ## _len = strlen(cc); } while (0)

class ln_parameter_server : public query_base, public query_dict_base, public override_dict_single_base, public request_parameter_topic_base {
	typedef list<parameter_block *> blocks_t;
	typedef map<string, blocks_t> parameter_groups_t;
	parameter_groups_t parameter_groups;

	ln::client *clnt;
	std::string svc_group_name;
	bool sync_handling;
	parameter_block *sync_handling_block;

	ln_parameter_server() {
		clnt = NULL;
	}

	~ln_parameter_server() {}

	bool _init_ln(parameter_block *block, ln::client *client) {
		// We need a private ln::client for the Simulink use case:
		// shared ln clients share the same underlying ln_client which has a usage counter, but
		// the ln::client we get here might have been deleted already before _deinit_ln is called.
		this->clnt = new ln::client(client->clnt);

		svc_group_name = "parameter_services";

		string svc_name = "ln.parameters.query.";
		svc_name += this->clnt->name;
		register_query(this->clnt, svc_name, svc_group_name.c_str());

		svc_name = "ln.parameters.query_dict.";
		svc_name += this->clnt->name;
		register_query_dict(this->clnt, svc_name, svc_group_name.c_str());

		svc_name = "ln.parameters.override_dict_single.";
		svc_name += this->clnt->name;
		register_override_dict_single(this->clnt, svc_name, svc_group_name.c_str());

		svc_name = "ln.parameters.request_topic.";
		svc_name += this->clnt->name;
		register_request_parameter_topic(this->clnt, svc_name, svc_group_name.c_str());

		char *sync_handling_env = getenv("LN_PARAMETER_SYNC_HANDLING");
		sync_handling = sync_handling_env && sync_handling_env[0] == '1';
		if(!sync_handling) {
			if(std::getenv("LN_PARAMETER_DEBUG"))
				printf(
					"ln_parameter_server will handle service requests in thread "
					"pool!\n");
			string svc_group_pool_name = "parameter_service_pool";
			this->clnt->handle_service_group_in_thread_pool(svc_group_name.c_str(), svc_group_pool_name);
			// sync_handling_block->trigger_sync_handling = NULL;
			// sync_handling_block = NULL;
		} else {
			if(std::getenv("LN_PARAMETER_DEBUG"))
				printf(
					"ln_parameter_server will handle service requests in sync! this "
					"might delay the main-rate-thread!\n");
		}
		sync_handling_block = NULL;
		return true;
	}

	void _deinit_ln() {
		unregister_query();
		unregister_query_dict();
		unregister_override_dict_single();
		unregister_request_parameter_topic();
		delete this->clnt;
		this->clnt = nullptr;
	}

  public:
	static ln_parameter_server *get() {
		_paramsvr_mutex.lock();
		if(_paramsvr == NULL)
			_paramsvr = new ln_parameter_server();
		_paramsvr_mutex.unlock();
		return _paramsvr;
	}

	void do_handle_service_group() {
		this->clnt->wait_and_handle_service_group_requests(svc_group_name.c_str(), 0);
	}

	bool register_block(parameter_block *block, ln::client *client) {
		bool success = true;
		_paramsvr_mutex.lock();
		if(!clnt)
			success = _init_ln(block, client);
		bool error_free = true;
		for(parameter_block::port_infos_t::iterator j = block->port_infos.begin(); j != block->port_infos.end(); j++) {
			port_info *pi = *j;
			string port_name = string(pi->query_value.name, pi->query_value.name_len);
			bool already_used = false;
			for(parameter_groups_t::iterator i = parameter_groups.begin(); i != parameter_groups.end(); ++i) {
				blocks_t &blocks = i->second;
				for(blocks_t::iterator k = blocks.begin(); k != blocks.end(); k++) {
					parameter_block *oblock = *k;
					vector<port_info *> matching_ports = oblock->get_matching_ports(port_name);
					if(matching_ports.size()) {
						if(std::getenv("LN_PARAMETER_DEBUG"))
							printf(
								"parameter %s.%s of dt %s is already provided by other "
								"block!\n",
								block->parameter_group_name.c_str(),
								pi->name.c_str(),
								pi->get_data_type_string().c_str());
						success = false;
						already_used = true;
					}
				}
			}
			if(already_used)
				error_free = false;
		}
		if(error_free) {
			block->clnt = clnt;
			parameter_groups[block->parameter_group_name].push_back(block);
		} else  // error!
			success = false;

		if(sync_handling && sync_handling_block == NULL) {
			block->trigger_sync_handling = this;
			sync_handling_block = block;
		}
		_paramsvr_mutex.unlock();
		return success;
	}

	void unregister_block(parameter_block *block) {
		_paramsvr_mutex.lock();
		blocks_t &blocks = parameter_groups[block->parameter_group_name];
		blocks_t::iterator i = std::find(blocks.begin(), blocks.end(), block);
		if(i != blocks.end())
			blocks.erase(i);
		if(!blocks.size())
			parameter_groups.erase(block->parameter_group_name);

		// if that was the last block, unregister parameter service providers
		if(parameter_groups.size() == 0) {
			_deinit_ln();
                }
		_paramsvr_mutex.unlock();
	}

	std::tuple<vector<ln_parameters_value_t>, vector<port_info *>> _copy_select_parameters(string pattern) {
		vector<ln_parameters_value_t> parameters;
		vector<port_info *> ports;
		int parameter_count = 0;
		for(parameter_groups_t::iterator i = parameter_groups.begin(); i != parameter_groups.end(); ++i) {
			blocks_t &blocks = i->second;
			for(blocks_t::iterator k = blocks.begin(); k != blocks.end(); k++) {
				parameter_block *block = *k;
				vector<port_info *> matching_ports = block->get_matching_ports(pattern);
				// alloc memory
				for(unsigned int i = 0; i < matching_ports.size(); i++) {
					port_info *port = matching_ports[i];
					ports.push_back(port);
					parameters.push_back(port->query_value);  // copy pointers!
					ln_parameters_value_t &value = parameters.back();
					value.input_data = (uint8_t *)malloc(value.input_data_len);
					value.output_data = (uint8_t *)malloc(value.input_data_len);
				}
				// lock block during the following operation
				{
					std::lock_guard<std::mutex> lck(block->_mutex);
					// copy data
					for(unsigned int i = 0; i < matching_ports.size(); i++) {
						port_info *port = matching_ports[i];
						ln_parameters_value_t &value = parameters[parameter_count];
						memcpy(value.input_data, port->query_value.input_data, port->query_value.input_data_len);
						memcpy(value.output_data, port->query_value.output_data, port->query_value.output_data_len);
						parameter_count++;
					}
				}
			}
		}
		return {parameters, ports};
	}

	void _delete_select_parameters(vector<ln_parameters_value_t> &parameters) {
		for(unsigned int i = 0; i < parameters.size(); i++) {
			free(parameters[i].input_data);
			free(parameters[i].output_data);
		}
	}

	virtual int on_query(ln::service_request &req, ln_parameters_query_t &svc) {
		vector<ln_parameters_value_t> parameters = std::get<0>(_copy_select_parameters(string(svc.req.pattern, svc.req.pattern_len)));
		svc.resp.values = &parameters[0];
		svc.resp.values_len = parameters.size();
		req.respond();
		_delete_select_parameters(parameters);
		return 0;
	}

	virtual int on_query_dict(ln::service_request &req, ln_parameters_query_dict_t &svc) {
		stringstream output;
		output << "{\n";

		std::tuple<vector<ln_parameters_value_t>, vector<port_info *>> selection =
			_copy_select_parameters(string(svc.req.pattern, svc.req.pattern_len));
		vector<ln_parameters_value_t> &parameters = std::get<0>(selection);
		vector<port_info *> &port_infos = std::get<1>(selection);
		for(unsigned int i = 0; i < parameters.size(); i++) {
			ln_parameters_value_t &value = parameters[i];
			port_info *port = port_infos[i];
			output << "  " << repr(value.name, value.name_len, false) << ": dict(input=" << port->format_data(value.input_data)
				   << ", output=" << port->format_data(value.output_data) << ", override_enabled=" << repr((bool)value.override_enabled)
				   << ", description=" << repr(port->description) << "),\n";
		}
		_delete_select_parameters(parameters);

		output << "}\n";
		auto output_str = output.str();
		assign_to_string_field(svc.resp.data, output_str);
		req.respond();
		return 0;
	}

	virtual int on_override_dict_single(ln::service_request &req, ln_parameters_override_dict_single_t &svc) {
		svc.resp.error_len = 0;
		try {
			// search parameter
			string name(svc.req.parameter_name, svc.req.parameter_name_len);
			port_info *port = search_parameter(name);
			string port_name(port->query_value.name, port->query_value.name_len);
			// decode override_data
			if(svc.req.override_data_len == 0) {
				// disable override
				port->query_value.override_enabled = 0;

			} else {
				// decode data
				string data(svc.req.override_data, svc.req.override_data_len);
				data = strip(data);
				std::vector<uint8_t> decoded_data(port->query_value.output_data_len);
				bool is_array = data.substr(0, 6) == "array(";
				if(is_array && !port->is_array())
					throw str_exception_tb("parameter %s is of scalar-type while your data is an array!", repr(port_name).c_str());
				if(!is_array && port->is_array())
					throw str_exception_tb("parameter %s is of array-type while your data is a scalar!", repr(port_name).c_str());
				if(!is_array) {
					// decode scalar
					port->_write_scalar(&decoded_data[0], data);
				} else {
					bool is_2d_array = data.substr(0, 6 + 2) == "array([[";
					if(is_2d_array && !port->is_2d_array())
						throw str_exception_tb("parameter %s is a 1d-array while your data is a 2d-array!", repr(port_name).c_str());
					if(!is_2d_array && port->is_2d_array())
						throw str_exception_tb("parameter %s is a 2d-array while your data is a 1d-array!", repr(port_name).c_str());
					if(!is_2d_array) {
						// decode 1d array
						string::size_type start = 7;  // behind '['
						uint8_t *dp = &decoded_data[0];
						unsigned int element = 0;
						bool done = false;
						while(true) {
							// search next ',' or closing ']'
							string::size_type p = data.find_first_of(",]", start);
							if(p == string::npos)
								throw str_exception_tb(
									"you provided invalid syntax for parameter %s - missing "
									"closing ] in\n%s",
									repr(port_name).c_str(),
									repr(data).c_str());
							if(data[p] == ']')  // done
								done = true;
							// next element
							element++;
							if(element > port->width)
								throw str_exception_tb(
									"you provided too many (%d) elements for parameter %s with "
									"%d elements in\n%s",
									element,
									repr(port_name).c_str(),
									port->width,
									repr(data).c_str());
							string value = data.substr(start, p - start);
							port->_write_scalar(dp, value);
							if(done)
								break;
							dp += port->element_size;
							start = p + 1;
						}
						if(element != port->width)
							throw str_exception_tb(
								"you provided too few (%d) elements for "
								"parameter %s with %d elements in\n%s",
								element,
								repr(port_name).c_str(),
								port->width,
								repr(data).c_str());

					} else {
						// decode 2d array
						string::size_type start = 8;  // behind '[['
						uint8_t *dp = &decoded_data[0];
						bool done = false;
						for(unsigned int row = 0; row < port->dims[0]; row++) {
							// read row
							done = false;
							unsigned int element = 0;
							while(true) {
								// search next ',' or closing ']'
								string::size_type p = data.find_first_of(",]", start);
								if(p == string::npos)
									throw str_exception_tb(
										"you provided invalid syntax for parameter %s in row %d "
										"- missing closing ] in\n%s",
										repr(port_name).c_str(),
										row,
										repr(data).c_str());
								if(data[p] == ']')  // done
									done = true;
								// next element
								element++;
								if(element > port->dims[1])
									throw str_exception_tb(
										"you provided too many (%d) elements in row %d for "
										"parameter %s with %d elements in\n%s",
										element,
										row,
										repr(port_name).c_str(),
										port->dims[1],
										repr(data).c_str());
								string value = data.substr(start, p - start);
								unsigned int col = element - 1;

								port->_write_scalar(dp + (col * port->dims[0] + row) * port->element_size, value);
								start = p + 1;
								if(done)
									break;
							}
							if(element != port->dims[1])
								throw str_exception_tb(
									"you provided too few (%d) elements in row %d for "
									"parameter %s with %d elements in\n%s",
									element,
									row,
									repr(port_name).c_str(),
									port->dims[1],
									repr(data).c_str());
							// search next opening '[' or closing ']'
							done = false;
							string::size_type p = data.find_first_of("][", start);
							if(p == string::npos)
								throw str_exception_tb(
									"you provided invalid syntax for parameter %s after row %d "
									"- missing closing ] or next opening [ in\n%s",
									repr(port_name).c_str(),
									row,
									repr(data).c_str());
							if(data[p] == ']') {
								// no next row!!
								if(row < port->dims[0] - 1)
									throw str_exception_tb(
										"you provided too few rows (%d) for "
										"parameter %s with %d rows in\n%s",
										row + 1,
										repr(port_name).c_str(),
										port->dims[0],
										repr(data).c_str());
								done = true;
								break;  // done
							}
							start = p + 1;  // continue with next row
						}
						if(!done)
							throw str_exception_tb(
								"you provided too many rows for parameter "
								"%s with %d rows in\n%s",
								repr(port_name).c_str(),
								port->dims[0],
								repr(data).c_str());
						// todo: transpose!
					}
				}
				if(!static_cast<bool>(port->before_update_callback) || port->before_update_callback(&decoded_data[0])) {
					// copy data
					std::unique_lock<std::mutex> lk(port->block->_mutex);
					memcpy(port->query_value.output_data, &decoded_data[0], port->query_value.output_data_len);
					// enable override
					port->query_value.override_enabled = 1;
					port->block->wait_step = svc.req.wait_step;
					port->new_override = true;
					if(svc.req.wait_step && !sync_handling) {
						uint32_t wait_counter = port->block->wait_counter;
						using namespace std::chrono_literals;
						if(!port->block->wait_cond.wait_for(lk, 10s, [&] { return port->block->wait_counter != wait_counter; })) {
							assign_const_char_to_string_field(svc.resp.error, "Timout occured during wait_step");
							req.respond();
							return 0;
						}
					}
				} else {
					assign_const_char_to_string_field(svc.resp.error, "Parameter did not pass validity checking");
					req.respond();
					return 0;
				}
			}
		} catch(const std::exception &e) {
			std::string err(e.what());
			assign_to_string_field(svc.resp.error, err);
			req.respond();
			return 0;
		}
		req.respond();
		return 0;
	}

	port_info *search_parameter(string name) {
		// search parameter
		vector<port_info *> all_matching_ports;
		for(parameter_groups_t::iterator i = parameter_groups.begin(); i != parameter_groups.end(); ++i) {
			blocks_t &blocks = i->second;
			for(blocks_t::iterator k = blocks.begin(); k != blocks.end(); k++) {
				parameter_block *block = *k;
				vector<port_info *> matching_ports = block->get_matching_ports(name);
				if(!matching_ports.size())
					continue;
				all_matching_ports.insert(all_matching_ports.end(), matching_ports.begin(), matching_ports.end());
			}
		}
		if(!all_matching_ports.size())
			throw str_exception_tb("there is no parameter matching this name: %s", repr(name).c_str());
		if(all_matching_ports.size() > 1) {
			stringstream param_names;
			for(unsigned int i = 0; i < all_matching_ports.size(); i++) {
				if(i > 0)
					param_names << ", ";
				param_names << repr(string(all_matching_ports[i]->query_value.name, all_matching_ports[i]->query_value.name_len));
			}
			throw str_exception_tb("there are multiple parameters matching this name: %s\n%s", repr(name).c_str(), param_names.str().c_str());
		}
		return all_matching_ports[0];
	}

	virtual int on_request_parameter_topic(ln::service_request &req, ln_parameters_request_parameter_topic_t &svc) {
		svc.resp.error_len = 0;

		try {
			string parameter_names_(svc.req.parameters, svc.req.parameters_len);
			vector<string> parameter_names = split_string(parameter_names_, ",");
			if(!parameter_names.size())
				throw str_exception_tb("requested parameter list is empty: %s", repr(parameter_names_).c_str());

			typedef map<parameter_block *, list<string>> blocks_t;
			blocks_t blocks;
			for(unsigned int i = 0; i < parameter_names.size(); i++) {
				// search parameter
				string name = strip(parameter_names[i]);
				if(!name.size())
					continue;
				port_info *port = search_parameter(name);
				blocks[port->block].push_back(name);
			}
			if(!blocks.size())
				throw str_exception_tb("requested parameter list is empty: %s", repr(parameter_names_).c_str());
			stringstream parameter_topics;
			parameter_topics << "{\n";
			for(blocks_t::iterator i = blocks.begin(); i != blocks.end(); ++i) {
				parameter_block *block = i->first;
				block->publish();
				for(list<string>::iterator k = i->second.begin(); k != i->second.end(); k++)
					parameter_topics << "  " << repr(*k) << ": (" << repr(block->outport->topic_name) << ", " << repr(block->outport->topic_md)
									 << "),\n";
			}
			parameter_topics << "}\n";
			auto parameter_topics_str = parameter_topics.str();
			assign_to_string_field(svc.resp.parameter_topics, parameter_topics_str);
			req.respond();
			return 0;
		} catch(const std::exception &e) {
			std::string err(e.what());
			assign_to_string_field(svc.resp.error, err);
			req.respond();
			return 0;
		}
	}
};

void port_info::transfer(const void *in, void *out) {
	const void *out_src = NULL;
	memcpy(query_value.input_data, in, size);
	if(!query_value.override_enabled) {
		// in -> out
		out_src = in;
		memcpy(query_value.output_data, in, size);
	} else {
		// last->out to out
		out_src = query_value.output_data;
	}
	if(out)
		memcpy(out, out_src, size);

	if(block->outport && !block->disable_outport) {
		// copy to ln packet
		uint8_t *p = (uint8_t *)block->outport_packet;
		p += packet_offset;
		memcpy(p, query_value.input_data, size);
		p += size;
		if(out) {
			memcpy(p, out_src, size);
			p += size;
			*p = query_value.override_enabled;
		}
	}

	if(new_override && static_cast<bool>(after_update_callback)) {
		after_update_callback(query_value.output_data);
	}
	new_override = false;
}

// Parameter block implementation
parameter_block::parameter_block(ln::client *clnt_, string parameter_group_name_, bool always_publish_, string topic_name_) :
	parameter_group_name(parameter_group_name_),
	always_publish(always_publish_),
	topic_name(topic_name_),
	parameters_registered(false) {
	clnt = clnt_;
	outport = NULL;
	outport_packet = NULL;
	wait_counter = 0;
	wait_step = false;
	trigger_sync_handling = NULL;

	topic_name = strip(topic_name);

	// prepend LN_PARAMETER_PREFIX from environment
	const char *env_prefix = getenv("LN_PARAMETER_PREFIX");
	if(env_prefix)
		parameter_group_name = string(env_prefix) + parameter_group_name;
}

// old function, calls new implementation with signal description
bool parameter_block::register_parameters(std::vector<port_description_void> &port_descriptions) {
    std::vector<port_description_void2> port_descriptions_with_signal_description;
    for(auto & port_desc: port_descriptions) {
        port_description_void2 port_desc_updated = {
                .signal_name = port_desc.signal_name,
                .signal_description = nullptr,
                .width = port_desc.width,
                .type_id = port_desc.type_id,
                .num_dims = port_desc.num_dims,
                .dims = port_desc.dims,
                .input = port_desc.input,
                .output = port_desc.output,
                .before_update_callback = port_desc.before_update_callback,
                .after_update_callback = port_desc.after_update_callback
        };
        port_descriptions_with_signal_description.push_back(port_desc_updated);
    }
    return register_parameters(port_descriptions_with_signal_description);
}

bool parameter_block::register_parameters(std::vector<port_description_void2> &port_descriptions) {
	// this method can only be called once
	if(parameters_registered) {
		throw std::logic_error("register_parameters can only be called once!");
	}
	parameters_registered = true;

	for(auto& port_desc : port_descriptions) {
		std::string description = (port_desc.signal_description == nullptr) ? "" : port_desc.signal_description;
		port_info *info = new port_info(this, port_desc.signal_name, description);
		info->set_size(port_desc);

		if(!info->query_value.name) {
			stringstream n;
			n << parameter_group_name;
			n << "." << info->name;
			info->query_value.name = strdup(n.str().c_str());
			info->query_value.name_len = strlen(info->query_value.name);
		}

		port_infos.push_back(info);
	}

	bool success = ln_parameter_server::get()->register_block(this, clnt);
	if(always_publish && success)
		publish();
	return success;
}

void parameter_block::update(void **input_signals, void **output_signals, bool only_inputs) {
	static bool warning_printed = false;
	if(!warning_printed) {
		std::cerr << "Using the deprecated parameter_block::update function with non-const input_signals. Please pass input_signals of type const void **" << std::endl;
		warning_printed = true;
	}
	update((const void**)input_signals, output_signals, only_inputs);
}

void parameter_block::update(const void **input_signals, void **output_signals, bool only_inputs) {
	bool do_trigger_sync_handling;
	{
		std::lock_guard<std::mutex> lck(_mutex);
		for(unsigned int i = 0; i < port_infos.size(); i++) {
			port_info *info = port_infos[i];
			const void *inputs = input_signals[i];
			void *outputs = NULL;
			if(!only_inputs)
				outputs = output_signals[i];
			info->transfer(inputs, outputs);
		}
		do_trigger_sync_handling = write();
	}
	if(do_trigger_sync_handling)
		trigger_sync_handling->do_handle_service_group();
}

void parameter_block::update() {
	bool do_trigger_sync_handling;
	{
		std::lock_guard<std::mutex> lck(_mutex);
		for(unsigned int i = 0; i < port_infos.size(); i++) {
			port_infos[i]->transfer();
		}
		do_trigger_sync_handling = write();
	}
	if(do_trigger_sync_handling)
		trigger_sync_handling->do_handle_service_group();
}

parameter_block::~parameter_block() {
	if(outport_packet)
		free(outport_packet);
	if(clnt && outport) {
		clnt->unpublish(outport);
	}
	ln_parameter_server::get()->unregister_block(this);
	for(port_infos_t::iterator i = port_infos.begin(); i != port_infos.end(); i++)
		delete *i;
}

std::vector<port_info *> parameter_block::get_matching_ports(std::string pattern) {
	vector<port_info *> matching_ports;
	vector<string> patterns = split_string(pattern, ",");
	for(unsigned int d = 0; d < port_infos.size(); d++) {
		port_info *info = port_infos[d];
		bool matches = false;
		for(unsigned int i = 0; i < patterns.size(); i++) {
			string &pattern = patterns[i];
			if(pattern_matches(pattern, string(info->query_value.name, info->query_value.name_len))) {
				matches = true;
				break;
			}
		}
		if(matches)
			matching_ports.push_back(info);
	}
	return matching_ports;
}

void parameter_block::_append_item(string name, uint32_t &offset, stringstream &md, stringstream &md_name, port_info *info) {
	string field_type = info->_get_md_data_type_string();
	string field_type_name = info->get_md_data_type_short();
	string field_dim = info->get_md_dims_string();
	string field_name = name;

	info->packet_offset = offset;
	if(info->output) {
		string md_fn = format_string("ln/parameters/%s_field", field_type_name.c_str());
		md << "define " << field_type_name << "_field_t as \"gen/" << md_fn << "\"\n";
		md << field_type_name << "_field_t " << field_name << "\n";

		stringstream inner_md;
		inner_md << field_type << " input" << info->get_md_dims_string() << "\n";
		inner_md << field_type << " output" << info->get_md_dims_string() << "\n";
		inner_md << "uint8_t"
				 << " override_enabled\n";
		clnt->put_message_definition(md_fn, inner_md.str());

		offset += 2 * info->width * info->element_size + 1;
	} else {
		md << field_type << " " << field_name << info->get_md_dims_string() << "\n";

		offset += info->width * info->element_size;
	}
	md_name << field_name << "_" << field_type_name;
}

void parameter_block::_generate_child_md(uint32_t &offset, list<child_list_item_t> &items, string &md_name_) {
	stringstream md_name;
	stringstream md;

	childs_t childs;
	for(list<child_list_item_t>::iterator i = items.begin(); i != items.end(); ++i) {
		port_info *info = i->first;
		vector<string> &pname = i->second;
		if(pname.size() == 1) {
			_append_item(pname[0], offset, md, md_name, info);
			continue;
		}
		string child_name = pname[0];
		pname.erase(pname.begin());
		childs[child_name].push_back(child_list_item_t(info, pname));
	}
	// now generate sub-md's
	for(childs_t::iterator i = childs.begin(); i != childs.end(); i++) {
		string child_name = i->first;
		list<child_list_item_t> &child_items = i->second;

		string child_short_type;
		_generate_child_md(offset, child_items, child_short_type);
		string child_md_name = format_string("ln/parameters/%s", child_short_type.c_str());

		md << "define " << child_short_type << "_t as \"gen/" << child_md_name << "\"\n";
		md << child_short_type << "_t " << child_name << "\n";
		md_name << child_name << "(" << child_short_type << ")";
	}

	bool only_inputs = true;
	for(port_info *info: port_infos) {
		if(info->output)
			only_inputs = false;
	}

	md_name_ = md_name.str();
	if(only_inputs)
		md_name_ += "_woin";

	string md_fn = format_string("ln/parameters/%s", md_name_.c_str());
	clnt->put_message_definition(md_fn, md.str());
}

void parameter_block::_generate_md(string &name_) {
	uint32_t offset = 0;

	list<child_list_item_t> child_items;
	for(unsigned int d = 0; d < port_infos.size(); d++) {
		port_info *info = port_infos[d];
		string pname_(info->query_value.name, info->query_value.name_len);
		vector<string> pname = split_string(pname_, ".");
		child_items.push_back(child_list_item_t(info, pname));
	}
	_generate_child_md(offset, child_items, name_);
	name_ = format_string("gen/ln/parameters/%s", name_.c_str());
}

void parameter_block::publish() {
	no_subscriber_since = -1;
	disable_outport = false;
	if(outport)
		return;
	string md_name;
	_generate_md(md_name);
	string topic_name;
	if(this->topic_name.size())
		topic_name = this->topic_name;
	else
		topic_name = format_string("%s.%s", clnt->name.c_str(), parameter_group_name.c_str());

	ln::outport *p = clnt->publish(topic_name, md_name);
	outport_packet = calloc(1, p->message_size);
	outport = p;
}

bool parameter_block::write() {
	if(wait_step) {
		wait_counter++;
		wait_cond.notify_one();
		wait_step = false;
	}

	if(outport)
		outport->write(outport_packet);

	return trigger_sync_handling != NULL;
}
}  // namespace parameters
};  // namespace ln
