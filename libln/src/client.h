#ifndef CLIENT_H
#define CLIENT_H

// private! do not include!

#include "ln/ln.h"
#include "line_assembler.h"

struct client_;
typedef struct client_ client;
//struct client;

#include "lists.h"

#include "os.h"
#include "port.h"
#include "multi_waiter.h"
#include "service.h"
#include "logger.h"
#include "request.h"
#include "event.h"
#include "thread_pool.h"

#if !defined(WITHOUT_FTRACE_EVENTS)
#  define WITH_FTRACE_EVENTS
#endif

#ifdef WITH_FTRACE_EVENTS
#  define _ln_ftrace_print(format, ...) _ln_ftrace_write(format, ## __VA_ARGS__)
void _ln_ftrace_write(const char* format, ...);
#else
#  define _ln_ftrace_print(format, ...)
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	char* src;
	char* dst;
} topic_mapping_t;
int _ln_topic_mapping_destroy(topic_mapping_t*);

typedef topic_mapping_t service_mapping_t;
int _ln_service_mapping_destroy(service_mapping_t*);

#define lock_client(clnt) { if(clnt->thread_safe) ln_mutex_lock(&clnt->mutex); }
#define unlock_client(clnt) { if(clnt->thread_safe) ln_mutex_unlock(&clnt->mutex); }

struct client_ {
	uint16_t version;
	uint32_t client_id;	
	uint32_t next_request_id;
	char* program_name;
	char* manager_host;
	int manager_port;
	char hostname[255];
	int sfd;

	char* remaining_args;
	unsigned int n_remaining_args;
	
	char* send_request_buffer;
	unsigned int send_request_buffer_len;

	line_assembler_t* line_assembler;

	char* error_message;
	unsigned int error_message_size;

	DEFINE_LIST_DATA(outport);
	DEFINE_LIST_DATA(inport);
	DEFINE_LIST_DATA(multi_waiter);
	DEFINE_LIST_DATA_T(topic_mapping);
	DEFINE_LIST_DATA_T(service_mapping);
	DEFINE_LIST_DATA(service);
	DEFINE_LIST_DATA(logger);
	DEFINE_LIST_DATA(event_connection);
	DEFINE_LIST_DATA(service_group);
	DEFINE_LIST_DATA(thread_pool);

	DEFINE_LIST_DATA_TYPE_NAME(service, async_service_call);
	ln_mutex_handle_t async_service_calls_mutex;

	ln_thread_handle_t async_thread;
	ln_pipe_t async_notification_pipe;
	// this data is used in async thread:
	service** services_todo;
	unsigned int max_services_todo;

	int is_initialized;
	int debug;

	ln_mutex_handle_t communication_mutex;

	ln_service request_topic_svc;
	uint8_t enable_async_service_handling;
	uint8_t do_async_service_handling;

	uint32_t recv_count;
	
	request_t async_request;
	uint8_t had_new_fake_service_client;

	double last_ping_sent;

	unsigned int manager_library_version;
	service_group* default_service_group;

	uint32_t thread_safe;
	ln_mutex_handle_t mutex;

	DEFINE_LIST_DATA(log_service);

	float reconnect_interval;
	float win32_async_ping_interval;
	float win32_async_ping_timeout;
	float win32_wait_for_service_request_timeout;

	char* manager_host_ip;

	char input_buffer[RECEIVER_BUFFER_SIZE];
	unsigned int input_buffer_pos;
	unsigned int input_buffer_len;
};

#define client_initialized(c) ((c) && ((c)->is_initialized == 1))

DECLARE_LIST_METHODS(client, outport);
DECLARE_LIST_METHODS(client, inport);
DECLARE_LIST_METHODS(client, multi_waiter);
DECLARE_LIST_METHODS_T(client, topic_mapping);
DECLARE_LIST_METHODS_T(client, service_mapping);
DECLARE_LIST_METHODS(client, service);
DECLARE_LIST_METHODS(client, logger);
DECLARE_LIST_METHODS_TYPE_NAME(client, service, async_service_call);
DECLARE_LIST_METHODS(client, event_connection);
DECLARE_LIST_METHODS(client, service_group);
DECLARE_LIST_METHODS(client, thread_pool);

// name_mapping.c:
void _read_name_mappings(client* clnt, const char* map_topics, const char* map_services);
char* _get_topic_mapping(client* clnt, const char* topic_name, const char* hint);
char* _get_service_mapping(client* clnt, const char* service_name, const char* hint);

void _ln_set_error(client* clnt, const char* format, ...);
void _ln_set_platform_error_message(client* clnt, const char* hint);

int _ln_try_to_connect(client* clnt);

#ifdef __WIN32__
DWORD WINAPI _ln_async_thread(void* data);
#else
thread_return_t _ln_async_thread(void* data);
#endif

int resolve_hostname(const char* hostname, struct sockaddr_in* sa);

void add_async_service_call(client* clnt, service* s);
void remove_async_service_call(client* clnt, service* s);

void _ln_iovec_mgr_print(client* clnt, struct _ln_iovec_mgr* mgr);

DECLARE_LIST_METHODS(client, log_service);
int _ln_client_add_log_service(client* clnt, const char* log_service_name, log_service** ls_);
int _ln_client_get_log_service(client* clnt, const char* log_service_name, log_service** s_);

unsigned int _ln_have_manager_input(client* clnt);
int _ln_read_from_manager(client* clnt, void* dst, unsigned int max_len);

int ln_enable_tcp_keepalive(int fd);

#define REGISTER_CLIENT_FIELDS()					\
	"method", "r", "register_client",				\
		"program_name", "r", clnt->program_name,		\
		"host", "r", clnt->hostname,				\
		"manager_host", "r", clnt->manager_host_ip,		\
		"manager_port", "d", clnt->manager_port,		\
		"pid", "d", getpid(),					\
		"library_version", "d", LN_LIBRARY_VERSION,		\
		"svn_revision", "d", 0,					\
		"is_modified", "d", 0,					\
		"pointer_size", "d", sizeof(void*),			\
		"have_unix_domain_sockets", "d", IS_WITH_AF_UNIX,	\
		"uid", "d", uid,					\
		"gid", "d", gid,					\
		"endianess", "r", endianess,				\
		"have_sys_futex", "d", IS_WITH_SYS_FUTEX,		\
		"have_glibc_atleast_2_25", "d", IS_GLIBC_ATLEAST_2_25,	\
		"have_win32", "d", IS_WIN32				\

#if defined(LN_TEST)
int _init_clnt_struct(client** clnt, const char* program_name);
#endif
	
#ifdef __cplusplus
}
#endif

#endif // CLIENT_H
