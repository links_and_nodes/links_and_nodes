/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#ifndef __WIN32__
#include <unistd.h>
#include <sys/types.h>
#endif
#include <errno.h>
#include <stdarg.h>
#include <ctype.h>

#include "ln/ln.h"
#include "repr.h"

#include "client.h"
#include "request.h"

#ifdef __ANDROID__
#include <android/log.h>
#endif

// a is unsafe, b is expected to be 0 terminated
#define strequal(a, b) !strncmp((a), (b), strlen((b)))

#include "ln/errors.h"

#define MIN(a, b) ((a)<(b)?(a):(b))

//#define LOG_TO_FILE
#ifdef LOG_TO_FILE
static FILE* log_fp = NULL;
#endif

#ifdef WITH_FTRACE_EVENTS
#include <unistd.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

static int _ftrace_fd = -1;
static bool _debug_to_ftrace = false;
void _open_ftrace();
#  define open_ftrace() _open_ftrace();
#else
#  define open_ftrace()
#endif

void ln_debug(void* clnt_, const char* format, ...) {
	client* clnt = (client*)clnt_;

#ifdef WITH_FTRACE_EVENTS
	if(_debug_to_ftrace && _ftrace_fd >= 0) {
		va_list ap;
		char buf[1024];
		int n;
		va_start(ap, format);
		n = vsnprintf(buf, sizeof(buf), format, ap);
		va_end(ap);
		write(_ftrace_fd, buf, n);
	}
#endif

	if(clnt && clnt->debug == 0)
		return;

#ifdef LOG_TO_FILE
	if(!log_fp) 
		log_fp = fopen("/tmp/lnc_%d.log", "w");
#endif

	static double start_time = 0;
	double now = ln_get_monotonic_time();
	if(start_time == 0)
		start_time = now;
	static int had_line_start = 1;

	va_list ap;
	va_start(ap, format);
#ifdef LOG_TO_FILE
	if(had_line_start)
		vfprintf(log_fp, "%9.3f: ", (now - start_time));
	vfprintf(log_fp, format, ap);
	fflush(log_fp);
#endif
#ifdef __ANDROID__
	__android_log_vprint(ANDROID_LOG_DEBUG, "ln_client", format, ap);
#endif
	if(had_line_start)
		printf("%9.3f: ", (now - start_time));
	vprintf(format, ap);
	va_end(ap);
	fflush(stdout);

	had_line_start = format[strlen(format)-1] == '\n';
}


void _ln_set_error(client* clnt, const char* format, ...) {
	int n;
	char* np;

	if(!clnt->error_message) {
		clnt->error_message_size = 255;
		if ((clnt->error_message = (char*)malloc(clnt->error_message_size)) == NULL)
			return; // throw std::bad_alloc();
	}

	if(!format || !format[0]) {
		clnt->error_message[0] = 0;
		return;
	}

	va_list ap;
	// va_start(ap, format);
	
	while(1) {
		/* Try to print in the allocated space. */
		va_start(ap, format); // needed for 64bit machines!
		n = vsnprintf(clnt->error_message, clnt->error_message_size, format, ap);
		va_end(ap);
		/* If that worked, return the string. */
		if (n > -1 && n < (signed)clnt->error_message_size)
			break;
		/* Else try again with more space. */
		if (n > -1)    /* glibc 2.1 */
			clnt->error_message_size = n+1; /* precisely what is needed */
		else           /* glibc 2.0 */
			clnt->error_message_size *= 2;  /* twice the old size */
		if ((np = (char*)realloc(clnt->error_message, clnt->error_message_size)) == NULL) {
			ln_debug(clnt, "failed realloc() to set_error message for format %s!\n", format);
			return; // throw std::bad_alloc();
		}
		clnt->error_message = np;
	}
	ln_debug(clnt, "did set error_message: %s\n", clnt->error_message);
	// va_end(ap);
}


void _ln_set_platform_error_message(client* clnt, const char* hint) {
	const char* empty_hint = "";
	if(!hint)
		hint = empty_hint;
#ifdef WIN32
	int wsaerr = WSAGetLastError();
	LPWSTR wsaerrstr = NULL;
	FormatMessageW(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, wsaerr,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPWSTR)&wsaerrstr, 0, NULL
	);
	ln_debug(clnt, "%swindows error(%d): %S\n", hint, wsaerr, wsaerrstr);
	_ln_set_error(clnt, "%swindows error(%d): %S\n", hint, wsaerr, wsaerrstr);
	LocalFree(wsaerrstr);
#else
	ln_debug(clnt, "%serrno: %d, %s\n", hint, errno, strerror(errno));
	_ln_set_error(clnt, "%serrno: %d, %s\n", hint, errno, strerror(errno));
#endif
}

int _ln_platform_interrupted() {
	// does errno indicate an interrupted system call?
	
#if !defined(WIN32)
	if(errno == EINTR)
		return 1; // yes
#else
	int wsaerr = WSAGetLastError();
	if(wsaerr == WSAEINTR)
		return 1; // yes
#endif
	return 0; // no
}

const char* ln_format_error(int ret) {
	if(ret == -LNE_CHECK_ERRNO) {
		char* errno_str = strerror(errno);
		static char* return_string = NULL; // todo: register at exit to free this memory!
		static int return_string_len = 0;
		int needed = strlen(errno_str) + 20;
		if(!return_string) {
			return_string_len = needed;
			return_string = (char*)malloc(return_string_len);
		} else if(return_string_len < needed) {
			return_string_len = needed;
			void* new_mem = realloc(return_string, return_string_len);
			if(!new_mem)
			  return "failed to format error message - realloc";
			return_string = (char*)new_mem;
		}
		snprintf(return_string, return_string_len - 1, "errno %d - %s", errno, errno_str);
		return return_string;
	}
	int error = -ret;
	if(error < ln_errors_min || error > ln_errors_max)
		return "unknown error";
	return ln_errors[error];
}

int resolve_hostname(const char* hostname, struct sockaddr_in* sa) {
	// resolve hostname
	if(!hostname || hostname[0] == 0)
		return -LNE_INVALID_HOSTNAME;

	if(isdigit(hostname[0])) {
		// printf("hostname[0] is digit: '%s'\n", hostname);
		// assume dotted decimal notation
		int ret = inet_aton(hostname, &sa->sin_addr);
#ifndef __VXWORKS__
		if(ret) {
#else
		if(ret == 0) {
#endif
			// printf("it seems to be a dotted decimal ip: %s, ret: %d\n", hostname, ret);
			sa->sin_family = AF_INET;
			return 0; // otherwise try to resolve...
		}
		printf("inet_aton failed: %d, %s\n", ret, strerror(errno));
	}/* else {
		printf("hostname[0] is not digit: '%s'\n", hostname);
	}
	 */
		
#ifdef __WIN32__
	struct addrinfo *result = NULL;

	int ret = getaddrinfo(hostname, NULL, NULL, &result);
	if(ret) {
		// error!
		ln_debug(NULL, "resolve_hostname: gai_strerror: %s\n", gai_strerror(ret));
		return -LNE_UNKNOWN_HOSTNAME;
	}

	struct addrinfo *ptr = NULL;
	int found = 0;
    for(ptr = result; ptr != NULL; ptr = ptr->ai_next) {
        if (ptr->ai_family == AF_INET) {
			//memcpy(&sa->sin_addr, ptr->ai_addr, sizeof(sa->sin_addr));
			memcpy(&sa->sin_addr, &((struct sockaddr_in*)ptr->ai_addr)->sin_addr, sizeof(sa->sin_addr));
			sa->sin_family = AF_INET;
			found = 1;
			break;
		}
    }
    freeaddrinfo(result);
	if(!found)
		return -LNE_UNKNOWN_HOSTNAME;
#else
	// struct hostent* he = gethostbyname(hostname);
	struct addrinfo hints;
	struct addrinfo* result = NULL;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	int ret;
	if((ret = getaddrinfo(hostname, NULL, &hints, &result))) {
		fprintf(stderr, "resolve_hostname(): getaddrinfo returned %d gai_strerror: %s\n", ret, gai_strerror(ret));
		return -LNE_UNKNOWN_HOSTNAME;
	}
	struct addrinfo *ptr = NULL;
	int found = 0;
	for(ptr = result; ptr != NULL; ptr = ptr->ai_next) {
		if (ptr->ai_family == AF_INET) {
			memcpy(&sa->sin_addr, &((struct sockaddr_in*)ptr->ai_addr)->sin_addr, sizeof(sa->sin_addr));
			sa->sin_family = AF_INET;
			found = 1;
			break;
		}
	}
	freeaddrinfo(result);
	if(!found)
		return -LNE_UNKNOWN_HOSTNAME;
#endif
	return 0;
}

char* ln_get_error_message(ln_client clnt_) {
	client* clnt = (client*)clnt_;
	static char no_client[] = "ln_get_error_message: error given clnt is NULL!";
	if(!clnt)
		return no_client;
	return clnt->error_message;
}

int _ln_try_to_connect(client* clnt) {
	struct sockaddr_in manager_address;
	struct sockaddr_in client_address;
	socklen_t client_address_len = sizeof(client_address);

	memset(&manager_address, 0, sizeof(manager_address));
	manager_address.sin_port = htons(clnt->manager_port);

	clnt->sfd = -1;
	int ret;
	if((ret = resolve_hostname(clnt->manager_host, &manager_address))) {
		ln_debug(clnt, "failed to resolve manager hostname '%s'\n", clnt->manager_host);
		return -LNE_INVALID_HOSTNAME;
	}

	if(clnt->manager_host_ip)
		free(clnt->manager_host_ip);
	const unsigned int manager_address_ip_len = INET6_ADDRSTRLEN + 7;
	char manager_address_ip[INET6_ADDRSTRLEN + 7];
	if(inet_ntop(manager_address.sin_family, &manager_address.sin_addr, manager_address_ip, manager_address_ip_len))
		clnt->manager_host_ip = strdup(manager_address_ip);
	else
		clnt->manager_host_ip = strdup(clnt->manager_host);
	
	clnt->sfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(clnt->sfd == -1) {
		ln_debug(clnt, "failed to create socket\n");
		return -LNE_CHECK_ERRNO;
	}
	
	if((ret = connect(clnt->sfd, (struct sockaddr*)&manager_address, sizeof(manager_address)))) {
		_ln_close_socket(clnt->sfd);
		clnt->sfd = -1;
		return -LNE_CHECK_ERRNO;
	}

	// check tcp simultaneous connect bug #1338
	if(getsockname(clnt->sfd, (struct sockaddr*)&client_address, &client_address_len)) {
		perror("getsockname");
		return -1;
	}
	if(memcmp(&client_address, &manager_address, sizeof(manager_address)) == 0) {
		// connected to itself!
		// https://lkml.org/lkml/2007/10/21/228
		// close and try again!
		ln_debug(clnt, "have TCP simultaneous connect! will try again!\n");
		_ln_close_socket(clnt->sfd);
		clnt->sfd = -1;
		return _ln_try_to_connect(clnt);
	} else {
		char mgr_addr[64];
		strnrepr(mgr_addr, 64, (const char*)&manager_address, sizeof(manager_address));
		char clnt_addr[64];
		strnrepr(clnt_addr, 64, (const char*)&client_address, sizeof(manager_address));

		ln_debug(clnt, "not a TCP simultaneous connect:\n"
			 " client-addr: %s,\n"
			 "manager-addr: %s\n",
			 clnt_addr, mgr_addr);
	}
	ln_enable_tcp_keepalive(clnt->sfd);
	return 0; 
}

int ln_enable_tcp_keepalive(int fd) {
	/* Set the option active */
#ifdef __WIN32__
	char optval = 1;
#else
	int optval = 1;
#endif
	os_socklen_t optlen = sizeof(optval);
	if(setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen) < 0) {
		perror("setsockopt(SO_KEEPALIVE)");
	}
#ifdef __LINUX__	
	optval = 5;
	if(setsockopt(fd, IPPROTO_TCP, TCP_KEEPIDLE, &optval, optlen) < 0) {
		perror("setsockopt(SO_KEEPIDLE)");
	}
	optval = 5; // 5 + 9 * 5 = 50seconds until timeout
	if(setsockopt(fd, IPPROTO_TCP, TCP_KEEPINTVL, &optval, optlen) < 0) {
		perror("setsockopt(SO_KEEPIDLE)");
	}
#endif
#ifdef __WIN32__
	struct tcp_keepalive ka;
	ka.onoff = 1;
	ka.keepalivetime = 5000;
	ka.keepaliveinterval = 5000;
	int ret = WSAIoctl(
		fd,  // descriptor identifying a socket
		SIO_KEEPALIVE_VALS,   // dwIoControlCode
		(LPVOID)&ka,          // pointer to tcp_keepalive struct 
		(DWORD)sizeof(ka),    // length of input buffer 
		NULL,                 // output buffer
		0,                    // size of output buffer
		NULL,                 // number of bytes returned
		NULL,                 // OVERLAPPED structure
		NULL                 // completion routine
		);
	ln_debug(NULL, "WSAIoctl SIO_KEEPALIVE_VALS returned %d\n", ret);
#endif
	return 0;
}

int _init_to_manager(client* clnt, const char* ln_manager_address) {
	int ret;
	const char* p = NULL;

#if !defined(WIN32) && !defined(__VXWORKS__)
	uid_t uid = geteuid();
	gid_t gid = getegid();
#else
	// win32 todo: overkill with OpenProcessToken(), OpenThreadToken() and GetUserSID()
	int uid = 0;
	int gid = 0;
#endif
	char endianess[16];
	
	// init values
	clnt->sfd = -1;

	p = getenv("LN_DEBUG_TO_FTRACE");
	_debug_to_ftrace = p && strchr("yt1", tolower(p[0])) != NULL;

	_read_name_mappings(clnt, getenv("LN_MAP_TOPICS"), getenv("LN_MAP_SERVICES"));
	
	if(!ln_manager_address || !ln_manager_address[0]) {
		// check environment variable
		ln_manager_address = getenv("LN_MANAGER");
	}
	if(!ln_manager_address) {
		ret = -LNE_NO_MANAGER_ADDRESS;
		goto error_exit1;
	}
	p = strchr(ln_manager_address, ':');
	if(!p) {
		ret = -LNE_INVALID_MANAGER_ADDRESS;
		goto error_exit1;
	}
	clnt->manager_port = atoi(p + 1);
	clnt->manager_host = strndup(ln_manager_address, p - ln_manager_address);

	ln_debug(clnt, "got manager host '%s', and port %d\n", clnt->manager_host, clnt->manager_port);

	if((ret = _ln_try_to_connect(clnt))) {
		ln_debug(clnt, "failed to connect to manager: %s\n", ln_format_error(ret));
		goto error_exit3;
	}
	ln_debug(clnt, "connected to manager.\n");

	init_line_assembler(&clnt->line_assembler, NULL, NULL); // no callback yet

	clnt->hostname[254] = '\0';
	gethostname(clnt->hostname, 254);

	// create request object
	setlocale(LC_ALL, "C"); // HACK!
	
	{ // check own endianess
		uint16_t test = 0x1234;
		if(((uint8_t*)&test)[0] == 0x34)
			strcpy(endianess, "little");
		else
			strcpy(endianess, "big");
	}

	request_t r;
	ret = _ln_init_request(
		clnt, &r,
		REGISTER_CLIENT_FIELDS(),
		NULL);
	if(ret)
		goto error_exit4;
	ret = _ln_request_and_wait(&r);
	if(ret) {
		if(ret == -LNE_LOST_CONNECTION)
			ret = -LNE_REGISTER_FAILED;
		goto error_exit5;
	}

	char real_program_name[256];
	if((ret = _ln_get_from_request(&r, "program_name", "s256", &real_program_name))) {
		_ln_destroy_request(&r);
		return ret;
	}
	if(strcmp(real_program_name, clnt->program_name)) {
		ln_debug(clnt, "manager changed client name from '%s' to '%s'\n", clnt->program_name, real_program_name);
		free(clnt->program_name);
		clnt->program_name = strdup(real_program_name);
	}

	// try to get manager's library version
	clnt->manager_library_version = 7; // assumption
	_ln_get_from_request(&r, "manager_library_version", "d", &clnt->manager_library_version);

	clnt->version = LN_LIBRARY_VERSION;
	if(clnt->manager_library_version >= 13) {
		_ln_get_from_request(&r, "client_id", "d", &clnt->client_id);
		ln_debug(clnt, "we are client_id %d\n", clnt->client_id);
		clnt->next_request_id = 1;
	}
	
	_ln_destroy_request(&r);

	if((ret = ln_mutex_create(&clnt->communication_mutex))) {
		errno = ret;
		ret = -LNE_CHECK_ERRNO;
		goto error_exit5;
	}

	if((ret = ln_mutex_create(&clnt->async_service_calls_mutex))) {
		errno = ret;
		ret = -LNE_CHECK_ERRNO;
		goto error_exit6;
	}

	if((ret = ln_pipe_init(&clnt->async_notification_pipe))) {
		goto error_exit7;
	}

	if((ret = ln_mutex_create(&clnt->mutex))) {
		errno = ret;
		ret = -LNE_CHECK_ERRNO;
		goto error_exit71;
	}
	clnt->thread_safe = 1;

	clnt->default_service_group = _ln_add_service_group(clnt, NULL);
	
	// start asynchronous processing thread!
	if((ret = ln_thread_create_with_prio(&clnt->async_thread, _ln_async_thread, clnt, LOW_PRIO_POLICY, LOW_PRIO, "ln_client_thread"))) {
		errno = ret;
		ret = -LNE_CHECK_ERRNO;
		goto error_exit8;
	}

	return ret;

error_exit8:
	ln_mutex_destroy(&clnt->mutex);
error_exit71:
	ln_pipe_close(&clnt->async_notification_pipe);
error_exit7:
	ln_mutex_destroy(&clnt->async_service_calls_mutex);
error_exit6:
	ln_mutex_destroy(&clnt->communication_mutex);
error_exit5:
	_ln_destroy_request(&r);
error_exit4:
	destroy_line_assembler(clnt->line_assembler);
	free(clnt->program_name);
    if(clnt->sfd != -1)
		_ln_close_socket(clnt->sfd);
error_exit3:
    if(clnt->manager_host_ip)
	    free(clnt->manager_host_ip);
    free(clnt->manager_host);
error_exit1:
    free(clnt);
    return ret;
}

int _init_clnt_struct(client** clnt, const char* program_name) {
	open_ftrace();

	*clnt = (client*)malloc(sizeof(client));
	if(!*clnt)
		return -LNE_NO_MEM;
	memset(*clnt, 0, sizeof(client));
	(*clnt)->debug = 0;
	{
		const char* do_debug = getenv("LN_DEBUG");
		if(do_debug)
			(*clnt)->debug = atoi(do_debug);
	}
#ifdef __WIN32__
	// force win32 to always be line buffered on stdout!
#ifdef MSVC
	setvbuf(stdout, NULL, _IOLBF, 1024);
	setvbuf(stderr, NULL, _IOLBF, 1024);
#else
	setvbuf(stdout, NULL, _IOLBF, 0);
	setvbuf(stderr, NULL, _IOLBF, 0);
#endif

#endif
	const char* pn = getenv("LN_PROGRAM_NAME");
	if(pn) {
		ln_debug(*clnt, "program name overwritten via env var LN_PROGRAM_NAME from '%s' to '%s'\n", program_name, pn);
		(*clnt)->program_name = strdup(pn);
	} else {
		(*clnt)->program_name = strdup(program_name);
	}

	if(!(*clnt)->debug && strstr((*clnt)->program_name, "dbg"))
		(*clnt)->debug = 1;

	(*clnt)->recv_count = 0;

	(*clnt)->enable_async_service_handling = 0;

	_ln_init_request(*clnt, &(*clnt)->async_request, NULL);
	(*clnt)->last_ping_sent = 0;

	// init parameters
	(*clnt)->reconnect_interval = 5;
	(*clnt)->win32_async_ping_interval = 25;
	(*clnt)->win32_async_ping_timeout = 15;
	(*clnt)->win32_wait_for_service_request_timeout = 0.25;
	return 0;
}

int ln_init(ln_client* clnt_, const char* program_name, int argc, char** argv) {
	ln_get_time(); // init timing
#ifdef __WIN32__
	WSADATA wsaData = {0};
	/*DWORD iResult = */ WSAStartup(MAKEWORD(2, 2), &wsaData);
#endif

	*clnt_ = NULL;
	int skip = 0;
	const char* ln_manager_address = NULL;

	client* clnt;
	int ret = _init_clnt_struct(&clnt, program_name);
	if(ret) {
		ln_debug(NULL, "ln_init() failed to initialize client struct: %d: %s\n", 
		      ret, ln_format_error(ret));
		return ret;
	}

	// search ln_manager connection info
	// check command line arguments
	char* remaining_args_cp = NULL;
	unsigned int remaining_args_size = 0;
	for(int i = 0; i < argc; i++) {
		if(skip) {
			skip --;
			continue;
		}
		if((strequal(argv[i], "--ln-manager") || strequal(argv[i], "-ln_manager")) && argc > (i + 1)) {
			ln_manager_address = argv[i + 1];
			skip = 1;
			continue;
		}
		if((strequal(argv[i], "--ln-program-name") || strequal(argv[i], "-ln_program_name")) && argc > (i + 1)) {
			ln_debug(clnt, "program name overwritten via command line from '%s' to '%s'\n", clnt->program_name, argv[i + 1]);
			free(clnt->program_name);
			clnt->program_name = strdup(argv[i + 1]);
			skip = 1;
			continue;
		}
		if((strequal(argv[i], "--ln-map-topic") || strequal(argv[i], "-ln_map_topic")) && argc > (i + 2)) {
			topic_mapping_t* mapping = (topic_mapping_t*)malloc(sizeof(topic_mapping_t));
			mapping->src = strdup(argv[i + 1]);
			mapping->dst = strdup(argv[i + 2]);
			ln_debug(clnt, "added topic mapping from '%s' to '%s'...\n", mapping->src, mapping->dst);
			_ln_client_append_topic_mapping(clnt, mapping);
			skip = 2; // CSA: no, mapping is consumed in clnt topic mappings' list
			continue;
		}
		if((strequal(argv[i], "--ln-map-service") || strequal(argv[i], "-ln_map_service")) && argc > (i + 2)) {
			service_mapping_t* mapping = (service_mapping_t*)malloc(sizeof(service_mapping_t));
			mapping->src = strdup(argv[i + 1]);
			mapping->dst = strdup(argv[i + 2]);
			ln_debug(clnt, "added service mapping...\n");
			_ln_client_append_service_mapping(clnt, mapping);
			skip = 2; // CSA: no, mapping is consumed in clnt service mappings' list
			continue;
		}
		// append to remaining_args
		growing_buffer_appendn(&clnt->remaining_args, &remaining_args_cp, &remaining_args_size, argv[i], strlen(argv[i]) + 1);
		clnt->n_remaining_args ++;
	}

	ret = _init_to_manager(clnt, ln_manager_address);
	if(ret == 0) {
		*clnt_ = (ln_client) clnt;
		clnt->is_initialized = 1;
	}
	return ret;
}

int ln_client_get_name(ln_client clnt_, char** name) {
	client* clnt = (client*)clnt_;
	
	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;
	*name = clnt->program_name;
	return 0;
}

int ln_ping(ln_client clnt_, double timeout, double* rtt) {
	client* clnt = (client*)clnt_;

	if(rtt)
		*rtt = 0;

	if(!client_initialized(clnt))
		return -LNE_LOST_CONNECTION;

	if(timeout == 0) {
		if(clnt->sfd == -1) {
			ln_debug(clnt, "ln_ping(%p) manager connection is closed!\n", clnt);
			return -LNE_LOST_CONNECTION;
		}
		ln_debug(clnt, "ln_ping(%p) have manager connection at fd %d\n", clnt, clnt->sfd);
		return 0;
	}

	if(clnt->sfd == -1) {
		// currently there is no connection!
		// todo: mybe try to actively reconnect now!
		ln_debug(clnt, "ln_ping(%p) manager connection is closed!\n", clnt);
		return -LNE_LOST_CONNECTION;
	}

	clnt->last_ping_sent = ln_get_monotonic_time();

	// send request
	request_t r;
	int ret = _ln_init_request(
		clnt, &r,
		"method", "r", "ping",
		NULL);
	if(ret)
		return ret;

	double start = ln_get_monotonic_time();
	double deadline = start + timeout;
	while(true) {
		// try to get communication lock!
		if(ln_mutex_trylock(&clnt->communication_mutex)) {
			// could not get lock
			// ln_debug(clnt, "async thread could not get lock!\n");
			ln_sleep_seconds(0.05);
			if(timeout != -1 && ln_get_monotonic_time() > deadline) {
				ln_debug(clnt, "ln_ping(%p) timeout waiting for comm-mutex of manager connection!\n", clnt);
				return -LNE_LOST_CONNECTION;
			}
			continue;
		}
		break;
	}
	ret = _ln_send_request(&r);
	while(!ret) {
		_ln_reset_request(&r);
		fd_set read_fds;
		FD_ZERO(&read_fds);
		FD_SET(clnt->sfd, &read_fds);
		double rem = deadline - start;
		if(timeout != -1) {
			struct timeval ts = {0, 0};
			ts.tv_sec = (unsigned int)rem;
			ts.tv_usec = (unsigned int)((rem - ts.tv_sec) * 1e6);
			ret = select(clnt->sfd + 1, &read_fds, NULL, NULL, &ts);
			if(ret == 0) { // timeout
				// close socket!
				ln_debug(clnt, "ln_ping(%p) actively closing timed out manager connection!\n", clnt);
				ret = -LNE_LOST_CONNECTION;
				break;
			}
		}
		ret = _ln_wait_response(&r, 0);
		if(ret)
			break;
		
		double d = ln_get_monotonic_time() - start;
		ln_debug(clnt, "ln_ping(%p) got manager answer after %.3fs\n", clnt, d);
		
		if(rtt)
			*rtt = d;
		break;
	}
	if(ret) {
		if(clnt->sfd != -1) {
			shutdown(clnt->sfd, SHUT_RDWR);
			_ln_close_socket(clnt->sfd);
			clnt->sfd = -1;
		}
#ifndef __WIN32__
		ln_pipe_send_notification(clnt->async_notification_pipe);
#endif
	}
	ln_mutex_unlock(&clnt->communication_mutex);
	_ln_destroy_request(&r);
	
	return ret;
}


int ln_get_remaining_args(ln_client clnt_, unsigned int* n_args, char** args_ptr) {
	client* clnt = (client*)clnt_;
	
	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	*n_args = clnt->n_remaining_args;
	*args_ptr = clnt->remaining_args;
	return 0;
}


int ln_init_to_manager(ln_client* clnt_, const char* program_name, const char* ln_manager_address) {
#ifdef __WIN32__
	WSADATA wsaData = {0};
	/*DWORD iResult = */WSAStartup(MAKEWORD(2, 2), &wsaData);
#endif
	client* clnt;
	int ret = _init_clnt_struct(&clnt, program_name);
	if(ret)
		return ret;

	ret = _init_to_manager(clnt, ln_manager_address);
	if(ret == 0) {
		*clnt_ = (ln_client) clnt;
		clnt->is_initialized = 1;
	}
	return ret;
}

DEFINE_LIST_METHODS(client, log_service);
int _ln_log_service_destroy(log_service* ls) {
	ln_mutex_destroy(&ls->mutex);
	_ln_iovec_mgr_destroy(&ls->mgr);
	if(ls->iov_element_lens)
		free(ls->iov_element_lens);
	// ls->client gets destroyed by client-destructor
	free(ls);
	return 0;
}


int ln_deinit(ln_client* clnt_) {
	client* clnt = (client*)*clnt_;
	ln_debug(clnt, "ln_deinit(%p) called.\n", clnt);

	if(clnt) {
		if(clnt->async_thread)
			ln_thread_cancel(clnt->async_thread);

		// notify async thread:
#ifndef __WIN32__
		ln_debug(clnt, "ln_deinit: trigger notification pipe!\n");
		ln_pipe_send_notification(clnt->async_notification_pipe);
#endif
		if(clnt->async_thread) {
			ln_debug(clnt, "ln_deinit: join async thread\n");
			ln_thread_join(clnt->async_thread, NULL);
			ln_debug(clnt, "ln_deinit: got async\n");
		}
		_ln_destroy_request(&clnt->async_request);

		if(clnt->remaining_args)
			free(clnt->remaining_args);
		
		if(clnt->request_topic_svc)
			ln_service_deinit(&clnt->request_topic_svc);

		if(clnt->services_todo)
			free(clnt->services_todo);

		DESTRUCT_LIST(clnt, multi_waiter);

		DESTRUCT_LIST(clnt, service_group);
		DESTRUCT_LIST(clnt, thread_pool);
		DESTRUCT_LIST(clnt, event_connection);
		DESTRUCT_LIST(clnt, outport);
		DESTRUCT_LIST(clnt, inport);
		DESTRUCT_LIST_T(clnt, topic_mapping);
		DESTRUCT_LIST_T(clnt, service_mapping);
		DESTRUCT_LIST(clnt, service);
		DESTRUCT_LIST(clnt, logger);
		DESTRUCT_LIST(clnt, log_service);
		DESTRUCT_LIST_TYPE_NAME(clnt, service, async_service_call);

		ln_mutex_lock(&clnt->communication_mutex);
		
		ln_pipe_close(&clnt->async_notification_pipe);
		ln_mutex_destroy(&clnt->async_service_calls_mutex);
		ln_mutex_destroy(&clnt->communication_mutex);
		ln_mutex_destroy(&clnt->mutex);


		if(clnt->error_message)
			free(clnt->error_message);
		if(clnt->line_assembler)
			destroy_line_assembler(clnt->line_assembler);

		if(clnt->send_request_buffer)
			free(clnt->send_request_buffer);

		if(clnt->sfd != -1)
			_ln_close_socket(clnt->sfd);
      
		if(clnt->manager_host)
			free(clnt->manager_host);
      
		if(clnt->manager_host_ip)
			free(clnt->manager_host_ip);
      
		if(clnt->program_name)
			free(clnt->program_name);
      
		clnt->is_initialized = 0;
		free(clnt);
    }

	// ln_debug(NULL, "ln_deinit(%p) finished.\n", clnt);
	*clnt_ = NULL;
    return 0;
}

int _ln_append(void*** list, unsigned int* s, unsigned int* n, void* elem) {
	if(!*list) {
		*s = 5;
		*list = (void**)malloc(sizeof(void*) * (*s));
	}
	if((*n + 1) >= (*s)) {
		*s *= 2;
		*list = (void**)realloc(*list, sizeof(void*) * (*s));
	}
	if(!*list)
		return -LNE_NO_MEM;
	(*list)[(*n)++] = elem;
	return 0;
}

DEFINE_LIST_METHODS(client, thread_pool);
DEFINE_LIST_METHODS(client, service_group);
DEFINE_LIST_METHODS(client, event_connection);
DEFINE_LIST_METHODS(client, outport);
DEFINE_LIST_METHODS(client, inport);
DEFINE_LIST_METHODS(client, multi_waiter);
DEFINE_LIST_METHODS_T(client, topic_mapping);
DEFINE_LIST_METHODS_T(client, service_mapping);
DEFINE_LIST_METHODS(client, service);
DEFINE_LIST_METHODS(client, logger);
DEFINE_LIST_METHODS_TYPE_NAME(client, service, async_service_call);

int ln_get_message_definition_for_topic_v17(
	ln_client clnt_, const char* topic_name,
	// outputs:
	char** message_definition_name,
	char** message_definition,
	unsigned int* message_size,
	char** hash)
{
	client* clnt = (client*)clnt_;

	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	char* topic = _get_topic_mapping(clnt, topic_name, "subscribed");
	int ret = 0;
	while(true) {
		request_t r;
		ret = _ln_init_request(
			clnt, &r,
			"method", "r", "get_message_definition_for_topic",
			"topic", "r", topic,
			NULL);
		if(ret)
			break;
		while(true) {
			ln_mutex_lock(&clnt->communication_mutex);
			ret = _ln_request_and_wait(&r);
			ln_mutex_unlock(&clnt->communication_mutex);
			if(ret)
				break;
			if((ret = _ln_get_from_request(&r, "name", "S", message_definition_name)))
				break;
			if((ret = _ln_get_from_request(&r, "definition", "S", message_definition)))
				break;
			if((ret = _ln_get_from_request(&r, "size", "u", message_size)))
				break;

			if(hash) {
				if(clnt->manager_library_version < 17) {
					_ln_set_error(clnt, "manager library version %d is too old for ln_get_message_definition_hash! need atleast 17!", clnt->manager_library_version);
					ret = -LNE_MANAGER_TOO_OLD;
				} else 	if((ret = _ln_get_from_request(&r, "hash", "S", hash))) {
					break;
				}
			}
			
			break;
		}
		_ln_destroy_request(&r);
		break;
	}
	free(topic);	
	return ret;
}

int ln_get_message_definition_for_topic(
	ln_client clnt, const char* topic_name,
	// outputs:
	char** message_definition_name,
	char** message_definition,
	unsigned int* message_size)
{
	return ln_get_message_definition_for_topic_v17(clnt, topic_name, message_definition_name, message_definition, message_size, NULL);
}

int ln_get_message_definition_v21(
	ln_client clnt_, const char* message_definition_name, 
	// outputs:
	char** message_definition, unsigned int* message_size, char** hash, char** flat_message_definition)
{
	client* clnt = (client*)clnt_;

	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	request_t r;
	int ret = _ln_init_request(
		clnt, &r,
		"method", "r", "get_message_definition",
		"md_name", "r", message_definition_name,
		NULL);
	if(ret)
		goto err0;

	ln_mutex_lock(&clnt->communication_mutex);
	ret = _ln_request_and_wait(&r);
	ln_mutex_unlock(&clnt->communication_mutex);
	if(ret)
		goto err1;

	if(message_definition && (ret = _ln_get_from_request(&r, "definition", "S", message_definition)))
		goto err1;

	if(message_size && (ret = _ln_get_from_request(&r, "size", "u", message_size)))
		goto err2;

	if(hash) {
		if(clnt->manager_library_version < 17) {
			_ln_set_error(clnt, "manager library version %d is too old for ln_get_message_definition_hash! need atleast 17!", clnt->manager_library_version);
			ret = -LNE_MANAGER_TOO_OLD;
			goto err2;
		}

		if((ret = _ln_get_from_request(&r, "hash", "S", hash)))
			goto err2;
	}

	if(flat_message_definition) {
		if(clnt->manager_library_version < 21)
			*flat_message_definition = NULL;
		else if((ret = _ln_get_from_request(&r, "flat_definition", "S", flat_message_definition)))
			goto err3;
	}

	_ln_destroy_request(&r);
	return 0;
err3:
	if(hash) free(*hash);
err2:
	if(message_definition) free(*message_definition);
err1:
	_ln_destroy_request(&r);
err0:
	return ret;

}

int ln_get_message_definition_v17(
	ln_client clnt_, const char* message_definition_name, 
	// outputs:
	char** message_definition, unsigned int* message_size, char** hash)
{
	return ln_get_message_definition_v21(clnt_, message_definition_name, message_definition, message_size, hash, NULL);
}

int ln_get_message_definition(
	ln_client clnt_, const char* message_definition_name,
	// outputs:
	char** message_definition, unsigned int* message_size)
{
	return ln_get_message_definition_v17(clnt_, message_definition_name, message_definition, message_size, NULL);
}

int ln_describe_message_definition(
	ln_client clnt_, const char* message_definition_name,
	// outputs:
	char** message_description)
{
	client* clnt = (client*)clnt_;

	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	request_t r;
	int ret = _ln_init_request(
		clnt, &r,
		"method", "r", "describe_message_definition",
		"md_name", "r", message_definition_name,
		NULL);
	if(ret)
		goto err0;

	ln_mutex_lock(&clnt->communication_mutex);
	ret = _ln_request_and_wait(&r);
	ln_mutex_unlock(&clnt->communication_mutex);
	if(ret)
		goto err1;

	if(message_description && (ret = _ln_get_from_request(&r, "description", "S", message_description)))
		goto err1;

err1:
	_ln_destroy_request(&r);
err0:
	return ret;
}

int ln_put_message_definition(ln_client clnt_, 
			      const char* message_definition_name, 
			      const char* message_definition,
			      char** real_message_definition_name) {
	client* clnt = (client*)clnt_;

	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	request_t r;
	int ret = _ln_init_request(
		clnt, &r,
		"method", "r", "put_message_definition",
		"md_name", "r", message_definition_name,
		"md", "r", message_definition,
		NULL);
	if(ret)
		return ret;

	ln_mutex_lock(&clnt->communication_mutex);
	ret = _ln_request_and_wait(&r);
	ln_mutex_unlock(&clnt->communication_mutex);
	if(ret) {
		_ln_destroy_request(&r);
		return ret;
	}

	if((ret = _ln_get_from_request(&r, "md_name", "S", real_message_definition_name))) {
		_ln_destroy_request(&r);
		return ret;
	}

	_ln_destroy_request(&r);

	return 0;
}

int ln_locate_member_in_message_definition(ln_client clnt_, const char* message_definition_name, const char* member, unsigned int* member_offset, unsigned int* member_byte_len) {
	client* clnt = (client*)clnt_;

	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	request_t r;
	int ret = _ln_init_request(
		clnt, &r,
		"method", "r", "locate_member_in_message_definition",
		"md_name", "r", message_definition_name,
		"member", "r", member,
		NULL);
	if(ret)
		return ret;

	ln_mutex_lock(&clnt->communication_mutex);
	ret = _ln_request_and_wait(&r);
	ln_mutex_unlock(&clnt->communication_mutex);
	if(ret) {
		_ln_destroy_request(&r);
		return ret;
	}

	if(member_offset) {
		if((ret = _ln_get_from_request(&r, "member_offset", "u", member_offset))) {
			_ln_destroy_request(&r);
			return ret;
		}
	}

	if(member_byte_len) {
		if((ret = _ln_get_from_request(&r, "member_byte_len", "u", member_byte_len))) {
			_ln_destroy_request(&r);
			return ret;
		}
	}

	_ln_destroy_request(&r);

	return 0;
}

int _ln_topic_mapping_destroy(topic_mapping_t* mapping) {
	free(mapping->src);
	free(mapping->dst);
	free(mapping);
	return 0;
}
int _ln_service_mapping_destroy(service_mapping_t* mapping) {
	free(mapping->src);
	free(mapping->dst);
	free(mapping);
	return 0;
}

void add_async_service_call(client* clnt, service* s) {
	ln_mutex_lock(&clnt->async_service_calls_mutex);
	_ln_client_append_async_service_call(clnt, s);	
	ln_mutex_unlock(&clnt->async_service_calls_mutex);
#ifndef __WIN32__
	// notify async thread:
	ln_pipe_send_notification(clnt->async_notification_pipe);
#endif
	return;
}
void remove_async_service_call(client* clnt, service* s) {
	ln_mutex_lock(&clnt->async_service_calls_mutex);
	_ln_client_remove_async_service_call(clnt, s);	
	ln_mutex_unlock(&clnt->async_service_calls_mutex);
	return;
}

int ln_needs_provider(ln_client clnt_, const char* service_or_topic_name, double timeout) {
	client* clnt = (client*)clnt_;
	
	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	request_t r;
	int ret = _ln_init_request(
		clnt, &r,
		"method", "r", "needs_provider",
		"service_or_topic_name", "r", service_or_topic_name,
		"timeout", "f", (float)timeout,
		NULL);
	if(ret)
		return ret;

	ln_mutex_lock(&clnt->communication_mutex);
	ret = _ln_request_and_wait(&r);
	ln_mutex_unlock(&clnt->communication_mutex);

	if(ret) {
		// failed
		char value[1024];
		_ln_get_from_request(&r, "success", "s1024", value);
		
		if(strstr(value, "no provider"))
			ret = -LNE_NO_PROVIDER_FOUND;
		else
			ret = -LNE_START_PROVIDER_FAILED;
	} /* else // success */

	_ln_destroy_request(&r);
	return ret;
}

int ln_get_library_version() {
	return LN_LIBRARY_VERSION;
}

int ln_client_find_services_with_interface(ln_client clnt_, const char* interface_name, char** services) {
	client* clnt = (client*)clnt_;
	
	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	if(!services)
		return -LNE_INVALID_PARAMETER;
	
	*services = NULL;

	request_t r;
	int ret = _ln_init_request(
		clnt, &r,
		"method", "r", "find_services_with_interface",
		"interface_name", "r", interface_name,
		NULL);
	if(ret)
		return ret;

	ln_mutex_lock(&clnt->communication_mutex);
	ret = _ln_request_and_wait(&r);
	ln_mutex_unlock(&clnt->communication_mutex);

	if(!ret) // success
		ret = _ln_get_from_request(&r, "services", "S", services);

	_ln_destroy_request(&r);
	return ret;

}

void _ln_iovec_mgr_print(client* clnt, struct _ln_iovec_mgr* mgr) {
	if(clnt->debug == 0)
		return;
	ln_debug(clnt, "%d iov's:\n", (int)mgr->msg.msg_iovlen);
	for(unsigned int i = 0; i < (unsigned)mgr->msg.msg_iovlen; i++) {
		ln_debug(clnt, "iov[%d]: %p, %d: ", i, mgr->msg.msg_iov[i].iov_base, (int)mgr->msg.msg_iov[i].iov_len);
		for(unsigned int k = 0; k < mgr->msg.msg_iov[i].iov_len; k++) {
			ln_debug(clnt, "%02x ", ((unsigned char*)mgr->msg.msg_iov[i].iov_base)[k]);
		}
		ln_debug(clnt, "\n");
	}
}

int ln_client_set_thread_safe(ln_client clnt_, int enable) {
	client* clnt = (client*)clnt_;
	
	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	if(enable)
		clnt->thread_safe = 1;
	else
		clnt->thread_safe = 0;
	return 0;
}

int ln_register_as_service_logger(ln_client clnt_, const char* logger_service_name, const char* service_name_patterns_to_log) {
	client* clnt = (client*)clnt_;
	
	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	struct service_* logger_service = NULL;
	if(clnt->services && clnt->n_services) {
		for(unsigned int i = 0; i < clnt->n_services; i++) {
			struct service_* service = clnt->services[i];
			
			if(!service->is_listening)
				continue;

			if(!strcmp(service->name, logger_service_name)) {
				logger_service = service;
				break;
			}
		}
	}
	if(!logger_service) {
		_ln_set_error(clnt, "can not register unknown service name %s as logger-service!", logger_service_name);
		return -LNE_SVC_NOT_REGISTERED;
	}
	logger_service->service_name_patterns_to_log = strdup(service_name_patterns_to_log);
	
	request_t r;
	int ret = _ln_init_request(
		clnt, &r,
		"method", "r", "register_as_service_logger",
		"logger_service_name", "r", logger_service_name,
		"service_name_patterns", "r", service_name_patterns_to_log,
		NULL);
	if(ret)
		return ret;

	ln_mutex_lock(&clnt->communication_mutex);
	ret = _ln_request_and_wait(&r);
	ln_mutex_unlock(&clnt->communication_mutex);
	_ln_destroy_request(&r);
	
	return ret;
}

int _ln_client_get_log_service(client* clnt, const char* log_service_name, log_service** s_) {
	int already_in = 0;
	for(unsigned int i = 0; i < clnt->n_log_services; i++) {
		if(!strcmp(clnt->log_services[i]->svc->name, log_service_name)) {
			already_in = 1;
			*s_ = clnt->log_services[i];
			break;
		}
	}
	if(already_in)
		return 0;
	return -1;
}

int _ln_client_add_log_service(client* clnt, const char* log_service_name, log_service** s_) {
	if(_ln_client_get_log_service(clnt, log_service_name, s_) == 0)
		return 0;

	// construct service client
	log_service* ls = (log_service*)calloc(1, sizeof(log_service));
	int ret;
	if((ret = ln_mutex_create(&ls->mutex))) {
		errno = ret;
		return -LNE_CHECK_ERRNO;
	}
	_ln_iovec_mgr_init(&ls->mgr);
	_ln_client_append_log_service(clnt, ls);
	
	ret = ln_service_init(clnt, (void**)&ls->svc, log_service_name, "ln/log_service", ln_log_service_signature);
	if(ret)
		return ret;
	*s_ = ls;
	return 0;
}

int ln_client_set_float_parameter(ln_client clnt_, const char* name, float value) {
	client* clnt = (client*)clnt_;
	
#define check_set_parameter(what) if(!strcmp(name, #what)) clnt->what = value;

	if(!strcmp(name, "__LN_CPP_WRAPPER_VERSION")) { // check version of cpp wrapper
		// no incompatibilities known
		return 0;
	}
	else if (!strcmp(name, "debug"))
		clnt->debug = (int)value;
	else check_set_parameter(win32_async_ping_timeout)
	else check_set_parameter(win32_async_ping_interval)
	else check_set_parameter(win32_wait_for_service_request_timeout)
	else check_set_parameter(reconnect_interval)
	else
		return -LNE_UNKNOWN_PARAMETER;
	
	return 0;
}

void ln_get_v16_features(unsigned int* is_with_sys_futex, unsigned int* is_glibc_atleast_2_25) {
	if(is_with_sys_futex)
		*is_with_sys_futex = IS_WITH_SYS_FUTEX;
	if(is_glibc_atleast_2_25)
		*is_glibc_atleast_2_25 = IS_GLIBC_ATLEAST_2_25;
}

unsigned int _ln_have_manager_input(client* clnt)
{
	/*
	  return how many unprocessed bytes there are in input buffer
	 */
	unsigned int in_buffer = clnt->input_buffer_len - clnt->input_buffer_pos;
	return in_buffer;
}

int _ln_read_from_manager(client* clnt, void* dst, unsigned int max_len)
{
	/*
	  return number of read bytes or negative error indicator

	  this should be the only code reading from clnt->sfd,
	  you also need to hold the comm-lock to use this function!
	 */

	unsigned int in_buffer = clnt->input_buffer_len - clnt->input_buffer_pos;
	if(in_buffer == 0) {
		// read from socket!
		while(true) {
			clnt->recv_count ++;
			int n = recv(clnt->sfd, clnt->input_buffer, sizeof(clnt->input_buffer), 0);
			if(n == -1) {
				if(_ln_platform_interrupted())
					continue;
				_ln_set_platform_error_message(clnt, "read_from_stream: ");
				return -LNE_LOST_CONNECTION;
			}
			if(n == 0)
				return -LNE_LOST_CONNECTION;
			clnt->input_buffer_len = n;
			clnt->input_buffer_pos = 0;
			break;
		}
		in_buffer = clnt->input_buffer_len - clnt->input_buffer_pos;
		//printf("new input buffer data %d bytes: '%*.*s'\n",
		//       in_buffer, in_buffer, in_buffer, clnt->input_buffer);
	}
	unsigned int to_take = MIN(max_len, in_buffer);
	memcpy(dst, &clnt->input_buffer[clnt->input_buffer_pos], to_take);
	clnt->input_buffer_pos += to_take;
	return to_take;
}

#ifdef WITH_FTRACE_EVENTS
void _open_ftrace()
{
	if(_ftrace_fd != -1)
		return;

	const char* do_ftrace = getenv("LN_FTRACE");
	if(do_ftrace == NULL || strchr("yt1", tolower(do_ftrace[0])) == NULL)
		return;

	_ftrace_fd = open("/sys/kernel/debug/tracing/trace_marker", O_WRONLY);
	if(_ftrace_fd == -1)
		printf("WARNING: this version of libln is compiled with -DWITH_FTRACE_EVENTS and LN_FTRACE was set, but opening trace_marker failed: %s!\n", strerror(errno));
}

void _ln_ftrace_write(const char* fmt, ...)
{
	va_list ap;
	char buf[1024];
	int n;

	if (_ftrace_fd < 0)
		return;

	va_start(ap, fmt);
	n = vsnprintf(buf, sizeof(buf), fmt, ap);
	va_end(ap);

	write(_ftrace_fd, buf, n);
}
#endif
