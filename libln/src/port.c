/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ln/ln.h"

#include "port.h"
#include "request.h"

// to have pipes and files 
#ifndef __WIN32__
#include <unistd.h>
#endif
#include <sys/types.h>
#include <errno.h>
//to have shm_source
#include "shm.h"

#include "ln/client_lib_messages.h"

#define MIN(a, b) ((a)<(b)?(a):(b))

int _ln_init_port(client* clnt, port* p, const char* topic, const char* message_definition, unsigned int message_size, char* msg_def_hash, port_type type, const char* shm_name) {
	memset(p, 0, sizeof(port));
	p->clnt = clnt;
	p->topic = strdup(topic);
	p->shm_name = strdup(shm_name);
	p->message_definition = strdup(message_definition);
	p->message_size = message_size;
	p->msg_def_hash = msg_def_hash;
	p->type = type;
	p->request_tid = 1;
	
	return 0;
}

#define _ln_port_record_tid(p) if((p)->request_tid) { (p)->last_tid = get_thread_id(); (p)->request_tid = 0; }

int _ln_destroy_port(port* p) {
	free(p->shm_name);
	// free(p->packet);
	free(p->topic);
	free(p->message_definition);
	if(p->msg_def_hash)
		free(p->msg_def_hash);
	return 0;
}

int _ln_get_message_size(port* p) {
	return p->message_size;
}
const char* _ln_get_msg_def_hash(port* p) {
	if(!p->msg_def_hash)
		return "";
	return p->msg_def_hash;
}

int _ln_outport_service_handler(ln_client clnt_, ln_service_request req, void* user_data) {
	client* clnt = (client*)clnt_;
	ln2_request_topic_t svc;
	ln_service_request_set_data(req, &svc, ln2_request_topic_signature);
	svc.resp.error_message_len = 0;
	svc.resp.data_len = 0;
	
	outport* port = NULL;
	// search inport matching to that topic!
	for(unsigned int i = 0; i < clnt->n_outports; i++) {
		if(!strncmp(clnt->outports[i]->hdr.topic, svc.req.topic_name, svc.req.topic_name_len)) {
			port = clnt->outports[i];
			break;
		}
	}
	if(!port) {
		char error_message[1024];
		snprintf(error_message, 1024, "request_topic('%*.*s'): no outport for this topic name!", svc.req.topic_name_len, svc.req.topic_name_len, svc.req.topic_name);
		svc.resp.error_message = error_message;
		svc.resp.error_message_len = strlen(error_message);
		ln_service_request_respond(req);
		return 0;
	}

	if(svc.req.timeout == -1) {
		// todo: blocking!
	} else if(svc.req.timeout > 0) {
		// todo: blocking with timeout!
	}
	
	// set last port data
	shm_t* shm = (shm_t*)port->target;

	unsigned int last_written = SHM_GET_PREV_ELEMENT(shm, shm->hdr->next_write);
	if(!shm->hdr->is_full && last_written == shm->hdr->n_elements) {
		// no element written yet!
		svc.resp.data = NULL;
		svc.resp.data_len = 0;
		svc.resp.counter = 0;
		svc.resp.timestamp = 0;
	} else {
		shm_header_element_t* element = SHM_GET_ELEMENT(shm, last_written);
		svc.resp.data = (uint8_t*)SHM_ELEMENT_GET_DATA(element);
		svc.resp.data_len = port->hdr.message_size;
		svc.resp.counter = element->data_counter;
		svc.resp.timestamp = element->data_timestamp;
	}

	ln_service_request_respond(req);
	
	return 0;
}

int _ln_init_outport(client* clnt, outport* p, const char* topic, const char* message_definition, unsigned int message_size, char* msg_def_hash, const char* shm_name) {
	int ret;
	memset(p, 0, sizeof(outport));
	if((ret = _ln_init_port(clnt, &p->hdr, topic, message_definition, message_size, msg_def_hash, OUTPUT_PORT, shm_name)))
		return ret;

	lock_client(clnt);
	_ln_client_append_outport(clnt, p);
	unlock_client(clnt);
	
	ret = open_shm_target(&p->target, shm_name);
	if(ret) {
		_ln_outport_destroy(p);
		return ret;
	}
	ln_debug(clnt, "opened shm target %p, name %s, version %d\n", p->target, shm_name, ((shm_t*)p->target)->hdr->version);
	
	p->have_shm_target = 1;

	if(clnt->n_outports == 1 && !clnt->request_topic_svc) {
		// first outport -> register request_topic service!
		char service_name[1024];
		snprintf(service_name, 1024, "%s.request_topic", clnt->program_name);

		uint8_t do_async_handling = 0;
		if(clnt->n_services == 0)
			do_async_handling = 1;
		
		ret = ln_service_init(clnt, &clnt->request_topic_svc, service_name, "ln2/request_topic", ln2_request_topic_signature);
		if(ret) {
			ln_debug(clnt, "_ln_init_outport('%s') ln_service_init('%s') failed: %d\n", topic, service_name, ret);
			return ret;
		}

		ret = ln_service_provider_set_handler(clnt->request_topic_svc, _ln_outport_service_handler, NULL);
		if(ret) {
			ln_debug(clnt, "_ln_init_outport('%s') ln_service_provider_set_handler('%s') failed: %d\n", topic, service_name, ret);
			return ret;
		}

		ret = ln_service_provider_register(clnt, clnt->request_topic_svc);
		if(ret) {
			ln_debug(clnt, "_ln_init_outport('%s') ln_service_provider_register('%s') failed: %d\n", topic, service_name, ret);
			return ret;
		}

		if(do_async_handling) {
			clnt->do_async_service_handling = 1;
#ifndef __WIN32__
			// notify async thread:
			ln_pipe_send_notification(clnt->async_notification_pipe);
#endif
		}
	}

	return 0;
}

int _ln_outport_destroy(outport* p) {
	if(p->have_shm_target)
		close_shm_target(&p->target);
	_ln_destroy_port(&p->hdr);
	_ln_client_remove_outport(p->hdr.clnt, p);
	free(p);
	return 0;
}

int _ln_has_subscriber(outport* p) {
	return p->has_subscriber;
}

int _ln_write(outport* p, void* data, unsigned int len, double* ts) {
	if(len > p->hdr.message_size) {
		_ln_set_error(p->hdr.clnt, "ln_write(%s): your packet has %d bytes, expected a packet of %d bytes only!\n", 
					  p->hdr.topic, len, p->hdr.message_size);
		return -LNE_PACKET_TOO_LARGE;
	}
	
	double timestamp;
	if(!ts) {
		timestamp = ln_get_time(); // not using monotonous time because might be sent over network
		ts = &timestamp;
	}
	
	_ln_port_record_tid(&p->hdr);
	
	if(!p->have_shm_target)
		return 0;
	if(p->have_new_shm_target) {
		// switch target!
		close_shm_target(&p->target);
		p->target = p->new_target;
		p->have_new_shm_target = 0;
	}
	_ln_ftrace_print("ln_write %s ts %f", p->hdr.topic, *ts);
	int ret = write_to_shm_target(p->target, *ts, data, len);
	_ln_ftrace_print("ln_write %s ret %d", p->hdr.topic, ret);
	return ret;
}

int _ln_outport_get_next_buffer(outport* p, void** buffer, unsigned int* buffer_len) {
	if(!p->have_shm_target) {
		*buffer = 0;
		if(buffer_len)
			*buffer_len = 0;
		return -LNE_SHM_DOES_NOT_EXIT;
	}
	
	if(p->have_new_shm_target) {
		// switch target!
		close_shm_target(&p->target);
		p->target = p->new_target;
		p->have_new_shm_target = 0;
	}
	if(buffer_len)
		*buffer_len = p->hdr.message_size;
	
	return get_next_buffer_from_shm_target(p->target, buffer);
}

int _ln_init_inport(client* clnt, inport* p, const char* topic, const char* message_definition, unsigned int message_size, char* msg_def_hash, double rate, const char* shm_name) {
	int ret;
	memset(p, 0, sizeof(inport));
	if((ret = _ln_init_port(clnt, &p->hdr, topic, message_definition, message_size, msg_def_hash, INPUT_PORT, shm_name))) {
		ln_debug(clnt, "_ln_init_port('%s') failed: %d\n", shm_name, ret);
		return ret;
	}
	p->rate = rate;

	lock_client(clnt);
	_ln_client_append_inport(clnt, p);
	unlock_client(clnt);
	
	if(strlen(shm_name)) {
		if((ret = open_shm_source(&p->source, shm_name))) {
			ln_debug(clnt, "open_shm_source('%s', %d) failed: %d\n", shm_name, message_size, ret);
			_ln_inport_destroy(p);
			return ret;
		}
		shm_source_get_last_counter(p->source, &p->hdr.packet_counter);
		ln_debug(clnt, "opened shm source %p, name %s, version %d\n", p->source, shm_name, ((shm_t*)p->source)->hdr->version);
		p->have_shm_source = 1;
		p->have_delayed_shm_sem = 0;
	} else {
		p->have_shm_source = 0;
		// create local semaphore to async notify blocking port reads
		p->have_delayed_shm_sem = 1;
		ln_semaphore_init(&p->delayed_shm_sem, 0);
	}
	ln_debug(clnt, "_ln_init_inport('%s') succeeded\n", shm_name);
	return 0;
}

int _ln_delayed_init_inport(inport* p, const char* shm_name) {
	int ret;
	free(p->hdr.shm_name);
	p->hdr.shm_name = strdup(shm_name);

	if((ret = open_shm_source(&p->source, shm_name))) {
		ln_debug(p->hdr.clnt, "ln_delayed_init_inport: open_shm_source('%s') failed: %d\n", shm_name, ret);
		// _ln_inport_destroy(p);
		return ret;
	}
	shm_source_get_last_counter(p->source, &p->hdr.packet_counter);

	ln_debug(p->hdr.clnt, "opened shm source %p, name %s, version %d\n", p->source, shm_name, ((shm_t*)p->source)->hdr->version);
	p->have_shm_source = 1;

	if(p->have_delayed_shm_sem) {
		// post sem
		ln_semaphore_post(p->delayed_shm_sem);
		p->have_delayed_shm_sem = 0;
	}
	ln_debug(p->hdr.clnt, "_ln_delayed_init_inport('%s') succeeded\n", shm_name);
	return 0;
}

int _ln_reinit_outport(outport* p, const char* shm_name) {
	if(p->have_new_shm_target)
		ln_debug(p->hdr.clnt, "ln_reinit_outport: warning, already have new shm target!\n");
	int ret = open_shm_target(&p->new_target, shm_name);
	if(ret)
		return ret;
	ln_debug(p->hdr.clnt, "re-opened shm target %p, name %s, version %d\n", p->new_target, shm_name, ((shm_t*)p->new_target)->hdr->version);
	p->have_new_shm_target = 1;
	// wait until once written to new target!
	uint32_t packet_counter = 0;
	block_only_on_shm_source(p->new_target, &packet_counter, 1);
	
	free(p->hdr.shm_name);
	p->hdr.shm_name = strdup(shm_name);

	return ret;
}

int _ln_inport_destroy(inport* p) {
	// check whether there are waiters waiting for this inport!
	for(unsigned int k = 0; k < p->hdr.clnt->n_multi_waiters; k ++) {
		multi_waiter* w = p->hdr.clnt->multi_waiters[k];
		for(unsigned int i = 0; i < w->n_multi_waiter_ports; i++) {
			multi_waiter_port* mwp = w->multi_waiter_ports[i];
			if(mwp->port != p)
				continue;
			_ln_multi_waiter_port_destroy(mwp);
			_ln_multi_waiter_remove_multi_waiter_port(w, mwp);
			break;
		}
	}

	if(p->have_delayed_shm_sem) {
		if(ln_semaphore_getvalue(p->delayed_shm_sem))
			ln_semaphore_post(p->delayed_shm_sem);
		ln_semaphore_destroy(p->delayed_shm_sem);
	}

	if(p->have_shm_source)
		close_shm_source(&p->source);
	if(p->needs_endianess_swap)
		free(p->needs_endianess_swap);
	_ln_destroy_port(&p->hdr);
	_ln_client_remove_inport(p->hdr.clnt, p);
	free(p);
	return 0;
}

int _ln_has_publisher(inport* p) {
	return p->has_publisher;
}

int _ln_block_only(inport* p, uint32_t* last_packet_counter, double timeout) {
	if(!p->have_shm_source) {
		if(timeout <= 0)
			return 0; // no new packet
		ln_semaphore_timedwait(p->delayed_shm_sem, timeout);
		if(!p->have_shm_source)
			return 0; // timeout
	}

	int ret = block_only_on_shm_source(p->source, last_packet_counter, timeout);
	
	if(ret == -2)
		return -LNE_PORT_UNBLOCKED;
	
	return ret;
}

int _ln_read_via_service(inport* p, void* data, unsigned int len, double* ts) {
	client* clnt = p->hdr.clnt;
	int ret;
	// only non-blocking semantics!
		
	if(!p->has_publisher) {
		ln_debug(clnt, "port does not yet have a publisher...\n");
		return 0;
	}
			
	int tried_reconnect = 0;
	ln2_request_topic_t svc;
	svc.req.topic_name = p->hdr.topic;
	svc.req.topic_name_len = strlen(p->hdr.topic);
	svc.req.timeout = 0;

	while(1) {
		if(!p->on_request_svc) {
			// ask manager who publishes this topic!

			request_t r;
			ret = _ln_init_request(
				clnt, &r,
				"method", "r", "get_topic_publisher_service_name",
				"topic", "r", p->hdr.topic,
				NULL);
			if(ret)
				return ret;
				
			ln_mutex_lock(&clnt->communication_mutex);
			ret = _ln_request_and_wait(&r);
			ln_mutex_unlock(&clnt->communication_mutex);
			if(ret) {  // probably not yet published...
				_ln_destroy_request(&r);
				return 0; // ret;
			}		
				
			char service_name[1024];
			if((ret = _ln_get_from_request(&r, "service_name", "s1024", &service_name))) {
				_ln_destroy_request(&r);
				return ret;
					
			}

			char endianess_swap[1024];
			if((ret = _ln_get_from_request(&r, "endianess_swap", "s1024", &endianess_swap))) {
				// assume no endianess swap is needed!				
			} else {
				if(p->needs_endianess_swap) {
					free(p->needs_endianess_swap);
					p->needs_endianess_swap = NULL;
				}
				if(endianess_swap[0]) {
					p->needs_endianess_swap = (uint8_t*)malloc(strlen(endianess_swap) + 1);
					char* cp = endianess_swap;
					uint8_t* op = p->needs_endianess_swap;
					while(*cp)
						*(op++) = *(cp++) - '0';
					*op = 0;
				}
			}
			
			_ln_destroy_request(&r);
				
			// if not yet published return 0
				
			// read port via service!
			ret = ln_service_init(clnt, &p->on_request_svc, service_name, "ln2/request_topic", ln2_request_topic_signature);
			if(ret) {
				ln_debug(clnt, "ln_read() ln_service_init() failed!\n");
				return ret;
			}
		}
			
		ret = ln_service_call(p->on_request_svc, &svc);
		if(!ret) {
			if(svc.resp.error_message_len) {
				ln_debug(clnt, "error while calling ON_REQUEST-service:\n%*.*s", svc.resp.error_message_len, svc.resp.error_message_len, svc.resp.error_message);
				if(tried_reconnect)
					return -LNE_SVC_HANDLER_EXC;
			} else
				break;
		}
		if(tried_reconnect)
			return ret;
		tried_reconnect++;
			
		ln_service_deinit(&p->on_request_svc);
		p->on_request_svc = 0;
	}
		
	// service call succeeded
	p->can_read = 0;
		
	shm_t* shm = (shm_t*)p->source;

	_ln_port_record_tid(&p->hdr);

	unsigned int last_written = SHM_GET_PREV_ELEMENT(shm, shm->hdr->next_write);
	shm_header_element_t* element = SHM_GET_ELEMENT(shm, last_written);

	shm->hdr->is_full = 1;
	if(!p->needs_endianess_swap)
		memcpy(data, svc.resp.data, MIN(len, svc.resp.data_len));
	else
		_ln_memcpy_swap_hetero(data, svc.resp.data, MIN(len, svc.resp.data_len), p->needs_endianess_swap);
	memcpy(SHM_ELEMENT_GET_DATA(element), data, MIN(p->hdr.message_size, svc.resp.data_len)); // todo: remove shm writing?
		
	p->hdr.packet_counter = element->data_counter = shm->hdr->last_data_counter = svc.resp.counter;
	element->data_timestamp = svc.resp.timestamp;
	if(ts)
		*ts = svc.resp.timestamp;
		
	return 1;
}

int _ln_read(inport* p, void* data, unsigned int len, double* ts, int blocking) {
	int ret;

	if(!p->have_shm_source) {
		ln_debug(p->hdr.clnt, "don't have shm source yet.\n");
		if(!blocking) {
			ln_debug(p->hdr.clnt, "not blocking -> no packet!\n");
			return 0; // no new packet
		}
		ln_debug(p->hdr.clnt, "blocking -> wait on semphore!\n");
		ln_semaphore_wait(p->delayed_shm_sem);
		ln_debug(p->hdr.clnt, "have semphore!\n");
		if(!p->have_shm_source)
			return 0; // error/exit
		ln_debug(p->hdr.clnt, "have shm!\n");
	}

	if(p->rate == RATE_ON_REQUEST_LAST || p->rate == RATE_ON_REQUEST_NEXT) {
		if(blocking) {
			_ln_set_error(p->hdr.clnt, "blocking read from subscribed topic %s with ON_REQUEST-rate not yet implemented!\n", p->hdr.topic);
			return -LNE_NOT_YET_IMPLEMENTED;
		}
		return _ln_read_via_service(p, data, len, ts);
	}
	
	_ln_port_record_tid(&p->hdr);
	
	// ln_debug(p->hdr.clnt, "doing read_from_shm_source!\n");
	ret = read_from_shm_source(p->source, ts, &p->hdr.packet_counter, data, len, blocking);
	//ln_debug(p->hdr.clnt, "ret: %d!\n", ret);

	if(ret == -2)
		return -LNE_PORT_UNBLOCKED;
	
	if(ret < 0)
		return -LNE_SHM_READ_ERROR;
	
	p->can_read = 0;
	
	if(ret == 0) // no new packet
		return 0;

	return 1; // got new packet
}

int _ln_read_timeout(inport* p, void* data, unsigned int len, double* ts, double timeout) {
	if(!p->have_shm_source) {
		if(timeout <= 0)
			return 0; // no new packet
		ln_semaphore_timedwait(p->delayed_shm_sem, timeout);
		if(!p->have_shm_source)
			return 0; // timeout
	}


	if(p->rate == RATE_ON_REQUEST_LAST || p->rate == RATE_ON_REQUEST_NEXT) {
		if(timeout == -1) {
			_ln_set_error(p->hdr.clnt, "blocking read (timeout==%f) from subscribed topic %s with ON_REQUEST-rate not yet implemented!\n",
			      timeout, p->hdr.topic);
			return -LNE_NOT_YET_IMPLEMENTED;
		}
		if(timeout > 0) {
			_ln_set_error(p->hdr.clnt, "read with timeout (timeout==%f) from subscribed topic %s with ON_REQUEST-rate not yet implemented!\n",
			      timeout, p->hdr.topic);
			return -LNE_NOT_YET_IMPLEMENTED;
		}
		return _ln_read_via_service(p, data, len, ts);
	}
	
	_ln_port_record_tid(&p->hdr);

	int ret = read_from_shm_source_timeout(p->source, ts, &p->hdr.packet_counter, data, len, timeout);
	
	if(ret == -2)
		return -LNE_PORT_UNBLOCKED;
	
	if(ret == 0) // timeout!
		return 0;
	
	// got new packet!
	p->can_read = 0;
	return 1; // got new packet
}

