/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef __VXWORKS__

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <netdb.h>
#include <sys/socket.h> 
#include <time.h>
#include <string.h>
#include <ioLib.h>
#include <pipeDrv.h>

#include "os.h"
#include "ln/ln.h"

#include "ln_helper_api.h"
/*
  
cd /opt/vxworks
./wrenv.sh -p vxworks-6.9
 */

int ln_semaphore_create(ln_semaphore_handle_t* handle, const char* name, int create) {
	unsigned int sem_open_flags = 0;
	// mode_t old_mask;
	if(create) {
		sem_open_flags |= O_CREAT;
		// not in 6.9 old_mask = umask(0);
	}
	*handle = sem_open(name, sem_open_flags, 0777, 0);
	/* not in 6.9
	   if(create == 1)
		umask(old_mask);
	*/
	if(*handle == SEM_FAILED)
		return -1;
	return 0;
}
int ln_semaphore_close(ln_semaphore_handle_t handle) {
	return sem_close(handle);
}
int ln_semaphore_unlink(const char* name) {
	return sem_unlink(name);
}


int ln_semaphore_init(ln_semaphore_handle_t* sem, unsigned int value) {
	*sem = (sem_t*)malloc(sizeof(sem_t));
	return sem_init(*sem, 0, value);
}
int ln_semaphore_destroy(ln_semaphore_handle_t sem) {
	int ret = sem_destroy(sem);
	free(sem);
	return ret;
}


int ln_semaphore_wait(ln_semaphore_handle_t handle) {
	int ret;
	UNTIL_RET_NOT_EINTR(ret, sem_wait(handle));
	return ret; // 0 on success
}
int ln_semaphore_trywait(ln_semaphore_handle_t handle) {
	int ret;
	UNTIL_RET_NOT_EINTR(ret, sem_trywait(handle));
	return ret;
}
int ln_semaphore_timedwait(ln_semaphore_handle_t handle, double seconds) {
	struct timespec ts;
	ln_double2ts(ln_get_time_sys() + seconds, &ts);
	int ret;
	UNTIL_RET_NOT_EINTR(ret, sem_timedwait(handle, &ts));
	return ret;
}
int ln_semaphore_timedwait_abs(ln_semaphore_handle_t handle, double abs_timeout) {
	struct timespec ts;
	ln_double2ts(abs_timeout, &ts);
	int ret;
	UNTIL_RET_NOT_EINTR(ret, sem_timedwait(handle, &ts));
	return ret;
}
int ln_semaphore_post(ln_semaphore_handle_t handle) {
	return sem_post(handle);
}
int ln_semaphore_getvalue(ln_semaphore_handle_t handle) {
	/* 
	   seems to be not implemented. at least in vxw6.8
	*/
	int val;
	if ((intptr_t)handle == -1)
		return -1;
	int ret = sem_getvalue(handle, &val);
	if(ret == -1)
		return -1;
	return val;
	// return 0; // safe default
}

int ln_mutex_create(ln_mutex_handle_t* mutex_handle) {
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutexattr_setprotocol(&attr, PTHREAD_PRIO_INHERIT);
	int ret = pthread_mutex_init(mutex_handle, &attr);
	pthread_mutexattr_destroy(&attr);
	return ret;
}

int ln_mutex_destroy(ln_mutex_handle_t* mutex_handle) {
	return pthread_mutex_destroy(mutex_handle);
}

int ln_mutex_lock(ln_mutex_handle_t* mutex_handle) {
	return pthread_mutex_lock(mutex_handle);
}

int ln_mutex_trylock(ln_mutex_handle_t* mutex_handle) {
	return pthread_mutex_trylock(mutex_handle);
}

int ln_mutex_unlock(ln_mutex_handle_t* mutex_handle) {
	return pthread_mutex_unlock(mutex_handle);
}

int ln_thread_create(ln_thread_handle_t* thread_handle, thread_func_type* thread, void* data) {
	return pthread_create(thread_handle, NULL, thread, data);
}
int ln_thread_create_with_prio(ln_thread_handle_t* thread_handle, thread_func_type* thread, void* data, int policy, int priority, const char* name) {
	return ln_thread_create_with_params(thread_handle, thread, data, policy, priority, 0, name);
}

int ln_thread_create_with_params(ln_thread_handle_t* thread_handle, thread_func_type* thread, void* data, int policy, int priority, unsigned int affinity_mask, const char* name) {
	int ret;
	int explicit_sched = 0;
	pthread_attr_t attr;
	pthread_attr_init(&attr);

	if(policy != -1) {
		ret = pthread_attr_setschedpolicy(&attr, policy);
		if(ret)
			fprintf(stderr, "failed to set policy to %d: %d %s\n", policy, ret, strerror(ret));
		explicit_sched = 1;
	}
	
	if(priority != -1) {
		struct sched_param param;
		ret = pthread_attr_getschedparam(&attr, &param);
		if(ret)
			fprintf(stderr, "failed to get sched_param: %d %s\n", ret, strerror(ret));
		else {	
			param.sched_priority = priority;
			ret = pthread_attr_setschedparam(&attr, &param);
			if(ret)
				fprintf(stderr, "failed to set sched_param with prio %d: %d %s\n", priority, ret, strerror(ret));
		}
		explicit_sched = 1;
	}

	// todo: how to specify thread affinity of to-be-started thread without pthread_attr_setaffinity_np?

	if(explicit_sched) {
		ret = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
		if(ret)
			fprintf(stderr, "failed to get sched_param: %d %s\n", ret, strerror(ret));
	}

	ret = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	if(ret)
		fprintf(stderr, "failed to set detachstate: %d %s\n", ret, strerror(ret));

	if(name)
		pthread_attr_setname(&attr, (char*)name);
	
	ret = pthread_create(thread_handle, &attr, thread, data);
	pthread_attr_destroy(&attr);
	if(!ret) {
		return 0;
	}
	errno = ret;
	return -LNE_CHECK_ERRNO;
}

int ln_thread_setschedparam(ln_thread_handle_t thread_handle, int policy, int priority, unsigned int affinity_mask) {
	int ret;
	if(policy != -1 || priority != -1) {
		int current_policy;
		struct sched_param current_param;
		pthread_getschedparam(thread_handle, &current_policy, &current_param);

		if(policy == -1)
			policy = current_policy;

		if(priority != -1)
			current_param.sched_priority = priority;
		
		ret = pthread_setschedparam(thread_handle, policy, &current_param);
		if(ret) {
			fprintf(stderr, "failed to set policy to %d, priority to %d: %d %s\n", policy, current_param.sched_priority, ret, strerror(ret));
			errno = ret;
			return -LNE_CHECK_ERRNO;
		}
	}
	
	// todo: how to specify thread affinity of already running pthread without pthread_attr_setaffinity_np?
	
	return 0;
}

void ln_thread_cancel(ln_thread_handle_t t) {
	pthread_cancel(t);
}

int ln_thread_join(ln_thread_handle_t t, void** retval) {
	return pthread_join(t, retval);
}


int ln_thread_test_cancel(ln_thread_handle_t t) {
	pthread_testcancel();
	return 0;
}

double ln_ts2double(struct timespec* ts) {
	return (double)ts->tv_sec + (ts->tv_nsec / 1e9);
}

void ln_double2ts(double s, struct timespec* ts) {
	ts->tv_sec = (time_t)s;
	ts->tv_nsec = (long)((s - ts->tv_sec) * 1e9);
}

#ifndef ARMEL
uint64_t rdtsc(void) {
    unsigned a, d;
    __asm__ volatile("rdtsc" : "=a" (a), "=d" (d));
    return ((uint64_t)a) | (((uint64_t)d) << 32);
}

typedef struct {
	int initialized;
	uint64_t start;
	double start_time;

	double cycles_per_second;
	uint64_t cycle_offset;
	double time_offset;
} rdtsc_clock_t;
	
static rdtsc_clock_t rdtsc_clock = { 0 };
#endif

double ln_get_time_sys() {
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	return ln_ts2double(&ts);
}

#ifdef ARMEL
double ln_get_time() {
	return ln_get_time_sys();
}
#else
double ln_get_time() {
	/** clock_gettime(CLOCK_REALTIME) has only timer-resolution in qnx6.3 - so used rdtsc replacement!
	    
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	return ln_ts2double(&ts);
	*/

	if(rdtsc_clock.initialized == 0) {
		// first, rough measurement
		rdtsc_clock.start_time = ln_get_time_sys();
		rdtsc_clock.start = rdtsc();
		ln_sleep_seconds(0.250);
		double end_time = ln_get_time_sys();
		uint64_t end = rdtsc();

		double delta = end_time - rdtsc_clock.start_time;
		rdtsc_clock.cycles_per_second = (double)(end - rdtsc_clock.start) / delta;
		// printf("first rdtsc_clock.cycles_per_second: %f\n", rdtsc_clock.cycles_per_second);
		
		rdtsc_clock.cycle_offset = end;
		rdtsc_clock.time_offset = end_time;
		
		rdtsc_clock.initialized = 1; // have rough calibration
	} else if(rdtsc_clock.initialized == -1) {
		
	} else if((rdtsc_clock.initialized % 1000) == 0) {
		// every 1000'th call
		double end_time = ln_get_time_sys();
		uint64_t end = rdtsc();

		double delta = end_time - rdtsc_clock.start_time;
		if(delta >= 10) {
			rdtsc_clock.initialized = -1; // finish
			// new calibration!
			//double before = ln_get_time();
			double a = 0.8;
			rdtsc_clock.cycles_per_second = a * rdtsc_clock.cycles_per_second + (1-a)*(double)(end - rdtsc_clock.start) / delta;
		
			rdtsc_clock.cycle_offset = end;
			rdtsc_clock.time_offset = end_time;

			/*
			double new_delta = ln_get_time() - before;
			printf("new  rdtsc_clock.cycles_per_second: %f, ln_get_time() delta: %.3fus\n",
			       rdtsc_clock.cycles_per_second,
				new_delta * 1e6);
			*/
		} else
			rdtsc_clock.initialized = 1;
		
	} else
		rdtsc_clock.initialized++;

	return rdtsc_clock.time_offset + (double)(rdtsc() - rdtsc_clock.cycle_offset) / rdtsc_clock.cycles_per_second;
}
#endif // ARMEL

double ln_get_monotonic_time() {
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return ln_ts2double(&ts);
}


void ln_sleep_seconds(double s) {
	struct timespec ts;
	ln_double2ts(s, &ts);
	nanosleep(&ts, NULL);
}

char *strndup(const char *s, size_t n) {
	const char* cp = s;
	size_t i = 0;
	while(*cp) {
		i++;
		if(i >= n)
			break; // enough chars
		cp++;
	}
	i ++;
	char* result = (char*)malloc(i);
	memcpy(result, s, i);
	result[i - 1] = 0;
	return result;
}

int ln_cond_create(ln_cond_handle_t* cond) {
	pthread_condattr_t attr;
	int ret;
	if((ret = pthread_condattr_init(&attr)))
		return ret;
	if((ret = pthread_condattr_setclock(&attr, CLOCK_MONOTONIC)))
		return ret;
	if((ret = pthread_cond_init(cond, &attr)))
		return ret;
	if((ret = pthread_condattr_destroy(&attr)))
		return ret;
	return 0;
}
int ln_cond_destroy(ln_cond_handle_t* cond) {
	return pthread_cond_destroy(cond);
}
int ln_cond_wait(ln_cond_handle_t* cond, ln_mutex_handle_t* mutex) {
	ret
		urn pthread_cond_wait(cond, mutex);
}
int ln_cond_timedwait(ln_cond_handle_t* cond, ln_mutex_handle_t* mutex, double abs_timeout) {
	struct timespec ts;
	ln_double2ts(abs_timeout, &ts);
	return pthread_cond_timedwait(cond, mutex, &ts);
}
int ln_cond_broadcast(ln_cond_handle_t* cond) {
	return pthread_cond_broadcast(cond);
}
int ln_cond_signal(ln_cond_handle_t* cond) {
	return pthread_cond_signal(cond);
}

#define PIPE_NAME_MAX 512

int ln_pipe_init(ln_pipe_t* pipe) {
	static int pipe_count = 0;
	char name[PIPE_NAME_MAX];
	snprintf(name, PIPE_NAME_MAX, "/unixpipe_%d_%d", getpid(), pipe_count++);
	if (pipeDevCreate(name, 10, 1024) == ERROR)
		return -LNE_CHECK_ERRNO;

	pipe->input_fd  = open(name, O_RDONLY | O_NONBLOCK, 777);
	pipe->output_fd = open(name, O_WRONLY, 777);

	if(pipe->input_fd == -1 || pipe->output_fd == -1)
		return -LNE_CHECK_ERRNO;

	return 0;
}

//! closes os dependant pipe device
/*!
  \param reference to pipe device structure
  \return success or error code
  */
int ln_pipe_close(ln_pipe_t* pipe) {
	char name[PIPE_NAME_MAX];
	if(ioctl(pipe->input_fd, FIOGETNAME, name) == ERROR) 
		return -LNE_CHECK_ERRNO;

	ioctl(pipe->input_fd, FIOFLUSH, 0);

	if (pipe->output_fd != -1)
		ioctl(pipe->output_fd, FIOFLUSH, 0);

	close(pipe->input_fd);
	if (pipe->output_fd != -1)
		close(pipe->output_fd);

	if (pipeDevDelete(name, 0) == ERROR)
		return -LNE_CHECK_ERRNO;

	return 0;
}

void ln_pipe_send_notification(ln_pipe_t pipe) {
	char n = '\n';
	write(pipe.output_fd, &n, 1);
}

void ln_pipe_read_notification(ln_pipe_t pipe) {
	char n = '\n';
	read(pipe.input_fd, &n, 1);
}

#include <taskLib.h>

static int ln_helper_fd = -1;
int my_vxworks_tid() {
	if(ln_helper_fd == -1) {
		ln_helper_fd = open("/dev/ln_helper", O_RDWR);
		if(ln_helper_fd == -1)
			return 0;
	}
	int tid;
	// int ret = ioctl(ln_helper_fd, LN_HELPER_GET_TASKIDSELF, (int)&tid);
	int ret = ioctl(ln_helper_fd, LN_HELPER_GET_TASKIDSELF, &tid);
	if(ret)
		return 0;
	
	//WIND_TCB* tid = (WIND_TCB*)taskIdSelf();
	// void* tid = (void*)taskIdSelf();
	// printf("tid: 0x%x\n", tid);
	// printf("rtpId: 0x%x\n", tid->rtpId);
	// printf("priority: 0x%x\n", *(UINT*)((uint8_t*)tid + 0x74) /*priority*/);
	printf("tid from kernel helper: 0x%x\n", tid);
	return tid; // ->rtpId;
}

void _ln_close_socket(_ln_socket_t fd) {
	close(fd);
}

void ln_thread_setname(const char* new_name) {
	// not on vxw?
}

#endif // __VXWORKS__
