/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <stdlib.h>
#include <string.h>

#include "repr.h"
#include "event.h"

int event_connection_call_handler(ln_client clnt, ln_service_request req, void* user_data) {
	event_connection* c = (event_connection*)user_data;

	ln_debug(clnt, "event connection call handler %p\n", c);

	ln_event_call_t event_call_data;
	ln_service_request_set_data(req, &event_call_data, ln_event_call_signature);

	if(c->call_handler) {
		event_call call = { c, req, &event_call_data };
		ln_debug(clnt, "event connection call handler %p: will now call user handler\n", c);
		c->call_handler(clnt, &call, c->call_user_data);
	}
	// todo: do this before calling user handler!
	ln_debug(clnt, "event connection call handler %p: responding\n", c);
	ln_service_request_respond(req);

	ln_debug(clnt, "event connection call handler %p: done\n", c);
	return 0;
}

int ln_event_call_set_data(ln_event_call call_, const char* call_signature, void* call_data) {
	event_call* call = (event_call*)call_;

	if(strncmp(call->data->req.signature, call_signature, call->data->req.signature_len))
		return -LNE_SVC_REQ_SET_DATA_WRONG;

	if(call->connection->have_call_fields == 0) {
		// parse call signature once
		char* sig_cp = (char*)strstr(call_signature, ": ");
		if(!sig_cp)
			return -LNE_INVALID_SIGNATURE;
		sig_cp += 2;
		_ln_parse_signature(&call->connection->call_fields, sig_cp);
		if(call->connection->call_fields.n_inner_fields)
			call->connection->call_fields.field_size = 1; // we have exactly one request!
		else
			call->connection->call_fields.field_size = 0;
		call->connection->have_call_fields = 1;
	}
	
	char* host_data_ptr = (char*)call_data;
	char* buffer_ptr = (char*)call->data->req.data;
	unsigned int left_in_buffer = call->data->req.data_len;
	
	_ln_aux_mem_clear(&call->connection->aux_mem); // needs free aux mem in detor
	
	if(call->connection->call_fields.n_inner_fields) {
		int ret = _ln_fill_structure(&call->connection->aux_mem, &call->connection->call_fields, &host_data_ptr, &buffer_ptr, &left_in_buffer, 0); // todo: don't know if we need endianess swap!
		if(ret)
			return -LNE_SVC_RECV_REQ;
	}
	return 0;
}

int ln_event_call_get_buffer(ln_event_call call_, void** ptr, uint32_t* len) {
	event_call* call = (event_call*)call_;
	*ptr = (void*)call->data->req.data;
	*len = call->data->req.data_len;
	return 0;
}


int _ln_event_connect(
	client* clnt, ln_event_connection* conn,
	const char* event_name, const char* connect_signature, void* connect_data_flat, uint32_t connect_data_flat_len,
	ln_event_handler handler, void* user_data, int have_flat_input) {
	
	*conn = NULL;

	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	int ret;

	char* sig_cp = (char*)strstr(connect_signature, ": ");
	if(!sig_cp) {
		ln_debug(clnt, "connect_signature contains no ': '");
		return -LNE_INVALID_SIGNATURE;
	}
	sig_cp += 2;

	// start call provider
	ln_service call_svc;	
	char* call_svc_name = NULL;
	unsigned int call_svc_name_size = 0;
	char* cp = call_svc_name;
	growing_buffer_append(&call_svc_name, &cp, &call_svc_name_size, event_name);
	growing_buffer_append(&call_svc_name, &cp, &call_svc_name_size, ".");
	growing_buffer_appendn(&call_svc_name, &cp, &call_svc_name_size, clnt->program_name, strlen(clnt->program_name) + 1);
		
	ret = ln_service_init(clnt, &call_svc, call_svc_name, "ln/event/call", ln_event_call_signature);
	if(ret) {
		ln_debug(clnt, "failed to init event call handler service!");
		free(call_svc_name);
		return ret;
	}

	event_connection* c = (event_connection*)calloc(1, sizeof(event_connection));
	c->clnt = clnt;
	c->event_name = strdup(event_name);
	c->is_internal_event = !strncmp(c->event_name, "ln.", 3);
	c->connect_signature = strdup(connect_signature);
	c->call_svc = call_svc;
	c->call_svc_name = call_svc_name;
	c->call_handler = handler;
	c->call_user_data = user_data;
	c->have_call_fields = 0;
	
	_ln_parse_signature(&c->connect_fields, sig_cp);
	if(c->connect_fields.n_inner_fields)
		c->connect_fields.field_size = 1; // we have exactly one request!
	else
		c->connect_fields.field_size = 0;
	
	ln_service_provider_set_handler(call_svc, event_connection_call_handler, c);
	ret = ln_service_provider_register(clnt, call_svc);
	if(ret) {
		_ln_event_connection_destroy(c);
		return ret;
	}

	_ln_iovec_mgr_init(&c->mgr);
	if(have_flat_input) {
		c->own_connect_data = 0;
		c->connect_data = (char*)connect_data_flat;
		c->connect_data_len = connect_data_flat_len;
	} else {
		// now pack connect data
		_ln_iovec_mgr_reset(&c->mgr, NULL);
		char* dp = (char*)connect_data_flat;
		_ln_read_structure(&c->mgr, &c->connect_fields, &dp, NULL);
		// now flatten iovec to flat memory
		c->own_connect_data = 1;
		c->connect_data = (char*)_ln_iovec_mgr_get_flat(&c->mgr);
		c->connect_data_len = c->mgr.n_bytes;
	}

	if(c->is_internal_event) {
		// do internal event connect!
		ln_debug(c->clnt, "its an internal event!");
		
		request_t r;
		int ret = _ln_init_request(
			c->clnt, &r,
			"method", "r", "connect_event",
			"name", "r", c->event_name,
			"signature", "r", c->connect_signature,
			"handler", "r", c->call_svc_name,
			NULL);		
		if(ret) {
			_ln_event_connection_destroy(c);
			return ret;
		}
		_ln_add_to_request_raw(&r, "connect_data", c->connect_data, c->connect_data_len, 0);

		ln_mutex_lock(&c->clnt->communication_mutex);
		ret = _ln_request_and_wait(&r);
		ln_mutex_unlock(&c->clnt->communication_mutex);
		if(ret) {
			_ln_destroy_request(&r);
			ln_debug(clnt, "connect_event request failed: %s\n", clnt->error_message);
			_ln_event_connection_destroy(c);
			return ret;
		}
		 
		_ln_get_from_request(&r, "event_connection_id", "i", &c->event_connection_id);
		_ln_destroy_request(&r);
		c->connected = 1;
		ln_debug(clnt, "event successfully connected with id %d\n", c->event_connection_id);
	} else {
		// todo:
		// user event connect!
		// now register at ln manager resource_change event for this event connect service!
		
		// does event provider already exist? -> connect now!
		// todo: ask manage whether this service exists
		int does_exist = 0;
		if(does_exist) {
			// register!
			
		}
		// if not, wait for resource event and register async!
	}

	_ln_client_append_event_connection(clnt, c);
	*conn = c;
	return 0;
}

int ln_event_connect(	
	ln_client clnt_, ln_event_connection* conn,
	const char* event_name, const char* connect_signature, void* connect_data,
	ln_event_handler handler, void* user_data) {
	return _ln_event_connect(
		(client*)clnt_, conn, event_name, connect_signature, connect_data, 0, handler, user_data, 0);
}

int ln_event_connect_flat(	
	ln_client clnt_, ln_event_connection* conn,
	const char* event_name, const char* connect_signature, void* connect_data_flat, uint32_t connect_data_flat_len,
	ln_event_handler handler, void* user_data) {
	return _ln_event_connect(
		(client*)clnt_, conn, event_name, connect_signature, connect_data_flat, connect_data_flat_len, handler, user_data, 1);
}

int ln_event_connection_deinit(ln_event_connection* conn) {
	event_connection* c = (event_connection*)*conn;
	client* clnt = c->clnt;
	ln_debug(clnt, "event connection %p deinit\n", c);
	c->call_handler = NULL;
	_ln_client_remove_event_connection(c->clnt, c);
	_ln_event_connection_destroy(c);
	*conn = NULL;
	ln_debug(clnt, "event connection %p deinit done\n", c);
	return 0;
}

int _ln_event_connection_destroy(event_connection* c) {
	client* clnt = c->clnt;
	ln_debug(clnt, "%s %p\n", __func__, c);
	if(c->event_name)
		free(c->event_name);
	if(c->connect_signature)
		free(c->connect_signature);
	if(c->call_svc)
		ln_service_deinit(&c->call_svc);
	if(c->call_svc_name)
		free(c->call_svc_name);
	if(c->connect_data && c->own_connect_data)
		free(c->connect_data);
	_ln_service_fields_free(&c->connect_fields);
	if(c->have_call_fields)
		_ln_service_fields_free(&c->call_fields);
	
	free(c->mgr.msg.msg_iov);
	_ln_aux_mem_free(&c->aux_mem);
	
	free(c);
	ln_debug(clnt, "%s %p done\n", __func__, c);
	
	return 0;
}

unsigned int ln_event_connection_get_id(ln_event_connection conn) {
	event_connection* c = (event_connection*)conn;
	return c->event_connection_id;
}
