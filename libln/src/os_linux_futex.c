#include "os.h"

#if defined(HAVE_SYS_FUTEX)

#include <time.h>
#include <unistd.h>
#include <linux/futex.h>
#include <sys/syscall.h>

void memory_barrier() {
#if defined(__x86_64__) || defined(__i386__)	
	asm volatile ("mfence" : : : "memory");
#elif defined(__aarch64__)
	// http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dui0802a/DMB.html
	asm volatile ("dmb ishst" : : : "memory");
#elif defined(__ARM_ARCH) && __ARM_ARCH < 7
	// these should work with armv5, as seen in u-boot:
	asm volatile ("mcr     p15, 0, %0, c7, c10, 5" : : "r" (0));
#elif defined(__ARM_ARCH)
	// assume arm7 or newer
	asm volatile ("dmb ishst" : : : "memory");
#else
#  error no mfence / DMB alternative known for this arch!
#endif
}

long futex_wake(uint32_t* uaddr, int n_threads) {
	return syscall(SYS_futex, uaddr, FUTEX_WAKE, n_threads);
}

long futex_wait(uint32_t* uaddr, uint32_t expected) {
	return syscall(SYS_futex, uaddr, FUTEX_WAIT, expected, NULL);
}

long futex_timedwait(uint32_t* uaddr, uint32_t expected, double timeout) {
	struct timespec ts = { (time_t)timeout, 0 };
	timeout -= ts.tv_sec;
	ts.tv_nsec = timeout * 1e9;
	return (int)syscall(SYS_futex, uaddr, FUTEX_WAIT, expected, &ts);
}

long futex_wake_all(uint32_t* uaddr) {
	return futex_wake(uaddr, 0x7fffffff); // returns n_woken
}

#endif
