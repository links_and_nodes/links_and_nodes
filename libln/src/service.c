/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "ln/ln.h"
#include "service.h"
#include "request.h"
#include "repr.h"

#define check_null_str(a) ((a) ? (a) : "NULL")

static struct _ln_service_field* _get_new_field(struct _ln_service_field* packet) {
	if (packet->n_inner_fields == packet->max_inner_fields) {
		if(!packet->max_inner_fields)
			packet->max_inner_fields = 4;
		else
			packet->max_inner_fields *= 2;
		struct _ln_service_field* new_mem = (struct _ln_service_field*)realloc(packet->inner_fields, sizeof(struct _ln_service_field) * packet->max_inner_fields);
		if(!new_mem)
			return NULL;
		packet->inner_fields = new_mem;
	}
	return &packet->inner_fields[packet->n_inner_fields++];
}

void _print_fields(client* clnt, struct _ln_service_field* packet, int depth) {
	if(clnt->debug == 0)
		return;

	char indent[128];
	memset(indent, ' ', 128);
	indent[depth * 4] = 0;

	ln_debug(clnt, "%spacket field_size %d, ptr_len_size %d ", indent, packet->field_size, packet->ptr_len_size);
	if(!packet->n_inner_fields) { // its a primitive
		ln_debug(clnt, "-> is_primitive: %d\n", packet->is_primitive);
		return; 
	}
	ln_debug(clnt, "-> is_primitive: %d, %d childs:\n", packet->is_primitive, packet->n_inner_fields);
	for(unsigned int i = 0; i < packet->n_inner_fields; i++)
		_print_fields(clnt, &packet->inner_fields[i], depth + 1);
	
}

unsigned int _get_host_side_mem_needs(struct _ln_service_field* field) {
	unsigned int field_size = 0;
	for(unsigned int i = 0; i < field->n_inner_fields; i++) {
		if(field->inner_fields[i].ptr_len_size)
			field_size += sizeof(void*); // pointer
		else if(!field->inner_fields[i].n_inner_fields)
			field_size += field->inner_fields[i].field_size; // primitive
		else
			field_size += field->inner_fields[i].field_size * _get_host_side_mem_needs(field->inner_fields + i); // non-primitive!
	}
	return field_size;
}


char* _ln_parse_signature(struct _ln_service_field* packet, char* signature) {
	packet->n_inner_fields = 0;
	packet->max_inner_fields = 0;
	
	// until ] or \0 or |
	
	char* cp = signature;
  
	// now fill in byte sizes!
	// struct iovec* iop = s->msg.msg_iov;
	// iop->iov_len = sizeof(uint64_t);
	// iop++;
	// struct _ln_service_field* field = s->fields;

	while(*cp && *cp != ']' && *cp != '|') {
		struct _ln_service_field* field = _get_new_field(packet);
		field->inner_fields = 0;
		field->max_inner_fields = 0;
		field->n_inner_fields = 0;
		
		if(*cp == '[') {
			// nested non primitive field!
			field->is_primitive = 0;
			// instead of a name we read a field structure
			cp = _ln_parse_signature(field, cp + 1);
			// *cp is ' ' or '*'
			if(*cp == '*')
				cp++;
			// *cp is ' '
		} else {
			// primitive!
			// field name
			while(*cp && *cp != ' ')
				cp++;
			if(!*cp) // not a valid field name
				break;
			field->is_primitive = 1;
			field->n_inner_fields = 0;
		}
		int is_pointer = *(cp - 1) == '*';
		char* field_size = cp + 1;
		cp = field_size;
		while(*cp && *cp != ' ')
			cp++;
		if(!*field_size) // invalid field size!
			break;
		char* field_count = cp + 1;
		cp = field_count;
		while(*cp && *cp != ',' && *cp != '|' && *cp != ']')
			cp++;
		if(!*field_count) // invalid field count!
			break;

		
		/*
		  2 faelle:
		  
		  [........]  x 1024    <- fixed array of non primitive!
		  [........]* x 1       <- pointer of non primitive!
		  
		*/

		if(field->is_primitive) {
			// assumes that field_size is 1 for non-primitives!
			field->field_size = atoi(field_size) * atoi(field_count);
		} else { // non primitive
			if(is_pointer) {
				// dynamic -- pointer! -> calc host side mem needs!
				field->field_size = _get_host_side_mem_needs(field); // if this returns 0 then we have a self-recursion! (field->is_primitive == 0 && field->n_inner_fields == 0)
			} else
				field->field_size = atoi(field_count); // fixed size!
		}

		if(is_pointer) {
			// fix pointer entry from len field before!
			struct _ln_service_field* ptr_len = (field - 1);
			field->ptr_len_size = ptr_len->field_size;
		} else {
			field->ptr_len_size = 0;
		}
		
		if(!*cp)
			break; // done
		if(*cp == '|' || *cp == ']')
			break;
		cp++; // ,
	}
	if(*cp == '|' || *cp == ']')
		cp++; // | or ] -> , or \0
	return cp;
}


#define get_n_elements(n_elems, field, data) {				\
		if((field).ptr_len_size == 1)						\
			n_elems = *(uint8_t*)((data) - 1);				\
		else if((field).ptr_len_size == 2)					\
			n_elems = *(uint16_t*)((data) - 2);				\
		else if((field).ptr_len_size == 4)					\
			n_elems = *(uint32_t*)((data) - 4);				\
	}

void print_hex(char* what, char* data, unsigned int len) {
	printf("%s", what);
	for(unsigned int i = 0; i < len; i++)
		printf("%02x ", (unsigned char)data[i]);
	printf("\n");
}

int _ln_fill_structure(struct _ln_aux_mem_holder* s, struct _ln_service_field* packet, char** host_data, char** buffer, unsigned int* left_in_buffer, unsigned int needs_endianess_swap) {
	unsigned int count = 1; // to avoid CSA's garbage claim
	if(packet->is_primitive) {
		// primitive
		if(packet->ptr_len_size) {
			// pointer!
			get_n_elements(count, *packet, *host_data); // as the len field is already decoded!
			unsigned int mem_needed = packet->field_size * count;
			if(mem_needed > *left_in_buffer)
				return -1;
			char* host_mem = _ln_aux_mem_get(s, mem_needed);
			*(void**)*host_data = host_mem;
			*host_data += sizeof(void*);

			if(needs_endianess_swap)
				_ln_memcpy_swap(host_mem, *buffer, mem_needed, packet->field_size);
			else
				memcpy(host_mem, *buffer, mem_needed);
			//print_hex("f-pointer  : ", host_mem, mem_needed);
			*buffer += mem_needed;
			*left_in_buffer -= mem_needed;
		} else {
			count = packet->field_size;
			if(count > *left_in_buffer)
				return -2;
			//print_hex("f-primitive: ", *buffer, count);
			if(needs_endianess_swap)
				_ln_memcpy_swap(*host_data, *buffer, count, count);
			else
				memcpy(*host_data, *buffer, count);
			*host_data += count;
			*buffer += count;
			*left_in_buffer -= count;
		}
		return 0;
	}
	// non primitive!
	if(!packet->ptr_len_size) {
		// fixed size
		count = packet->field_size;
	} else {
		// pointer!
		get_n_elements(count, *packet, *host_data);

		// host side is a pointer ->
		// do malloc!!!
		// how big?
		// -> elements size times fieldcount!
		
		// calc host side mem needs -> stored in field_size!
		char* host_mem = _ln_aux_mem_get(s, packet->field_size * count);
		*(void**)*host_data = host_mem;
		*host_data += sizeof(void*);
		for(unsigned int i = 0; i < count; i++) {
			for(unsigned int k = 0; k < packet->n_inner_fields; k++) {
				int ret = _ln_fill_structure(s, &packet->inner_fields[k], &host_mem, buffer, left_in_buffer, needs_endianess_swap);
				if(ret)
					return ret;
			}
		}
		return 0;
	}
	// fixed size!
	for(unsigned int i = 0; i < count; i++) {
		for(unsigned int k = 0; k < packet->n_inner_fields; k++) {
			int ret = _ln_fill_structure(s, &packet->inner_fields[k], host_data, buffer, left_in_buffer, needs_endianess_swap);
			if(ret)
				return ret;
		}
	}
	return 0;
}

void _ln_read_structure(_ln_iovec_mgr* mgr, struct _ln_service_field* packet, char** host_data, struct _ln_service_field* parent_packet) {
	unsigned int count = 1; // again, to avoid CSA's garbage claim
	if(packet->is_primitive) {
		// primitive
		if(packet->ptr_len_size) {
			// pointer!
			get_n_elements(count, (*packet), *host_data);
			_ln_iovec_mgr_add(mgr, *(void**)*host_data, packet->field_size * count, packet->field_size);
			*host_data += sizeof(void*);
			//print_hex("pointer  : ", (char*)*(void**)*host_data, packet->field_size * count);
			return;
		}
		_ln_iovec_mgr_add(mgr, *host_data, packet->field_size, packet->field_size);
		//print_hex("primitive: ", *host_data, packet->field_size);
		*host_data += packet->field_size;
		return;
	}
	// non primitive!
	if(!packet->ptr_len_size) {
		// fixed size
		count = packet->field_size;
		for(unsigned int i = 0; i < count; i++)
			for(unsigned int k = 0; k < packet->n_inner_fields; k++)
				_ln_read_structure(mgr, &packet->inner_fields[k], host_data, packet);
		return;
	}
	// pointer!
	get_n_elements(count, *packet, *host_data);	
	// host side is a pointer ->
	// do malloc!!!
	// how big?
	// -> elements size times fieldcount!
	
	// calc host side mem needs -> stored in field_size!
	char* host_mem = *(char**)*host_data;
	*host_data += sizeof(void*);

	struct _ln_service_field* fields_packet = packet;
	if(fields_packet->n_inner_fields == 0) // self recursion
		fields_packet = parent_packet;
	for(unsigned int i = 0; i < count; i++)
		for(unsigned int k = 0; k < fields_packet->n_inner_fields; k++)
			_ln_read_structure(mgr, &fields_packet->inner_fields[k], &host_mem, fields_packet);
}

// user:
int ln_service_init(ln_client clnt_, ln_service* svc, const char* service_name, const char* service_interface, const char* signature) {
	client* clnt = (client*)clnt_;

	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	service* s = (service*)calloc(1, sizeof(service));
	int ret;
	if((ret = ln_mutex_create(&s->mutex))) {
		errno = ret;
		free(s);
		return -LNE_CHECK_ERRNO;
	}
	s->clnt = clnt;
	char* service_name_mapped = _get_service_mapping(clnt, service_name, "service");

	s->name = service_name_mapped; // take ownership
	s->interface_name = strdup(service_interface);
	s->signature = strdup(signature);

	s->is_provider = 0;
	s->fd = -1;

	s->response_buffer = NULL;
	s->response_buffer_len = 0;
	
	s->is_listening = 0;
	s->handler = NULL;

	_ln_aux_mem_init(&s->aux_mem);

	s->service_port = -1;
	
	s->unix_socket_name = NULL;
	s->unix_fd = -1;
	s->notified_tcp_fd = false;
	s->notified_unix_fd = false;

	s->async_req_running = 0;
	s->async_request_notification_pipe.input_fd = ln_pipe_invalid;
	s->new_provider_fd_notification_pipe.input_fd = ln_pipe_invalid;

	s->handler = NULL;
	s->fd_handler = NULL;

	// now parse request signature
	char* cp = _ln_parse_signature(&s->req, s->signature);
	if(s->req.n_inner_fields)
		s->req.field_size = 1; // we have exactly one request!
	else
		s->req.field_size = 0;
	// now parse response signature
	_ln_parse_signature(&s->resp, cp);
	s->resp.field_size = 1; // we have exactly one response!

	ln_debug(clnt, "%s\nrequest fields:\n", s->name);
	_print_fields(clnt, &s->req, 0);
	ln_debug(clnt, "response fields:\n");
	_print_fields(clnt, &s->resp, 0);

	// calculate host side request structure length
	s->request_len = _get_host_side_mem_needs(&s->req);
	s->hostside_response_len = _get_host_side_mem_needs(&s->resp);
	ln_debug(clnt, "host-side request_len: %d, response_len: %d\n", s->request_len, s->hostside_response_len);

	_ln_iovec_mgr_init(&s->mgr);
	
	lock_client(clnt);
	_ln_client_append_service(clnt, s);
	unlock_client(clnt);
	
	*svc = s;
	
	return 0;
}

int ln_service_deinit(ln_service* svc) {
	service* s = (service*)*svc;
	if(s->is_provider && s->clnt->sfd != -1) {
		request_t r;
		int ret = _ln_init_request(
			s->clnt, &r,
			"method", "r", "unregister_service",
			"name", "r", s->name,
			NULL);
		if(!ret) {
			ln_mutex_lock(&s->clnt->communication_mutex);
			_ln_request_and_wait(&r);
			ln_mutex_unlock(&s->clnt->communication_mutex);
			_ln_destroy_request(&r);
		}
	}
	lock_client(s->clnt);
	_ln_client_remove_service(s->clnt, s);
	unlock_client(s->clnt);
	_ln_service_destroy(s);

	*svc = NULL;
	return 0;
}

void _ln_service_fields_free(struct _ln_service_field* packet) {
	if(!packet->n_inner_fields) // simple primitive, no fields
		return;
	for(unsigned int i = 0; i < packet->n_inner_fields; i++)
		_ln_service_fields_free(&packet->inner_fields[i]);
	free(packet->inner_fields);
}

int _ln_service_destroy(service* s) {
	ln_mutex_lock(&s->mutex);
	_ln_service_group_remove(s);
	ln_mutex_unlock(&s->mutex);
	
	// is only list of pointers: DESTRUCT_LIST(s, log_service); only destroy pointers
	free(s->log_services);
	
	DESTRUCT_LIST(s, service_client);
	DESTRUCT_LIST_TYPE_NAME(s, service_client, fake_service_clients);

	if(s->service_name_patterns_to_log)
		free(s->service_name_patterns_to_log);
	
	if(s->fd != -1)
		_ln_close_socket(s->fd);

	if(s->unix_fd != -1)
		_ln_close_socket(s->unix_fd);
	
	if(s->unix_socket_name)
		free(s->unix_socket_name);

	if(s->peer_address)
		free(s->peer_address);

	if(s->async_request_notification_pipe.input_fd != ln_pipe_invalid)
		ln_pipe_close(&s->async_request_notification_pipe);

	if(s->new_provider_fd_notification_pipe.input_fd != ln_pipe_invalid)
		ln_pipe_close(&s->new_provider_fd_notification_pipe);

	_ln_aux_mem_free(&s->aux_mem);

	free(s->name);
	free(s->interface_name);
	free(s->signature);
	free(s->mgr.msg.msg_iov);

	_ln_service_fields_free(&s->req);
	_ln_service_fields_free(&s->resp);

	if(s->response_buffer)
		free(s->response_buffer);

	ln_mutex_destroy(&s->mutex);
	free(s);

	return 0;
}

int not_connreset() {
#ifndef __WIN32__		
	return errno != ECONNRESET;
#else
	int last_error = WSAGetLastError();
	return !(last_error == WSAECONNABORTED || last_error == WSAECONNRESET || last_error == WSAETIMEDOUT);
#endif
}

int _ln_service_send_request(service* s, int* did_connect) {
	ln_debug(s->clnt, "%s\n", __func__);

	unsigned int n_messages_already_sent = 0;
	unsigned int n_messages_to_send = 1;
	struct msghdr* next_message = &s->mgr.msg;
	unsigned int* next_message_to_send = &s->mgr.n_bytes;

	struct msghdr* all_messages = NULL;
	unsigned int* all_message_to_sends = NULL;

#ifdef __LINUX__
	unsigned int iov_max = sysconf(_SC_IOV_MAX);
#else
	unsigned int iov_max = IOV_MAX;
#endif
	if((unsigned)s->mgr.msg.msg_iovlen > iov_max) {
		ln_debug(s->clnt, "%s - message has %d iov's - system max is %d -> will split!\n", __func__, s->mgr.msg.msg_iovlen, iov_max);

		n_messages_to_send = s->mgr.msg.msg_iovlen / iov_max + 1;
		all_messages = (struct msghdr*)malloc(sizeof(struct msghdr) * n_messages_to_send);
		memset(all_messages, 0, sizeof(struct msghdr) * n_messages_to_send);
		all_message_to_sends = (unsigned int*)malloc(sizeof(unsigned int) * n_messages_to_send);
		memset(all_message_to_sends, 0, sizeof(unsigned int) * n_messages_to_send);

		// fill messages
		unsigned int iovs_left = s->mgr.msg.msg_iovlen;
		unsigned int iovs_done = 0;

		next_message = all_messages;
		next_message_to_send = all_message_to_sends;		

		n_messages_to_send = 0;
		while(iovs_left > 0) {
			unsigned int to_copy = iov_max;
			if(to_copy > iovs_left)
				to_copy = iovs_left;
			
			next_message->msg_iov = &s->mgr.msg.msg_iov[iovs_done];
			next_message->msg_iovlen = to_copy;
			for(unsigned int i = 0; i < (unsigned)next_message->msg_iovlen; i++)
				*next_message_to_send += next_message->msg_iov[i].iov_len;
			n_messages_to_send ++;
			
			ln_debug(s->clnt, "next_message %p, %d iov's, *next_message_to_send: %d\n", next_message, next_message->msg_iovlen, *next_message_to_send);
		
			
			iovs_left -= to_copy;
			iovs_done += to_copy;

			next_message_to_send++;
			next_message++;
		}
		
		// set to first one
		next_message = all_messages;
		next_message_to_send = all_message_to_sends;		
	}
	
	ssize_t n;
	while(true) {
		/*
		  printf("sending request:\n");
		  for(unsigned int i = 0; i < next_message->msg_iovlen; i++) {
		  print_hex("send: ", (char*)next_message->msg_iov[i].iov_base, next_message->msg_iov[i].iov_len);
		  }
		*/

		unsigned int already_sent = 0;
		unsigned int to_send = *next_message_to_send;
		while(to_send) {
			// ln_debug(s->clnt, "sendmsg(%d, msghdr: %p, iov: %p, iov_len: %d)\n", s->fd, next_message, next_message->msg_iov, next_message->msg_iovlen);
			n = sendmsg(s->fd, next_message, 0);
			if(n == -1) {
				if(errno == EINTR || errno == EAGAIN)
					continue;
				break;
			}
			if((unsigned)n == to_send) {
				// finished this message, is there another?
				n_messages_already_sent += 1;
				ln_debug(s->clnt, "finished %d messages\n", n_messages_already_sent);
				if(n_messages_already_sent == n_messages_to_send) {
					if(all_messages) {
						free(all_messages);
						free(all_message_to_sends);
					}
					return 0; // finished
				}
				// select next message!
				next_message++;
				next_message_to_send++;

				// ln_debug(s->clnt, "next message hdr is: %p with %d bytes\n", next_message, *next_message_to_send);
				already_sent = 0;
				to_send = *next_message_to_send;
				continue;
			}
			// not yet finished sending...
			already_sent += n;
			// adjust iovec msg to remove those already sent bytes
			to_send -= n;
			// remove n bytes!
			for(unsigned int i = 0; i < (unsigned)next_message->msg_iovlen; i++) {
				if(next_message->msg_iov[i].iov_len <= (unsigned)n) {
					// this iov is complete!
					n -= next_message->msg_iov[i].iov_len;
					next_message->msg_iov[i].iov_len = 0;
					if(n == 0)
						break;
				} else {
					// this iov is only partly completed!
					next_message->msg_iov[i].iov_base = (iov_base_t)((char*)next_message->msg_iov[i].iov_base + n);
					next_message->msg_iov[i].iov_len -= n;
					break;
				}
			}
		}
		// error...
		if(already_sent > 0 || n_messages_already_sent > 0 || not_connreset() || *did_connect) {
			if(all_messages) {
				free(all_messages);
				free(all_message_to_sends);
			}		
			return -LNE_CHECK_ERRNO;
		}
		
		// try once to automatically reconnect per ln_service_call! (but only if we did not already got some bytes thru)
		_ln_close_socket(s->fd);
		s->fd = -1;
		int ret = _ln_service_connect(s, did_connect);
		if(ret) {
			if(all_messages) {
				free(all_messages);
				free(all_message_to_sends);
			}					
			return ret;
		}
	}
	return 0;
}

int _ln_service_recv_response(service* s) {
	ln_debug(s->clnt, "%s\n", __func__);

	uint32_t req_size;
	ssize_t n = full_recv(s->clnt, s->fd, &req_size, sizeof(req_size));

	if(s->is_aborted)
		return -LNE_SVC_REQ_ABORTED;
	
	int not_connreset = 0;
#ifndef __WIN32__		
	not_connreset = errno != ECONNRESET;
#else
	int last_error = WSAGetLastError();
	not_connreset = !(last_error == WSAECONNABORTED || last_error == WSAECONNRESET || last_error == WSAETIMEDOUT);
#endif

	if(n == 0 || (n == -1 && !not_connreset)) {
		// service provider closed connection // disappeared!
		ln_debug(s->clnt, "service provider close connection // disappeared -- retry!\n");
		_ln_close_socket(s->fd);
		s->fd = -1;
		// retry!!
		return -1;
	}
	if(n < 0) { // error
		_ln_set_error(s->clnt, "error in ln_service_call('%s') wait for response size!", s->name);
		return n;
	}
	if(s->needs_endianess_swap) {
		_ln_swap(sizeof(req_size), &s->response_len, &req_size);
		s->response_len -= sizeof(req_size);
	} else
		s->response_len = req_size - sizeof(req_size);
	
	ln_debug(s->clnt, "want to received response data of length %d\n", s->response_len);

	if(s->response_len > s->response_buffer_len) {
		char* new_buffer = (char*)realloc(s->response_buffer, s->response_len);
		if(!new_buffer)
			return -LNE_SVC_RECV_RESP_MEM;
		s->response_buffer = new_buffer;
		s->response_buffer_len = s->response_len;
	}
	n = full_recv(s->clnt, s->fd, s->response_buffer, s->response_len);

	if(s->is_aborted)
		return -LNE_SVC_REQ_ABORTED;

#ifndef __WIN32__		
	not_connreset = errno != ECONNRESET;
#else
	last_error = WSAGetLastError();
	not_connreset = !(last_error == WSAECONNABORTED || last_error == WSAECONNRESET || last_error == WSAETIMEDOUT);
#endif
	if(n == 0 || (n == -1 && !not_connreset)) {
		// service provider closed connection // disappeared!
		ln_debug(s->clnt, "service provider close connection // disappeared2 -- retry!\n");
		_ln_close_socket(s->fd);
		s->fd = -1;
		// retry!!
		return -1;
	}
	if(n < 0) { // error		
		_ln_set_error(s->clnt, "error in ln_service_call('%s') wait for response body of size %lld!", s->name, s->response_len);
		return n;
	}
	/*
	  printf("received %d bytes: ", s->response_len);
	  for(unsigned int i = 0; i < s->response_len; i++)
	  printf("%02x ", ((unsigned char*)s->response_buffer)[i]);
	  printf("\n");
	*/
	return 0;
}

int _ln_service_call_only(service* s, int* did_connect) {
	if(s->is_provider)
		return -LNE_SVC_IS_PROVIDER;

	double completion_time;
	if(s->n_log_services)
		completion_time = ln_get_time();
	
	int ret = _ln_service_send_request(s, did_connect);
	if(ret)
		return ret;
	
	if(s->n_log_services &&
	   _ln_log_service_event(s, LOG_SERVICE_REQUEST, completion_time, 0,
				 s->peer_address, s->clnt->client_id, s->request_id, &s->mgr))
		ln_debug(s->clnt, "failed to log service request!\n");	
		
	// wait for response!
	ret = _ln_service_recv_response(s);
	
	if(s->is_aborted)
		return -LNE_SVC_REQ_ABORTED;
			
	if(ret) {
		ret = _ln_service_connect(s, did_connect);
		if(ret)
			return ret;			
		return _ln_service_call_only(s, did_connect);
	}
	
	int32_t response_error;
	if(s->needs_endianess_swap) {
		_ln_swap(sizeof(response_error), &response_error, s->response_buffer);
	} else
		response_error = *(int32_t*)s->response_buffer;
	
	if(response_error != 0)
		return response_error;

	return 0;
}

int ln_service_call_abort(ln_service svc) {
	service* s = (service*)svc;
	if(s->is_provider)
		return -LNE_SVC_IS_PROVIDER;
	// set service_client_ request_error to LNE_SVC_REQ_ABORTED
	s->is_aborted = 1;

	if(s->fd != -1) {
		ln_debug(s->clnt, "ln_service_call_abort; close connection!\n");
		_ln_close_socket(s->fd);
		s->fd = -1;
	}
	
	return 0;
}

void _ln_service_get_new_request_id(service* s) {
	lock_client(s->clnt);
	s->request_id = s->clnt->next_request_id;
	s->clnt->next_request_id ++;
	unlock_client(s->clnt);
}

void _ln_service_add_request_header(service* s) {
	if(!s->send_request_id)
		return;
	_ln_iovec_mgr_add(&s->mgr, &s->clnt->version, sizeof(s->clnt->version), sizeof(s->clnt->version));
	_ln_iovec_mgr_add(&s->mgr, &s->clnt->client_id, sizeof(s->clnt->client_id), sizeof(s->clnt->client_id));
	_ln_iovec_mgr_add(&s->mgr, &s->request_id, sizeof(s->request_id), sizeof(s->request_id));

}

int ln_service_callv_with_timeout(ln_service svc, struct iovec* iov, unsigned int iov_len, uint8_t* iov_element_lens, char** response_buffer, uint32_t* response_len, uint8_t* need_swap, double timeout)
{
	if(timeout == 0)
		return ln_service_callv(svc, iov, iov_len, iov_element_lens, response_buffer, response_len, need_swap);

	double deadline = ln_get_time() + timeout;

	service* s = (service*)svc;
	ln_service_request req;
	int ret = ln_service_callv_async(svc, iov, iov_len, iov_element_lens, &req);
	if(ret) {
		ln_debug(s->clnt, "callv_service_with_timeout: ln_service_callv_async error %d\n", ret);
		return ret;
	}

	int fd;
	ret = ln_service_request_get_finished_notification_fd(req, &fd);
	if(ret) {
		ln_debug(s->clnt, "call_service_with_timeout: could not get notification fd: %d\n", ret);
		ln_service_request_abort(req);
		return ret;
	}

	int call_retval;
	while(true) {
		ret = ln_service_request_finished(req, &call_retval);
		if(ret == 1) // finished
			break;
		// blocking wait
		double remaining = deadline - ln_get_time();
		if(remaining > 0) {
			fd_set read_fds;
			FD_ZERO(&read_fds);
			FD_SET(fd, &read_fds);
			struct timeval tv = {(unsigned int)remaining, 0};
			tv.tv_usec = (remaining - tv.tv_sec) * 1e6;
			ret = select(fd + 1, &read_fds, NULL, NULL, &tv);
			if(ret != 0)
				continue; // have finish notification, or select error
			// ret == 0, select timeout!
		}
		// timed out
		ln_debug(s->clnt, "call_service_with_timeout: timed-out after %.3fs", -(deadline - ln_get_time()) + timeout);
		ln_service_request_abort(req);
		return -LNE_SVC_TIMEOUT;
	}

	// finished
	ln_service_callv_async_get_response(svc, response_buffer, response_len);

	return call_retval;
}

int ln_service_callv(ln_service svc, struct iovec* iov, unsigned int iov_len, uint8_t* iov_element_lens, char** response_buffer, uint32_t* response_len, uint8_t* need_swap)
{
	service* s = (service*)svc;
	ln_mutex_lock(&s->mutex);
	s->is_aborted = 0;

	_ln_service_get_new_request_id(s);
	ln_debug(s->clnt, "ln_service_callv sending request id %d (%s)\n", s->request_id, s->name);

	// construct iovec to send!
	// do complete resize at once if neede
	if((iov_len + 1) > s->mgr.msg_max_len) {
		struct iovec* new_vec = (struct iovec*)realloc(s->mgr.msg.msg_iov, sizeof(struct iovec) * (iov_len + 1));
		if(!new_vec) {
			ln_mutex_unlock(&s->mutex);
			return -LNE_NO_MEM;
		}
		s->mgr.msg.msg_iov = new_vec;
		s->mgr.msg_max_len = (iov_len + 1);
	}

	// try to connect to provider to get endianess information!
	int did_connect = 0;
	int ret = _ln_service_connect(s, &did_connect);
	if(ret) {
		ln_mutex_unlock(&s->mutex);
		return ret;
	}
	
	struct _ln_aux_mem_holder* swap_aux_mem = NULL;
	if(s->needs_endianess_swap) {
		// need to byteswap!
		swap_aux_mem = &s->aux_mem;
		_ln_aux_mem_clear(swap_aux_mem);
		ln_debug(s->clnt, "call_service: will need to swap endianess of request!\n");
		if(need_swap)
			*need_swap = 1;
	} else {
		if(need_swap)
			*need_swap = 0;
		// ln_debug(s->clnt, "call_service: will NOT need to swap endianess of request!\n");
	}
	
	_ln_iovec_mgr_reset(&s->mgr, swap_aux_mem);
	_ln_iovec_mgr_add(&s->mgr, &s->mgr.n_bytes, sizeof(uint32_t), sizeof(uint32_t)); // first data is always compelte request length
	_ln_service_add_request_header(s);
	for(unsigned int i = 0; i < iov_len; i++)
		_ln_iovec_mgr_add(&s->mgr, iov[i].iov_base, iov[i].iov_len, iov_element_lens[i]);
	
	ln_debug(s->clnt, "ln_service_callv req iovec:\n");
	_ln_iovec_mgr_print(s->clnt, &s->mgr);
	
	if(s->needs_endianess_swap) // swap length field!
		_ln_swap(sizeof(uint32_t), s->mgr.msg.msg_iov[0].iov_base, &s->mgr.n_bytes);
	
	ret = _ln_service_call_only(s, &did_connect);
	if(ret) {
		ln_mutex_unlock(&s->mutex);
		return ret;
	}

	// now fillin response!
	*response_buffer = s->response_buffer + sizeof(int32_t);
	*response_len = s->response_len - sizeof(int32_t);
	ln_mutex_unlock(&s->mutex);
	return 0;
}

int ln_service_call_with_timeout(ln_service svc, void* data, double timeout)
{
	if(timeout == 0)
		return ln_service_call(svc, data); // normal blocking call without timeout

	double deadline = ln_get_time() + timeout;

	service* s = (service*)svc;
	ln_service_request req;
	int ret = ln_service_call_async(svc, data, &req);
	if(ret) {
		ln_debug(s->clnt, "call_service_with_timeout: ln_service_call_async error %d\n", ret);
		return ret;
	}

	int fd;
	ret = ln_service_request_get_finished_notification_fd(req, &fd);
	if(ret) {
		ln_debug(s->clnt, "call_service_with_timeout: could not get notification fd: %d\n", ret);
		ln_service_request_abort(req);
		return ret;
	}

	int call_retval;
	while(true) {
		ret = ln_service_request_finished(req, &call_retval);
		if(ret == 1) // finished
			break;
		// blocking wait
		double remaining = deadline - ln_get_time();
		if(remaining > 0) {
			fd_set read_fds;
			FD_ZERO(&read_fds);
			FD_SET(fd, &read_fds);
			struct timeval tv = {(unsigned int)remaining, 0};
			tv.tv_usec = (remaining - tv.tv_sec) * 1e6;
			ret = select(fd + 1, &read_fds, NULL, NULL, &tv);
			if(ret != 0)
				continue; // have finish notification, or select error
			// ret == 0, select timeout!
		}
		// timed out
		ln_debug(s->clnt, "call_service_with_timeout: timed-out after %.3fs", -(deadline - ln_get_time()) + timeout);
		ln_service_request_abort(req);
		return -LNE_SVC_TIMEOUT;
	}

	// finished
	return call_retval;
}

int ln_service_call(ln_service svc, void* data)
{
	service* s = (service*)svc;
	ln_mutex_lock(&s->mutex);
	s->is_aborted = 0;
	_ln_service_get_new_request_id(s);
	ln_debug(s->clnt, "ln_service_call with user data at %p, thread %#x, request id %d\n",
		 data, get_thread_id(), s->request_id);
	
	// try to connect to provider to get endianess information!
	int did_connect = 0;
	int ret = _ln_service_connect(s, &did_connect);
	if(ret) {
		ln_mutex_unlock(&s->mutex);
		return ret;
	}
	
	struct _ln_aux_mem_holder* swap_aux_mem = NULL;
	if(s->needs_endianess_swap) {
		// need to byteswap!
		swap_aux_mem = &s->aux_mem;
		_ln_aux_mem_clear(swap_aux_mem);
		ln_debug(s->clnt, "call_service: will need to swap endianess of request!\n");
	}

	// struct/buffer into iovec!
	_ln_iovec_mgr_reset(&s->mgr, swap_aux_mem);
	_ln_iovec_mgr_add(&s->mgr, &s->mgr.n_bytes, sizeof(uint32_t), sizeof(uint32_t));
	_ln_service_add_request_header(s);
	char* dp = (char*)data; 
	ln_debug(s->clnt, "ln_service_call reading user request structure\n");
	_ln_read_structure(&s->mgr, &s->req, &dp, NULL);

	if(s->needs_endianess_swap) // swap length field!
		_ln_swap(sizeof(uint32_t), s->mgr.msg.msg_iov[0].iov_base, &s->mgr.n_bytes);
	
	ln_debug(s->clnt, "ln_service_call req iovec:\n");
	_ln_iovec_mgr_print(s->clnt, &s->mgr);

	ret = _ln_service_call_only(s, &did_connect);
	if(ret) {
		ln_mutex_unlock(&s->mutex);
		return ret;
	}

	// buffer into host-side struct
	// fill host side structure from received buffer!
	char* host_data_ptr = (char*)data;
	host_data_ptr += s->request_len;
	// char* hdpb = host_data_ptr;
	char* buffer_ptr = s->response_buffer + sizeof(uint32_t); // ... without return value
	unsigned int left_in_buffer = s->response_len - sizeof(uint32_t);
	_ln_aux_mem_clear(&s->aux_mem);
	/*
	  ln_debug(s->clnt, "ln_service_call filling user response structure. request_len: %d\n", s->request_len);

	  char dbg[1024];
	  char* cp = dbg;
	  *cp = 0;
	  cp += sprintf(cp, "resp buffer: ");
	  for(unsigned int i = 0; i < (s->response_len - sizeof(int32_t)); i++)
	  cp += sprintf(cp, "%02x ", (uint8_t)buffer_ptr[i]);
	  ln_debug(s->clnt, "%s\n", dbg);
	*/
	
	ret = _ln_fill_structure(&s->aux_mem, &s->resp, &host_data_ptr, &buffer_ptr, &left_in_buffer, s->needs_endianess_swap);

	/*
	  cp = dbg;
	  *cp = 0;
	  cp += sprintf(cp, "host buffer: ");
	  for(unsigned int i = 0; i < (s->response_len - sizeof(int32_t)); i++)
	  cp += sprintf(cp, "%02x ", (uint8_t)hdpb[i]);
	  ln_debug(s->clnt, "%s\n", dbg);
	*/

	ln_mutex_unlock(&s->mutex);

	if(ret)
		return LNE_SVC_RECV_RESP;

	if(left_in_buffer > 0) {
		_ln_set_error(s->clnt, "%s: error in received service response: received %d unexpected additional bytes!", s->name, left_in_buffer);
		return -LNE_SVC_RECV_RESP;
	}

	return 0;
}

int ln_service_call_async(ln_service svc, void* data, ln_service_request* req) {
	service* s = (service*)svc;

	if(s->is_provider)
		return -LNE_SVC_IS_PROVIDER;

	if(s->async_req_running) 
		return -LNE_SVC_REQ_RUNNING;

	*req = s;
	s->async_user_data = data;
	s->async_req_is_v = 0;
	s->async_req_finished = 0; // not yet finished
	s->async_request_notification_pipe_was_signaled = 0;

	s->async_req_running = 1;
	s->async_did_connect = 0;
	s->async_did_read_user_data = 0;
	s->async_did_send = 0;

	if(s->n_log_services)
		s->async_completion_time = ln_get_time();

	_ln_service_get_new_request_id(s);
	ln_debug(s->clnt, "ln_service_call_async send request id %d\n", s->request_id);
	add_async_service_call(s->clnt, s);

	return 0;
}

int ln_service_callv_async(ln_service svc, struct iovec* iov, unsigned int iov_len, uint8_t* iov_element_lens, ln_service_request* req) {
	service* s = (service*)svc;

	if(s->is_provider)
		return -LNE_SVC_IS_PROVIDER;

	if(s->async_req_running) 
		return -LNE_SVC_REQ_RUNNING;

	ln_mutex_lock(&s->mutex);
	*req = s;
	s->async_user_data = NULL;
	s->async_req_is_v = 1;
	s->async_req_finished = 0; // not yet finished
	s->async_request_notification_pipe_was_signaled = 0;

	s->async_req_running = 1;
	s->async_did_connect = 0;
	s->async_did_read_user_data = 0;
	s->async_did_send = 0;

	// v
	s->async_user_data_iov = iov;
	s->async_user_data_iov_len = iov_len;
	s->async_user_data_iov_element_lens = iov_element_lens;

	_ln_service_get_new_request_id(s);
	ln_debug(s->clnt, "ln_service_callv_async send request id %d\n", s->request_id);
	add_async_service_call(s->clnt, s);
	ln_mutex_unlock(&s->mutex);
	return 0;
}

int ln_service_callv_async_get_response(ln_service svc, char** response_buffer, uint32_t* response_len) {
	service* s = (service*)svc;

	// now fillin response!
	// skip error field
	*response_buffer = s->response_buffer + sizeof(int32_t);
	*response_len = s->response_len - sizeof(int32_t);

	return 0;
}

int ln_service_callv_async_get_response_data(ln_service svc, void* data) {
	service* s = (service*)svc;
	// buffer into host-side struct
	ln_mutex_lock(&s->mutex);
	// fill host side structure from received buffer!
	char* host_data_ptr = (char*)data;
	host_data_ptr += s->request_len;
	char* buffer_ptr = s->response_buffer + sizeof(uint32_t); // ... without return value
	unsigned int left_in_buffer = s->response_len - sizeof(uint32_t);
	_ln_aux_mem_clear(&s->aux_mem);
	int ret = _ln_fill_structure(&s->aux_mem, &s->resp, &host_data_ptr, &buffer_ptr, &left_in_buffer, s->needs_endianess_swap);
	ln_mutex_unlock(&s->mutex);
	if(ret)
		return -LNE_SVC_RECV_RESP;
	if(left_in_buffer > 0) {
		_ln_set_error(s->clnt, "%s: error in received service response: received %d unexpected additional bytes!", s->name, left_in_buffer);
		return -LNE_SVC_RECV_RESP;
	}

	return 0;
}

int ln_service_request_finished(ln_service_request req, int* retval) {
	// <0 on error, 0 if not finished, 1 if finished
	service* s = (service*) req;
	ln_mutex_lock(&s->mutex);
	int is_finished = s->async_req_finished;
	if(is_finished && retval) 
		*retval = s->async_req_retval;
	if(is_finished && s->async_request_notification_pipe.input_fd != ln_pipe_invalid) {
		// check whether there is a notification to read
		ln_pipe_read_notification(s->async_request_notification_pipe);
	}
	ln_mutex_unlock(&s->mutex);
	return is_finished;
}
int ln_service_request_get_finished_notification_fd(ln_service_request req, int* fd) {
	service* s = (service*) req;
	
	ln_mutex_lock(&s->mutex);
	if(s->async_request_notification_pipe.input_fd == ln_pipe_invalid) {
		// need to create notification pipe...
		int ret;
		ln_debug(s->clnt, "initialize service async request notification pipe\n");
		if((ret = ln_pipe_init(&s->async_request_notification_pipe))) {
			ln_mutex_unlock(&s->mutex);
			ln_debug(s->clnt, "failed to initialize service async request notification pipe: %d\n", ret);
			return ret;
		}
	}
	*fd = (int)s->async_request_notification_pipe.input_fd;
	ln_debug(s->clnt, "  returning async request notification pipe fd %d\n", *fd);
	int is_finished = s->async_req_finished;
	if(is_finished && !s->async_request_notification_pipe_was_signaled) {
		s->async_request_notification_pipe_was_signaled = 1;
		ln_pipe_send_notification(s->async_request_notification_pipe);
	}
	ln_mutex_unlock(&s->mutex);

	return 0;
}


int _finish_async_request(service* s, int ret) {
	remove_async_service_call(s->clnt, s);
	ln_debug(s->clnt, "async service request is now finished!\n");
	ln_mutex_lock(&s->mutex);
	s->async_req_running = 0;
	s->async_req_finished = 1;
	s->async_req_retval = ret;
	if(s->async_request_notification_pipe.input_fd != ln_pipe_invalid && !s->async_request_notification_pipe_was_signaled) {
		ln_debug(s->clnt, "async request is now finished: signaling pipe output fd: %d\n",
		      s->async_request_notification_pipe.output_fd);
		s->async_request_notification_pipe_was_signaled = 1;
		ln_pipe_send_notification(s->async_request_notification_pipe);
	}
	ln_mutex_unlock(&s->mutex);
	return 0;
}

int ln_service_request_abort(ln_service_request req) {
	service* s = (service*) req;
	if(!s->async_req_running)
		return -LNE_SVC_REQ_NOT_RUNNING;
	s->async_req_running = 0;
	_finish_async_request(s, -LNE_SVC_REQ_ABORTED);
	_ln_close_socket(s->fd);
	s->fd = -1;
	return 0;
}

// called from async thread before select is called for this async service call!
// this should be nonblocking!!
// but it is not on win32!
int _ln_service_process_async_service_call_before_select(service* s) {
	// (shortly) blocking ops are okay in here
	// the more they block the more the interleaving service processing is prohibited
	s->async_request_wait_for_write = 0;
	s->async_request_wait_for_read = 0;

	// are we connected? (might be relatively long blocking due to request to manager)
	int ret;
	if(!s->async_did_connect && s->async_did_send == 0) {
		ret = _ln_service_connect(s, &s->async_did_connect);
		if(ret) { // error -> finish async request!
			ln_debug(s->clnt, "error while trying to do async service connect: %d\n", ret);
			return _finish_async_request(s, ret);
		}
	}
	// assume we are connected.

	// did we process the client's request data?
	if(!s->async_did_read_user_data) {
		// struct/buffer into iovec!
		struct _ln_aux_mem_holder* swap_aux_mem = NULL;
		if(s->needs_endianess_swap) {
			// need to byteswap!
			swap_aux_mem = &s->aux_mem;
			_ln_aux_mem_clear(swap_aux_mem);
			ln_debug(s->clnt, "call_service: will need to swap endianess of request!\n");
		}
		_ln_iovec_mgr_reset(&s->mgr, swap_aux_mem);
		_ln_iovec_mgr_add(&s->mgr, &s->mgr.n_bytes, sizeof(uint32_t), sizeof(uint32_t));
		_ln_service_add_request_header(s);
		if(!s->async_req_is_v) {
			char* dp = (char*)s->async_user_data; 
			_ln_read_structure(&s->mgr, &s->req, &dp, NULL);
		} else {
			// v - construct iovec to send!
			for(unsigned int i = 0; i < s->async_user_data_iov_len; i++)
				_ln_iovec_mgr_add(&s->mgr, s->async_user_data_iov[i].iov_base, s->async_user_data_iov[i].iov_len, s->async_user_data_iov_element_lens[i]);
		}

		if(s->needs_endianess_swap) {
			// swap length field!
			_ln_swap(sizeof(uint32_t), s->mgr.msg.msg_iov[0].iov_base, &s->mgr.n_bytes);
		}	

		s->async_did_read_user_data = 1;
		s->async_n_sent = 0;
		s->async_n_to_send = s->mgr.n_bytes;

		// does the socket accept data?
		fd_set write_fds;
		FD_ZERO(&write_fds);
		FD_SET(s->fd, &write_fds);
		struct timeval ts = {0, 0};
		int ret = select(s->fd + 1, NULL, &write_fds, NULL, &ts); // non-blocking question
		if(ret < 0) {
			if(not_connreset() || s->async_did_connect)
				return _finish_async_request(s, -LNE_CHECK_ERRNO);
			// it was connreset!
			// try once to automatically reconnect per ln_service_call!
			_ln_close_socket(s->fd);
			s->fd = -1;
			return _ln_service_process_async_service_call_before_select(s);
		}
		if(ret == 0) { // not ready to send
			s->async_request_wait_for_write = 1; // request select to block on write!
			return 0;
		}
	}

	// did we sent the request?
	if(!s->async_did_send) {
		// ssize_t n; -> a->async_n_sent

		// assume socket can write - otherwise we would'nt be here...
		// ... we will also be here if we can not send more data because the outer select-loop does not check the select's fd_set whether its really us who can continue
		// ready to send some bytes!
		ssize_t n = sendmsg(s->fd, &s->mgr.msg, MSG_DONTWAIT);
		if(n == -1) {
			if(errno == EINTR || errno == EAGAIN || errno == EWOULDBLOCK) { // interrupted system call
				s->async_request_wait_for_write = 1; // request select to block on write!
				return 0;
			}
			if(not_connreset() || s->async_did_connect) {
				_ln_set_error(s->clnt, "error in ln_service_call_async('%s') send data: %d %s!", s->name, errno, strerror(errno));
				return _finish_async_request(s, -LNE_CHECK_ERRNO);
			}
			// it was connreset!
			// try once to automatically reconnect per ln_service_call!
			_ln_close_socket(s->fd);
			s->fd = -1;
			return _ln_service_process_async_service_call_before_select(s);
		}
		// so we've sent something -- how much?
		s->async_n_sent += n;
		if(s->async_n_sent < s->async_n_to_send) {
			// not yet finished sending
			// update iovec structures to remove the already sent bytes!
			// remove n bytes!
			// if service logging is enabled, make copy of original mgr!
			if(s->n_log_services && !s->async_backup_mgr) {
				// create complete copy
				s->async_backup_mgr = _ln_iovec_mgr_clone(&s->mgr);
			}
			for(unsigned int i = 0; i < (unsigned)s->mgr.msg.msg_iovlen; i++) {
				if(s->mgr.msg.msg_iov[i].iov_len <= (unsigned)n) {
					// this iov is complete!
					n -= s->mgr.msg.msg_iov[i].iov_len;
					s->mgr.msg.msg_iov[i].iov_len = 0;
					if(n == 0)
						break;
				} else {
					// this iov is only partly completed!
					s->mgr.msg.msg_iov[i].iov_base = (iov_base_t)((char*)s->mgr.msg.msg_iov[i].iov_base + n);
					s->mgr.msg.msg_iov[i].iov_len -= n;
					break;
				}
			}			
			s->async_request_wait_for_write = 1; // request select to block on write!
			return 0;
		}
		// finished sending!
		if(s->n_log_services) {
			struct _ln_iovec_mgr* mgr_to_use;
			if(s->async_backup_mgr)
				mgr_to_use = s->async_backup_mgr; // backup
			else
				mgr_to_use = &s->mgr; // original
			if(_ln_log_service_event(s, LOG_SERVICE_REQUEST, s->async_completion_time, 0,
						 s->peer_address, s->clnt->client_id, s->request_id, mgr_to_use))
				ln_debug(s->clnt, "failed to log service async_call!\n");
			if(s->async_backup_mgr) {
				// release memory
				_ln_iovec_mgr_destroy(s->async_backup_mgr);
				free(s->async_backup_mgr);
				s->async_backup_mgr = NULL;
			}
		}

		s->async_did_send = 1;
		s->async_did_recv_size = 0;
		s->async_n_recvd = 0;

		// does the socket already have data?
		fd_set read_fds;
		FD_ZERO(&read_fds);
		FD_SET(s->fd, &read_fds);
		struct timeval ts = {0, 0};
		UNTIL_RET_NOT_EINTR(ret, select(s->fd + 1, &read_fds, NULL, NULL, &ts)); // non-blocking question
		if(ret < 0) {
			_ln_set_error(s->clnt, "error in ln_service_call_async('%s') wait for response!", s->name);
			return _finish_async_request(s, ret);
		}
		if(ret == 0) { // not ready to recv
			s->async_request_wait_for_read = 1; // request select to block on read!
			return 0;
		}

	}	
	// wait for response!
	// socket should already have data here!

	if(!s->async_did_recv_size) {
		// NON- blocking receive response size!
		ssize_t n = recv(s->fd, (char*)&s->response_len + s->async_n_recvd, sizeof(s->response_len) - s->async_n_recvd, MSG_DONTWAIT);
		if(n == -1) {
			if(errno == EINTR || errno == EAGAIN || errno == EWOULDBLOCK) { // interrupted system call
				s->async_request_wait_for_read = 1; // request select to block on read!
				return 0;
			}
			_ln_set_error(s->clnt, "error in ln_service_call_async('%s') wait for response size!", s->name);
			return _finish_async_request(s, n);
		}
		if(n == 0) {
			_ln_set_error(s->clnt, "error in ln_service_call_async('%s') received EOF while waiting for response size!", s->name);
			return _finish_async_request(s, -LNE_SVC_RECV_RESP);
		}
		s->async_n_recvd += n;
		if((unsigned)s->async_n_recvd < sizeof(s->response_len)) {
			s->async_request_wait_for_read = 1; // request select to block on read!
			return 0;
		}
		
		if(s->needs_endianess_swap) {
			uint32_t tmp;
			_ln_swap(sizeof(s->response_len), &tmp, &s->response_len);
			s->response_len = tmp;
		}
		s->response_len -= sizeof(s->response_len);
		if(s->response_len > s->response_buffer_len) {
			char* new_buffer = (char*)realloc(s->response_buffer, s->response_len);
			if(!new_buffer)
				return -LNE_SVC_RECV_RESP_MEM;
			s->response_buffer = new_buffer;
			s->response_buffer_len = s->response_len;
		}
		s->async_did_recv_size = 1;
		s->async_response_buffer = s->response_buffer;
		s->async_n_recvd = 0;
	}
	// now non-blocking read response!
	// n = full_recv(s->fd, s->response_buffer, s->response_len);

	/*
	  ignore signals,
	  chunked read until len is reached -> return len
	  or EOF or error -> return 0 or -LNE_CHECK_ERRNO
	 */
	ssize_t n = recv(s->fd, s->async_response_buffer, s->response_len - s->async_n_recvd, MSG_DONTWAIT);
	if(n == -1) {
		if(errno == EINTR || errno == EAGAIN || errno == EWOULDBLOCK) { // interrupted system call
			s->async_request_wait_for_read = 1; // request select to block on read!
			return 0;
		}
		_ln_set_error(s->clnt, "error in ln_service_call_async('%s') wait for response!", s->name);
		return _finish_async_request(s, n);
	}
	if(n == 0) {
		_ln_set_error(s->clnt, "error in ln_service_call_async('%s') received EOF while waiting for response!", s->name);
		return _finish_async_request(s, -LNE_SVC_RECV_RESP);
	}

	s->async_n_recvd += n;
	s->async_response_buffer += n;
	
	if((unsigned)s->async_n_recvd < s->response_len) { // not yet finished
		s->async_request_wait_for_read = 1; // request select to block on read!
		return 0;
	}
	// completely received response!
	int32_t response_error;
	if(s->needs_endianess_swap) {
		_ln_swap(sizeof(response_error), &response_error, s->response_buffer);
	} else
		response_error = *(int32_t*)s->response_buffer;
		
	if(response_error != 0)
		return _finish_async_request(s, response_error);

	if(!s->async_req_is_v) {		
		// buffer into host-side struct
		// fill host side structure from received buffer!
		char* host_data_ptr = (char*)s->async_user_data;
		host_data_ptr += s->request_len;
		char* buffer_ptr = s->response_buffer + sizeof(uint32_t); // ... without return value
		unsigned int left_in_buffer = s->response_len - sizeof(uint32_t);
		_ln_aux_mem_clear(&s->aux_mem);
		ret = _ln_fill_structure(&s->aux_mem, &s->resp, &host_data_ptr, &buffer_ptr, &left_in_buffer, s->needs_endianess_swap);
		if(ret) {
			_ln_set_error(s->clnt, "error in ln_service_call_async('%s') invalid response packet!", s->name);
			return _finish_async_request(s, LNE_SVC_RECV_RESP);
		}
		if(left_in_buffer > 0) {
			_ln_set_error(s->clnt, "%s: error in received service response: received %d unexpected additional bytes!", s->name, left_in_buffer);
			return -LNE_SVC_RECV_RESP;
		}
	}

	return _finish_async_request(s, 0); // finished!
}

int _ln_service_connect(service* s, int* did_connect) {
	ln_debug(s->clnt, "%s\n", __func__);

	if(did_connect)
		*did_connect = 0;
	if(s->fd != -1) {
		// there is a fd, test for eof!
		char dummy;
#ifdef __WIN32__
		// assume socket is still alive
		int m = s->fd + 1;
		fd_set readfds;
		FD_ZERO(&readfds);
		FD_SET(s->fd, &readfds);
		fd_set excfds;
		FD_ZERO(&excfds);
		FD_SET(s->fd, &excfds);
		struct timeval ts = { 0, 0 };
		int n = select(m, &readfds, NULL, &excfds, &ts);
		if(n == 0)
			return 0; // timeout, assume socket is alive
		if(n == 1) {
#endif
		int ret = recv(s->fd, &dummy, 1, MSG_DONTWAIT | MSG_PEEK);
		if(ret == -1 && (errno == EWOULDBLOCK || errno == EAGAIN))
			return 0; // already connected, no eof
		if(ret == 1) {
			ln_debug(s->clnt, "ERROR: there is unexpected incoming data on service connection! protocol error! -- closing connection!\n");
		}
#ifdef __WIN32__
		}
#endif
		// assume connection is closed!
		_ln_close_socket(s->fd);
		// repopen!
		s->fd = -1;
	}

	request_t r;
	int ret = _ln_init_request(
		s->clnt, &r,
		"method", "r", "request_service",
		"name", "r", s->name,
		"interface", "r", s->interface_name,
		"signature", "r", s->signature,
		NULL);
	if(ret)
		return ret;
	ln_mutex_lock(&s->clnt->communication_mutex);
	ret = _ln_request_and_wait(&r);
	ln_mutex_unlock(&s->clnt->communication_mutex);
	if(ret) {
		_ln_destroy_request(&r);
		return ret;
	}

	// is there a unix_socket in the response?
	char unix_socket[128];
	int use_unix_socket = true;
	unsigned int port;
	char ip_address[128];
	if((ret = _ln_get_from_request(&r, "unix_socket", "s128", &unix_socket))) {
		// nope
		s->clnt->error_message[0] = 0;
		use_unix_socket = false;
		if((ret = _ln_get_from_request(&r, "port", "u", &port))) {
			_ln_destroy_request(&r);
			return ret;
		}
		
		if((ret = _ln_get_from_request(&r, "address", "s128", &ip_address))) {
			_ln_destroy_request(&r);
			return ret;
		}
	}
	
	char endianess[32] = "little";
	if((ret = _ln_get_from_request(&r, "endianess", "s32", &endianess)))
		ln_debug(s->clnt, "failed to get endianess from request_service-call. your ln_manager is probably too old. assume little endian service provider!\n");
	
	if(!strcmp(endianess, "little"))
		s->endianess = BO_LITTLE_ENDIAN;
	else if(!strcmp(endianess, "big"))
		s->endianess = BO_BIG_ENDIAN;
	else {
		ln_debug(s->clnt, "manager gave unknown endianess '%s' - assuming little endian.",
		      endianess);
		s->endianess = BO_LITTLE_ENDIAN;
	}
	ln_debug(s->clnt, "service provider endianess: %s\n", endianess);

	enum endianess_t host_byte_order;
	uint16_t test = 0x1234;
	if( ((uint8_t*)&test)[0] == 0x34 )
		host_byte_order = BO_LITTLE_ENDIAN;
	else
		host_byte_order = BO_BIG_ENDIAN;

	if(host_byte_order != s->endianess) {
		// we have to swap request and response on our side!
		s->needs_endianess_swap = 1;
	} else {
		s->needs_endianess_swap = 0;
	}

	if(s->clnt->manager_library_version >= 13 && _ln_is_in_request(&r, "log_services")) {
		char log_services[1024] = "";
		if((ret = _ln_get_from_request(&r, "log_services", "s1024", &log_services))) {
			ln_debug(s->clnt, "manager did not send log_services?!\n");
		} else if(log_services[0] != 0) {
			char* ip = log_services;
			while(true) {
				char* fp = strtok(ip, ",");
				ip = NULL;
				if(!fp)
					break;
				log_service* ls;
				if((ret = _ln_client_add_log_service(s->clnt, fp, &ls)))
					ln_debug(s->clnt, "failed to add log_service to client!");
				else if((ret = _ln_service_add_log_service(s, ls)))
					ln_debug(s->clnt, "failed to add log_service to service client!");
			}
		}
	}

	s->send_request_id = 0;
	if(s->clnt->manager_library_version >= 13 && _ln_is_in_request(&r, "send_request_id")) {
		if((ret = _ln_get_from_request(&r, "send_request_id", "d", &s->send_request_id))) {
			ln_debug(s->clnt, "manager did not send send_request_id?!\n");
		}
	}		
	_ln_destroy_request(&r);	

	if(s->peer_address) {
		free(s->peer_address);
		s->peer_address = NULL;
	}
	
	if(!use_unix_socket) {
		struct sockaddr_in provider_address;
		provider_address.sin_port = htons(port);
		if((ret = resolve_hostname(ip_address, &provider_address))) {
			ln_debug(s->clnt, "failed to reolve hostname '%s'\n", ip_address);
			return -LNE_CHECK_ERRNO;
		}

		s->fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if(s->fd == -1) {
			ln_debug(s->clnt, "failed to create socket\n");
			return -LNE_CHECK_ERRNO;
		}

		int flag = 1;
		if((ret = setsockopt(s->fd, IPPROTO_TCP, TCP_NODELAY, (char*)&flag, sizeof(int))))
			ln_debug(s->clnt, "warning: failed to set TCP_NODELAY: %d %s\n", errno, strerror(errno));
		
		if((ret = connect(s->fd, (struct sockaddr*)&provider_address, sizeof(provider_address)))) {
			_ln_close_socket(s->fd);
			s->fd = -1;
			ln_debug(s->clnt, "failed to connect to service at %s:%d\n", ip_address, port);
			return -LNE_CHECK_ERRNO;
		}
		char peer_address[256];
		snprintf(peer_address, 256, "%s:%d", ip_address, port);
		s->peer_address = strdup(peer_address);
		
		ln_enable_tcp_keepalive(s->fd);
	} else {
#if defined(HAVE_AF_UNIX)
		struct sockaddr_un provider_address;

		provider_address.sun_family = AF_UNIX;
		// use linux abstrace namespace
		provider_address.sun_path[0] = 0;
		strcpy(provider_address.sun_path + 1, unix_socket);

		s->fd = socket(AF_UNIX, SOCK_STREAM, 0);
		if(s->fd == -1) {
			ln_debug(s->clnt, "failed to create unix socket\n");
			return -LNE_CHECK_ERRNO;
		}

		ln_debug(s->clnt, "%s: connecting to unix socket at %s with fd %d\n", __func__, unix_socket, s->fd);
		if((ret = connect(s->fd, (struct sockaddr*)&provider_address, sizeof(sa_family_t) + 1 + strlen(unix_socket) + 1))) {
			_ln_close_socket(s->fd);
			s->fd = -1;
			ln_debug(s->clnt, "failed to connect service at unix socket %s: %d, %s\n",
				 unix_socket, errno, strerror(errno));
			return -LNE_CHECK_ERRNO;
		}
		s->peer_address = strdup(unix_socket);
#else
		return -LNE_NOT_ON_THIS_OS;
#endif		
	}

	// service connected!
	if(did_connect)
		*did_connect = 1;
	return 0;
}

// provider
DEFINE_LIST_METHODS(service, service_client);
DEFINE_LIST_METHODS_TYPE_NAME(service, service_client, fake_service_clients);

int ln_service_provider_set_handler(ln_service svc, ln_service_handler handler, void* user_data) {
	service* s = (service*)svc;
	s->handler = handler;
	s->handler_user_data = user_data;
	return 0;
}

int ln_service_provider_get_fd(ln_service svc) {
       service* s = (service*)svc;
	ln_debug(s->clnt, "%s in thread %#x\n", __func__, get_thread_id());
       if(!s->is_provider)
               return -LNE_SVC_IS_CLIENT;
       if(!s->is_listening)
               return -LNE_START_PROVIDER_FAILED;

       if(s->new_provider_fd_notification_pipe.input_fd == ln_pipe_invalid)
	       if(ln_pipe_init(&s->new_provider_fd_notification_pipe))
		       ln_debug(s->clnt, "s %s failed to init new provider notification pipe!\n", check_null_str(s->name));
       
       return (int)s->new_provider_fd_notification_pipe.input_fd;
}

int ln_service_provider_set_client_fd_handler(ln_service svc, ln_service_fd_handler handler, void* user_data) {
	service* s = (service*)svc;
	ln_debug(s->clnt, "%s in thread %#x\n", __func__, get_thread_id());
	if(!s->is_provider)
		return -LNE_SVC_IS_CLIENT;

	
	s->fd_handler = handler;
	s->fd_handler_user_data = user_data;

	if(s->new_provider_fd_notification_pipe.input_fd == ln_pipe_invalid)
		if(ln_pipe_init(&s->new_provider_fd_notification_pipe))
			ln_debug(s->clnt, "s %s failed to init new provider notification pipe!\n", check_null_str(s->name));
		
	// do we already have listening sockets? if so: synthesize events!
	if(s->new_provider_fd_notification_pipe.input_fd != ln_pipe_invalid && (s->fd != -1 || s->unix_fd != -1))
		ln_pipe_send_notification(s->new_provider_fd_notification_pipe);
	return 0;
}

void _ln_service_add_provider_fd(service* s, int fd) {
	ln_debug(s->clnt, "%s in thread %#x\n", __func__, get_thread_id());
	if(!s->fd_handler) {
		if(s->clnt->debug) ln_debug(s->clnt, "%s: no fd handler registered...\n", __func__);
		return;
	}
	if(s->clnt->debug) ln_debug(s->clnt, "%s: call fd handler LN_NEW_SVC_FD for provider fd %d\n", __func__, fd);
	s->fd_handler(s->clnt, LN_NEW_SVC_FD, fd, s->fd_handler_user_data);
}
void _ln_service_remove_provider_fd(service* s, int fd) {
	if(!s->fd_handler)
		return;
	s->fd_handler(s->clnt, LN_REMOVE_SVC_FD, fd, s->fd_handler_user_data);
}

void _ln_service_add_client_fd(service_client* c) {
	ln_debug(c->svc->clnt, "%s in thread %#x\n", __func__, get_thread_id());
	if(!c->svc->fd_handler)
		return;
	c->svc->fd_handler(c->svc->clnt, LN_NEW_SVC_CLIENT, c->fd, c->svc->fd_handler_user_data);	
}
void _ln_service_remove_client_fd(service_client* c) {
	if(c->svc->fd_handler)
		c->svc->fd_handler(c->svc->clnt, LN_REMOVE_SVC_CLIENT, c->fd, c->svc->fd_handler_user_data);	
	_ln_close_socket(c->fd);
	c->fd = -1;
}

int ln_service_provider_register(ln_client clnt, ln_service svc) {
	return ln_service_provider_register_in_group(clnt, svc, NULL);
}

int _ln_service_provider_bind_tcp(service* s) {
	// assumes client is locked
	ln_debug(s->clnt, "bind tcp port for service provider %s\n", check_null_str(s->name));
	if(s->fd != -1)
		return -LNE_SVC_ALREADY_REGISTERED;

	s->fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(s->fd == -1)
		return -LNE_CHECK_ERRNO;

#if defined(__WIN32__)
	// win32 always needs a bind"!
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = 0;
	addr.sin_addr.s_addr = INADDR_ANY;
	if(bind(s->fd, (struct sockaddr*) &addr, sizeof(addr))) {
		_ln_close_socket(s->fd);
		s->fd = -1;
		return -LNE_CHECK_ERRNO;
	}
#endif

	if(listen(s->fd, 5)) {
		_ln_close_socket(s->fd);
		s->fd = -1;
		return -LNE_CHECK_ERRNO;
	}

	struct sockaddr_in server_address;
	os_socklen_t server_address_len = sizeof(server_address);
	if(getsockname(s->fd, (struct sockaddr*)&server_address, &server_address_len) == -1) {
		_ln_close_socket(s->fd);
		s->fd = -1;
		return -LNE_CHECK_ERRNO;
	}
	s->service_port = ntohs(server_address.sin_port);
	if(s->new_provider_fd_notification_pipe.input_fd != ln_pipe_invalid) {
		if(s->clnt->debug) ln_debug(s->clnt, "_bind_tcp: send new_provider_notification!\n");
		ln_pipe_send_notification(s->new_provider_fd_notification_pipe);
	}

	return 0;
}

int _ln_service_provider_bind_unix(service* s) {
#if !defined(HAVE_AF_UNIX)
	return -LNE_NOT_ON_THIS_OS;
#else
	// assumes client is locked
	ln_debug(s->clnt, "bind unix listening socket for service provider %s\n", check_null_str(s->name));
	if(s->unix_fd != -1)
		return -LNE_SVC_ALREADY_REGISTERED;

	s->unix_fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if(s->unix_fd == -1) {
		ln_debug(s->clnt, "failed to create AF_UNIX socket: %d %s\n", errno, strerror(errno));       
		return -LNE_CHECK_ERRNO;
	}

	char* socket_name = NULL;
	unsigned int socket_name_size = 0;
	strformat_(&socket_name, &socket_name_size, "/%s:%d/%#x/%p", s->clnt->manager_host, s->clnt->manager_port, getpid(), s);

	ln_debug(s->clnt, "%s: unix socket name: %s\n", __func__, socket_name);
	
	unsigned int addr_len = sizeof(sa_family_t) + 1 + strlen(socket_name) + 1;
	struct sockaddr_un* addr = (struct sockaddr_un*)calloc(addr_len, 1);
	addr->sun_family = AF_UNIX;
	// use linux abstrace namespace
	addr->sun_path[0] = 0;
	strcpy(addr->sun_path + 1, socket_name);
		
	if(bind(s->unix_fd, (struct sockaddr*) addr, addr_len)) {
		int org_errno = errno;
		ln_debug(s->clnt, "failed to bind AF_UNIX socket: %d %s\n", errno, strerror(errno));
		_ln_close_socket(s->unix_fd);
		s->unix_fd = -1;
		free(socket_name);
		free(addr);
		errno = org_errno;
		return -LNE_CHECK_ERRNO;
	}
	free(addr);
	
	if(listen(s->unix_fd, 5)) {
		int org_errno = errno;
		ln_debug(s->clnt, "failed to listen AF_UNIX socket: %d %s\n", errno, strerror(errno));
		_ln_close_socket(s->unix_fd);
		s->unix_fd = -1;
		free(socket_name);
		errno = org_errno;
		return -LNE_CHECK_ERRNO;
	}

	s->unix_socket_name = socket_name;
	if(s->new_provider_fd_notification_pipe.input_fd != ln_pipe_invalid)
		ln_pipe_send_notification(s->new_provider_fd_notification_pipe);
#endif	
	return 0;
}

int _ln_service_request_tcp_port(service* s) {
	if(s->fd == -1) {
		int ret = _ln_service_provider_bind_tcp(s);
		if(ret)
			return ret;
	}
	return s->service_port;
}

int _ln_service_request_unix_socket(service* s) {
	if(s->unix_fd != -1)
		return 0;
	return _ln_service_provider_bind_unix(s);
}

int ln_service_provider_register_in_group(ln_client clnt, ln_service svc, const char* group_name) {
	// now this service is a provider!
	service* s = (service*)svc;
	s->is_provider = 1;
	if(s->fd != -1)
		return -LNE_SVC_ALREADY_REGISTERED;

	if(!s->handler)
		return -LNE_SVC_NO_HANDLER;

	lock_client(s->clnt);
	
	_ln_service_group_add(group_name, s);
	ln_debug(s->clnt, "adding service provider to group %s at %p\n", check_null_str(group_name), s->group);

	int port = -1;
	if(s->clnt->manager_library_version < 12) {
	  // can't do on demand, backward compatibility with old managers
	  int ret = _ln_service_provider_bind_tcp(s);
	  if(ret) {
	    unlock_client(s->clnt);
	    return ret;
	  }
	  port = s->service_port;
	}

	// now register this port with the manager
	request_t r;
	int ret = _ln_init_request(
		s->clnt, &r,
		"method", "r", "register_service",
		"name", "r", s->name,
		"interface", "r", s->interface_name,
		"signature", "r", s->signature,
		"port", "d", port,
		NULL);
	if(ret) {
		unlock_client(s->clnt);
		return ret;
	}
	ln_mutex_lock(&s->clnt->communication_mutex);
	ret = _ln_request_and_wait(&r);
	ln_mutex_unlock(&s->clnt->communication_mutex);

	char log_services[1024] = "";
	if(s->clnt->manager_library_version >= 13 && _ln_is_in_request(&r, "log_services"))
		if((ret = _ln_get_from_request(&r, "log_services", "s1024", &log_services)))
			ln_debug(s->clnt, "manager did not send log_services?!\n");	
	_ln_destroy_request(&r);
	
	if(ret) {
		unlock_client(s->clnt);
		return ret;
	}
	
	s->clnt->do_async_service_handling = s->clnt->enable_async_service_handling;
			
	// successfully registered
	s->is_listening = 1;
	
	unlock_client(s->clnt);
	
	if(log_services[0] != 0) {
		char* ip = log_services;
		while(true) {
			char* fp = strtok(ip, ",");
			ip = NULL;
			if(!fp)
				break;
			log_service* ls;
			if((ret = _ln_client_add_log_service(s->clnt, fp, &ls)))
				ln_debug(s->clnt, "failed to add log_service to client!");
			else if((ret = _ln_service_add_log_service(s, ls)))
				ln_debug(s->clnt, "failed to add log_service to service client!");
		}
	}
#ifndef __WIN32__
	if(s->clnt->do_async_service_handling) {
		// notify async thread:
		ln_pipe_send_notification(s->clnt->async_notification_pipe);
	}
#endif
	return 0;
}

/**
   this function should only be called from within service handlers

   (-> s->mutex is locked in _ln_handle_service_group_requests(service_group* sg))
 */
int ln_service_request_set_data(ln_service_request req, void* data, const char* signature) {
	service_client* c = (service_client*)req;
	service* s = c->svc;

	if(strcmp(s->signature, signature)) {
		ln_debug(s->clnt, "%s - expected signature '%s' user passed '%s'!\n", __func__, s->signature, signature);
		return -LNE_SVC_REQ_SET_DATA_WRONG;
	}
	
	// buffer into host-side struct
	// now fillin request!
	// fill host side structure from received buffer!
	c->service_data = (char*)data;
	char* host_data_ptr = c->service_data;
	char* buffer_ptr = c->user_buffer;
	unsigned int left_in_buffer = c->user_buffer_len;
	// printf("buffer at %p\n", buffer_ptr);
	// print_hex("set data from: ", c->buffer, c->buffer_len);
	
	_ln_aux_mem_clear(&c->aux_mem);
	if(s->req.n_inner_fields) {
		int ret = _ln_fill_structure(&c->aux_mem, &s->req, &host_data_ptr, &buffer_ptr, &left_in_buffer, 0); // service provider does not need to swap!
		if(ret)
			return -LNE_SVC_RECV_REQ;
		if(left_in_buffer > 0) {
			_ln_set_error(s->clnt, "%s: error in received service request: received %d unexpected additional bytes!", s->name, left_in_buffer);
			return -LNE_SVC_RECV_REQ;
		}
	}

	// set response part to zero!
	memset((char*)data + s->request_len, 0, s->hostside_response_len);
	
	return 0;
}

int ln_service_request_get_request_data(ln_service_request req, char** buffer, uint64_t* buffer_len) {
	service_client* c = (service_client*)req;

	*buffer = c->user_buffer;
	*buffer_len = c->user_buffer_len;

	return 0;
}

// for service handler
int ln_service_request_is_aborted(ln_service_request req) {
	service_client* c = (service_client*)req;
	/*service* s = c->svc;*/

	if(c->is_fake)
		return 0;
	/*
	  we assume that there should be nothing to read from this socket, if there is - then its probably eof
	 */

	if(c->fd == -1)
		return 1; // aborted!

	// try select on this socket
	fd_set read_fds;
	FD_ZERO(&read_fds);
	FD_SET(c->fd, &read_fds);
	struct timeval tv = {0, 0};
	int n = select(c->fd + 1, &read_fds, NULL, NULL, &tv);
	if(n == -1) {
		// perror("select");
		_ln_service_remove_client_fd(c);
		return 1; // aborted
	}
	if(FD_ISSET(c->fd, &read_fds)) {
		// do non blocking read of 0 bytes
		char buffer;
		n = recv(c->fd, &buffer, 1, MSG_DONTWAIT);
		if(n == 0 || n == -1) {
			// printf("assume aborted!\n");
			_ln_service_remove_client_fd(c);
			return 1; // aborted!
		}
		// printf("recv: %d\n", n);
	}

	// printf("assume not aborted: %d\n", n);
	return 0;
}

int ln_service_request_respond_data(ln_service_request req, void* data) {
	service_client* c = (service_client*)req;
	c->service_data = (char*)data;
	return ln_service_request_respond(req);
}


static void _send_fake_response(service* s, service_client* c, double completion_time)
{
	// send response to manager!
	char* response_data = NULL;
	unsigned int response_data_size = 0;
	unsigned int response_data_len = 0;
	char* cp = response_data;

	struct iovec* iov = c->mgr.msg.msg_iov;
	for(unsigned int i = 1; i < (unsigned)c->mgr.msg.msg_iovlen; i++) { // skip n_bytes here
		//printf("add %d bytes: %d %d\n", iov[i].iov_len, response_data_len, response_data_size);
		growing_buffer_appendn(&response_data, &cp, &response_data_size, (char*)iov[i].iov_base, iov[i].iov_len);
		response_data_len += iov[i].iov_len;
	}

	request_t resp;
	_ln_init_request(
		s->clnt, &resp,
		"answer_to", "r", "call_service",
		"success", "r", "success",
		"name", "r", s->name,
		"request_id", "u", c->fake_request_id,
		NULL);
	_ln_add_to_request_raw(&resp, strdup("response_data"), response_data, response_data_len, 1);
	_ln_send_request(&resp);
	_ln_destroy_request(&resp);

	if(s->n_log_services) {
		uint32_t client_id = 0;
		uint32_t request_id = 0;
		if(c->request_hdr) {
			client_id = c->request_hdr->client_id;
			request_id = c->request_hdr->request_id;
		}
		if(_ln_log_service_event(s, LOG_SERVICE_RESPONSE, completion_time, c->request_time,
					 c->peer_address, client_id, request_id, &c->mgr))
			ln_debug(s->clnt, "failed to log service fake response!\n");
	}
}

/**
   this function should only be called from within service handlers

   (-> s->mutex is locked in _ln_handle_service_group_requests(service_group* sg))
 */
int ln_service_request_respond(ln_service_request req) {
	service_client* c = (service_client*)req;
	service* s = c->svc;
	double completion_time;
	
	if(s->n_log_services)
		completion_time = ln_get_time();	
	
	// struct/buffer into iovec!

	// send request with data over fd 
	_ln_iovec_mgr_reset(&c->mgr, NULL);
	_ln_iovec_mgr_add(&c->mgr, &c->mgr.n_bytes, sizeof(uint32_t), sizeof(uint32_t)); // first data is always compelte request length
	_ln_iovec_mgr_add(&c->mgr, &c->request_error, sizeof(uint32_t), sizeof(uint32_t)); // second is response error code

	if(c->request_error == 0) {
		// send success response -> fill in data fields!
		// only access c->service_data on success!
		char* dp = (char*)c->service_data; 
		dp += s->request_len; // read response part!

		_ln_read_structure(&c->mgr, &s->resp, &dp, NULL);
	}
	if(s->clnt->debug) {
		ln_debug(s->clnt, "ln_service_request_respond resp iovec:\n");
		_ln_iovec_mgr_print(s->clnt, &c->mgr);
	}

	c->did_respond = 1;

	if(c->is_fake) {
		_send_fake_response(s, c, completion_time);
		return 0;
	}
	// add multiple message handling
	unsigned int n_messages_already_sent = 0;
	unsigned int n_messages_to_send = 1;
	struct msghdr* next_message = &c->mgr.msg;
	unsigned int* next_message_to_send = &c->mgr.n_bytes;

	struct msghdr* all_messages = NULL;
	unsigned int* all_message_to_sends = NULL;

#ifdef __LINUX__
	unsigned int iov_max = sysconf(_SC_IOV_MAX);
#else
	unsigned int iov_max = IOV_MAX;
#endif
	if((unsigned)c->mgr.msg.msg_iovlen > iov_max) {
		ln_debug(s->clnt, "%s - message has %d iov's - system max is %d -> will split!\n", __func__, c->mgr.msg.msg_iovlen, iov_max);

		n_messages_to_send = c->mgr.msg.msg_iovlen / iov_max + 1;
		all_messages = (struct msghdr*)malloc(sizeof(struct msghdr) * n_messages_to_send);
		memset(all_messages, 0, sizeof(struct msghdr) * n_messages_to_send);
		all_message_to_sends = (unsigned int*)malloc(sizeof(unsigned int) * n_messages_to_send);
		memset(all_message_to_sends, 0, sizeof(unsigned int) * n_messages_to_send);

		// fill messages
		unsigned int iovs_left = c->mgr.msg.msg_iovlen;
		unsigned int iovs_done = 0;

		next_message = all_messages;
		next_message_to_send = all_message_to_sends;		

		n_messages_to_send = 0;
		while(iovs_left > 0) {
			unsigned int to_copy = iov_max;
			if(to_copy > iovs_left)
				to_copy = iovs_left;
			
			next_message->msg_iov = &c->mgr.msg.msg_iov[iovs_done];
			next_message->msg_iovlen = to_copy;
			for(unsigned int i = 0; i < (unsigned)next_message->msg_iovlen; i++)
				*next_message_to_send += next_message->msg_iov[i].iov_len;
			n_messages_to_send ++;
			
			ln_debug(s->clnt, "next_message %p, %d iov's, *next_message_to_send: %d\n", next_message, next_message->msg_iovlen, *next_message_to_send);
					
			iovs_left -= to_copy;
			iovs_done += to_copy;

			next_message_to_send++;
			next_message++;
		}
		
		// set to first one
		next_message = all_messages;
		next_message_to_send = all_message_to_sends;		
	}

	unsigned int to_send = *next_message_to_send;
	while(to_send) {
		ssize_t n = sendmsg(c->fd, next_message, 0);
		if(n == -1) {
			if(errno == EINTR || errno == EAGAIN)
				continue;
			if(all_messages) {
				free(all_messages);
				free(all_message_to_sends);
			}
			return -LNE_CHECK_ERRNO;
		}
		
		if((unsigned)n == to_send) {
			// finished this message, is there another?
			n_messages_already_sent += 1;
			ln_debug(s->clnt, "finished %d messages\n", n_messages_already_sent);
			if(n_messages_already_sent == n_messages_to_send)
				break; // finished
			// select next message!
			next_message++;
			next_message_to_send++;
			
			// ln_debug(s->clnt, "next message hdr is: %p with %d bytes\n", next_message, *next_message_to_send);
			to_send = *next_message_to_send;
			continue;
		}
		
		// not yet finished sending
		to_send -= n;
		// update iovec structures to remove the already sent bytes!
		// remove n bytes!
		for(unsigned int i = 0; i < (unsigned)next_message->msg_iovlen; i++) {
			if(next_message->msg_iov[i].iov_len <= (unsigned)n) {
				// this iov is complete!
				n -= next_message->msg_iov[i].iov_len;
				next_message->msg_iov[i].iov_len = 0;
				if(n == 0)
					break;
			} else {
				// this iov is only partly completed!
				next_message->msg_iov[i].iov_base = (iov_base_t)((char*)next_message->msg_iov[i].iov_base + n);
				next_message->msg_iov[i].iov_len -= n;
				break;
			}
		}
	}
	
	if(all_messages) {
		free(all_messages);
		free(all_message_to_sends);
	}
	if(s->n_log_services) {
		uint32_t client_id = 0;
		uint32_t request_id = 0;
		if(c->request_hdr) {
			client_id = c->request_hdr->client_id;
			request_id = c->request_hdr->request_id;
		}
		if(_ln_log_service_event(s, LOG_SERVICE_RESPONSE, completion_time, c->request_time,
					 c->peer_address, client_id, request_id, &c->mgr))
			ln_debug(s->clnt, "failed to log service response!\n");
	}
	return 0;
}

int ln_service_request_respondv(ln_service_request req, struct iovec* iov, unsigned int iov_len) {
	service_client* c = (service_client*)req;
	client* clnt = c->svc->clnt;

	if(c->did_respond) {
		ln_debug(c->svc->clnt, "ERROR: service handler already responded to this service-request!\n");
		return -LNE_SVC_HANDLER_MULTI_RESP;
	}
	
	double completion_time;
	if(c->svc->log_services)
		completion_time = ln_get_time();
	
	// construct iovec to send!
	if((iov_len + 2) > c->mgr.msg_max_len) {
		struct iovec* new_vec = (struct iovec*)realloc(c->mgr.msg.msg_iov, sizeof(struct iovec) * (iov_len + 2));
		if(!new_vec)
			return -LNE_NO_MEM;
		c->mgr.msg.msg_iov = new_vec;
		c->mgr.msg_max_len = (iov_len + 2);
	}

	_ln_iovec_mgr_reset(&c->mgr, NULL);
	_ln_iovec_mgr_add(&c->mgr, &c->mgr.n_bytes, sizeof(uint32_t), sizeof(uint32_t));
	_ln_iovec_mgr_add(&c->mgr, &c->request_error, sizeof(uint32_t), sizeof(uint32_t)); // second is response error code

	if(c->request_error == 0)
		for(unsigned int i = 0; i < iov_len; i++)
			_ln_iovec_mgr_add(&c->mgr, iov[i].iov_base, iov[i].iov_len, 1); // no swap!

	_ln_iovec_mgr_print(clnt, &c->mgr);
	ln_debug(clnt, "ln_service_request_respondv resp iovec of %d bytes, in thread %#x\n", c->mgr.n_bytes, get_thread_id());
	c->did_respond = 1;

	if(c->is_fake) {
		_send_fake_response(c->svc, c, completion_time);
		return 0;
	}
	if(c->fd == -1)
		return 0;
	
	// add multiple message handling
	unsigned int n_messages_already_sent = 0;
	unsigned int n_messages_to_send = 1;
	struct msghdr* next_message = &c->mgr.msg;
	unsigned int* next_message_to_send = &c->mgr.n_bytes;

	struct msghdr* all_messages = NULL;
	unsigned int* all_message_to_sends = NULL;

#ifdef __LINUX__
	unsigned int iov_max = sysconf(_SC_IOV_MAX);
#else
	unsigned int iov_max = IOV_MAX;
#endif
	if((unsigned)c->mgr.msg.msg_iovlen > iov_max) {
		ln_debug(clnt, "%s - message has %d iov's - system max is %d -> will split!\n", __func__, c->mgr.msg.msg_iovlen, iov_max);

		n_messages_to_send = c->mgr.msg.msg_iovlen / iov_max + 1;
		all_messages = (struct msghdr*)malloc(sizeof(struct msghdr) * n_messages_to_send);
		memset(all_messages, 0, sizeof(struct msghdr) * n_messages_to_send);
		all_message_to_sends = (unsigned int*)malloc(sizeof(unsigned int) * n_messages_to_send);
		memset(all_message_to_sends, 0, sizeof(unsigned int) * n_messages_to_send);

		// fill messages
		unsigned int iovs_left = c->mgr.msg.msg_iovlen;
		unsigned int iovs_done = 0;

		next_message = all_messages;
		next_message_to_send = all_message_to_sends;		

		n_messages_to_send = 0;
		while(iovs_left > 0) {
			unsigned int to_copy = iov_max;
			if(to_copy > iovs_left)
				to_copy = iovs_left;
			
			next_message->msg_iov = &c->mgr.msg.msg_iov[iovs_done];
			next_message->msg_iovlen = to_copy;
			for(unsigned int i = 0; i < (unsigned)next_message->msg_iovlen; i++)
				*next_message_to_send += next_message->msg_iov[i].iov_len;
			n_messages_to_send ++;
			
			ln_debug(clnt, "next_message %p, %d iov's, *next_message_to_send: %d\n", next_message, next_message->msg_iovlen, *next_message_to_send);
					
			iovs_left -= to_copy;
			iovs_done += to_copy;

			next_message_to_send++;
			next_message++;
		}
		
		// set to first one
		next_message = all_messages;
		next_message_to_send = all_message_to_sends;		
	}
	
	
	unsigned int to_send = *next_message_to_send;
	while(to_send) {
		ssize_t n = sendmsg(c->fd, next_message, 0);
		if(n == -1) {
			if(errno == EINTR || errno == EAGAIN)
				continue;
			if(all_messages) {
				free(all_messages);
				free(all_message_to_sends);
			}
			return -LNE_CHECK_ERRNO;
		}
		
		if((unsigned)n == to_send) {
			// finished this message, is there another?
			n_messages_already_sent += 1;
			ln_debug(clnt, "finished %d messages\n", n_messages_already_sent);
			if(n_messages_already_sent == n_messages_to_send)
				break; // finished
			// select next message!
			next_message++;
			next_message_to_send++;
			
			// ln_debug(clnt, "next message hdr is: %p with %d bytes\n", next_message, *next_message_to_send);
			to_send = *next_message_to_send;
			continue;
		}
		
		// not yet finished sending
		to_send -= n;
		// update iovec structures to remove the already sent bytes!
		// remove n bytes!
		for(unsigned int i = 0; i < (unsigned)next_message->msg_iovlen; i++) {
			if(next_message->msg_iov[i].iov_len <= (unsigned)n) {
				// this iov is complete!
				n -= next_message->msg_iov[i].iov_len;
				next_message->msg_iov[i].iov_len = 0;
				if(n == 0)
					break;
			} else {
				// this iov is only partly completed!
				next_message->msg_iov[i].iov_base = (iov_base_t)((char*)next_message->msg_iov[i].iov_base + n);
				next_message->msg_iov[i].iov_len -= n;
				break;
			}
		}
	}
	
	if(all_messages) {
		free(all_messages);
		free(all_message_to_sends);
	}
	if(c->svc->n_log_services) {
		uint32_t client_id = 0;
		uint32_t request_id = 0;
		if(c->request_hdr) {
			client_id = c->request_hdr->client_id;
			request_id = c->request_hdr->request_id;
		}
		if(_ln_log_service_event(c->svc, LOG_SERVICE_RESPONSE, completion_time, c->request_time,
					 c->peer_address, client_id, request_id, &c->mgr))
			ln_debug(c->svc->clnt, "failed to log service responsev!\n");
	}
	
	return 0;
}


int _wait_for_service_requests_top(service_group* sg, fd_set* read_fds, int max_fd) {
	ln_debug(sg->clnt, "_wait_for_service_requests_top group %s\n", check_null_str(sg->name));

#ifndef __WIN32__ // on win32 pipe are not allowed within select()
	FD_SET(sg->notification_pipe.input_fd, read_fds);
	if(sg->notification_pipe.input_fd > max_fd)
		max_fd = sg->notification_pipe.input_fd;
#endif
	
	for(unsigned int i = 0; i < sg->n_providers; i++) {
		service* s = sg->providers[i];
		if(!s->is_listening) // not a provider
			continue;

#ifndef __WIN32__
		if (s->new_provider_fd_notification_pipe.input_fd != ln_pipe_invalid) {
			FD_SET(s->new_provider_fd_notification_pipe.input_fd, read_fds);
			if(s->new_provider_fd_notification_pipe.input_fd > max_fd)
				max_fd = s->new_provider_fd_notification_pipe.input_fd;
		}
#endif		
		s->is_client_pending = 0;
		if(s->fd != -1) {
			FD_SET(s->fd, read_fds);
			if(s->fd > max_fd)
				max_fd = s->fd;
			ln_debug(s->clnt, "  listen on tcp fd %d for service %s\n", s->fd, s->name);
		}
			
		s->is_client_pending_unix = 0;
		if(s->unix_fd != -1) {
			FD_SET(s->unix_fd, read_fds);
			if(s->unix_fd > max_fd)
				max_fd = s->unix_fd;
			ln_debug(s->clnt, "  listen on unix fd %d for service %s\n", s->unix_fd, s->name);
		}
			
		for(unsigned int k = 0; k < s->n_service_clients; k++) {
			service_client* c = s->service_clients[k];
			if(c->fd == -1)
				continue;
			if(c->is_busy)
				continue;
			c->is_pending = 0;
			FD_SET(c->fd, read_fds);
			if(c->fd > max_fd)
				max_fd = c->fd;
			ln_debug(s->clnt, "  listen on client fd %d for service requests %s\n", c->fd, s->name);
		}
	}
	return max_fd;
}

int _wait_for_service_requests_bottom(service_group* sg, fd_set* read_fds) {
	int pending = 0;

#ifndef __WIN32__ // on win32 pipe are not allowed within select()
	if(FD_ISSET(sg->notification_pipe.input_fd, read_fds)) {
		ln_debug(sg->clnt, "sg %s received notification!\n", sg->name);
		ln_pipe_read_notification(sg->notification_pipe);
	}
#endif
	/*
	if(sg->clnt->debug) 
		ln_debug(sg->clnt, "%s: n_providers: %d\n", 
			 __func__, sg->n_providers);
	*/
	
	for(unsigned int i = 0; i < sg->n_providers; i++) {
		service* s = sg->providers[i];
		if(!s->is_listening) // not a provider
			continue;
		pending += s->n_fake_service_clientss;

		/*
		if(s->clnt->debug) 
			ln_debug(s->clnt, "%s: provider: %s, have valid provider notification fd: %d, is_set: %d\n", 
				 __func__, s->name,
				 s->new_provider_fd_notification_pipe.input_fd != ln_pipe_invalid,
				 (s->new_provider_fd_notification_pipe.input_fd != ln_pipe_invalid) && (FD_ISSET(s->new_provider_fd_notification_pipe.input_fd, read_fds))
				);
		*/
		
		if(s->new_provider_fd_notification_pipe.input_fd != ln_pipe_invalid
		   && FD_ISSET(s->new_provider_fd_notification_pipe.input_fd, read_fds)) {
			if(s->clnt->debug) ln_debug(s->clnt, "%s: got new_provider_notification!\n", __func__);
			ln_pipe_read_notification(s->new_provider_fd_notification_pipe);
			if(s->fd != -1 && !s->notified_tcp_fd) {
				if(s->clnt->debug) ln_debug(s->clnt, "%s: add provider tcp fd %d\n", __func__, s->fd);
				_ln_service_add_provider_fd(s, s->fd);
				s->notified_tcp_fd = true;
				pending += 1;
			}
			if(s->unix_fd != -1 && !s->notified_unix_fd) {
				if(s->clnt->debug) ln_debug(s->clnt, "%s: add provider unix fd %d\n", __func__, s->unix_fd);
				_ln_service_add_provider_fd(s, s->unix_fd);
				s->notified_unix_fd = true;
				pending += 1;
			}
		}
		if(s->fd != -1 && FD_ISSET(s->fd, read_fds)) {
			s->is_client_pending = 1;
			pending++;			
		}
		
		if(s->unix_fd != -1 && FD_ISSET(s->unix_fd, read_fds)) {
			s->is_client_pending_unix = 1;
			pending++;
		}
		
		for(unsigned int k = 0; k < s->n_service_clients; k++) {
			service_client* c = s->service_clients[k];
			if(c->fd == -1)
				continue;
			if(FD_ISSET(c->fd, read_fds)) {
				c->is_pending = 1;
				pending ++;
			}
		}
	}
	return pending;
}

int _ln_wait_for_service_group_requests(service_group* sg, double timeout) {
	fd_set read_fds;
	FD_ZERO(&read_fds);
	int max_fd = _wait_for_service_requests_top(sg, &read_fds, 0);

	struct timeval* tvp = NULL;
	struct timeval tv;
#ifdef __WIN32__ // on win32 pipe are not allowed within select()
	timeout = sg->clnt->win32_wait_for_service_request_timeout;
#endif

	ln_debug(sg->clnt, "_ln_wait_for_service_group_requests group %s calling select max_fd: %d, to %f\n",
		 check_null_str(sg->name), max_fd, timeout);
#ifdef __WIN32__
	if(timeout >= 0) {
		tv.tv_sec = (int)timeout;
		tv.tv_usec = (int)((timeout - (int)timeout) * 1e6);
		tvp = &tv;
	}
	int n = 0;
	if(max_fd == 0)
		Sleep((unsigned int)(sg->clnt->win32_wait_for_service_request_timeout * 1000));
	else
		n = select(max_fd, &read_fds, NULL, NULL, tvp);
#else
	double abs_timeout;
	if(timeout >= 0) {
		tvp = &tv;
		abs_timeout = ln_get_monotonic_time() + timeout;
	}
	int n;
	while(1) {
		if(tvp != NULL) {
			double rel_timeout = abs_timeout - ln_get_monotonic_time();
			if(rel_timeout < 0)
				rel_timeout = 0;
			tv.tv_sec = (int)rel_timeout;
			tv.tv_usec = (int)((rel_timeout - tv.tv_sec) * 1e6);
		}
		n = select(max_fd + 1, &read_fds, NULL, NULL, tvp);
		if(n != -1)
			break;
		if(errno != EINTR)
			break;
	}
#endif
	if(n == -1)
		return -LNE_CHECK_ERRNO;
	
	if(!n) {
		if(!sg->clnt->had_new_fake_service_client) // really?
			return 0;
		sg->clnt->had_new_fake_service_client = 0;
		n = 1;
	}

	_wait_for_service_requests_bottom(sg, &read_fds);
	
	return n;

}

void __ln_service_client_handle_event(pool_thread* thread, void* data) {
	service_client* c = (service_client*) data;
	int is_async = thread != NULL;
	if(is_async) {
		c->thread = thread;
		{
			char thread_name[128];
			snprintf(thread_name, 128, "ln:sc:%s", c->svc->name);
			ln_thread_setname(thread_name);
		}
	}
	int ret = _ln_service_client_handle_request(c);
	if(is_async && c->fd == -1) {
		// destroy client
		service* s = c->svc;
		ln_mutex_lock(&s->mutex);
		_ln_service_remove_service_client(s, c); // CSA: no, c is not dereferenced in this method!
		ln_mutex_unlock(&s->mutex);
		_ln_service_client_destroy(c);
		return;
	}
	c->handler_return = ret;
	c->is_busy = 0;
	if(is_async) {
		ln_debug(c->svc->clnt, "__ln_service_client_handle_event notify service group!\n");
		ln_pipe_send_notification(c->svc->group->notification_pipe);
	}
}


int _ln_service_client_handle_event(service_group* sg, service_client* c) {	
	c->is_busy = 1;
	if(!sg->pool || sg->pool->max_n_threads == 1) {
		// synchronous execution!
		__ln_service_client_handle_event(NULL, c);
		if(c->fd == -1) {
			// destroy client
			service* s = c->svc;
			_ln_service_remove_service_client(s, c); // CSA: no, c is not dereferenced in this method!
			_ln_service_client_destroy(c);
			return 0;
		}
		return c->handler_return;
	}
	// request thread from pool
	return thread_pool_request_thread(sg->pool, __ln_service_client_handle_event, c, NULL);
}

int _ln_handle_service_group_requests(service_group* sg) {
	int n_events = 0;
	int ret;
	for(unsigned int i = 0; i < sg->n_providers; i++) {
		service* s = sg->providers[i];
		if(!s->is_listening) // not a provider
			continue;
		if(ln_mutex_trylock(&s->mutex)) {
			ln_sleep_seconds(0.010); // do not burn cpu here
			continue; // locked
		}
		if(s->is_client_pending) {
			n_events ++;
			s->is_client_pending = 0;
			ret = _ln_service_accept_client_tcp(s);
			ln_debug(s->clnt, "_ln_service_accept_client_tcp returned %d\n", ret);
			if(ret) {
				ln_mutex_unlock(&s->mutex);
				return ret;
			}
		}
		if(s->is_client_pending_unix) {
			n_events ++;
			s->is_client_pending_unix = 0;
			ret = _ln_service_accept_client_unix(s);
			ln_debug(s->clnt, "_ln_service_accept_client_unix returned %d\n", ret);
			if(ret) {
				ln_mutex_unlock(&s->mutex);
				return ret;
			}
		}
		for(int k = (int)s->n_service_clients - 1; k >= 0; k--) {
			service_client* c = s->service_clients[k];
			if(c->fd == -1)
				continue;
			if(c->is_pending) {
				c->is_pending = 0;
				ln_debug(s->clnt, "call _ln_service_client_handle_event\n");
				ret = _ln_service_client_handle_event(sg, c);
				if(ret) {
					_ln_set_error(sg->clnt, "error in service request handling '%s': %d %s!", s->name, errno, strerror(errno));
					ln_mutex_unlock(&s->mutex);
					return ret;
				}
			}
		}
		for(int k = (int)s->n_fake_service_clientss - 1; k >= 0; k--) {
			service_client* c = s->fake_service_clientss[k];
			c->is_pending = 0;
			ret = _ln_service_client_handle_request(c);
			// destroy client
			_ln_service_client_destroy(c);
			_ln_service_remove_fake_service_clients(s, c); // CSA: no, c is not dereferenced in this method!
			if(ret) {
				_ln_set_error(sg->clnt, "error in fake service request handling '%s': %d %s!", s->name, errno, strerror(errno));
				return ret;
			}
		}
		ln_mutex_unlock(&s->mutex);
	}
	return n_events;
}

int ln_wait_and_handle_service_group_requests(ln_client clnt_, const char* group_name, double timeout) {
	client* clnt = (client*)clnt_;
	lock_client(clnt);
	service_group* sg = find_service_group(clnt, group_name);
	unlock_client(clnt);
	if(!sg) {
		_ln_set_error(clnt, "error in service request handling: unknown service_group '%s'!", check_null_str(group_name));
		return -LNE_SVC_GRP_UNKNOWN;
	}
	
	int ret = _ln_wait_for_service_group_requests(sg, timeout);
	if(ret < 1)
		return ret;

	ret = _ln_handle_service_group_requests(sg);
	if(ret < 0)
		return ret;
	return 0;
}

// service clients
int _ln_service_accept_client_at_fd(service* s, int client_fd, struct sockaddr* client_addr, os_socklen_t client_addr_len) {
	// spawn new client!
	service_client* c = (service_client*)calloc(1, sizeof(service_client));
	c->svc = s;
	c->fd = client_fd;
	c->is_pending = 1; // assume request is pending after connect!
	c->client_addr = client_addr;
	c->client_addr_len = client_addr_len;

	if(client_addr->sa_family == AF_UNIX) {
		// struct sockaddr_un* peer = (struct sockaddr_un*)client_addr;
		// linux does not transmit peer name socket
		c->peer_address = strdup(s->unix_socket_name);
	} else {
		struct sockaddr_in* peer = (struct sockaddr_in*)client_addr;
		const unsigned int peer_ip_len = INET6_ADDRSTRLEN + 7;
		char peer_ip[INET6_ADDRSTRLEN + 7];
		if(inet_ntop(client_addr->sa_family, &peer->sin_addr, peer_ip, peer_ip_len)) {
			unsigned int l = strlen(peer_ip);
			snprintf(peer_ip + l, peer_ip_len - l, ":%d", ntohs(peer->sin_port));
			c->peer_address = strdup(peer_ip);
		}
	}
	if(!c->peer_address)
		c->peer_address = strdup("<unknoewn peer address>");
	
	_ln_iovec_mgr_init(&c->mgr);
	_ln_aux_mem_init(&c->aux_mem);

	_ln_service_append_service_client(s, c);
	_ln_service_add_client_fd(c);
	
	return 0;
}

int _ln_service_accept_client(service* s) {
	return _ln_service_accept_client_tcp(s);
}
int _ln_service_accept_client_tcp(service* s) {
	if(s->fd == -1) {
		ln_debug(s->clnt, "_ln_service_accept_client_tcp(service %p, name=%s) -- unix listening socket not initialized!\n", s, s->name);
		return -LNE_CLIENT_NOT_INITIALIZED;
	}

	os_socklen_t client_addr_len = sizeof(struct sockaddr_in);
	struct sockaddr_in* client_addr = (struct sockaddr_in*)calloc(1, client_addr_len);
	int client_fd;
	UNTIL_RET_NOT_EINTR(client_fd, accept(s->fd, (struct sockaddr*)client_addr, &client_addr_len));
	if(client_fd == -1) {
		free(client_addr);
		ln_debug(s->clnt, "_ln_service_accept_client_tcp: accept: errno: %d, strerror: %s\n", errno, strerror(errno));
		return -LNE_CHECK_ERRNO;
	}

	return _ln_service_accept_client_at_fd(s, client_fd, (struct sockaddr*)client_addr, client_addr_len);
}
int _ln_service_accept_client_unix(service* s) {
#if !defined(HAVE_AF_UNIX)
	return -LNE_NOT_ON_THIS_OS;
#else
	if(s->unix_fd == -1) {
		ln_debug(s->clnt, "_ln_service_accept_client_unix(service %p, name=%s) -- unix listening socket not initialized!\n", s, s->name);
		return -LNE_CLIENT_NOT_INITIALIZED;
	}

	os_socklen_t client_addr_len = sizeof(struct sockaddr_un);
	struct sockaddr_un* client_addr = (struct sockaddr_un*)calloc(1, client_addr_len);
	int client_fd;
	UNTIL_RET_NOT_EINTR(client_fd, accept(s->unix_fd, (struct sockaddr*)client_addr, &client_addr_len));
	if(client_fd == -1) {
		free(client_addr);
		ln_debug(s->clnt, "_ln_service_accept_client_unix: accept: errno: %d, strerror: %s\n", errno, strerror(errno));
		return -LNE_CHECK_ERRNO;
	}
	return _ln_service_accept_client_at_fd(s, client_fd, (struct sockaddr*)client_addr, client_addr_len);
#endif
}

ssize_t full_recv(client* clnt, int fd, void* buffer_in, ssize_t len) {
	/*
	  ignore signals,
	  chunked read until len is reached -> return len
	  or EOF or error -> return 0 or -LNE_CHECK_ERRNO
	 */
	char* buffer = (char*) buffer_in;
	ssize_t already_received = 0;
	while(already_received < len) {
		ssize_t n = recv(fd, buffer + already_received, len - already_received, 0);
		if(clnt->debug)
			ln_debug(clnt, "recv(fd %d, buffer %p, len %d) returned %d\n",
			      fd, buffer + already_received, len - already_received, n);
		
		int not_connreset = 0;
#ifndef __WIN32__		
		not_connreset = errno != ECONNRESET;
#else
		int last_error = WSAGetLastError();
		if(clnt->debug)
			ln_debug(clnt, "win32 last_error: %d 0x%x\n", last_error, last_error);
		not_connreset = !(last_error == WSAECONNABORTED || last_error == WSAECONNRESET || last_error == WSAETIMEDOUT);
#endif
		if(clnt->debug)
			ln_debug(clnt, "not_connreset: %d\n", not_connreset);

		if(n == 0 || (n == -1 && !not_connreset)) { // disconnect!
			ln_debug(clnt, "full_recv() returns 0 to signal closed connection!\n");
			return 0; // no error!
		}
		if(n == -1) {
			if(errno == EINTR) // interrupted system call
				continue;
			return -LNE_CHECK_ERRNO;
		}
		already_received += n;
		if(clnt->debug)
			ln_debug(clnt, "received %d of %d bytes\n", already_received, len);
	}
	return already_received;
}

int _ln_service_client_handle_request(service_client* c) {
	// receive request
	uint32_t req_size = 0;
	ssize_t n;

	// printf("handle request receive buffer: %p, len: %d\n", c->buffer, c->buffer_size);
	
	if(!c->is_fake) {
		ln_debug(c->svc->clnt, "service_client_handle_request for client %p, service %p, thread %#x\n", c, c->svc, get_thread_id());
		n = full_recv(c->svc->clnt, c->fd, &req_size, sizeof(req_size));
		if(n < 0) // error
			return -LNE_SVC_RECV_REQ;
		if(n == 0) {
			// client disconnected!
			ln_debug(c->svc->clnt, "assume client disconnected!\n");
			_ln_service_remove_client_fd(c);
			return 0; // no error!		
		}
		req_size -= sizeof(req_size);

		if(req_size > c->buffer_size) {
			char* new_buffer = (char*)realloc(c->buffer, req_size);
			if(!new_buffer)
				return -LNE_SVC_RECV_REQ_MEM;
			c->buffer = new_buffer;
			c->buffer_size = req_size;
		}
	}
	
	if(req_size) {
		n = full_recv(c->svc->clnt, c->fd, c->buffer, req_size);
		if(n < 0) // error
			return -LNE_SVC_RECV_REQ;
		if(n == 0) {
			// client disconnected!
			_ln_service_remove_client_fd(c);
			return 0; // no error!		
		}
		c->buffer_len = req_size;
	}
	if(c->svc->clnt->manager_library_version >= 13) {
		// new manager, we expect a version header!!
		c->request_hdr = (service_request_header*)c->buffer;
		c->user_buffer = (char*)(c->request_hdr + 1);
		c->user_buffer_len = c->buffer_len - sizeof(service_request_header);
		/*
		printf("ver: %d, client: %d, request: %d\n",
		       c->request_hdr->version,
		       c->request_hdr->client_id,
		       c->request_hdr->request_id);
		*/
	} else {
		// old manager, old client requesting...
		c->request_hdr = NULL;
		c->user_buffer = c->buffer;
		c->user_buffer_len = c->buffer_len;
	}
	
	//print_hex("recv'D request: ", c->buffer, req_size);

	if(c->svc->log_services)
		c->request_time = ln_get_time();
		
	// call handler
	int ret = 0;
	c->did_respond = 0;
	c->request_error = 0;
	c->service_data = NULL;
	if(c->svc->handler) {
		ret = c->svc->handler(c->svc->clnt, c, c->svc->handler_user_data);
	} else
		ret = -1;

	if(c->did_respond || c->fd == -1 || c->is_fake)
		return 0;

	if(ret == -LNE_SVC_RESP_LATER) {
		ln_debug(c->svc->clnt, "service handler wants to respond later to this service request!\n");
		return 0; 
	}
	
	// no response sent yet - always send response!!!
	if(ret == 0) {
		// ret is assumed to be 0 -> handler returned success but did not send response -> programming error in provider!
		// send error response
		c->request_error = -LNE_SVC_HANDLER_NO_RESP;
		ln_service_request_respond(c);
		return -LNE_SVC_HANDLER_NO_RESP;
	}

	// send error response
	c->request_error = ret;
	ln_service_request_respond(c);
	return 0; // normal exit for provider
}

int _ln_service_client_destroy(service_client* c) {
	ln_debug(c->svc->clnt, "destroying service client %p\n", c);
	if(c->fd != -1)
		_ln_close_socket(c->fd);
	if(c->peer_address)
		free(c->peer_address);
	free(c->buffer);
	free(c->mgr.msg.msg_iov);
	_ln_aux_mem_free(&c->aux_mem);
	if(c->client_addr)
		free(c->client_addr);	
	free(c);
	return 0;
}
int _ln_fake_service_clients_destroy(service_client* c) {
	return _ln_service_client_destroy(c);
}

int _ln_async_service_call_destroy(service* c) {
	// nothing allocated for an async-service call
	return 0;
}

int ln_get_service_signature(ln_client clnt_, const char* service_interface, char** definition, char** signature) {
	client* clnt = (client*)clnt_;

	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	request_t r;
	int ret = _ln_init_request(
		clnt, &r,
		"method", "r", "get_service_signature",
		"interface", "r", service_interface,
		NULL);
	if(ret)
		return ret;

	ln_mutex_lock(&clnt->communication_mutex);
	ret = _ln_request_and_wait(&r);
	ln_mutex_unlock(&clnt->communication_mutex);
	if(ret) {
		_ln_destroy_request(&r);
		return ret;
	}

	if((ret = _ln_get_from_request(&r, "definition", "S", definition))) {
		_ln_destroy_request(&r);
		return ret;
	}

	if((ret = _ln_get_from_request(&r, "signature", "S", signature))) {
		_ln_destroy_request(&r);
		return ret;
	}

	_ln_destroy_request(&r);

	return 0;
}

int ln_get_service_signature_for_service(ln_client clnt_, const char* service_name, char** service_interface, char** definition, char** signature) {
	client* clnt = (client*)clnt_;

	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	request_t r;
	int ret = _ln_init_request(
		clnt, &r,
		"method", "r", "get_service_signature_for_service",
		"name", "r", service_name,
		NULL);
	if(ret)
		return ret;

	ln_mutex_lock(&clnt->communication_mutex);
	ret = _ln_request_and_wait(&r);
	ln_mutex_unlock(&clnt->communication_mutex);
	if(ret) {
		_ln_destroy_request(&r);
		return ret;
	}

	if((ret = _ln_get_from_request(&r, "interface", "S", service_interface))) {
		_ln_destroy_request(&r);
		return ret;
	}

	if((ret = _ln_get_from_request(&r, "definition", "S", definition))) {
		_ln_destroy_request(&r);
		return ret;
	}

	if((ret = _ln_get_from_request(&r, "signature", "S", signature))) {
		_ln_destroy_request(&r);
		return ret;
	}

	_ln_destroy_request(&r);

	return 0;

}

service_group* find_service_group(client* clnt, const char* group_name) {
	for(unsigned int i = 0; i < clnt->n_service_groups; i++) {
		service_group* group = clnt->service_groups[i];
		if(group_name == NULL) {
			if(group->name == NULL)
				return group;
			continue;
		}
		if(group->name == NULL)
			continue;
		if(!strcmp(group->name, group_name))
			return group;
	}
	return NULL;
}

service_group* _ln_add_service_group(client* clnt, const char* group_name) {
	service_group* found_group = (service_group*)calloc(1, sizeof(service_group));
	found_group->clnt = clnt;
	if(group_name)
		found_group->name = strdup(check_null_str(group_name));
	if(ln_pipe_init(&found_group->notification_pipe))
		ln_debug(clnt, "sg %s failed to init notification pipe!\n", check_null_str(found_group->name));

	_ln_client_append_service_group(clnt, found_group);
	return found_group;
}

int _ln_service_group_add(const char* group_name, service* s) {
	// does this group already exist?
	client* clnt = s->clnt;
	service_group* found_group = find_service_group(clnt, group_name);	
	if(!found_group) // create new group with this name
		found_group = _ln_add_service_group(clnt, group_name);
	// remove service from other group if its already in one
	if(s->group && s->group != found_group)
		_ln_service_group_remove(s);
		
	// add service to this group if it is not already in
	s->group = found_group;
	for(unsigned int i = 0; i < found_group->n_providers; i++) {
		if(found_group->providers[i] == s)
			return 0; // already in
	}
	_ln_service_group_append_provider(found_group, s);
	return 0;
}

int _ln_service_group_remove(service* s) {
	if(!s->group)
		return 0;
	
	_ln_service_group_remove_provider(s->group, s);
	s->group = NULL;
	return 0;
}

int _ln_provider_destroy(service* s) {
	s->group = NULL;
	return 0;
}

DEFINE_LIST_METHODS_TYPE_NAME(service_group, service, provider);
int _ln_service_group_destroy(service_group* sg) {
	if(sg->pool && sg->thread) {
		ln_debug(sg->clnt, "ln_service_group_destroy notify!\n");
		ln_pipe_send_notification(sg->notification_pipe);
		_ln_stop_pool_thread(sg->thread);
	}
	DESTRUCT_LIST_TYPE_NAME(sg, service, provider);
	if(sg->name)
		free(sg->name);
	
	ln_pipe_close(&sg->notification_pipe);
	free(sg);
	return 0;
}

void service_group_thread(pool_thread* thread, void* data) {
	service_group* sg = (service_group*)data;

	{
		char thread_name[128];
		snprintf(thread_name, 128, "ln:sg:%s", check_null_str(sg->name));
		ln_thread_setname(thread_name);
	}
	ln_debug(thread->pool->clnt, "handle service group %s in tid %d\n", check_null_str(sg->name), get_thread_id());
	
	while(thread->keep_running && sg->pool != NULL) {
		int ret = _ln_wait_for_service_group_requests(sg, .5);
		ln_debug(thread->pool->clnt, "_ln_wait_for_service_group_requests group %s returned %d\n", check_null_str(sg->name), ret);
		if(!thread->keep_running)
			break;
		if(ret == 0)
			continue;
		
		if(ret < 0) {
			fprintf(stderr, "libln error: service_group_thread %s: wait_for_service_group_requests: %s\n",
				check_null_str(sg->name), check_null_str(sg->clnt->error_message));
			break;
		}

		ret = _ln_handle_service_group_requests(sg);
		if(ret < 0) {
			fprintf(stderr, "libln error: service_group_thread %s: handle_service_group_requests: %s\n",
				check_null_str(sg->name), check_null_str(sg->clnt->error_message));
			break;
		}
	}
	// wait for all handler threads to return!
	for(unsigned int i = 0; i < sg->n_providers; i++) {
		service* s = sg->providers[i];
		if(!s->is_listening) // not a provider
			continue;
		ln_mutex_lock(&s->mutex);
		for(int k = (int)s->n_service_clients - 1; k >= 0; k--) {
			service_client* c = s->service_clients[k];
			if(c->is_busy == 0)
				continue;
			pool_thread* thread = c->thread;
			if(!thread)
				continue;
			ln_debug(s->clnt, "wait for thread %p...\n", thread);
			ln_semaphore_wait(thread->in_use);
			ln_debug(s->clnt, "got thread %p...\n", thread);
			ln_semaphore_post(thread->in_use);
		}
		ln_mutex_unlock(&s->mutex);
	}
	ln_debug(sg->clnt, "stopping service_group_thread for sg %s!\n", check_null_str(sg->name));
	
	sg->pool = NULL; // leave thread pool?
}

int ln_remove_service_group_from_thread_pool(ln_client clnt_, const char* group_name)
{
	client* clnt = (client*)clnt_;

	lock_client(clnt);
	service_group* sg = find_service_group(clnt, group_name);
	unlock_client(clnt);
	if(!sg || !sg->pool) // group does not exist -> done, no assigned pool -> done
		return 0;


	ln_debug(sg->clnt, "ln_remove_service_group_from_thread_pool\n");
	sg->pool = NULL;
	ln_pipe_send_notification(sg->notification_pipe);

	_ln_wait_for_pool_thread_idle(sg->thread);
	sg->thread = NULL;
	ln_debug(sg->clnt, "ln_remove_service_group_from_thread_pool thread done\n");

	return 0;

}

int ln_handle_service_group_in_thread_pool(ln_client clnt_, const char* group_name, const char* pool_name) {
	client* clnt = (client*)clnt_;
	lock_client(clnt);
	service_group* sg = find_service_group(clnt, group_name);
	if(!sg) // create new group with this name
		sg = _ln_add_service_group(clnt, group_name);
	unlock_client(clnt);
	if(!sg) {
		_ln_set_error(clnt, "error: failed to create service_group '%s'!",
			      check_null_str(group_name));
		return -LNE_SVC_GRP_UNKNOWN;
	}

	lock_client(clnt);
	thread_pool* new_pool = get_thread_pool(clnt, pool_name);
	unlock_client(clnt);
	
	if(sg->pool && sg->pool != new_pool) {
		_ln_set_error(clnt, "error: service_group '%s' is already in thread_pool '%s'! todo?",
			      check_null_str(group_name), check_null_str(sg->pool->name));
		return -LNE_SVC_GRP_UNKNOWN;
	}
	
	if(sg->pool != new_pool) {
		sg->pool = new_pool;
		thread_pool_request_thread(sg->pool, service_group_thread, sg, &sg->thread);
	}
	
	return 0;
}

DEFINE_LIST_METHODS(service, log_service);
int _ln_service_add_log_service(service* s, log_service* ls) {
	for(unsigned int i = 0; i < s->n_log_services; i++) {
		log_service* op = s->log_services[i];
		if(op == ls)
			return 0;
	}
	return _ln_service_append_log_service(s, ls);
}
int _ln_service_del_log_service(service* s, log_service* ls) {
	int ret = _ln_service_remove_log_service(s, ls);
	ln_debug(s->clnt, "_ln_service_del_log_service %d log services left in %s\n", s->n_log_services, s->name);
	return ret;
}

#include "ln/packed.h"
typedef struct LN_PACKED {
	uint32_t error_message_len;
	char* error_message;
} log_service_response_t;
#include "ln/endpacked.h"

int _ln_log_service_event(service* s,
			  log_service_item_type_t item_type,
			  double completion_time,
			  double request_time,
			  const char* peer_address,
			  uint32_t client_id,
			  uint32_t request_id,
			  struct _ln_iovec_mgr* data) {
	
	double transfer_time = ln_get_time() - completion_time;

	if(s->clnt->debug != 0)
		ln_debug(s->clnt,
			 "log_service event:\n"
			 "  service: %s\n"
			 "  item_type: %d\n"
			 "  client_id: %d, request_id: %d\n"
			 "  completion_time: %.3fs\n"
			 "  request_time: %.3fs\n"
			 "  transfer_time: %.3fs\n"
			 "  peer_address: %s\n"
			 "  iovec-items: %d\n"
			 "  n_bytes: %d\n",
			 
			 s->name,
			 item_type,
			 client_id,
			 request_id,
			 completion_time,
			 request_time,
			 transfer_time,
			 peer_address,
			 data->msg.msg_iovlen,
			 data->n_bytes);

	// assemble iovec to send as request

	for(unsigned int lsi = 0; lsi < s->n_log_services; lsi++) {
		log_service* ls = s->log_services[lsi];
		
		ln_mutex_lock(&ls->mutex);

#define add_single_value(variable) _ln_iovec_mgr_add(&ls->mgr, &variable, sizeof(variable), sizeof(variable)); *(element_len++) = sizeof(variable);
#define add_string(variable, len_var) uint32_t len_var = strlen(variable); add_single_value(len_var); _ln_iovec_mgr_add(&ls->mgr, variable, len_var, 1); *(element_len++) = 1;
	
		_ln_iovec_mgr_reset(&ls->mgr, NULL);
	
		unsigned int this_n_iovs = 17 + data->msg.msg_iovlen;
		if(!ls->iov_element_lens) {
			ls->iov_element_lens_size = this_n_iovs;
			ls->iov_element_lens = (uint8_t*)calloc(1, ls->iov_element_lens_size);
		} else if(ls->iov_element_lens_size < this_n_iovs) {
			ls->iov_element_lens_size = this_n_iovs;
			ls->iov_element_lens = (uint8_t*)realloc(ls->iov_element_lens, ls->iov_element_lens_size);
		}
		uint8_t* element_len = ls->iov_element_lens;
	
		add_string(s->clnt->program_name, client_name_len);
		add_string(s->name, service_name_len);
		add_string(s->interface_name, interface_name_len);

		uint8_t item_type_field = (uint8_t)item_type;
		add_single_value(item_type_field);

		uint8_t flags_field = 0;
		// flags_field |= 1 << 0; // todo: did s had to do endianess swap?
		if(item_type == 0 && s->send_request_id) // was it a request?
			flags_field |= 1 << 1; // data contains packet header
		add_single_value(flags_field);

		add_single_value(completion_time);
		add_single_value(transfer_time);
		add_single_value(request_time);

		const char* empty = "";
		if(!peer_address)
			peer_address = empty;
		add_string((char*)peer_address, peer_address_len);
		add_single_value(client_id);
		add_single_value(request_id);

		uint64_t thread_id = (uint64_t)gettid();
		add_single_value(thread_id);

		add_single_value(data->n_bytes);
		for(unsigned int i = 0; i < (unsigned)data->msg.msg_iovlen; i++) {
			struct iovec* iop = &data->msg.msg_iov[i];
			_ln_iovec_mgr_add(&ls->mgr, iop->iov_base, iop->iov_len, 1);
			*(element_len++) = 1;
		}
	
		// outputs:
		int ret;
		uint8_t need_swap = 0;
		char* response_buffer;
		uint32_t response_len;
		ret = ln_service_callv(ls->svc, ls->mgr.msg.msg_iov, ls->mgr.msg.msg_iovlen, ls->iov_element_lens, &response_buffer, &response_len, &need_swap);
		if(ret)
			ln_debug(s->clnt, "failed to callv() log_service: %d\n", ret);
		else {
			log_service_response_t* resp = (log_service_response_t*)response_buffer;
			if(resp->error_message_len)
				ln_debug(s->clnt, "error calling log_service:\n%*.*s\n", resp->error_message_len, resp->error_message_len, resp->error_message);
		}
	
		ln_mutex_unlock(&ls->mutex);
	}
	return 0;
}
