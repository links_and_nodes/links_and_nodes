/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef __LINUX__
#ifndef __ANDROID__

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <pthread.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <time.h>
#include <netdb.h>
#include <sys/socket.h> 

#include "os.h"
#include "ln/ln.h"
#include <time.h>

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/syscall.h>

// named semaphores:
int ln_semaphore_create(ln_semaphore_handle_t* handle, const char* name, int create) {
	unsigned int sem_open_flags = 0;
	mode_t old_mask;
	if(create) {
		sem_open_flags |= O_CREAT;
		old_mask = umask(0);
	}
	*handle = sem_open(name, sem_open_flags, 0777, 0);
	if(create == 1)
		umask(old_mask);
	if(*handle == SEM_FAILED)
		return -1;
	return 0;
}
int ln_semaphore_close(ln_semaphore_handle_t handle) {
	return sem_close(handle);
}
int ln_semaphore_unlink(const char* name) {
	return sem_unlink(name);
}
// unnamed semaphores:
int ln_semaphore_init(ln_semaphore_handle_t* sem, unsigned int value) {
	*sem = (sem_t*)malloc(sizeof(sem_t));
	return sem_init(*sem, 0, value);
}
int ln_semaphore_destroy(ln_semaphore_handle_t sem) {
	int ret = sem_destroy(sem);
	free(sem);
	return ret;
}

// common for semaphores:
int ln_semaphore_wait(ln_semaphore_handle_t handle) {
	int ret;
	UNTIL_RET_NOT_EINTR(ret, sem_wait(handle));
	return ret; // 0 on success
}
int ln_semaphore_trywait(ln_semaphore_handle_t handle) {
	int ret;
	UNTIL_RET_NOT_EINTR(ret, sem_trywait(handle));
	return ret;
}
int ln_semaphore_timedwait(ln_semaphore_handle_t handle, double seconds) {
	struct timespec ts;
	ln_double2ts(ln_get_time_sys() + seconds, &ts);
	int ret;
	UNTIL_RET_NOT_EINTR(ret, sem_timedwait(handle, &ts));
	return ret;
}
int ln_semaphore_post(ln_semaphore_handle_t handle) {
	return sem_post(handle);
}
int ln_semaphore_getvalue(ln_semaphore_handle_t handle) {
	int val;
	int ret = sem_getvalue(handle, &val);
	if(ret == -1)
		return -1;
	return val;
}


int ln_mutex_create(ln_mutex_handle_t* mutex_handle) {
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutexattr_setprotocol(&attr, PTHREAD_PRIO_INHERIT);
	int ret = pthread_mutex_init(mutex_handle, &attr);
	if(ret != 0) {
            fprintf(stderr, "warning: pthread_mutex_init with protocol PTHREAD_PRIO_INHERIT failed with error code %d (%s),"\
		            " try again with PTHREAD_PRIO_NONE\n", ret, strerror(ret));
            pthread_mutexattr_setprotocol(&attr, PTHREAD_PRIO_NONE);
            ret = pthread_mutex_init(mutex_handle, &attr);
	}
	pthread_mutexattr_destroy(&attr);
	return ret;
}

int ln_mutex_destroy(ln_mutex_handle_t* mutex_handle) {
	return pthread_mutex_destroy(mutex_handle);
}

int ln_mutex_lock(ln_mutex_handle_t* mutex_handle) {
	//printf("lock %#x\n", mutex_handle);
	return pthread_mutex_lock(mutex_handle);
}

int ln_mutex_trylock(ln_mutex_handle_t* mutex_handle) {
	int ret = pthread_mutex_trylock(mutex_handle);
	//printf("trylock %#x, returned: %d, %s\n", mutex_handle, ret, strerror(ret));
	return ret;
}

int ln_mutex_unlock(ln_mutex_handle_t* mutex_handle) {
	//printf("unlock %#x\n", mutex_handle);
	return pthread_mutex_unlock(mutex_handle);
}

int ln_thread_create(ln_thread_handle_t* thread_handle, thread_func_type* thread, void* data) {
	int ret = pthread_create(thread_handle, NULL, thread, data);
	if(!ret)
		return 0;
	errno = ret;
	return -LNE_CHECK_ERRNO;
}

int ln_thread_create_with_prio(ln_thread_handle_t* thread_handle, thread_func_type* thread, void* data, int policy, int priority, const char* name) {
	return ln_thread_create_with_params(thread_handle, thread, data, policy, priority, 0, name);
}

int ln_thread_create_with_params(ln_thread_handle_t* thread_handle, thread_func_type* thread, void* data, int policy, int priority, unsigned int affinity_mask, const char* name) {
	int ret;
	int explicit_sched = 0;
	pthread_attr_t attr;
	pthread_attr_init(&attr);

	if(policy != -1) {
		ret = pthread_attr_setschedpolicy(&attr, policy);
		if(ret)
			fprintf(stderr, "failed to set policy to %d: %d %s\n", policy, ret, strerror(ret));
		explicit_sched = 1;
	}
	
	if(priority != -1) {
		struct sched_param param;
		ret = pthread_attr_getschedparam(&attr, &param);
		if(ret)
			fprintf(stderr, "failed to get sched_param: %d %s\n", ret, strerror(ret));
		else {	
			param.sched_priority = priority;
			ret = pthread_attr_setschedparam(&attr, &param);
			if(ret)
				fprintf(stderr, "failed to set sched_param with prio %d: %d %s\n", priority, ret, strerror(ret));
		}
		explicit_sched = 1;
	}
	
	if(affinity_mask != 0) {
		cpu_set_t cpuset;
		CPU_ZERO(&cpuset);
		for(unsigned int cpu = 0; cpu < sizeof(affinity_mask) * 8; cpu++) {
			if(affinity_mask & 1)
				CPU_SET(cpu, &cpuset);
			affinity_mask >>= 1;
		}
		pthread_attr_setaffinity_np(&attr, sizeof(cpuset), &cpuset);
		explicit_sched = 1;
	}

	if(explicit_sched) {
		ret = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
		if(ret)
			fprintf(stderr, "failed to get sched_param: %d %s\n", ret, strerror(ret));
	}

	ret = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	if(ret)
		fprintf(stderr, "failed to set detachstate: %d %s\n", ret, strerror(ret));

	ret = pthread_create(thread_handle, &attr, thread, data);
	pthread_attr_destroy(&attr);
	if(!ret) {
		if(name) {
#ifndef NO_PTHREAD_SETNAME
			pthread_setname_np(*thread_handle, name);
#endif
		}
		return 0;
	}
	errno = ret;
	return -LNE_CHECK_ERRNO;
}

int ln_thread_setschedparam(ln_thread_handle_t thread_handle, int policy, int priority, unsigned int affinity_mask) {
	int ret;
	if(policy != -1 || priority != -1) {
		int current_policy;
		struct sched_param current_param;
		pthread_getschedparam(thread_handle, &current_policy, &current_param);

		if(policy == -1)
			policy = current_policy;

		if(priority != -1)
			current_param.sched_priority = priority;
		
		ret = pthread_setschedparam(thread_handle, policy, &current_param);
		if(ret) {
			fprintf(stderr, "failed to set policy to %d, priority to %d: %d %s\n", policy, current_param.sched_priority, ret, strerror(ret));
			errno = ret;
			return -LNE_CHECK_ERRNO;
		}
	}
	
	if(affinity_mask != 0) {
		cpu_set_t cpuset;
		CPU_ZERO(&cpuset);
		for(unsigned int cpu = 0; cpu < sizeof(affinity_mask) * 8; cpu++) {
			if(affinity_mask & 1)
				CPU_SET(cpu, &cpuset);
			affinity_mask >>= 1;
		}
		pthread_setaffinity_np(thread_handle, sizeof(cpuset), &cpuset);
	}
	return 0;
}


void ln_thread_cancel(ln_thread_handle_t t) {
	pthread_cancel(t);
}

int ln_thread_join(ln_thread_handle_t t, void** retval) {
	return pthread_join(t, retval);
}


int ln_thread_test_cancel(ln_thread_handle_t t) {
	pthread_testcancel();
	return 0;
}

double ln_ts2double(struct timespec* ts) {
	return (double)ts->tv_sec + (ts->tv_nsec / 1e9);
}

void ln_double2ts(double s, struct timespec* ts) {
	ts->tv_sec = (time_t)s;
	ts->tv_nsec = (long)((s - ts->tv_sec) * 1e9);
}

double ln_get_time() {
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	return ln_ts2double(&ts);
}
double ln_get_time_sys() {
	return ln_get_time();
}

double ln_get_monotonic_time() {
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return ln_ts2double(&ts);
}

void ln_sleep_seconds(double s) {
	struct timespec ts;
	ln_double2ts(s, &ts);
	nanosleep(&ts, NULL);
}

#ifndef USE_OWN_PROCESS_SHARED_IPC
int ln_process_shared_mutex_open(ln_process_shared_mutex_t* mutex, int create) {
	pthread_mutexattr_t attr;
	int ret;
	if(!create)
		return 0;
	if((ret = pthread_mutexattr_init(&attr)))
		return ret;
	if((ret = pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED))) // pshared!
		return ret;
	if((ret = pthread_mutexattr_setrobust_np(&attr, PTHREAD_MUTEX_ROBUST))) // if owner dies _lock() returns EOWNERDEAD
		return ret;
	if((ret = pthread_mutexattr_setprotocol(&attr, PTHREAD_PRIO_INHERIT)))
		return ret;	
	memset(mutex, 0, sizeof(*mutex));
	ret = pthread_mutex_init(mutex, &attr);
	if(ret != 0) {
            fprintf(stderr, "warning: pthread_mutex_init with protocol PTHREAD_PRIO_INHERIT failed with error code %d (%s),"\
		            " try again with PTHREAD_PRIO_NONE\n", ret, strerror(ret));
            pthread_mutexattr_setprotocol(&attr, PTHREAD_PRIO_NONE);
            ret = pthread_mutex_init(mutex, &attr);
	}
	if(ret)
		return ret;
	if((ret = pthread_mutexattr_destroy(&attr)))
		return ret;
	return 0;
}
int ln_process_shared_mutex_close(ln_process_shared_mutex_t* mutex, int destroy) {
	if(!destroy)
		return 0;
	return pthread_mutex_destroy(mutex);
}
int ln_process_shared_mutex_lock(ln_process_shared_mutex_t* mutex) {
	int ret = pthread_mutex_lock(mutex);
	if(ret == EOWNERDEAD) {
		printf("robust futex/mutex: owner died...\n");
		pthread_mutex_consistent_np(mutex);
	}
	return 0;
}
int ln_process_shared_mutex_unlock(ln_process_shared_mutex_t* mutex) {
	return pthread_mutex_unlock(mutex);
};

int ln_process_shared_cond_open(ln_process_shared_cond_t* cond, int create) {
	pthread_condattr_t attr;
	int ret;
	if(!create)
		return 0;
	if((ret = pthread_condattr_init(&attr)))
		return ret;
	if((ret = pthread_condattr_setpshared(&attr, PTHREAD_PROCESS_SHARED))) // pshared!
		return ret;
	if((ret = pthread_condattr_setclock(&attr, CLOCK_REALTIME)))
		return ret;
	if((ret = pthread_cond_init(cond, &attr)))
		return ret;
	if((ret = pthread_condattr_destroy(&attr)))
		return ret;
	return 0;
}
int ln_process_shared_cond_close(ln_process_shared_cond_t* cond, int destroy) {
	if(!destroy)
		return 0;
#if !defined(GLIBC_ATLEAST_2_25)
	// enforce non-blocking destroy. manager has to make sure that there are no waiter left!
	cond->__data.__nwaiters = 0;
	cond->__data.__mutex = (void *) 0;
#endif
	// return pthread_cond_destroy(cond);
	return 0; // ignore cond_destroy because it blocks if there was a client waiting on that condition which then got killed!
}
int ln_process_shared_cond_wait(ln_process_shared_cond_t* cond, ln_process_shared_mutex_t* mutex) {
	return pthread_cond_wait(cond, mutex);
}
int ln_process_shared_cond_timedwait(ln_process_shared_cond_t* cond, ln_process_shared_mutex_t* mutex, double abs_timeout) {
	struct timespec ts;
	ln_double2ts(abs_timeout, &ts);
	return pthread_cond_timedwait(cond, mutex, &ts);
}
int ln_process_shared_cond_broadcast(ln_process_shared_cond_t* cond) {
	return pthread_cond_broadcast(cond);
}
#endif

int ln_cond_create(ln_cond_handle_t* cond) {
	pthread_condattr_t attr;
	int ret;
	if((ret = pthread_condattr_init(&attr)))
		return ret;
	if((ret = pthread_condattr_setclock(&attr, CLOCK_MONOTONIC)))
		return ret;
	if((ret = pthread_cond_init(cond, &attr)))
		return ret;
	if((ret = pthread_condattr_destroy(&attr)))
		return ret;
	return 0;
}
int ln_cond_destroy(ln_cond_handle_t* cond) {
	return pthread_cond_destroy(cond);
}
int ln_cond_wait(ln_cond_handle_t* cond, ln_mutex_handle_t* mutex) {
	return pthread_cond_wait(cond, mutex);
}
int ln_cond_timedwait(ln_cond_handle_t* cond, ln_mutex_handle_t* mutex, double abs_timeout) {
	struct timespec ts;
	ln_double2ts(abs_timeout, &ts);
	return pthread_cond_timedwait(cond, mutex, &ts);
}
int ln_cond_broadcast(ln_cond_handle_t* cond) {
	return pthread_cond_broadcast(cond);
}
int ln_cond_signal(ln_cond_handle_t* cond) {
	return pthread_cond_signal(cond);
}


//! initialize os dependant pipe device
/*!
  \param reference to pipe device structure
  \return success or error code
  */
int ln_pipe_init(ln_pipe_t* p) {
	int* fds = &p->input_fd; // first int is read-end!

	if(pipe(fds))
		return -LNE_CHECK_ERRNO;

	return 0;
}

//! closes os dependant pipe device
/*!
  \param reference to pipe device structure
  \return success or error code
  */
int ln_pipe_close(ln_pipe_t* pipe) {
	close(pipe->input_fd);
	if (pipe->output_fd != -1)
		close(pipe->output_fd);

	return 0;
}

void ln_pipe_send_notification(ln_pipe_t pipe) {
	char n = '\n';
	write(pipe.output_fd, &n, 1);
}

void ln_pipe_read_notification(ln_pipe_t pipe) {
	char n = '\n';
	read(pipe.input_fd, &n, 1);
}

void set_thread_prio(int policy, int priority) {
	if(priority > 0 || policy != -1) {
		struct sched_param param;
		sched_getparam(0, &param);
		param.sched_priority = priority;
		if(policy == -1)
			policy = SCHED_OTHER;
		int ret = sched_setscheduler(0, policy, &param);
		if(ret) {
			fprintf(stderr, "warning: sched_setscheduler(policy=%d, priority=%d) failed with %d: %d/%s\n",
				policy, priority, ret, errno, strerror(errno));
			int minp = sched_get_priority_min(policy);
			int maxp = sched_get_priority_max(policy);
			fprintf(stderr, "allowerd priority range for this policy: from %d to %d\n", minp, maxp);
		}
	}
}

pid_t gettid() {
	return syscall( __NR_gettid );
}

void _ln_close_socket(_ln_socket_t fd) {
	close(fd);
}

void ln_thread_setname(const char* new_name) {
#ifndef NO_PTHREAD_SETNAME
	pthread_setname_np(pthread_self(), new_name);
#endif
}

#endif // not __ANDROID__

#endif // __LINUX__
