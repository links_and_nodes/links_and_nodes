/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <stdlib.h>

#include "ln/ln.h"

#ifdef USE_POSIX_SHM

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#ifdef __ANDROID__
#include <errno.h>
#include <linux/ashmem.h>

// unix domain sockets:
#include <sys/socket.h>
#include <sys/un.h>

// see 

// endlih ne antwort:
// http://stackoverflow.com/questions/14215462/how-to-create-a-android-native-service-and-use-binder-to-communicate-with-it

// https://github.com/gburca/BinderDemo


// http://elinux.org/Android_Kernel_Features#ashmem
// http://www.androidenea.com/2010/03/share-memory-using-ashmem-and-binder-in.html
// http://www.netmite.com/android/mydroid/system/core/libcutils/ashmem-dev.c


#define ASHMEM_DEVICE	"/dev/ashmem"

void aln_debug(const char* format, ...) {
	va_list ap;
	va_start(ap, format);
	__android_log_vprint(ANDROID_LOG_DEBUG, "ln_client", format, ap);
	vprintf(format, ap);
	va_end(ap);
}

/*
 * ashmem_create_region - creates a new ashmem region and returns the file
 * descriptor, or <0 on error
 *
 * `name' is an optional label to give the region (visible in /proc/pid/maps)
 * `size' is the size of the region, in page-aligned bytes
 */
int ashmem_create_region(const char *name, size_t size)
{
	int fd, ret;

	fd = open(ASHMEM_DEVICE, O_RDWR);
	if (fd < 0)
		return fd;

	if (name) {
		char buf[ASHMEM_NAME_LEN];

		strlcpy(buf, name, sizeof(buf));
		ret = ioctl(fd, ASHMEM_SET_NAME, buf);
		if (ret < 0)
			goto error;
	}

	ret = ioctl(fd, ASHMEM_SET_SIZE, size);
	if (ret < 0)
		goto error;

	return fd;

error:
	close(fd);
	return ret;
}

int ashmem_set_prot_region(int fd, int prot)
{
	return ioctl(fd, ASHMEM_SET_PROT_MASK, prot);
}

int ashmem_pin_region(int fd, size_t offset, size_t len)
{
	struct ashmem_pin pin = { offset, len };
	return ioctl(fd, ASHMEM_PIN, &pin);
}

int ashmem_unpin_region(int fd, size_t offset, size_t len)
{
	struct ashmem_pin pin = { offset, len };
	return ioctl(fd, ASHMEM_UNPIN, &pin);
}

pthread_mutex_t _unix_fd_mutex = PTHREAD_MUTEX_INITIALIZER;
int _unix_fd = -1;

int get_android_fd_for_shm(const char* name_, int* fd, unsigned int* shm_size) {
	int ret = 0;

	pthread_mutex_lock(&_unix_fd_mutex);

	const char* name = name_ + 1; // remove leading /

	if(_unix_fd == -1) {
		_unix_fd = socket(AF_UNIX, SOCK_STREAM, 0);
		if(_unix_fd == -1) {
			aln_debug("socket(AF_UNIX, SOCK_STREAM, 0): %d %s", errno, strerror(errno));
			ret = -1;
			goto android_fd_exit;
		}
		struct sockaddr_un unix_addr;
		memset(&unix_addr, 0, sizeof(unix_addr));
		unix_addr.sun_family = AF_UNIX;
		strcpy(unix_addr.sun_path, ANDROID_FD_UNIX_SOCKET);
		if(connect(_unix_fd, (struct sockaddr*)&unix_addr, sizeof(unix_addr)) == -1) {
			aln_debug("connect(unix_fd, %s): %d %s", ANDROID_FD_UNIX_SOCKET, errno, strerror(errno));
			ret = -1;
			goto android_fd_exit;
		}
	}
	if(strlen(name) > 1022) {
		aln_debug("name too long: %d chars, '%s'\n", strlen(name), name);
		ret = -1;
		goto android_fd_exit;
	}
	// 1022 chars, \0 and command-char

	char command[1024];
	command[0] = 0; // request shm fd
	strcpy(command + 1, name); // shm name

	ssize_t n;
	while(true) {
		n = send(_unix_fd, command, sizeof(command), 0);
		if(n < 1) {
			if(n == -1) {
				if(errno == EINTR)
					continue;
				aln_debug("error on unix client fd %d: %d %s - closing socket!\n", _unix_fd, errno, strerror(errno));
			}
			if(n == 0)
				aln_debug("EOF on unix client fd %d: closing socket!\n", _unix_fd);
			close(_unix_fd);
			_unix_fd = -1;
			ret = -1;
			goto android_fd_exit;
		}
		if(n != sizeof(command))
			aln_debug("error, sent only %d bytes over unix_fd %d\n", (int)n, _unix_fd);
		break;
	}
	aln_debug("sent request on unix fd %d: %d\n", _unix_fd, (int)n);

	uint8_t response;
	struct iovec iov[2];
	iov[0].iov_base = &response;
	iov[0].iov_len = sizeof(response);
	iov[1].iov_base = shm_size;
	iov[1].iov_len = sizeof(*shm_size);
	struct msghdr msg;
	memset(&msg, 0, sizeof(msg));
	msg.msg_iov = iov;
	msg.msg_iovlen = 2;
	char control_buffer[1024];
	msg.msg_control = control_buffer;
	msg.msg_controllen = sizeof(control_buffer);

	while(true) {
		n = recvmsg(_unix_fd, &msg, 0);
		if(n < 1) {
			if(n == -1) {
				if(errno == EINTR)
					continue;
				aln_debug("error on unix client fd %d: %d %s - closing socket!", _unix_fd, errno, strerror(errno));
			}
			if(n == 0)
				aln_debug("EOF on unix client fd %d: closing socket!", _unix_fd);
			close(_unix_fd);
			_unix_fd = -1;
			ret = -1;
			goto android_fd_exit;
		}
		break;
	}

	aln_debug("got response code %d, shm_size %d\n", response, *shm_size);
	if(!response) {
		aln_debug("got error from ln_daemon for shm name %s\n", name);
		ret = -1;
		goto android_fd_exit;
	}

	struct cmsghdr *cmsg;
	/* Receive auxiliary data in msgh */
	for(cmsg = CMSG_FIRSTHDR(&msg); cmsg != NULL; cmsg = CMSG_NXTHDR(&msg, cmsg)) {
		if(cmsg->cmsg_level == SOL_SOCKET && cmsg->cmsg_type == SCM_RIGHTS) {
			*fd = *(int *) CMSG_DATA(cmsg);
			break;
		}
	}
	if (cmsg == NULL) {
		aln_debug("no fd in response!\n");
		ret = -1;
	}

android_fd_exit:
	pthread_mutex_unlock(&_unix_fd_mutex);
	return ret;
}
#endif

int _fwrite_zeros(int fd, unsigned int size) {
	uint32_t word = 0;
	while(size >= sizeof(word)) {
		int ret = write(fd, &word, sizeof(word));
		if(ret != sizeof(word))
			return -1;
		size -= sizeof(word);
	}
	uint8_t byte = 0;
	while(size >= sizeof(byte)) {
		int ret = write(fd, &byte, sizeof(byte));
		if(ret != sizeof(byte))
			return -1;
		size -= sizeof(byte);
	}
	return 0;
}

int ln_shared_mem_create(ln_shared_mem_handle_t* data, const char* name, unsigned int size) {
	if(strlen(name) > MAX_SHARED_MEM_LEN)
		return -LNE_SHM_NAME_TOO_LONG;
	strncpy(data->name, name, MAX_SHARED_MEM_LEN);
	data->name[MAX_SHARED_MEM_LEN] = 0;
	data->size = size;

	int open_flags = O_RDWR | O_CREAT;
#ifndef __VXWORKS__
	mode_t old_mask = umask(0);
#endif

#ifdef __ANDROID__
	int fd;
	fd = ashmem_create_region(data->name, data->size); 
	data->fd = fd;
#else
	int fd = shm_open(data->name, open_flags, 0777);
#endif

#ifndef __VXWORKS__
	umask(old_mask);
#endif
	if(fd == -1) {
		fprintf(stderr, "could not shm_open('%s'): %d, %s\n", name, errno, strerror(errno));
		return -LNE_CHECK_ERRNO;
	}
#ifndef __ANDROID__
	// set size
	if(_fwrite_zeros(fd, data->size)) {
		fprintf(stderr, "could not create/fill shm at %d to size %d: %s\n", fd, data->size, strerror(errno));
		return -LNE_CHECK_ERRNO;
	}
#endif

	int mmap_flags = MAP_SHARED | MAP_POPULATE;
	
#ifndef __VXWORKS__
	struct rlimit mem_lock_limit;
	int ret = getrlimit(RLIMIT_MEMLOCK, &mem_lock_limit);
	if(ret) {
		fprintf(stderr, "could not getrlimit(RLIMIT_MEMLOCK)!\n");
		return -LNE_CHECK_ERRNO;
	}
	if(data->size < mem_lock_limit.rlim_cur)
		mmap_flags |= MAP_LOCKED;
#else
	mmap_flags |= MAP_LOCKED;
#endif
	
	// map memory
	data->mem = mmap(NULL, data->size, PROT_READ | PROT_WRITE, mmap_flags, fd, 0);

	if(data->mem == MAP_FAILED && (mmap_flags & MAP_LOCKED)) {
		// try again without MAP_LOCKED
		mmap_flags &= ~MAP_LOCKED;
		data->mem = mmap(NULL, data->size, PROT_READ | PROT_WRITE, mmap_flags, fd, 0);
	}
#ifndef __ANDROID__
	close(fd);
#endif
	if(data->mem == MAP_FAILED) {
		fprintf(stderr, "could not mmap!\n");
		return -LNE_CHECK_ERRNO;
	}
	return 0;
}

int ln_shared_mem_open(ln_shared_mem_handle_t* data, const char* name) {
	if(strlen(name) > MAX_SHARED_MEM_LEN)
		return -LNE_SHM_NAME_TOO_LONG;
	strncpy(data->name, name, MAX_SHARED_MEM_LEN);
	data->name[MAX_SHARED_MEM_LEN] = 0;

#ifdef __ANDROID__
	int fd;
	if(get_android_fd_for_shm(data->name, &fd, &data->size))
		return -LNE_ERROR_GETTING_SHM_FD;
	data->fd = fd;
#else
	int fd = shm_open(data->name, O_RDWR, 0777);
#endif
	if(fd == -1) {
		fprintf(stderr, "could not open shm: '%s'\n", name);
		return -LNE_CHECK_ERRNO;
	}
#ifndef __ANDROID__
	// get size
	struct stat sbuf;
	if(fstat(fd, &sbuf)) {
		fprintf(stderr, "could not stat fd %d\n", fd);
		return -LNE_CHECK_ERRNO;
	}
	data->size = (unsigned)sbuf.st_size;
#endif
	int mmap_flags = MAP_SHARED | MAP_POPULATE;
#ifndef __VXWORKS__
	struct rlimit mem_lock_limit;
	int ret = getrlimit(RLIMIT_MEMLOCK, &mem_lock_limit);
	if(ret) {
		fprintf(stderr, "could not getrlimit(RLIMIT_MEMLOCK)!\n");
		return -LNE_CHECK_ERRNO;
	}
	if(data->size < mem_lock_limit.rlim_cur)
		mmap_flags |= MAP_LOCKED;
#else	
	mmap_flags |= MAP_LOCKED;
#endif	

	// map memory
	data->mem = mmap(NULL, data->size, PROT_READ | PROT_WRITE, mmap_flags, fd, 0);

	if(data->mem == MAP_FAILED && (mmap_flags & MAP_LOCKED)) {
		// try again without MAP_LOCKED
		mmap_flags &= ~MAP_LOCKED;
		data->mem = mmap(NULL, data->size, PROT_READ | PROT_WRITE, mmap_flags, fd, 0);
	}
#ifndef __ANDROID__
	close(fd);
#endif
	if(data->mem == MAP_FAILED) {
		fprintf(stderr, "could not mmap!\n");
		return -LNE_CHECK_ERRNO;
	}
	return 0;
}


int ln_shared_mem_close(ln_shared_mem_handle_t* data, int destroy) {
	munmap(data->mem, data->size);
	if(destroy)
#ifdef __ANDROID__
		close(data->fd);
#else
		shm_unlink(data->name);
#endif
	return 0;
}

int ln_shared_mem_resize(ln_shared_mem_handle_t* data, unsigned int new_size) {
#ifndef __ANDROID__
	// unmap
	ln_shared_mem_close(data, 0);
	// open shm
	int fd = shm_open(data->name, O_RDWR, 0777);
	if(fd == -1) {
		fprintf(stderr, "shm_open('%s') in remap of ln_process_shared_cond failed!", data->name);
		return -LNE_CHECK_ERRNO;
	}
	// ftruncate to new size
	if(new_size != 0) {
		// we want to resize!
		data->size = new_size;
		ftruncate(fd, data->size);
	} else {
		struct stat sbuf;
		if(fstat(fd, &sbuf))
			return -LNE_CHECK_ERRNO;
		// exactly map size of shm!
		data->size = (unsigned)sbuf.st_size;
	}
	// map again
	data->mem = mmap(NULL, data->size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_LOCKED | MAP_POPULATE, fd, 0);
	if(data->mem == MAP_FAILED) {
		fprintf(stderr, "mmap() in remap of ln_process_shared_cond failed!");
		return -LNE_CHECK_ERRNO;
	}
	// close fd
	close(fd);
#endif
	return 0;
}

#endif
