/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/types.h>

#ifndef __WIN32__
#include <unistd.h>
#endif

#include "ln/ln.h"

#include "shm.h"
#include "port.h"

typedef shm_t shm_source;

int create_shm_source(shm_source_t* source_, const char* name, unsigned int element_size, unsigned int n_elements, unsigned int version) {
	return create_shm((shm_t**)source_, name, element_size, n_elements, version);
}

int open_shm_source(shm_source_t* source_, const char* name) {
	return open_shm((shm_t**)source_, name);
}

int shm_source_get_last_counter(shm_source_t* source_, uint32_t* counter) {
	shm_source* source = (shm_source*) source_;
	*counter = source->hdr->last_data_counter;
	return 0;
}

static void cleanup_ln_mutex_unlock(void* mutex) {
	ln_process_shared_mutex_unlock((ln_process_shared_mutex_t*)mutex);
}

#ifdef __ANDROID__
int get_shm_fd(void* source_or_target, unsigned int* shm_size) {
	shm_t* shm = (shm_t*) source_or_target;
	*shm_size = shm->shared_mem.size;
	return shm->shared_mem.fd;
}
#endif

static int have_new_packet(shm_source* source, uint32_t counter, shm_header_element_t** return_element) {
	unsigned int last_written = SHM_GET_PREV_ELEMENT(source, source->hdr->next_write);
	if(!source->hdr->is_full && last_written == source->hdr->n_elements) {
		// no element written yet!
		return 0;
	}
	// last written element at last_written
	unsigned int read_is_behind = source->hdr->last_data_counter - counter;
	if(read_is_behind > source->hdr->n_elements) {
		// slow reader! n_elements too small! start reading at last_written!
		if(source->hdr->is_full) // ensure non blocking behaviour!
			source->next_read = source->hdr->next_write;
		else
			source->next_read = last_written;
	}
	if(source->next_read != source->hdr->next_write || read_is_behind >= source->hdr->n_elements) {
		shm_header_element_t* element = SHM_GET_ELEMENT(source, source->next_read);
		if(counter != element->data_counter) {
			if(return_element)
				*return_element = element;
			return 1;
		}
	}
	return 0;
}

#ifdef HAVE_SYS_FUTEX
static int have_new_packet_v3(shm_source* source, uint32_t counter, shm_header_element_t** return_element) {
	if(counter == source->hdr->last_data_counter) {
		return 0; // no change
	}
	// counter changed, return new element
	shm_header_element_t* element = SHM_GET_ELEMENT(source, source->next_read);
	if(return_element)
		*return_element = element;
	return 1;
}

static inline uint32_t shm_element_get_data_cnt_write_fin(shm_source* source, shm_header_element_t* element) {
	// have it as function to enforce a sequence point
	return SHM_ELEMENT_GET_DATA_CNT_WRITE_FIN(source, element);
}

static inline int try_fetch_data(shm_source* source, shm_header_element_t* element, double* ts, uint32_t* packet_counter, void* data, unsigned int len) {
	memory_barrier();
	uint32_t packet_cnt_start = SHM_ELEMENT_GET_DATA_CNT(element);
	uint32_t packet_cnt_end = shm_element_get_data_cnt_write_fin(source, element);
	if(packet_cnt_start != packet_cnt_end) {
		// unfinished element!
		*packet_counter = packet_cnt_start;
		return -1;
	}
	memcpy(data, SHM_ELEMENT_GET_DATA(element), len);
	if(ts)
		memcpy(ts, &element->data_timestamp, sizeof(*ts)); // avoid fpu op
	
	uint32_t packet_cnt_start2 = SHM_ELEMENT_GET_DATA_CNT(element);
	*packet_counter = packet_cnt_start;
	
	source->next_read = SHM_GET_NEXT_ELEMENT(source, source->next_read);
	if((packet_cnt_start != packet_cnt_start2) // packet was already overwritten while we were reading
	   || (packet_cnt_start != packet_cnt_end) // packet was not yet finished while we read all of it
		) {
		return -1;
	}
	return 0; // success		
}
	
#endif // HAVE_SYS_FUTEX

int block_only_on_shm_source(shm_source_t source_, uint32_t* packet_counter, double timeout) {
	/* return values:
	   1 - new packet!
	   0 - timeout!
	   -2 - unblock from other thread!
	*/
	shm_source* source = (shm_source*) source_;
	shm_header_t* hdr = source->hdr;
	double abs_timeout = timeout + ln_get_monotonic_time();
	int ret = 0;

#ifdef HAVE_SYS_FUTEX
	if(hdr->version >= 3) { // v3 with linux
		while(ret == 0) {
			if(*packet_counter != hdr->last_data_counter)
				break;
			if(source->unblock) {
				source->unblock = 0;
				ret = -2;
				break;
			}
			// We can't use UNTIL_RET_NOT_EINTR() safely here because
			// futex_timedwait() uses a relative time-out.
			// If there is an early return caused by a signal,
			// we need to adjust the time-out.
			// (might be mostly relevant in cases where high-frequency
			// timer signals happen, see issue #249).
			double remaining_timeout;
			double const now = ln_get_time();
			if(now < abs_timeout)
				remaining_timeout = abs_timeout - now;
			else
				remaining_timeout = 0;

			ret = futex_timedwait(&hdr->last_data_counter,
					      *packet_counter, remaining_timeout);

			if(ret == -1 && (errno == EINTR || errno == EAGAIN))
				ret = 0; // handle interruption & new-packet-on-entry as spurious wake-up
			// ret == -1, errno ETIMEDOUT
		}
		if(ret == 0)
			*packet_counter = hdr->last_data_counter;
	} else
#endif
	{ // for non linux or < v3
		ln_process_shared_mutex_lock(&hdr->mutex);
		thread_cleanup_push(cleanup_ln_mutex_unlock, &hdr->mutex);
		while(ret == 0) {
			if(*packet_counter != hdr->last_data_counter)
				break;
			if(source->unblock) {
				source->unblock = 0;
				ret = -2;
				break;
			}
			ret = ln_process_shared_cond_timedwait(&hdr->cond, &hdr->mutex, abs_timeout);
		}
		if(ret == 0)
			*packet_counter = hdr->last_data_counter;
		ln_process_shared_mutex_unlock(&hdr->mutex);
		thread_cleanup_pop(0);
	}
	
	if(ret == 0)
		return 1; // condition!
	else if(ret == -2)
		return -2; // unblocked!
	return 0; // timeout
}

int read_from_shm_source(shm_source_t source_, double* ts, uint32_t* packet_counter, void* data, unsigned int len, int blocking) {
	shm_source* source = (shm_source*) source_;
	int ret = 0;

	if(len > source->shared_mem.size)
		len = source->shared_mem.size;
	
	shm_header_element_t* element;
#ifdef HAVE_SYS_FUTEX
	shm_header_t* hdr = source->hdr;
	bool is_version3_or_higher = hdr->version >= 3;
	if(is_version3_or_higher) {
		while(true) {
			if(blocking) {
				// blocking
				while(ret == 0) {
					if(have_new_packet_v3(source, *packet_counter, &element)) {
						ret = 1; // new packet
						break;
					}
					if(source->unblock) {
						source->unblock = 0;
						ret = -2;
						break;
					}
					UNTIL_RET_NOT_EINTR(ret, (int)futex_wait(&hdr->last_data_counter, *packet_counter));
					if(ret) {
						if(errno == EAGAIN)
							ret = 0;
						else {
							fprintf(stderr, "futex_wait returned %d: %d %s\n", ret, errno, strerror(errno));
							ret = -1;
						}
					}
				}
			} else // non-blocking
				ret = have_new_packet_v3(source, *packet_counter, &element);
		
			if(ret == 1) {
				// get data
				if(try_fetch_data(source, element, ts, packet_counter, data, len) == -1) {
					// data changed while reading!
					if(blocking) // try again!
						continue;
					// non, blocking...
					return 0; // not a valid packet
				}
			}
			return ret; // 1 for new packet, -1 for error, -2 for unblock
		}
	}
#endif
	// for non linux or < v3
	if(ln_process_shared_mutex_lock(&source->hdr->mutex)) {
		printf("had error from ln_process_shared_mutex_lock(&source->hdr->mutex: %p)\n", &source->hdr->mutex);
		return -1;
	}
	thread_cleanup_push(cleanup_ln_mutex_unlock, &source->hdr->mutex);

	if(blocking) {
		// blocking
		while(ret == 0) {
			if(have_new_packet(source, *packet_counter, &element)) {
				ret = 1; // new packet
				break;
			}
			if(source->unblock) {
				source->unblock = 0;
				ret = -2;
				break;
			}
			ret = ln_process_shared_cond_wait(&source->hdr->cond, &source->hdr->mutex);
			if(ret) {
				fprintf(stderr, "ln_process_shared_cond_wait returned %d: %s\n", ret, strerror(ret));
				ret = -1;
			}
		}
	} else {
		// non-blocking
		ret = have_new_packet(source, *packet_counter, &element);
	}
	if(ret == 1) {
		// get data
		memcpy(data, SHM_ELEMENT_GET_DATA(element), len);
		if(ts) *ts = element->data_timestamp;
		*packet_counter = element->data_counter;
		source->next_read = SHM_GET_NEXT_ELEMENT(source, source->next_read);
	}
	ln_process_shared_mutex_unlock(&source->hdr->mutex);
	thread_cleanup_pop(0);
	
	return ret; // 1 for new packet, -1 for error, -2 for unblock
}


int read_from_shm_source_timeout(shm_source_t source_, double* ts, uint32_t* packet_counter, void* data, unsigned int len, double timeout) {
	shm_source* source = (shm_source*) source_;
	int ret = 0;
	shm_header_element_t* element;
	
	if(len > source->shared_mem.size)
		len = source->shared_mem.size;

#ifdef HAVE_SYS_FUTEX
	shm_header_t* hdr = source->hdr;
	double deadline = ln_get_monotonic_time() + timeout;
	if(hdr->version >= 3) {
		while(true) {
			while(ret == 0) {
				if(have_new_packet(source, *packet_counter, &element)) // this will tell us on which element we want to wait!
					break;
				if(source->unblock) {
					source->unblock = 0;
					return -2;
				}
				// We can't use UNTIL_RET_NOT_EINTR() safely here because
				// futex_timedwait() uses a relative time-out.
				// If there is an early return caused by a signal,
				// we need to adjust the time-out.

				double remaining_timeout;
				double const now = ln_get_monotonic_time();
				if(now < deadline)
					remaining_timeout = deadline - now;
				else
					remaining_timeout = 0;
				
				ret = futex_timedwait(&hdr->last_data_counter,
						      *packet_counter, remaining_timeout);
				if(ret == -1 && (errno == EINTR || errno == EAGAIN)) {
					/*
					  EINTR just signals that this process received a signal. ignore & try again!

					  EAGAIN signals that our last_data_counter was already expired when futex_timedwait() was entered!
					  there is already data to read! we want another round to the top to get element from have_new_packet()!
					*/
					ret = 0;
				}
			}
		
			if(ret == -1)
				return 0; // no new packet!
			
			// get data
			if(try_fetch_data(source, element, ts, packet_counter, data, len) == -1) {
				// data changed while reading - try again!
				continue;
			}
			return 1; // new packet!
		}
	}
#endif
	// for non linux or < v3
	// timeout-blocking
	double abs_timeout = timeout + ln_get_monotonic_time();

	ln_process_shared_mutex_lock(&source->hdr->mutex);
	thread_cleanup_push(cleanup_ln_mutex_unlock, &source->hdr->mutex);

	while(ret == 0) {
		if(have_new_packet(source, *packet_counter, &element))
			break;
		if(source->unblock) {
			source->unblock = 0;
			ret = -2;
			break;
		}
		ret = ln_process_shared_cond_timedwait(&source->hdr->cond, &source->hdr->mutex, abs_timeout);
	}

	if(ret == 0) {
		ret = 1; // new packet!
		// get data
		memcpy(data, SHM_ELEMENT_GET_DATA(element), len);
		if(ts) *ts = element->data_timestamp;
		*packet_counter = element->data_counter;
		source->next_read = SHM_GET_NEXT_ELEMENT(source, source->next_read);
	} else if(ret != -2) { // not unblock,
		ret = 0; // no new packet!
	}
	
	ln_process_shared_mutex_unlock(&source->hdr->mutex);
	thread_cleanup_pop(0);
	
	return ret;  // 1 for new packet, -1 for error, -2 for unblock
}

int read_last_from_shm_source(shm_source_t source_, double* ts, uint32_t* packet_counter, void* data, unsigned int len) {
	shm_source* source = (shm_source*) source_;
	int ret = 0;
	
	if(len > source->shared_mem.size)
		len = source->shared_mem.size;

#ifdef HAVE_SYS_FUTEX
	shm_header_t* hdr = source->hdr;
	if(hdr->version >= 3) {
		unsigned int last_written = SHM_GET_PREV_ELEMENT(source, hdr->next_write);
		if(!hdr->is_full && last_written == hdr->n_elements)
			return 0; // no packet at all!
		if(last_written >= hdr->n_elements) {
			fprintf(stderr, "invalid last_written element %u in shm source %p!\n",
				last_written, source);
			return 0;
		}
		
		while(true) {
			shm_header_element_t* element = SHM_GET_ELEMENT(source, last_written);
			ret = 1; // new packet!
			// get data
			if(try_fetch_data(source, element, ts, packet_counter, data, len) == -1) {
				// data changed while reading - try again!
				last_written = SHM_GET_PREV_ELEMENT(source, hdr->next_write);
				continue;
			}
			return 1;
		}
	}
#endif
	ln_process_shared_mutex_lock(&source->hdr->mutex);
	thread_cleanup_push(cleanup_ln_mutex_unlock, &source->hdr->mutex);

	unsigned int last_written = SHM_GET_PREV_ELEMENT(source, source->hdr->next_write);
	if(!source->hdr->is_full && last_written == source->hdr->n_elements) {
		ret = 0; // no packet at all!
	} else {
		shm_header_element_t* element = SHM_GET_ELEMENT(source, last_written);
		ret = 1; // new packet!
		// get data
		memcpy(data, SHM_ELEMENT_GET_DATA(element), len);
#ifdef ARM
		if(ts) memcpy(ts, &element->data_timestamp, sizeof(*ts)); // avoid fpu op
#else
		if(ts) *ts = element->data_timestamp;
#endif
		*packet_counter = element->data_counter;
		source->next_read = SHM_GET_NEXT_ELEMENT(source, source->next_read);
	}
	
	ln_process_shared_mutex_unlock(&source->hdr->mutex);
	thread_cleanup_pop(0);
	
	return ret;
}


int destroy_shm_source(shm_source_t* source_) {
	destroy_shm((shm_t*)*source_);
	*source_ = NULL;
	return 0;
}

int close_shm_source(shm_source_t* source_) {
	close_shm((shm_t*)*source_);
	*source_ = NULL;
	return 0;
}

int unblock_shm_source(shm_source_t source_) {
	shm_source* source = (shm_source*) source_;
	source->unblock = 1;
	ln_process_shared_cond_broadcast(&source->hdr->cond);
	return 0;
}
