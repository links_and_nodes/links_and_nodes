#ifndef LN_THREAD_POOL
#define LN_THREAD_POOL

#include "os.h"

struct thread_pool_;
typedef struct thread_pool_ thread_pool;

struct pool_thread_;
typedef struct pool_thread_ pool_thread;

#include "os.h"
#include "client.h"

typedef void (*pool_thread_handler)(pool_thread* thread, void* user_data);

struct pool_thread_ {
	thread_pool* pool;
	ln_semaphore_handle_t in_use;
	ln_thread_handle_t handle;

	int keep_running;
	
	ln_cond_handle_t cond;
	ln_mutex_handle_t data_mutex;
	pool_thread_handler handler;
	void* user_data;
};

struct thread_pool_ {
	client* clnt;
	char* name;

	unsigned int max_n_threads;
	DEFINE_LIST_DATA(pool_thread);
	
	ln_mutex_handle_t mutex;
	ln_cond_handle_t cond;

	int policy;
	int priority;
	unsigned int affinity_mask;
};


DECLARE_LIST_METHODS(thread_pool, pool_thread);
thread_pool* get_thread_pool(client* clnt, const char* pool_name);
int _ln_thread_pool_destroy(thread_pool* p);
int thread_pool_request_thread(thread_pool* pool, pool_thread_handler handler, void* user_data, pool_thread** thread_out);

void _ln_stop_pool_thread(pool_thread* t);
void _ln_wait_for_pool_thread_idle(pool_thread* t);

#endif // LN_THREAD_POOL
