/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ln/sharedlnclient.h"
#include "client.h"

#include <set>
#include <map>
#include <pthread.h>

using namespace std;

typedef set<void*> ln_client_users_t;
typedef pair<ln_client, ln_client_users_t> ln_client_t;

typedef map<string, ln_client_t> ln_clients_t;
ln_clients_t ln_clients; // export this!

static pthread_mutex_t ln_clients_mutex = PTHREAD_MUTEX_INITIALIZER;

// unly used for testing with ln_manager == "__dummy_test_client":
extern "C" int _init_clnt_struct(client** clnt, const char* program_name);

// #define DEBUG
int sharedlnclient_init(string client_name, string ln_manager, ln_client* clnt, void* user_ptr, const char* hint) {
	pthread_mutex_lock(&ln_clients_mutex);
	ln_clients_t::iterator i = ln_clients.find(client_name);
#ifdef DEBUG		
	fprintf(stderr, "have %d clients in %p!\n", ln_clients.size(), &ln_clients);
#endif		
	if(i == ln_clients.end()) {
		// pthread_mutex_unlock(&ln_clients_mutex);
		// create new ln_client instance
		// try to connect to ln_manager
		int ret;

#ifdef DEBUG		
		fprintf(stderr, "%s: connect to ln_manager at %s in %p\n", 
		       hint,
		       ln_manager.c_str(), user_ptr);
#endif
		if(ln_manager == "__dummy_test_client") {
			_init_clnt_struct((client**)clnt, "test");
			((client*)(*clnt))->debug = 1;
		} else {
			if((ret = ln_init_to_manager(clnt, client_name.c_str(), ln_manager.c_str()))) {
				pthread_mutex_unlock(&ln_clients_mutex);
				fprintf(stderr, "%s: could not init ln_client '%s' to manager '%s'. returned error %d: %s\n",
					hint,
					client_name.c_str(),
					ln_manager.c_str(),
					-ret,
					ln_format_error(ret));
				*clnt = NULL;
				return -1;
			}
		}
		ln_clients[client_name].first = *clnt;
		ln_clients[client_name].second.insert(user_ptr);
		//		fprintf(stderr, "added client %s\n", client_name);
	} else {
		// use existing ln_client instance
		*clnt = ln_clients[client_name].first;
		ln_clients[client_name].second.insert(user_ptr);
#ifdef DEBUG
		 fprintf(stderr, "%s: reusing exiting connection in %p.\n", hint, user_ptr);
#endif
	}
	pthread_mutex_unlock(&ln_clients_mutex);
	return 0;
}

int sharedlnclient_deinit(string client_name, void* user_ptr, const char* hint) {
	pthread_mutex_lock(&ln_clients_mutex);
	ln_clients_t::iterator i = ln_clients.find(client_name);
	if(i != ln_clients.end()) {
		i->second.second.erase(user_ptr);
		if(i->second.second.size()) {
#ifdef DEBUG
			printf("%s: NOT disconnecting from ln_manager in %p because there are still other users.\n", hint, user_ptr);
#endif
		} else {
#ifdef DEBUG
			printf("%s: disconnecting from ln_manager in %p.\n", hint, user_ptr);
#endif
			ln_deinit(&i->second.first);
			ln_clients.erase(i);
		}
	} else {
#ifdef DEBUG
		printf("%s: already disconnected in %p.\n", hint, user_ptr);
#endif
	}
	pthread_mutex_unlock(&ln_clients_mutex);
	return 0;
}
