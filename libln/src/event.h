#ifndef LN_EVENT
#define LN_EVENT

#include "os.h"

struct event_connection_;
typedef struct event_connection_ event_connection;

struct event_call_;
typedef struct event_call_ event_call;

#include "os.h"
#include "client.h"
#include "service.h"
#include "ln/client_lib_messages.h"

struct event_call_ {
	event_connection* connection;
	ln_service_request req;
	ln_event_call_t* data;
};

struct event_connection_ {
	client* clnt;
	char* event_name;
	int is_internal_event;
	char* connect_signature;

	ln_service call_svc;
	char* call_svc_name;
	struct _ln_iovec_mgr mgr;
	struct _ln_aux_mem_holder aux_mem;

	struct _ln_service_field connect_fields;
	char* connect_data;
	uint32_t connect_data_len;
	unsigned int own_connect_data;
	
 	ln_event_handler call_handler;
	void* call_user_data;

	struct _ln_service_field call_fields;
	int have_call_fields;

	int connected;
	unsigned int event_connection_id;

};

int _ln_event_connection_destroy(event_connection* c);


#endif // LN_EVENT
