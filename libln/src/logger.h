#ifndef LN_LOGGER
#define LN_LOGGER

struct logger_;
typedef struct logger_ logger;

#include "client.h"

// on disc representation:
typedef struct {
	logger_topic** topics;
	unsigned int n_topics;
	// unsigned char own_by_user;
} logger_data;

struct logger_ {
	client* clnt;
	char* name;

	DEFINE_LIST_DATA(logger_topic);	
	logger_data own_data;

	int only_ts;
};

DECLARE_LIST_METHODS(logger, logger_topic);

int _ln_logger_destroy(logger* l);
int _ln_logger_topic_destroy(logger_topic* l);

#endif // LN_LOGGER
