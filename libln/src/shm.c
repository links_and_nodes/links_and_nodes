/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/types.h>
#include <stdio.h>

#ifndef __WIN32__
#include <unistd.h>
#endif

#include "ln/ln.h"
#include "shm.h"
#include "client.h"

#ifdef MSVC
#define roundup_to_n(v, n) ((unsigned)((v) / (n)) * (n) + (((v) % (n)) ? (n) : 0))
#else
#define roundup_to_n(v, n) ((typeof(v))((v) / (n)) * (n) + (((v) % (n)) ? (n) : 0))
#endif

static unsigned int _calc_size(unsigned int element_step, unsigned int n_elements) {
	return sizeof(shm_header_t) + element_step * n_elements - sizeof(shm_header_element_t);
}


int open_shm(shm_t** shm_, const char* name) {
	shm_t* shm = (shm_t*)malloc(sizeof(shm_t));
	if(!shm)
		return -LNE_CHECK_ERRNO;
	shm->unblock = 0;
	char shm_name[1024];
	snprintf(shm_name, 1024, "/%s", name);

	int ret = ln_shared_mem_open(&shm->shared_mem, shm_name);
	if(ret) {
		ln_debug(NULL, "ln_shared_mem_open('%s') returned: %d\n", shm_name, ret);
		free(shm);
		return ret;
	}	

	shm->hdr = (shm_header_t*)shm->shared_mem.mem;
	if(memcmp(shm->hdr->ln_shm_magic, LN_SHM_MAGIC, 7)) {
		free(shm);
		return -LNE_INVALID_SHM_MAGIC;
	}
	if(shm->hdr->version > LN_SHM_VERSION) {
		free(shm);
		return -LNE_SHM_VERSION_TOO_NEW;
	}

	if(shm->hdr->version == 1)
		shm->element_step = (sizeof(shm_header_element_t) + shm->hdr->element_size);
	else if(shm->hdr->version < 3)
		shm->element_step = roundup_to_n((sizeof(shm_header_element_t) + shm->hdr->element_size), 8);
	else
		shm->element_step = roundup_to_n((sizeof(shm_header_element_t) + shm->hdr->element_size + sizeof(uint32_t)), 8); // additional data_counter_write_finished field at end
	
	unsigned int shm_size = _calc_size(shm->element_step, shm->hdr->n_elements);

	if(shm->shared_mem.size < shm_size) {
		ln_debug(NULL, "shared memory reported size of %d, expected size is %d!\n", shm->shared_mem.size, shm_size);
		free(shm);
		return -LNE_WRONG_SHM_SIZE;
	}

	shm->next_read = shm->hdr->next_write;

#ifdef HAVE_SYS_FUTEX
	if(shm->hdr->version < 3)// on linux with v3, no init is needed!
#endif
	{
		char success = 0;
		while(1) {
			// mutex
#ifdef __WIN32__
			ln_debug(NULL, "ln_process_shared_mutex_open: mutex at %p, mutex_name: %-128.128s\n", &shm->hdr->mutex, &shm->hdr->mutex.mutex_name);
#endif
			if((ret = ln_process_shared_mutex_open(&shm->hdr->mutex, 0)))
				break;
			if((ret = ln_process_shared_cond_open(&shm->hdr->cond, 0)))
				break;
			success = 1;
			break;
		}
		if(!success) {
			free(shm);
			errno = ret;
			return -LNE_CHECK_ERRNO;
		}
	}
	
	*shm_ = shm;
	return 0;
}

int create_shm(shm_t** shm_, const char* name, unsigned int element_size, unsigned int n_elements, unsigned int version) {
	if(version > LN_SHM_VERSION)
		return -LNE_SHM_VERSION_TOO_NEW;

	shm_t* shm = (shm_t*)malloc(sizeof(shm_t));
	if(!shm) {
		ln_debug(NULL, "create_shm('%s') malloc can not get mem for %d bytes!\n", name, sizeof(shm_t));
		return -LNE_CHECK_ERRNO;
	}
	shm->unblock = 0;
	char shm_name[1024];
	snprintf(shm_name, 1024, "/%s", name);

	unsigned int element_step;
	if(version <= 1)
		element_step = (sizeof(shm_header_element_t) + element_size);
	else if(version < 3)
		element_step = roundup_to_n((sizeof(shm_header_element_t) + element_size), 8);
	else
		element_step = roundup_to_n((sizeof(shm_header_element_t) + element_size + sizeof(uint32_t)), 8); // additional data_counter_write_finished field at end
	unsigned int shm_size = _calc_size(element_step, n_elements);

	int ret = ln_shared_mem_create(&shm->shared_mem, shm_name, shm_size);
	if(ret) {
		if(ret == -LNE_CHECK_ERRNO)
			ln_debug(NULL, "ln_shared_mem_create('%s', size %d) failed: %s\n", shm_name, shm_size, strerror(errno));
		else
			ln_debug(NULL, "ln_shared_mem_create('%s', size %d) returned: %d\n", shm_name, shm_size, ret);
		free(shm);
		return ret;
	}	
	memset(shm->shared_mem.mem, 0, shm_size);

	shm->hdr = (shm_header_t*)shm->shared_mem.mem;
	strncpy(shm->hdr->ln_shm_magic, LN_SHM_MAGIC, 7);
	shm->hdr->version = version;
	
	shm->hdr->element_size = element_size;
	shm->hdr->n_elements = n_elements;
	shm->hdr->next_write = 0;
	shm->hdr->is_full = 0;
	
	shm->next_read = 0;
	shm->element_step = element_step;

#ifdef HAVE_SYS_FUTEX
	if(version < 3)// on linux with v3, no init is needed!
#endif
	{
		char success = 0;
		while(1) {
			// mutex
			if((ret = ln_process_shared_mutex_open(&shm->hdr->mutex, 1))) {
				ln_debug(NULL, "create_shm('%s') ln_process_shared_mutex_open() returned: %d\n", name, ret);
				break;
			}
			if((ret = ln_process_shared_cond_open(&shm->hdr->cond, 1))) {
				ln_debug(NULL, "create_shm('%s') ln_process_shared_cond_open() returned: %d\n", name, ret);
				break;
			}
			success = 1;
			break;
		}
		if(!success) {
			free(shm);
			// ln_shared_mem_destroy...
			errno = ret;
			return -LNE_CHECK_ERRNO;
		}
	}
	

	*shm_ = shm;
	return 0;
}

int destroy_shm(shm_t* shm) {
	if(shm) {
#ifdef HAVE_SYS_FUTEX
		if(shm->hdr->version >= 3) {
			// try to free any left/lost waiters...
			shm->hdr->last_data_counter ++;
			futex_wake_all(&shm->hdr->last_data_counter);
		} else
#endif
		{
			ln_process_shared_mutex_lock(&shm->hdr->mutex);
			ln_process_shared_cond_close(&shm->hdr->cond, 1);
			ln_process_shared_mutex_unlock(&shm->hdr->mutex);
			ln_process_shared_mutex_close(&shm->hdr->mutex, 1);
		}
		ln_shared_mem_close(&shm->shared_mem, 1);
		free(shm);
	}
	return 0;
}

int close_shm(shm_t* shm) {
	if(shm) {
#ifdef HAVE_SYS_FUTEX
		if(shm->hdr->version < 3)
#endif
		{
			ln_process_shared_mutex_lock(&shm->hdr->mutex);
			ln_process_shared_cond_close(&shm->hdr->cond, 0); // do not destroy!
			ln_process_shared_mutex_unlock(&shm->hdr->mutex);
			ln_process_shared_mutex_close(&shm->hdr->mutex, 0);
		}
		ln_shared_mem_close(&shm->shared_mem, 0);

		free(shm);
	}
	return 0;
}
