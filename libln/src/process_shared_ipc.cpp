/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <sys/stat.h>
#ifndef __WIN32__
#include <fcntl.h>
#endif
#include <string.h>
#include <errno.h>

#ifdef USE_OWN_PROCESS_SHARED_IPC

#include <map>
#include <string>

using namespace std;

class _mutex_data_t {
public:
	ln_semaphore_handle_t sem;
};
typedef map<ln_process_shared_mutex_t*, _mutex_data_t*> mutexes_t;
mutexes_t mutexes;

#ifdef __WIN32__
#define DEFAULT_MAX_COND_LISTENERS 64 // no resizing possibility on this os!
#else
#define DEFAULT_MAX_COND_LISTENERS 8
#endif
struct _public_cond_listener_data_t {
	char in_use;
	char sem_name[128];
};

struct _private_cond_listener_data_t {
	_public_cond_listener_data_t* pub_listener;
	ln_semaphore_handle_t sem;
	bool used;

	_private_cond_listener_data_t(_public_cond_listener_data_t* pub_listener, int create) : pub_listener(pub_listener) {
		// printf("open cond semaphore %p named '%s', create=%d\n", &sem, pub_listener->sem_name, create);
		ln_semaphore_create(&sem, pub_listener->sem_name, create);
	}
	~_private_cond_listener_data_t() {
		// printf("close cond semaphore %p\n", &sem);
		ln_semaphore_close(sem);
	}

	void post() {
		int v = ln_semaphore_getvalue(sem);
		// no error check possible! valid value range of ret is neg, zero and positive....
		/*
		if(v == -1) {
			fprintf(stderr, "ln_semaphore_getvalue() returned %d: %s\n", v, strerror(errno));
			v = 0;
			}*/
		if(v <= 0) {
			// printf("post cond semaphore %p\n", &sem);
			ln_semaphore_post(sem);
		} else {
			// printf("DO NOT post cond semaphore %p - already has value %d\n", &sem, v);
		}
		used = true;
	}
	void wait() {
		// printf("%p: _private_cond_listener_data_t::wait()\n");
		ln_semaphore_wait(sem);
	}
	int timedwait(double abs_timeout) {
		return ln_semaphore_timedwait_abs(sem, abs_timeout);
	}
};


typedef map<_public_cond_listener_data_t*, _private_cond_listener_data_t*> _private_listeners_t;

struct _cond_data_t {
	ln_process_shared_cond_t* cond;
	_private_listeners_t private_listeners;


	unsigned int shm_size;
	unsigned int last_max_listeners;

	ln_shared_mem_handle_t shared_mem;
	void* mem;
	
	// pointers into mem:
	unsigned int* n_listeners;
	unsigned int* max_listeners;
	_public_cond_listener_data_t* listeners;
	_public_cond_listener_data_t* own_public_listener;
	_private_cond_listener_data_t* own_private_listener;

	void mark_listeners_as_unused() {
		for(_private_listeners_t::iterator i = private_listeners.begin(); i != private_listeners.end(); i++)
			i->second->used = false;
	}

	void remove_unused_listeners() {
		for(_private_listeners_t::iterator i = private_listeners.begin(); i != private_listeners.end(); ) {
			if(!i->second->used) {
				delete i->second;
				private_listeners.erase(i);
				i = private_listeners.begin();
				continue;
			}
			i++;
		}
	}
	_private_cond_listener_data_t* get_priv_listener(_public_cond_listener_data_t* pub_listener, int create=0) {
		_private_listeners_t::iterator i = private_listeners.find(pub_listener);
		if(i != private_listeners.end()) 
			return i->second;
		_private_cond_listener_data_t* priv_listener = new _private_cond_listener_data_t(pub_listener, create);
		private_listeners[pub_listener] = priv_listener;
		return priv_listener;
	}

	void remap(unsigned int new_max=0) {
		// store private_listener to public_listener-name mapping
		typedef map<string, _private_cond_listener_data_t*> name_map_t;
		name_map_t name_map;
		for(_private_listeners_t::iterator i = private_listeners.begin(); i != private_listeners.end(); i++)
			name_map[i->first->sem_name] = i->second;
		
		shm_size = new_max * sizeof(_public_cond_listener_data_t) + 2 * sizeof(unsigned int);
		int ret = ln_shared_mem_resize(&shared_mem, shm_size);
		if(ret)
			return;

		// restore private_listeners map from public_listener-name mapping
		mem = shared_mem.mem;
		n_listeners = (unsigned int*)mem;
		max_listeners = n_listeners + 1;
		listeners = (_public_cond_listener_data_t*)(max_listeners + 1);
		private_listeners.clear();
		unsigned int j = 0;
		for(unsigned int k = 0; k < *n_listeners; ) {
			_public_cond_listener_data_t* public_listener = &listeners[j];
			if(!public_listener->in_use) {
				j ++;
				continue;
			}
			_private_cond_listener_data_t* private_listener = name_map[public_listener->sem_name];
			if(private_listener) {
				// fix link again!
				private_listener->pub_listener = public_listener;
				private_listeners[public_listener] = private_listener;
			}
			k++;
			j++;
		}
		// set max_listeners, and listeners
		if(new_max)
			*max_listeners = new_max;
		last_max_listeners = *max_listeners;
	}

	_private_cond_listener_data_t* get_own_priv_listener(ln_process_shared_mutex_t* mutex) {
		if(!own_public_listener) {
			// do mem-resizing if needed
			if(*n_listeners >= *max_listeners) {
				// printf("remap to %d listeners!\n", *max_listeners * 2);
				remap(*max_listeners * 2);
			}
			// find unused listener slot
			for(unsigned int i = 0; i < *max_listeners; i++) {
				if(!listeners[i].in_use) {
					own_public_listener = &listeners[i];
					break;
				}
			}
			if(!own_public_listener) {
				// error! no free listener slot while having lock!?! and made sure that enough slots are avaliable?
				return NULL;
			}
			*n_listeners = *n_listeners + 1;
			own_public_listener->in_use = 1;
			// generate listener name
			snprintf(own_public_listener->sem_name, 128, "%s_listener_0x%x_%p", cond->cond_name, getpid(), mutex);
			own_private_listener = get_priv_listener(own_public_listener, 1);
		}
		return own_private_listener;
	}

	void broadcast() {
		// check whether the shm changed and remap if needed!
		if(last_max_listeners != *max_listeners) {
			//printf("publisher noticed that shm changed! (from %d to %d)\n", last_max_listeners, *max_listeners);
			remap(0);
		}
		
		// iterate over all known private listeners and mark them as unused
		mark_listeners_as_unused();
		// iterate over all registered listeners and post their semaphores
		unsigned int k = 0;
		//printf("cond %p: broadcast to %d clients\n", this, *n_listeners);
		for(unsigned int i = 0; i < *n_listeners; ) {
			_public_cond_listener_data_t* pub_listener = &listeners[k];
			// printf("listeners[k:%d]: %p, i:%d\n", k, pub_listener, i);
			if(!pub_listener->in_use) {
				k ++;
				if(k >= *max_listeners)
					break;
				continue;
			}
			i++;
			k++;
			_private_cond_listener_data_t* priv_listener = get_priv_listener(pub_listener);
			if(priv_listener)
				priv_listener->post();
		}
		// remove unused private listener entries!
		remove_unused_listeners();
	}

};
typedef map<ln_process_shared_cond_t*, _cond_data_t*> conds_t;
conds_t conds;

#ifdef __cplusplus
extern "C" {
#endif

int ln_process_shared_mutex_open(ln_process_shared_mutex_t* mutex, int create) {
	/**

	   WARNING:
	   this process-shared-mutex implementation uses named semaphores and because of
	   this can not protect against PRIORITY INVERSION!
	   
	 */
	_mutex_data_t* data = new _mutex_data_t();
	char name[128];
	if(create)
		snprintf(name, 128, "/ln_process_shared_mutex_0x%x_%p", getpid(), mutex);
	else
		strcpy(name, mutex->mutex_name);

	// printf("open mutex semaphore %p named '%s', create=%d\n", &data->sem, name, create);
	if(ln_semaphore_create(&data->sem, name, create) == -1) {
		printf("ln_process_shared_mutex_open ln_semaphore_create on '%s' create=%d for mutex %p failed! %s\n",
			   name, create, mutex, strerror(errno));
		return -errno;
	}
	if(create)
		ln_semaphore_post(data->sem);

	mutexes[mutex] = data;

	if(create)
		strcpy(mutex->mutex_name, name);

	return 0;		
}

int ln_process_shared_mutex_close(ln_process_shared_mutex_t* mutex, int destroy) {
	mutexes_t::iterator i = mutexes.find(mutex);
	if(i != mutexes.end()) {
		_mutex_data_t* data = i->second;
		// printf("close mutex semaphore %p named '%s', destroy=%d\n", &data->sem, mutex->mutex_name, destroy);
		ln_semaphore_close(data->sem);
		if(destroy == 1)
			ln_semaphore_unlink(mutex->mutex_name);
		delete data;
		mutexes.erase(i);
	}
	// don't know how to destroy semaphore!
	if(destroy == 1)
		mutex->mutex_name[0] = 0;
	return 0;
}

int ln_process_shared_mutex_lock(ln_process_shared_mutex_t* mutex) {
	mutexes_t::iterator i = mutexes.find(mutex);
	if(i == mutexes.end()) {
		printf("ln_process_shared_mutex_lock: mutex %p not found in %p!\n", mutex, &mutexes);
		return -1;
	}
	_mutex_data_t* data = i->second;
	// printf("lock mutex semaphore %p\n", &data->sem);
	return ln_semaphore_wait(data->sem);
}

int ln_process_shared_mutex_unlock(ln_process_shared_mutex_t* mutex) {
	mutexes_t::iterator i = mutexes.find(mutex);
	if(i == mutexes.end()) {
		printf("ln_process_shared_mutex_unlock: mutex %p not found!\n", mutex);
		return -1;
	}
	_mutex_data_t* data = i->second;
	// printf("unlock mutex semaphore %p\n", &data->sem);
	return ln_semaphore_post(data->sem);	
}


int ln_process_shared_cond_open(ln_process_shared_cond_t* cond, int create) {
	_cond_data_t* data = new _cond_data_t();
	data->cond = cond;

	char name[128];
	if(create == 1) // generate system-wide unique cond name
		snprintf(name, 128, "/ln_process_shared_cond_0x%x_%p", getpid(), cond);
	else
		strcpy(name, cond->cond_name);

	unsigned int shm_size = DEFAULT_MAX_COND_LISTENERS * sizeof(_public_cond_listener_data_t) + 2 * sizeof(unsigned int);

 	int ret;

	if(create)
		ret = ln_shared_mem_create(
			&data->shared_mem, 
			name, 
			shm_size);
	else {
		ret = ln_shared_mem_open(
			&data->shared_mem, 
			name);
		if(data->shared_mem.size < shm_size) {
			fprintf(stderr, "process shared cond open: shared memory reported size of %d, expected size is %d!\n", 
				data->shared_mem.size, shm_size);
			return -1;
		}
	}

	if(ret)
		return ret;

	data->mem = data->shared_mem.mem;
	data->n_listeners = (unsigned int*)data->mem;
	data->max_listeners = data->n_listeners + 1;
	data->listeners = (_public_cond_listener_data_t*)(data->max_listeners + 1);
	if(create)
		*data->max_listeners = DEFAULT_MAX_COND_LISTENERS;
	data->last_max_listeners = *data->max_listeners;
	// printf("last_max_listeners as ln_process_shared_cond_open: %d\n", data->last_max_listeners);
	data->own_public_listener = NULL;
	data->own_private_listener = NULL;


	conds[cond] = data;
	if(create) // publish new cond:
		strcpy(cond->cond_name, name);
	return 0;
}

int ln_process_shared_cond_close(ln_process_shared_cond_t* cond, int destroy) {
	conds_t::iterator i = conds.find(cond);
	if(i == conds.end())
		return -1;
	_cond_data_t* data = i->second;
	char name[128];
	strcpy(name, cond->cond_name);

	if(data->last_max_listeners != *data->max_listeners)
		data->remap(0);

	if(destroy) {
		cond->cond_name[0] = 0; // mark as to be closed!
		// ln_process_shared_cond_broadcast(cond); // notify listeners
	}
	data->mark_listeners_as_unused();
	data->remove_unused_listeners();

	if(data->own_public_listener) {
		ln_semaphore_unlink(data->own_public_listener->sem_name);
		data->own_public_listener->sem_name[0] = 0;
		data->own_public_listener->in_use = 0;
	}

	if(destroy) {
		unsigned int j = 0;
		for(unsigned int k = 0; k < *data->n_listeners && j < *data->max_listeners; ) {
			_public_cond_listener_data_t* public_listener = &data->listeners[j];
			if(!public_listener->in_use) {
				j ++;
				continue;
			}
			if(!public_listener->sem_name[0])
				continue;
			ln_semaphore_unlink(public_listener->sem_name);
			k++;
			j++;
		}
	}

	ln_shared_mem_close(&data->shared_mem, destroy);

	conds.erase(i);
	delete data;
	return 0;
}

int ln_process_shared_cond_wait(ln_process_shared_cond_t* cond, ln_process_shared_mutex_t* mutex) {
	// assume mutex is locked!
	conds_t::iterator i = conds.find(cond);
	if(i == conds.end()) {
		printf("ln_process_shared_cond_wait: unknown condition %p!\n", cond);
		return -1;
	}
	_cond_data_t* data = i->second;
	_private_cond_listener_data_t* priv_listener = data->get_own_priv_listener(mutex);
	if(!priv_listener)
		return -2;
	// printf("wait on cond %p\n", cond);
	ln_process_shared_mutex_unlock(mutex);
	priv_listener->wait();
	ln_process_shared_mutex_lock(mutex);

	// iff cond->cond_name[0] == 0 -> close!

	return 0;
}

int ln_process_shared_cond_timedwait(ln_process_shared_cond_t* cond, ln_process_shared_mutex_t* mutex, double abs_timeout) {
	// assume mutex is locked!
	conds_t::iterator i = conds.find(cond);
	if(i == conds.end())
		return -1;
	_cond_data_t* data = i->second;
	_private_cond_listener_data_t* priv_listener = data->get_own_priv_listener(mutex);
	if(!priv_listener)
		return -2;
	// printf("wait on cond %p\n", cond);
	ln_process_shared_mutex_unlock(mutex);
	int ret = priv_listener->timedwait(abs_timeout);
	ln_process_shared_mutex_lock(mutex);

	// iff cond->cond_name[0] == 0 -> close!
	return ret;
}

int ln_process_shared_cond_broadcast(ln_process_shared_cond_t* cond) {
	conds_t::iterator i = conds.find(cond);
	if(i == conds.end()) {
		printf("process shared cond broadcast - cond not found!\n");
		return -1;
	}
	_cond_data_t* data = i->second;
	// printf("broadcast cond %p\n", cond);
	data->broadcast();
	return 0;
}

#ifdef __cplusplus
}
#endif

#endif // USE_OWN_PROCESS_SHARED_IPC
