#ifdef USE_OWN_PROCESS_SHARED_IPC

#ifdef __cplusplus 
extern "C" {
#endif
#ifdef WE_START_AT_THE_LINE__BEGINNING
}
#endif
	
// process shared ipc mechanisms
typedef struct {
	// carefull: this struct is stored completely in shm and shared by multiple processes!
	// -> no per process info in here!
	// -> no pointers in here!
	char mutex_name[128];
} ln_process_shared_mutex_t;

typedef struct {
	// carefull: this struct is stored completely in shm and shared by multiple processes!
	// -> no per process info in here!
	// -> no pointers in here!
	char cond_name[128];
} ln_process_shared_cond_t;


#ifdef __cplusplus 
}
#endif

#endif
