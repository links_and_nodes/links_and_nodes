/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <stdio.h>
#include <stdlib.h>

#include "ln/ln.h"
#include "client.h"
#include "request.h"
#include "port.h"

int ln_publish(ln_client clnt_, const char* topic_name, const char* message_definition_name, ln_outport* port) {
	return ln_publish_with_buffers(clnt_, topic_name, message_definition_name, 1, port);
}

int ln_publish_with_buffers(ln_client clnt_, const char* topic_name, const char* message_definition_name, unsigned int buffers, ln_outport* port) {
	client* clnt = (client*)clnt_;

	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	char* topic = _get_topic_mapping(clnt, topic_name, "published");
	int ret = 0;
	while(true) {
		request_t r;
		unsigned int message_size;
		char shm_name[MAX_SHARED_MEM_LEN + 1];
		char name_format[32];
		char* msg_def_hash = NULL;
		
		ret = _ln_init_request(
			clnt, &r,					  
			"method", "r", "publish",				
			"topic", "r", topic,
			"message_definition_name", "r", message_definition_name,
			"buffers", "d", buffers,
			NULL);
		if(ret)
			break;

		while(true) {
			ln_mutex_lock(&clnt->communication_mutex);
			ret = _ln_request_and_wait(&r);
			ln_mutex_unlock(&clnt->communication_mutex);

			if(ret)
				break;

			if((ret = _ln_get_from_request(&r, "message_size", "u", &message_size)))
				break;

			snprintf(name_format, 32, "s%d", MAX_SHARED_MEM_LEN);
			if((ret = _ln_get_from_request(&r, "shm_name", name_format, &shm_name)))
				break;

			if(clnt->manager_library_version >= 17 && (ret = _ln_get_from_request(&r, "msg_def_hash", "S", &msg_def_hash)))
				break;
			
			break;
		}
		_ln_destroy_request(&r);

		if(ret)
			break;

		// create outport
		outport* p = (outport*)malloc(sizeof(outport)); // will be free'd on library deinit!
		if(!p) {
			ret = -LNE_NO_MEM;
			break;
		}

		if((ret = _ln_init_outport(clnt, p, topic, message_definition_name, message_size, msg_def_hash, (const char*)shm_name)))
			break;

		*port = p;
		break;
	}
	free(topic);
	return ret;
}

int ln_unpublish(ln_outport* port) {
	outport* p = (outport*)*port;
	client* clnt = p->hdr.clnt;

	if(clnt->sfd != -1) {
		// notify manager
		request_t r;
		int ret = _ln_init_request(
			clnt, &r,
			"method", "r", "unpublish",
			"topic_name", "r", p->hdr.topic,
			NULL);
		if(!ret) {
			ln_mutex_lock(&clnt->communication_mutex);
			_ln_request_and_wait(&r);
			ln_mutex_unlock(&clnt->communication_mutex);
			_ln_destroy_request(&r);
		}
	}
	
	_ln_outport_destroy(p);
	*port = NULL;

	return 0;
}

int ln_has_subscriber(void* p_) {
	port* p = (port*)p_;
	if(p->type != OUTPUT_PORT)
		return -LNE_INVALID_PORT_OBJECT;
	outport* op = (outport*)p_;
	return op->has_subscriber;
}

int ln_get_message_size(void* p_) {
	port* p = (port*)p_;
	if(p->type != OUTPUT_PORT && p->type != INPUT_PORT)
		return -LNE_INVALID_PORT_OBJECT;
	return _ln_get_message_size(p);
}

const char* ln_get_msg_def_hash(void* p_) {
	port* p = (port*)p_;
	if(p->type != OUTPUT_PORT && p->type != INPUT_PORT)
		return NULL;
	return _ln_get_msg_def_hash(p);
}

int ln_write(ln_outport p_, void* packet, unsigned int packet_size, double* ts) {
	outport* p = (outport*)p_;
	
	if(p->hdr.type != OUTPUT_PORT)
		return -LNE_INVALID_PORT_OBJECT;

	return _ln_write(p, packet, packet_size, ts);
}


int ln_outport_get_next_buffer(ln_outport p_, void** buffer, unsigned int* buffer_len) {
	outport* p = (outport*)p_;
	
	if(p->hdr.type != OUTPUT_PORT)
		return -LNE_INVALID_PORT_OBJECT;
	if(!buffer)
		return LNE_INVALID_PARAMETER;
	return _ln_outport_get_next_buffer(p, buffer, buffer_len);
}




