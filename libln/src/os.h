#ifndef OS_H
#define OS_H

#ifdef __cplusplus
extern "C" {
#else
#define true 1
#define false 0
#endif
#ifdef EMACS_IS_STUPID
}
#endif

int _ln_platform_interrupted(); // client.c
#define UNTIL_RET_NOT_EINTR(ret, what) while(1) {				\
		ret = what;							\
		if(ret == -1 && _ln_platform_interrupted() == 1) continue;	\
		break;								\
	}


#ifdef __LINUX__
#ifndef __ANDROID__
#include "os_linux.h"
#else
#include "os_android.h"
#endif
#endif

#ifdef __QNX__
#include "os_qnx.h"
#endif

#ifdef __VXWORKS__
#include "os_vxworks.h"
#endif

#ifdef __WIN32__
#  include "os_win32.h"
#  define IS_WIN32 1
#else
#  define IS_WIN32 0
#endif

#ifdef HAVE_AF_UNIX
#include <sys/un.h>
#endif

#if defined(GLIBC_ATLEAST_2_25)
#  define IS_GLIBC_ATLEAST_2_25 1
#else
#  define IS_GLIBC_ATLEAST_2_25 0
#endif

#if defined(HAVE_SYS_FUTEX)
#  define IS_WITH_SYS_FUTEX 1
#else
#  define IS_WITH_SYS_FUTEX 0
#endif

#if defined(HAVE_AF_UNIX)
#  define IS_WITH_AF_UNIX 1
#else
#  define IS_WITH_AF_UNIX 0
#endif

// required os-interface for all os'es

// named shared memory
int ln_shared_mem_open(ln_shared_mem_handle_t*, const char* name);
int ln_shared_mem_create(ln_shared_mem_handle_t*, const char* name, unsigned int size);
int ln_shared_mem_close(ln_shared_mem_handle_t*, int destroy);
int ln_shared_mem_resize(ln_shared_mem_handle_t* data, unsigned int new_size);

// unnamed semaphore
int ln_semaphore_init(ln_semaphore_handle_t* handle, unsigned int value);
int ln_semaphore_destroy(ln_semaphore_handle_t handle);
// named semaphore
int ln_semaphore_create(ln_semaphore_handle_t* handle, const char* name, int create);
int ln_semaphore_close(ln_semaphore_handle_t);
int ln_semaphore_unlink(const char* name);

// common semaphores
int ln_semaphore_wait(ln_semaphore_handle_t);
int ln_semaphore_trywait(ln_semaphore_handle_t);
int ln_semaphore_timedwait(ln_semaphore_handle_t, double seconds);
int ln_semaphore_post(ln_semaphore_handle_t handle);
int ln_semaphore_getvalue(ln_semaphore_handle_t handle);

// unnamed in-process-only mutex
int ln_mutex_create(ln_mutex_handle_t* mutex_handle);
int ln_mutex_destroy(ln_mutex_handle_t* mutex_handle);
int ln_mutex_lock(ln_mutex_handle_t* mutex_handle);
int ln_mutex_trylock(ln_mutex_handle_t* mutex_handle);
int ln_mutex_unlock(ln_mutex_handle_t* mutex_handle);

int ln_cond_create(ln_cond_handle_t* cond);
int ln_cond_destroy(ln_cond_handle_t* cond);
int ln_cond_wait(ln_cond_handle_t* cond, ln_mutex_handle_t* mutex);
// ln_cond_timedwait() waits until an absolute time-out value
// computed on the base of ln_get_time_monotonic().
int ln_cond_timedwait(ln_cond_handle_t* cond, ln_mutex_handle_t* mutex, double abs_timeout);
int ln_cond_broadcast(ln_cond_handle_t* cond);
int ln_cond_signal(ln_cond_handle_t* cond);

// threads
#ifdef MSVC
int ln_thread_create(ln_thread_handle_t* thread_handle, thread_func_type thread, void* data);
int ln_thread_create_with_prio(ln_thread_handle_t* thread_handle, thread_func_type thread, void* data, int policy, int priority, const char* name);
int ln_thread_create_with_params(ln_thread_handle_t* thread_handle, thread_func_type thread, void* data, int policy, int priority, unsigned int affinity_mask, const char* name);
#else
int ln_thread_create(ln_thread_handle_t* thread_handle, thread_func_type* thread, void* data);
int ln_thread_create_with_prio(ln_thread_handle_t* thread_handle, thread_func_type* thread, void* data, int policy, int priority, const char* name);
int ln_thread_create_with_params(ln_thread_handle_t* thread_handle, thread_func_type* thread, void* data, int policy, int priority, unsigned int affinity_mask, const char* name);
#endif
void ln_thread_cancel(ln_thread_handle_t t);
int ln_thread_join(ln_thread_handle_t t, void** retval);
int ln_thread_test_cancel(ln_thread_handle_t t);
ln_thread_handle_t ln_thread_self();
void ln_thread_setname(const char* new_name);
int ln_thread_setschedparam(ln_thread_handle_t thread_handle, int policy, int priority, unsigned int affinity_mask);

// thread cancelation
// these are needed - but they might be macros!
//void thread_cleanup_push(void (*)(void*), void* arg);
//void thread_cleanup_pop(int exec);

// time services
#ifndef __WIN32__
double ln_ts2double(struct timespec* ts);
void ln_double2ts(double s, struct timespec* ts);
#endif
double ln_get_time();
double ln_get_time_sys();
double ln_get_monotonic_time();  /* returns monotonic time where avaliable, will return 
				    value from ln_get_time() on a platform without monotonic time */
void ln_sleep_seconds(double s);

// pid_t getpid()

#ifdef USE_OWN_PROCESS_SHARED_IPC
# include "process_shared_ipc.h"
#endif

// named / system-wide mutex
int ln_process_shared_mutex_open(ln_process_shared_mutex_t* mutex, int create);
int ln_process_shared_mutex_close(ln_process_shared_mutex_t* mutex, int destroy);
int ln_process_shared_mutex_lock(ln_process_shared_mutex_t* mutex);
int ln_process_shared_mutex_unlock(ln_process_shared_mutex_t* mutex);

// named / system-wide condition
int ln_process_shared_cond_open(ln_process_shared_cond_t* cond, int create);
int ln_process_shared_cond_close(ln_process_shared_cond_t* cond, int destroy);
int ln_process_shared_cond_wait(ln_process_shared_cond_t* cond, ln_process_shared_mutex_t* mutex);
int ln_process_shared_cond_timedwait(ln_process_shared_cond_t* cond, ln_process_shared_mutex_t* mutex, double abs_timeout);
int ln_process_shared_cond_broadcast(ln_process_shared_cond_t* cond);

struct ln_pipe;
typedef struct ln_pipe ln_pipe_t;

// pipes
int ln_pipe_init(ln_pipe_t* pipe);
int ln_pipe_close(ln_pipe_t* pipe);
void ln_pipe_send_notification(ln_pipe_t pipe);
void ln_pipe_read_notification(ln_pipe_t pipe);

// sockets
void _ln_close_socket(_ln_socket_t fd);
	
#ifdef __cplusplus
}
#endif

#endif // OS_H
