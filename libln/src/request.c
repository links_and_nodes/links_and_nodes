/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>
#include <ctype.h>
#include <sys/types.h>

#ifndef __WIN32__
#include <unistd.h>
#endif

#include <locale.h>

#include "client.h"
#include "ln/ln.h"
#include "repr.h"

#include "request.h"

#ifdef __cplusplus
extern "C" {
#endif
#ifdef EMACS_IS_STILL_DUMB
}
#endif

#define MIN(a, b) ((a)<(b)?(a):(b))

void* _ln_new_request() {
	return malloc(sizeof(request_t));
}

#define GB_ADD(name, value) growing_buffer_append(&name, &name ## _cp, &name ## _size, value)

int _ln_init_request(client* clnt, request_t* req, ...) {
	memset(req, 0, sizeof(request_t));
	req->clnt = clnt;
	req->need_success = 1;
	const char* org_locale = setlocale(LC_ALL, NULL);
	setlocale(LC_ALL, "C");
	va_list ap;
	va_start(ap, req);
	char* field_name;
	while((field_name = va_arg(ap, char*))) {
		char* field_format = va_arg(ap, char*);
		char* field_value;
		if(!strcmp(field_format, "r")) {
			char* str = va_arg(ap, char*);
			field_value = strrepr_(&req->format_buffer, &req->format_buffer_size, str);
		} else if(!strcmp(field_format, "O")) { // just pass an unmodified python repr
			char* str = va_arg(ap, char*);
			char* cp = req->format_buffer;
			growing_buffer_append(&req->format_buffer, &cp, &req->format_buffer_size, str);
			*cp = 0;
			field_value = req->format_buffer;
		} else if(!strcmp(field_format, "d") || !strcmp(field_format, "i")) {
			int v = va_arg(ap, int);
			field_value = strformat_(&req->format_buffer, &req->format_buffer_size, "%d", v);
		} else if(!strcmp(field_format, "u")) {
			unsigned int v = va_arg(ap, int);
			field_value = strformat_(&req->format_buffer, &req->format_buffer_size, "%u", v);
		} else if(!strcmp(field_format, "f")) {
			double v = va_arg(ap, double);
			field_value = strformat_(&req->format_buffer, &req->format_buffer_size, "%f", v);
		} else if(!strcmp(field_format, "data")) {
			// printf("append data!\n");
			void* data = va_arg(ap, void*);
			unsigned int data_len = va_arg(ap, unsigned int);
			char data_hdr[256];
			snprintf(data_hdr, 256, "data %d\n", data_len);
			// printf("%s", data_hdr);
			char* cp = req->format_buffer;
			growing_buffer_append(&req->format_buffer, &cp, &req->format_buffer_size, data_hdr);
			growing_buffer_appendn(&req->format_buffer, &cp, &req->format_buffer_size, (char*)data, data_len);
			growing_buffer_append(&req->format_buffer, &cp, &req->format_buffer_size, "\n");
			field_value = req->format_buffer;
		} else {
			setlocale(LC_ALL, "");
			return -LNE_INVALID_REQ_FIELD_FORMAT;
		}
		_ln_add_to_request(req, field_name, field_value);
	}
	va_end(ap);
	setlocale(LC_ALL, org_locale);
	return 0;
}

int _ln_is_in_request(request_t* req, const char* field_name) {
	for(unsigned int i = 0; i < req->n_lines; i++) {
		request_line_t* line = req->lines + i;
		if(!strcmp(line->header, field_name))
			return 1;
	}
	return 0;
}

int _ln_get_from_request(request_t* req, const char* field_name, const char* field_format, void* target) {
	char* field_value = NULL;
	for(unsigned int i = 0; i < req->n_lines; i++) {
		request_line_t* line = req->lines + i;
		if(!strcmp(line->header, field_name)) {
			field_value = line->value;
			break;
		}
	}
	if(!field_value) {
		_ln_set_error(req->clnt, "missing field in request: '%s'\n", field_name);
		return -LNE_FIELD_NOT_IN_HEADER;
	}
	if(!strcmp(field_format, "u")) {
		unsigned int* t = (unsigned int*)target;
		*t = (unsigned int)atol(field_value);
	} else if(!strcmp(field_format, "d")) {
		int* t = (int*)target;
		*t = atoi(field_value);
	} else if(!strcmp(field_format, "i")) {
		int* t = (int*)target;
		*t = atoi(field_value);
	} else if(field_format[0] == 's') {
		const char* len = field_format + 1;
		if(!*len || !isdigit(*len))
			return -LNE_STRING_LENGTH_SPEC_MISSING;
		unsigned int max_len = atoi(len);
		char* t = (char*)target;
		evalstr(t, max_len, field_value);
	} else if(field_format[0] == 'S') { // allocate needed memory!
		unsigned int len = strlen(field_value) + 1; // over estimate
		char** t = (char**)target;
		*t = (char*) malloc(len);
		evalstr(*t, len, field_value);
	}
	return 0;
}

int _ln_get_from_request_raw_named(request_t* req, const char* field_name, request_line_t** field) {
	for(unsigned int i = 0; i < req->n_lines; i++) {
		request_line_t* line = req->lines + i;
		if(!strcmp(line->header, field_name)) {
			*field = line;
			return 0;
		}
	}
	_ln_set_error(req->clnt, "missing raw field in request: '%s'\n", field_name);
	return -LNE_FIELD_NOT_IN_HEADER;
}

int _ln_get_count_from_request(request_t* req) {
	return req->n_lines;
}
int _ln_get_from_request_raw(request_t* req, int i, char** field_name, char** target) {
	if((unsigned)i >= req->n_lines) {
		_ln_set_error(req->clnt, "missing field in request: '%s'\n", field_name);
		return -LNE_FIELD_NOT_IN_HEADER;
	}
	request_line_t* line = req->lines + i;
	*field_name = line->header;
	*target = line->value;
	return 0;
}

int _ln_reset_request(request_t* req) {
	if(req->clnt->debug)
		ln_debug(req->clnt, "%s(req: %p, %d lines)\n", __func__, req, req->n_lines);
	
	for(unsigned int i = 0; i < req->n_lines; i++) {
		if(req->lines[i].do_free) {
			// if(req->clnt->debug) ln_debug(req->clnt, "%s free lines[%d].header %p\n", __func__, i, req->lines[i].header);
			free(req->lines[i].header);
			// if(req->clnt->debug) ln_debug(req->clnt, "%s free lines[%d].value %p\n", __func__, i, req->lines[i].value);
			free(req->lines[i].value);
		}
	}
	req->n_lines = 0;
	req->need_success = 1;
	return 0;
}
int _ln_destroy_request(request_t* req) {
	_ln_reset_request(req);
	free(req->binary_data_field);
	free(req->binary_data);
	free(req->format_buffer);
	free(req->lines);
	req->lines = 0;
	req->lines_size = 0;
	return 0;
}

int _ln_add_to_request_raw(request_t* req, char* header, void* value, unsigned int data_len, int do_free) {
	// header and data are not copied!!
	if(req->n_lines >= req->lines_size) {
		if(req->lines_size == 0)
			req->lines_size = 10;
		else
			req->lines_size *= 2;
		void* new_mem = realloc(req->lines, sizeof(request_line_t) * req->lines_size);
		if(!new_mem)
			return -LNE_NO_MEM;
		req->lines = (request_line_t*)new_mem;
	}
	req->lines[req->n_lines].header = header;
	req->lines[req->n_lines].value = (char*)value;
	req->lines[req->n_lines].binary_data_len = data_len;
	req->lines[req->n_lines].do_free = do_free;
	req->n_lines ++;

	return 0;
}
int _ln_add_to_request(request_t* req, const char* header, const char* value) {
	char* header_nc = strdup(header);
	char* value_nc = strdup(value);
	int ret = _ln_add_to_request_raw(req, header_nc, value_nc, 0, 1);
	if(ret) {
		// failed
		free(header_nc);
		free(value_nc);
	}
	return ret;
}

int _ln_format_request(request_t* req, char** bp, unsigned int* bp_len, int indent, int pretty) {
	const char* indent_buffer;
	if(indent)
		indent_buffer = "  ";
	else
		indent_buffer = "";
	if(!req->format_buffer) {
		req->format_buffer_size = 128;
		req->format_buffer = (char*)malloc(req->format_buffer_size);
		if(!req->format_buffer)
			return -LNE_NO_MEM;
	}
	req->format_buffer_cp = req->format_buffer;
	for(unsigned int i = 0; i < req->n_lines; i++) {
		request_line_t* line = req->lines + i;
		char* np;
		unsigned int in_buffer = req->format_buffer_cp - req->format_buffer;
		while(1) {
			unsigned int remaining = req->format_buffer_size - in_buffer;
			
			int n;
			if(line->binary_data_len) {
				if(pretty) {
					n = snprintf(req->format_buffer_cp, remaining - 2, 
						     "%s%s: (data %d)\n", 
						     indent_buffer, line->header, line->binary_data_len); 
					// -1 to be able to append \n after last line!
				} else {
					n = snprintf(req->format_buffer_cp, remaining - 2, 
						     "%s%s: data %d\n", 
						     indent_buffer, line->header, line->binary_data_len); 
					if(n > -1 && (unsigned)n < (remaining - 2)) {
						// header fits, append data
						req->format_buffer_cp += n;
						growing_buffer_appendn(
							&req->format_buffer, 
							&req->format_buffer_cp, 
							&req->format_buffer_size, 
							line->value, line->binary_data_len);
						growing_buffer_appendn(
							&req->format_buffer, 
							&req->format_buffer_cp, 
							&req->format_buffer_size, 
							"\n", 1);
						n = 0;
					}
				}
			} else
				n = snprintf(req->format_buffer_cp, remaining - 2, "%s%s: %s\n", indent_buffer, line->header, line->value); // -1 to be able to append \n after last line!
			if (n > -1 && (unsigned)n < (remaining - 2)) {
				// fits into buffer, success...
				req->format_buffer_cp += n;
				break;
			}
			// too small
			req->format_buffer_size *= 2;

			if ((np = (char*)realloc(req->format_buffer, req->format_buffer_size)) == NULL)
				return -LNE_NO_MEM;
			req->format_buffer = np;
			req->format_buffer_cp = req->format_buffer + in_buffer;
		}
	}
	*req->format_buffer_cp++ = '\n';
	*req->format_buffer_cp++ = 0;
	*bp = req->format_buffer;
	if(bp_len)
		*bp_len = (req->format_buffer_cp - req->format_buffer) -1;
	return 0;
}
int _ln_print_request(request_t* req) {
	int ret;
	char* bp;
	if((ret = _ln_format_request(req, &bp, NULL, 1, 1)))
		return ret;
	// printf("%s", bp);
	ln_debug(req->clnt, "%s", bp);
	return 0;
}

int _ln_send_all(int fd, char* data, unsigned int len)
{
	unsigned int remaining = len;
	char* dp = data;
	while(remaining) {
		int ret = send(fd, dp, remaining, 0);
		if(ret == -1) {
			if(_ln_platform_interrupted())
				continue;
			return -LNE_LOST_CONNECTION;
		}
		remaining -= ret;
		dp += ret;
	}
	return len;
}

int _ln_send_request(request_t* r) {
	int ret;

	if(r->clnt->debug) {
		ln_debug(r->clnt, "send request:\n");
		_ln_print_request(r);
	}

	char* buffer;
	unsigned int buffer_len;
	if((ret = _ln_format_request(r, &buffer, &buffer_len, 0, 0)))
		return ret;

	int n = _ln_send_all(r->clnt->sfd, buffer, buffer_len);
	if(n < 0) {
		_ln_set_platform_error_message(r->clnt, "send_request: ");
		return n;
	}
	
	if((unsigned)n != buffer_len)
		printf("wanted to send request with %d bytes but only %d bytes got thru!\n", buffer_len, n);
	
	return 0;
}

int _ln_process_manager_request(request_t* r) {
	client* clnt = r->clnt;
	int ret;
	char value[128];
	if((ret = _ln_get_from_request(r, "method", "s128", value))) {
		//return -LNE_GOT_INVALID_RESPONSE;
		fprintf(stderr, "libln: async thread received invalid request - method: %s\nreceived this request:\n", ln_format_error(ret));
		_ln_print_request(r);
		return -1;
	}
	unsigned int request_id;
	if((ret = _ln_get_from_request(r, "request_id", "u", &request_id))) {
		fprintf(stderr, "libln: async thread received invalid request - request_id: %s\n", ln_format_error(ret));
		return 0;
	}

	if(!strcmp(value, "update_subscribers")) {
		char topic[128];
		if(_ln_get_from_request(r, "topic", "s128", topic)) {
			fprintf(stderr, "libln: async thread received invalid request\n");
			return -1;
		}
		int n_subscribers;
		if(_ln_get_from_request(r, "n_subscribers", "i", &n_subscribers)) {
			fprintf(stderr, "libln: async thread received invalid request\n");
			return -1;
		}
		if(clnt->outports) {
			for(unsigned int i = 0; i < clnt->n_outports; i++) {
				outport* op = clnt->outports[i];
				if(!strcmp(op->hdr.topic, topic)) {
					op->has_subscriber = n_subscribers;
					break;
				}
			}
		}
	} else if(!strcmp(value, "update_publishers")) {
		char topic[128];
		if((ret = _ln_get_from_request(r, "topic", "s128", topic))) {
			fprintf(stderr, "libln: async thread received invalid request - topic: %s\n", ln_format_error(ret));
			return -1;
		}
		int n_publishers;
		if((ret = _ln_get_from_request(r, "n_publishers", "i", &n_publishers))) {
			fprintf(stderr, "libln: async thread received invalid request - n_publishers: %s\n", ln_format_error(ret));
			return -1;
		}
		if(clnt->inports) {
			for(unsigned int i = 0; i < clnt->n_inports; i++) {
				inport* op = clnt->inports[i];
				if(!strcmp(op->hdr.topic, topic)) {
					op->has_publisher = n_publishers;
					break;
				}
			}
		}
	} else if(!strcmp(value, "get_port_tids")) {
		char topic[128];
		if((ret = _ln_get_from_request(r, "topic", "s128", topic))) {
			fprintf(stderr, "libln: async thread received invalid request - topic: %s\n", ln_format_error(ret));
			return -1;
		}
		char direction[16];
		if((ret = _ln_get_from_request(r, "direction", "s16", direction))) {
			fprintf(stderr, "libln: async thread received invalid request - topic: %s\n", ln_format_error(ret));
			return -1;
		}
		
		port* p = NULL;
		if(!strcmp(direction, "input") && clnt->inports) {
			unsigned int subscription_id = 0;
			_ln_get_from_request(r, "subscription_id", "i", &subscription_id);
			
			for(unsigned int i = 0; i < clnt->n_inports; i++) {
				inport* op = clnt->inports[i];
				if(subscription_id == 0) {
					if(!strcmp(op->hdr.topic, topic)) {
						p = &op->hdr;
						break;
					}
				} else {
					if(op->subscription_id == subscription_id) {
						p = &op->hdr;
						break;
					}
				}
			}
		} else if(!strcmp(direction, "output") && clnt->outports) {
			for(unsigned int i = 0; i < clnt->n_outports; i++) {
				outport* op = clnt->outports[i];
				if(!strcmp(op->hdr.topic, topic)) {
					p = &op->hdr;
					break;
				}
			}
		}

		request_t resp;
		_ln_init_request(
			clnt, &resp,
			"answer_to", "r", "get_port_tids",				
			"topic", "r", topic,
			"direction", "r", direction,
			"request_id", "u", request_id,
			NULL);
		
		char* repr_value = NULL;
		unsigned int repr_value_size = 0;
		
		if(!p) {
			// return error: port not found!
			_ln_add_to_request(&resp, "success", "'error: port not found!'");
		} else {
			_ln_add_to_request(&resp, "success", "'success'");
			p->request_tid = 1;
			strformat_(&repr_value, &repr_value_size, "%uL", (unsigned long)p->last_tid);
			_ln_add_to_request(&resp, "last_tid", repr_value);
		}
		_ln_send_request(&resp);
		_ln_destroy_request(&resp);
		if(repr_value)
			free(repr_value);
	       
	} else if(!strcmp(value, "get_svn_diff")) {
		request_t resp;
		_ln_init_request(
			clnt, &resp,
			"answer_to", "r", "get_svn_diff",				
			"success", "r", "success",				
			"svn_revision", "d", 0,
			"is_modified", "d", 0,
			"svn_diff", "r", "",
			"request_id", "u", request_id,
			NULL);
		_ln_send_request(&resp);
		_ln_destroy_request(&resp);

	} else if(!strcmp(value, "call_service")) {
		char service_name[256];
		if((ret = _ln_get_from_request(r, "name", "s256", service_name))) {
			fprintf(stderr, "libln: async thread received invalid call_service request - service_name: %s\n", ln_format_error(ret));
			return 0;
		}
		request_line_t* field;
		if((ret = _ln_get_from_request_raw_named(r, "request", &field))) {
			fprintf(stderr, "libln: async thread received invalid call_service request - request: %s\n", ln_format_error(ret));
			_ln_print_request(r);
			return 0;
		}
		/*
		  field->value
		  field->binary_data_len
		*/
		
		if(clnt->services && clnt->n_services) {
			int found = false;
			for(unsigned int i = 0; i < clnt->n_services; i++) {
				struct service_* service = clnt->services[i];
				if(!service->is_provider || !service->is_listening)
					continue;
				if(strcmp(service->name, service_name))
					continue;
				// found!
				found = true;
				ln_debug(clnt, "%s: call_service %s: service found!\n", __func__, service_name);

				service_client* client = (service_client*)malloc(sizeof(service_client));
				memset(client, 0, sizeof(service_client));
				client->svc = service;
				client->buffer = (char*)malloc(field->binary_data_len);
				memcpy(client->buffer, field->value, field->binary_data_len);
				client->buffer_len = client->buffer_size = field->binary_data_len;
				/*
				  printf("got %d bytes of request data at %p:\n", field->binary_data_len, client->buffer);
				for(unsigned int i = 0; i < field->binary_data_len; i++) {
				printf("%02x ", ((uint8_t*)client->buffer)[i]);
				}
				printf("\n");
				*/
				client->fd = -1;
				client->is_fake = 1;
				client->fake_request_id = request_id;
				client->is_pending = 1;
				_ln_service_append_fake_service_clients(service, client);
				clnt->had_new_fake_service_client = 1;

				if(service->n_log_services)
					ln_debug(clnt, "%s: would have to call log services now for sending fake request!\n", __func__);
				
				// now need to wake service loop!
				if(service->new_provider_fd_notification_pipe.input_fd != ln_pipe_invalid) {
					ln_debug(clnt, "%s: call_service %s: wake via provider notification fd!\n", __func__, service_name);
					ln_pipe_send_notification(service->new_provider_fd_notification_pipe); // use provider fd
				} else if(service->group && service->group->notification_pipe.input_fd != ln_pipe_invalid) {
					ln_debug(clnt, "%s: call_service %s: wake via service group notification!\n", __func__, service_name);
					ln_pipe_send_notification(service->group->notification_pipe);
				} else if(service->fd != -1) {
					ln_debug(clnt, "%s: call_service %s: wake via tcp connect, close on tcp socket!\n", __func__, service_name);
					struct sockaddr_in provider_address;
					provider_address.sin_family = AF_INET;
					provider_address.sin_port = htons(service->service_port);
					provider_address.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
					int sfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

					if(sfd != -1)
						connect(sfd, (struct sockaddr*)&provider_address, sizeof(provider_address));
					_ln_close_socket(sfd);
				} else
					ln_debug(clnt, "%s: call_service %s: can not wake service handling loop! no provider-notification-fd and no tcp port bound!\n",
						 __func__, service_name);

				break;
			}
			if(!found)
				ln_debug(clnt, "%s: call_service %s: service not found!\n", __func__, service_name);
		} else
			ln_debug(clnt, "%s: call_service %s: no services defined!\n", __func__, service_name);
		
	} else if(!strcmp(value, "delayed_init_inport")) {
		char topic[256];
		if((ret = _ln_get_from_request(r, "topic", "s256", topic))) {
			fprintf(stderr, "libln: async thread received invalid request - topic: %s\n", ln_format_error(ret));
			return 0;
		}
		char shm_name[1024];
		if((ret = _ln_get_from_request(r, "shm_name", "s256", shm_name))) {
			fprintf(stderr, "libln: async thread received invalid request - shm_name: %s\n", ln_format_error(ret));
			return 0;
		}
		bool found = false;
		for(unsigned int i = 0; i < clnt->n_inports; i++) {
			inport* op = clnt->inports[i];
			if(!strcmp(op->hdr.topic, topic)) {
				found = true;
				ret = _ln_delayed_init_inport(op, shm_name);
				if(ret != 0)
					break;
			}
		}

		request_t resp;
		_ln_init_request(
			clnt, &resp,
			"answer_to", "r", "delayed_init_inport",				
			"request_id", "u", request_id,
			"retval", "d", ret,
			NULL);
		
		if(!found) {
			// return error: port not found!
			_ln_add_to_request(&resp, "success", "'error: port not found!'");
		} else {
			if(ret == 0)
				_ln_add_to_request(&resp, "success", "'success'");
			else
				_ln_add_to_request(&resp, "success", "'error wrong return code'");
		}
		_ln_send_request(&resp);
		_ln_destroy_request(&resp);
		
	} else if(!strcmp(value, "republish_outport")) {
		char topic[256];
		if((ret = _ln_get_from_request(r, "topic", "s256", topic))) {
			fprintf(stderr, "libln: async thread received invalid request - topic: %s\n", ln_format_error(ret));
			return 0;
		}
		char shm_name[1024];
		if((ret = _ln_get_from_request(r, "shm_name", "s256", shm_name))) {
			fprintf(stderr, "libln: async thread received invalid request - shm_name: %s\n", ln_format_error(ret));
			return 0;
		}
		outport* p = NULL;
		for(unsigned int i = 0; i < clnt->n_outports; i++) {
			outport* op = clnt->outports[i];
			if(!strcmp(op->hdr.topic, topic)) {
				p = op;
				break;
			}
		}
		if(p)
			ret = _ln_reinit_outport(p, shm_name);

		request_t resp;
		_ln_init_request(
			clnt, &resp,
			"answer_to", "r", "republish_outport",				
			"request_id", "u", request_id,
			"retval", "d", ret,
			NULL);
		
		if(!p) {
			// return error: port not found!
			_ln_add_to_request(&resp, "success", "'error: port not found!'");
		} else {
			if(ret == 0)
				_ln_add_to_request(&resp, "success", "'success'");
			else
				_ln_add_to_request(&resp, "success", "'error wrong return code'");
		}
		_ln_send_request(&resp);
		_ln_destroy_request(&resp);
	} else if(!strcmp(value, "enable_debug")) {
		int enable;
		if((ret = _ln_get_from_request(r, "enable", "d", &enable))) {
			fprintf(stderr, "libln: async thread received invalid enable_debug request - enable: %s\n", ln_format_error(ret));
			return 0;
		}
		clnt->debug = enable;
		if(clnt->debug)
			ln_debug(clnt, "enabled debug output\n");

		request_t resp;
		_ln_init_request(
			clnt, &resp,
			"answer_to", "r", "enable_debug",				
			"request_id", "u", request_id,
			"success", "r", "success",
			NULL);
		
		_ln_send_request(&resp);
		_ln_destroy_request(&resp);
	} else if(!strcmp(value, "request_tcp_port_for_service")) {
		char service_name[256];
		if((ret = _ln_get_from_request(r, "service_name", "s256", service_name))) {
			fprintf(stderr, "libln: async thread received invalid request - service_name: %s\n", ln_format_error(ret));
			return 0;
		}
		service* p = NULL;
		for(unsigned int i = 0; i < clnt->n_services; i++) {
			service* op = clnt->services[i];
			if(op->is_provider && !strcmp(op->name, service_name)) {
				p = op;
				break;
			}
		}
		int service_port = -1;
		if(p)
			service_port = _ln_service_request_tcp_port(p);

		request_t resp;
		_ln_init_request(
			clnt, &resp,
			"answer_to", "r", "request_tcp_port_for_service",				
			"request_id", "u", request_id,
			"tcp_port", "d", service_port,
			NULL);
		
		if(!p) {
			// return error: port not found!
			_ln_add_to_request(&resp, "success", "'error: service not found!'");
		} else {
			if(service_port > 0)
				_ln_add_to_request(&resp, "success", "'success'");
			else
				_ln_add_to_request(&resp, "success", "'error wrong return code'");
		}
		_ln_send_request(&resp);
		_ln_destroy_request(&resp);
		if(p) {
			ln_debug(clnt, "send notification to service group!\n");
			ln_pipe_send_notification(p->group->notification_pipe);
		}
		
	} else if(!strcmp(value, "request_unix_socket_for_service")) {
		char service_name[256];
		if((ret = _ln_get_from_request(r, "service_name", "s256", service_name))) {
			fprintf(stderr, "libln: async thread received invalid request - service_name: %s\n", ln_format_error(ret));
			return 0;
		}
		service* p = NULL;
		for(unsigned int i = 0; i < clnt->n_services; i++) {
			service* op = clnt->services[i];
			if(op->is_provider && !strcmp(op->name, service_name)) {
				p = op;
				break;
			}
		}

		request_t resp;
		
		if(!p) {
			// return error: port not found!
			_ln_init_request(
				clnt, &resp,
				"answer_to", "r", "request_unix_socket_for_service",				
				"request_id", "u", request_id,
				"success", "r", "error: service not found!",
				NULL);
		} else {
			ret = _ln_service_request_unix_socket(p);
			if(ret == 0) {
				_ln_init_request(
					clnt, &resp,
					"answer_to", "r", "request_unix_socket_for_service",				
					"request_id", "u", request_id,
					"unix_socket", "r", p->unix_socket_name,
					"success", "r", "success",
					NULL);
			} else {
				_ln_init_request(
					clnt, &resp,
					"answer_to", "r", "request_unix_socket_for_service",				
					"request_id", "u", request_id,
					"success", "r", "error: wrong return code",
					"retval", "d", ret,
					"errno", "d", errno,
					NULL);
			}
		}
		_ln_send_request(&resp);
		_ln_destroy_request(&resp);
		if(p) {
			ln_debug(clnt, "send notification to service group!\n");		
			ln_pipe_send_notification(p->group->notification_pipe);
		}
		
	} else if(!strcmp(value, "add_service_logger")) {
		char service_name[256];
		if((ret = _ln_get_from_request(r, "service_name", "s256", service_name))) {
			fprintf(stderr, "libln: async thread received invalid request - service_name: %s\n", ln_format_error(ret));
			return 0;
		}
		service* s = NULL;
		for(unsigned int i = 0; i < clnt->n_services; i++) {
			service* op = clnt->services[i];
			if(!strcmp(op->name, service_name)) {
				s = op;
				break;
			}
		}
		request_t resp;
		
		if(!s) {
			// return error: port not found!
			_ln_init_request(
				clnt, &resp,
				"answer_to", "r", "add_service_logger",
				"request_id", "u", request_id,
				"success", "r", "error: service not found!",
				NULL);
		} else {
			char logger_service_name[256];
			if((ret = _ln_get_from_request(r, "logger_service_name", "s256", logger_service_name))) {
				fprintf(stderr, "libln: async thread received invalid request - logger_service_name: %s\n", ln_format_error(ret));
				return 0;
			}
			log_service* ls;
			if((ret = _ln_client_add_log_service(clnt, logger_service_name, &ls))) {
				fprintf(stderr, "libln: asyn thread received add_service_logger request but failed to create log_service client!");
				return 0;
			}
			if((ret = _ln_service_add_log_service(s, ls))) {
				fprintf(stderr, "libln: asyn thread received add_service_logger request but failed to add log_service to service!");
				return 0;
			}
			_ln_init_request(
				clnt, &resp,
				"answer_to", "r", "add_service_logger",
				"request_id", "u", request_id,
				"success", "r", "success",
				NULL);
		}
		_ln_send_request(&resp);
		_ln_destroy_request(&resp);
		
	} else if(!strcmp(value, "del_service_logger")) {
		char service_name[256];
		if((ret = _ln_get_from_request(r, "service_name", "s256", service_name))) {
			fprintf(stderr, "libln: async thread received invalid request - service_name: %s\n", ln_format_error(ret));
			return 0;
		}
		char logger_service_name[256];
		if((ret = _ln_get_from_request(r, "logger_service_name", "s256", logger_service_name))) {
			fprintf(stderr, "libln: async thread received invalid request - logger_service_name: %s\n", ln_format_error(ret));
			return 0;
		}
		
		request_t resp;
		
		service* s = NULL;
		for(unsigned int i = 0; i < clnt->n_services; i++) {
			service* op = clnt->services[i];
			if(!strcmp(op->name, service_name)) {
				s = op;
				break;
			}
		}
		log_service* ls = NULL;
		_ln_client_get_log_service(clnt, logger_service_name, &ls);
		
		if(!s) {
			// return error: port not found!
			_ln_init_request(
				clnt, &resp,
				"answer_to", "r", "del_service_logger",
				"request_id", "u", request_id,
				"success", "r", "error: service not found!",
				NULL);
		} else if(!ls) {
			_ln_init_request(
				clnt, &resp,
				"answer_to", "r", "del_service_logger",
				"request_id", "u", request_id,
				"success", "r", "error: log_service not found!",
				NULL);
		} else if((ret = _ln_service_del_log_service(s, ls))) {
			_ln_init_request(
				clnt, &resp,
				"answer_to", "r", "del_service_logger",
				"request_id", "u", request_id,
				"success", "r", "error: failed to remove log_service!",
				NULL);				
		} else {
			_ln_init_request(
				clnt, &resp,
				"answer_to", "r", "del_service_logger",
				"request_id", "u", request_id,
				"success", "r", "success",
				NULL);
		}
		_ln_send_request(&resp);
		_ln_destroy_request(&resp);
	}
	return 0;
}

void _check_binary_data_finished(request_t* resp) {
	if(resp->binary_data && resp->binary_data_pos == resp->binary_data_len) {
		// binary data finished! transfer to request line!
		// printf("binary data '%s' ready\n", resp->binary_data_field);
		_ln_add_to_request_raw(resp, resp->binary_data_field, resp->binary_data, resp->binary_data_len - 1, 1); // terminating '\n'
		// giveup ownership of this buffer!
		resp->binary_data = NULL;
		resp->binary_data_field = NULL;
		resp->binary_data_pos = 0;
		resp->binary_data_len = 0;
	}
}

int _on_complete_line_cb(void* data, char* line, unsigned int line_len, void* remaining_data, unsigned int remaining_data_len, unsigned int* skip_bytes) {
	request_t* r = (request_t*) data;

	char* header_name = line;
	char* header_value = (char*)memchr(line, ':', line_len);

	if(!header_value || header_value >= (line + line_len))
		return 1; // invalid

	char* name_end = header_value;

	header_value ++; // skip :

	while(*header_value == ' ' && *header_value)
		header_value++;

	if(strncmp(header_value, "data ", 5))
		return 1; // not binary data

	// receive binary data!
	// printf("complete line binary data: '%*.*s'\n", line_len, line_len, line);
	*name_end = 0; // terminate name

	r->binary_data_field = strdup(header_name);
	char* cp = header_value + 5;
	char number[64];
	unsigned int n_chars = line_len - (cp - line);
	snprintf(number, 64, "%*.*s", n_chars, n_chars, cp);
	r->binary_data_len = (unsigned int)strtoul(number, NULL, 0) + 1; // terminating '\n'
	//printf("number: '%s': %d\n", number, r->binary_data_len - 1);
	void* new_mem = realloc(r->binary_data, r->binary_data_len);
	if(!new_mem)
		return 1;
	r->binary_data = (uint8_t*)new_mem;
	// printf("will await %d bytes of binary data for %s!\n", r->binary_data_len, r->binary_data_field);

	// transfer already received data from line_assembler!
	unsigned int to_take = MIN(r->binary_data_len, remaining_data_len);
	memcpy(r->binary_data, remaining_data, to_take);
	r->binary_data_pos = to_take;

	_check_binary_data_finished(r);
	
	*skip_bytes = to_take;
	return 0; // do not append this line
}

int _ln_wait_response(request_t* resp, int async) {
	/*
	  you need to have the comm-lock!
	  
	  read complete request object from stream.
	  if it has a "method"-field, pass it to _ln_process_manager_request() (we probably have async=1)
	  if it has a "answer_to"-field, we likely have async=0 and want to receive a sync response.

	  difficulty is that reading a RECEIVER_BUFFER_SIZE'ed chunk from tcp stream might include multiple request objects
	 */
	int ret;

	resp->clnt->line_assembler->on_complete_line_cb = _on_complete_line_cb;
	resp->clnt->line_assembler->on_complete_line_cb_data = resp;

	while(true) {
		int response_finished = 0;
		// char input[1024];
		while(!response_finished) {
			char* line;
			while(true) {
				_check_binary_data_finished(resp);

				if(!resp->binary_data) {
					// no binary data to read...
					line = get_next_from_line_assembler(resp->clnt->line_assembler);
				} else {
					// receive into binary data buffer!
					unsigned int to_take = resp->binary_data_len - resp->binary_data_pos;
					ret = _ln_read_from_manager(resp->clnt, (resp->binary_data + resp->binary_data_pos), to_take);
					if(ret < 0)
						return ret;
					resp->binary_data_pos += ret;
					continue;
				}
				if(!line) {
					char byte;
					ret = _ln_read_from_manager(resp->clnt, &byte, 1);
					if(ret != 1)
						return ret;
					// feed only one char into la, ugly fix for #93
					if((ret = write_to_line_assembler(resp->clnt->line_assembler, &byte, 1)))
						return ret;
					continue;
				}
				
				// process line
				/*
				if(resp->clnt->debug) {
					char dbgstr[1024];
					ln_debug(resp->clnt, "line from manager: '%s'\n", strrepr(dbgstr, 1024, line));
				}
				*/

				char* cp = strchr(line, ':');
				if(!cp && strlen(line) == 0) {
					// response is finished
					response_finished = 1;
					ln_debug(resp->clnt, "response finished!\n");
					break;
				}
				if(!cp) {
					char rl[512];
					printf("received invalid request-line: %s\n", strrepr(rl, 512, line));
					continue;
				}
				*(cp++) = 0;
				char* header_name = line;
				char* header_value = cp;

				while(*header_value == ' ' && *header_value)
					header_value++;
				if(!*header_value) {
					char hr[128];
					char vr[128];
					printf("received invalid request header-value: %s to header-name %s\n", 
						   strrepr(vr, 128, cp),
						   strrepr(hr, 128, header_name));
					continue;
				}
				if((ret = _ln_add_to_request(resp, header_name, header_value)))
					return ret;
			}
		}
		if(resp->clnt->debug) {
			ln_debug(resp->clnt, "wait_response 0x%x received:\n", get_thread_id());
			_ln_print_request(resp);
		}

		if(_ln_is_in_request(resp, "method")) {
			// have method field!
			char value[32];
			_ln_get_from_request(resp, "method", "s32", value);
			// manager sent request to us - process this first
			if(!async)
				ln_debug(resp->clnt, "manager sent async method '%s' - will process that now.\n", value);
			char need_success = resp->need_success;
			_ln_process_manager_request(resp);
			_ln_reset_request(resp);
			if(async)
				return 0; // processed async response...
			
			resp->need_success = need_success;
			// printf("wait for response got ln_manager_request!\n");
			// _ln_print_request(resp);
			ln_debug(resp->clnt, "will now wait for real response!\n");
			continue;
		}

		if(!_ln_is_in_request(resp, "answer_to")) {
			printf("received illegal/out-of-sync request:\n");
			_ln_print_request(resp);
			continue;
		}
		break;
	}
	_ln_set_error(resp->clnt, "");

	if(resp->need_success) {
		char value[1024];
		if(_ln_get_from_request(resp, "success", "s1024", value)) {
			printf("ln error: no 'success' field in this response:\n");
			_ln_print_request(resp);
			return -LNE_GOT_INVALID_RESPONSE;
		}
		if(strcmp(value, "success")) {
			_ln_set_error(resp->clnt, "%s", value);
			return -LNE_REQUEST_FAILED;
		}
	}
	resp->clnt->line_assembler->on_complete_line_cb = NULL;
	resp->clnt->line_assembler->on_complete_line_cb_data = NULL;

	return 0;
}

int _ln_request_and_wait(request_t* r) {
	// you need to have the comm-lock!
	int ret = _ln_send_request(r);
	if(ret)
		return ret;

	// reuse the request object as response object
	_ln_reset_request(r);
	ret = _ln_wait_response(r, 0);
	return ret;
}

void _ln_lock_request(request_t* r) {
	ln_mutex_lock(&r->clnt->communication_mutex);
}
void _ln_unlock_request(request_t* r) {
	ln_mutex_unlock(&r->clnt->communication_mutex);
}

#ifdef __cplusplus
}
#endif
