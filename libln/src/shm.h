#ifndef SHM_H
#define SHM_H

#include "os.h"

#define LN_SHM_MAGIC  "LN_SHM"
//#define LN_SHM_VERSION 0 // first shm version
//#define LN_SHM_VERSION 1 // n_buffers & shm header
//#define LN_SHM_VERSION 2 // elements are 8-byte aligned
#define LN_SHM_VERSION 3 // support for direct linux futex lockless signaling

typedef struct {
	uint32_t data_counter;
	double data_timestamp;

	// data starts here
	// with v3 we have an additional uint32_t data_counter_write_finished; after the actual element data
} shm_header_element_t;
#define SHM_ELEMENT_GET_DATA(element_ptr) (void*)((element_ptr) + 1)
#define SHM_ELEMENT_GET_DATA_CNT(element_ptr) (*(volatile uint32_t*)&(element_ptr)->data_counter)
#define SHM_ELEMENT_GET_DATA_CNT_WRITE_FIN(shm, element_ptr) (*(volatile uint32_t*)((uint8_t*)(element_ptr) + (shm)->element_step - sizeof(uint32_t))) // >= v3

typedef struct {
	char ln_shm_magic[7]; // LN_SHM\0
	unsigned char version; // >= v1
	
	unsigned int element_size;
	unsigned int n_elements;
	ln_process_shared_mutex_t mutex;
	ln_process_shared_cond_t cond;
	unsigned int next_write; // element
	unsigned int is_full;
	
	uint32_t last_data_counter; // for multiwaiter and with v3 on linux, this will be used as futex uaddr with last written counter as value

	shm_header_element_t first_element;
} shm_header_t;

typedef struct {
	ln_shared_mem_handle_t shared_mem;
	shm_header_t* hdr; // this points to the shared memory
	unsigned int next_read;
	unsigned int unblock;
	unsigned int element_step; // ceil(hdr->element_size / 8) * 8
} shm_t;

#define SHM_GET_ELEMENT(shm, element) (shm_header_element_t*)((uint8_t*)&shm->hdr->first_element + (element) * shm->element_step)
#define SHM_GET_NEXT_ELEMENT(shm, element) (((element + 1) == shm->hdr->n_elements) ? 0 : (element + 1))
#define SHM_GET_PREV_ELEMENT(shm, element) ((element == 0) ? (shm->hdr->n_elements - 1) : (element - 1))

int open_shm(shm_t** shm_, const char* name);
int create_shm(shm_t** shm_, const char* name, unsigned int element_size, unsigned int n_elements, unsigned int version);

int destroy_shm(shm_t* shm);
int close_shm(shm_t* shm);


#endif // SHM_H
