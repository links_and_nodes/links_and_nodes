#ifndef REPR_H
#define REPR_H

int growing_buffer_append(char** base, char** cp, unsigned int* size, const char* source);
int growing_buffer_appendn(char** base, char** user_cp, unsigned int* size, const char* source, unsigned int source_len);

char* strrepr(char* target, unsigned int max_len, const char* source);
char* strrepr_(char** target, unsigned int* max_len, const char* source); // will use a growing buffer

char* strnrepr(char* target, unsigned int max_len, const char* source, unsigned int source_len);
char* strnrepr_(char** target, unsigned int* max_len, const char* source, unsigned int source_len); // will use a growing buffer

char* strformat(char* target, unsigned int max_len, const char* format, ...);
char* strformat_(char** target, unsigned int* max_len, const char* format, ...); // will use a growing buffer

char* evalstr(char* target, unsigned int max_len, const char* s);
char* evalstr_(char** target, unsigned int* max_len, const char* s); // will use a growing buffer

#endif // REPR_H
