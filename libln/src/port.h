#ifndef PORT_H
#define PORT_H

struct port_;
typedef struct port_ port;
struct outport_;
typedef struct outport_ outport;
struct inport_;
typedef struct inport_ inport;

#include "ln/ln.h"
#include "client.h"


typedef enum {
	OUTPUT_PORT,
	INPUT_PORT
} port_type;

struct port_ {
	client* clnt;
	char* topic;
	char* message_definition;
	char* shm_name;
	unsigned int message_size;
	char* msg_def_hash;
	port_type type;
	uint32_t packet_counter; // <- this is needed!
	// no longer needed -> no extra copies! void* packet;

	uint8_t request_tid;
	tid_t last_tid;

};

// ports
int _ln_init_port(client* clnt, port* p, const char* topic, const char* message_definition, unsigned int message_size, char* msg_def_hash, port_type t, const char* shm_name);
int _ln_destroy_port(port* p);

int _ln_get_message_size(port* p);
const char* _ln_get_msg_def_hash(port* p);
int _ln_get_message_string(port* p, char* message_def_string, int maxlen);



// outports
struct outport_ {
	port hdr;
	unsigned int has_subscriber;
	
	shm_target_t target;
	int have_shm_target;

	shm_target_t new_target;
	int have_new_shm_target;

};

int _ln_init_outport(client* clnt, outport* p, const char* topic, const char* message_definition, unsigned int message_size, char* msg_def_hash, const char* shm_name);
int _ln_reinit_outport(outport* p, const char* shm_name);
int _ln_outport_destroy(outport* p);

int _ln_has_subscriber(outport* p);
int _ln_write(outport* p, void* data, unsigned int len, double* timestamp);
int _ln_outport_get_next_buffer(outport* p, void** buffer, unsigned int* buffer_len);


// inports
struct inport_ {
	port hdr;
	unsigned int has_publisher;
	shm_source_t source;
	int have_shm_source;
	double rate;
	int transport;
	int can_read; // for multi waiter
	
	ln_service on_request_svc;
	uint8_t* needs_endianess_swap;

	int have_delayed_shm_sem;
	ln_semaphore_handle_t delayed_shm_sem;
	unsigned int subscription_id;
	unsigned int buffers;
};

int _ln_init_inport(client* clnt, inport* p, const char* topic, const char* message_definition, unsigned int message_size, char* msg_def_hash, double rate, const char* shm_name);
int _ln_delayed_init_inport(inport* p, const char* shm_name);
int _ln_inport_destroy(inport* p);

int _ln_has_publisher(inport* p);
int _ln_read(inport* p, void* data, unsigned int len, double* timestamp, int blocking);
int _ln_read_timeout(inport* p, void* data, unsigned int len, double* ts, double timeout);
int _ln_block_only(inport* p, uint32_t* last_packet_counter, double timeout);



#endif // PORT_H
