#ifndef OS_QNX_H
#define OS_QNX_H

#include <pthread.h>
#include <sys/select.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

#include "ln/ln.h"

// switches
// #define USE_OWN_PROCESS_SHARED_IPC
#define USE_POSIX_SHM

// defined in ln/ln.h typedef int ln_pipe_fd_t;
struct ln_pipe {
	ln_pipe_fd_t input_fd;
	ln_pipe_fd_t output_fd;
};
#define ln_pipe_invalid -1

typedef void* iov_base_t;
typedef socklen_t os_socklen_t;
#define _ln_socket_t int

#define MAX_SHARED_MEM_LEN 1023
// shared memory type
typedef struct {
	char name[MAX_SHARED_MEM_LEN + 1];
	unsigned int size;
	void* mem;
} ln_shared_mem_handle_t;

// semaphore type
typedef sem_t* ln_semaphore_handle_t;

// mutex type
typedef pthread_mutex_t ln_mutex_handle_t;
typedef pthread_cond_t ln_cond_handle_t;

// thread types
typedef pthread_t ln_thread_handle_t;
typedef void *(thread_func_type)(void*);
typedef void* thread_return_t;
#define THREAD_RETURN_NULL NULL

typedef pthread_t tid_t;
#define gettid() pthread_self()
#define get_thread_id() gettid()

// compatibility fixes
char* strndup(const char* src, unsigned int src_len);

// no such thing in qnx
#define MAP_LOCKED 0
#define MAP_POPULATE 0

#ifndef USE_OWN_PROCESS_SHARED_IPC
 typedef pthread_mutex_t ln_process_shared_mutex_t;
 typedef pthread_cond_t ln_process_shared_cond_t;
#endif

#define thread_cleanup_push pthread_cleanup_push
#define thread_cleanup_pop pthread_cleanup_pop

#define LOW_PRIO 10
#define LOW_PRIO_POLICY SCHED_RR

#ifndef IOV_MAX
#define IOV_MAX 64
#endif

#endif // OS_QNX_H
