/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <stdio.h>
#include <stdlib.h>

#include "ln/ln.h"
#include "client.h"
#include "request.h"
#include "port.h"

#include "ln/client_lib_messages.h"

int ln_subscribe(ln_client clnt_, const char* topic_name, const char* message_definition_name, ln_inport* port, double rate, int reliable_transport) {
	return ln_subscribe_with_buffers(clnt_, topic_name, message_definition_name, port, rate, reliable_transport, 1);
}
int ln_subscribe_with_buffers(ln_client clnt_, const char* topic_name, const char* message_definition_name, ln_inport* port, double rate, int reliable_transport, unsigned int buffers) {
	client* clnt = (client*)clnt_;

	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	char* topic = _get_topic_mapping(clnt, topic_name, "subscribed");
	int ret;
	while(true) {
		request_t r;
		ret = _ln_init_request(
			clnt, &r,
			"method", "r", "subscribe",
			"topic", "r", topic,
			"message_definition_name", "r", message_definition_name,
			"rate", "f", rate,
			"buffers", "d", buffers,
			"reliable_transport", "d", reliable_transport,
			NULL);
		if(ret)
			break;

		ln_mutex_lock(&clnt->communication_mutex);
		ret = _ln_request_and_wait(&r);
		ln_mutex_unlock(&clnt->communication_mutex);
		if(ret) {
			_ln_destroy_request(&r);
			break;
		}

		unsigned int message_size;
		if((ret = _ln_get_from_request(&r, "message_size", "u", &message_size))) {
			_ln_destroy_request(&r);
			break;
		}

		char shm_name[MAX_SHARED_MEM_LEN + 1];
		char name_format[32];
		snprintf(name_format, 32, "s%d", MAX_SHARED_MEM_LEN);
		if((ret = _ln_get_from_request(&r, "shm_name", name_format, &shm_name))) {
			_ln_destroy_request(&r);
			break;
		}

		int has_publisher = 0;
		if((ret = _ln_get_from_request(&r, "has_publisher", "i", &has_publisher))) {
			// nothing...
		}

		unsigned int subscription_id = 0;
		_ln_get_from_request(&r, "subscription_id", "i", &subscription_id);

		char* msg_def_hash = NULL;
		if(clnt->manager_library_version >= 17 && (ret = _ln_get_from_request(&r, "msg_def_hash", "S", &msg_def_hash))) {
			// nothing...
		}
		
		_ln_destroy_request(&r);

		// create inport
		inport* p = (inport*)malloc(sizeof(inport)); // will be free'd on library deinit!
		p->transport = reliable_transport; // todo: will get destroyed by memset in init_inport!
		if(!p) {
			ln_debug(clnt, "ln_subscribe() malloc() for inport failed!\n");
			ret = -LNE_NO_MEM;
			break;
		}
		if((ret = _ln_init_inport(clnt, p, topic, message_definition_name, message_size, msg_def_hash, rate, (const char*)shm_name)))
			break;

		p->has_publisher = has_publisher;
		p->subscription_id = subscription_id;
		p->transport = reliable_transport;
		p->buffers = buffers;
	
		*port = p;
		ret = 0;
		break;
	}
	
	free(topic);	
	return ret;
}

int ln_unsubscribe(ln_inport* port) {
	inport* p = (inport*)*port;
	client* clnt = p->hdr.clnt;

	if(p->on_request_svc)
		ln_service_deinit(&p->on_request_svc);

	if(clnt->sfd != -1) {
		// notify manager
		request_t r;
		int ret = _ln_init_request(
			clnt, &r,
			"method", "r", "unsubscribe",
			"topic_name", "r", p->hdr.topic,
			"rate", "f", p->rate,
			"shm_name", "r", ((struct port_*)p)->shm_name,
			"subscription_id", "d", p->subscription_id,
			NULL);
		if(!ret) {
			ln_mutex_lock(&clnt->communication_mutex);
			_ln_request_and_wait(&r);
			ln_mutex_unlock(&clnt->communication_mutex);
			_ln_destroy_request(&r);
		}
			
	}
	
	_ln_inport_destroy(p);
	*port = NULL;

	return 0;
}

int ln_has_publisher(void* p_) {
	struct port_* p = (struct port_*)p_;
	if(p->type != INPUT_PORT)
		return -LNE_INVALID_PORT_OBJECT;
	inport* op = (inport*)p_;
	return op->has_publisher;
}

/*
int ln_get_message_size(void* p_) {
	port* p = (port*)p_;
	if(p->type != OUTPUT_PORT && p->type != INPUT_PORT)
		return -LNE_INVALID_PORT_OBJECT;
	return _ln_get_message_size(p);
}
*/

int ln_read(ln_inport p_, void* packet, unsigned int packet_size, double* ts, int blocking) {
	inport* p = (inport*)p_;
	
	if(p->hdr.type != INPUT_PORT)
		return -LNE_INVALID_PORT_OBJECT;
	
	_ln_ftrace_print("ln_read %s", p->hdr.topic);
	int ret = _ln_read(p, packet, packet_size, ts, blocking);
	if(ts)
		_ln_ftrace_print("ln_read %s returns %d ts %f", p->hdr.topic, ret, *ts);
	else
		_ln_ftrace_print("ln_read %s returns %d", p->hdr.topic, ret);
	return ret;
}

int ln_read_timeout(ln_inport p_, void* packet, unsigned int packet_size, double* ts, double timeout) {
	inport* p = (inport*)p_;
	
	if(p->hdr.type != INPUT_PORT)
		return -LNE_INVALID_PORT_OBJECT;
	
	_ln_ftrace_print("ln_read_timeout %s to %.3fs", p->hdr.topic, timeout);
	int ret = _ln_read_timeout(p, packet, packet_size, ts, timeout);
	if(ts)
		_ln_ftrace_print("ln_read_timeout %s ret %d ts %f", p->hdr.topic, ret, *ts);
	else
		_ln_ftrace_print("ln_read_timeout %s ret %d", p->hdr.topic, ret);
	return ret;
}

int ln_unblock(ln_inport p_) {
	inport* p = (inport*)p_;
	if(p->hdr.type != INPUT_PORT)
		return -LNE_INVALID_PORT_OBJECT;

	if(!p->have_shm_source) {
		if(p->have_delayed_shm_sem)
			ln_semaphore_post(p->delayed_shm_sem);
		return 0;
	}
	
	return unblock_shm_source(p->source);
}
