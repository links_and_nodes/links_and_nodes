/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#ifndef __WIN32__
#include <unistd.h>
#endif
#include <sys/types.h>
#include <stdio.h>

#include "ln/ln.h"
#include "shm.h"
#include "port.h"
typedef shm_t shm_target;

int create_shm_target(shm_target_t* target_, const char* name, unsigned int element_size, unsigned int n_elements, unsigned int version) {
	return create_shm((shm_t**)target_, name, element_size, n_elements, version);
}

int open_shm_target(shm_target_t* target_, const char* name) {
	return open_shm((shm_t**)target_, name);
}

int write_to_shm_target(shm_target_t target_, double ts, void* data, unsigned int len) {
	shm_target* target = (shm_target*) target_;
	if(len > target->shared_mem.size)
		return -LNE_PACKET_TOO_LARGE;
	// 	memcpy(target->mem, data, len);

	/*
	  mysnc is only needed for real file backends!
	if(msync(target->mem, len, MS_ASYNC | MS_INVALIDATE))
		return -LNE_CHECK_ERRNO;
	*/

	//post_shm(target, data, len);
	/*
	while(ln_semaphore_trywait(target->sem) == 0)
		; // remove current semaphore count!
	ln_semaphore_post(target->sem);
	*/

	// get write lock!
	// pthread_rwlock_wrlock(target->lock);
	// pthread_rwlock_unlock(target->lock);

	// take care of lost wakeups!
	shm_header_t* hdr = target->hdr;
	
#ifdef HAVE_SYS_FUTEX
	if(hdr->version >= 3) {
		
		shm_header_element_t* element = SHM_GET_ELEMENT(target, hdr->next_write);
		uint32_t new_counter = hdr->last_data_counter + 1; // as long as this holds we can do slow-reader-handling! if not we need to change shm_source reading!
		*(volatile uint32_t*)&element->data_counter = new_counter;
		if(data)
			memcpy(SHM_ELEMENT_GET_DATA(element), data, len);
		memcpy(&element->data_timestamp, &ts, sizeof(ts));
		SHM_ELEMENT_GET_DATA_CNT_WRITE_FIN(target, element) = new_counter; // mark element writing as finished!
	
		hdr->next_write = SHM_GET_NEXT_ELEMENT(target, hdr->next_write);
		if(!hdr->is_full && hdr->next_write == 0)
			hdr->is_full = 1;

		// wake waiters!
		// on linux we just wake the futex waiters!
		hdr->last_data_counter = new_counter;
		
		memory_barrier();
		futex_wake_all(&hdr->last_data_counter);
		return 0;
	}
#endif
	
	ln_process_shared_mutex_lock(&hdr->mutex); // no dead-locks: subscribers are waiting in the condition -> so this mutex should always be free!
	shm_header_element_t* element = SHM_GET_ELEMENT(target, hdr->next_write);
	if(data)
		memcpy(SHM_ELEMENT_GET_DATA(element), data, len);
	hdr->last_data_counter ++; // as long as this holds we can do slow-reader-handling! if not we need to change shm_source reading!
	memcpy(&element->data_timestamp, &ts, sizeof(ts));
	*(volatile uint32_t*)&element->data_counter = hdr->last_data_counter;
	// printf("write at target->hdr->next_write: %d counter %d\n", target->hdr->next_write, element->data_counter);
	
	hdr->next_write = SHM_GET_NEXT_ELEMENT(target, hdr->next_write);
	if(!hdr->is_full && hdr->next_write == 0)
		hdr->is_full = 1;
	ln_process_shared_mutex_unlock(&hdr->mutex);
	
	ln_process_shared_cond_broadcast(&hdr->cond);
	return 0;
}

int get_next_buffer_from_shm_target(shm_target_t target_, void** buffer) {
	shm_target* target = (shm_target*) target_;
	// take care of lost wakeups!
	shm_header_t* hdr = target->hdr;
	shm_header_element_t* element = SHM_GET_ELEMENT(target, hdr->next_write);
	*buffer = SHM_ELEMENT_GET_DATA(element);
	return 0;
	
}

int destroy_shm_target(shm_target_t* target_) {
	destroy_shm((shm_t*)*target_);
	*target_ = NULL;
	return 0;
}

int close_shm_target(shm_target_t* target_) {
	close_shm((shm_t*)*target_);
	*target_ = NULL;
	return 0;
}

