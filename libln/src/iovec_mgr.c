/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <stdlib.h>
#include <string.h>

#include "client.h"
#include "iovec_mgr.h"

/*
	// init request msg
	s->msg.msg_iovlen = s->n_fields + 1; // for over-all-size
	s->msg_max_len = s->msg.msg_iovlen;
	s->msg.msg_iov = (struct iovec*)malloc(sizeof(struct iovec) * (s->msg_max_len));

	s->resp_fields = (struct _ln_service_field*)malloc(sizeof(struct _ln_service_field) * s->n_resp_fields);

*/

void _ln_iovec_mgr_init(struct _ln_iovec_mgr* mgr) {
	mgr->msg_max_len = 0;
	mgr->msg.msg_iov = NULL;
	mgr->msg.msg_iovlen = 0;
	mgr->msg.msg_name = NULL; // provider_address;
	mgr->msg.msg_namelen = 0; // sizeof(provider_address);
#ifndef __WIN32__
	mgr->msg.msg_control = NULL;
	mgr->msg.msg_controllen = 0;
#endif
	mgr->msg.msg_flags = 0;
	mgr->swap_aux_mem = NULL;
}
void _ln_iovec_mgr_destroy(struct _ln_iovec_mgr* mgr) {
	free(mgr->msg.msg_iov);
}

void _ln_iovec_mgr_reset(struct _ln_iovec_mgr* mgr, struct _ln_aux_mem_holder* swap_aux_mem) {
	mgr->msg.msg_iovlen = 0;
	mgr->n_bytes = 0;
	mgr->swap_aux_mem = swap_aux_mem;
}

struct iovec* _ln_iovec_mgr_next(struct _ln_iovec_mgr* mgr) {
	if((unsigned)mgr->msg.msg_iovlen == mgr->msg_max_len) {
		if(mgr->msg_max_len == 0)
			mgr->msg_max_len = 8;
		else
			mgr->msg_max_len *= 2;
		mgr->msg.msg_iov = (struct iovec*)realloc(mgr->msg.msg_iov, mgr->msg_max_len * sizeof(struct iovec));
	}
	return &mgr->msg.msg_iov[mgr->msg.msg_iovlen++];
}

void _ln_swap(unsigned int what, void* dst_, void* src_) {
	uint8_t* dst = (uint8_t*)dst_;
	uint8_t* src = (uint8_t*)src_;
	if(what == 1) {
		*dst = *src;
	} else {
		dst += what;
		for(unsigned int c = 0; c < what; c++)
			*(--dst) = *(src++);
	}
}

/*
static void print_hex(void* ptr, unsigned int len) {
	uint8_t* cp = (uint8_t*)ptr;
	for(unsigned int i = 0; i < len; ++i) {
		printf("%02x ", *cp);
		cp++;
	}
	printf("\n");
	return;
}
*/
void _ln_memcpy_swap(void* dst_, void* src_, unsigned int len, unsigned int element_size) {
	if(element_size == 1) {
		memcpy(dst_, src_, len);
		return;
	}
	uint8_t* dst = (uint8_t*)dst_;
	uint8_t* src = (uint8_t*)src_;

	while(len) {
		_ln_swap(element_size, dst, src);

		dst += element_size;
		src += element_size;
		len -= element_size;
	}
}

void _ln_memcpy_swap_hetero(void* dst_, void* src_, unsigned int len, uint8_t* swap_instr) {
	uint8_t* dst = (uint8_t*)dst_;
	uint8_t* src = (uint8_t*)src_;
	uint8_t* swap = swap_instr;

	while(*swap) {
		uint8_t element_size = *(swap++);

		_ln_swap(element_size, dst, src);

		dst += element_size;
		src += element_size;
	}
}

void _ln_iovec_mgr_add(struct _ln_iovec_mgr* mgr, void* base, unsigned int len, unsigned int element_size) {
	struct iovec* iop = _ln_iovec_mgr_next(mgr);
	if(!mgr->swap_aux_mem) {
		// let it point to user data!
		iop->iov_base = (iov_base_t)base;
	} else {
		// create swapped copy into aux_mem and let it point there
		iop->iov_base = (iov_base_t)_ln_aux_mem_get(mgr->swap_aux_mem, len);
		if(element_size == 1)
			memcpy(iop->iov_base, base, len);
		else
			_ln_memcpy_swap(iop->iov_base, base, len, element_size);
	}
	iop->iov_len = len;
	mgr->n_bytes += len;
}

void* _ln_iovec_mgr_get_flat(struct _ln_iovec_mgr* mgr) {
	char* p = (char*)malloc(mgr->n_bytes);
	if(!p)
		return NULL;
	char* base = p;
	for(unsigned int i = 0; i < (unsigned)mgr->msg.msg_iovlen; i++) {
		struct iovec* iop = &mgr->msg.msg_iov[i];
		memcpy(p, iop->iov_base, iop->iov_len);
		p += iop->iov_len;
	}
	return base;
}

struct _ln_iovec_mgr* _ln_iovec_mgr_clone(struct _ln_iovec_mgr* mgr) {
	struct _ln_iovec_mgr* new_mgr = (struct _ln_iovec_mgr*)calloc(1, sizeof(struct _ln_iovec_mgr));
	memcpy(new_mgr, mgr, sizeof(struct _ln_iovec_mgr));
	new_mgr->swap_aux_mem = NULL;
	if(mgr->msg.msg_iov) {
		new_mgr->msg.msg_iov = (struct iovec*)calloc(1, mgr->msg_max_len * sizeof(struct iovec));
		memcpy(new_mgr->msg.msg_iov, mgr->msg.msg_iov, mgr->msg_max_len * sizeof(struct iovec));
	}
	return new_mgr;
}
