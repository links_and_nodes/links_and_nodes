/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <locale.h>

#include "client.h"
#include "request.h"
#include "repr.h"

#define GB_ADD(name, value) growing_buffer_append (&name, &name ## _cp, &name ## _size, value)
#define GB_ADD_ZERO(name)   growing_buffer_appendn(&name, &name ## _cp, &name ## _size, "\0", 1)
void _ln_reconnect(client* clnt) {
#if defined(HAVE_GETEUID)
	uid_t uid = geteuid();
	gid_t gid = getegid();
#elif defined(HAVE_GETUID)
	uid_t uid = getuid();
	gid_t gid = getgid();
#else
	// win32 todo: overkill with OpenProcessToken(), OpenThreadToken() and GetUserSID()
	int uid = 0;
	int gid = 0;
#endif
	char endianess[16];
	{ // check own endianess
		uint16_t test = 0x1234;
		if(((uint8_t*)&test)[0] == 0x34)
			strcpy(endianess, "little");
		else
			strcpy(endianess, "big");
	}

	
	while(true) {
		if(clnt->sfd != -1) {
			_ln_close_socket(clnt->sfd);
			clnt->sfd = -1;
		}
		ln_sleep_seconds(clnt->reconnect_interval);
		if(_ln_try_to_connect(clnt) != 0)
			continue;
		
		//ln_mutex_lock(&clnt->communication_mutex);

		request_t r;
		//int ret;

		// re-register to manager!
		// giving all already used ports

	
		char* port_repr_value = NULL;
		unsigned int port_repr_value_size = 0;

		char* output_ports = NULL;
		unsigned int output_ports_size = 0;
		char* output_ports_cp = output_ports;

		setlocale(LC_ALL, "C"); // HACK!
		if(clnt->outports && clnt->n_outports) {
			GB_ADD(output_ports, "[");
			for(unsigned int i = 0; i < clnt->n_outports; i++) {
				struct outport_* port = clnt->outports[i];

				GB_ADD(output_ports, "(");

				strrepr_(&port_repr_value, &port_repr_value_size, port->hdr.topic);
				GB_ADD(output_ports, port_repr_value);
				GB_ADD(output_ports, ", ");

				strrepr_(&port_repr_value, &port_repr_value_size, port->hdr.message_definition);
				GB_ADD(output_ports, port_repr_value);
				GB_ADD(output_ports, ", ");

				strformat_(&port_repr_value, &port_repr_value_size, "%d", port->hdr.message_size);
				GB_ADD(output_ports, port_repr_value);
				GB_ADD(output_ports, ", ");

				strrepr_(&port_repr_value, &port_repr_value_size, port->hdr.shm_name);
				GB_ADD(output_ports, port_repr_value);

				GB_ADD(output_ports, "),");

			}
			GB_ADD(output_ports, "]");
		} else {
			GB_ADD(output_ports, "[]");
		}
		GB_ADD_ZERO(output_ports);

		char* input_ports = NULL;
		unsigned int input_ports_size = 0;
		char* input_ports_cp = input_ports;

		if(clnt->inports && clnt->n_inports) {
			GB_ADD(input_ports, "[");
			for(unsigned int i = 0; i < clnt->n_inports; i++) {
				struct inport_* port = clnt->inports[i];

				GB_ADD(input_ports, "(");

				strrepr_(&port_repr_value, &port_repr_value_size, port->hdr.topic);
				GB_ADD(input_ports, port_repr_value);
				GB_ADD(input_ports, ", ");

				strrepr_(&port_repr_value, &port_repr_value_size, port->hdr.message_definition);
				GB_ADD(input_ports, port_repr_value);
				GB_ADD(input_ports, ", ");

				strformat_(&port_repr_value, &port_repr_value_size, "%d", port->hdr.message_size);
				GB_ADD(input_ports, port_repr_value);
				GB_ADD(input_ports, ", ");

				strrepr_(&port_repr_value, &port_repr_value_size, port->hdr.shm_name);
				GB_ADD(input_ports, port_repr_value);
				GB_ADD(input_ports, ", ");

				strformat_(&port_repr_value, &port_repr_value_size, "%f", port->rate);
				GB_ADD(input_ports, port_repr_value);

				GB_ADD(input_ports, "),");

			}
			GB_ADD(input_ports, "]");
		} else {
			GB_ADD(input_ports, "[]");
		}
		GB_ADD_ZERO(input_ports);

		char* input_port_infos = NULL;
		unsigned int input_port_infos_size = 0;
		char* input_port_infos_cp = input_port_infos;

		if(clnt->inports && clnt->n_inports) {
			GB_ADD(input_port_infos, "[");
			for(unsigned int i = 0; i < clnt->n_inports; i++) {
				struct inport_* port = clnt->inports[i];

				GB_ADD(input_port_infos, "(");

				strformat_(&port_repr_value, &port_repr_value_size, "%d", port->subscription_id);
				GB_ADD(input_port_infos, port_repr_value);
				GB_ADD(input_port_infos, ", ");

				strformat_(&port_repr_value, &port_repr_value_size, "%d", port->transport);
				GB_ADD(input_port_infos, port_repr_value);
				GB_ADD(input_port_infos, ", ");

				strformat_(&port_repr_value, &port_repr_value_size, "%d", port->buffers);
				GB_ADD(input_port_infos, port_repr_value);
				GB_ADD(input_port_infos, ", ");

				GB_ADD(input_port_infos, "),");

			}
			GB_ADD(input_port_infos, "]");
		} else {
			GB_ADD(input_port_infos, "[]");
		}
		GB_ADD_ZERO(input_port_infos);
		
		char* services = NULL;
		unsigned int services_size = 0;
		char* services_cp = services;

		if(clnt->services && clnt->n_services) {
			GB_ADD(services, "[");
			for(unsigned int i = 0; i < clnt->n_services; i++) {
				struct service_* service = clnt->services[i];

				if(!service->is_listening)
					continue;

				GB_ADD(services, "(");

				strrepr_(&port_repr_value, &port_repr_value_size, service->name);
				GB_ADD(services, port_repr_value);
				GB_ADD(services, ", ");

				strrepr_(&port_repr_value, &port_repr_value_size, service->interface_name);
				GB_ADD(services, port_repr_value);
				GB_ADD(services, ", ");

				strrepr_(&port_repr_value, &port_repr_value_size, service->signature);
				GB_ADD(services, port_repr_value);
				GB_ADD(services, ", ");

				strformat_(&port_repr_value, &port_repr_value_size, "%d", service->service_port);
				GB_ADD(services, port_repr_value);
				GB_ADD(services, ", ");

				if(service->unix_socket_name)
					strrepr_(&port_repr_value, &port_repr_value_size, service->unix_socket_name);
				else
					strrepr_(&port_repr_value, &port_repr_value_size, "");
				GB_ADD(services, port_repr_value);
				GB_ADD(services, ", ");
				
				if(service->service_name_patterns_to_log)
					strrepr_(&port_repr_value, &port_repr_value_size, service->service_name_patterns_to_log);
				else
					strrepr_(&port_repr_value, &port_repr_value_size, "");
				GB_ADD(services, port_repr_value);
				
				GB_ADD(services, "),");

			}
			GB_ADD(services, "]");
		} else {
			GB_ADD(services, "[]");
		}
		GB_ADD_ZERO(services);

		// todo: error handling
		/*ret = */_ln_init_request(
			clnt, &r,
			REGISTER_CLIENT_FIELDS(),
			"output_ports", "O", output_ports,
			"input_ports", "O", input_ports,
			"input_port_infos", "O", input_port_infos,
			"services", "O", services,
			NULL);
		
		free(services);
		free(input_ports);
		free(input_port_infos);
		free(output_ports);
		free(port_repr_value);

		int ret = _ln_request_and_wait(&r);
		_ln_destroy_request(&r);
		if(ret != 0)
			continue;

		break;

	}
	fprintf(stderr, "libln: async_thread: successfully reconnected to manager!\n");
	ln_mutex_unlock(&clnt->communication_mutex);
}

static void cleanup_ln_mutex_unlock(void* mutex) {
	ln_process_shared_mutex_unlock((ln_process_shared_mutex_t*)mutex);
}

#ifdef __WIN32__
thread_return_t WINAPI _ln_async_thread(void* data) {
#else
thread_return_t _ln_async_thread(void* data) {
#endif
	client* clnt = (client*) data;

	ln_thread_setname("ln:async");
	setlocale(LC_ALL, "C"); // HACK!
	ln_debug(clnt, "async thread started at tid %d! (%f)\n", get_thread_id(), 3.1415);

	request_t* r = &clnt->async_request;
	int ret;

	while(1) {
		if(ln_thread_test_cancel(clnt->async_thread))
			break;

		// block on incoming traffic
		fd_set read_fds;
		fd_set write_fds;
		int max_fd = 0;
		FD_ZERO(&read_fds);
		FD_ZERO(&write_fds);
		FD_SET(clnt->sfd, &read_fds);
		max_fd = clnt->sfd;
#ifndef __WIN32__ // on win32 pipe are not allowed within select()
		FD_SET(clnt->async_notification_pipe.input_fd, &read_fds);
		if(clnt->async_notification_pipe.input_fd > max_fd)
			max_fd = clnt->async_notification_pipe.input_fd;
#endif
		// process async services!
		// non-blocking get all services todo!
		uint32_t initial_recv_count;
		while(true) {
			ln_mutex_lock(&clnt->async_service_calls_mutex);
			initial_recv_count = clnt->recv_count;
			if(clnt->n_async_service_calls > clnt->max_services_todo) {
				// realloc needed - release mutex
				clnt->max_services_todo = clnt->n_async_service_calls;
				ln_mutex_unlock(&clnt->async_service_calls_mutex);
				clnt->services_todo = (service**)malloc(sizeof(service*) * clnt->max_services_todo);
				// check again!
				continue;
			}
			break;
		}
		// now get all pointers!
		unsigned int n_services_todo = clnt->n_async_service_calls;
		for(unsigned int i = 0; i < clnt->n_async_service_calls; i++)
			clnt->services_todo[i] = clnt->async_service_calls[i];
		ln_mutex_unlock(&clnt->async_service_calls_mutex);
		// now process all pointers! (no lock needed!)
		for(unsigned int i = 0; i < n_services_todo; i++) {
			service* s = clnt->services_todo[i];
			if(!s->async_did_connect)
				_ln_service_process_async_service_call_before_select(s);
			
			if(s->async_request_wait_for_write) {
				FD_SET(s->fd, &write_fds);
				if(s->fd > max_fd)
					max_fd = s->fd;
			}
			if(s->async_request_wait_for_read) {
				FD_SET(s->fd, &read_fds);
				if(s->fd > max_fd)
					max_fd = s->fd;
			}
		}

		// process async incoming service requests - i.e. do_async_service_handling
		if(clnt->do_async_service_handling) {
			// ln_debug(clnt, "async service handling: there are %d providers in NULL-group!\n", clnt->default_service_group->n_providers);
			max_fd = _wait_for_service_requests_top(clnt->default_service_group, &read_fds, max_fd);
		}

		struct timeval* ts = NULL;
#ifdef __WIN32__
		struct timeval to = {0, 0};
		// win32 needs to send pings!
		if(clnt->sfd != -1) { // only when manager is connected!
			double now = ln_get_monotonic_time();
			double since_last = now - clnt->last_ping_sent;
			double interval = clnt->win32_async_ping_interval;
			ts = &to;
			if(since_last < interval) {
				double seconds_until = interval - since_last;
				if(seconds_until > 0) {
					to.tv_sec = (unsigned int)seconds_until;
					to.tv_usec = (unsigned int)((seconds_until - (unsigned int)seconds_until) * 1e6);
				}
			}
		}
#endif

		int n_lines_left = get_n_finished_lines(r->clnt->line_assembler);
		int n = 0;
		if(n_lines_left == 0 && !_ln_have_manager_input(clnt)) { // no more lines to process, wait for more data
			n = select(max_fd + 1, &read_fds, &write_fds, NULL, ts);
			if(clnt->debug) {
				ln_debug(clnt, "async thread select unblock: n-fd: %d\n", n);
				for(unsigned int ii = 0; ii < max_fd + 1; ii++) {
					if(FD_ISSET(ii, &read_fds))
						ln_debug(clnt, "  fd ready to read: %d\n", ii);
					if(FD_ISSET(ii, &write_fds))
						ln_debug(clnt, "  fd ready to write: %d\n", ii);
				}
			}

			if(n < 0) {
				if(errno == EINTR)
					continue;
				fprintf(stderr, "libln: async_thread: select: %s\n", strerror(errno));
				break;
			}

			if(clnt->do_async_service_handling) {
				if(_wait_for_service_requests_bottom(clnt->default_service_group, &read_fds)) {
					// have pending requests
					ret = _ln_handle_service_group_requests(
						find_service_group(clnt, NULL));
					if(ret < 0) {
						ln_debug(clnt, "libln: async thread have error in async service handling: %d\n", ret);
						if(ret == -LNE_CHECK_ERRNO)
							ln_debug(clnt, "errno: %d - %s", errno, strerror(errno));
						else
							ln_debug(clnt, "msg: %s\n", ln_get_error_message(clnt));
					}
				}
			}
		
			for(unsigned int i = 0; i < n_services_todo; i++) {
				service* s = clnt->services_todo[i];
				if((s->async_request_wait_for_write && FD_ISSET(s->fd, &write_fds))
				   || (s->async_request_wait_for_read && FD_ISSET(s->fd, &read_fds)))
					_ln_service_process_async_service_call_before_select(s);
			}

#ifndef __WIN32__
			if(FD_ISSET(clnt->async_notification_pipe.input_fd, &read_fds)) {
				// async thread notification!
				ln_debug(clnt, "received async notification!\n");
				ln_pipe_read_notification(clnt->async_notification_pipe);
				// check for new async jobs!
				// todo
			}
#endif

			if(n == 0 && clnt->sfd != -1) {
				// timeout -> send ping!
				ln_ping(clnt, clnt->win32_async_ping_timeout, NULL);
			}
		
			if(clnt->sfd != -1 && !FD_ISSET(clnt->sfd, &read_fds))
				continue;
		}
		
		// try to get communication lock!
		if(ln_mutex_trylock(&clnt->communication_mutex)) {
			// could not get lock
			// ln_debug(clnt, "async thread could not get lock!\n");
			ln_sleep_seconds(0.05);
			continue;
		}
		thread_cleanup_push(cleanup_ln_mutex_unlock, &clnt->communication_mutex);

		if(clnt->sfd == -1)
			_ln_reconnect(clnt);
		else if(clnt->recv_count == initial_recv_count) { // no new recv()'s called on lnm-socket since loop start?

			if(n_lines_left == 0 && !_ln_have_manager_input(clnt)) {
				// only do nonblocking wait if there are no unprocessed lines in line-assembler-buffer!
				// after getting the lock: is there still data to read from the manager?
				FD_ZERO(&read_fds);
				FD_SET(clnt->sfd, &read_fds);
				struct timeval to = {0, 0};
				UNTIL_RET_NOT_EINTR(n, select(clnt->sfd + 1, &read_fds, NULL, NULL, &to));
				if(n == 0) {
					ln_debug(clnt, "read condition on manager connection no longer given after getting comm-lock.\n");
				}
			} else
				n = n_lines_left; // have _ln_wait_response() process already received lines

			if(n > 0 || _ln_have_manager_input(clnt)) {
				_ln_reset_request(r);
				r->need_success = 0;
				ret = _ln_wait_response(r, 1);
				if(ret < 0) {
					if(ret == -LNE_LOST_CONNECTION) {
						fprintf(stderr, "libln: async thread lost connection to manager. will try to re-establish connection!!\n");
						_ln_reconnect(clnt);
						continue;
					}
					fprintf(stderr, "libln: async thread had error receiving response: %d!\n", ret);
					if(ret == -LNE_CHECK_ERRNO)
						fprintf(stderr, "errno: %d - %s", errno, strerror(errno));
					else
						fprintf(stderr, "msg: %s\n", ln_get_error_message(clnt));
					break; // todo: continue?
				}
			}
		}
		
		// _ln_process_manager_request(&r);
		
		ln_mutex_unlock(&clnt->communication_mutex);
		thread_cleanup_pop(0);

	}
	ln_debug(clnt, "async thread exiting...\n");
	return THREAD_RETURN_NULL;
}
