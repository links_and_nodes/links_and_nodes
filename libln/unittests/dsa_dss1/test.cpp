/**
   ssh-keygen -N '' -t dsa -m PEM -f dsa_key
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <sstream>
#include <ln/ln.h>

static const std::string private_key = TEST_DIR "/dsa_key";
static const std::string public_key = TEST_DIR "/dsa_key.pub";

std::string read_file(const std::string& fn) {

	FILE* fp = fopen(fn.c_str(), "rb");
	if(!fp)
		throw std::runtime_error((std::string("open ") + fn).c_str());
	struct stat sbuf;
	int ret = fstat(fileno(fp), &sbuf);
	if(ret == -1)
		throw std::runtime_error((std::string("stat fd ") + fn).c_str());
	std::vector<uint8_t> data(sbuf.st_size);
	ret = fread(&data[0], sbuf.st_size, 1, fp);
	if(ret != 1) {
		std::ostringstream ss;
		ss << "read '" << fn << "': " << ret;
		throw std::runtime_error(ss.str().c_str());
	}
	fclose(fp);
	return std::string((const char*)&data[0], data.size());
}

static void print_hex(const char* name, const void* src_, unsigned int len)
{
	const uint8_t* src = (const uint8_t*)src_;
	printf("%s: ", name);
	for(unsigned int i = 0; i < len; i++)
		printf("%02x", src[i]);
	printf("\n");
}


bool test() {
	std::string test_message = "hello world!";

	auto public_key_contents = read_file(public_key);

	for(unsigned int i = 0; i < 3; i++) {
		printf("\nrun %d\n", i);

		std::string signature = ln::dsa_dss1_sign_message(private_key, test_message);
		print_hex("signature", signature.c_str(), signature.size());
		bool valid = ln::dsa_dss1_verify_message_signature(public_key, test_message, signature);
		if(!valid) {
			printf("signature of test message IS NOT valid?!");
			return false;
		}
		printf("signature of test message is valid!\n");

		valid = ln::dsa_dss1_verify_message_signature_memkey(public_key_contents, test_message, signature);
		if(!valid) {
			printf("signature of test message IS NOT valid?! (in-mem pubkey)");
			return false;
		}
		printf("signature of test message is valid! (in-mem pubkey)\n");

	}

	return true;
}

int main(int argc, char* argv[]) {
	bool success = test();
	if(!success)
		return 1;
	return 0;
}
