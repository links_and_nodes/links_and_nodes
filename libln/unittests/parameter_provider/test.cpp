#include <chrono>
#include <thread>
#include <iostream>

#include <ln/ln.h>
#include <ln/parameters.h>


int main(void) {
	if(getenv("LN_MANAGER") == nullptr)
		return 0; // only test compilation & runtime linkage for now. would need lnm otherwise
	
	ln::client clnt("ln_parameters_example");

	// Create parameter block
	ln::parameters::parameter_block params(&clnt, "ln.parameters.example.cpp", false);
	ln::parameters::parameter_block params_only_inputs(&clnt, "ln.parameters.example.cpp_only_inputs", false);

	// Signals
	double sig1 = 0, sig2 = 0, sig3 = 0, sig4 = 0;
	double sig1_out, sig2_out;

	double q_msr[7] {};
	double q_cmd[7] {};
    
	// double J[6][7] {};
    
	params.register_parameters<double, double, double, double, decltype(q_msr)
				   // , decltype(J)
				   >(
					   {
						   "sig1", "sig1 desc", &sig1, &sig1_out,
							   [](double &new_val) {
							   std::cout << "before update sig1: "
								     << new_val << std::endl;
							   return true;
						   },
							   [](double &new_val) {
								   std::cout << "after update sig1: "
									     << new_val << std::endl;
							   }},
					   {"sig2", "sig2 desc", &sig2, &sig2_out,
							   [](double &new_val) {
							   std::cout << "before update sig2: "
								     << new_val << std::endl;
							   return false;
						   }},
					   {"sig3", "sig3 desc", &sig3, &sig3},
					   {"sig4", "sig4 desc", &sig4, nullptr},
					   {"q", "sig5 desc", &q_msr, &q_cmd,
							   [](double (&q)[7]) {
							   printf("q before update: ");
							   for(unsigned int i = 0; i < 7; i++)
								   printf("%.1f ", q[i]);
							   printf("\n");
							   return true;
						   },
							   [](double (&q)[7]) {
								   printf("q after update: ");
								   for(unsigned int i = 0; i < 7; i++)
									   printf("%.1f ", q[i]);
								   printf("\n");
							   }
					   }
					   //, {"J", &J, nullptr }
					   );
	params_only_inputs.register_parameters<double, double, double, double>(
		{"sig1", "sig1 desc", &sig1, nullptr},
		{"sig2", "sig2 desc", &sig2, nullptr},
		{"sig3", "sig3 desc", &sig3, nullptr},
		{"sig4", "sig4 desc", &sig4, nullptr}
		);

	while (1) {
		sig1 += 1;
		sig2 += 10;
		sig3 += 1;
		sig4 += 3;
	
		for(unsigned int i = 0; i < sizeof(q_msr)/sizeof(q_msr[0]); i++)
			q_msr[i] += i;
	
		params.update();

		std::cout << "sig 1 input: " << sig1 << ", sig 2 input: " << sig2 << ", sig 3 input: " << sig3
			  << ", sig4 input: " << sig4 << ", sig 1 output: " << sig1_out << ", sig2 output: " << sig2_out
			  << std::endl;
		
		printf("q_cmd: ");
		for(unsigned int i = 0; i < sizeof(q_msr)/sizeof(q_msr[0]); i++)
			printf("%.1f ", q_cmd[i]);
		printf("\n");

		std::this_thread::sleep_for(std::chrono::seconds(5));	
	}
}
