#include <ln/sharedlnclient.h>
#include "ln_tests.h"

#include <set>
#include <map>
#include <string>
#include <utility>

bool test()
{
	std::string client_name = "shared client";
	std::string ln_manager = "__dummy_test_client";
	const char* this_client_hint = "from unittest";

	void* our_fake_id1 = (void*)1;
	void* our_fake_id2 = (void*)2;
	ln_client shared_clnt1;
	ln_client shared_clnt2;

	printf("\ntesting: 1st sharedlnclient_init\n");
	int ret = sharedlnclient_init(client_name, ln_manager, &shared_clnt1, our_fake_id1, this_client_hint);
	if(ret != 0) {
		printf("ERROR: sharedlnclient_init returned %d!\n", ret);
		return false;
	}
	printf("ok\n");


	printf("\ntesting: 2nd sharedlnclient_init\n");
	ret = sharedlnclient_init(client_name, ln_manager, &shared_clnt2, our_fake_id2, this_client_hint);
	if(ret != 0) {
		printf("ERROR: sharedlnclient_init returned %d!\n", ret);
		return false;
	}
	if(shared_clnt1 != shared_clnt2) {
		printf("ERROR: sharedlnclient_init returned different ln_client %p but we expect %p!\n", shared_clnt2, shared_clnt1);
		return false;
	}
	printf("ok\n");

	printf("\ntesting: 1st sharedlnclient_deinit\n");
	ret = sharedlnclient_deinit(client_name, our_fake_id2, this_client_hint);
	if(ret != 0) {
		printf("ERROR: 1st sharedlnclient_deinit returned %d!\n", ret);
		return false;
	}
	printf("ok\n");

	printf("\ntesting: 2nd sharedlnclient_deinit\n");
	ret = sharedlnclient_deinit(client_name, our_fake_id1, this_client_hint);
	if(ret != 0) {
		printf("ERROR: 2nd sharedlnclient_deinit returned %d!\n", ret);
		return false;
	}
	printf("ok\n");

	return true;
}
int main(int argc, char* argv[])
{
	bool success = test();

	if(!success)
		return 1;
	return 0;
}
