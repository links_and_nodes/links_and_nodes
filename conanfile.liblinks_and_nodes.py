import os
import sys

from conan import ConanFile, conan_version
from conan.tools.files import copy
from conan.tools.scm import Version


class liblinks_and_nodes_conan(ConanFile):
    package_type = "shared-library"
    python_requires = "ln_conan/[^6.0.3]@common/stable"
    python_requires_extend = "ln_conan.Base"

    name = "liblinks_and_nodes"
    homepage = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"
    license = "LGPLv3"
    description = "links_and_nodes is a framework for easy creation and maintanance of distributed computing networks."
    settings = "os", "compiler", "build_type", "arch"
    options = {"without_parameters": [True, False]}
    default_options = {"without_parameters": False}
    exports_sources = [
        "site_scons*",
        "SConstruct",
        "libln*",
        # excludes are checked with fnmatch against results of positive patterns!
        "!libln/.*",
        "!libln/build/*",
        "!libln/tests/*",
        "!libln/examples/*",
        "share*",
        "python*",
        "external*",
        "!python/links_and_nodes/*",
        "!python/links_and_nodes_manager/*",
        "!python/pyparsing/*",
        "!python/pyutils/*.pyc",
    ]

    resdir = "share/libln"

    def package_id(self):
        if Version(conan_version) < "2.0.0" and not self.options.without_parameters:
            self.info.requires["libstring_util"].full_package_mode()

    def requirements(self):
        if not self.options.without_parameters:
            # needed for the parameter server
            self.requires("libstring_util/[^1.2.3]@common/stable", transitive_headers=True, transitive_libs=True)

    def source(self):
        self.write_version_header("LIBLN_VERSION", os.path.join("libln", "include", "ln", "version.h"))
        self.write_version_file(os.path.join("libln", "version"))

    def build(self):
        opts = "--use-private-libtomcrypt"
        if self.options.without_parameters:
            opts += " --without-parameters"
        self.scons_build("libln", opts=opts, verbose=True)

    def package(self):
        copy(self, "version", os.path.join(self.build_folder, "libln"), os.path.join(self.package_folder, self.resdir))
        install = os.path.join("build", "libln", "install", self.prefix[1:])
        copy(self, "*", os.path.join(self.build_folder, install), self.package_folder)

    def package_info(self):
        self.cpp_info.libs = ["ln"]
        if self.settings.get_safe("os") == "Neutrino":
            self.cpp_info.system_libs = ["socket"]
        else:
            self.cpp_info.system_libs = ["rt"]

        self.cpp_info.includedirs = ["include"]
        self.cpp_info.resdirs = [self.resdir]
        self.cpp_info.libdirs = ["lib"]
        if Version(conan_version) < "2.0.0":
            self.env_info.LD_LIBRARY_PATH.append(os.path.join(self.package_folder, "lib"))
        self.runenv_info.append_path("LD_LIBRARY_PATH", os.path.join(self.package_folder, "lib"))
