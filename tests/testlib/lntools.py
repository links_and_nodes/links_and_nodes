from __future__ import print_function
import sys
import time

import links_and_nodes as ln

class lnm_remote_wrapper(ln.lnm_remote):
    def __init__(self, address):
        ln.lnm_remote.__init__(self, address)

        # this needs to have manager python dir in path!
        if ln.ln_manager_dir not in sys.path:
            sys.path.insert(0, ln.ln_manager_dir)
        
        self.default_timeout = 5
        self.outputs = {}

    def _wait_for_stopped(self, p, deadline):
        while p.state != "stopped":
            self.use_mainloop_iterate(deadline=deadline)
    
    def _wait_for_starting(self, p, deadline):
        while p.state != "starting...":
            self.use_mainloop_iterate(deadline=deadline)
    
    def _wait_for_started(self, p, deadline):
        while p.state != "started":
            if "error" in p.state:
                raise Exception("error waiting for process %r start: %s" % (p.name, p.state))
            self.use_mainloop_iterate(deadline=deadline)
    
    def _wait_for_ready(self, p, deadline):
        self._wait_for_started(p, deadline)
        while p.state != "ready":
            self.use_mainloop_iterate(deadline=deadline)
    
    def start_process(self, pname, wait=True, flush_output=False, timeout=None, deadline=None):
        if deadline is None:
            if timeout is None:
                timeout = self.default_timeout
            start = time.time()
            deadline = start + timeout
        
        p = self.sysconf.processes.get(pname)
        if p is None:
            raise Exception("process %r is not avaliable in this manager config:\n%s" % (
                pname, list(self.sysconf.processes.keys())))
        if flush_output and pname in self.outputs:
            del self.outputs[pname]
            
        print("sending start request for %r" % pname)
        self.request("set_process_state_request", ptype="Process", pname=pname, requested_state="start")
        
        self._wait_for_starting(p, deadline)        
        if p.has_ready_state():
            self._wait_for_ready(p, deadline)
        else:
            self._wait_for_started(p, deadline)
        print("%s is started" % pname)
        return True

    def stop_process(self, pname, wait=True, timeout=None, deadline=None):
        if deadline is None:
            if timeout is None:
                timeout = self.default_timeout
            start = time.time()
            deadline = start + timeout
        
        p = self.sysconf.processes.get(pname)
        if p is None:
            raise Exception("process %r is not avaliable in this manager config:\n%s" % (
                pname, list(self.sysconf.processes.keys())))
        
        print("set stop request for %r" % pname)
        self.request("set_process_state_request", ptype="Process", pname=pname, requested_state="stop")
        self._wait_for_stopped(p, deadline)
        print("saw stop for %r" % pname)

    def sleep(self, timeout):
        deadline = time.time() + timeout
        while time.time() < deadline:
            self.use_mainloop_iterate(deadline=deadline, raise_on_timeout=False)

    def loop(self, n=1, timeout=None, deadline=None):
        if deadline is None:
            if timeout is None:
                timeout = self.default_timeout
            start = time.time()
            deadline = start + timeout
        for i in range(n):
            self.use_mainloop_iterate(deadline=deadline, raise_on_timeout=False)

    def get_output(self, name):
        return "".join(self.outputs.get(name, []))
        
    def on_output(self, name, output):
        # disable printing of process output
        print("process output of %r: %r" % (name, output))
        outputs = self.outputs.get(name)
        if outputs is None:
            self.outputs[name] = outputs = []
        outputs.append(output)
        return False

    def on_log_messages(self, msgs):
        # disable printing of log messages
        return False

    def on_obj_state(self, obj_type, name, state):
        print("new state %r for object %s: %r" % (state, obj_type, name))
        p = self.sysconf.processes.get(name)
        p.state = state

    def stop_all(self):
        self.sysconf = self.request("get_system_configuration")
        for pname, p in self.sysconf.processes.items():
            if "stop" not in p.state and "error" not in p.state:
                self.request("set_process_state_request", ptype="Process", pname=pname, requested_state="stop")
