import re
import sys
import time

def without_gui(test, *args):
    if test.get_parameter("ln_manager_without_gui", True):
        args = list(args)
        args.insert(0, "--without_gui")
    return args

def check_lnm_output(test, lnm, removes=[]):
    lnm_out = lnm.get_output()
    for rem in removes:
        lnm_out = re.sub(rem, "", lnm_out)
    lnm_out_lower = lnm_out.lower()
    for what, method in [("error", test.fail), ("warning", test.warning)]:
        for sp in " :":
            this = what + sp
            if this in lnm_out_lower:
                sys.stdout.write("lines with %r:\n%s\n" % (this, "\n".join(filter(lambda s: this in s, lnm_out_lower.split("\n")))))
                method("there are %s messages in lnm output!" % what)

class console_lnm(object):
    def __init__(self, lnm):
        self.lnm = lnm
        self.test = lnm.test
        self.prompt = "^## ready: .* ##$"
        self.wait_prompt()
        self.last_notifications = []
        self.last_nonlog_notifications = []

    def _process_output(self, deadline, hint):
        try:
            line = self.lnm.read_line(deadline=deadline)
        except Exception:
            raise Exception("timeout waiting for %s!" % hint)
        #self.test.debug("process line for %s: %r" % (hint, line))
        
        if re.match(self.prompt, line):
            return ("prompt", )
        
        if line.startswith("### "):
            # read multiline
            first_line = line[4:]
            lines = [  ]
            while True:
                try:
                    line = self.lnm.read_line(deadline=deadline)
                except Exception:
                    raise Exception("timeout (%ss) waiting for %s!" % (deadline, hint))
                if line == "###":
                    break
                lines.append(line)
            return self._process_ev("%s %s" % (first_line, "\n".join(lines)))
            
        if line.startswith("## "):
            return self._process_ev(line[3:])

        return ("unknown", line)

    def _process_ev(self, data):
        #self.test.debug("process: %r\n" % data)
        rtype, body = data.split(" # ", 1)
        if rtype in ("response", "error"):
            return rtype, body
        elif rtype == "notify":
            ntype, nbody = body.split(" # ", 1)
            #self.test.debug("notification %r body %r" % (ntype, nbody))
            this = (ntype, nbody)
            self.last_notifications.append(this)
            if ntype != "log":
                self.last_nonlog_notifications.append(this)
            return rtype, ntype, body
        else:
            self.test.debug("process: type %r body %r\n" % (rtype, body))
            

    def _wait_for_response(self, deadline, or_prompt=True):
        while True:
            ev = self._process_output(deadline, "wait for response")
            if ev is None:
                continue
            if ev[0] == "error":
                return ev
            if ev[0] == "response":
                return ev
            if or_prompt and ev[0] == "prompt":
                return ev

    def wait_prompt(self, timeout=None):
        if timeout is None:
            timeout = self.test.get_parameter("wait_for_output_timeout", 30)
        start = time.time()
        deadline = start + timeout
        while True:
            ev = self._process_output(deadline, "mi-prompt")
            if ev[0] == "prompt":
                return
            
    def exec(self, cmd, timeout=None, expect_response=False, might_produce_response=False):
        cmd = cmd.strip()
        if timeout is None:
            timeout = self.test.get_parameter("wait_for_output_timeout", 30)
        start = time.time()
        deadline = start + timeout + 2
        self.lnm.write("%s\n" % cmd)
        ret = self._wait_for_response(deadline)
        if ret[0] == "error":
            raise Exception("error: %r" % (ret[1:]))
        if might_produce_response and ret[0] == "response":
            self.wait_prompt()
            return ret[1]
        if expect_response:
            if ret[0] != "response":
                raise Exception("unexpected event after %r: %r" % (cmd, ret))
            self.wait_prompt()
            return ret[1]
        if ret[0] != "prompt":
            self.wait_prompt()
        return ret

    def start_wait_ready(self, which, has_ready_state=True, extra_args=None, timeout=None):
        if timeout is None:
            timeout = self.test.get_parameter("wait_for_output_timeout", 30)
        start = time.time()
        deadline = start + timeout + 2
        if extra_args is None:
            extra_args = ""
        self.lnm.write("start %s -r -t %s '%s'\n" % (extra_args, timeout, which))
        resp = self._wait_for_response(deadline)
        if resp[0] == "error":
            self.wait_prompt()
            raise Exception("error starting %r: %s" % (which, resp[1]))
        if resp[0] == "prompt":
            return
        return resp[1]

    def start_wait_done(self, which, extra_args=None, timeout=None):
        self.start(which, timeout=timeout)
        self.wait_until_stopped(which)
        return self.exec("tail %r" % which, expect_response=True)

    def start(self, which, extra_args=None, timeout=None):
        if timeout is None:
            timeout = self.test.get_parameter("wait_for_output_timeout", 30)
        start = time.time()
        deadline = start + timeout + 2
        if extra_args is None:
            extra_args = ""
        self.lnm.write("start %s -t %s '%s'\n" % (extra_args, timeout, which))
        resp = self._wait_for_response(deadline)
        if resp[0] == "error":
            self.wait_prompt()
            raise Exception("error starting %r: %s" % (which, resp[1]))
        if resp[0] == "prompt":
            return
        return resp[1]

    def stop_wait(self, which, timeout=None):
        if timeout is None:
            timeout = self.test.get_parameter("wait_for_output_timeout", 30)
        self.lnm.write("stop -t %s -w '%s'\n" % (timeout, which))
        start = time.time()
        deadline = start + timeout + 2
        responses = []
        while True:
            resp = self._wait_for_response(deadline)
            if resp[0] == "error":
                raise Exception("error stopping %r: %s" % (which, resp[1]))
            if resp[0] == "prompt":
                return responses

    def wait_until_stopped(self, which, timeout=None):
        return self.exec("wait '%s'" % which, might_produce_response=True)

    def state(self, command, state_name):
        if command == "CHECK":
            command = "-c"
        elif command == "UP":
            command = "-u"
        elif command == "DOWN":
            command = "-d"
        else:
            raise Exception("invalid state command %r!" % command)
        return self.exec('state %s "%s"' % (command, state_name), expect_response=True)


def get_lnm(test, extra_args=None, name="lnm", config_file=None):
    if config_file is None:
        config_file = "test.lnc"
    args = without_gui(test, "-c", test.relative_to_test(config_file))
    if extra_args:
        args.extend(extra_args)
    args.append("--isolated-test")
    ln_manager_pyscript = test.get_parameter("ln_manager_pyscript", "ln_manager.py")
    with open(ln_manager_pyscript, "r") as fp:
        shebang = fp.readline().strip()[2:]
    interpreter = shebang.rsplit(" ", 1)[1]
    test.set_parameter("global.python_binary", interpreter)
    lnm = test.new_python_process(
        name,
        pyscript=ln_manager_pyscript,
        arguments=args
    )
    match = lnm.wait_for_line_with_regex("listening on (.*)' for ln-clients!", timeout=60) # with some default timeout
    test.lnm_port = lnm.port = int(match.group(1).split(":", 1)[1])
    test.debug("found lnm port %r" % lnm.port)
    if extra_args and "--mi-console" in extra_args:
        lnm.wait_for_line_with_regex("ln_manager console.", timeout=30) # with some default timeout
        lnm.console = console_lnm(lnm)
        
    return lnm
