#!/usr/bin/python

from __future__ import print_function

import pprint

import links_and_nodes as ln

import dict_example

class DictClient:
    def __init__(self):
        self.clnt = ln.client("dict_client")
        self.direct_get_dict_svc = self.clnt.get_service("direct_get_dict", "test/get_dict_str_str")

    def direct_get_dict(self):
        self.direct_get_dict_svc.call()
        resp = dict_example.decode_str_str_dict(self.direct_get_dict_svc.resp.data_dict)
        print("received dict response:\n%s" % pprint.pformat(resp))
        
if __name__ == "__main__":
    c = DictClient()
    c.direct_get_dict()
