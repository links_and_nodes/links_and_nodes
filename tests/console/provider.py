#!/usr/bin/python

import sys
import time
import pprint
import links_and_nodes as ln

import dict_example

class test_provider(ln.service_provider):
    def __init__(self):
        self.clnt = ln.client("test_provider", sys.argv)
        ln.service_provider.__init__(self, self.clnt, "test")

        self.wrap_service_provider("add", "test/add")
        self.wrap_service_provider("strange", "test/strange")
        self.wrap_service_provider("simple", "test/simple")
        self.wrap_service_provider("set_frame", "test/ln_bridge_websocket_set_frame")
        self.wrap_service_provider("error", "test/error_data")

        self.direct_set_frame = self.clnt.get_service_provider("direct_set_frame", "test/ln_bridge_websocket_set_frame")
        #self.direct_set_frame = self.clnt.get_service_provider("direct_set_frame", "test/ln_bridge_websocket_set_frame_real")
        #self.direct_set_frame = self.clnt.get_service_provider("direct_set_frame", "ln_bridge_websocket_tests/set_frame")
        self.direct_set_frame.set_handler(self.on_direct_set_frame)
        self.direct_set_frame.do_register()

        self.direct_get_dict = self.clnt.get_service_provider("direct_get_dict", "test/get_dict_str_str")
        self.direct_get_dict.set_handler(self.on_direct_get_dict)
        self.direct_get_dict.do_register()

        self.nested = self.clnt.get_service_provider("nested", "test/nested")
        self.nested.set_handler(self.on_nested)
        self.nested.do_register()

    def on_nested(self, conn, req, resp):
        res = pprint.pformat(req)
        print("req:\n%s" % res)
        resp.result = res
        conn.respond()
        return 0

    def on_direct_get_dict(self, request, req, resp):
       response_dict = dict(
           some="some value",
           another="value",
       )
       dict_example.encode_str_str_dict(resp.data_dict, response_dict)
       request.respond()

    def error(self, request_error_message, request_error_code):
        ret = dict(
            error_message=request_error_message,
            error_info_code=request_error_code,
            value=42
        )
        print("error return:\n%s" % pprint.pformat(ret))
        return ret
        
    def add(self, a, b, name):
        res = a + b
        sys.stdout.write("a %s + b %s is %s for %r\n" % (a, b, res, name))
        if a == 43 and b == 4445:
            time.sleep(5)
        elif a == 42 and b == 43:
            raise Exception("client requested provider to throw!")
        else:
            time.sleep(0.05)
        sys.stdout.write("return\n")
        return res

    def strange(self, **kwargs):
        print("strange called with:\n%s" % pprint.pformat(kwargs))
        return dict(
            py_value=dict(a=15),
            two_py_values=[42, None],
            n_py_values=[42, None, "flo", dict(d="f")],
            str_value=repr(kwargs),
            two_str_values=["Hello", "World"],
            str_values=["hello", "flo"]
        )

    def simple(self, a):
        b = a + 1
        print("simple called with a=%r, returning b=%r" % (a, b))
        return b
    
    def set_frame(self, frame, fixed_char_array, dyn_char_array, dyn_float_array, some_pyobject):
        print("this is set_frame called!")
        frames = []
        for i in range(5):
            frames.append(dict(pose=[i+1] * 16))
        string = "hallo welt!"
        message = "all is fine!"
        char1 = "ok"
        char2 = "okay"
        frameA = dict(pose=[44] * 16)
        frameA["pose"][-1] = 42
        frameB = dict(pose=[45] * 16)
        frameB["pose"][-1] = 42
        floats1 = [1.1,2.2,3,100]
        return dict(
            error_message=message,
            frames=frames,
            two_frames=[frameA, frameB],
            frame=dict(pose=frame["pose"]),
            fixed_char_array=char1,
            dyn_char_array=char2,
            dyn_float_array=floats1,
            string_object=string,
            #some_pyobject_back=dict(u=42.3) # this doesnt work yet
        )
        #return dict(
        #    error_message="",
        #    frame=frame,
        #    frames=[frame],
        #    two_frames = [frame, frame],
        #    fixed_char_array="some",
        #    dyn_char_array="more",
        #    dyn_float_array=[1, 2, 3, 4],
        #    string_object="string magic"
        #)

    def on_direct_set_frame(self, request, req, resp):
        print("got some pyobject: %r" % req.some_pyobject)
        if req.some_pyobject == "throw":
            raise Exception("client requested provider to throw!")
        print("got direct set_frame request from %s" % sys.executable)
        print("fixed_char_array: %r" % req.fixed_char_array)
        frames = []
        for i in range(5):
            f = resp.new_frame_t_packet()
            f.pose = [i+1] * 16
            frames.append(f)
            
        string = "hallo welt!"
        message = "all is fine!"
        char1 = "ok"
        char2 = "okay"
        frameA = dict(pose=[44] * 16)
        frameA["pose"][-1] = 42
        frameB = dict(pose=[45] * 16)
        frameB["pose"][-1] = 42
        floats1 = [1.1,2.2,3,100]
        
        resp.error_message_len = len(message)
        resp.error_message = message
        resp.frames_len = len(frames)
        resp.frames = frames
        resp.two_frames[0].pose = frameA["pose"]
        resp.two_frames[1].pose = frameB["pose"]
        char1_enc = char1.encode("utf-8")
        resp.fixed_char_array = char1 + (len(resp.fixed_char_array) - len(char1_enc)) * "\x00"
        resp.dyn_char_array_len = len(char2)
        resp.dyn_char_array = char2
        resp.dyn_float_array_len = len(floats1)
        import numpy as np
        resp.dyn_float_array = np.array(floats1, dtype=np.float32)
        resp.string_object.string_len = len(string)
        resp.string_object.string = string
        
        import pprint
        print("sending response:\n%s" % pprint.pformat(resp))
        request.respond()
    
    def run(self):
        self.handle_service_group_requests()
        
if __name__ == "__main__":
    p = test_provider()
    p.run()
