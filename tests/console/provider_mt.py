#!/usr/bin/python

import sys
import time
import threading
import links_and_nodes as ln

class MTProvider(object):
    def __init__(self):
        self.clnt = ln.client("provider_mt", sys.argv)

        self.cnt = 0
        self.strange_svc = self.clnt.get_service_provider("test_mt.strange", "test/strange")
        self.strange_svc.set_handler(self.strange_handler)
        self.strange_svc.do_register("strange_group_name")

    def strange_handler(self, request, req, resp):
        this = "%#x" % id(threading.current_thread())
        msg = "%s handler %d key-values: %s" % (
            this,
            req.keys_len,
            ", ".join([ "%d:%r" % (req.keys[i], req.values[i].string) for i in range(req.keys_len) ])
        )
        #print(msg)
        time.sleep(0.0001) # shortly release GIL
        resp.py_value = (this, req.keys[-1])
        request.respond()
        return 0
    
    def run(self):
        self.clnt.set_max_threads("large_pool", 8+1)
        self.clnt.handle_service_group_in_thread_pool("strange_group_name", "large_pool")
        print("ready")
        while True:
            time.sleep(10) # idle
        
if __name__ == "__main__":
    p = MTProvider()
    p.run()
