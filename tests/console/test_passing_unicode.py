#!/usr/bin/env python2

import sys
import links_and_nodes as ln

clnt = ln.client(u"test")

for name in ( "ln/frame34", u"ln/frame34", b"ln/frame34", 42):
    print("requesting %r" % name)

    if name == 42:
        try:
            ret = clnt.get_message_definition(name)
        except Exception:
            print("got expected exception: %s --> all fine!" % sys.exc_info()[1])
            continue
        raise Exception("error: expected exception for name %r!" % name)
    
    ret = clnt.get_message_definition(name)

print("done\n\n")
