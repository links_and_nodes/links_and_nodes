#!/usr/bin/env python
# -*- encoding: utf-8 -*-


import sys

if len(sys.argv) > 1:
    transcode_to = sys.argv[1]
    transcoded = u"hellö wörld".encode(transcode_to)
    if hasattr(sys.stdout, "buffer"):
        sys.stdout.buffer.write(transcoded)
    else:
        sys.stdout.write(transcoded)
else:
    utf8_data = b'message: stdbuf: failed to run command \xe2\x80\x98ptpd2\xe2\x80\x99: No such file or directory\n'
    sys.stdout.write(utf8_data)
