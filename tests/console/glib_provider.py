#!/usr/bin/python

import sys
import time
import pprint
import links_and_nodes as ln

class glib_provider(ln.service_provider):
    def __init__(self, mainloop):
        self.clnt = ln.client("test_provider", sys.argv)
        self.mainloop = mainloop
        
        ln.service_provider.__init__(self, self.clnt, "test", mainloop=self.mainloop)
        self.wrap_service_provider("glib_add", "test/add")

        self.direct_set_frame = self.clnt.get_service_provider("glib_direct_set_frame", "test/ln_bridge_websocket_set_frame")
        self.direct_set_frame.set_handler(self.on_direct_set_frame)
        self.direct_set_frame.do_register_with_mainloop(self.mainloop)
        
        
    def glib_add(self, a, b, name):
        res = a + b
        sys.stdout.write("a %s + b %s is %s for %r\n" % (a, b, res, name))
        if a == 43 and b == 4445:
            self.mainloop.sleep(5)
        else:
            self.mainloop.sleep(0.05)
        sys.stdout.write("return\n")
        return res

    def on_direct_set_frame(self, request, req, resp):
        print("got direct set_frame request from %s" % sys.executable)
        print("fixed_char_array: %r" % req.fixed_char_array)
        frames = []
        for i in range(5):
            f = resp.new_frame_t_packet()
            f.pose = [i+1] * 16
            frames.append(f)
            
        string = "hallo welt!"
        message = "all is fine!"
        char1 = "ok"
        char2 = "okay"
        frameA = dict(pose=[44] * 16)
        frameA["pose"][-1] = 42
        frameB = dict(pose=[45] * 16)
        frameB["pose"][-1] = 42
        floats1 = [1.1,2.2,3,100]
        
        resp.error_message_len = len(message)
        resp.error_message = message
        resp.frames_len = len(frames)
        resp.frames = frames
        resp.two_frames[0].pose = frameA["pose"]
        resp.two_frames[1].pose = frameB["pose"]
        char1 = char1.encode("utf-8")
        resp.fixed_char_array = char1 + (len(resp.fixed_char_array) - len(char1)) * b"\x00"
        resp.dyn_char_array_len = len(char2)
        resp.dyn_char_array = char2
        resp.dyn_float_array_len = len(floats1)
        import numpy as np
        resp.dyn_float_array = np.array(floats1, dtype=np.float32)
        resp.string_object.string_len = len(string)
        resp.string_object.string = string

        if req.some_pyobject == "throw":
            print("throwing!")
            raise Exception("stop requested!")
        
        if req.some_pyobject == "sleep5":
            print("sleeping 5...")
            self.mainloop.sleep(5)
        
        print("sending response:\n%s" % pprint.pformat(resp))
        request.respond()
    
class check_timer(object):
    def __init__(self, loop):
        self.period = 1. / 2
        main.timeout_add(self.period, self.on_timer)
        self.last = time.time()
        
    def on_timer(self):
        now = time.time()
        since_last = now - self.last
        if since_last > 2 * self.period:
            print("ERROR: measured timer period of %.1fs -- should be %.1fs!" % (since_last, self.period))
        self.last = now
        return True
    
if __name__ == "__main__":
    main = ln.GLibMainloop() # provides ln.MainloopInterface for GLib    
    #main = ln.SelectMainloop() # provides ln.MainloopInterface for GLib    
    p = glib_provider(main)
    ct = check_timer(main)
    print("ready")
    main.run()
