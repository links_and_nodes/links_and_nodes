#!/usr/bin/python

from __future__ import print_function

import sys
import pprint

import links_and_nodes as ln

class ErrorClient(ln.services_wrapper):
    def __init__(self):
        self.clnt = ln.client("ErrorClient", sys.argv)
        ln.services_wrapper.__init__(self, self.clnt, "test")

        self.wrap_service( # default, old, backward compatbile behaviour: throw exception if error_message != ""
            "error", "test/error_data",
            method_name="call_default"
        ) # will now throw ln.ServiceErrorResponse which has .response attribute with all response-fields as dict
        
        self.wrap_service(
            "error", "test/error_data",
            method_name="call_msg_code",
            throw_on_these_error_indicators=["error_message", "error_info_code"] # throw exception if bool(error_message) or bool(error_info_code)
        ) # will throw ln.ServiceErrorResponse which has .response attribute with all response-fields as dict
        
        self.wrap_service(
            "error", "test/error_data",
            method_name="call_msg_code_cb",
            throw_on_these_error_indicators=None, # wrapper does never throw
            postprocessor=self.check_for_error_response # user-defined check for error-response can be done in post-processor
        ) # will throw user-defined exception from self.check_for_error_response

    def check_for_error_response(self, response):
        if response["error_info_code"] != 0:
            raise Exception("error info code is %r!" % response["error_info_code"]) # throw any user-defined exception
        if response["error_message"]:
            raise Exception("error message is %r!" % response["error_message"]) # throw any user-defined exception
        
        # no error, only return direct "postprocessed" value to caller
        return response["value"]

def print_error_response(e):
    print("got expected %s. text:\n--\n%s\n--" % (type(e).__name__, str(e)))
    print("all response fields:")
    for field in sorted(e.response.keys()):
        print("  %s: %s" % (field, pprint.pformat(e.response[field])))
    assert e.response["value"] == "42"
    print()
    
if __name__ == "__main__":
    ec = ErrorClient()

    response = ec.call_default("", 0) # no error
    # value should be dict of non-error-message response fields
    if len(response) != 2 or "error_info_code" not in response or response.get("value") != "42":
        raise Exception("invalid default response:\n%s" % pprint.pformat(response))    
    
    ec.call_default("", 15) # no error, as default does not care about error_info_code
    try:
        ec.call_default("error0", 0) # error, as default error_message != ""
        raise Exception("should throw!!")
    except ln.ServiceErrorResponse as e:
        print_error_response(e)
        
    value = ec.call_msg_code("", 0) # no error
    if value != "42":
        raise Exception("invalid no-error-msg-code return: %r" % (value, ))
    
    try:
        ec.call_msg_code("", 15) # error, as code is != 0
        raise Exception("should throw!!")
    except ln.ServiceErrorResponse as e:
        print_error_response(e)

    try:
        ec.call_msg_code("error1", 0) # error, as msg is != ""
        raise Exception("should throw!!")
    except ln.ServiceErrorResponse as e:
        print_error_response(e)

    try:
        ec.call_msg_code("error2", 2) # error, as msg is != "" and code != 0
        raise Exception("should throw!!")
    except ln.ServiceErrorResponse as e:
        print_error_response(e)

    value = ec.call_msg_code_cb("", 0) # no error
    # value should be only one scalar -- 3rd response field
    if value != "42":
        raise Exception("invalid no-error-msg-code return: %r" % (value, ))

    print("test passed")
    sys.exit(0)
