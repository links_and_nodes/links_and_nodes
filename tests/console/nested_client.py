#!/usr/bin/python

from __future__ import print_function
import sys
import threading
import random
import links_and_nodes as ln

if __name__ == "__main__":
    clnt = ln.client("test_client", sys.argv)

    md = "test/nested"
    print(clnt.describe_message_definition(md))

    svc = clnt.get_service("nested", md)
    i1 = svc.req.new_inner1_t_packet()
    svc.req.inner1.append(i1)
    i1.value1 = 32
    i2 = i1.new_inner2_t_packet()
    i1.inner2.append(i2)
    i2.value2 = 16

    svc.call()
    print("\nreturned result:")
    print(svc.resp.result)
