#!/usr/bin/python

from __future__ import print_function
import sys
import threading
import random
import links_and_nodes as ln

class test_client(ln.services_wrapper):
    def __init__(self):
        self.clnt = ln.client("test_client", sys.argv)
        args = self.clnt.get_remaining_args()
        self.prefix = ""
        if len(args) > 1:
            if args[1] == "call_glib_mainloop":
                call_method = ln.GLibMainloop()
                default_name = "john mainloop"
                self.prefix = "glib_"

                # test specifying only service name an mainloop
                svc = self.clnt.get_service("test.glib_add", mainloop=call_method)
                print("got glib using svc client")
                self.clnt.release_service(svc)

            else:
                call_method = args[1]
                default_name = "john " + call_method
        else:
            call_method = "call"
            default_name = "john doe"
        ln.services_wrapper.__init__(self, self.clnt, "test")

        self.wrap_service(
            self.prefix + "add", "test/add",
            method_name="add",
            call_method=call_method,
            default_arguments=dict(name=default_name)
        )

    def test(self, a, b, timeout=0):
        expected_ret = a + b
        ret = self.add(a, b, _timeout=timeout)
        if ret != expected_ret:
            raise Exception("error: a %d + b %d expected %d got %d!" % (a, b, expected_ret, ret))

    def test_simple(self):
        if self.prefix:
            return
        
        simple_svc = c.clnt.get_service("test.simple", "test/simple")
        simple_svc.req.a = 42
        simple_svc.call()
        #print("simple answer: %r" % simple_svc.resp.b)
        assert simple_svc.resp.b == 43

        # test explicit call via mainloop
        simple_svc.req.a = 43
        simple_svc.call_via_mainloop(_mainloop=ln.GLibMainloop())
        assert simple_svc.resp.b == 44

        # test timeouts
        add_svc = c.clnt.get_service("test.add", "test/add")
        add_svc.req.name.string = "test_to"
        add_svc.call_with_timeout(a=42, b=1, timeout=1)
        assert add_svc.resp.c == 43

        add_svc.call_with_timeout(a=42, b=2, timeout=0)
        assert add_svc.resp.c == 44

        add_svc.call_with_timeout(a=42, b=2)
        assert add_svc.resp.c == 44

        try:
            add_svc.call_with_timeout(a=43, b=4445, timeout=1)
            raise Exception("error: expected timeout exception!")
        except:
            if "timed out" in str(sys.exc_info()[1]):
                print("got expected timeout exception.")
            else:
                raise

    def test_ws(self):
        if self.prefix:
            return
        
        # test service from ln_bridge_websocket test
        import numpy as np
        expected = [42.] * 16
        expected[-1] = 43.
        set_frame_svc = c.clnt.get_service("test.set_frame", "test/ln_bridge_websocket_set_frame")
        if False:
            print("early exit")
            return
        set_frame_svc.req.frame.pose = expected
        set_frame_svc.req.fixed_char_array = "fixed char"
        set_frame_svc.req.dyn_char_array  = "dyn22"
        set_frame_svc.req.dyn_float_array = np.array([2.2,42.3,67], dtype=np.float32)
        set_frame_svc.req.some_pyobject = dict(list=[dict(a=1), dict(b=2), None], number=42)
        set_frame_svc.call()
        assert (set_frame_svc.resp.frame.pose == expected).all()
    
class test_thread(threading.Thread):
    def __init__(self, clnt):
        threading.Thread.__init__(self)
        self.clnt = clnt
        self.success = False

    def run(self):
        N = 25
        rm = 10000
        for i in range(N):
            self.clnt.test(
                random.randint(0, rm),
                random.randint(0, rm))
        sys.stdout.write("%s tests done\n" % N)
        self.success = True
        
if __name__ == "__main__":
    c = test_client()

    c.test_simple()
    c.test_ws()
    
    c.test(5, 10)
    c.test(5, 10, timeout=2)
    try:
        c.test(43, 4445, timeout=1) # should throw
    except:
        if "timed out" not in str(sys.exc_info()[1]):
            raise

    N = 2
    threads  = [test_thread(c) for i in range(N)]
    [thread.start() for thread in threads]
    print("%s threads running" % N)
    
    [thread.join() for thread in threads]
    
    success = False not in [thread.success for thread in threads]
    sys.exit(0 if success else 1)
