#!/usr/bin/env python

"""
this test is supposed to test service transport of ln/string & ln2/pyobject
via single values, fixed-length-lists and variable-length-lists.

it also exercises ln.services_wrapper and ln.service_provider's translation between
ln_packet_part <-> python-data-types.
"""
import sys
import pprint
import links_and_nodes as ln
from test_c_api import test_c_api

class strange_test_client(ln.services_wrapper):
    def __init__(self):
        self.clnt = ln.client("test_client", sys.argv)
        args = self.clnt.get_remaining_args()
        if len(args) > 1:
            call_method = args[1]
        else:
            call_method = "call"
        ln.services_wrapper.__init__(self, self.clnt, "test")

        wrapper = self.wrap_service("strange", "test/strange", call_method=call_method)

        # test field constructor for "strange" typedef-names
        p = wrapper.svc.req.new_vector_string_packet()
        p.string = "some string"
        p.string_len = len(p.string)
        v = p.dict()
        print("packet-ctor-test: %r" % v)
        if v != {'string_len': 11, 'string': 'some string'}:
            raise Exception("'strange' packet ctor does not work as expected!")

    def test_empty(self):
        return self.strange(
            [], [],
            None, [None, None],
            []
        )

    def test_values(self):
        return self.strange(
            [0,1,2,3,4], ["zero", "one", "two"],
            dict(a=1, b=2), ["str", (1,2)],
            [1, "one", (2, "minus", 1)]
        )

    def test(self, *tests):
        res = {}
        for i, test in enumerate(tests):
            ret = getattr(self, "test_%s" % test)()
            if i + 1 != len(tests):
                comma = ","
            else:
                comma = ""
            print("%s:\n%s\n" % (test, pprint.pformat(ret)))
            res[test] = ret
        return res

if __name__ == "__main__":
    test_c_api()

    c = strange_test_client()
    res = c.test("empty", "values")

    expected = {
'empty': {
 'n_py_values': [42, None, 'flo', {'d': 'f'}],
 'py_value': {'a': 15},
 'str_value': u"{'keys': array([], dtype=uint32), 'values': [], 'py_value': "
              "None, 'two_py_values': [None, None], 'n_py_values': []}",
 'str_values': [u'hello', u'flo'],
 'two_py_values': [42, None],
 'two_str_values': [u'Hello', u'World']},

'values': {
 'n_py_values': [42, None, 'flo', {'d': 'f'}],
 'py_value': {'a': 15},
 'str_value': u"{'keys': array([0, 1, 2, 3, 4], dtype=uint32), 'values': [], "
              "'py_value': {'a': 1, 'b': 2}, 'two_py_values': ['str', (1, 2)], "
              "'n_py_values': []}",
 'str_values': [u'hello', u'flo'],
 'two_py_values': [42, None],
 'two_str_values': [u'Hello', u'World']}

}
    # all this just to compare equal in py2
    def normdict(d):
        ret = []
        if isinstance(d, list):
            for v in d:
                ret.append(normdict(v))
            return ret
        if isinstance(d, dict):
            for k, v in d.items():
                if k == "str_value":
                    v = eval(v)
                ret.append((k, normdict(v)))
            return sorted(ret)
        return d
    res, expected = map(normdict, [res, expected])
    if str(res) != str(expected):
        print("ERROR does not match expected result:\n%s\nvs:\n%s" % (pprint.pformat(res), pprint.pformat(expected)))
        sys.exit(1)
    else:
        print("ok")
        sys.exit(0)
