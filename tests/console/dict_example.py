from __future__ import print_function

def decode_str_str_dict(dict_object):
    """
    returns dict
    """
    ret = dict()
    for kv_pair in dict_object.key_values:
        ret[kv_pair.key.string] = kv_pair.value.string
    return ret

def encode_str_str_dict(dict_str_str, pydict):
    """
    fills test/dict_str_str-ln_packet
    """
    for key, value in pydict.items():
        kv_pair = dict_str_str.new_str_key_str_value_t_packet()
        kv_pair.key.string = str(key)
        kv_pair.value.string = str(value)        
        dict_str_str.key_values.append(kv_pair)
