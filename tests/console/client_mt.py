#!/usr/bin/python

import sys

import numpy as np

import links_and_nodes as ln

clnt = ln.client("client_mt", sys.argv)

svc = clnt.get_service("test_mt.strange", "test/strange")
svc.req.py_value = None # pyobject
svc.req.two_py_values = [None, None] # 2 pyobject's
svc.req.n_py_values = [] # list of pyobject's

cnt = 0
n_errors = 0
threads_seen = set()
while True:
    svc.req.keys = np.array([cnt, cnt + 1], dtype=np.uint32)

    svc.req.values = [] # list of vector/string
    values_item = svc.req.new_vector_string_packet()
    values_item.string = str(cnt)
    svc.req.values.append(values_item)

    values_item = svc.req.new_vector_string_packet()
    values_item.string = str(cnt + 1)
    svc.req.values.append(values_item)

    svc.call() # blocking call

    thread = svc.resp.py_value[0]
    threads_seen.add(thread)
    received = svc.resp.py_value[1] # should be cnt + 1
    print("thread %s, received %d, n_threads_seen: %d, n_errors %d" % (
        thread,
        received,
        len(threads_seen),
        n_errors))
    if received != cnt + 1:
        n_errors += 1
        print("  error:  expected %d" % (cnt + 1))
        
    
    cnt += 1

