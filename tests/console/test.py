#!/usr/bin/env python3

import os
import sys
import re

tests_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), ".."))
sys.path.insert(0, tests_dir)
import lntest # noqa: E402
import testlib # noqa: E402

test = lntest.new_test()
lnm = testlib.get_lnm(test, extra_args=["--mi-console"])

lnm.console.exec("log enable")

ls = lnm.console.exec("ls", expect_response=True)
cd = lnm.console.exec("cd /")
ls_all = lnm.console.exec("ls -R", expect_response=True)
cd = lnm.console.exec("cd all")

# connect daemon
lnm.console.stop_wait("all")

lnm.console.last_nonlog_notifications = []
try:
    lnm.console.start_wait_ready("dummy_client_fail")
    raise Exception("startup of 'dummy_client_fail' did not result in error!")
except Exception:
    if "failed to start" not in str(sys.exc_info()[1]):
        raise
provider = "all/dummy_provider_fail"
starting_count = 0
for ntype, body in lnm.console.last_nonlog_notifications:
    if ntype == "state change" and body.startswith(provider + ": starting..."):
        starting_count += 1
assert starting_count == 1 # check that provider was only started once!

def state(command):
    state = lnm.console.state(command, "ui programs/test_state")
    test.debug("test-state: %r" % state)
    if command == "CHECK":
        return state
    if state != command:
        raise Exception("commanded state %r but got %r!" % (command, state))
if state != "DOWN":
    state("DOWN")
state("UP")
state("DOWN")

# test warning-regex and error on unexpected exit
lnm.console.start_wait_ready("show errors") # expect exception because of error
# todo: check for "process warning" notification!

lnm.console.start_wait_ready("date", has_ready_state=False)
date = lnm.console.exec("tail date", expect_response=True)
test.debug("date output: %r" % date)

proc = "test_passing_unicode"
lnm.console.start_wait_ready(proc, has_ready_state=False)
lnm.console.wait_until_stopped(proc)
out = lnm.console.exec("tail %s" % proc, expect_response=True)
test.debug("%s output: %r" % (proc, out))

lnm.console.stop_wait("all")

lnm.console.start_wait_ready("after date")
lnm.console.wait_until_stopped("after date")
out = lnm.console.exec("tail 'date ign error'", expect_response=True)
if "retval: 42" not in out:
    raise Exception("'date ign error' process output not as expected: %r" % out)
out = lnm.console.exec("tail 'after date'", expect_response=True)
test.debug("after date output: %r" % out)
if "we were started" not in out:
    raise Exception("'after date' did not output expected output!")


name = "strange client"
lnm.console.start(name)
lnm.console.wait_until_stopped(name)
out = lnm.console.exec("tail -n 1000 %r" % name, expect_response=True)
test.debug("%s output:\n%s\n----\n" % (name, out))
if "ERROR" in out:
    provider_name = "python provider single threaded"
    provider_out = lnm.console.exec("tail -n 1000 %r" % provider_name, expect_response=True)
    raise Exception("%r did output error message:\n%s\n----\nprovider output:\n%s\n----" % (name, out, provider_out))

name = "error client"
lnm.console.start(name)
lnm.console.wait_until_stopped(name)
out = lnm.console.exec("tail -n 1000 %r" % name, expect_response=True)
if "terminated with retval: 0" not in out:
    raise Exception("%r did output error message:\n%s\n" % (name, out))

lnm.console.start("client using call_glib_mainloop")
try:
    lnm.console.start_wait_ready("client using call")
except Exception:
    lnm.console.exec("tail 'python provider single threaded'")
    lnm.console.exec("tail 'client using call'")
    raise Exception("could not start client!")
test.debug("client started...")

procs = [ "client using call", "client using call_glib_mainloop"]
for proc in procs:
    lnm.console.wait_until_stopped(proc)
    output = lnm.console.exec("tail '%s'" % proc, expect_response=True)

    regexes = [
        "2 threads running",
        "25 tests done",
        "25 tests done",
    ]
    for line in output.split("\n"):
        if re.search(regexes[0], line):
            del regexes[0]
            if not regexes:
                break
    if regexes:
        raise Exception("%s: regexes not found: %r" % (proc, regexes))

lnm.console.stop_wait("all")

# test blocking on ready regex in source scripts
# todo: source cmd in mi interface?
lnm.write("source '%s'\n" % os.path.join(test.src_dir, "test_source.script")) # this is spilling exec-cmd notifications
# todo: wait for exec-cmd notify with "hello world"
lnm.wait_for_line_with_regex([
    "all/show errors: ready", # first we want to see ready,
    "^hello world", # then we want to see our message
    "all/show errors: stopped" # and then the script should stop the process
]) 

lnm.write("quit\n")
lnm.wait()

test.ok()
