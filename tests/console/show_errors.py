#!/usr/bin/python

from __future__ import print_function
import time

time.sleep(0.75)
print("tester ready!")

for i in range(2):
    for i in range(2):
        time.sleep(0.25)
        print("time: %s" % time.time())
    print("warning: some warning!")
    for i in range(2):
        time.sleep(0.25)
        print("time: %s" % time.time())
    print("error: time did timeout!")
print("ohoh: done!")

print("end.")
