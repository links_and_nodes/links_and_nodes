from links_and_nodes_base.c_api import free, create_string_buffer, c_void_p, c_char_p, ln_get_service_signature_for_service, ln_service_init, ln_service_call, ln_service_deinit, ln_get_error_message, libln, ln_deinit, byref

def test_c_api():
    clnt = c_void_p(None)
    ret = libln.ln_init(byref(clnt), b"c-api-client", 0, None)
    assert ret == 0

    service_interface = c_char_p()
    service_message_definition = c_char_p()
    service_signature = c_char_p()
    ret = ln_get_service_signature_for_service(
        clnt,
        b"test.strange", # service name
        # output arguments: (should be free'd by us!)
        byref(service_interface),
        byref(service_message_definition),
        byref(service_signature)
    )
    assert ret == 0
    print("correct service interface: %r" % service_interface.value.decode("utf-8")) # this is the correct message_definition_name for this service
    #print("message_definition: %r\nsignature: %r" % (
    #    service_message_definition.value, # this is a python-repr of the md-contents!
    #    service_signature.value))
    
    svc = c_void_p(None)
    ret = ln_service_init(
        clnt, byref(svc),
        b"test.strange", # service name
        b"test.strange", # INVALID service message definition! (it should be "test/strange")
        service_signature.value)
    assert ret == 0

    # free unneeded strings
    free(service_interface)
    free(service_message_definition)
    free(service_signature)

    # now try to call this service!
    ptr_size = 8
    keys_size = 4 + ptr_size
    values_size = 4 + ptr_size
    py_object_size = 4 + ptr_size + 4
    py_value_size = py_object_size
    n_py_values_size = 4 + ptr_size
    request_size = keys_size + values_size + py_value_size + 2 * py_object_size + n_py_values_size
    error_message_size = 4 + ptr_size
    str_value_size = 4 + ptr_size
    str_values_size = 4 + ptr_size
    response_size = error_message_size + py_value_size + 2 * py_object_size + n_py_values_size + str_value_size + 2 * str_value_size + str_values_size
    
    data = create_string_buffer(request_size + response_size)
    ret = ln_service_call(svc, data)
    if ret == 0:
        raise Exception("error! call did get thru! but lnm should have complained about wrong/invalid md-name!")

    msg = ln_get_error_message(clnt)
    msg = msg.decode("utf-8").split("\n", 1)[0]
    print("return value: %r, expected, reported error message: %s" % (ret, msg))

    # cleanup
    ret = ln_service_deinit(byref(svc))
    assert ret == 0
    
    ln_deinit(byref(clnt))
