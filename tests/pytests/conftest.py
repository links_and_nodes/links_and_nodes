import pytest

import links_and_nodes as ln

@pytest.fixture(scope="module")
def ln_client():
    client = ln.client("ln_client")
    yield client
