import links_and_nodes as ln


class DummyClass(object):
    @ln.util.args_to_self
    def __init__(self, a, b, c, d=None, e=None):
        super(DummyClass, self).__init__()


def test_util():
    dummy_obj = DummyClass("a", 1, "c", e={'test': 'ok'})
    assert dummy_obj.a == "a"
    assert dummy_obj.b == 1
    assert dummy_obj.c == "c"
    assert dummy_obj.d is None
    assert dummy_obj.e == {'test': 'ok'}
