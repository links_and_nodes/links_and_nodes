from __future__ import division
from __future__ import print_function

import time
from copy import deepcopy
from threading import Thread


import links_and_nodes as ln

SERVICE_NAME = 'multiply_service'
SERVICE_INTERFACE = 'services/multiply_service'
TOPIC_NAME = 'test_topic'
MESSAGE_TYPE = 'uint32_t'


class MyServiceProvider(ln.service_provider):
    def __init__(self, ln_client, service_prefix="",
                 default_register_method="do_register"):
        super(MyServiceProvider, self).__init__(ln_client, service_prefix, default_register_method)
        # Register callback method
        self.wrap_service_provider(SERVICE_NAME, SERVICE_INTERFACE, method_name="handler")

    def run(self):
        self.handle_service_group_requests()

    def handler(self, a, b):
        return a * b

class MyService(ln.services_wrapper):
    def __init__(self, ln_client, service_prefix=""):
        super(MyService, self).__init__(ln_client, service_prefix)
        # Register callback method
        self.wrap_service(SERVICE_NAME, SERVICE_INTERFACE)

# def call(self, a, b):
#     return a, b


def service_provider_thread():
    ln_client = ln.client("service_provider")
    service_provider = MyServiceProvider(ln_client)
    service_provider.run()


def test_service_provider(ln_client):
    thread = Thread(target=service_provider_thread)
    thread.daemon = True
    thread.start()

    # Wait for the service provider thread to start
    time.sleep(0.1)

    # Retrieve service
    # here the service consists in multiplying two ints
    service_client = ln_client.get_service(SERVICE_NAME)

    service_client.req.a = 23
    service_client.req.b = 2
    service_client()
    result = service_client.resp.result

    assert result == 2 * 23

    # Using the wrapper
    wrapped_service = MyService(ln_client)

    assert result == wrapped_service.multiply_service(23, 2)
    assert result == wrapped_service.multiply_service(a=23, b=2)

    # release the service
    ln_client.release_service(service_client)


def test_packet_part(ln_client):
    """
    Additional tests for the packet part wrapper.
    """
    # TODO: proper unit testing
    port = ln_client.publish(TOPIC_NAME, MESSAGE_TYPE, buffers=10)
    packet_part = port.packet
    _str_rep = str(packet_part)
    packet_part.get_field_ctors()
    packet_part.get_field_types()
    packet_part.keys()
    iter(packet_part)
    packet_part.dict()
    packet_part.items()
    _copy = deepcopy(packet_part)
    packet_part.get_dict()
