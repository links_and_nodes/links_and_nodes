from __future__ import print_function
from __future__ import division
from __future__ import absolute_import

import os
import time
from multiprocessing import Process

import test_client

# Change topic name to avoid error during tests
test_client.TOPIC_NAME = "test_topic_2"

from test_client import publisher, TOPIC_NAME # noqa: E402


def test_logger(ln_client):
    """
    Start a publisher log it
    """
    process = Process(target=publisher)
    process.start()

    # Wait for the publisher process to start
    time.sleep(0.1)

    logger = ln_client.get_logger("test_logger")
    logger.set_only_ts(False)
    logger.add_topic(TOPIC_NAME, log_size=10, divisor=1)
    logger.enable()

    time.sleep(1)

    logger.disable()
    # test different format
    log_dir = "tests/logs"
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)
    manager_log_filename_1 = os.path.join(log_dir, "test1.log")
    manager_log_filename_2 = os.path.join(log_dir, "test2.log")
    log_filename = os.path.join(log_dir, "test3.log")
    # Save using the manager
    logger.manager_save(os.path.abspath(manager_log_filename_1), format='pickle')
    logger.manager_save(os.path.abspath(manager_log_filename_2), format='matlab')
    logger.manager_save(os.path.abspath(manager_log_filename_2), format='auto')
    # Save and log in a file
    logger_data = logger.download()
    logger_data.save(log_filename)
    # IMPLEMENTED in custom version only
    # logger_data = logger_data.load(log_filename)
    _log_dict = logger_data.unpack_into({})
    process.join()
