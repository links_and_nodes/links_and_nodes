from __future__ import print_function
from __future__ import division

import time

import pytest # noqa: F401

import links_and_nodes as ln

SERVICE_NAME = 'my_param_server'
PARAMETERS_INTERFACE = 'ln/parameters/query_dict'
REQUEST_TOPIC_INTERFACE = 'ln/parameters/request_parameter_topic'
QUERY_DICT_INTERFACE = 'ln/parameters/query_dict'
PARAM_TOPIC_TYPE = 'uint32_t'
PARAM_TOPIC_NAME = 'test_param_topic'

# 'apply': {'full_name': 'left_arm.apply',
#  'input': 0,
#  'output': 0,
#  'override_enabled': False,
#  'service': 'ln.parameters.query_dict.left_arm_model 1'},

# TODO: One service for query_dict/request_topic/query/request_parameter_topic

def param_publisher():
    """
    Publish N_PUBLISHED data on TOPIC_NAME
    """
    ln_client = ln.client("publisher")
    rate = 100  # 100 Hz

    port = ln_client.publish(PARAM_TOPIC_NAME, PARAM_TOPIC_TYPE, buffers=10)
    port.packet.data = 0
    while True:
        port.packet.data = 34
        port.write()
        time.sleep(1 / rate)


def service_provider_process():
    ln_client = ln.client("service_provider")

    def query_dict_handler(service_client, request, response):
        """
        The callback that handles the request.

        :param service_client: (ln.service_wrapper object)
        :param request: (ln_wrappers.ln_packet_part object)
        :param response: (ln_wrappers.ln_packet_part object)
        """
        data_dict = {
            'test_param': {
                'full_name': 'test_param.test',
                'input': 0,
                'output': 0,
                'override_enabled': False,
                # 'service': SERVICE_NAME
            }
        }
        response.data = str(data_dict)
        service_client.respond()

    def request_topic_handler(service_client, request, response):
        """
        The callback that handles the request.

        :param service_client: (ln.service_wrapper object)
        :param request: (ln_wrappers.ln_packet_part object)
        :param response: (ln_wrappers.ln_packet_part object)
        """
        print("Requested parameters: {}".format(request.parameters))
        data_dict = {
            'test_param': ('param_topic', 'u_int32t')
        }
        response.parameter_topics = str(data_dict)
        service_client.respond()

    # Register service that follows the given interface
    query_dict_service = ln_client.get_service_provider(SERVICE_NAME, QUERY_DICT_INTERFACE)
    query_dict_service.set_handler(query_dict_handler)
    query_dict_service.do_register()

    request_topic_service = ln_client.get_service_provider(SERVICE_NAME + "_request", REQUEST_TOPIC_INTERFACE)
    request_topic_service.set_handler(request_topic_handler)
    request_topic_service.do_register()

    # Wait for the service client
    time.sleep(0.2)
    ln_client.wait_and_handle_service_group_requests(group_name=None, timeout=2)

    # unregister service provider
    ln_client.release_service(query_dict_service)


# def test_parameters(ln_client):
#
#     # publisher_process = Process(target=param_publisher)
#     # publisher_process.deamon = True
#     # publisher_process.start()
#     process = Process(target=service_provider_process)
#     process.start()
#
#     # Wait for the service provider process to start
#     time.sleep(10)
#
#     try:
#         params = ln_client.get_parameters("")
#         print(params._query_services)
#         print(params._parameters)
#     except Exception as e:
#         print(e)
#     # print(params)
#     # import ipdb; ipdb.set_trace()
#     # params.refresh()
#     # params.subscribe()
#     # params.set_override("is_logging", 1, wait_step=True)
#     # print(params.get_dict())
#     # print(ln_client.get_parameters("test_param"))
#
#     process.join()
#     # publisher_process.terminate()


# def test_parameters(ln_manager):
#     """
#     Set and retrieve parameters
#     """
#     return
#     ln_client = ln.client("param_client", LN_MANAGER)
#     ln_client.set_float_parameter("float_param", 1.3)
#     param = ln_client.get_parameters("float_param")
#     print(param)
