import pytest
import os
import shutil
try:
    from cStringIO import StringIO
except ImportError:
    from io import StringIO


import links_and_nodes as ln

# Expand default message def dirs
os.environ['LN_MESSAGE_DEFINITION_DIRS'] = 'tests/msg_defs/'

MESSAGE_DEF_NAME = 'custom_message_def'
MESSAGE_DEF_PATH = 'tests/msg_defs/custom_message_def'
SERVICE_INTERFACE_PATH = 'tests/msg_defs/services/multiply_service'
SERVICE_INTERFACE_NAME = 'services/multiply_service'
LOG_FOLDER = '/tmp/tests_ln/'


def test_search_message_def():
    # Without rescanning, finding custom message def fails
    with pytest.raises(Exception):
        ln.search_message_definition(MESSAGE_DEF_NAME, allow_rescan=False, with_abs_name=False)

    ln.search_message_definition(MESSAGE_DEF_NAME, allow_rescan=True, with_abs_name=False)
    paths = ln.search_message_definition(MESSAGE_DEF_NAME, with_abs_name=True)

    assert isinstance(paths, tuple)

    ln.search_message_definition(MESSAGE_DEF_PATH, with_abs_name=False)
    ln.search_message_definition(MESSAGE_DEF_PATH, with_abs_name=True)

    with pytest.raises(Exception):
        ln.search_message_definition("not_a_message_def", with_abs_name=True)


def test_get_message_def():
    # Update the known messages definitions
    ln.rescan_message_definition_dirs()
    ln.get_message_definition_from_filename(os.path.abspath(MESSAGE_DEF_PATH))

    with pytest.raises(Exception):
        ln.get_message_definition_from_filename("not_a_message_def")


def test_message_def_class():
    # Load from file
    msg_def_1 = ln.message_definition(MESSAGE_DEF_NAME, fn=MESSAGE_DEF_PATH)
    # Load from dict
    from_dict = 'from_dict:{}'.format(msg_def_1.dict())
    msg_def_2 = ln.message_definition(MESSAGE_DEF_NAME, fn=from_dict)
    assert msg_def_1.dict() == msg_def_2.dict()
    # Load from raw string
    raw_string = """define my_int as "uint32_t"
    my_int int_array[2]
    """
    msg_def_3 = ln.message_definition("test_from_text", text=raw_string)
    # 2 int of 4 bytes -> 8 bytes
    assert msg_def_3.calculate_size() == 8
    assert ln.compute_message_definition_size(MESSAGE_DEF_NAME) == msg_def_1.calculate_size()

    ln.compute_message_definition_string(MESSAGE_DEF_NAME)

    # Load service message def
    service_msg = ln.message_definition(MESSAGE_DEF_NAME, fn=SERVICE_INTERFACE_PATH)

    offset, item_size = msg_def_1.locate_member('c')

    # Compare message def
    msg_def_4 = ln.message_definition(MESSAGE_DEF_NAME, fn=MESSAGE_DEF_PATH)
    assert msg_def_1 == msg_def_4
    assert msg_def_1 != msg_def_3
    _str_rep = str(msg_def_1)


def test_message_gen():
    """
    Test message generation using the message_definition_header.
    """
    # TODO: proper unit testing
    ln.rescan_message_definition_dirs()
    msg_header = ln.message_definition_header()
    msg_header.add_message_definition(MESSAGE_DEF_NAME)
    msg_header.add_message_definition(SERVICE_INTERFACE_NAME)

    assert msg_header.find_md_for_filename("test") is None

    msg_header.generate_sfunction_parameter()
    msg_header.generate_sfunction_lnrk_parameter()
    msg_header.generate_idf()

    file_like_object = StringIO()
    msg_header.generate(file_like_object)
    assert len(file_like_object.getvalue()) > 0
    msg_def = ln.message_definition(MESSAGE_DEF_NAME, fn=MESSAGE_DEF_PATH)
    msg_header.generate_flat_md(msg_def)
    msg_header.generate_flat_lnrk_md(msg_def)

    if os.path.isdir(LOG_FOLDER):
        shutil.rmtree(LOG_FOLDER)
    os.makedirs(LOG_FOLDER)

    msg_header.generate_jna(LOG_FOLDER)
    msg_header.generate_java(LOG_FOLDER, False)
