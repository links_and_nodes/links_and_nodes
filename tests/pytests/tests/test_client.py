from __future__ import print_function
from __future__ import division

import time
from multiprocessing import Process, Event

import numpy as np
import pytest # noqa: F401

import links_and_nodes as ln


def _assert_eq(left, right):
    assert left == right, '{} != {}'.format(left, right)


TOPIC_NAME = 'test_topic'
SERVICE_NAME = 'services.multiply_service'
SERVICE_INTERFACE = 'services/multiply_service'
N_PUBLISHED = 10
MESSAGE_TYPE = 'uint32_t'
FAKE_TIMESTAMP = 1548078970


def publisher(start_publishing):
    """
    Publish N_PUBLISHED data on TOPIC_NAME
    """
    ln_client = ln.client("publisher")
    rate = 10.0  # 10 Hz. 100 Hz context switching is a bad race condition for a CI/CD server! that test is not robust enough!

    port = ln_client.publish(TOPIC_NAME, MESSAGE_TYPE, buffers=10)
    start_publishing.wait(timeout=10)
    for i in range(N_PUBLISHED):
        port.packet.data = i
        if i % 2 == 0:
            port.write(timestamp=FAKE_TIMESTAMP)
        else:
            port.write()
        time.sleep(1 / rate)


def test_subscribe(ln_client):
    """
    Start a publisher and subscribe to it
    """

    start_publishing = Event()

    process = Process(target=publisher, args=(start_publishing, ))
    process.start()

    port = ln_client.subscribe(topic_name=TOPIC_NAME, message_definition_name=MESSAGE_TYPE, buffers=10)

    for i in range(100):
        time.sleep(0.1)
        if port.has_publisher():
            break
    else:
        raise Exception("timeout waiting for publisher!")

    # LN ports are now connected
    start_publishing.set()

    timeout = 10.0 # seconds. O.1s is a bad race condition for a CI/CD server! that test is not robust enough!
    messages = []
    for i in range(N_PUBLISHED):
        have_packet = port.read(timeout) is not None
        if not have_packet:
            raise Exception("timeout waiting for packet %d" % i)
        messages.append(port.packet.data)
        if port.packet.data % 2 == 0:
            assert port.timestamp == FAKE_TIMESTAMP

    ln_client.unsubscribe(port)

    # Check that no packet where lost
    assert len(messages) == N_PUBLISHED
    assert np.allclose(messages, np.arange(0, N_PUBLISHED, dtype=np.uint32))
    process.join()


def test_ping(ln_client):
    ping_result = ln_client.ping(timeout=0.1)
    assert ping_result > 0


def service_provider_process():
    ln_client = ln.client("service_provider")

    def handler(service_client, request, response):
        """
        The callback that handles the request.

        :param service_client: (ln.service_wrapper object)
        :param request: (ln_wrappers.ln_packet_part object)
        :param response: (ln_wrappers.ln_packet_part object)
        """
        response.result = request.a * request.b
        service_client.respond()

    # Register service that follows the given interface
    service_provider = ln_client.get_service_provider(SERVICE_NAME, SERVICE_INTERFACE)
    service_provider.set_handler(handler)
    service_provider.do_register()

    # Wait for the service client
    time.sleep(0.2)
    ln_client.wait_and_handle_service_group_requests(group_name=None, timeout=2)

    # unregister service provider
    ln_client.release_service(service_provider)


def test_service_provider(ln_client):
    process = Process(target=service_provider_process)
    process.start()

    # Wait for the service provider process to start
    time.sleep(0.1)

    services = ln_client.find_services_with_interface(SERVICE_INTERFACE)

    assert SERVICE_NAME in services

    # Check service interface
    message_def, message_size, message_hash, flat_md = ln_client.get_message_definition(SERVICE_INTERFACE)
    service_def, service_signature = ln_client.get_service_signature(SERVICE_INTERFACE)
    service_interface, msg_def, msg_signature = ln_client.get_service_signature_for_service(SERVICE_NAME)
    # Convert strings to python dict
    message_def = eval(message_def)
    service_def = eval(service_def)
    msg_def = eval(msg_def)
    assert message_def == service_def
    assert message_def == msg_def
    assert service_interface == SERVICE_INTERFACE
    assert service_signature == msg_signature
    assert len(message_def['resp_fields']) == 1
    assert len(message_def['fields']) == 2
    assert 'a' in message_def['fields'][0]
    assert 'result' in message_def['resp_fields'][0]
    # 2 fields of 4 Bytes
    assert message_size == 8

    # Retrieve service
    # here the service consists in multiplying two ints
    service_client = ln_client.get_service(SERVICE_NAME, SERVICE_INTERFACE)

    service_client.req.a = 10
    service_client.req.b = 2
    service_client()
    result = service_client.resp.result

    assert result == 2 * 10

    # release the service
    ln_client.release_service(service_client)

    process.join()


def test_message_definition(ln_client):
    # uint32_t message, using 4 * 8 Bytes = 32 bits
    message_def, message_size, message_hash, flat_md = ln_client.get_message_definition(MESSAGE_TYPE)
    # Convert string to python dict
    message_def = eval(message_def)
    assert len(message_def) == 3
    assert len(message_def['resp_fields']) == 0
    assert len(message_def['defines']) == 0
    assert len(message_def['fields']) == 1
    assert 'data' in message_def['fields'][0]
    assert message_size == 4

    # test adding a message def
    test_name = "test_array"
    test_def = """define my_int as "uint32_t"
    my_int int_array[2]
    """
    message_definition_name = ln_client.put_message_definition(test_name, test_def)
    assert message_definition_name.startswith('gen/')
    message_def, message_size, message_hash, flat_md = ln_client.get_message_definition(message_definition_name)
    message_def = eval(message_def)
    assert len(message_def['fields']) == 1
    _, param_name, n_params = message_def['fields'][0]
    assert param_name == 'int_array' and n_params == 2
    defines_key = list(message_def['defines'].keys())[0]
    assert defines_key == 'my_int'
    assert 'data' in message_def['defines'][defines_key][0] and 'uint32_t' in message_def['defines'][defines_key][0]
    assert message_size == 8

# def test_parameters(ln_manager):
#     """
#     Set and retrieve parameters
#     """
#     return
#     ln_client = ln.client("param_client", LN_MANAGER)
#     ln_client.set_float_parameter("float_param", 1.3)
#     param = ln_client.get_parameters("float_param")
#     print(param)
