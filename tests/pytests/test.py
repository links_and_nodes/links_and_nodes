#!/usr/bin/env python3

import os
import sys
import re

tests_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), ".."))
sys.path.insert(0, tests_dir)
import lntest # noqa: E402
import testlib # noqa: E402

test = lntest.new_test()

lnm = testlib.get_lnm(test, extra_args=["--mi-console"], config_file="tests/test_conf.lnc")
lnm.console.exec("log enable")

#pub = test.new_python_process("publisher", "publisher.py")

pytest = test.new_python_process(
    "pytest", library_module="pytest",
    arguments=["-v", test.relative_to_test("./tests/")],
    cwd=test.src_dir
)
pytest.wait()
# we got returncode == 0! (no error)
out = pytest.get_output()

match = re.search("===+ ([0-9]+) failed.*?===", out)
if match:
    n_failed = int(match.group(1))
else:
    n_failed = 0

if test.mainloop.runtime.log_level == "debug": # -v
    # in successfull case, also print pytest output:
    print(out)

lnm.write("quit\n")
lnm.wait(timeout=180)

if n_failed != 0:
    test.fail("pytest had %d failures!" % n_failed)
else:
    test.ok()
