
from __future__ import print_function

import re
import sys
import time

import links_and_nodes as ln

class test_lnm_remote(ln.lnm_remote):
    def __init__(self, address="localhost:54123"):
        ln.lnm_remote.__init__(self, address)

        # this needs to have manager python dir in path!
        if ln.ln_manager_dir not in sys.path:
            sys.path.insert(0, ln.ln_manager_dir)

    def iterate(self):
        try:
            self.use_mainloop_iterate(timeout=0.25)
        except Exception:
            if "timeout" not in str(sys.exc_info()[1]):
                raise

    def run(self):
        time.sleep(0.1)
        self.sysconf = self.request("get_system_configuration")
        print("have %d processes defined in this config" % len(self.sysconf.processes))
        
        self.pname = pname = "all/test process"
        
        print("start %r" % pname)
        self.output = []
        self.had_started = False
        self.request("set_process_state_request", ptype="Process", pname=pname, requested_state="start")

        self.had_stop_or_error = False
        while not self.had_stop_or_error:
            self.iterate()

        # todo: need something to enforce getting all outstanding outputs from process!
        self.request("push_output")
        self.iterate()
        
        # and then remove those:
        #print("stop %r" % pname)
        #self.request("set_process_state_request", ptype="Process", pname=pname, requested_state="stop")
        #

        print("now assemble output!")
        output = b"".join(self.output)
        output = output.decode("utf-8")
        regex = "\r\nprocess %s started with pid .*?\r\n\r\ntest process started!\r\ntest process stopped!\r\n\r\nprocess with pid .*? terminated with retval: 0\r\n" % (
            self.pname)
        m = re.match(regex, output)

        if m is None:
            info = "%s\nprocess output was:\n%s\n%s\n%r" % (
                "-" * 25,
                output,
                "-" * 25,
                output)
            raise Exception("process output does not match expected regex:\n%s\n%s" % (regex, info))

        print("sending 'exit'-request to lnm...")
        self.request("exit")
        print("exiting...")

    def on_output(self, name, output):
        # disable printing of process output
        print("process output of %r: %r" % (name, output))
        if name == self.pname:
            self.output.append(output)
        return False
    def on_log_messages(self, msgs):
        # disable printing of log messages
        return False

    def on_obj_state(self, obj_type, name, state):
        print("new state %r for object %s: %r" % (state, obj_type, name))
        if name == self.pname:
            if not self.had_started and (state =="started" or "error" in state):
                self.had_started = True
            if self.had_started and not self.had_stop_or_error and ("error" in state or "stopped" in state):
                self.had_stop_or_error = True


if __name__ == "__main__":
    test = test_lnm_remote(sys.argv[1])
    #test = test_lnm_remote()
    test.run()
