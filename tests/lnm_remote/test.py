#!/usr/bin/env python3

import os
import sys
import re

tests_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), ".."))
sys.path.insert(0, tests_dir)
import lntest # noqa: E402
import testlib # noqa: E402

test = lntest.new_test()

test.short_description = "test ln_manager gui connection"
test.description = """
start lnm with/without gui with simple lnc file
start test-lnm-gui-client which tests connection and protocol
wait until test-client finishes
"""

"""
implicitely:

stop all processes at end of test.
collect all generated reports
classify reports into OK, WARNING, ERROR
"""

def without_gui(*args):
    if test.get_parameter("ln_manager_without_gui", True):
        args = list(args)
        args.insert(0, "--without_gui")
    return args

lnm = testlib.get_lnm(test, config_file=os.path.join(test.src_dir, "test.lnc"))

test_client = test.new_python_process(
    "test_client", 
    pyscript="test_client.py",
    arguments=["localhost:%d" % lnm.port],
    
    # todo: env
)

test_client.wait() # expect to terminate with status 0!
lnm.wait()

# when we get here we assume the test went fine up to now. 
# now inspect generated reports to decide the result of this test

lnm_out = lnm.get_output()
lnm_out_lower = lnm_out.lower()
lnm_out_lower = re.sub(r"\[warning]: installing ", "cissy-installing ", lnm_out_lower)
if "error" in lnm_out_lower:
    test.fail("there are error messages in lnm output!")
if "warning" in lnm_out_lower:
    with open("warnings.txt", "w") as fp:
        fp.write(lnm_out_lower)
    test.warning("there are warning messages in lnm output!")


test.ok()
