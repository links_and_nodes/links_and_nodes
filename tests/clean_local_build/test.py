#!/usr/bin/python

import os
import lntest

test = lntest.new_test()

test.short_description = "test clean local build from fresh git export"
test.description = """
create git export
call make local
"""

"""
implicitely:

stop all processes at end of test.
collect all generated reports
classify reports into OK, WARNING, ERROR
"""

file_dir = os.path.realpath(os.path.dirname(__file__))
build_dir = os.path.join("/tmp", "ln_clean_local_build_export")

if os.path.isdir(build_dir):
    os.system("rm -rf '%s'" % build_dir)
os.makedirs(build_dir)

ln_base = os.path.dirname(os.path.dirname(file_dir))
os.chdir(ln_base)

git_archive = test.new_process(
    "git_archive", 
    "/bin/bash",
    arguments=["-c", "/usr/bin/git archive --format=tar HEAD | tar -C '%s' -x" % build_dir]
)

git_archive.wait()
os.chdir(file_dir)

make_local = test.new_process(
    "make_local", 
    "/bin/bash",
    arguments=["-c", "make -C '%s' local" % build_dir]
)
make_local.wait()

test.ok()
