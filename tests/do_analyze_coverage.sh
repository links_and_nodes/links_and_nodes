#!/bin/bash

SCRIPT=$(readlink ${BASH_SOURCE[0]} || echo ${BASH_SOURCE[0]})
RELPATH=$(dirname ${SCRIPT})
pushd ${RELPATH} > /dev/null
TESTS_DIR=$(pwd -P)
popd > /dev/null

tests="console local_publish_subscribe"
args=""
for test in $tests; do
    args="$args $test/*_coverage.py"
done

cd $TESTS_DIR
python3 -m lntest.coverage_analyzer $args
