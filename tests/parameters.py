import os
ln_base = os.path.dirname(os.path.dirname(__file__))

ln_manager_pyscript = os.getenv("LNTEST_LN_MANAGER_PYSCRIPT", None)
if ln_manager_pyscript is None:
    ln_manager_pyscript = os.path.join(ln_base, "python", "links_and_nodes_manager", "ln_manager") # assumes source tree
else:
    if " " in ln_manager_pyscript:
        ln_manager_pyscript = ln_manager_pyscript.split(" ") # if it contains an interpreter...

coverage_skip_list = [
    "/usr/lib",
    os.path.realpath(os.path.join(ln_base, "contrib")),
    os.path.realpath(os.path.join(ln_base, "library", "bindings", "python", "links_and_nodes"))
]

wait_for_output_timeout = 90
