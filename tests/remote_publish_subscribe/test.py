#!/usr/bin/python

import os
import sys
import time
import tempfile

sys.path.insert(0, "..")
import lntest # noqa: E402

test = lntest.new_test()

import testlib # noqa: E402

subB = "all/via udp/subscribeB"
sub = "all/via udp/subscribeC"
tcp_sub = "all/via tcp/subscribeD"

if True:
    lnm = testlib.get_lnm(test, extra_args=["--console"])

    for host in "rmc-lx0307,rmc-lx0141,rmc-taygete,localhost".split(","):
        lnm.write('print self.manager.release_all_daemon_resources("%s")\n' % host)
        time.sleep(0.5)
    lnm.write("quit\n")
    lnm.wait(2*4)

#test.ok(); sys.exit(0)

lnm = testlib.get_lnm(test, extra_args=["--console"], name="lnm2")

def stop(pname):
    lnm.write('stop -w "%s"\n' % pname)
    lnm.wait_for_line_with_regex(["%s: stopped" % pname], timeout=10)
def start(pname):
    lnm.write('start -r "%s"\n' % pname)
    try:
        lnm.wait_for_line_with_regex(["%s: ready" % pname], timeout=10, no_fail_on_exception=True)
    except:
        lnm.write("save_debug_info debug_info\n")
        raise

stop(sub)
#lnm.write("log enable\n")
start(sub)
start("all/publish")
start(subB)
time.sleep(2)

def check_rate(lnm, sub):
    lnm.write('tail -n 1 "%s"\n' % sub)
    matches = lnm.wait_for_line_with_regex(["pub-rate: (.*),.*"], timeout=10)
    try:
        rate = float(matches.group(1))
    except Exception:
        raise Exception("invalid subscriber rate in %r" % matches.group(0))
    expected = 100
    if rate < 0.9 * expected or rate > 1.1 * expected:
        raise Exception("%s: measured rate %r (from %r) is too far from expected rate %r!" % (
            sub, rate, matches.group(0), expected))
check_rate(lnm, sub)
check_rate(lnm, subB)

def dump_state(lnm, fn):
    fd, dump_fn = tempfile.mkstemp()
    os.close(fd)
    lnm.write("save_debug_info %s; echo save_done\n" % dump_fn)
    save_done = lnm.wait_for_line_with_regex("save_done", timeout=10)
    test.report_file(dump_fn, read_at_end=False, out_filename="%s.tar.bz2" % fn)
    os.unlink(dump_fn)
dump_state(lnm, "dump1")

# now kill lnm and start a new one!
lnm.write("quit\n")
lnm.wait(3)

lnm = testlib.get_lnm(test, extra_args=["--console"], name="lnm3")
time.sleep(6) # wait for reconnects
# check output of sub
time.sleep(1) # wait for reconnects
check_rate(lnm, sub)
check_rate(lnm, subB)
dump_state(lnm, "dump2")

start(tcp_sub)
time.sleep(3)
check_rate(lnm, tcp_sub)
check_rate(lnm, sub)

lnm.write("stop -w all\n")
lnm.write("quit\n")
lnm.wait(3)

# when we get here we assume the test went fine up to now. 
# now inspect generated reports to decide the result of this test

testlib.check_lnm_output(test, lnm)

test.ok()
