#!/usr/bin/python

import sys
import time

import links_and_nodes as ln

clnt = ln.client("subscribe", sys.argv)
args = clnt.get_remaining_args()[1:]

reliable = "-reliable" in args
if "-rate" in args:
    rate = float(args[args.index("-rate") + 1])
else:
    rate = -1

msg_def = "uint32_t"
if "-md" in args:
    msg_def = args[args.index("-md") + 1]
    
#while True:
#    time.sleep(1)
#    print "ready, sleeping..."

if ln.LN_LIBRARY_VERSION <= 14:
    port = clnt.subscribe("topic1", msg_def, rate=rate)
else:
    port = clnt.subscribe("topic1", msg_def, need_reliable_transport=reliable, rate=rate)

print("ready, reliable: %s" % reliable)
    
log = []
last = time.time()
while True:
    have_packet = port.read(0.1) is not None
    now = time.time()
    if have_packet:
        log.append((now, port.packet.data))

    while len(log) > 1 and now - log[0][0] > 1:
        del log[0]

    if now - last > 1:
        # est rate
        if not log:
            print("rate: 0")
        else:
            start_t, start_v = log[0]
            end_t, end_v = log[-1]
            end_t = time.time()
            N = end_v - start_v
            t = end_t - start_t
            print("pub-rate: %.2f, packet-rate: %.2f" % (N / t, len(log) / t))
        last = now
    
