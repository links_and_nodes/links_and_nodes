short_description = "test ln_manager gui connection"

description = """
start lnm with/without gui with simple lnc file
start test-lnm-gui-client which tests connection and protocol
wait until test-client finishes
"""

publish_rate = 100

provides = [
    "use ln python binding",
    "add env vars",
    "use local mds",
    "use ready regex"
]

enable_coverage_monitor = True
enable_coverage_monitor = False
