#!/usr/bin/env python2

from __future__ import print_function

import sys
import time

import links_and_nodes as ln

clnt = ln.client("publish", sys.argv)
args = clnt.get_remaining_args()[1:]

rate = 100
if len(args) > 0:
    rate = float(args[0])
msg_def = "uint32_t"
if len(args) > 1:
    msg_def = args[1]

port = clnt.publish("robot.topic.MARKER", msg_def, buffers=10)

print("stuff\nre", end="")
sys.stdout.flush()
time.sleep(1)
crap = "__ady__"
cl = 0
for i in range(int(2100 / len(crap))):
    sys.stdout.write(crap)
    cl += len(crap)
    if cl > 1024:
        cl = 0
        sys.stdout.flush()
        time.sleep(0.5)
sys.stdout.write("\rre")

print("a", end="")
sys.stdout.flush()
time.sleep(0.5)
print("d", end="")
sys.stdout.flush()
time.sleep(0.5)
print("y!")
port.packet.data_array[:] = range(len(port.packet.data_array))

wl = 1 / float(rate)
while True:
    port.packet.data += 1
    port.write()
    time.sleep(wl)
