#!/usr/bin/python

import os
import sys
import threading
import random
import links_and_nodes as ln

class test_client(ln.services_wrapper):
    def __init__(self):
        self.clnt = ln.client("test_client", sys.argv)
        args = self.clnt.get_remaining_args()
        if args:
            call_method = args[0]
        else:
            call_method = "call"
        ln.services_wrapper.__init__(self, self.clnt, "test")

        self.wrap_service("add", "test/add", call_method="call_gobject") # doing call_gobject from multiple threads is invalid!
        
        self.clnt.needs_provider("test.add") # now cause provider to be started!

    def test(self, a, b):
        expected_ret = a + b
        ret = self.add(a, b)
        if ret != expected_ret:
            raise Exception("error: a %d + b %d expected %d got %d!" % (a, b, expected_ret, ret))

class test_thread(threading.Thread):
    def __init__(self, clnt):
        threading.Thread.__init__(self)
        self.clnt = clnt
        self.success = False

    def run(self):
        N = 50
        rm = 10000
        for i in range(N):
            self.clnt.test(
                random.randint(0, rm),
                random.randint(0, rm))
        print("%s tests done" % N)
        self.success = True
        
if __name__ == "__main__":
    c = test_client()
    N = int(os.getenv("N_CLIENT_THREADS", 2))
    threads  = [test_thread(c) for i in range(N)]
    [thread.start() for thread in threads]
    print("%s threads running" % N)
    
    [thread.join() for thread in threads]
    
    success = False not in [thread.success for thread in threads]
    sys.exit(0 if success else 1)
