#!/usr/bin/python

import os
import sys

sys.path.insert(0, "..")
import lntest # noqa: E402

test = lntest.new_test()

old_path = test.parameters["old_ln_base"]
#print "old_path:", old_path
lnc = test.new_template("test.lnc.in")
lnc.generate("test.lnc")

import testlib # noqa: E402

lnm = testlib.get_lnm(test, extra_args=["--console"])

lnm.write("stop -w all\n")
lnm.wait_for_line_with_regex("all/python provider single threaded: stopped")

lnm.write("log enable\n")
lnm.write("start all/current\n")
wait_for = [
    "process 'all/current/client using call' changes state from 'ready' to ",
    "process 'all/current/client using call_gobject' changes state from 'ready' to "
]

if os.path.isdir(old_path):
    lnm.write("start all/old\n")
    wait_for.append("process 'all/old/client using call' changes state from 'ready' to ")

lnm.wait_for_line_with_regex(wait_for, timeout=25)

lnm.write("stop -w all\n")
lnm.write("quit\n")
lnm.wait(10)

# when we get here we assume the test went fine up to now. 
# now inspect generated reports to decide the result of this test

testlib.check_lnm_output(test, lnm, removes=[
    "warning.*?warning, client has older LN-library version than manager",
    "error.*?this client is too old"
])

test.ok()
