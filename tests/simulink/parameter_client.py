import sys
import time

import links_and_nodes

clnt = links_and_nodes.client("dummy_parameter_provider", sys.argv)

pp = clnt.get_parameters("test_ln")

print("pp: %s" % pp)

last = None
for i in range(2):
    pp.set_override("int_clock", None, wait_step=True)

    clock = pp.get_input("clock")
    int_clock = pp.get_input("int_clock", refresh=False)
    fb_int_clock = pp.get_input("fb_int_clock", refresh=False)

    if last is not None:
        if last == clock:
            raise Exception("clock does not change!")
        last = clock
    
    if clock == 0:
        raise Exception("clock is 0!?")
    if int(clock) != int_clock:
        raise Exception("clock %f as int %d does not match int_clock %d!" % (
            clock, int(clock), int_clock))
    if fb_int_clock != int_clock:
        raise Exception("fb_int_clock %d does not match int_clock %d!" % (
            fb_int_clock, int_clock))

    fb_test = 43
    pp.set_override("int_clock", fb_test, wait_step=True)
    fb = pp.get_input("fb_int_clock")

    if fb != fb_test:
        raise Exception("fb_int_clock %r does not match expected override of %r" % (
            fb, fb_test))

    time.sleep(0.1)
    
pp.set_override("int_clock", None, wait_step=True)
print("parameters seem to work fine!")
