#!/bin/bash

export LN_DEBUG=y

exec ./start_lnm.sh --lnm ../../python/links_and_nodes_manager/ln_manager --not-isolated "$@"
