#!/bin/bash

if [ ! -f activate.sh ]; then
    ./conan_install.sh || exit
fi

source activate.sh
source activate_run_python.sh

ln_manager=ln_manager
isolated=--isolated-test
IS_ISOLATED_TO="for lnm pid $(readlink /proc/self)"
other=
while [ ! -z "$1" ]; do
    if [ "$1" == "--lnm" ]; then
	shift
	ln_manager=$1
	shift
    elif [ "$1" == "--not-isolated" ]; then
	shift
	isolated=
	IS_ISOLATED_TO=
    else
	other="$1"
	shift
    fi
done

export IS_ISOLATED_TO

exec $ln_manager $isolated -c test.lnc $other
