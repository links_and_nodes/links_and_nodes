#!/usr/bin/python

import sys

sys.path.insert(0, "..")
import lntest # noqa: E402

test = lntest.new_test()

import testlib # noqa: E402

lnm = testlib.get_lnm(test, extra_args=["--console"])

lnm.write("stop -w all\n")

def start(host):
    global connection
    lnm.write('start -t 5 -i "%s/%s"\n' % (connection, host))
    ret = lnm.wait_for_line_with_regex([
        "connected",
        "terminated unexpectedly",
        "timed out",
    ], timeout=10, wait_for_all=False)
    if ret.group(0) != "connected":
        test.fail("x11-connection to %r on %r failed!" % (connection, host))

lnm.write("stop -w all\n")
lnm.write("log enable\n")

connection = "to gui"
start("A")
start("B")
start("C")

connection = "to manager"
start("A")
start("B")
start("C")

lnm.write("quit\n")
lnm.wait(10)

test.ok()
