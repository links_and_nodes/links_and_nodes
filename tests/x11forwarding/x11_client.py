#!/usr/bin/python

import os
import sys
import gtk.gdk

display = os.environ.get("DISPLAY")

if not display:
    print("there is no DISPLAY environment variable!")
    sys.exit(1)

try:
    dpy = gtk.gdk.Display(display)
    print("connected to %r" % display)
except Exception:
    print("failed to connect to %r" % display)
    sys.exit(1)
