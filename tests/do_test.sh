#!/bin/bash

SCRIPT=$(readlink ${BASH_SOURCE[0]} || echo ${BASH_SOURCE[0]})
RELPATH=$(dirname ${SCRIPT})
pushd ${RELPATH} > /dev/null
TESTS_DIR=$(pwd -P)
popd > /dev/null
TESTARGS="$@"

function run_tests {
    grep -v "^#" $TESTS_DIR/known_good_tests | while read test; do
	if ! python3 $TESTS_DIR/$test/test.py "$TESTARGS"; then
	    echo "ERROR: test '$test' did not succeed!"
	    return 1
	fi
    done || return 1

    echo "all tests OK"
    return 0
}

if [ "x$JENKINS_NODE_COOKIE" == "x" ]; then
    # not on jenkins
    run_tests
else
    # on jenkins!
    export LN_TERM_DEBUG=y
    OUTFN=/tmp/ln_tests_$$.log
    run_tests > $OUTFN 2>&1 # do sane shell-redirect to avoid jenkin's braindead output shuffling
    retval=$?

    cat $OUTFN
    rm $OUTFN

    exit $retval
fi
