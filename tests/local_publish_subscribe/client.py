# -*- encoding: utf-8 -*-

from __future__ import print_function

import sys
import links_and_nodes as ln

clnt = ln.client("service_client test.add", sys.argv)
svc = clnt.get_service('test.add', 'test/add')
svc.utf8_decode_char_fields(False)

svc.req.a = 1
svc.req.b = 2
svc.req.fixed_text = '123456hello'
#svc.req.fixed_text = '123456'
svc.req.fixed_text = '123456'.encode("utf-8")
#svc.req.fixed_text = 'hellöflö'
#svc.req.fixed_text = 'hellöflö'.encode("utf-8")
svc.req.dyn_text = 'd\0yn\0 hellö wörld'
svc.req.dyn_text = 'dyn hellö wörld'
svc.req.dyn_text = 'wörld'

print("call!")
svc.call() # blocking call (might block gui!)
print("call done!")

print("response: c=%r, fixed_text: %r, dyn_text: %r" % (
    svc.resp.c,
    svc.resp.fixed_text,
    svc.resp.dyn_text))
