#!/usr/bin/env python

import os
import sys

retval = 0

some_var = os.getenv("SOME_VAR")
if some_var != "def":
    print("ERROR: invalid some_var: %r" % some_var)
    retval = 1
else:
    print("OK: remove from env-var works")

argv1 = sys.argv[1]
if "*" not in argv1:
    print("ERROR: this command line was interpreted by a shell (%r)!" % argv1)
    retval = 2
else:
    print("OK: del from flags")

path = os.getenv("PATH")
if path is not None:
    print("ERROR: have path env-var! should not be set!")
    retval = 3
else:
    print("OK: del from environment")

sys.exit(retval)
