#!/usr/bin/python

import sys
import time

import links_and_nodes as ln

clnt = ln.client("publish", sys.argv)
args = clnt.get_remaining_args()[1:]

rate = 100
if len(args):
    rate = float(args[0])

port = clnt.publish("topic1", "uint32_t", buffers=10)

print("ready")

wl = 1 / float(rate)
for i in range(100):
    port.packet.data += 1
    port.write()
    time.sleep(wl)

print("about to exit")
time.sleep(0.5)
clnt.unpublish(port)
print("done.")

