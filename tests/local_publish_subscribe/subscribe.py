#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from __future__ import print_function
import sys
import time

import links_and_nodes as ln

print("stdout encoding: %r, is tty: %s" % (sys.stdout.encoding, sys.stdout.isatty()))

clnt = ln.client("subscribe", sys.argv)
args = clnt.get_remaining_args()[1:]

add_rate = None
if args:
    if "/" in args[0]:
        rate, add_rate = tuple(map(float, args[0].split("/")))
    else:
        rate = float(args[0])
else:
    rate = -1.0

md = clnt.get_message_definition("uint32_t")
print("md:")
for i, what in enumerate("md, msg_size, md_hash, flat_md".split(", ")):
    print("  %s: %r" % (what, md[i]))

port = clnt.subscribe("topic1", rate=rate) # no md given at all (same as None)

if add_rate is not None:
    add_port = clnt.subscribe("topic1", rate=add_rate)

print("\nready")

log = []
last = time.time()
p = port.packet
while True:
    have_packet = port.read(0.1) is not None
    now = time.time()
    if have_packet:
        log.append((now, p.data))

    while len(log) > 1 and now - log[0][0] > 1:
        del log[0]

    if now - last > 1 and len(log) > 1:
        # est rate
        start_t, start_v = log[0]
        end_t, end_v = log[-1]
        end_t = time.time()
        N = end_v - start_v
        t = end_t - start_t
        print("rate: %.3f, f1: %f, f1_text: %r, %s" % (N / t, p.f1, p.f1_text, p.f1_text))
        last = now
