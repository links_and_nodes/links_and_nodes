# -*- encoding: utf-8 -*-

from __future__ import print_function

import sys
import time
import signal
import pprint
import shutil
import tempfile

import numpy as np

import links_and_nodes as ln
import links_and_nodes.lnrecorder
import lnrdb

clnt = ln.client(sys.argv[0])
recorder = ln.lnrecorder.lnrecorder_service_wrapper(clnt, "lnrecorder")

keep_running = True

def on_sigterm(signo, frame):
    global keep_running
    print("sigterm!")
    keep_running = False
signal.signal(signal.SIGTERM, on_sigterm)

temp_dir = tempfile.mkdtemp(suffix="-lnrecorder-test")
recorder.record(
    temp_dir,
    patterns=["topic*", "service:*", "!service:lnrecorder.*"] # all services, and stuff that starts with "topic"
    # but not "own" lnrecorder start/stop services!
)
print("recording to %r..." % temp_dir)
while keep_running:
#for i in range(3):
    time.sleep(0.5)
print("stopping recording.")
recorder.stop()

# analyze lnrdb log!
db = lnrdb.Db(temp_dir)
print("have %d db tables:" % len(db.tables))
for name, table in db.tables.items():
    print("  %r has %d rows,  meta:" % (name, table.mmap().shape[0]))
    ind = "    "
    print(ind + ("\n" + ind).join(pprint.pformat(table.meta).split("\n")))
    print()

"""
# no, we dont want to check for our own stop service req/resp!
stop_requests = "services/lnrecorder.stop/client_1"
if stop_requests not in db.tables:
    print("error: missing lnrecorder.stop logged service record!")
else:
    srt = db.tables[stop_requests]
    if srt.meta["service.req_or_resp"] != "req":
        print("error: expected to have stop-request in log!")
    sr = srt.mmap()
    print("stop-request transfer time: %.0fus" % (sr.transfer_time[0] * 1e6))
"""

t1 = db.tables["topics/topic1/1"].mmap()
if t1.shape[0] < 100:
    print("error: too few topic1 rows recorded!")

mean_publisher_rate = 1. / np.diff(t1.publisher_timestamp).mean()
print("mean_publisher_rate: %.1f Hz" % mean_publisher_rate)
if mean_publisher_rate < 90:
    print("recorded publisher rate too low!")

data_diff = np.diff(t1["data"])
ddmin, ddmax = data_diff.min(), data_diff.max()
ddmean = np.array(data_diff, float).mean()
print("data diff min: %d, max: %d, mean: %.2f" % (ddmin, ddmax, ddmean))
if ddmin != 1 or ddmax > 2 or ddmean > 1.05:
    print("error: either lost packets while recording or recorded same packet more than once!")

ok = (t1["data"] == t1.st1["data"]).all() # test consistency of packets
print("data == st1.data: %s" % (ok, ))
if not ok:
    print("error: not all values of data and st1.data match!")

ok = (np.array(t1["data"]/2, dtype=np.uint32) == t1.st3["data"][:, 0]).all() # test consistency of packets
print("data == st3.data: %s" % (ok, ))
if not ok:
    print("error: not all values of data and st3.data match!")

shutil.rmtree(temp_dir)
