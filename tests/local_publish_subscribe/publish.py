# -*- encoding: utf-8 -*-

from __future__ import print_function

import sys
import time
import math

import links_and_nodes as ln

clnt = ln.client("publish", sys.argv)
args = clnt.get_remaining_args()[1:]

rate = 100
if len(args):
    rate = float(args[0])

port = clnt.publish("topic1", "uint32_t", buffers=10)
port2 = clnt.publish("topic2", "double", buffers=10)

print("ready")

f_double_value = 10.

wl = 1 / float(rate)
t0 = time.time()
p = port.packet
subs = [p.st1]
subs.extend(p.st3)
while True:
    now = time.time()
    t = now - t0
    
    p.bigdata += 1
    p.data = p.bigdata & 0xffffffff
    p.f1 = t
    p.f1_text = "ö%.2f" % t # expect ln binding to encode to utf-8
    p.d1 = now
    for i in range(8):
        p.d8[i] = math.sin(t ** i)
        p.d80[i] = t ** i
        p.d80[-i] = t / (i + 1)
        p.f8[i] = t ** i
        for st in subs:
            st.f8[i] = t / (i + 1)
    for k, st in enumerate(subs):
        st.data = int(p.data / (k + 1))
        st.f1 = p.f1 / (k + 1)
        st.f1_text = "%.1f %d" % (st.f1, k)
    p.long_text = (":" * 1023) + "|" + "_" # expect truncation
    port.write()

    port2.packet.value = math.sin(t * f_double_value * 2 * math.pi)
    port2.write()
    time.sleep(wl)
