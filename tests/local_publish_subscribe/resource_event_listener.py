#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

from __future__ import print_function

import sys
import time

import links_and_nodes as ln


class TestApp(object):
    def __init__(self):
        self.clnt = ln.client("resource_event_listener", sys.argv)
        self.event_connection = self.clnt.connect_to_event(
            "ln.resource_event", "ln/resource_event", self.on_resource_event,
            event_pattern="*",
            name_pattern="*",
            md_pattern="*",
            client_pattern="*"
        )
        self.clnt.handle_service_group_in_thread_pool(None, "main")

    def on_resource_event(self, ev):
        print("ev.name %r, ev.event %r, ev.md %r" % (ev.name, ev.event, ev.md))

    def run(self):
        while True:
            time.sleep(1) # all done by thread pool service handlers
            
if __name__ == "__main__":
    app = TestApp()

    print("ready")
    app.run()
