#!/usr/bin/env python3

import sys
import time
import threading
import re
import math
import fnmatch

import links_and_nodes as ln

inter_cmd_sleep = 0.0 # 1
pub_rate = 10
msg_def_name = "double"
wait_input_instead_sleep = "--wait" in sys.argv[1:]

class SimPublisher(threading.Thread):
    def __init__(self, clnt, name, rate):
        threading.Thread.__init__(self)
        self.clnt = clnt
        self.name = name
        self.rate = rate

        self.port = self.clnt.publish(name, msg_def_name)
        self.daemon = True
        self.keep_running = True
        self.start()

    def stop(self):
        self.keep_running = False
        self.join()
        self.clnt.unpublish(self.port)

    def run(self):
        time_step = 1. / self.rate
        start_time = time.time()
        next_time = start_time
        f = 0.5
        while self.keep_running:
            t = time.time() - start_time
            self.port.packet.value = math.sin(t * 2 * math.pi * f)
            self.port.write()

            next_time += time_step
            to_sleep = next_time - time.time()
            if to_sleep > 0:
                time.sleep(to_sleep)

class Sim:
    def __init__(self):
        self.published = {}

        self.clnt = ln.client("publisher")

    def do_pub(self, topic):
        if topic in self.published:
            return
        self.published[topic] = SimPublisher(self.clnt, topic, pub_rate)

    def do_unpub(self, pattern):
        for topic in list(self.published.keys()):
            if fnmatch.fnmatch(topic, pattern):
                self.published[topic].stop()
                del self.published[topic]

    def do_sleep(self, t):
        t = float(t)
        if wait_input_instead_sleep:
            input("sleep %s" % t)
        else:
            time.sleep(t)

def main():
    tasks = """
pub robotC,justin,robotB,robotA
 left_arm,right_arm,torso
  joint,control_module
   telemetry
   commands
  psu
   telemetry
sleep 2
unpub justin.torso.*
sleep 2
pub justin.torso
  joint,control_module
   telemetry
   commands
  psu
   telemetry
sleep 2
unpub justin.torso.*
sleep 2
unpub justin.left_arm.*
sleep 5
unpub justin.right_arm.telemetry
sleep 5
unpub justin.*
sleep 1
unpub robotA,robotB
 *
sleep 1
unpub robotC
 left_arm,right_arm
  *
sleep 1
unpub robotC.torso.psu.*
sleep 1
unpub robotC.torso.*
sleep 1
unpub robotC.*
sleep 5
"""

    # parse
    commands = []
    tasks = tasks.strip().split("\n")
    def unravel(args, prefix=None):
        #print("unravel", args)
        if prefix is None:
            prefix = []
        ret = []
        for parent in args[0]:
            inner_prefix = list(prefix)
            inner_prefix.append(parent)
            if len(args) > 1:
                ret.extend(unravel(args[1:], inner_prefix))
            else:
                ret.append(".".join(inner_prefix))
        return ret

    def finish_cmd(cmd_args):
        if cmd_args is None:
            return
        cmd, args = cmd_args[0], cmd_args[1:]
        args = [a for _, a in args]
        commands.append((cmd, unravel(args)))

    pub = None
    for line in tasks:
        if not line.strip():
            continue
        if line[:1] == "#":
            continue
        m = re.match("^( *)", line)
        plen = len(m.group(1))
        if plen == 0:
            if pub:
                finish_cmd(pub)
            cmd, args = line.split(" ", 1)
            pub = [ cmd, (0, args.split(",")) ]
            continue
        if pub is None:
            print("ignore %r" % line)
            continue
        this_parts = line.lstrip().split(",")
        if plen > pub[-1][0]:
            pub.append((plen, this_parts))
            continue
        finish_cmd(pub)
        while len(pub) > 1 and plen <= pub[-1][0]:
            del pub[-1]
        pub.append((plen, this_parts))
    finish_cmd(pub)
    print("ready")

    # execute
    sim = Sim()
    while True:
        for cmd, args in commands:
            for arg in args:
                print("%s %r" % (cmd, arg))
                getattr(sim, "do_%s" % cmd)(arg)
                time.sleep(inter_cmd_sleep)

    
if __name__ == "__main__":
    main()
