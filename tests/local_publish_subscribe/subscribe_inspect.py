#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from __future__ import print_function
import sys
import time

import links_and_nodes as ln


clnt = ln.client("subscribe", sys.argv)

port = clnt.subscribe("topic1", "uint32_t")

print("\nready")

had_packet = False
last = time.time()
last_had_packet = last
p = port.packet
expected = None
while True:
    have_packet = port.read(0.6) is not None
    now = time.time()
    if have_packet:
        if not had_packet:
            print("again got packet after %.3fs from counter %r" % (now - last_had_packet, p.data))
            had_packet = True
        last_had_packet = now
        if p.data != expected:
            print("expected counter %r, got %r!" % (expected, p.data))
            expected = p.data + 1
        else:
            expected += 1
    else:
        if had_packet:
            print("no longer getting packets! last counter was %r %.3fs ago" % (p.data, now - last_had_packet))
            had_packet = False
