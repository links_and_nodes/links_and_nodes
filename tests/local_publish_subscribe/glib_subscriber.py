#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

from __future__ import print_function

import sys

import links_and_nodes as ln

start_timeout_with_first = "--start-with-first" in sys.argv

class glib_subscriber_test_app(object):
    def __init__(self, mainloop):
        self.mainloop = mainloop
        self.clnt = ln.client("glib_subscribe", sys.argv)
        self.port1 = self.clnt.subscribe("topic1", u"uint32_t") # md given as unicode/str
        if start_timeout_with_first:
            # subscribe same port twice
            self.port2 = self.clnt.subscribe("topic1", "uint32_t")
        else:
            self.port2 = self.clnt.subscribe("topic2", "double") # md given as str
        self.port3 = self.clnt.subscribe("topic1", "") # md given as empty unicode/str
        self.port4 = self.clnt.subscribe("topic1", None) # md given as explicitely as None

        self._timeout = None
        
        self.multi_waiter = ln.MainloopMultiWaiter(self.clnt, mainloop)
        self.multi_waiter.add_port(self.port1, self.on_port1)
        self.multi_waiter.add_port(self.port2, self.on_port2, "test arg1", 42)

        self.port1_count = 0
        self.port2_count = 0

    def check_start_timeout(self):
        if not start_timeout_with_first:
            return
        if self._timeout is None:
            self._timeout = self.mainloop.timeout_add(3, lambda: main.quit())
            print("timeout started!")

    def on_port1(self, port):
        if self.port1_count % 10 == 0:
            print("port1 did publish %r" % port.packet.data)
        self.check_start_timeout()
        self.port1_count += 1
        return True
    
    def on_port2(self, port, arg1, arg2):
        if self.port2_count % 10 == 0:
            if start_timeout_with_first:
                val = port.packet.data
            else:
                val = port.packet.value
            print("port2 did publish %r" % val)
        self.check_start_timeout()
        self.port2_count += 1
        return True


if __name__ == "__main__":
    main = ln.GLibMainloop() # provides ln.MainloopInterface for GLib
    app = glib_subscriber_test_app(main)

    if not start_timeout_with_first:
        main.timeout_add(1, lambda: main.quit())
    
    print("ready")
    main.run()
    
    print("done. port1: %d, port2: %d" % (app.port1_count, app.port2_count))

    expected = 0.5 * 100    
    if app.port1_count < expected or app.port2_count < expected:
        print("ERROR: ports did not get expected number of packets!")
        sys.exit(1)
    
# todo: parameter proxy with mainloop integration
