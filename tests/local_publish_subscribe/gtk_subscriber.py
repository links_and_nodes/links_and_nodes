#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

from __future__ import print_function

import sys

import links_and_nodes as ln

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk # noqa: E402

class gtk_subscriber_test_app(object):
    def __init__(self):
        self.clnt = ln.client("glib_subscribe", sys.argv)
        self.port1 = self.clnt.subscribe("topic1", "uint32_t")
        self.port2 = self.clnt.subscribe("topic2", "double")

        mainloop = ln.GLibMainloop() # will use Gtk's default context! (try to reuse it to not waste resources)
        self.multi_waiter = ln.MainloopMultiWaiter(self.clnt, mainloop)
        self.multi_waiter.add_port(self.port1, self.on_port1)
        self.multi_waiter.add_port(self.port2, self.on_port2, "test arg1", 42)

        self.port1_count = 0
        self.port2_count = 0

        self.window = Gtk.Window()
        self.window.connect("destroy", self.on_destroy)
        b = Gtk.VBox()
        self.window.add(b)
        
        self.label1 = Gtk.Label()
        b.pack_start(self.label1, True, True, 5)
        self.label2 = Gtk.Label()
        b.pack_start(self.label2, True, True, 5)

        self.window.show_all()
        

    def on_port1(self, port):
        self.port1_count += 1
        self.label1.set_label("cnt: %d, val: %r" % (self.port1_count, port.packet.data))
        return True
    
    def on_port2(self, port, arg1, arg2):
        self.port2_count += 1
        self.label2.set_label("cnt: %d, val: %+5.3f" % (self.port2_count, port.packet.value))
        return True

    def on_destroy(self, window):
        self.multi_waiter.stop()
        print("window was closed / destroyed")
        Gtk.main_quit()
    
if __name__ == "__main__":
    app = gtk_subscriber_test_app()

    print("ready")
    Gtk.main()
