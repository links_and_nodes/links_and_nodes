#!/usr/bin/env python3

import os
import sys

from links_and_nodes.lnrpc_server import lnrpc_server

class MyServer(lnrpc_server):
    def __init__(self, **kwargs):
        name = "MyServer"
        if kwargs.get("use_gtk"):
            name += "Gtk"
        lnrpc_server.__init__(self, name, os.path.join(os.path.dirname(__file__), "mds", "lnrpc_test"), **kwargs)

    def add(self, a, b):
        print("add %r to %r" % (a, b))
        return a + b

def main():
    if "gtk3" in sys.argv[1:]:
        srv = MyServer(use_gtk=True)
    else:
        srv = MyServer()

    print("ready")
    srv.run()

if __name__ == "__main__":
    main()
