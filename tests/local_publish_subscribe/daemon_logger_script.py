# -*- encoding: utf-8 -*-

from __future__ import print_function

import sys
import time

import numpy as np

import links_and_nodes as ln

clnt = ln.client(sys.argv[0])

topic_name = "topic1"

t = 0.5
N = int(100 * t)
logger = clnt.get_logger("test logger")
logger.add_topic(topic_name, N, 1)
logger.enable()
print("recording for %.1fs"  % t)
time.sleep(t)
logger.disable()
data = logger.download()

# direct API access to individual samples:
assert list(data.topics.keys()) == [topic_name]
tdata = data.topics[topic_name]
assert tdata.name == topic_name
Nl = tdata.topic.n_samples
print("have %s samples" % Nl)
assert Nl > 0.7 * N

sample = tdata.get_sample(0)
f1 = sample.f1
print("f1: %r" % f1)
#assert sample.f1_text[0] == "ö"

sample = tdata.get_sample(1)
f2 = sample.f1
print("f2: %r" % f2)
assert f2 > f1

direct_data = np.empty((Nl, ), dtype=np.uint32)
for i in range(Nl):
    sample = tdata.get_sample(i)
    direct_data[i] = sample.data

# use wrapper-helper to get arrays from fields
arrays = data.get_dict(do_print=True, use_attr_dict=True)

print("topics in log: %r" % arrays.keys())
assert list(arrays.keys()) == [ topic_name ]

tarrays = arrays[topic_name]
print("topic fields: %s" % ", ".join(tarrays.keys()))

assert (tarrays.data == direct_data).all()

text = tarrays["f1_text"]
print("text array type: %s, shape: %s" % (text.dtype, text.shape))
print("text first: %s/%r, last: %s/%r" % (type(text[0]), text[0], type(text[-1]), text[-1]))

text = tarrays["st1"]["f1_text"]
print("st1.text first: %s/%r, last: %s/%r" % (type(text[0]), text[0], type(text[-1]), text[-1]))

print(len(tarrays["st3"]))
assert len(tarrays["st3"]) == 3

print("st3[0].f1.shape: %s" % (tarrays["st3"][0]["f1"].shape, ))

# todo: save -> load
