import os
import sys
import time
import select
import traceback
import signal

from . import output_buffer

class mainloop(object):
    def __init__(self, runtime):
        self.runtime = runtime
        self.io_watches = {}
        self.child_watches = {}
        self.sigchild_pipe = os.pipe()
        signal.signal(signal.SIGCHLD, self.on_sigchild)
        self.child_watches_to_deliver = []

    def _write_into_output_buffer(self, fd, ob):
        data = fd.read().decode("utf-8")
        #self.runtime.debug("write into output buffer: %r" % data)
        return ob.write(data)

    def add_io_watch(self, fd, cb, name):
        if type(cb) == output_buffer.output_buffer:
            def get_ob_wrap(fd, ob):
                return lambda: self._write_into_output_buffer(fd, ob)
            cb = get_ob_wrap(fd, cb)
        self.io_watches[name] = fd, cb

    def add_child_watch(self, pid, cb):
        self.child_watches[pid] = cb

    def on_sigchild(self, signum, frame):
        #print "got sigchild, wait for pid"
        try:
            pid, exit_status = os.waitpid(-1, os.WNOHANG)
        except Exception:
            return True
        #print "got pid", pid
        if pid == 0:
            return True
        cb = self.child_watches.get(pid)
        if cb is None:
            #self.runtime.info("exit of unknown child process %r with status %d" % (pid, exit_status))
            pass
        else:
            #self.runtime.debug("had sigchild from pid %r with exit_status %r!" % (pid, exit_status))
            self.child_watches_to_deliver.append((pid, exit_status))
            #cb(pid, exit_status)
        os.write(self.sigchild_pipe[1], b".")
        return True

    def iterate(self, deadline):        
        #self.runtime.debug("iterate")
        read_fds = [self.sigchild_pipe[0]]
        for name, (fd, cb) in self.io_watches.items():
            read_fds.append(fd)
        time_left = deadline - time.time()
        if time_left < 0:
            time_left = 0
        self.on_sigchild(0, None)
        try:
            read, write, exc = select.select(read_fds, [], read_fds, time_left)
        except:
            if "Interrupted system call" in str(sys.exc_info()[1]):
                #print "select had signal"
                #print "".join(traceback.format_stack())
                return
            raise
        if self.sigchild_pipe[0] in read:
            os.read(self.sigchild_pipe[0], 1)
            print("## processing received sigchild: %r" % self.child_watches_to_deliver)
            while self.child_watches_to_deliver:
                pid, status = self.child_watches_to_deliver.pop(0)
                print("## pid: %r, status: %r" % (pid, status))
                cb = self.child_watches.get(pid)
                if cb is not None:
                    print("## calling sigchild user cb")
                    cb(pid, status)
                    print("## calling sigchild user cb: done.")
                    return
            print("## no user sigchild cb??")
            return

        if not read and not write and not exc:
            raise Exception("timeout!")
        to_del = []
        for name, (fd, cb) in self.io_watches.items():
            if fd in read or fd in exc:
                try:
                    keep = cb()
                except Exception:
                    self.runtime.info("error in io_watch %r:\n%s" % (name, traceback.format_exc()))
                    keep = False
                if not keep:
                    to_del.append(name)
        for name in to_del:
            del self.io_watches[name]
                    
