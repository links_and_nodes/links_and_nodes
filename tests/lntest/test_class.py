import os
import sys
import traceback
import inspect
import datetime
import atexit
import time
import pprint

def fail_on_exception(func):
    def my_method(self, *args, **kwargs):
        if "no_fail_on_exception" in kwargs:
            no_fail_on_exception = kwargs["no_fail_on_exception"]
            del kwargs["no_fail_on_exception"]
        else:
            no_fail_on_exception = False
        try:
            return func(self, *args, **kwargs)
        except Exception:
            if no_fail_on_exception:
                raise
            stack = traceback.format_stack()[:-1]
            self.test.info(traceback.format_exc())
            self.test.fail("%s\n%s: %s" % ("".join(stack), sys.exc_info()[0].__name__, sys.exc_info()[1]), failing_object=self)
    return my_method

from . import runtime # noqa: E402
from . import file_template # noqa: E402
from . import python_process # noqa: E402
from . import output_buffer # noqa: E402
from . import process # noqa: E402

class test_class(object):
    def __init__(self):
        # figure out own name!
        frame = inspect.currentframe()
        stack = inspect.getouterframes(frame)        
        self.src_file = os.path.realpath(stack[2][1])
        self.src_dir = os.path.dirname(self.src_file)
        self.tests_dir = os.path.dirname(self.src_dir)
        self.start = datetime.datetime.now()
        self.name = os.path.basename(self.src_dir)
        self.processes = {}
        self.buffers = {}
        self.files_to_report_at_end = []
        self.result_name = None
        
        self.mainloop = runtime.get_mainloop()
        self.is_single_test = self.mainloop.runtime.is_single()

        self.show_failing_process_output = False
        self.do_not_save_ok = True
        if self.is_single_test:
            self.show_failing_process_output = True
            if "-v" in sys.argv:
                self.mainloop.runtime.log_level = "debug"
            else:
                self.mainloop.runtime.log_level = "info"
            
            self.do_not_save_ok = True # False # should be True
        
        self.error = self.mainloop.runtime.error
        self.info = self.mainloop.runtime.info
        self.debug = self.mainloop.runtime.debug

        self.file_to_report = []

        self.parameters = {}
        if "--check-coverage" in sys.argv:
            self.parameters["enable_coverage_monitor"] = True
        global_parameters_fn = os.path.join(self.tests_dir, "parameters.py")
        if os.path.isfile(global_parameters_fn):
            self.debug("reading global parameters %r" % global_parameters_fn)
            self.parameters["__file__"] = os.path.realpath(global_parameters_fn)
            exec(compile(open(global_parameters_fn).read(), global_parameters_fn, 'exec'), self.parameters)
            del self.parameters["__file__"]
        local_parameters_fn = os.path.join(self.src_dir, "parameters.py")
        if os.path.isfile(local_parameters_fn):
            self.debug("reading local parameters %r" % local_parameters_fn)
            self.parameters["__file__"] = os.path.realpath(local_parameters_fn)
            exec(compile(open(local_parameters_fn).read(), local_parameters_fn, 'exec'), self.parameters)
            del self.parameters["__builtins__"]
            del self.parameters["__file__"]
        self.file_to_report.append(("parameters.py", pprint.pformat(self.parameters, width=180)))

        atexit.register(self.clean_on_exit)

        self._log = []
        self.mainloop.runtime.running_test = self
        self.report_file(self.src_file)

        if self.tests_dir not in sys.path:
            sys.path.insert(0, self.tests_dir)

    def new_template(self, input_file):
        return file_template.file_template(self, input_file)

    def new_python_process(self, name, *args, **kwargs):
        if name in self.processes:
            raise Exception("process with name %r already exists!" % name)
        p = self.processes[name] = python_process.python_process(self, name, *args, **kwargs)
        return p

    def new_process(self, name, *args, **kwargs):
        if name in self.processes:
            raise Exception("process with name %r already exists!" % name)
        p = self.processes[name] = process.process(self, name, *args, **kwargs)
        return p

    def new_buffer(self, name):
        if name in self.buffers:
            raise Exception("buffer with name %r already exists!" % name)
        b = self.buffers[name] = output_buffer.output_buffer(self, name)
        return b

    def get_parameter(self, name, default=None):
        return self.parameters.get(name, default)
    def set_parameter(self, name, value):
        self.parameters[name] = value

    def report_file(self, filename, read_at_end=False, out_filename=None):
        if read_at_end:
            self.files_to_report_at_end.append(filename)
            return
        if out_filename is None:
            out_filename = filename
        self.file_to_report.append((out_filename, open(filename, "r").read()))

    def fail(self, msg=None, failing_object=None):
        if self.show_failing_process_output and isinstance(failing_object, process.process):
            all_data = "".join(failing_object.stdout.data)
            for line in all_data.split("\n")[-500:]:
                sys.stdout.write("%s\n" % line)
        if msg is not None:
            self.error(msg, throw=False)
        self.result("fail")

    def warning(self, msg=None):
        if msg is not None:
            self.info(msg)
        self.result("warning")

    def ok(self, msg=None):
        if msg is not None:
            self.info(msg)
        self.result("ok")

    def result(self, what):
        self.result_name = what
        sys.stdout.write("%-10.10s %s\n" % (what + ":", self.name))
        do_save = what != "ok"
        if not do_save:
            do_save = True
            if self.do_not_save_ok:
                # check whether last run was also ok, then do not save!
                results_dir = os.path.join(
                    self.src_dir,
                    self.get_parameter("results_dir_name", "results")
                )
                fn = os.path.join(results_dir, "latest")
                if os.path.exists(fn):
                    latest = os.readlink(fn)
                    fn = os.path.join(results_dir, "latest_ok")
                    if os.path.exists(fn):
                        latest_ok = os.readlink(fn)
                        if latest_ok == latest:
                            # latest was ok! do not save!
                            do_save = False            

        # kill all remaining process groups!
        shutdown_time = 5
        for p in list(self.processes.values()):
            if p.terminated is None:                
                p.flush_read(timeout=0.5)
                try:
                    p.p.kill()
                except Exception:
                    pass
        start = time.time()        
        deadline = start + shutdown_time
        for p in list(self.processes.values()):
            while time.time() < deadline and p.terminated is None:
                try:
                    self.mainloop.iterate(deadline)
                except Exception:
                    pass
            if p.terminated is None:
                self.info("killing process %r at pid %r" % (p.name, p.p.pid))
                try:
                    p.p.kill()
                except Exception:
                    pass
        #ps_left = list(self.processes.values())
        #while ps_left:
        #    for p in ps_left:
        #        print "p.name: %r, p.poll(): %r" % (p.name, p.p.poll())
        #for p in self.processes.values():
        #    try:
        #        p.p.send_signal(2)
        #        #p.p.kill()
        #    except Exception:
        #        pass
        #for p in self.processes.values():
        #    print "wait for process %r to exit..." % p.name
        #    try:
        #        p.p.wait()
        #    except Exception:
        #        pass
        
        if do_save:
            self._save_results()

        if what != "ok":
            sys.exit(-1) # todo: only exit if this is a toplevel single test running
        else:
            sys.exit(0)

    def _save_results(self):
        sys.stdout.write("saving results...\n")
        results_base = os.getenv("LNTEST_RESULTS_BASE", None)
        if results_base is None:
            results_base = os.path.join(
                self.src_dir,
                self.get_parameter("results_dir_name", "results")
            )
        else:
            results_base = os.path.join(
                results_base,
                os.path.basename(self.src_dir)
            )
        results_dir = os.path.join(
            results_base,
            self.start.strftime("%Y%m%d_%H%M%S") + "_%s" % self.result_name
        )
        if not os.path.isdir(results_dir):
            os.makedirs(results_dir)

        with open(os.path.join(results_dir, "log"), "w") as fp:
            repl = None
            for ts, level, msg in self._log:
                prefix = "%s %-10.10s " % (ts.strftime("%Y-%m-%d %H:%M:%S"), level)
                if repl is None:
                    repl = "\n" + (" " * len(prefix))
                fp.write("%s%s\n" % (prefix, msg.rstrip("\n").replace("\n", repl)))

        for b in list(self.buffers.values()):
            b.dump_below(results_dir)

        for fn in self.files_to_report_at_end:
            if not os.path.exists(fn):
                self.error("file %r does not exist!" % fn)
            else:
                self.file_to_report.append((fn, open(fn, "r").read()))
        for fn, data in self.file_to_report:
            with open(os.path.join(results_dir, os.path.basename(fn)), "w") as fp:
                fp.write(data)

        def create_link(which):
            link_name = os.path.join(os.path.dirname(results_dir), which)
            if os.path.exists(link_name):
                os.unlink(link_name)
            os.symlink(os.path.basename(results_dir), link_name)
        create_link("latest")
        if self.result_name == "ok":
            create_link("latest_ok")
        if self.result_name == "fail":
            create_link("latest_fail")

    def clean_on_exit(self):
        if self.result_name is None:
            self.fail("test script had exception!")

    def relative_to_test(self, path):
        """
        return pathname in self.test_src if it is not already absolute
        """
        if os.path.isabs(path):
            return path
        return os.path.join(self.src_dir, path)
