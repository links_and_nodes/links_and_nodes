import os

from . import process

class python_process(process.process):
    def __init__(self, test, name, pyscript=None, arguments=[], library_module=None, skip_coverage=False, add_env=None, cwd=None):
        self.pyarguments = arguments
        all_arguments = [ "-u" ]

        if add_env is None:
            add_env = dict()

        if test.get_parameter("enable_coverage_monitor", False) and not skip_coverage:
            coverage_fn = os.path.join(test.src_dir, "%s_coverage.py" % name)
            if pyscript:
                all_arguments.extend((
                    "-m", "lntest.coverage_monitor",
                    "-coverage-fn", coverage_fn))
            else:
                pass # when user wants to execute some library_module we can only enable cov via ln_base from env var...
            test.report_file(coverage_fn, read_at_end=True)
            add_env.update(dict(
                LN_COVERAGE_RECORD="1",
                LN_COVERAGE_RECORD_LNTEST_BASE=test.tests_dir,
                LN_COVERAGE_RECORD_DIR=test.src_dir,
                LN_COVERAGE_RECORD_NAME=name
            ))

        if pyscript:
            self.pyscript = test.relative_to_test(pyscript)
            all_arguments.append(self.pyscript)
        elif library_module:
            all_arguments.extend(("-m", library_module))

        path = os.getenv("PYTHONPATH")
        if path is not None:
            ppath = list(map(os.path.realpath, path.split(":")))
        else:
            ppath = []
        if test.tests_dir not in ppath:
            ppath.insert(0, test.tests_dir)
        add_env["PYTHONPATH"] = ":".join(ppath)

        all_arguments.extend(arguments)
        process.process.__init__(
            self, test, name, 
            command=test.get_parameter("global.python_binary", "python"),
            arguments=all_arguments,
            add_env=add_env,
            cwd=cwd
        )
        if pyscript:
            test.report_file(self.pyscript)
