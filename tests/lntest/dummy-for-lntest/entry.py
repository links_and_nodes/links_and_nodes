#!/usr/bin/python

import sys
import other


def only_need_coverage_for(*features):
    """
    the rest of the calling code-block only needs to be covered by 
    tests that want to test the named features
    """
    pass


test = 1
test += 2
print(test)

try:
    a = test / 0
except Exception:
    print(sys.exc_value)

try: 
    a = 3 / 2
except Exception:
    only_need_coverage_for("div_by_two_is_not_ok")
    print("what?")
    if True:
        print("hier noch wichtiger code!")
        print("der muss ausgefuehrt werden!")
    print("jaja, der ist wichtig!")


if False:
    only_need_coverage_for("if false evaluates true")
    print("true")
else:
    print("wrong")
    print("...helpless")

class app(object):
    class_member = True
    def __init__(self):
        self.cted = True
        self.result = []
        self.had_c = False
        print(self.class_member)
        
    def method_a(self):
        self.result.append("a")
        
    def method_b(self):
        self.result.append("b")
        
    def method_c(self):
        self.result.append("c")
        self.had_c = True
        
    def method_d(self):
        self.result.append("d")
        if self.had_c:
            only_need_coverage_for("test_c_before_d")
            self.result.append("D")

def entry(what):
    a = app()
    for name in what:
        getattr(a, "method_%s" % name)()
    print("".join(a.result))
    other.from_other()
    
if __name__ == "__main__":
    entry(sys.argv[1:])
