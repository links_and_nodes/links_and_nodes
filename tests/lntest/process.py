import traceback
import subprocess
import time
import fcntl
import os
import re
import sys

from . import test_class

class process(object):
    def __init__(self, test, name, command, arguments, add_env=None, cwd=None):
        self.test = test
        self.name = name
        self.command = command
        self.arguments = arguments
        self.p = None
        self.env = dict(os.environ)
        self.env["LNTEST"] = self.test.name
        if add_env is not None:
            self.env.update(add_env)

        if "LN_MANAGER" not in self.env and hasattr(test, "lnm_port"):
            self.env["LN_MANAGER"] = "localhost:%s" % test.lnm_port

        self.cwd = cwd
        self._start()

    def _start(self):
        main = self.test.mainloop
        self.stdout = self.test.new_buffer("%s_stdout" % self.name)
        self.stderr = self.test.new_buffer("%s_stderr" % self.name)

        args = list(self.arguments)
        if isinstance(self.command, (tuple, list)):
            new_args = list(self.command)
            new_args.extend(args)
            args = new_args
        else:
            args.insert(0, self.command)

        self.test.debug("start process %s" % " ".join(['"%s"' % arg for arg in args]))
        self.terminated = None
        self.p = subprocess.Popen(
            map(str, args), 
            stdin=subprocess.PIPE, 
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
            close_fds=True, shell=False,
            env=self.env,
            cwd=self.cwd
        )
        self.test.debug("new process pid: %r" % self.p.pid)
        main.add_child_watch(self.p.pid, self.on_terminate)

        fcntl.fcntl(self.p.stdout, fcntl.F_SETFL, os.O_NONBLOCK)
        #fcntl.fcntl(self.p.stderr, fcntl.F_SETFL, os.O_NONBLOCK) 

        main.add_io_watch(self.p.stdout, self.stdout, self.stdout.name)
        #main.add_io_watch(self.p.stderr, self.inject_stderr_to_stdout, self.stderr.name)

    def write(self, what):
        self.test.debug("\n%s to-stdin: %r" % (self.name, what))
        self.p.stdin.write(what.encode("utf-8"))
        self.p.stdin.flush()
        
    def get_output(self):
        return "".join(self.stdout.data)

    def inject_stderr_to_stdout(self):
        data = self.p.stderr.read().decode("utf-8")
        ret = self.stderr.write(data)
        self.stdout.write(data)
        return ret

    def read_byte(self, timeout=None, deadline=None):
        if deadline is None:
            if timeout is None:
                timeout = self.test.get_parameter("wait_for_output_timeout", 5)
            start = time.time()
            deadline = start + timeout
        return self.stdout.read(1, deadline=deadline)

    def read_line(self, timeout=None, deadline=None):
        if deadline is None:
            if timeout is None:
                timeout = self.test.get_parameter("wait_for_output_timeout", 5)
            start = time.time()
            deadline = start + timeout
        # ugly but for now no line_Assembler
        line = ""
        while True:
            char = self.read_byte(deadline=deadline)
            if char == "\n":
                return line
            line += char

    def flush_read(self, timeout=None, deadline=None):
        if deadline is None:
            if timeout is None:
                timeout = self.test.get_parameter("wait_for_output_timeout", 5)
            start = time.time()
            deadline = start + timeout
        ret = []
        while True:
            try:
                ret.append(self.read_byte(deadline=deadline))
            except Exception:
                break
        return "".join(ret)
    
    def wait_for_stable(self, search, deadline, timeout=None):
        if timeout is None:
            timeout = 1
        matched = 0
        ret = []
        while True:
            if time.time() > deadline:
                raise Exception("timeout waiting for stable %r" % search)
            try:
                ch = self.read_byte(timeout=timeout)
            except Exception:
                if matched == len(search):
                    break
                continue
                            
            ret.append(ch)
            if matched == len(search):
                if ch == "":
                    break
                matched = 0
            if ch == search[matched]:
                matched += 1
            else:
                matched = 0                
        return "".join(ret)

    def on_terminate(self, pid, exit_status):
        print("## on_terminate called for pid %r exit_status %r" % (pid, exit_status))
        self.terminated = exit_status
        return False
    
    @test_class.fail_on_exception
    def wait_for_line_with_regex(self, regex, timeout=None, hint=None, wait_for_all=True, throw_on=None):
        """
        blocking read lines of stdout from this process until all given regex'es matched atleast once.
        
        args:
          regex: can be string or sequence of strings
        """
        if not isinstance(regex, (tuple, list)):
            regex = [ regex ]
        if throw_on is not None and not isinstance(throw_on, (tuple, list)):
            throw_on = [ throw_on ]
        if timeout is None:
            timeout = self.test.get_parameter("wait_for_output_timeout", 30)
        start = time.time()
        deadline = start + timeout
        ret = []
        while regex:
            try:
                line = self.read_line(deadline=deadline)
            except Exception:
                if hint is not None:
                    hint = "\nhint: %s\n" % hint
                else:
                    hint = ""
                raise Exception("timeout (%ss) waiting for these regexes:\n  %s\n%s%s" % (timeout, "\n  ".join(regex), traceback.format_exc(), hint))
            if throw_on:
                for ex in throw_on:
                    m = re.search(ex, line)
                    if m is None:
                        continue
                    raise Exception("aborting wait for below regexes because %r matched %r!\n  %s" % (ex, line, "\n  ".join(regex)))
            for i, ex in enumerate(regex):
                m = re.search(ex, line)
                if m is not None:
                    ret.append(m)
                    del regex[i]
                    if not wait_for_all:
                        return ret[0]
                    break
            #else:
            #    self.test.debug("wait ignore this line: %r" % line)
        if len(ret) == 1:
            return ret[0]
        return ret
    
    @test_class.fail_on_exception
    def wait(self, timeout=None):
        if timeout is None:
            timeout = self.test.get_parameter("wait_for_exit_timeout", 50)
        start = time.time()
        deadline = start + timeout
        while self.terminated is None:
            print("## wait for %r pid %s %.1f more seconds" % (self.name, self.p.pid, deadline - time.time()))
            now = time.time()
            small_deadline = now + 5
            try:
                self.test.mainloop.iterate(deadline=small_deadline)
            except Exception:
                if sys.exc_info()[1] != "timeout!":
                    print("## exception from mainloop iterate: %s" % sys.exc_info()[1])
            ret = self.p.poll()
            print("## %r iteration done after %.1fs, self.terminated: %r, poll-returncode: %r" % (
                self.name, time.time() - now, self.terminated, ret))
            if ret is not None: # somehow we seem to miss some sigchilds?!
                self.terminated = ret
                break
            if time.time() >= deadline:
                raise Exception("timeout waiting for pid %r!" % self.p.pid)
        self.p.wait()
        # is already set by sigchild handler: self.terminated = self.p.returncode
        self.test.debug("process %r had returncode of %r" % (self.name, self.terminated))
        if self.terminated != 0:
            raise Exception("process %r had returncode of %r" % (self.name, self.terminated))
