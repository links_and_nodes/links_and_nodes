## nb properties ##
{'current_cell': 4, 'window_position': (1929, 207), 'window_size': (1507, 781)}
## nb properties end ##
## cell 0 input ##
import ast
fn = "dummy-for-lntest/entry.py"


## cell 0 end ##
## cell 1 input ##
root = ast.parse(open(fn, "r").read(), fn)

## cell 1 end ##
## cell 2 input ##
root = ast.parse("""

if False:
    only_need_coverage_for("publish_known_topic")
    print "not needed"
else:
    print "needed"
""", "<test>")
% ast.dump(root)
## cell 2 output ##
# ast.dump(root): Module(body=[If(test=Name(id='False', ctx=Load()), body=[Expr(value=Call(func=Name(id='only_need_coverage_for', ctx=Load()), args=[Str(s='publish_known_topic')], keywords=[], starargs=None, kwargs=None)), Print(dest=None, values=[Str(s='not needed')], nl=True)], orelse=[Print(dest=None, values=[Str(s='needed')], nl=True)])])
# 
## cell 2 end ##
## cell 3 input ##
ast.dump(root): Module(body=[
 If(test=Name(id='False', ctx=Load()), 
    body=[
      Expr(value=Call(func=Name(id='only_need_coverage_for', ctx=Load()), args=[Str(s='publish_known_topic')], keywords=[], starargs=None, kwargs=None)), 
      Print(dest=None, values=[Str(s='not needed')], nl=True)
    ], 
    orelse=[
      Print(dest=None, values=[Str(s='needed')
    ], 
    nl=True)]
 )
])

## cell 3 end ##
## cell 4 input ##
expr = root.body[0].body[0]
call = expr.value
%% type(call)
%% call.func.id
%% call.args[0].s

## cell 4 output ##
#  type(call): <class '_ast.Call'>
#  call.func.id: 'only_need_coverage_for'
#  call.args[0].s: 'publish_known_topic'
# 
## cell 4 end ##
## cell 5 input ##
%% root
% ast.dump(root)
## cell 5 output ##
# root: <_ast.Module object at 0x909bf6c>
#  ast.dump(root): Module(body=[If(test=Name(id='False', ctx=Load()), body=[Expr(value=Call(func=Name(id='only_need_coverage_for', ctx=Load()), args=[Str(s='publish_known_topic')], keywords=[], starargs=None, kwargs=None)), Print(dest=None, values=[Str(s='not needed')], nl=True)], orelse=[Print(dest=None, values=[Str(s='needed')], nl=True)])])
# 
## cell 5 end ##
## cell 6 input ##
 ast.dump(root): Module(body=[
 Import(names=[alias(name='sys', asname=None)]), 
 Import(names=[alias(name='other', asname=None)]), 
 Assign(targets=[Name(id='test', ctx=Store())], value=Num(n=1)), 
 AugAssign(target=Name(id='test', ctx=Store()), op=Add(), value=Num(n=2)),
 Print(dest=None, values=[Name(id='test', ctx=Load())], nl=True), 
 TryExcept(
   body=[
     Assign(targets=[Name(id='a', ctx=Store())], value=BinOp(left=Name(id='test', ctx=Load()), op=Div(), right=Num(n=0)))
   ], 
   handlers=[
     ExceptHandler(type=None, name=None, body=[
      Print(dest=None, values=[Attribute(value=Name(id='sys', ctx=Load()), attr='exc_value', ctx=Load())], nl=True)])], orelse=[]), 
      
      
 ClassDef(name='app', bases=[Name(id='object', ctx=Load())], body=[Assign(targets=[Name(id='class_member', ctx=Store())], value=Name(id='True', ctx=Load())), FunctionDef(name='__init__', args=arguments(args=[Name(id='self', ctx=Param())], vararg=None, kwarg=None, defaults=[]), body=[Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), attr='cted', ctx=Store())], value=Name(id='True', ctx=Load())), Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), attr='result', ctx=Store())], value=List(elts=[], ctx=Load())), Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), attr='had_c', ctx=Store())], value=Name(id='False', ctx=Load())), Print(dest=None, values=[Attribute(value=Name(id='self', ctx=Load()), attr='class_member', ctx=Load())], nl=True)], decorator_list=[]), FunctionDef(name='method_a', args=arguments(args=[Name(id='self', ctx=Param())], vararg=None, kwarg=None, defaults=[]), body=[Expr(value=Call(func=Attribute(value=Attribute(value=Name(id='self', ctx=Load()), attr='result', ctx=Load()), attr='append', ctx=Load()), args=[Str(s='a')], keywords=[], starargs=None, kwargs=None))], decorator_list=[]), FunctionDef(name='method_b', args=arguments(args=[Name(id='self', ctx=Param())], vararg=None, kwarg=None, defaults=[]), body=[Expr(value=Call(func=Attribute(value=Attribute(value=Name(id='self', ctx=Load()), attr='result', ctx=Load()), attr='append', ctx=Load()), args=[Str(s='b')], keywords=[], starargs=None, kwargs=None))], decorator_list=[]), FunctionDef(name='method_c', args=arguments(args=[Name(id='self', ctx=Param())], vararg=None, kwarg=None, defaults=[]), body=[Expr(value=Call(func=Attribute(value=Attribute(value=Name(id='self', ctx=Load()), attr='result', ctx=Load()), attr='append', ctx=Load()), args=[Str(s='c')], keywords=[], starargs=None, kwargs=None)), Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), attr='had_c', ctx=Store())], value=Name(id='True', ctx=Load()))], decorator_list=[]), FunctionDef(name='method_d', args=arguments(args=[Name(id='self', ctx=Param())], vararg=None, kwarg=None, defaults=[]), body=[Expr(value=Call(func=Attribute(value=Attribute(value=Name(id='self', ctx=Load()), attr='result', ctx=Load()), attr='append', ctx=Load()), args=[Str(s='d')], keywords=[], starargs=None, kwargs=None)), If(test=Attribute(value=Name(id='self', ctx=Load()), attr='had_c', ctx=Load()), body=[Expr(value=Call(func=Attribute(value=Attribute(value=Name(id='self', ctx=Load()), attr='result', ctx=Load()), attr='append', ctx=Load()), args=[Str(s='D')], keywords=[], starargs=None, kwargs=None))], orelse=[])], decorator_list=[])], decorator_list=[]), FunctionDef(name='entry', args=arguments(args=[Name(id='what', ctx=Param())], vararg=None, kwarg=None, defaults=[]), body=[Assign(targets=[Name(id='a', ctx=Store())], value=Call(func=Name(id='app', ctx=Load()), args=[], keywords=[], starargs=None, kwargs=None)), For(target=Name(id='name', ctx=Store()), iter=Name(id='what', ctx=Load()), body=[Expr(value=Call(func=Call(func=Name(id='getattr', ctx=Load()), args=[Name(id='a', ctx=Load()), BinOp(left=Str(s='method_%s'), op=Mod(), right=Name(id='name', ctx=Load()))], keywords=[], starargs=None, kwargs=None), args=[], keywords=[], starargs=None, kwargs=None))], orelse=[]), Print(dest=None, values=[Call(func=Attribute(value=Str(s=''), attr='join', ctx=Load()), args=[Attribute(value=Name(id='a', ctx=Load()), attr='result', ctx=Load())], keywords=[], starargs=None, kwargs=None)], nl=True), Expr(value=Call(func=Attribute(value=Name(id='other', ctx=Load()), attr='from_other', ctx=Load()), args=[], keywords=[], starargs=None, kwargs=None))], decorator_list=[]), If(test=Compare(left=Name(id='__name__', ctx=Load()), ops=[Eq()], comparators=[Str(s='__main__')]), body=[Expr(value=Call(func=Name(id='entry', ctx=Load()), args=[Subscript(value=Attribute(value=Name(id='sys', ctx=Load()), attr='argv', ctx=Load()), slice=Slice(lower=Num(n=1), upper=None, step=None), ctx=Load())], keywords=[], starargs=None, kwargs=None))], orelse=[])])
## cell 6 end ##
## cell 7 input ##
import inspect
import _ast
%% inspect.getclasstree([_ast.ExceptHandler])
% ast.excepthandler
## cell 7 output ##
#  inspect.getclasstree([_ast.ExceptHandler]): [(<class '_ast.excepthandler'>, (<type '_ast.AST'>,)),
#  [(<class '_ast.ExceptHandler'>, (<class '_ast.excepthandler'>,))]]
#  ast.excepthandler: <class '_ast.excepthandler'>
# 
## cell 7 end ##
## cell 8 input ##
%% root._fields
%% root.body[4]
%% isinstance(root.body[4], (ast.expr, ast.stmt))
%% root.body[4].lineno
%% root.body[4].col_offset
## cell 8 output ##
#  root._fields: ('body',)
#  root.body[4]: <_ast.If object at 0x912fc4c>
#  isinstance(root.body[4], (ast.expr, ast.stmt)): True
#  root.body[4].lineno: 50
#  root.body[4].col_offset: 0
# 
## cell 8 end ##
## cell 9 input ##
%% isinstance(root.body[0], ast.Import)
%% root.body[3].name
## cell 9 output ##
#  isinstance(root.body[0], ast.Import): True
#  root.body[3].name: 'percent'
# 
## cell 9 end ##
## cell 10 input ##
def recurse(parent, depth=0):
    for node in ast.iter_child_nodes(parent):
        if isinstance(node, (ast.expr, ast.stmt)):
            %% depth, node.lineno, node.col_offset, node
        recurse(node, depth+1)
recurse(root)
## cell 10 output ##
#  depth, node.lineno, node.col_offset, node: (0, 1, 0, <_ast.Import object at 0x911a3cc>)
#  depth, node.lineno, node.col_offset, node: (0, 2, 0, <_ast.Import object at 0x911a42c>)
#  depth, node.lineno, node.col_offset, node: (0, 4, 0, <_ast.ClassDef object at 0x911a46c>)
#  depth, node.lineno, node.col_offset, node: (1, 4, 24, <_ast.Name object at 0x911a48c>)
#  depth, node.lineno, node.col_offset, node: (1, 5, 4, <_ast.FunctionDef object at 0x911a4ac>)
#  depth, node.lineno, node.col_offset, node: (3, 5, 17, <_ast.Name object at 0x911a4ec>)
#  depth, node.lineno, node.col_offset, node: (2, 6, 8, <_ast.Assign object at 0x911a54c>)
#  depth, node.lineno, node.col_offset, node: (3, 6, 8, <_ast.Attribute object at 0x911a56c>)
#  depth, node.lineno, node.col_offset, node: (4, 6, 8, <_ast.Name object at 0x911a58c>)
#  depth, node.lineno, node.col_offset, node: (3, 6, 29, <_ast.Num object at 0x911a5ac>)
#  depth, node.lineno, node.col_offset, node: (2, 7, 8, <_ast.Assign object at 0x911a5cc>)
#  depth, node.lineno, node.col_offset, node: (3, 7, 8, <_ast.Attribute object at 0x911a5ec>)
#  depth, node.lineno, node.col_offset, node: (4, 7, 8, <_ast.Name object at 0x911a60c>)
#  depth, node.lineno, node.col_offset, node: (3, 7, 31, <_ast.Num object at 0x911a62c>)
#  depth, node.lineno, node.col_offset, node: (2, 8, 8, <_ast.Assign object at 0x911a64c>)
#  depth, node.lineno, node.col_offset, node: (3, 8, 8, <_ast.Attribute object at 0x911a66c>)
#  depth, node.lineno, node.col_offset, node: (4, 8, 8, <_ast.Name object at 0x911a68c>)
#  depth, node.lineno, node.col_offset, node: (3, 8, 29, <_ast.List object at 0x911a6ac>)
#  depth, node.lineno, node.col_offset, node: (2, 10, 8, <_ast.Assign object at 0x911a6cc>)
#  depth, node.lineno, node.col_offset, node: (3, 10, 8, <_ast.Attribute object at 0x911a6ec>)
#  depth, node.lineno, node.col_offset, node: (4, 10, 8, <_ast.Name object at 0x911a70c>)
#  depth, node.lineno, node.col_offset, node: (3, 10, 33, <_ast.Num object at 0x911a72c>)
#  depth, node.lineno, node.col_offset, node: (2, 11, 8, <_ast.Assign object at 0x911a74c>)
#  depth, node.lineno, node.col_offset, node: (3, 11, 8, <_ast.Attribute object at 0x911a76c>)
#  depth, node.lineno, node.col_offset, node: (4, 11, 8, <_ast.Name object at 0x911a78c>)
#  depth, node.lineno, node.col_offset, node: (3, 11, 35, <_ast.Num object at 0x911a7ac>)
#  depth, node.lineno, node.col_offset, node: (2, 12, 8, <_ast.Assign object at 0x911a7cc>)
#  depth, node.lineno, node.col_offset, node: (3, 12, 8, <_ast.Attribute object at 0x911a7ec>)
#  depth, node.lineno, node.col_offset, node: (4, 12, 8, <_ast.Name object at 0x911a80c>)
#  depth, node.lineno, node.col_offset, node: (3, 12, 35, <_ast.List object at 0x911a82c>)
#  depth, node.lineno, node.col_offset, node: (2, 14, 8, <_ast.Assign object at 0x911a84c>)
#  depth, node.lineno, node.col_offset, node: (3, 14, 8, <_ast.Attribute object at 0x911a86c>)
#  depth, node.lineno, node.col_offset, node: (4, 14, 8, <_ast.Name object at 0x911a88c>)
#  depth, node.lineno, node.col_offset, node: (3, 14, 29, <_ast.Dict object at 0x911a8ac>)
#  depth, node.lineno, node.col_offset, node: (1, 16, 4, <_ast.FunctionDef object at 0x911a8cc>)
#  depth, node.lineno, node.col_offset, node: (3, 16, 14, <_ast.Name object at 0x911a90c>)
#  depth, node.lineno, node.col_offset, node: (3, 16, 20, <_ast.Name object at 0x911a92c>)
#  depth, node.lineno, node.col_offset, node: (2, 17, 8, <_ast.Assign object at 0x911a94c>)
#  depth, node.lineno, node.col_offset, node: (3, 17, 8, <_ast.Name object at 0x911a96c>)
#  depth, node.lineno, node.col_offset, node: (3, 17, 14, <_ast.Dict object at 0x911a98c>)
#  depth, node.lineno, node.col_offset, node: (2, 18, 8, <_ast.TryExcept object at 0x911a9ac>)
#  depth, node.lineno, node.col_offset, node: (3, 19, 12, <_ast.Expr object at 0x911a9cc>)
#  depth, node.lineno, node.col_offset, node: (4, 19, 12, <_ast.Call object at 0x911a9ec>)
#  depth, node.lineno, node.col_offset, node: (5, 19, 12, <_ast.Name object at 0x911aa0c>)
#  depth, node.lineno, node.col_offset, node: (5, 19, 21, <_ast.Name object at 0x911aa2c>)
#  depth, node.lineno, node.col_offset, node: (5, 19, 31, <_ast.Name object at 0x911aa4c>)
#  depth, node.lineno, node.col_offset, node: (5, 19, 36, <_ast.Name object at 0x911aa6c>)
#  depth, node.lineno, node.col_offset, node: (4, 21, 12, <_ast.Print object at 0x911aacc>)
#  depth, node.lineno, node.col_offset, node: (5, 21, 18, <_ast.BinOp object at 0x911aaec>)
#  depth, node.lineno, node.col_offset, node: (6, 21, 18, <_ast.Str object at 0x911ab0c>)
#  depth, node.lineno, node.col_offset, node: (6, 21, 59, <_ast.Tuple object at 0x911ab2c>)
#  depth, node.lineno, node.col_offset, node: (7, 21, 59, <_ast.Name object at 0x911ab4c>)
#  depth, node.lineno, node.col_offset, node: (7, 21, 69, <_ast.Call object at 0x911ab6c>)
#  depth, node.lineno, node.col_offset, node: (8, 21, 69, <_ast.Attribute object at 0x911ab8c>)
#  depth, node.lineno, node.col_offset, node: (9, 21, 69, <_ast.Name object at 0x911abac>)
#  depth, node.lineno, node.col_offset, node: (4, 22, 12, <_ast.Raise object at 0x911abcc>)
#  depth, node.lineno, node.col_offset, node: (2, 23, 8, <_ast.Assign object at 0x911ac2c>)
#  depth, node.lineno, node.col_offset, node: (3, 23, 8, <_ast.Subscript object at 0x911ac4c>)
#  depth, node.lineno, node.col_offset, node: (4, 23, 8, <_ast.Attribute object at 0x911ac6c>)
#  depth, node.lineno, node.col_offset, node: (5, 23, 8, <_ast.Name object at 0x911ac8c>)
#  depth, node.lineno, node.col_offset, node: (5, 23, 27, <_ast.Name object at 0x911accc>)
#  depth, node.lineno, node.col_offset, node: (3, 23, 39, <_ast.Subscript object at 0x911acec>)
#  depth, node.lineno, node.col_offset, node: (4, 23, 39, <_ast.Name object at 0x911ad0c>)
#  depth, node.lineno, node.col_offset, node: (5, 23, 43, <_ast.Str object at 0x911ad4c>)
#  depth, node.lineno, node.col_offset, node: (1, 25, 4, <_ast.FunctionDef object at 0x911ad6c>)
#  depth, node.lineno, node.col_offset, node: (3, 25, 16, <_ast.Name object at 0x911adac>)
#  depth, node.lineno, node.col_offset, node: (2, 26, 8, <_ast.Assign object at 0x911adcc>)
#  depth, node.lineno, node.col_offset, node: (3, 26, 8, <_ast.Attribute object at 0x911adec>)
#  depth, node.lineno, node.col_offset, node: (4, 26, 8, <_ast.Name object at 0x911ae0c>)
#  depth, node.lineno, node.col_offset, node: (3, 26, 29, <_ast.Call object at 0x911ae2c>)
#  depth, node.lineno, node.col_offset, node: (4, 26, 29, <_ast.Name object at 0x911ae4c>)
#  depth, node.lineno, node.col_offset, node: (2, 27, 8, <_ast.For object at 0x911ae6c>)
#  depth, node.lineno, node.col_offset, node: (3, 27, 12, <_ast.Name object at 0x911ae8c>)
#  depth, node.lineno, node.col_offset, node: (3, 27, 21, <_ast.Call object at 0x911aeac>)
#  depth, node.lineno, node.col_offset, node: (4, 27, 21, <_ast.Attribute object at 0x911aecc>)
#  depth, node.lineno, node.col_offset, node: (5, 27, 21, <_ast.Attribute object at 0x911aeec>)
#  depth, node.lineno, node.col_offset, node: (6, 27, 21, <_ast.Name object at 0x911af0c>)
#  depth, node.lineno, node.col_offset, node: (3, 28, 12, <_ast.Expr object at 0x911af2c>)
#  depth, node.lineno, node.col_offset, node: (4, 28, 12, <_ast.Call object at 0x911af4c>)
#  depth, node.lineno, node.col_offset, node: (5, 28, 12, <_ast.Attribute object at 0x911af6c>)
#  depth, node.lineno, node.col_offset, node: (6, 28, 12, <_ast.Attribute object at 0x911af8c>)
#  depth, node.lineno, node.col_offset, node: (7, 28, 12, <_ast.Name object at 0x911afac>)
#  depth, node.lineno, node.col_offset, node: (5, 28, 38, <_ast.Call object at 0x911afcc>)
#  depth, node.lineno, node.col_offset, node: (6, 28, 38, <_ast.Attribute object at 0x911afec>)
#  depth, node.lineno, node.col_offset, node: (7, 28, 38, <_ast.Name object at 0x912f02c>)
#  depth, node.lineno, node.col_offset, node: (2, 29, 8, <_ast.Assign object at 0x912f04c>)
#  depth, node.lineno, node.col_offset, node: (3, 29, 8, <_ast.Attribute object at 0x912f06c>)
#  depth, node.lineno, node.col_offset, node: (4, 29, 8, <_ast.Name object at 0x912f08c>)
#  depth, node.lineno, node.col_offset, node: (3, 29, 29, <_ast.Call object at 0x912f0ac>)
#  depth, node.lineno, node.col_offset, node: (4, 29, 29, <_ast.Name object at 0x912f0cc>)
#  depth, node.lineno, node.col_offset, node: (4, 29, 34, <_ast.Attribute object at 0x912f0ec>)
#  depth, node.lineno, node.col_offset, node: (5, 29, 34, <_ast.Name object at 0x912f10c>)
#  depth, node.lineno, node.col_offset, node: (2, 30, 8, <_ast.Expr object at 0x912f12c>)
#  depth, node.lineno, node.col_offset, node: (3, 30, 8, <_ast.Call object at 0x912f14c>)
#  depth, node.lineno, node.col_offset, node: (4, 30, 8, <_ast.Attribute object at 0x912f16c>)
#  depth, node.lineno, node.col_offset, node: (5, 30, 8, <_ast.Attribute object at 0x912f18c>)
#  depth, node.lineno, node.col_offset, node: (6, 30, 8, <_ast.Name object at 0x912f1ac>)
#  depth, node.lineno, node.col_offset, node: (2, 31, 8, <_ast.Assign object at 0x912f1cc>)
#  depth, node.lineno, node.col_offset, node: (3, 31, 8, <_ast.Attribute object at 0x912f1ec>)
#  depth, node.lineno, node.col_offset, node: (4, 31, 8, <_ast.Name object at 0x912f20c>)
#  depth, node.lineno, node.col_offset, node: (3, 31, 31, <_ast.Call object at 0x912f22c>)
#  depth, node.lineno, node.col_offset, node: (4, 31, 31, <_ast.Name object at 0x912f24c>)
#  depth, node.lineno, node.col_offset, node: (4, 31, 35, <_ast.Attribute object at 0x912f26c>)
#  depth, node.lineno, node.col_offset, node: (5, 31, 35, <_ast.Name object at 0x912f28c>)
#  depth, node.lineno, node.col_offset, node: (2, 34, 8, <_ast.Assign object at 0x912f2ac>)
#  depth, node.lineno, node.col_offset, node: (3, 34, 8, <_ast.Attribute object at 0x912f2cc>)
#  depth, node.lineno, node.col_offset, node: (4, 34, 8, <_ast.Name object at 0x912f2ec>)
#  depth, node.lineno, node.col_offset, node: (3, 34, 27, <_ast.Call object at 0x912f30c>)
#  depth, node.lineno, node.col_offset, node: (4, 34, 27, <_ast.Name object at 0x912f32c>)
#  depth, node.lineno, node.col_offset, node: (4, 34, 32, <_ast.Attribute object at 0x912f34c>)
#  depth, node.lineno, node.col_offset, node: (5, 34, 32, <_ast.Name object at 0x912f36c>)
#  depth, node.lineno, node.col_offset, node: (2, 35, 8, <_ast.Assign object at 0x912f38c>)
#  depth, node.lineno, node.col_offset, node: (3, 35, 8, <_ast.Attribute object at 0x912f3ac>)
#  depth, node.lineno, node.col_offset, node: (4, 35, 8, <_ast.Name object at 0x912f3cc>)
#  depth, node.lineno, node.col_offset, node: (3, 35, 29, <_ast.Call object at 0x912f3ec>)
#  depth, node.lineno, node.col_offset, node: (4, 35, 29, <_ast.Name object at 0x912f40c>)
#  depth, node.lineno, node.col_offset, node: (4, 35, 33, <_ast.Attribute object at 0x912f44c>)
#  depth, node.lineno, node.col_offset, node: (5, 35, 33, <_ast.Name object at 0x912f46c>)
#  depth, node.lineno, node.col_offset, node: (2, 38, 8, <_ast.Assign object at 0x912f4ac>)
#  depth, node.lineno, node.col_offset, node: (3, 38, 8, <_ast.Attribute object at 0x912f4ec>)
#  depth, node.lineno, node.col_offset, node: (4, 38, 8, <_ast.Name object at 0x912f50c>)
#  depth, node.lineno, node.col_offset, node: (3, 38, 31, <_ast.List object at 0x912f52c>)
#  depth, node.lineno, node.col_offset, node: (2, 39, 8, <_ast.For object at 0x912f56c>)
#  depth, node.lineno, node.col_offset, node: (3, 39, 12, <_ast.Name object at 0x912f58c>)
#  depth, node.lineno, node.col_offset, node: (3, 39, 18, <_ast.Attribute object at 0x912f5ac>)
#  depth, node.lineno, node.col_offset, node: (4, 39, 18, <_ast.Name object at 0x912f5cc>)
#  depth, node.lineno, node.col_offset, node: (3, 40, 12, <_ast.Expr object at 0x912f60c>)
#  depth, node.lineno, node.col_offset, node: (4, 40, 12, <_ast.Call object at 0x912f62c>)
#  depth, node.lineno, node.col_offset, node: (5, 40, 12, <_ast.Attribute object at 0x912f64c>)
#  depth, node.lineno, node.col_offset, node: (6, 40, 12, <_ast.Name object at 0x912f66c>)
#  depth, node.lineno, node.col_offset, node: (5, 40, 41, <_ast.Name object at 0x912f6ac>)
#  depth, node.lineno, node.col_offset, node: (5, 40, 45, <_ast.Attribute object at 0x912f6cc>)
#  depth, node.lineno, node.col_offset, node: (6, 40, 45, <_ast.Name object at 0x912f6ec>)
#  depth, node.lineno, node.col_offset, node: (1, 42, 4, <_ast.FunctionDef object at 0x912f76c>)
#  depth, node.lineno, node.col_offset, node: (3, 42, 32, <_ast.Name object at 0x912f7cc>)
#  depth, node.lineno, node.col_offset, node: (3, 42, 38, <_ast.Name object at 0x912f7ec>)
#  depth, node.lineno, node.col_offset, node: (3, 42, 42, <_ast.Name object at 0x912f80c>)
#  depth, node.lineno, node.col_offset, node: (2, 43, 8, <_ast.Pass object at 0x912f86c>)
#  depth, node.lineno, node.col_offset, node: (0, 45, 0, <_ast.FunctionDef object at 0x912f8cc>)
#  depth, node.lineno, node.col_offset, node: (2, 45, 12, <_ast.Name object at 0x912f92c>)
#  depth, node.lineno, node.col_offset, node: (2, 45, 21, <_ast.Name object at 0x912f94c>)
#  depth, node.lineno, node.col_offset, node: (1, 46, 4, <_ast.If object at 0x912f9ac>)
#  depth, node.lineno, node.col_offset, node: (2, 46, 7, <_ast.Compare object at 0x912f9cc>)
#  depth, node.lineno, node.col_offset, node: (3, 46, 7, <_ast.Name object at 0x912f9ec>)
#  depth, node.lineno, node.col_offset, node: (3, 46, 24, <_ast.Num object at 0x912fa4c>)
#  depth, node.lineno, node.col_offset, node: (2, 47, 8, <_ast.Return object at 0x912fa8c>)
#  depth, node.lineno, node.col_offset, node: (3, 47, 15, <_ast.Num object at 0x912faac>)
#  depth, node.lineno, node.col_offset, node: (1, 48, 4, <_ast.Return object at 0x912faec>)
#  depth, node.lineno, node.col_offset, node: (2, 48, 42, <_ast.BinOp object at 0x912fb0c>)
#  depth, node.lineno, node.col_offset, node: (3, 48, 11, <_ast.BinOp object at 0x912fb2c>)
#  depth, node.lineno, node.col_offset, node: (4, 48, 11, <_ast.Name object at 0x912fb4c>)
#  depth, node.lineno, node.col_offset, node: (4, 48, 21, <_ast.Call object at 0x912fb6c>)
#  depth, node.lineno, node.col_offset, node: (5, 48, 21, <_ast.Name object at 0x912fb8c>)
#  depth, node.lineno, node.col_offset, node: (5, 48, 27, <_ast.Name object at 0x912fbcc>)
#  depth, node.lineno, node.col_offset, node: (3, 48, 44, <_ast.Num object at 0x912fc0c>)
#  depth, node.lineno, node.col_offset, node: (0, 50, 0, <_ast.If object at 0x912fc4c>)
#  depth, node.lineno, node.col_offset, node: (1, 50, 3, <_ast.Compare object at 0x912fc6c>)
#  depth, node.lineno, node.col_offset, node: (2, 50, 3, <_ast.Name object at 0x912fc8c>)
#  depth, node.lineno, node.col_offset, node: (2, 50, 15, <_ast.Str object at 0x912fcec>)
#  depth, node.lineno, node.col_offset, node: (1, 51, 4, <_ast.Assign object at 0x912fd2c>)
#  depth, node.lineno, node.col_offset, node: (2, 51, 4, <_ast.Name object at 0x912fd6c>)
#  depth, node.lineno, node.col_offset, node: (2, 51, 9, <_ast.Call object at 0x912fd8c>)
#  depth, node.lineno, node.col_offset, node: (3, 51, 9, <_ast.Name object at 0x912fdac>)
#  depth, node.lineno, node.col_offset, node: (1, 52, 4, <_ast.For object at 0x912fe0c>)
#  depth, node.lineno, node.col_offset, node: (2, 52, 8, <_ast.Name object at 0x912fe2c>)
#  depth, node.lineno, node.col_offset, node: (2, 52, 14, <_ast.Subscript object at 0x912fe4c>)
#  depth, node.lineno, node.col_offset, node: (3, 52, 14, <_ast.Attribute object at 0x912fe6c>)
#  depth, node.lineno, node.col_offset, node: (4, 52, 14, <_ast.Name object at 0x912fe8c>)
#  depth, node.lineno, node.col_offset, node: (4, 52, 23, <_ast.Num object at 0x912fecc>)
#  depth, node.lineno, node.col_offset, node: (2, 53, 8, <_ast.Expr object at 0x912ff0c>)
#  depth, node.lineno, node.col_offset, node: (3, 53, 8, <_ast.Call object at 0x912ff2c>)
#  depth, node.lineno, node.col_offset, node: (4, 53, 8, <_ast.Attribute object at 0x912ff4c>)
#  depth, node.lineno, node.col_offset, node: (5, 53, 8, <_ast.Name object at 0x912ff6c>)
#  depth, node.lineno, node.col_offset, node: (4, 53, 17, <_ast.Name object at 0x912ffac>)
#  depth, node.lineno, node.col_offset, node: (1, 54, 4, <_ast.Expr object at 0x912502c>)
#  depth, node.lineno, node.col_offset, node: (2, 54, 4, <_ast.Call object at 0x912504c>)
#  depth, node.lineno, node.col_offset, node: (3, 54, 4, <_ast.Attribute object at 0x912506c>)
#  depth, node.lineno, node.col_offset, node: (4, 54, 4, <_ast.Name object at 0x912508c>)
#  depth, node.lineno, node.col_offset, node: (1, 56, 4, <_ast.Print object at 0x91250ec>)
#  depth, node.lineno, node.col_offset, node: (2, 56, 10, <_ast.BinOp object at 0x912512c>)
#  depth, node.lineno, node.col_offset, node: (3, 56, 10, <_ast.Str object at 0x912514c>)
#  depth, node.lineno, node.col_offset, node: (3, 57, 8, <_ast.Tuple object at 0x912516c>)
#  depth, node.lineno, node.col_offset, node: (4, 57, 8, <_ast.Attribute object at 0x91251ac>)
#  depth, node.lineno, node.col_offset, node: (5, 57, 8, <_ast.Name object at 0x91251cc>)
#  depth, node.lineno, node.col_offset, node: (4, 58, 8, <_ast.Attribute object at 0x91251ec>)
#  depth, node.lineno, node.col_offset, node: (5, 58, 8, <_ast.Name object at 0x912520c>)
#  depth, node.lineno, node.col_offset, node: (4, 59, 8, <_ast.Call object at 0x912522c>)
#  depth, node.lineno, node.col_offset, node: (5, 59, 8, <_ast.Name object at 0x912524c>)
#  depth, node.lineno, node.col_offset, node: (5, 59, 16, <_ast.Attribute object at 0x912528c>)
#  depth, node.lineno, node.col_offset, node: (6, 59, 16, <_ast.Name object at 0x91252ac>)
#  depth, node.lineno, node.col_offset, node: (5, 59, 36, <_ast.Attribute object at 0x91252cc>)
#  depth, node.lineno, node.col_offset, node: (6, 59, 36, <_ast.Name object at 0x91252ec>)
#  depth, node.lineno, node.col_offset, node: (1, 61, 4, <_ast.Print object at 0x912532c>)
#  depth, node.lineno, node.col_offset, node: (2, 61, 10, <_ast.BinOp object at 0x912536c>)
#  depth, node.lineno, node.col_offset, node: (3, 61, 10, <_ast.Str object at 0x912538c>)
#  depth, node.lineno, node.col_offset, node: (3, 62, 8, <_ast.Tuple object at 0x91253ac>)
#  depth, node.lineno, node.col_offset, node: (4, 62, 8, <_ast.Attribute object at 0x91253ec>)
#  depth, node.lineno, node.col_offset, node: (5, 62, 8, <_ast.Name object at 0x912540c>)
#  depth, node.lineno, node.col_offset, node: (4, 63, 8, <_ast.Attribute object at 0x912542c>)
#  depth, node.lineno, node.col_offset, node: (5, 63, 8, <_ast.Name object at 0x912544c>)
#  depth, node.lineno, node.col_offset, node: (4, 64, 8, <_ast.Call object at 0x912546c>)
#  depth, node.lineno, node.col_offset, node: (5, 64, 8, <_ast.Name object at 0x912548c>)
#  depth, node.lineno, node.col_offset, node: (5, 64, 16, <_ast.Attribute object at 0x91254cc>)
#  depth, node.lineno, node.col_offset, node: (6, 64, 16, <_ast.Name object at 0x91254ec>)
#  depth, node.lineno, node.col_offset, node: (5, 64, 40, <_ast.Attribute object at 0x912550c>)
#  depth, node.lineno, node.col_offset, node: (6, 64, 40, <_ast.Name object at 0x912552c>)
#  depth, node.lineno, node.col_offset, node: (1, 66, 4, <_ast.Print object at 0x912556c>)
#  depth, node.lineno, node.col_offset, node: (2, 66, 10, <_ast.Str object at 0x91255ac>)
#  depth, node.lineno, node.col_offset, node: (1, 67, 4, <_ast.For object at 0x91255cc>)
#  depth, node.lineno, node.col_offset, node: (2, 67, 8, <_ast.Name object at 0x91255ec>)
#  depth, node.lineno, node.col_offset, node: (2, 67, 18, <_ast.Attribute object at 0x912560c>)
#  depth, node.lineno, node.col_offset, node: (3, 67, 18, <_ast.Name object at 0x912562c>)
#  depth, node.lineno, node.col_offset, node: (2, 68, 8, <_ast.Print object at 0x912566c>)
#  depth, node.lineno, node.col_offset, node: (3, 68, 14, <_ast.BinOp object at 0x91256ac>)
#  depth, node.lineno, node.col_offset, node: (4, 68, 14, <_ast.Str object at 0x91256cc>)
#  depth, node.lineno, node.col_offset, node: (4, 68, 30, <_ast.Tuple object at 0x91256ec>)
#  depth, node.lineno, node.col_offset, node: (5, 68, 30, <_ast.Attribute object at 0x912572c>)
#  depth, node.lineno, node.col_offset, node: (6, 68, 30, <_ast.Name object at 0x912574c>)
#  depth, node.lineno, node.col_offset, node: (5, 68, 41, <_ast.Attribute object at 0x912576c>)
#  depth, node.lineno, node.col_offset, node: (6, 68, 41, <_ast.Name object at 0x912578c>)
#  depth, node.lineno, node.col_offset, node: (5, 68, 60, <_ast.Attribute object at 0x91257ac>)
#  depth, node.lineno, node.col_offset, node: (6, 68, 60, <_ast.Name object at 0x91257cc>)
# 
## cell 10 end ##
## cell 11 input ##
"""
Module(
 body=[
  Import(names=[alias(name='sys', asname=None)]), 
  Import(names=[alias(name='traceback', asname=None)]), 
  ClassDef(
   name='coverage_analyzer', 
   bases=[Name(id='object', ctx=Load())], 
   body=[
    FunctionDef(name='__init__', args=arguments(args=[Name(id='self', ctx=Param())], vararg=None, kwarg=None, defaults=[]), 
                body=[
                 Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), attr='n_total_files', ctx=Store())], value=Num(n=0)), 
                 Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), attr='n_covered_files', ctx=Store())], value=Num(n=0)), 
                 Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), attr='covered_files', ctx=Store())], value=List(elts=[], ctx=Load())), 
                 Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), attr='n_total_functions', ctx=Store())], value=Num(n=0)), 
                 Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), attr='n_covered_functions', ctx=Store())], value=Num(n=0)), 
                 Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), attr='uncovered_functions', ctx=Store())], value=List(elts=[], ctx=Load())), 
                 Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), attr='coverage_data', ctx=Store())], value=Dict(keys=[], values=[]))], 
                decorator_list=[]
    ), 
    FunctionDef(name='cover', args=arguments(args=[Name(id='self', ctx=Param()), Name(id='cover_fn', ctx=Param())], vararg=None, kwarg=None, defaults=[]), 
                body=[
                 Assign(targets=[Name(id='loc', ctx=Store())], value=Dict(keys=[], values=[])), 
                 TryExcept(body=[Expr(value=Call(func=Name(id='execfile', ctx=Load()), args=[Name(id='cover_fn', ctx=Load()), Name(id='loc', ctx=Load()), Name(id='loc', ctx=Load())], 
                 keywords=[], starargs=None, kwargs=None))], handlers=[ExceptHandler(type=None, name=None, body=[Print(dest=None, values=[BinOp(left=Str(s='error reading coverage file %r:\n%s'), 
                 op=Mod(), right=Tuple(elts=[Name(id='cover_fn', ctx=Load()), Call(func=Attribute(value=Name(id='traceback', ctx=Load()), attr='format_exc', ctx=Load()), args=[], keywords=[], 
                 starargs=None, kwargs=None)], ctx=Load()))], nl=True), Raise(type=None, inst=None, tback=None)])], orelse=[]), Assign(targets=[Subscript(value=Attribute(value=Name(id='self', 
                 ctx=Load()), attr='coverage_data', ctx=Load()), slice=Index(value=Name(id='cover_fn', ctx=Load())), ctx=Store())], value=Subscript(value=Name(id='loc', ctx=Load()), 
                 slice=Index(value=Str(s='coverage')), ctx=Load()))], decorator_list=[]), 
    FunctionDef(name='analyze', args=arguments(args=[Name(id='self', ctx=Param())], vararg=None, 
                 kwarg=None, defaults=[]), body=[Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), attr='covered_files', ctx=Store())], value=Call(func=Name(id='set', ctx=Load()), 
                 args=[], keywords=[], starargs=None, kwargs=None)), For(target=Name(id='fncov', ctx=Store()), iter=Call(func=Attribute(value=Attribute(value=Name(id='self', ctx=Load()), 
                 attr='coverage_data', ctx=Load()), attr='values', ctx=Load()), args=[], keywords=[], starargs=None, kwargs=None), body=[Expr(value=Call(func=Attribute(value=Attribute(value=Name(
                 id='self', ctx=Load()), attr='covered_files', ctx=Load()), attr='update', ctx=Load()), args=[Call(func=Attribute(value=Name(id='fncov', ctx=Load()), attr='keys', ctx=Load()), 
                 args=[], keywords=[], starargs=None, kwargs=None)], keywords=[], starargs=None, kwargs=None))], orelse=[]), Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), 
                 attr='covered_files', ctx=Store())], value=Call(func=Name(id='list', ctx=Load()), args=[Attribute(value=Name(id='self', ctx=Load()), attr='covered_files', ctx=Load())], 
                 keywords=[], starargs=None, kwargs=None)), Expr(value=Call(func=Attribute(value=Attribute(value=Name(id='self', ctx=Load()), attr='covered_files', ctx=Load()), attr='sort',
                 ctx=Load()), args=[], keywords=[], starargs=None, kwargs=None)), Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), attr='n_covered_files', ctx=Store())], 
                 value=Call(func=Name(id='len', ctx=Load()), args=[Attribute(value=Name(id='self', ctx=Load()), attr='covered_files', ctx=Load())], keywords=[], starargs=None, kwargs=None)), 
                 Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), attr='total_files', ctx=Store())], value=Call(func=Name(id='list', ctx=Load()), args=[Attribute(value=Name(id='self', 
                 ctx=Load()), attr='covered_files', ctx=Load())], keywords=[], starargs=None, kwargs=None)), Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), attr='n_total_files', 
                 ctx=Store())], value=Call(func=Name(id='len', ctx=Load()), args=[Attribute(value=Name(id='self', ctx=Load()), attr='total_files', ctx=Load())], keywords=[], starargs=None, 
                 kwargs=None)), Assign(targets=[Attribute(value=Name(id='self', ctx=Load()), attr='total_functions', ctx=Store())], value=List(elts=[], ctx=Load())), For(target=Name(id='fn', 
                 ctx=Store()), iter=Attribute(value=Name(id='self', ctx=Load()), attr='total_files', ctx=Load()), body=[Expr(value=Call(func=Attribute(value=Name(id='self', ctx=Load()), 
                 attr='add_functions_from_file', ctx=Load()), args=[Name(id='fn', ctx=Load()), Attribute(value=Name(id='self', ctx=Load()), attr='total_functions', ctx=Load())], keywords=[], 
                 starargs=None, kwargs=None))], orelse=[])], decorator_list=[]), 
    FunctionDef(name='add_functions_from_file', args=arguments(args=[Name(id='self', ctx=Param()), Name(id='fn', 
                 ctx=Param()), Name(id='functions', ctx=Param())], vararg=None, kwarg=None, defaults=[]), body=[Pass()], decorator_list=[])
   ], 
   decorator_list=[]
  ), 
  
  FunctionDef(name='percent', 
                 args=arguments(args=[Name(id='n_items', ctx=Param()), Name(id='n_total_items', ctx=Param())], vararg=None, kwarg=None, defaults=[]), body=[If(test=Compare(left=Name(
                 id='n_total_items', ctx=Load()), ops=[Eq()], comparators=[Num(n=0)]), body=[Return(value=Num(n=0))], orelse=[]), Return(value=BinOp(left=BinOp(left=Name(id='n_items', ctx=Load()), 
                 op=Div(), right=Call(func=Name(id='float', ctx=Load()), args=[Name(id='n_total_items', ctx=Load())], keywords=[], starargs=None, kwargs=None)), op=Mult(), right=Num(n=100)))], 
                 decorator_list=[]),
                 
   If(test=Compare(left=Name(id='__name__', ctx=Load()), ops=[Eq()], comparators=[Str(s='__main__')]), 
     body=[
        Assign(targets=[Name(id='ca', ctx=Store())], value=Call(func=Name(id='coverage_analyzer', ctx=Load()), args=[], keywords=[], starargs=None, kwargs=None)), 
        For(target=Name(id='fn', ctx=Store()), iter=Subscript(value=Attribute(value=Name(id='sys', ctx=Load()), attr='argv', ctx=Load()), slice=Slice(lower=Num(n=1), upper=None, step=None), ctx=Load()), 
         body=[
            Expr(value=Call(func=Attribute(value=Name(id='ca', ctx=Load()), attr='cover', ctx=Load()), args=[Name(id='fn', ctx=Load())], keywords=[], starargs=None, kwargs=None))
         ],
        orelse=[]), 
        Expr(value=Call(func=Attribute(value=Name(id='ca', ctx=Load()), attr='analyze', ctx=Load()), args=[], keywords=[], starargs=None, kwargs=None)), 
        Print(dest=None, values=[BinOp(left=Str(s='total files seen: %d, covered: %d (%.1f%%)'), op=Mod(), right=Tuple(elts=[Attribute(value=Name(id='ca', ctx=Load()), attr='n_total_files', ctx=Load()), 
         Attribute(value=Name(id='ca', ctx=Load()), attr='n_covered_files', ctx=Load()), Call(func=Name(id='percent', ctx=Load()), args=[Attribute(value=Name(id='ca', ctx=Load()), 
         attr='n_covered_files', ctx=Load()), Attribute(value=Name(id='ca', ctx=Load()), attr='n_total_files', ctx=Load())], keywords=[], starargs=None, kwargs=None)], ctx=Load()))], 
         nl=True), 
        Print(dest=None, values=[BinOp(left=Str(s='total functions seen: %d, covered: %d (%.1f%%)'), op=Mod(), right=Tuple(elts=[Attribute(value=Name(id='ca', ctx=Load()), 
         attr='n_total_functions', ctx=Load()), Attribute(value=Name(id='ca', ctx=Load()), attr='n_covered_functions', ctx=Load()), Call(func=Name(id='percent', ctx=Load()), 
         args=[Attribute(value=Name(id='ca', ctx=Load()), attr='n_covered_functions', ctx=Load()), Attribute(value=Name(id='ca', ctx=Load()), attr='n_total_functions', ctx=Load())], 
         keywords=[], starargs=None, kwargs=None)], ctx=Load()))], nl=True), 
        Print(dest=None, values=[Str(s='uncovered functions:')], nl=True), 
        For(target=Name(id='method', ctx=Store()), iter=Attribute(value=Name(id='ca', ctx=Load()), attr='uncovered_functions', ctx=Load()), body=[Print(dest=None, values=[BinOp(left=Str(s='  %s:%d %s'), op=Mod(), 
     right=Tuple(elts=[Attribute(value=Name(id='method', ctx=Load()), attr='fn', ctx=Load()), Attribute(value=Name(id='method', ctx=Load()), attr='start_line', ctx=Load()), 
     Attribute(value=Name(id='method', ctx=Load()), attr='name', ctx=Load())], ctx=Load()))], nl=True)], orelse=[])
     ], orelse=[]
   ) 
   ])
"""
## cell 11 end ##
## cell 12 input ##

## cell 12 end ##
