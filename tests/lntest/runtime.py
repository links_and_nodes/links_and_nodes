import sys
import datetime

from . import mainloop

class runtime_class(object):
    def __init__(self):
        #print "runtime constructed"
        self.is_single_test = True
        self.log_levels = "error", "info", "debug"
        self.log_level = "error"
        self.running_test = None
        self.mainloop = mainloop.mainloop(self)

    def is_single(self):
        return self.is_single_test

    def error(self, msg, throw=True):
        self.log("error", msg)
        if throw:
            raise Exception(msg)

    def info(self, msg):
        self.log("info", msg)

    def debug(self, msg):
        self.log("debug", msg)

    def log(self, level, msg):
        if self.log_levels.index(self.log_level) >= self.log_levels.index(level):
            sys.stdout.write("%s\n" % msg)
            sys.stdout.flush()
        if self.running_test is not None:
            self.running_test._log.append((datetime.datetime.now(), level, msg))
                
runtime = None

def get_runtime():
    global runtime
    if runtime is None:
        runtime = runtime_class()
    return runtime

def get_mainloop():
    return get_runtime().mainloop
