import os
import time

class output_buffer(object):
    def __init__(self, test, name):
        self.test = test
        self.name = name
        self.data = []
        self.read_pos = [0, 0]
        self.main = self.test.mainloop
        self.new_data_count = 0
        self.eof = False

    def write(self, data):
        if not data:
            self.eof = True
            return False
        debug_data = data
        if debug_data.endswith("\n"):
            debug_data = debug_data[:-1]
        for line in debug_data.split("\n"):
            self.test.debug("%s: %r" % (self.name, line + "\n"))
        self.data.append(data)
        self.new_data_count += len(data)
        return True

    def dump_below(self, dn):
        with open(os.path.join(dn, "%s_output" % self.name), "w") as fp:
            for data in self.data:
                fp.write(data)

    def read(self, n=1, timeout=None, deadline=None):
        #self.test.debug("read %d bytes from %r. rp: %d %d" % (n, self.name, self.read_pos[0], self.read_pos[1]))
        if deadline is None:
            if timeout is None:
                timeout = self.test.get_parameter("wait_for_output_timeout", 5)
            start = time.time()
            deadline = start + timeout
        while True:
            # is there atleast n bytes of data to read?
            to_read_avaliable = 0
            rp = list(self.read_pos)
            while rp[0] < len(self.data):
                in_this_data = len(self.data[rp[0]]) - rp[1]
                rp[0] += 1
                rp[1] = 0
                to_read_avaliable += in_this_data
                if to_read_avaliable >= n:
                    break
            if to_read_avaliable >= n:
                break
            
            # not enough data in buffer! wait for more!
            self.new_data_count = to_read_avaliable
            while self.new_data_count < n:
                if self.eof:
                    raise Exception("eof!")
                #self.test.debug("wait for new data to arrive for this output buffer!")
                self.main.iterate(deadline)
                #self.test.debug("after main event: %d" % self.new_data_count)
            
            break # have neough data!
        data = []
        rp = self.read_pos
        while n > 0:
            while rp[1] >= len(self.data[rp[0]]):
                rp[0] += 1
                rp[1] = 0
            new_data = self.data[rp[0]][rp[1]:rp[1]+n]
            data.append(new_data)
            n -= len(new_data)
            rp[1] += len(new_data)
        data = "".join(data)
        #self.test.debug("read returns data: %r, new read pos: %d, %d" % (data, self.read_pos[0], self.read_pos[1]))
        return data
        
