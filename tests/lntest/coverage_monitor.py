#!/usr/bin/python

import os 
import sys
import time
import pprint
import atexit

class coverage_monitor(object):
    def __init__(self):
        self.skip_dirs = [
            #"/usr/lib"
        ]
        sys._ln_coverage_trace_active = False

    def run(self):
        del sys.argv[0]
        if sys.argv[0] == "-coverage-fn":
            del sys.argv[0]
            coverage_fn = sys.argv[0]
            del sys.argv[0]
        else:
            coverage_fn = "coverage.py"
        sys.stdout.write("starting %s\n" % (" ".join(map(repr, sys.argv)), ))
        glob = {}
        loc = {
            "__name__": "__main__",
            "__file__": sys.argv[0]
        }
        dn = os.path.dirname(os.path.realpath(sys.argv[0]))
        if dn not in sys.path:
            sys.path.insert(0, dn)
        #self.base_dir = dn

        self.start_recording(coverage_fn)
        exec(compile(open(sys.argv[0]).read(), sys.argv[0], 'exec'), loc, loc)
        self.stop_recording()

    def start_recording(self, coverage_fn):
        self.coverage_fn = coverage_fn
        self.current_method = None
        self.start_time = time.time()
        self.coverage = {} # filename -> dict of
        #                                line_no -> [hit_count, first_seen]
        atexit.register(self.on_exit)
        import signal
        def on_sigterm(signo, frame):
            print("got sigterm!\n")
            sys.exit(1)
        signal.signal(signal.SIGTERM, on_sigterm)
        sys._ln_coverage_trace_active = True
        sys.settrace(self.trace)

    def stop_recording(self):
        sys.settrace(None)
        if sys._ln_coverage_trace_active:
            sys._ln_coverage_trace_active = False
            self.end_time = time.time()

    def on_exit(self):
        self.stop_recording()
        sys.stdout.write("duration: %.3fs\n" % (self.end_time - self.start_time))
        #pprint.pprint(self.coverage)
        self.dump()

    def dump(self):
        cov = {}
        for fn, fncov in list(self.coverage.items()):
            cov[os.path.realpath(fn)] = fncov
        print("dump to %r" % self.coverage_fn)
        with open(self.coverage_fn, "w") as fp:
            fp.write("coverage = {\n ")
            fp.write(pprint.pformat(cov, width=180)[1:])

    def trace(self, frame, ev, arg):
        c = frame.f_code
        #print("%s: %s" % (ev, arg))
        #if ev == "return":
        #    print "%s:%d return from function %s" % (c.co_filename, frame.f_lineno, c.co_name)
        if ev == "line":
            #print("%s:%d in function %s" % (c.co_filename, frame.f_lineno, c.co_name))
            cov = self.coverage.get(c.co_filename)
            if cov is None:
                #rfn = os.path.realpath(c.co_filename)
                #if not rfn.startswith(self.base_dir):
                #    return None
                cov = self.coverage[c.co_filename] = {}
            lcov = cov.get(frame.f_lineno)
            if lcov is None:
                cov[frame.f_lineno] = [1, time.time() - self.start_time]
            else:
                lcov[0] += 1
        if ev == "call":
            rfn = os.path.realpath(c.co_filename)
            for skip in self.skip_dirs:
                if rfn.startswith(skip):
                    sys.stdout.write("skip %s\n" % rfn)
                    return None
        #    self.current_method = c.co_name
        #    print "%s:%d call new local %s" % (c.co_filename, frame.f_lineno, c.co_name)
        return self.trace
    
if __name__ == "__main__":
    mon = coverage_monitor()
    mon.run()
