import sys
import ast
import os
import traceback

class method(object):
    def __init__(self, filename, name, start_line, start_col, prefixes):
        self.filename = filename
        self.name = name
        self.start_line = start_line
        self.start_col = start_col
        self.end_line, self.end_col = self.start_line, self.start_col
        self.prefixes = prefixes

        self.code_segments = [
            [self.start_line, self.start_line]
        ]

        self.uncovered_lines = set()
        self.covered = set()
        
        self.coverage_features = []

    def add_code_line(self, line, col, segments=None):
        #print("%s add code line %r %r" % (self.get_fqn(), line, col))
        #if (self.get_fqn(), line, col) == ("module.func1", 2, 4):
        #    import pdb; pdb.set_trace()
        if segments is None:
            self.uncovered_lines.add(line)
            segments = self.code_segments
        # search code segment before or at line
        for idx, seg in enumerate(segments):
            if seg[0] <= line:
                if seg[1] + 1 == line:
                    seg[1] += 1
                    return
                if line <= seg[1]:
                    return
            if seg[0] > line:
                # insert before idx
                segments.insert(idx, [line, line])
                return
        segments.append([line, line])

    def iter_lines(self):
        for seg in self.code_segments:
            for line in range(seg[0], seg[1]+1):
                yield line

    def add_covered_line(self, line):
        self.uncovered_lines.discard(line)
        self.covered.add(line)

    def __str__(self):
        return "<method %s>" % self.get_fqn()

    def get_fqn(self):
        if self.prefixes:
            return "%s.%s" % (".".join(self.prefixes), self.name)
        return self.name

    def set_end(self, end):
        self.end_line, self.end_col = end

    def get_uncovered_line_segments(self):
        uncovered_segments = []
        for line in self.uncovered_lines:
            self.add_code_line(line, 0, uncovered_segments)
        if uncovered_segments == self.code_segments:
            return [[self.start_line, self.end_line]]
        return uncovered_segments

    def mark_coverage_only_needed_for(self, from_line, features):
        self.coverage_features.append([from_line, None, features])
        return len(self.coverage_features) - 1
        
    def end_coverage_features(self, end, ids):
        for i in ids:
            self.coverage_features[i][1] = end[0]


class coverage_analyzer(object):
    def __init__(self):
        self.n_total_files = 0
        self.n_covered_files = 0
        self.covered_files = []
        
        self.n_total_functions = 0
        self.n_covered_functions = 0
        self.uncovered_functions = []

        self.coverage_data = {}
        self.skip_dirs = set()
        self.files_failed_parsing = set()
        self.provided_features = []

    def _read_parameters(self, pfn):
        if not os.path.isfile(pfn):
            return
        parameters = dict(
            __file__=pfn,
        )
        exec(compile(open(pfn).read(), pfn, 'exec'), parameters)
        for skip in parameters.get("coverage_skip_list", []):
            self.skip_dirs.add(skip)
        self.provided_features = list(set(self.provided_features).union(parameters.get("provides", [])))

    def cover(self, cover_fn):
        loc = {}
        test_dir = os.path.dirname(os.path.realpath(cover_fn))
        self._read_parameters(os.path.join(test_dir, "parameters.py"))
        self._read_parameters(os.path.join(test_dir, "..", "parameters.py"))
        try:
            exec(compile(open(cover_fn).read(), cover_fn, 'exec'), loc, loc)
        except:
            sys.stdout.write("error reading coverage file %r:\n%s\n" % (cover_fn, traceback.format_exc()))
            raise
        cov = {}
        self.coverage_data[cover_fn] = cov
        for fn, data in list(loc["coverage"].items()):
            for skip in self.skip_dirs:
                if fn.startswith(skip):
                    break
            else:
                cov[fn] = data
        
    def analyze(self):
        self.covered_files = set()
        for fncov in list(self.coverage_data.values()):
            self.covered_files.update(list(fncov.keys()))
        self.covered_files = list(self.covered_files)
        self.covered_files.sort()
        self.n_covered_files = len(self.covered_files)
        
        # find total files (files sitting nearby covered files...)
        self.total_files = list(self.covered_files)
        total = []
        # remove dirs to skip from total files!
        for fn in self.total_files:
            for skip in self.skip_dirs:
                if fn.startswith(skip):
                    break
            else:
                total.append(fn)
        self.total_files = total
        self.n_total_files = len(self.total_files)

        # go throu all files and remember line numbers of all methods
        self.total_functions = []
        for fn in self.total_files:
            self.add_functions_from_file(fn, self.total_functions)
        self.n_total_functions = len(self.total_functions)

        known_lines = {}
        for method in self.total_functions:
            for line in method.iter_lines():
                key = method.filename, line
                item = known_lines.get(key)
                if item is None:
                    known_lines[key] = [method]
                else:
                    item.append(method)
        # apply coverage
        for fncov in list(self.coverage_data.values()):
            for fn, cov in list(fncov.items()):
                if fn in self.files_failed_parsing:
                    continue
                for line, (hit_count, first_seen) in list(cov.items()):
                    key = fn, line
                    methods = known_lines.get(key)
                    if methods:
                        for method in methods:
                            method.add_covered_line(line)
                    elif fn.endswith(">"):
                        pass
                    else:
                        sys.stdout.write("warning: covered line %s:%d is not known to be a code line!\n" % (
                            fn, line))
        # apply simulated coverage for not declared features!
        features_to_test = set(self.provided_features)
        for method in self.total_functions:
            for block_start, block_end, feats in method.coverage_features:
                if not features_to_test.intersection(feats):
                    # apply coverage for this block... not tested...
                    for line in range(block_start, block_end + 1):
                        method.add_covered_line(line)
            
        self.uncovered_functions = []
        self.covered_functions = []
        for method in self.total_functions:
            if method.uncovered_lines:
                self.uncovered_functions.append(method)
            else:
                self.covered_functions.append(method)
        self.n_uncovered_functions = len(self.uncovered_functions)
        self.n_covered_functions = self.n_total_functions - self.n_uncovered_functions

    def add_functions_from_file(self, fn, functions):
        if fn.endswith(">"):
            return
        data = open(fn, "r").read()
        try:
            root = ast.parse(data, fn)
        except Exception:
            #print "error parsing contents of file %r:\n%s\n%r" % (
            #    fn, data, data)
            #raise
            sys.stdout.write("error parsing %s\n" % (fn))
            self.files_failed_parsing.add(fn)
            return
        def recurse(parent, depth=0, name_prefix=[], last_line=None, parent_function=None, first_in_block=True):
            #print(("  " * depth), "recurse with", parent, getattr(parent, "lineno", None), "from", parent_function)
            this_name_prefix = name_prefix
            # todo: handle imports as hints to other sources!: if isinstance(parent, ast.Import)


            active_coverage_features = []

            if isinstance(parent, ast.Expr) and isinstance(parent.value, ast.Call) and isinstance(parent.value.func, ast.Name) and parent.value.func.id == "only_need_coverage_for":
                only_need_coverage_for = [arg.s for arg in parent.value.args]
                start_line = parent.lineno
                if first_in_block:
                    start_line = last_line[0]
                active_coverage_features.append(
                    parent_function.mark_coverage_only_needed_for(start_line, only_need_coverage_for))
                parent_function.add_covered_line(parent.lineno)

            if isinstance(parent, (ast.expr, ast.stmt, ast.ExceptHandler)):
                last_line = parent.lineno, parent.col_offset            
                
            if isinstance(parent, ast.ClassDef):
                this_function = method(fn, parent.name, parent.lineno, parent.col_offset, this_name_prefix)
                this_name_prefix = list(this_name_prefix)
                functions.append(this_function)
                this_name_prefix.append(parent.name)
            elif isinstance(parent, ast.FunctionDef):
                this_function = method(fn, parent.name, parent.lineno, parent.col_offset, this_name_prefix)
                if parent.name != "only_need_coverage_for":
                    functions.append(this_function)
                this_name_prefix = list(this_name_prefix)
                #this_name_prefix.append("<func %s>" % parent.name)
                this_name_prefix.append(parent.name)
            elif isinstance(parent, ast.Module):
                this_function = method(fn, "module", 1, 0, this_name_prefix)
                functions.append(this_function)
                this_name_prefix = list(this_name_prefix)
                #this_name_prefix.append("<mod %s>" % parent.name)
                this_name_prefix.append("module")
            else:
                this_function = None
            
            if this_function:
                parent_function = this_function

            all_inner_active_coverage_features = []
            for i, node in enumerate(ast.iter_child_nodes(parent)):
                if parent_function is not None and isinstance(node, (ast.expr, ast.stmt, ast.excepthandler)):
                    # ignore Expr whose value is a Constant -- probably just used as doc-string
                    is_only_constant = isinstance(node, ast.Expr) and isinstance(node.value, ast.Constant)
                    if is_only_constant:
                        continue
                    parent_function.add_code_line(node.lineno, node.col_offset)
                last_line, inner_active_coverage_features = recurse(node, depth+1, this_name_prefix, last_line=last_line, parent_function=parent_function, first_in_block=(i == 0))
                all_inner_active_coverage_features.extend(inner_active_coverage_features)
            if parent_function is not None:                
                parent_function.end_coverage_features(last_line, all_inner_active_coverage_features)
            if this_function is not None:
                if last_line: # hmmm
                    this_function.set_end(last_line)
            return last_line, active_coverage_features
        recurse(root)
        
def percent(n_items, n_total_items):
    if n_total_items == 0:
        return 0
    return n_items / float(n_total_items) * 100
    
if __name__ == "__main__":
    ca = coverage_analyzer()
    for fn in sys.argv[1:]:
        ca.cover(fn)
    ca.analyze()
    
    sys.stdout.write("total files seen: %d, touched: %d (%.1f%%)\n" % (
        ca.n_total_files,
        ca.n_covered_files,
        percent(ca.n_covered_files, ca.n_total_files)
    ))
    sys.stdout.write("total functions seen: %d, covered: %d (%.1f%%)\n" % (
        ca.n_total_functions,
        ca.n_covered_functions,
        percent(ca.n_covered_functions, ca.n_total_functions)
    ))

    sys.stdout.write("tested features: %r\n" % (ca.provided_features, ))

    #print "total functions:"
    #for method in ca.total_functions:
    #    print "  %s %4d %4d %s" % (method.filename, method.start_line, method.end_line, method.get_fqn())


    if ca.uncovered_functions:
        sys.stdout.write("uncovered functions:\n")
        last_fn = None
        for method in ca.uncovered_functions:
            if method.filename != last_fn:
                print("  %s:" % method.filename)
                last_fn = method.filename
            print("    %4d %4d %s" % (method.start_line, method.end_line, method.get_fqn()))
            #for seg in method.code_segments:
            #    print seg,
            segs = method.get_uncovered_line_segments()

            if len(segs) == 1 and tuple(segs[0]) == (method.start_line + 1, method.end_line):
                continue
            print("              ", end="")
            for start, end in segs:
                #print start,
                if start == end:
                    print("%d" % start, end=" ")
                else:
                    print("%d-%d " % (start, end), end=" ")
            print()
            #for f in method.coverage_features:
            #    pprint.pprint(f)
    else:
        sys.stdout.write("NO uncovered functions!\n")

    if False:
        sys.stdout.write("\n")
        sys.stdout.write("covered functions:\n")
        for method in ca.covered_functions:
            sys.stdout.write("  %s %4d %4d %s\n" % (method.filename, method.start_line, method.end_line, method.get_fqn()))
