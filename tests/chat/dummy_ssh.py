#!/usr/bin/env python3

import time
import sys
import random

host = sys.argv[1]

if host == "questions":
    print("connected.")
    questions = [
        ("4+4", "8"),
        ("2*2", "4"),
        ("345624576**2", "119456347535179776_")
    ]
    random.seed(int(time.time() * 10))
    question, answer = random.choice(questions)
    print("question: %s" % question)
    ans = input("your answer:")
    if ans == answer:
        print("correct!")
    else:
        print("wrong!")
        if random.randint(0, 1) == 1:
            print("correct answer: %r" % answer)
        else:
            print("find out yourself!")
    time.sleep(0.5)
    sys.exit(0)

print("connecting %s..." % host)
time.sleep(1)

print("connected.")
time.sleep(1)
while True:
    print("Password: ", end="")
    pw = input()

    print("you gave password: %r" % (pw, ))
    time.sleep(1)

    if pw != "secret":
        time.sleep(0.5)
        print("invalid password!")
        continue
    if host == "A":
        time.sleep(0.25)
    else:
        for i in range(200):
            print("%d %s" % (i, "#" * 90))
            if i % 20 == 0:
                time.sleep(0.1)

    print("welcome!")

    time.sleep(1)
    while True:
        print("server:~# ", end="")
        cmd = input()
        print("you gave command: %r" % cmd)
        if cmd == "exit":
            sys.exit(0)
