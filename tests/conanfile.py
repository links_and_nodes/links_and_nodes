import os
from io import StringIO
from conan import ConanFile, conan_version
from conan.tools.env import Environment
from conan.tools.scm import Version

class test_links_and_nodes_manager(ConanFile):
    test_type = "explicit"

    def requirements(self):
        self.requires(self.tested_reference_str)

    def test(self):
        print("pwd: %r" % os.getcwd())
        tests_folder = os.path.dirname(os.path.dirname(os.getcwd()))

        mybuf = StringIO()
        if Version(conan_version) < "2.0.0":
            self.run("which ln_manager", output=mybuf, env="conanrun")
        else:
            self.run("which ln_manager", stdout=mybuf, env="conanrun")
        ln_manager_pyscript = mybuf.getvalue().strip()
        self.output.info("ln_manager location: %r" % ln_manager_pyscript)
        env = Environment()
        env.define("LNTEST_RESULTS_BASE", os.getcwd())
        env.define("LNTEST_LN_MANAGER_PYSCRIPT", ln_manager_pyscript)
        env.define("LN_MANAGER_CONDITIONS_CHECK_FOR_LONG_WAITERS", "n")
        with env.vars(self).apply():
            test_cmd = "./do_test.sh" if Version(conan_version) < "2.0.0" else f"{os.getcwd()}/do_test.sh"
            self.run(f"{test_cmd} -v", cwd=tests_folder, env="conanrun")
