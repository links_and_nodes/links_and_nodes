#!/usr/bin/python

import sys
sys.path.insert(0, "..")

import lntest # noqa: E402
import testlib # noqa: E402

def write_net(netdef):
    with open("net", "wb") as fp:
        fp.write(netdef)

tests = [
("simply test for shortest route in redundant nets", """
A B C
  B C D
""", "A D", "len:3 A, B, D"),

("simply test for shortest route in redundant nets2", """
A B C
  B C D
      D     G
    C     F
          F   H
        E     H
        E   G
""", "A D", "len:3 A, B, D"),

("test for single shortest route", """
A   C
  B C D
      D     G
    C     F
          F   H
        E     H
        E   G
""", "A E", "len:5 A, C, D, G, E"),

("simple priority", """
A B2 C
  B  C D
""", "A D", "len:3 A, B, D"),
("simple priority2", """
A B C2
  B C  D
""", "A D", "len:3 A, C, D"),

("preferred gateways", """
A H1           U
H B1 C  F
  B  C D F
       D     G
         E   G
               U I
       D         I J
       D   	   J
""", "A D", "len:4 A, H, B, D"),

("deterministic order", """
A H            U
H B1 C  F
  B  C D F
       D     G
         E   G
               U I
       D         I J
       D   	   J
""", "A D", "len:4 A, H, B, D"),

]

test = lntest.new_test()
for i, (name, net, cmd, expect) in enumerate(tests):
    write_net(net)
    if i == 0:
        lnm = testlib.get_lnm(test, extra_args=["--console"])
        lnm.wait_for_line_with_regex("ln_manager console.")
        lnm.write("log enable\n")
    else:
        lnm.write("set sysconf.ignore_hosts_from_old_config True; reload\n")
    cmd = "test_route %s; echo len:$route_len $route_hops" % cmd
    lnm.write(cmd + "\n")
    lnm.wait_for_line_with_regex(expect, timeout=3, hint=name)
    
lnm.write("quit\n")
lnm.wait(3)

test.ok()
