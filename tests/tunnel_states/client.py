#!/usr/bin/python

from __future__ import print_function
import sys
import socket

target = sys.argv[1].split(":", 1)
target = target[0], int(target[1])

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
s.settimeout(5)
s.connect(target)

data = s.recv(1024)
print("received: %r" % data)

if data == "all fine!":
    if "-wait" in sys.argv[2:]:
        input("press enter to exit")
    sys.exit(0)

print ("invalid response!")
sys.exit(1)
