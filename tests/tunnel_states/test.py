#!/usr/bin/python

import sys

sys.path.insert(0, "..")
import lntest # noqa: E402

test = lntest.new_test()

import testlib # noqa: E402

lnm = testlib.get_lnm(test, extra_args=["--console"])

lnm.write("stop -w all\n")

def start(what):
    lnm.write('start -t 5 -r "%s/server"\n' % what)
    lnm.write('start -t 5 -i "%s/client"\n' % what)    
    ret = lnm.wait_for_line_with_regex([
        "retval: 0",
        "retval: 1",
        "terminated unexpectedly",
    ], timeout=10, wait_for_all=False)
    if ret.group(0) != "retval: 0":
        test.fail("client %r failed!" % what)

start("B2A")
start("C2B")
start("A2D")

lnm.write("quit\n")
lnm.wait(10)

testlib.check_lnm_output(test, lnm)

# try again (should rediscover states and reuse them)
lnm = testlib.get_lnm(test, extra_args=["--console"], name="lnm2")
start("B2A")
start("C2B")
start("A2D")

lnm.write("stop -w all; echo all are stopped\n")
ret = lnm.wait_for_line_with_regex("all are stopped", timeout=10)

#for host in "rmc-lx0307,rmc-lx0141,rmc-taygete,localhost".split(","):
#    lnm.write('print self.manager.release_all_daemon_resources("%s")\n' % host)
#    time.sleep(0.5)

lnm.write("quit\n")
lnm.wait(10)

testlib.check_lnm_output(test, lnm)

test.ok()
