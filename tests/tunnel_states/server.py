#!/usr/bin/python

from __future__ import print_function
import sys
import socket

port = int(sys.argv[1])

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(("", port))
s.listen(1)

while True:
    print("waiting for clients...")
    c, addr = s.accept()
    print("got connection from %r" % (addr, ))
    c.settimeout(5)
    c.send("all fine!")
    if "-wait" in sys.argv[2:]:
        empty = c.recv(1024)
    c.close()

