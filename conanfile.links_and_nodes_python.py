import os
import subprocess

from conan import ConanFile, conan_version
from conan.tools.build import cross_building
from conan.tools.env import VirtualBuildEnv
from conan.tools.files import copy
from conan.tools.scm import Version


class links_and_nodes_python_conan(ConanFile):
    package_type = "shared-library"
    python_requires = "ln_conan/[^6.0.3]@common/stable", "cissy_conantools/[~1]@tools/stable"
    python_requires_extend = "ln_conan.Base"

    @property
    def cissy_conantools(self):
        return self.python_requires["cissy_conantools"].module

    name = "links_and_nodes_python"
    homepage = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"
    license = "LGPLv3"
    description = "links_and_nodes is a framework for easy creation and maintanance of distributed computing networks."
    settings = "os", "compiler", "build_type", "arch"
    exports = []
    exports_sources = [
        # our self
        "python*",
        "!python/links_and_nodes_base/*.pyc",
        "!python/links_and_nodes/*.pyc",
        "!python/links_and_nodes/.*",
        "!python/links_and_nodes/src-*",
        "!python/links_and_nodes/linux-*",
        "!python/pyutils/*.pyc",
        "!python/pyutils/*.pyx",
        "!python/pyutils/.*",
        "share*",
        # will need base-build-system
        "site_scons*",
        "SConstruct",
        # all of library
        "libln*",
        "external*",
        # excludes are checked with fnmatch against results of positive patterns!
        "!libln/.*",
        "!libln/build/*",
        "!libln/tests/*",
        "!libln/examples/*",
        "ln_runtime*file_services*fs_sync",
    ]

    def init(self):
        pydist_options = {
            "python_version": ["2", "3"],
            "python_dist_version": self.cissy_conantools.get_python_dist_versions(),
        }
        pydist_default_options = {
            "python_version": "3",
            "python_dist_version": self.cissy_conantools.get_python_dist_latest_version(),
        }

        if Version(conan_version) < "2.0.0":
            self.options = {**self.options, **pydist_options}
            self.default_options = {**self.default_options, **pydist_default_options}
        else:
            # options dicts are converted to Options object when calling the constructor
            # https://github.com/conan-io/conan/blob/fb30f48f22eba0a08556080db795e37b2b7b181b/conans/model/conan_file.py#L100
            # https://github.com/conan-io/conan/blob/fb30f48f22eba0a08556080db795e37b2b7b181b/conans/model/options.py#L332
            self.options.update(pydist_options, pydist_default_options)

    def validate(self):
        self.validate_python_version()

    def package_id(self):
        if Version(conan_version) < "2.0.0":
            self.info.requires["links_and_nodes_base_python"].full_package_mode()
            self.info.requires["liblinks_and_nodes"].full_package_mode()

    def configure(self):
        for pkg in ("boost_python", "links_and_nodes_base_python"):
            self.options[pkg].python_version = self.options.python_version
            self.options[pkg].python_dist_version = self.options.python_dist_version

    def build_requirements(self): # would be nice to get rid of this boilerplate and have it in ln_conan only once...DRY
        super().build_requirements()
        self.tool_requires(
            self.cissy_conantools.get_python_dist_requirement(
                self.options.python_version.value, self.options.python_dist_version.value
            )
        )

    def requirements(self):
        self.requires(
            self.cissy_conantools.get_python_dist_requirement(
                self.options.python_version.value, self.options.python_dist_version.value
            )
        )
        self.requires("liblinks_and_nodes/%s" % self.same_branch_or("[^2.5.0]"))
        self.requires("links_and_nodes_base_python/%s" % self.same_branch_or("[~2]"))
        self.requires("boost_python/1.66.0@3rdparty/stable")

    def source(self):
        self.write_version_header("LIBLN_VERSION", os.path.join("libln", "include", "ln", "version.h"))
        self.write_version_file(os.path.join("libln", "version"))

    def build(self):
        add_flags = ""
        if cross_building(self):
            with VirtualBuildEnv(self).vars().apply():
                add_flags = (
                    '--python-flags="'
                    + subprocess.check_output("pkg-config python3 --cflags --libs --static", shell=True)
                    .decode("utf-8")
                    .strip()
                    + '"'
                )
        self.scons_build(
            "python",
            "--no-manager --no-python-base --use-private-libtomcrypt " + add_flags,
            with_python_lib=True,
            parallel=False,
        )

    def package(self):
        install = os.path.join("build", "python", self.install_sandbox, self.prefix[1:])
        copy(
            self,
            "version",
            os.path.join(self.build_folder, "libln"),
            os.path.join(self.package_folder, self.python_path, "links_and_nodes"),
        )  # todo: this is strange
        copy(self, "*", os.path.join(self.build_folder, install), self.package_folder)
        install_bin = os.path.join("build", "libln", self.install_sandbox, self.prefix[1:])
        copy(self, "*", os.path.join(self.build_folder, install_bin), self.package_folder)

    def package_info(self):
        if Version(conan_version) < "2.0.0":
            self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
            self.env_info.PYTHONPATH.append(os.path.join(self.package_folder, self.python_path))
        self.runenv_info.append_path("PATH", os.path.join(self.package_folder, "bin"))
        self.runenv_info.append_path("PYTHONPATH", os.path.join(self.package_folder, self.python_path))
