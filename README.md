[![Build Status](https://rmc-jenkins.robotic.dlr.de/jenkins/job/common/job/links_and_nodes/job/master/33/badge/icon)](https://rmc-jenkins.robotic.dlr.de/jenkins/job/common/job/links_and_nodes/job/master/33/)

# links_and_nodes
links_and_nodes GPLv3 release.
library is released under the terms of LGPLv3.

all tested with debian buster & stretch

## how to build

if your are at DLR-RMC, please read [README.RMC](README.RMC) for RMC-specific build-instructions.

otherwise first check whether there is a premade build script for your distribution:

    ls scripts/build_*

if so, try running it with the option `-h` first.

it will leave an `ln_env.sh` script in your `INSTALL_PREFIX`-folder which you can source in your shell-init
scripts to "activate" that LN-install. as the help-output shows, the `INSTALL_PREFIX` is given as argument
to the `scripts/build_*` script.

example:
```
 $ sudo scripts/build_ubuntu22.04.sh /usr/local
```
then add to your `~/.bashrc`:
```
source /usr/local/ln_env.sh
```

see the folowing instructions if you can not use any of these build scripts.

### dependencies
to build the library, daemon and python binding:

- common for debian and ubuntu systems:

```shell
 $ sudo apt-get update
 $ sudo apt-get install scons libtool autoconf libprocps-dev coreutils dash file g++ git gzip libacl1 libncurses5-dev libselinux1 libtomcrypt-dev libtommath-dev locales make patch sed tar wget
```

- additions for debian stretch, buster
```shell
 $ sudo apt-get install python-sphinx libboost-python-dev python-numpy iproute2
```
- additions for debian bullseye
```shell
 $ sudo apt-get install python3-sphinx python3-dev libboost-python-dev python3-yaml python3-numpy
```

- additions for ubuntu 18.04
add the "universe" repository to your /etc/apt/sources.list, it might look like this: `deb http://de.archive.ubuntu.com/ubuntu bionic main universe`
```shell
 $ sudo apt-get update
 $ sudo apt-get install sphinx-doc sphinx-common python-dev libboost-python-dev
```
- additions for ubuntu 20.04
add the "universe" repository to your /etc/apt/sources.list, it might look like this: `deb http://de.archive.ubuntu.com/ubuntu focal main universe`
```shell
 $ sudo apt-get update
 $ sudo apt-get install sphinx-doc sphinx-common python-dev libboost-python1.67-dev 
```

- arch linux (2020-03-25)
```shell
 $ sudo pacman -S scons libtool autoconf procps-ng coreutils dash file gcc git gzip acl boost ncurses libtomcrypt libtommath make patch sed tar wget python2-numpy python-sphinx
```

### libstring_util

you can get libstring_util from
  https://gitlab.com/links_and_nodes/libstring_util
or
  https://rmc-github.robotic.dlr.de/common/libstring_util

```shell
 $ cd /path/to
 $ git clone .../libstring_util.git
 $ cd libstring_util
 $ autoreconf -if
 $ mkdir build; cd build
 $ ../configure --prefix=/usr/local
 $ make
 $ sudo make install
```

### links_and_nodes

```shell
 $ cd /path/to
 $ git clone --recursive .../links_and_nodes.git
 $ cd links_and_nodes
 $ scons --prefix=/usr/local
 $ sudo scons --prefix=/usr/local install
```
this will build the library, python binding and the daemon and intall them
to /usr/local.
you can specify a different target with the option `--prefix=/home/user/my_local`.
but be careful to always specify an absolute path as prefix!

depending on which install-prefix and python version you chose, you might want to add this to
your e.g. ~/.bashrc file:
```shell
export LN_PREFIX=/usr/local
export PATH=$LN_PREFIX/bin:$PATH
export LD_LIBRARY_PATH=$LN_PREFIX/lib:$LD_LIBRARY_PATH
# for debian and related:
export PYTHONPATH=$LN_PREFIX/lib/python3/dist-packages:$PYTHONPATH
# for suse, ARCH:
export PYTHONPATH=$LN_PREFIX/lib/python3/site-packages:$PYTHONPATH
```
(not needed if you install to --prefix=/usr)

## optional runtime dependencies
to start the ln_manager gui:
- debian, ubuntu18.04, ubuntu20.04
```shell
 $ sudo apt-get install openssh-client python-gi gir1.2-gtk-3.0 gir1.2-vte-2.91
```
- arch
```shell
 $ sudo pacman -S openssh python2-gobject gtk3 vte3
```

to start notebooks (pygtksvnb):
- newer debian's
```shell
 $ sudo apt-get install gir1.2-gtksource-4 python-matplotlib
```
- debian stretch
```shell
 $ sudo apt-get install gir1.2-gtksource-3 python-matplotlib
```
- ubuntu18.04
```shell
 $ sudo apt-get install gir1.2-gtksource-3.0 python-matplotlib
```
- ubuntu20.04
```shell
 $ sudo apt-get install gir1.2-gtksource-4
 $ wget https://bootstrap.pypa.io/pip/2.7/get-pip.py -O get-pip.py
 $ python get-pip.py --force-reinstall
 $ python -m pip install pyparsing matplotlib
```
- arch
```shell
 $ sudo pacman -S gtksourceview4 python-matplotlib
```

TODO: currently no working Gtk3 OpenGL widget found for pyscopes!


## how to test your installation
to test the python binding:
```shell
 $ python -c "import links_and_nodes as ln; print 'ok'"
 ok
```

to test the ln_manager:
```shell
 $ ln_manager --help
 2015-11-05 13:50:29.73 info    Manager:           $Id: Manager.py 20622 2013-10-01 13:12:52Z schm_fl $
 
 usage: ln_manager [-h] [-i instance_name] [-p PORT] [--log-level LEVEL] -c configuration_file
        -h                  show this usage info and exit
 ...
```

to test notebookts:
```shell
 $ python -m pyutils.pygtksvnb.notebook
```


## further reading
please read the incomplete $LN_documentation/links_and_nodes.md, have a
look at those example ln-clients below examples/ and maybe
grep for things of interesst in my personal test-cases
libln/tests.

the official API documentation is currently here:
- C-API: libln/include/ln/ln.h
- C++-API: libln/include/ln/cppwrapper.h
- python-API: python/links_and_nodes/_ln.cpp
  (have a look at the last function BOOST_PYTHON_MODULE() in that file)
  and python/links_and_nodes/ln_wrappers.py
- ln-config-file options: python/links_and_nodes_manager/SystemConfiguration.py

## license issues
all of links and nodes but the ln-library is released with a GPLv3
license. the ln-library is released with a LGPLv3 license - by that you
can dynamically link libln.so into your non-GPL compatible binaries
(needed header files use a non-copyleft BSD license).

