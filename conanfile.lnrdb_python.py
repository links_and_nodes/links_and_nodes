import os

from conan import ConanFile, conan_version
from conan.tools.files import copy
from conan.tools.scm import Version


class lnrdb_python_conan(ConanFile):
    package_type = "application"
    python_requires = "ln_conan/[^6.0.3]@common/stable", "cissy_conantools/[~1]@tools/stable"
    python_requires_extend = "ln_conan.Base"

    @property
    def cissy_conantools(self):
        return self.python_requires["cissy_conantools"].module

    name = "lnrdb_python"
    homepage = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"
    license = "LGPLv3"
    description = (
        "lnrdb is an efficient column-oriented database format used by lnrecorder. this is a pure-python binding (2 & 3)"
    )
    settings = "os", "arch", "compiler", "build_type"  # those are needed for conan 2 builds but deleted from the package id
    exports = []
    exports_sources = [
        # our self
        "python*",
        "!python/links_and_nodes*",
        "!python/py*",
        # will need base-build-system
        "site_scons*",
        "SConstruct*",
    ]

    def init(self):
        # add python dist
        pydist_options = {
            "python_version": ["2", "3"],
            "python_dist_version": self.cissy_conantools.get_python_dist_versions(),
        }
        pydist_default_options = {
            "python_version": "3",
            "python_dist_version": self.cissy_conantools.get_python_dist_latest_version(),
        }
        if Version(conan_version) < "2.0.0":
            self.options = {**self.options, **pydist_options}
            self.default_options = {**self.default_options, **pydist_default_options}
        else:
            self.options.update(pydist_options, pydist_default_options)

    def package_id(self):
        # the package id should not depend on any settings, delete all of them
        del self.info.settings.os
        del self.info.settings.arch
        del self.info.settings.build_type
        del self.info.settings.compiler

    def build_requirements(self): # would be nice to get rid of this boilerplate and have it in ln_conan only once...DRY
        super().build_requirements()
        self.tool_requires(
            self.cissy_conantools.get_python_dist_requirement(
                self.options.python_version.value, self.options.python_dist_version.value
            )
        )

    def requirements(self):
        self.requires(
            self.cissy_conantools.get_python_dist_requirement(
                self.options.python_version.value, self.options.python_dist_version.value
            )
        )

    def build(self):
        self.scons_build(
            "libln python",
            "--no-manager --no-python-api --no-python-base --without-daemon-authentication",
            with_python_lib=True,
        )

    def package(self):
        install = os.path.join("build", "python", self.install_sandbox, self.prefix[1:])
        copy(self, "*", os.path.join(self.build_folder, install), self.package_folder)

    def package_info(self):
        if Version(conan_version) < "2.0.0":
            self.env_info.PYTHONPATH.append(os.path.join(self.package_folder, self.python_path))
        self.runenv_info.append_path("PYTHONPATH", os.path.join(self.package_folder, self.python_path))
